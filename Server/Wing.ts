///////////////////////////////////////////////////////////////////////////////////////////////////////
// Dungeon Knight
///////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////// Cloud Script Handler Functions /////////////////////////
interface IResetWingStat{
    error: boolean;
    requiredGem: number;
}

let ResetWingStat = function(args: any, context: IPlayFabContext): IResetWingStat{
    let targetWingId = "";
    let lockStatCount = 0;
    let isLockOwnedStat1 = false;
    let isLockOwnedStat2 = false;
    let isLockEquipmentStat1 = false;
    let isLockEquipmentStat2 = false;
    let isLockEquipmentStat3 = false;

    let toReturn: IResetWingStat = {error: true, requiredGem: 0};

    if(args.targetWingId)
        targetWingId = args.targetWingId;
    if(args.isLockOwnedStat1)
        isLockOwnedStat1 = args.isLockOwnedStat1;
    if(args.isLockOwnedStat2)
        isLockOwnedStat2 = args.isLockOwnedStat2;
    if(args.isLockEquipmentStat1)
        isLockEquipmentStat1 = args.isLockEquipmentStat1;
    if(args.isLockEquipmentStat2)
        isLockEquipmentStat2 = args.isLockEquipmentStat2;
    if(args.isLockEquipmentStat3)
        isLockEquipmentStat3 = args.isLockEquipmentStat3;

    if(targetWingId != "")
    {        
        // Get user inventory
        let currentInventory = server.GetUserInventory(
            {
                PlayFabId: currentPlayerId
            }
        );

        // Get target character instance
        let targetWingInstance = currentInventory.Inventory.filter(n => n.ItemId == targetWingId)[0];
        
        // Get custom data of the instance
        let tmpWingCustomData = GetWingCustomData(null, targetWingInstance);
        
        let tmpOwnedInfo = tmpWingCustomData.ownedStatInfo;
        let tmpEquipmentInfo = tmpWingCustomData.equipmentStatInfo;

        let getTitleDataResult = server.GetTitleData({
            Keys: [
                "balance_inventory_growth",
                "setting_json"
            ]
        }).Data;
    
        let tmpBalanceGrowthSheet = JSON.parse(getTitleDataResult["balance_inventory_growth"]);
        let settingInfo = JSON.parse(getTitleDataResult["setting_json"]);
        
        if(tmpBalanceGrowthSheet != null && tmpBalanceGrowthSheet.WingInfo != null && settingInfo != null && settingInfo.WingResetStatPriceList != null) 
        {
            // Gacha stat rank
            if(!isLockOwnedStat1)
            {
                var resetResult = GetResetOwnedStat(tmpBalanceGrowthSheet.WingInfo);
                tmpOwnedInfo.Stat1Rank = resetResult.rank;
                tmpOwnedInfo.Stat1SubRank = resetResult.subRank;
            }
            else
                lockStatCount = lockStatCount + 1;
            
            if(!isLockOwnedStat2)
            {
                var resetResult = GetResetOwnedStat(tmpBalanceGrowthSheet.WingInfo);
                tmpOwnedInfo.Stat2Rank = resetResult.rank;
                tmpOwnedInfo.Stat2SubRank = resetResult.subRank;                
            }
            else
                lockStatCount = lockStatCount + 1;

            if(!isLockEquipmentStat1)
                tmpEquipmentInfo.Stat1Rank = GetResetEquipmentStat(tmpBalanceGrowthSheet.WingInfo);
            else
                lockStatCount = lockStatCount + 1;

            if(!isLockEquipmentStat2)
                tmpEquipmentInfo.Stat2Rank = GetResetEquipmentStat(tmpBalanceGrowthSheet.WingInfo);
            else
                lockStatCount = lockStatCount + 1;
            
            if(!isLockEquipmentStat3)
                tmpEquipmentInfo.Stat3Rank = GetResetEquipmentStat(tmpBalanceGrowthSheet.WingInfo);
            else
                lockStatCount = lockStatCount + 1;

            lockStatCount = Math.min(lockStatCount, 4);

            // Caculate reset price
            let requiredGem = parseInt(settingInfo.WingResetStatPriceList[lockStatCount]);
            
            // Check user's current gem
            if(requiredGem > currentInventory.VirtualCurrency["GE"].valueOf())
            {
                log.info("Don't have enough gem!" + requiredGem);
                return toReturn;
            }

            // Update custom data
            SetWingCustomData(targetWingInstance.ItemInstanceId, tmpOwnedInfo, tmpEquipmentInfo);

            // Subtract gem
            SubtractVirtualCurrency("GE", requiredGem);

            toReturn.error = false;
            toReturn.requiredGem = requiredGem;
            return toReturn;
        }
        else
        {
            log.info("Balance sheet is null!");
            return toReturn;
        }
    }
    else
    {
        log.info("Can't get valid parameters for wing!");
        return toReturn;
    }
}

handlers["resetWingStat"] = ResetWingStat;

interface IOpenWingBody{
    error: boolean;
    updatedLevel: Number;
    requiredFeather: number;
}

let OpenWingBody = function(args: any, context: IPlayFabContext): IOpenWingBody{
    let toReturn: IOpenWingBody = {error: true, updatedLevel:0, requiredFeather: 0};
    let purchasedWingCount = 0;

    // Get user inventory
    let currentInventory = server.GetUserInventory(
        {
            PlayFabId: currentPlayerId
        }
    );

    for(let i = 0; i < currentInventory.Inventory.length; i++)
    {
        if(currentInventory.Inventory[i].ItemId == "wing_body")
        {
            log.info("User already open wing body!");
            return toReturn;
        }
        else if(currentInventory.Inventory[i].ItemId.includes("wing_"))
        {
            purchasedWingCount = purchasedWingCount + 1;
        }
    }

    // Get setting info
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "setting_json"
        ]
    }).Data;

    let settingInfo = JSON.parse(getTitleDataResult["setting_json"]);

    if(settingInfo != null && settingInfo.WingUpgradePrice != null && settingInfo.WingBodyOpenCount != null)
    {
        if(purchasedWingCount < settingInfo.WingBodyOpenCount)
        {
            log.info("Purchased wing's count is not enough!");
            return toReturn;
        }
        
        // Get wing body price
        let requiredFeather = parseInt(settingInfo.WingUpgradePrice);
            
        // Check user's current feather
        if(requiredFeather > currentInventory.VirtualCurrency["FE"].valueOf())
        {
            log.info("Don't have enough Feather! required: " + requiredFeather);
            return toReturn;
        }

        let resultGrantItemsToUser = server.GrantItemsToUser
        (
            {
                PlayFabId: currentPlayerId,
                ItemIds: ["wing_body"]
            }
        );

        SetWingBodyCustomLevelData(resultGrantItemsToUser.ItemGrantResults[0].ItemInstanceId, 1);
        
        toReturn.error = false;
        toReturn.requiredFeather = requiredFeather;
        toReturn.updatedLevel = 1;
        return toReturn;
    }
    else
    {
        log.info("Setting info is null!");
        return toReturn;
    }
}
handlers["openWingBody"] = OpenWingBody;

interface IUpgradeWingBody{
    error: boolean;
    previousLevel: Number,
    updatedLevel: Number;
    isUpgradeSuccess: boolean;
    requiredFeather: number;
}

let UpgradeWingBody = function(args: any, context: IPlayFabContext): IUpgradeWingBody{
    let toReturn: IUpgradeWingBody = {error: true, previousLevel: 0, updatedLevel:0, isUpgradeSuccess:false, requiredFeather: 0};

    // Get user inventory
    let currentInventory = server.GetUserInventory(
        {
            PlayFabId: currentPlayerId
        }
    );

    // Get wing body instance
    let wingBodyId = "wing_body";
    let targetWingInstance = currentInventory.Inventory.filter(n => n.ItemId == wingBodyId)[0];

    if(targetWingInstance != null)
    {
        // Get wing body level from custom data       
        let wingBodyLevel = GetWingBodyLevel(targetWingInstance);

        let getTitleDataResult = server.GetTitleData({
            Keys: [
                "setting_json"
            ]
        }).Data;
    
        let settingInfo = JSON.parse(getTitleDataResult["setting_json"]);
        
        if(settingInfo != null && settingInfo.WingUpgradePrice != null && settingInfo.WingMaxLevel != null) 
        {
            toReturn.previousLevel = wingBodyLevel;

            // Check max level
            if(wingBodyLevel >= settingInfo.WingMaxLevel)
            {
                log.info("Wing body's level is max");
                return toReturn;
            }

            // Get wing upgrade price
            let requiredFeather = parseInt(settingInfo.WingUpgradePrice);
            
            // Check user's current feather
            if(requiredFeather > currentInventory.VirtualCurrency["FE"].valueOf())
            {
                log.info("Don't have enough Feather!" + requiredFeather);
                return toReturn;
            }

            // Logic for wing upgrade
            // randomNumber (0 ~ 99)
            let randomNumber = Math.min(Math.floor(Math.random() * 100), 99);
            log.info("randomNumber: " + randomNumber);

            if(randomNumber < Math.max(100 - wingBodyLevel, 20))
            {
                // Update custom data after wing level up success
                wingBodyLevel = wingBodyLevel + 1; 
                SetWingBodyCustomLevelData(targetWingInstance.ItemInstanceId, wingBodyLevel);
                
                toReturn.isUpgradeSuccess = true;
            }

            // Subtract feather
            SubtractVirtualCurrency("FE", requiredFeather);

            toReturn.error = false;
            toReturn.requiredFeather = requiredFeather;
            toReturn.updatedLevel = wingBodyLevel;
            return toReturn;
        }
        else
        {
            log.info("Setting info is null!");
            return toReturn;
        }
    }
    else
    {
        log.info("Can't find target wing in inventory!");
        return toReturn;
    }
}

handlers["upgradeWingBody"] = UpgradeWingBody;


///////////////////////// HELPER FUNCTIONS (NOT DIRECTLY CALLABLE FROM THE CLIENT) /////////////////////////
interface IWingCustomData{
    ownedStatInfo: any;
    equipmentStatInfo: any;
}

function GetWingCustomData(defaultCustomData: any, wingInstance: PlayFabServerModels.ItemInstance): IWingCustomData
{
    let tmpDefaultCustomData = null;
    let toReturn : IWingCustomData = {ownedStatInfo: null, equipmentStatInfo: null};

    if(wingInstance.CustomData == null)
    {
        if(defaultCustomData == null)
        {
            // If not existing, we need to create a new custom data
            let tmpTitleData = server.GetTitleData({
                Keys: [
                    "default_custom_data"
                ]
            }).Data;

            tmpDefaultCustomData = JSON.parse(tmpTitleData["default_custom_data"]);
        }
        else
            tmpDefaultCustomData = defaultCustomData;
            
        toReturn.ownedStatInfo = tmpDefaultCustomData.OwnedStatInfo;
        toReturn.equipmentStatInfo = tmpDefaultCustomData.EquipmentStatInfo;
    }
    else
    {
        if(wingInstance.CustomData.OwnedStatInfo != null && wingInstance.CustomData.EquipmentStatInfo != null)
        {
            toReturn.ownedStatInfo = JSON.parse(wingInstance.CustomData.OwnedStatInfo);
            toReturn.equipmentStatInfo = JSON.parse(wingInstance.CustomData.EquipmentStatInfo);
        }
        else
        {
            if(defaultCustomData == null)
            {
                // If not existing, we need to create a new custom data
                let tmpTitleData = server.GetTitleData({
                    Keys: [
                        "default_custom_data"
                    ]
                }).Data;

                tmpDefaultCustomData = JSON.parse(tmpTitleData["default_custom_data"]);
            }
            else
                tmpDefaultCustomData = defaultCustomData;
                
            toReturn.ownedStatInfo = tmpDefaultCustomData.OwnedStatInfo;
            toReturn.equipmentStatInfo = tmpDefaultCustomData.EquipmentStatInfo;
        } 
    }

    return toReturn;
}

function SetWingCustomData(wingInstanceId: string, ownedStatInfo: any, equipmentStatInfo: any)
{
    let customDataUpdateResult = server.UpdateUserInventoryItemCustomData(
        {
            PlayFabId: currentPlayerId,
            ItemInstanceId: wingInstanceId,
            Data: 
                {
                    "OwnedStatInfo": unescape(JSON.stringify(ownedStatInfo)),
                    "EquipmentStatInfo": unescape(JSON.stringify(equipmentStatInfo))
                }
        }
    );
}

function GetWingBodyLevel(wingBodyInstance: PlayFabServerModels.ItemInstance): number
{
    let level = 0;

    // Check null
    if(wingBodyInstance != null && wingBodyInstance.CustomData != null && wingBodyInstance.CustomData.Level != null)
       level = parseInt(wingBodyInstance.CustomData.Level);

    return level;
}

function SetWingBodyCustomLevelData(wingBodyInstanceId: string, level: number)
{
    let customDataUpdateResult = server.UpdateUserInventoryItemCustomData(
        {
            PlayFabId: currentPlayerId,
            ItemInstanceId: wingBodyInstanceId,
            Data: 
                {
                    "Level": level.toString()
                }
        }
    );
}

interface IResetOwnedStat{
    rank: string;
    subRank: string;
}

function GetResetOwnedStat(wingInfo: any): IResetOwnedStat
{
    // Make a summon talbe
    let toReturn : IResetOwnedStat = {rank: "E", subRank: "0"};
    let randomNumber = Math.min(Math.floor(Math.random() * 1000), 999);
    let summonInfoList = [];

    for(let i = 0; i < wingInfo.length; i ++)
    {
        let summonPower = 0;

        for(let j = i; j < wingInfo.length; j ++)
        {
            summonPower = summonPower + parseInt(wingInfo[j].SummonPower);
        }
        
        summonInfoList.push({rank: wingInfo[i].Rank, summonPower: summonPower});
    }

    for(let i = summonInfoList.length - 1; i >= 0; i--)
    {
        if(randomNumber < summonInfoList[i].summonPower)
        {
            toReturn.rank = summonInfoList[i].rank;
            toReturn.subRank = Math.min(Math.floor(Math.random() * 4), 3).toString();
            return toReturn;
        }
    }
}

function GetResetEquipmentStat(wingInfo: any): string
{
    // Make a summon talbe
    let toReturn = "E";
    let randomNumber = Math.min(Math.floor(Math.random() * 1000), 999);
    let summonInfoList = [];

    for(let i = 0; i < wingInfo.length; i ++)
    {
        let summonPower = 0;

        for(let j = i; j < wingInfo.length; j ++)
        {
            summonPower = summonPower + parseInt(wingInfo[j].SummonPower);
        }
        
        summonInfoList.push({rank: wingInfo[i].Rank, summonPower: summonPower});
    }

    for(let i = summonInfoList.length - 1; i >= 0; i--)
    {        
        if(randomNumber < summonInfoList[i].summonPower)
        {
            toReturn = summonInfoList[i].rank;
            return toReturn;
        }
    }
}