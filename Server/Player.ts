///////////////////////////////////////////////////////////////////////////////////////////////////////
// Dungeon Knight
///////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////// Cloud Script Handler Functions /////////////////////////

interface IDefaultPlayerSetting 
{
    error: boolean;
    userData: string;
    inventoryData: string;
}

let DefaultPlayerSetting = function (args: any, context: IPlayFabContext): IDefaultPlayerSetting 
{
    let toReturn : IDefaultPlayerSetting = {error: true, userData: "", inventoryData: ""};

    // Check wether default settings have been applied
    let didGetDefaultItem = server.GetUserData({
        PlayFabId: currentPlayerId,
        Keys: ["is_default_settings_applied"]
    }).Data["is_default_settings_applied"];

    if(didGetDefaultItem == null || didGetDefaultItem.Value == "false")
    {
        // Get title data
        let getTitleDataResult = server.GetTitleData({
            Keys: [
                "default_user_data",
                "default_inventory_data"
            ]
        }).Data;

        if(getTitleDataResult["default_user_data"] != null && getTitleDataResult["default_inventory_data"] != null)
        {
            log.info(getTitleDataResult["default_user_data"]);

            // Input default user and inventory info
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "is_default_settings_applied": "true",
                    "user_data": getTitleDataResult["default_user_data"],
                    "inventory_data": getTitleDataResult["default_inventory_data"]
                }
            });

            toReturn.userData = getTitleDataResult["default_user_data"];
            toReturn.inventoryData = getTitleDataResult["default_inventory_data"];
            toReturn.error = false;
        }
    }

    return toReturn;
}

handlers["defaultPlayerSetting"] = DefaultPlayerSetting;

interface IPushNotificationSetting
{
    error: boolean;
}

let PushNotificationSetting = function (args: any, context: IPlayFabContext): IPushNotificationSetting 
{
    let toReturn : IPushNotificationSetting = {error: true};
    let isPushOff = false;

    if(args.isPushOff)
        isPushOff = args.isPushOff;

    if(isPushOff)
    {
        let addPlayerTagResult = server.AddPlayerTag({
            PlayFabId: currentPlayerId,
            TagName: "PushOff"
        });
    }
    else
    {
        let removePlayerTagResult = server.RemovePlayerTag({
            PlayFabId: currentPlayerId,
            TagName: "PushOff"
        });
    }

    toReturn.error = false;
    return toReturn;
}

handlers["pushNotificationSetting"] = PushNotificationSetting;

interface ISyncPlayerData
{
    error: boolean;
    reasonOfError: string;
    gemsCollided: boolean;
    gemsChange: number;
    unsyncedGemChangeVersion: number;
    syncOrigin: string;
}

let SyncPlayerData = function (args: any, context: IPlayFabContext): ISyncPlayerData 
{
    let toReturn : ISyncPlayerData = {error: true, reasonOfError: "", gemsCollided: false, gemsChange: 0, unsyncedGemChangeVersion: -1, syncOrigin: ""};

    let serverIdModifier = GetServerIdModifier(currentPlayerId);
    
    let gemsChange = 0
    if(args.gemsChange != null)
        gemsChange = args.gemsChange;
    toReturn.gemsChange = gemsChange;

    let inventoryData = "";
    if(args.inventoryData != null)
        inventoryData = args.inventoryData;
        
    let userData = "";
    if(args.userData != null)
        userData = args.userData;

    let stageProgress = 0;
    if(args.stageProgress != null)
        stageProgress = args.stageProgress;

    let killedMobsForCurrentStage = -1;
    if(args.killedMobsForCurrentStage != null)
        killedMobsForCurrentStage = args.killedMobsForCurrentStage;

    let dps = 0;
    if(args.dps != null)
        dps = args.dps;

    let isSuspicious = false;
    if(args.isSuspicious != null)
        isSuspicious = args.isSuspicious;

    let progressValue = -1;
    if(args.progressValue != null)
        progressValue = args.progressValue;

    let localCacheLost = false;
    if(args.localCacheLost != null)
        localCacheLost = args.localCacheLost;

    let unsyncedGemChangeVersion = -1;
    if(args.unsyncedGemChangeVersion != null)
        unsyncedGemChangeVersion = args.unsyncedGemChangeVersion;
    toReturn.unsyncedGemChangeVersion = unsyncedGemChangeVersion;

    if(args.syncOrigin != null)
        toReturn.syncOrigin = args.syncOrigin;

    log.info(userData);
    log.info(inventoryData);

    // Possibly cheater
    if(isSuspicious)
    {
        server.UpdatePlayerStatistics({
            PlayFabId: currentPlayerId,
            Statistics: [
                {
                    "StatisticName": "cheaters",
                    "Value": 1
                }
            ]
        });
        
        let ban : PlayFabServerModels.BanRequest = {
            PlayFabId: currentPlayerId,
            Reason: "Cheater"
        };
        server.BanUsers({Bans: [ban]});

        return toReturn;
    }

    // Local cache removed... if it keeps increasing too high, this may mean cheating attempts
    if(localCacheLost)
    {
        server.UpdatePlayerStatistics({
            PlayFabId: currentPlayerId,
            Statistics: [
                {
                    "StatisticName": "local_cache_lost",
                    "Value": 1
                }
            ]
        });
    }

    let getReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: [
            "progress_value",
            "unsynced_gem_change_version"
        ]
    });

    // Possibly multiple devices lead collision
    if(getReadOnlyDataResult.Data["progress_value"] != null && parseInt(getReadOnlyDataResult.Data["progress_value"].Value) > progressValue)
    {
        toReturn.reasonOfError = "data_collision";
        return toReturn;
    }

    if(gemsChange != 0 && inventoryData != "")
    {
        var inventoryFix = JSON.parse(inventoryData);
        inventoryFix.UnsyncedGem = 0;
        inventoryData = JSON.stringify(inventoryFix);
    }

    let dataTouse = {};
    dataTouse["user_data"] = userData;
    dataTouse["inventory_data"] = inventoryData;
    dataTouse["progress_value"] = progressValue.toString();
    dataTouse["unsynced_gem_change_version"] = unsyncedGemChangeVersion.toString();

    // Use this as confirmation for raid
    if(toReturn.syncOrigin == "EnterRaid")
    {
        let currentTimestampHour = Math.floor(+ new Date() / (1000 * 3600));
        let currentTimestampDay = Math.floor(+ new Date() / (1000 * 3600 * 24));
        dataTouse["last_confirmed_raid_timestamp_hour"] = currentTimestampHour.toString();
        dataTouse["last_confirmed_raid_timestamp_day"] = currentTimestampDay.toString();
    }

    let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Data: dataTouse
    });

    if(gemsChange > 0)
    {
        if(gemsChange > 1000000)
        {
            let getPlayerCombinedInfoResult = server.GetPlayerCombinedInfo({
                PlayFabId: currentPlayerId,
                InfoRequestParameters: {
                    GetCharacterInventories: false,
                    GetCharacterList: false,
                    GetPlayerStatistics: false,
                    GetTitleData: false,
                    GetUserAccountInfo: false,
                    GetUserData: false,
                    GetUserInventory: false,
                    GetUserReadOnlyData: false,
                    GetUserVirtualCurrency: false,
                    GetPlayerProfile: true,
                    ProfileConstraints: {
                        ShowAvatarUrl: false,
                        ShowBannedUntil: false,
                        ShowCampaignAttributions: false,
                        ShowContactEmailAddresses: false,
                        ShowCreated: false,
                        ShowDisplayName: false,
                        ShowLastLogin: false,
                        ShowLinkedAccounts: false,
                        ShowLocations: false,
                        ShowMemberships: false,
                        ShowOrigination: false,
                        ShowPushNotificationRegistrations: false,
                        ShowStatistics: false,
                        ShowTags: false,
                        ShowTotalValueToDateInUsd: true,
                        ShowValuesToDate: false,
                    }
                }
            }).InfoResultPayload;
            
            let playerValue = 0;
            if(
                getPlayerCombinedInfoResult != null 
                && getPlayerCombinedInfoResult.PlayerProfile != null
                && getPlayerCombinedInfoResult.PlayerProfile.TotalValueToDateInUSD != null
            )
            {
                playerValue = getPlayerCombinedInfoResult.PlayerProfile.TotalValueToDateInUSD;
            }

            if(playerValue < 10000)
            {
                server.UpdatePlayerStatistics({
                    PlayFabId: currentPlayerId,
                    Statistics: [
                        {
                            "StatisticName": "cheaters",
                            "Value": 1
                        }
                    ]
                });
                
                let ban : PlayFabServerModels.BanRequest = {
                    PlayFabId: currentPlayerId,
                    Reason: "Cheater2"
                };
                server.BanUsers({Bans: [ban]});
        
                return toReturn;
            }
            else
            {
                toReturn.gemsCollided = ApplyAsyncedGems(gemsChange, unsyncedGemChangeVersion, getReadOnlyDataResult);
            }
        }
        else if(gemsChange > 100000)
        {
            server.UpdatePlayerStatistics({
                PlayFabId: currentPlayerId,
                Statistics: [
                    {
                        "StatisticName": "gem_increased_a_lot",
                        "Value": 1
                    }
                ]
            });

            toReturn.gemsCollided = ApplyAsyncedGems(gemsChange, unsyncedGemChangeVersion, getReadOnlyDataResult);
        }
        else
        {
            toReturn.gemsCollided = ApplyAsyncedGems(gemsChange, unsyncedGemChangeVersion, getReadOnlyDataResult);
        }
    }
    else if(gemsChange < 0)
    {
        toReturn.gemsCollided = ApplyAsyncedGems(gemsChange, unsyncedGemChangeVersion, getReadOnlyDataResult);
    }

    if(stageProgress != 0 && killedMobsForCurrentStage != -1)
    {
        server.UpdatePlayerStatistics({
            PlayFabId: currentPlayerId,
            Statistics: [
                {
                    "StatisticName": "ranking" + serverIdModifier,
                    "Value": Math.min(stageProgress, 2100000) * 100000 + Math.min(killedMobsForCurrentStage, 99999)
                },
                {
                    "StatisticName": "dps" + serverIdModifier,
                    "Value": dps
                }
            ]
        });
    }

    toReturn.error = false;
    return toReturn;
}

handlers["syncPlayerData"] = SyncPlayerData;

interface IUpdateVirtualServer
{
    error: boolean;
    serverId: string;
}

let UpdateVirtualServer = function (args: any, context: IPlayFabContext): IUpdateVirtualServer 
{
    let toReturn : IUpdateVirtualServer = {serverId: "", error: true};
    let serverId = "";

    if(args && args.serverId)
        serverId = args.serverId;

    let addPlayerTagResult = server.AddPlayerTag({
        PlayFabId: currentPlayerId,
        TagName: "Server" + serverId
    });
    
    toReturn.serverId = serverId;
    toReturn.error = false;
    return toReturn;
}

handlers["updateVirtualServer"] = UpdateVirtualServer;

interface ILeaveVirtualServer
{
    error: boolean;
}

let LeaveVirtualServer = function (args: any, context: IPlayFabContext): ILeaveVirtualServer 
{
    let toReturn : ILeaveVirtualServer = {error: true};
    let serverId = "";

    if(args && args.serverId)
        serverId = args.serverId;

    let removePlayerTagResult = server.RemovePlayerTag({
        PlayFabId: currentPlayerId,
        TagName: "Server" + serverId
    });

    toReturn.error = false;
    return toReturn;
}

handlers["leaveVirtualServer"] = LeaveVirtualServer;

interface IRefreshLastDevice
{
    willOtherClose: boolean;
    otherWasPlayingRecently: boolean;
    needClose: boolean;
    isChatBanned: boolean;
    minAndroidVersionCode: string;
    miniOSVersionCode: string;
    minLoginCode: number;
    error: boolean;
}

let RefreshLastDevice = function (args: any, context: IPlayFabContext): IRefreshLastDevice 
{
    let toReturn : IRefreshLastDevice = {willOtherClose: false, otherWasPlayingRecently: false, needClose: false, isChatBanned: false, minAndroidVersionCode: "0.0.0", miniOSVersionCode: "0.0.0", minLoginCode: 0, error: true};

    let deviceTag = "";
    if(args.deviceTag != null)
        deviceTag = args.deviceTag;

    let isForLogin = false;
    if(args.isForLogin != null)
        isForLogin = args.isForLogin;

    let currentTimestampSecond = Math.floor(+ new Date() / (1000));

    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: [
            "last_device_tag", "last_device_timestamp", "is_chat_banned"
        ]
    });

    if(getUserReadOnlyDataResult.Data["last_device_tag"] != null && getUserReadOnlyDataResult.Data["last_device_tag"].Value != deviceTag)
    {
        if(isForLogin)
        {
            if(getUserReadOnlyDataResult.Data["last_device_timestamp"] != null)
            {
                if(currentTimestampSecond - parseInt(getUserReadOnlyDataResult.Data["last_device_timestamp"].Value) < 60)
                {
                    toReturn.otherWasPlayingRecently = true;
                }
            }
            toReturn.willOtherClose = true;
        }
        else
        {
            toReturn.needClose = true;
        }
    }

    // Check chat banned
    if(getUserReadOnlyDataResult.Data["is_chat_banned"] != null && getUserReadOnlyDataResult.Data["is_chat_banned"].Value == "true")
        toReturn.isChatBanned = true;

    if(isForLogin)
    {
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "last_device_tag": deviceTag
            }
        });
    }
    else
    {
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "last_device_timestamp": currentTimestampSecond.toString()
            }
        });
    }

    // Will be used for kick users
    toReturn.minAndroidVersionCode = "1.3.5";
    toReturn.miniOSVersionCode = "0.0.0";
    toReturn.minLoginCode = 0;

    toReturn.error = false;
    return toReturn;
}

handlers["refreshLastDevice"] = RefreshLastDevice;

let RefreshLastDeviceQA = function (args: any, context: IPlayFabContext): IRefreshLastDevice 
{
    let toReturn : IRefreshLastDevice = {willOtherClose: false, otherWasPlayingRecently: false, needClose: false, isChatBanned: false, minAndroidVersionCode: "0.0.0", miniOSVersionCode: "0.0.0", minLoginCode: 0, error: true};

    let deviceTag = "";
    if(args.deviceTag != null)
        deviceTag = args.deviceTag;

    let isForLogin = false;
    if(args.isForLogin != null)
        isForLogin = args.isForLogin;

    let currentTimestampSecond = Math.floor(+ new Date() / (1000));

    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: [
            "last_device_tag", "last_device_timestamp", "is_chat_banned"
        ]
    });

    if(getUserReadOnlyDataResult.Data["last_device_tag"] != null && getUserReadOnlyDataResult.Data["last_device_tag"].Value != deviceTag)
    {
        if(isForLogin)
        {
            if(getUserReadOnlyDataResult.Data["last_device_timestamp"] != null)
            {
                if(currentTimestampSecond - parseInt(getUserReadOnlyDataResult.Data["last_device_timestamp"].Value) < 60)
                {
                    toReturn.otherWasPlayingRecently = true;
                }
            }
            toReturn.willOtherClose = true;
        }
        else
        {
            toReturn.needClose = true;
        }
    }

    // Check chat banned
    if(getUserReadOnlyDataResult.Data["is_chat_banned"] != null && getUserReadOnlyDataResult.Data["is_chat_banned"].Value == "true")
        toReturn.isChatBanned = true;

    if(isForLogin)
    {
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "last_device_tag": deviceTag
            }
        });
    }
    else
    {
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "last_device_timestamp": currentTimestampSecond.toString()
            }
        });
    }

    // Will be used for kick users
    toReturn.minAndroidVersionCode = "1.3.5";
    toReturn.miniOSVersionCode = "0.0.0";
    toReturn.minLoginCode = 1;

    toReturn.error = false;
    return toReturn;
}

handlers["refreshLastDeviceQA"] = RefreshLastDeviceQA;

interface IAddChatBlockList
{
    updatedChatBlockData: {};
    reasonOfError: string;
    error: boolean;
}

let AddChatBlockList = function (args: any, context: IPlayFabContext): IAddChatBlockList 
{
    let toReturn : IAddChatBlockList = {updatedChatBlockData: {}, reasonOfError: "", error: true};
    let targetNickname = "";
    let targetId = "";

    if(args.targetNickname != null)
        targetNickname = args.targetNickname;

    if(args.targetId != null)
        targetId = args.targetId;

    if(targetNickname == "" || targetId == "")
        return toReturn;

    // Get chat block list
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["chat_block_data"]
    });

    if(getUserReadOnlyDataResult.Data["chat_block_data"] != null)
    {
        let chatBlockData = JSON.parse(getUserReadOnlyDataResult.Data["chat_block_data"].Value);

        // Check chat block list limit (100)
        if(chatBlockData.ChatBlockList.length >= 100)
        {
            toReturn.reasonOfError = "ban_over_limit";
            return toReturn;
        }

        // Check duplicate chat block
        if(chatBlockData.ChatBlockList.filter(n => n.Id == targetId).length > 0)
        {
            toReturn.reasonOfError = "ban_duplicate";
            return toReturn;
        }

        chatBlockData.ChatBlockList.push({Id: targetId, Nickname: targetNickname});

        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "chat_block_data": JSON.stringify(chatBlockData)
            }
        });

        toReturn.updatedChatBlockData = chatBlockData;
        toReturn.error = false;
    }
    else
    {
        let defaultChatBlockData = {"ChatBlockList":[]};

        defaultChatBlockData.ChatBlockList.push({Id: targetId, Nickname: targetNickname});

        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "chat_block_data": JSON.stringify(defaultChatBlockData)
            }
        });

        toReturn.updatedChatBlockData = defaultChatBlockData;
        toReturn.error = false;
    }

    return toReturn;
}

handlers["addChatBlockList"] = AddChatBlockList;

interface IAddBanAndRemoveStat{
    error: boolean;
}

let AddBanAndRemoveStat = function (args: any, context: IPlayFabContext): IAddBanAndRemoveStat 
{
    let toReturn : IAddBanAndRemoveStat = {error: true};

    // Get server id
    let getPlayerTagsResult = server.GetPlayerTags({
        PlayFabId: currentPlayerId
    });

    let serverId = "";
    if(getPlayerTagsResult != null && getPlayerTagsResult.Tags.length > 0)
    {
        let serverTags = getPlayerTagsResult.Tags.filter(n => n.indexOf("Server") >= 0);
        if(serverTags != null && serverTags.length > 0)
            serverId = GetServerIdFromTag(serverTags[0]);
    }

    if(serverId != "")
    {
        let statistics = server.GetPlayerStatistics({
            PlayFabId: currentPlayerId,
            StatisticNames: ["arena_" + serverId]
        });

        server.UpdatePlayerStatistics({
            PlayFabId: currentPlayerId,
                Statistics: [
                {
                    "StatisticName": "arena_" + serverId,
                    "Value": statistics.Statistics[0].Value * -1
                },
                {
                    "StatisticName": "ranking_" + serverId,
                    "Value": 0
                }
            ]
        });
    }

    let ban : PlayFabServerModels.BanRequest = {
        PlayFabId: currentPlayerId,
        Reason: "Cheater"
    };
    server.BanUsers({Bans: [ban]});

    toReturn.error = false;
    return toReturn;
}

handlers["addBanAndRemoveStat"] = AddBanAndRemoveStat;

interface IRemoveChatBlockList
{
    updatedChatBlockData: {};
    error: boolean;
}

let RemoveChatBlockList = function (args: any, context: IPlayFabContext): IRemoveChatBlockList 
{
    let toReturn : IRemoveChatBlockList = {updatedChatBlockData: {}, error: true};
    let targetId = "";

    if(args.targetId != null)
        targetId = args.targetId;

    if(targetId == "")
        return toReturn;

    // Get chat block list
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["chat_block_data"]
    });

    if(getUserReadOnlyDataResult.Data["chat_block_data"] != null)
    {
        let chatBlockData = JSON.parse(getUserReadOnlyDataResult.Data["chat_block_data"].Value);
        let updatedChatBlockList = chatBlockData.ChatBlockList.filter(n => n.Id != targetId);
        
        chatBlockData.ChatBlockList = updatedChatBlockList;     

        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "chat_block_data": JSON.stringify(chatBlockData)
            }
        });

        toReturn.updatedChatBlockData = chatBlockData;
        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;
}

handlers["removeChatBlockList"] = RemoveChatBlockList;

interface ICheckCheater
{
    error: boolean;
}

let CheckCheater = function (args: any, context: IPlayFabContext): ICheckCheater 
{
    let toReturn : ICheckCheater = {error: true};

    let currentTimestampDay = Math.floor(+ new Date() / (1000 * 60 * 60 * 24));

    let limitWeaponId = 16;
    let limitStageProgress = 61;

    if(args.limitWeaponId)
        limitWeaponId = args.limitWeaponId;
    
    if(args.limitStageProgress)
        limitStageProgress = args.limitStageProgress;

    // Get user data and inventory data
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["user_data", "inventory_data"]
    });

    if(getUserReadOnlyDataResult.Data["user_data"] != null && getUserReadOnlyDataResult.Data["inventory_data"] != null)
    {
        let isWeaponSuspicious = true;
        let isStageSuspicious = true;
        let isOwnedBasicPackage = false;

        let userData = JSON.parse(getUserReadOnlyDataResult.Data["user_data"].Value);
        let inventoryData = JSON.parse(getUserReadOnlyDataResult.Data["inventory_data"].Value);
        let currentBestStageProgress = userData.StageProgress;
        
        // Check owned best weapon
        for(let i = 0; i < inventoryData.OwnedWeaponList.length ; i ++)
        {
            let weaponId = inventoryData.OwnedWeaponList[i].id;
            let convertedWeaponId = parseInt(weaponId.substring(7));
            
            if(convertedWeaponId > limitWeaponId)
            {
                isWeaponSuspicious = false;
                break;
            }
        }

        // Check best stage progress
        if(currentBestStageProgress < limitStageProgress)
            isStageSuspicious = false;

        // Check owned basic package 1
        let getUserInventoryResult = server.GetUserInventory(
            {
                PlayFabId: currentPlayerId
            }
        );
        
        if(getUserInventoryResult.Inventory.filter(n => n.ItemId == "bundle_package_basic_01")[0] != null)
            isOwnedBasicPackage = true;
        
        // Possibly cheater
        if(isWeaponSuspicious && isStageSuspicious && !isOwnedBasicPackage)
        {
            // Get server id
            let getPlayerTagsResult = server.GetPlayerTags({
                PlayFabId: currentPlayerId
            });
        
            let serverId = "";
            if(getPlayerTagsResult != null && getPlayerTagsResult.Tags.length > 0)
            {
                let serverTags = getPlayerTagsResult.Tags.filter(n => n.indexOf("Server") >= 0);
                if(serverTags != null && serverTags.length > 0)
                    serverId = GetServerIdFromTag(serverTags[0]);
            }

            if(serverId == "")
            {
                server.UpdatePlayerStatistics({
                    PlayFabId: currentPlayerId,
                        Statistics: [
                        {
                            "StatisticName": "cheater_stage_" + serverId,
                            "Value": currentTimestampDay
                        }
                    ]
                });
            }
            else
            {
                server.UpdatePlayerStatistics({
                    PlayFabId: currentPlayerId,
                        Statistics: [
                        {
                            "StatisticName": "cheater_stage_" + serverId,
                            "Value": currentTimestampDay
                        },
                        {
                            "StatisticName": "arena_" + serverId,
                            "Value": 0
                        },
                        {
                            "StatisticName": "arena_all_season_" + serverId,
                            "Value": 0
                        },
                        {
                            "StatisticName": "ranking_" + serverId,
                            "Value": 0
                        }
                    ]
                });
            }
            
            let ban : PlayFabServerModels.BanRequest = {
                PlayFabId: currentPlayerId,
                Reason: "Cheater"
            };
            server.BanUsers({Bans: [ban]});
        }
        else
            log.info("This user is not cheater");

        toReturn.error = false;
    }

    return toReturn;
}

handlers["checkCheater"] = CheckCheater;


interface ICheckPet
{
    error: boolean;
}

let CheckPet = function (args: any, context: IPlayFabContext): ICheckPet 
{
    let toReturn : ICheckPet = {error: true};

    let currentTimestampDay = Math.floor(+ new Date() / (1000 * 60 * 60 * 24));

    // Get user data and inventory data
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["user_data", "inventory_data"]
    });

    let superCriteria = 700;
    if(args.superCriteria)
        superCriteria = superCriteria;

    if(getUserReadOnlyDataResult.Data["user_data"] != null && getUserReadOnlyDataResult.Data["inventory_data"] != null)
    {
        let userData = JSON.parse(getUserReadOnlyDataResult.Data["user_data"].Value);
        let inventoryData = JSON.parse(getUserReadOnlyDataResult.Data["inventory_data"].Value);
        
        // Check pet
        let superPet = false;
        for(let i = 0; i < inventoryData.OwnedPetList.length ; i ++)
        {
            let petLevel = inventoryData.OwnedPetList[i].level;
            if(petLevel > superCriteria)
            {
                superPet = true;
            }
        }

        // Check basic package 1
        let getUserInventoryResult = server.GetUserInventory(
            {
                PlayFabId: currentPlayerId
            }
        );
        
        let isOwnedBasicPackage = false;
        if(getUserInventoryResult.Inventory.filter(n => n.ItemId == "bundle_package_basic_01")[0] != null)
            isOwnedBasicPackage = true;

        // Possibly cheater
        if(superPet && !isOwnedBasicPackage)
        {
            // Get server id
            let getPlayerTagsResult = server.GetPlayerTags({
                PlayFabId: currentPlayerId
            });
        
            let serverId = "";
            if(getPlayerTagsResult != null && getPlayerTagsResult.Tags.length > 0)
            {
                let serverTags = getPlayerTagsResult.Tags.filter(n => n.indexOf("Server") >= 0);
                if(serverTags != null && serverTags.length > 0)
                    serverId = GetServerIdFromTag(serverTags[0]);
            }

            if(serverId == "")
            {
                server.UpdatePlayerStatistics({
                    PlayFabId: currentPlayerId,
                        Statistics: [
                        {
                            "StatisticName": "cheater_super_pet",
                            "Value": currentTimestampDay
                        }
                    ]
                });
            }
            else
            {
                server.UpdatePlayerStatistics({
                    PlayFabId: currentPlayerId,
                        Statistics: [
                        {
                            "StatisticName": "cheater_super_pet_" + serverId,
                            "Value": currentTimestampDay
                        },
                        {
                            "StatisticName": "arena_" + serverId,
                            "Value": 0
                        },
                        {
                            "StatisticName": "arena_all_season_" + serverId,
                            "Value": 0
                        },
                        {
                            "StatisticName": "ranking_" + serverId,
                            "Value": 0
                        }
                    ]
                });
            }

            let ban : PlayFabServerModels.BanRequest = {
                PlayFabId: currentPlayerId,
                Reason: "Cheater"
            };
            server.BanUsers({Bans: [ban]});
        }
        else
            log.info("This user is not cheater");

        toReturn.error = false;
    }

    return toReturn;
}

handlers["checkPet"] = CheckPet;

interface ICheckBestPetOwned
{
    error: boolean;
}

let CheckBestPetOwned = function (args: any, context: IPlayFabContext): ICheckBestPetOwned 
{
    let toReturn : ICheckBestPetOwned = {error: true};
    let limitPetId = "pet_11";

    if(args.limitPetId)
        limitPetId = args.limitPetId;

    let inventorySize = 0;
    let getUserInventoryResult = server.GetUserInventory(
        {
            PlayFabId: currentPlayerId
        }
    );
    inventorySize = getUserInventoryResult.Inventory.length;

    // Get user data and inventory data
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["user_data", "inventory_data"]
    });

    let inventoryData = JSON.parse(getUserReadOnlyDataResult.Data["inventory_data"].Value);

    if(getUserReadOnlyDataResult.Data["user_data"] != null && getUserReadOnlyDataResult.Data["inventory_data"] != null)
    {
        let isOwned = false;
        
        // Check owned target pet
        for(let i = 0; i < inventoryData.OwnedPetList.length ; i ++)
        {
            let petId = inventoryData.OwnedPetList[i].id;
            
            if(petId == limitPetId)
            {
                isOwned = true;
                break;
            }
        }
        
        if(isOwned)
        {
            // Get server id
            let getPlayerTagsResult = server.GetPlayerTags({
                PlayFabId: currentPlayerId
            });
        
            let serverId = "";
            if(getPlayerTagsResult != null && getPlayerTagsResult.Tags.length > 0)
            {
                let serverTags = getPlayerTagsResult.Tags.filter(n => n.indexOf("Server") >= 0);
                if(serverTags != null && serverTags.length > 0)
                    serverId = GetServerIdFromTag(serverTags[0]);
            }

            // Ban 0 owners
            if(inventorySize == 0)
            {
                if(serverId != "")
                {
                    server.UpdatePlayerStatistics({
                        PlayFabId: currentPlayerId,
                            Statistics: [
                            {
                                "StatisticName": "owned_" + limitPetId + "_" + serverId,
                                "Value": inventorySize
                            },
                            {
                                "StatisticName": "arena_" + serverId,
                                "Value": 0
                            },
                            {
                                "StatisticName": "arena_all_season_" + serverId,
                                "Value": 0
                            },
                            {
                                "StatisticName": "ranking_" + serverId,
                                "Value": 0
                            }
                        ]
                    });

                    let ban : PlayFabServerModels.BanRequest = {
                        PlayFabId: currentPlayerId,
                        Reason: "Cheater"
                    };
                    server.BanUsers({Bans: [ban]});
                }
            }
            else
            {
                server.UpdatePlayerStatistics({
                    PlayFabId: currentPlayerId,
                        Statistics: [
                        {
                            "StatisticName": "owned_" + limitPetId + "_" + serverId,
                            "Value": inventorySize
                        }
                    ]
                });
            }
        }
        else
            log.info("This user is not owned !");

        toReturn.error = false;
    }

    return toReturn;
}

handlers["checkBestPetOwned"] = CheckBestPetOwned;

interface ICheckLegendary
{
    error: boolean;
}

let CheckLegendary = function (args: any, context: IPlayFabContext): ICheckLegendary 
{
    let toReturn : ICheckLegendary = {error: true};

    // Get user data and inventory data
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["inventory_data"]
    });

    if(getUserReadOnlyDataResult.Data["inventory_data"] != null)
    {
        let inventoryData = JSON.parse(getUserReadOnlyDataResult.Data["inventory_data"].Value);
        
        let isAllMax = true;
        
        if(inventoryData.OwnedLegendaryRelicList == null || inventoryData.OwnedLegendaryRelicList.length < 7)
        {
            isAllMax = false;
        }
        else
        {
            for(let i = 0; i < inventoryData.OwnedLegendaryRelicList.length ; i ++)
            {
                if(inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_01" && 
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 < 99)
                {
                    isAllMax = false;
                }
                
                if(inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_02" && 
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 < 1000)
                {
                    isAllMax = false;
                }

                if(inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_03" && 
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 < 50)
                {
                    isAllMax = false;
                }

                if(inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_04" && 
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 < 300)
                {
                    isAllMax = false;
                }

                if(inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_05" && 
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 < 300)
                {
                    isAllMax = false;
                }

                if(inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_06" && 
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 < 300)
                {
                    isAllMax = false;
                }

                if(inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_07" && 
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 < 200)
                {
                    isAllMax = false;
                }
            }
        }

        if(isAllMax)
        {
            server.UpdatePlayerStatistics({
                PlayFabId: currentPlayerId,
                    Statistics: [
                    {
                        "StatisticName": "finished_legendary",
                        "Value": 1
                    }
                ]
            });
        }

        toReturn.error = false;
    }

    return toReturn;
}

handlers["checkLegendary"] = CheckLegendary;

let CheckLegendary2 = function (args: any, context: IPlayFabContext): ICheckLegendary 
{
    let toReturn : ICheckLegendary = {error: true};

    // Get user data and inventory data
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["inventory_data"]
    });

    if(getUserReadOnlyDataResult.Data["inventory_data"] != null)
    {
        let inventoryData = JSON.parse(getUserReadOnlyDataResult.Data["inventory_data"].Value);

        let isAllMax = true;
        
        if(inventoryData.OwnedLegendaryRelicList == null || inventoryData.OwnedLegendaryRelicList.length < 7)
        {
            isAllMax = false;
        }
        else
        {
            for(let i = 0; i < inventoryData.OwnedLegendaryRelicList.length ; i ++)
            {
                if(inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_01" && 
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 < 99)
                {
                    isAllMax = false;
                }
                
                if(inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_02" && 
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 < 1000)
                {
                    isAllMax = false;
                }

                if(inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_03" && 
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 < 50)
                {
                    isAllMax = false;
                }

                if(inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_04" && 
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 < 300)
                {
                    isAllMax = false;
                }

                if(inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_05" && 
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 < 300)
                {
                    isAllMax = false;
                }

                if(inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_06" && 
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 < 300)
                {
                    isAllMax = false;
                }

                if(inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_07" && 
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 < 200)
                {
                    isAllMax = false;
                }
            }
        }
        
        let is02Max = false;
        if(inventoryData.OwnedLegendaryRelicList == null || inventoryData.OwnedLegendaryRelicList.length < 7)
        {
            is02Max = false;
        }
        else
        {
            for(let i = 0; i < inventoryData.OwnedLegendaryRelicList.length ; i ++)
            {
                if(inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_02" && 
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 >= 1000)
                {
                    is02Max = true;
                }
            }
        }

        if(is02Max && !isAllMax)
        {
            server.UpdatePlayerStatistics({
                PlayFabId: currentPlayerId,
                    Statistics: [
                    {
                        "StatisticName": "finished_legendary",
                        "Value": 2
                    }
                ]
            });
        }

        toReturn.error = false;
    }

    return toReturn;
}

handlers["checkLegendary2"] = CheckLegendary2;

interface ICheckArenaWeeklyBug
{
    error: boolean;
    removedGems: number;
    resultGems: number;
    expectedResultGems: number;
}

let CheckArenaWeeklyBug = function (args: any, context: IPlayFabContext): ICheckArenaWeeklyBug 
{
    let toReturn : ICheckArenaWeeklyBug = {error: false, removedGems: 0, resultGems: 0, expectedResultGems: 0};

    let getTitleDataResult = server.GetTitleInternalData({
        Keys: [
            "to_roll_back"
        ]
    });

    var listToUse = JSON.parse(getTitleDataResult.Data["to_roll_back"]);

    if(listToUse != null)
    {
        let resultOfFilter = listToUse.Data.filter(n => n.id == currentPlayerId);
        if(resultOfFilter.length > 0)
        {
            toReturn.removedGems = (resultOfFilter[0].count) * 500;
        }
    }
    
    if(toReturn.removedGems != 0)
    {
        let getUserInventoryResult = server.GetUserInventory(
            {
                PlayFabId: currentPlayerId
            }
        );

        toReturn.expectedResultGems = getUserInventoryResult.VirtualCurrency["GE"] - toReturn.removedGems;

        /*
        let subtractUserVirtualCurrencyResult = server.SubtractUserVirtualCurrency({
            PlayFabId: currentPlayerId,
            VirtualCurrency: "GE",
            Amount: toReturn.removedGems
        });

        toReturn.resultGems = subtractUserVirtualCurrencyResult.Balance;
        */
    }
    
    return toReturn;
}

handlers["checkArenaWeeklyBug"] = CheckArenaWeeklyBug;

interface IMoveServer
{
    error: boolean;
}

let MoveServer = function (args: any, context: IPlayFabContext): IMoveServer 
{
    let toReturn : IMoveServer = {error: false};
    let fromServerId = "";
    let toServerId = "";
    let markSJP = false;

    if(args && args.fromServerId)
        fromServerId = args.fromServerId;
    
    if(args && args.toServerId)
        toServerId = args.toServerId;

    if(args && args.markSJP)
        markSJP = true;

    let statistics = server.GetPlayerStatistics({
        PlayFabId: currentPlayerId,
        StatisticNames: [
            "ranking_" + fromServerId,
            "arena_" + fromServerId, 
            "arena_all_season_" + fromServerId,
            "star_" + fromServerId
        ]
    });

    let rankingOriginalValue = 0;
    let rankingOriginal = statistics.Statistics.filter(n => n.StatisticName == "ranking_" + fromServerId);
    if(rankingOriginal.length > 0)
        rankingOriginalValue = rankingOriginal[0].Value;

    let arenaOriginalValue = 0;
    let arenaOriginal = statistics.Statistics.filter(n => n.StatisticName == "arena_" + fromServerId);
    if(arenaOriginal.length > 0)
        arenaOriginalValue = arenaOriginal[0].Value;

    let arenaAllSeasonOriginalValue = 0;
    let arenaAllSeasonOriginal = statistics.Statistics.filter(n => n.StatisticName == "arena_all_season_" + fromServerId);
    if(arenaAllSeasonOriginal.length > 0)
        arenaAllSeasonOriginalValue = arenaAllSeasonOriginal[0].Value;

    let starOriginalValue = 0;
    let starOriginal = statistics.Statistics.filter(n => n.StatisticName == "star_" + fromServerId);
    if(starOriginal.length > 0)
        starOriginalValue = starOriginal[0].Value;
    
    log.info(rankingOriginalValue.toString());
    log.info(arenaOriginalValue.toString());
    log.info(arenaAllSeasonOriginalValue.toString());
    log.info(starOriginalValue.toString());

    server.UpdatePlayerStatistics({
        PlayFabId: currentPlayerId,
            Statistics: [
            {
                "StatisticName": "arena_" + fromServerId,
                "Value": arenaOriginalValue * -1
            },
            {
                "StatisticName": "arena_all_season_" + fromServerId,
                "Value": arenaAllSeasonOriginalValue * -1
            },
            {
                "StatisticName": "ranking_" + fromServerId,
                "Value": 0
            },
            {
                "StatisticName": "star_" + fromServerId,
                "Value": starOriginalValue * -1
            },
            {
                "StatisticName": "arena_" + toServerId,
                "Value": arenaOriginalValue
            },
            {
                "StatisticName": "arena_all_season_" + toServerId,
                "Value": arenaAllSeasonOriginalValue
            },
            {
                "StatisticName": "ranking_" + toServerId,
                "Value": rankingOriginalValue
            },
            {
                "StatisticName": "star_" + toServerId,
                "Value": starOriginalValue
            }
        ]
    });

    let removePlayerTagResult = server.RemovePlayerTag({
        PlayFabId: currentPlayerId,
        TagName: "Server" + fromServerId
    });

    let addPlayerTagResult = server.AddPlayerTag({
        PlayFabId: currentPlayerId,
        TagName: "Server" + toServerId
    });

    if(markSJP)
    {
        let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
            PlayFabId: currentPlayerId,
            StatisticName: "arena_" + fromServerId,
            MaxResultsCount: 1,
            Version: 110
        });

        if(getLeaderboardAroundUserResult.Leaderboard[0] != null) // Only give reward when play record exists
        {
            let lastSeasonLeaderboardEntry = getLeaderboardAroundUserResult.Leaderboard[0];
            let lastSeasonPosition = lastSeasonLeaderboardEntry.Position;
            let lastSeasonScore = lastSeasonLeaderboardEntry.StatValue;
            let lastSeasonRewardGem = 0;

            if(lastSeasonScore != 0)
            {
                // Give reward based on last season's performance
                if(lastSeasonPosition == 0)
                {
                    lastSeasonRewardGem = 2500;
                }
                else if(lastSeasonPosition == 1)
                {
                    lastSeasonRewardGem = 2000;
                }
                else if(lastSeasonPosition == 2)
                {
                    lastSeasonRewardGem = 1500;
                }
                else if(lastSeasonPosition < 10)
                {
                    lastSeasonRewardGem = 1000;
                }
                else if(lastSeasonPosition < 50)
                {
                    lastSeasonRewardGem = 750;
                }
                else if(lastSeasonPosition < 100)
                {
                    lastSeasonRewardGem = 250;
                }
                else
                {
                    lastSeasonRewardGem = 50;
                }
                GiveEventGemReward({amount: lastSeasonRewardGem}, context);
            }
        }

        getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
            PlayFabId: currentPlayerId,
            StatisticName: "star_" + fromServerId,
            MaxResultsCount: 1,
            Version: 7
        });

        if(getLeaderboardAroundUserResult.Leaderboard[0] != null) // Only give reward when play record exists
        {
            let lastSeasonLeaderboardEntry = getLeaderboardAroundUserResult.Leaderboard[0];
            let lastSeasonScore = lastSeasonLeaderboardEntry.StatValue;
            let lastSeasonRewardGem = 0;

            if(lastSeasonScore != 0)
            {
                // Give reward based on last season's performance
                if(lastSeasonScore >= 2000) // Grandmaster
                {
                    lastSeasonRewardGem = 5000;
                }
                else if(lastSeasonScore >= 1500) // Master
                {
                    lastSeasonRewardGem = 3000;
                }
                else if(lastSeasonScore >= 1000) // Diamond
                {
                    lastSeasonRewardGem = 2000;
                }
                else if(lastSeasonScore >= 750) // Platinum
                {
                    lastSeasonRewardGem = 1500;
                }
                else if(lastSeasonScore >= 500) // Gold
                {
                    lastSeasonRewardGem = 1000;
                }
                else if(lastSeasonScore >= 250) // Silver
                {
                    lastSeasonRewardGem = 750;
                }
                else // Bronze
                {
                    lastSeasonRewardGem = 500;
                }
                GiveEventGemReward({amount: lastSeasonRewardGem}, context);
            }
        }
    }

    toReturn.error = false;
    return toReturn;
}

handlers["moveServer"] = MoveServer;

interface IGiveLastSeasonReward
{
    error: boolean;
    gemsRewarded: number;
}

let GiveLastSeasonReward = function (args: any, context: IPlayFabContext): IGiveLastSeasonReward 
{
    let toReturn : IGiveLastSeasonReward = {error: false, gemsRewarded: 0};

    let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
        PlayFabId: currentPlayerId,
        StatisticName: "star_02",
        MaxResultsCount: 1,
        Version: 7
    });

    if(getLeaderboardAroundUserResult.Leaderboard[0] != null) // Only give reward when play record exists
    {
        let lastSeasonLeaderboardEntry = getLeaderboardAroundUserResult.Leaderboard[0];
        let lastSeasonScore = lastSeasonLeaderboardEntry.StatValue;
        let lastSeasonRewardGem = 0;

        if(lastSeasonScore != 0)
        {
            // Give reward based on last season's performance
            if(lastSeasonScore >= 2000) // Grandmaster
            {
                lastSeasonRewardGem = 5000;
            }
            else if(lastSeasonScore >= 1500) // Master
            {
                lastSeasonRewardGem = 3000;
            }
            else if(lastSeasonScore >= 1000) // Diamond
            {
                lastSeasonRewardGem = 2000;
            }
            else if(lastSeasonScore >= 750) // Platinum
            {
                lastSeasonRewardGem = 1500;
            }
            else if(lastSeasonScore >= 500) // Gold
            {
                lastSeasonRewardGem = 1000;
            }
            else if(lastSeasonScore >= 250) // Silver
            {
                lastSeasonRewardGem = 750;
            }
            else // Bronze
            {
                lastSeasonRewardGem = 500;
            }
            GiveEventGemReward({amount: lastSeasonRewardGem}, context);
            
            toReturn.gemsRewarded = lastSeasonRewardGem;
        }
    }

    toReturn.error = false;
    return toReturn;
}

handlers["giveLastSeasonReward"] = GiveLastSeasonReward;

interface IFixJapanese
{
    error: boolean;
    previousSeason: number;
    newSeason: number;
    previousSeasonWeekly: number;
    newSeasonWeekly: number;
}

let FixJapanese = function (args: any, context: IPlayFabContext): IFixJapanese 
{
    let toReturn : IFixJapanese = {error: false, previousSeason: 0, newSeason: 0, previousSeasonWeekly: 0, newSeasonWeekly: 0};

    // Recent played season
    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "arena_reward_last_played_season",
            "arena_reward_last_played_season_weekly"
        ]
    });

    let dataToUse = {};
    let needUpdate = false;
    if(getUserInternalDataResult.Data["arena_reward_last_played_season"] != null)
    {
        toReturn.previousSeason = parseInt(getUserInternalDataResult.Data["arena_reward_last_played_season"].Value);
        if(toReturn.previousSeason > 22)
        {
            toReturn.newSeason = 21;
            dataToUse["arena_reward_last_played_season"] = 21;
            needUpdate = true;
        }
    }

    if(getUserInternalDataResult.Data["arena_reward_last_played_season_weekly"] != null)
    {
        toReturn.previousSeasonWeekly = parseInt(getUserInternalDataResult.Data["arena_reward_last_played_season_weekly"].Value);
        if(toReturn.previousSeasonWeekly > 3)
        {
            toReturn.newSeasonWeekly = 3;
            dataToUse["arena_reward_last_played_season_weekly"] = 3;
            needUpdate = true;
        }
    }

    if(needUpdate)
    {
        let updateUserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: dataToUse
        });
    }

    toReturn.error = false;
    return toReturn;
}

handlers["fixJapanese"] = FixJapanese;

interface ILiappUserKey
{
    error: boolean;
    userKey: string;
}

let LiappUserKey = function (args: any, context: IPlayFabContext): ILiappUserKey 
{
    let toReturn : ILiappUserKey = {error: false, userKey: ""};
    toReturn.userKey = currentPlayerId + "_liapp";
    return toReturn;
};

handlers["liappUserKey"] = LiappUserKey;

interface ILiappAuth
{
    error: boolean;
    response: string;
}

let LiappAuth = function (args: any, context: IPlayFabContext): ILiappAuth 
{
    let toReturn : ILiappAuth = {error: false, response: "404"};
    
    let token = "";
    if(args && args.token)
        token = args.token;

    let url = "http://mobirix.lockincomp.com/auth/LiappClientAuthChecker.php";
    let content = "user_key=" + encodeURI(currentPlayerId + "_liapp") + "&token=" + encodeURI(token);
    let httpMethod = "post";
    let contentType = "application/x-www-form-urlencoded";

    toReturn.response = http.request(url, httpMethod, content, contentType);

    if(toReturn.response == "404" || toReturn.response == "300" || toReturn.response == "400")
    {
        server.UpdatePlayerStatistics({
            PlayFabId: currentPlayerId,
            Statistics: [
                {
                    "StatisticName": "liapp_auth_failed",
                    "Value": parseInt(toReturn.response)
                }
            ]
        });
    }
    
    return toReturn;
};

handlers["liappAuth"] = LiappAuth;

///////////////////////// HELPER FUNCTIONS (NOT DIRECTLY CALLABLE FROM THE CLIENT) /////////////////////////

function GetServerIdModifier(playerId: string): string
{
    let getPlayerTagsResult = server.GetPlayerTags({
        PlayFabId: playerId
    });

    let serverId = "";
    if(getPlayerTagsResult != null && getPlayerTagsResult.Tags.length > 0)
    {
        let serverTags = getPlayerTagsResult.Tags.filter(n => n.indexOf("Server") >= 0);
        if(serverTags != null && serverTags.length > 0)
            serverId = GetServerIdFromTag(serverTags[0]);
        else
            serverId = "";
    }
    else
        serverId = "";
    return (serverId == "")? "" : "_" + serverId;
}

function GetServerIdModifierFromServerId(serverId: string): string
{
    return (serverId == "")? "" : "_" + serverId;
}

function GetServerIdFromTag(serverId: string): string
{
    let convertedServerId = serverId.substring(serverId.length - 2);
        
    if(convertedServerId == "er")
        convertedServerId = "";

    return convertedServerId;
}

function ApplyAsyncedGems(gemsChange: number, unsyncedGemChangeVersion: number, getReadOnlyDataResult: PlayFabServerModels.GetUserDataResult): boolean
{
    if(gemsChange == 0)
        return false;

    let didCollide = false;

    if(getReadOnlyDataResult.Data["unsynced_gem_change_version"] != null 
        && parseInt(getReadOnlyDataResult.Data["unsynced_gem_change_version"].Value) != -1
        && parseInt(getReadOnlyDataResult.Data["unsynced_gem_change_version"].Value) == unsyncedGemChangeVersion)
    {
        // Duplication of vesion happens
        server.UpdatePlayerStatistics({
            PlayFabId: currentPlayerId,
            Statistics: [
                {
                    "StatisticName": "gem_changes_collision",
                    "Value": 1
                }
            ]
        });

        // Do not reduce gems for duplicate changes
        didCollide = true;
        return didCollide;
    }
    
    // Need to check gems being minus
    let getUserInventoryResult = server.GetUserInventory(
        {
            PlayFabId: currentPlayerId
        }
    );
    if(getUserInventoryResult.VirtualCurrency["GE"] + gemsChange < 0)
    {
        // Minus gems happens
        server.UpdatePlayerStatistics({
            PlayFabId: currentPlayerId,
            Statistics: [
                {
                    "StatisticName": "gem_changes_minus",
                    "Value": 1
                }
            ]
        });
        return didCollide;
    }

    if(gemsChange > 0)
    {
        let addUserVirtualCurrencyResult = server.AddUserVirtualCurrency({
            PlayFabId: currentPlayerId,
            VirtualCurrency: "GE",
            Amount: gemsChange
        });
    }
    else
    {
        let subtractUserVirtualCurrencyResult = server.SubtractUserVirtualCurrency({
            PlayFabId: currentPlayerId,
            VirtualCurrency: "GE",
            Amount: gemsChange * -1
        });
    }

    return didCollide;
}