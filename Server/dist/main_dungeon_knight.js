let AddUsedCouponTag = function (args, context) {
    let toReturn = { error: true };
    let couponId = "";
    if (args && args.couponId)
        couponId = args.couponId;
    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "received_only_once_rewards"
        ]
    }).Data["received_only_once_rewards"];
    let listOfReceivedItems = [];
    if (getUserInternalDataResult != null) {
        listOfReceivedItems = JSON.parse(getUserInternalDataResult.Value);
        if (listOfReceivedItems.some(n => n == couponId)) {
            let addPlayerTagResult = server.AddPlayerTag({
                PlayFabId: currentPlayerId,
                TagName: couponId
            });
            toReturn.error = false;
        }
    }
    return toReturn;
};
handlers["addUsedCouponTag"] = AddUsedCouponTag;
let RemoveUsedCouponTag = function (args, context) {
    let toReturn = { error: true };
    let couponId = "";
    if (args && args.couponId)
        couponId = args.couponId;
    let removePlayerTagResult = server.RemovePlayerTag({
        PlayFabId: currentPlayerId,
        TagName: couponId
    });
    toReturn.error = false;
    return toReturn;
};
handlers["removeUsedCouponTag"] = RemoveUsedCouponTag;
let ArenaRefresh = function (args, context) {
    let toReturn = { error: true, currentSeason: 0, dailyLimitLeft: 0, position: -1, score: 1500, lastSeasonPosition: -1, lastSeasonScore: 0,
        lastSeasonRewardGem: 0, rankerList: [], minAndroidVersion: "0.0.0", miniOSVersion: "0.0.0" };
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "setting_json"
        ]
    }).Data;
    if (getTitleDataResult["setting_json"] != null) {
        let convertedData = JSON.parse(getTitleDataResult["setting_json"]);
        toReturn.minAndroidVersion = convertedData.MinAndroidVersion;
        toReturn.miniOSVersion = convertedData.MiniOSVersion;
    }
    let serverIdModifier = GetServerIdModifier(currentPlayerId);
    let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
        PlayFabId: currentPlayerId,
        StatisticName: "arena" + serverIdModifier,
        MaxResultsCount: 1
    });
    let currentLeaderboardEntry = getLeaderboardAroundUserResult.Leaderboard[0];
    if (currentLeaderboardEntry != null) {
        toReturn.currentSeason = getLeaderboardAroundUserResult.Version;
        toReturn.position = currentLeaderboardEntry.Position;
        toReturn.score = currentLeaderboardEntry.StatValue;
        if (toReturn.score == 0)
            toReturn.score = 0;
    }
    let getLeaderboardResult = server.GetLeaderboard({
        StatisticName: "arena" + serverIdModifier,
        StartPosition: 0,
        MaxResultsCount: 25
    });
    for (let i = 0; i < getLeaderboardResult.Leaderboard.length; i++) {
        toReturn.rankerList.push({
            "name": getLeaderboardResult.Leaderboard[i].DisplayName,
            "score": getLeaderboardResult.Leaderboard[i].StatValue,
            "position": getLeaderboardResult.Leaderboard[i].Position
        });
    }
    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "arena_reward_last_played_season"
        ]
    });
    if (getUserInternalDataResult.Data["arena_reward_last_played_season"] != null) {
        let lastSeason = parseInt(getUserInternalDataResult.Data["arena_reward_last_played_season"].Value);
        if (getLeaderboardAroundUserResult.Version == lastSeason + 1) {
            let rewardResult = ReceiveArenaReward(serverIdModifier, getUserInternalDataResult, getLeaderboardAroundUserResult.Version);
            toReturn.lastSeasonPosition = rewardResult.lastSeasonPosition;
            toReturn.lastSeasonScore = rewardResult.lastSeasonScore;
            toReturn.lastSeasonRewardGem = rewardResult.lastSeasonRewardGem;
            log.info("Received reward gems: " + rewardResult.lastSeasonRewardGem);
        }
    }
    let dodgeResult = UpdateScoreIfDodged(serverIdModifier);
    if (dodgeResult.updated)
        toReturn.score = dodgeResult.myNewScore;
    toReturn.dailyLimitLeft = GetLeftDailyLimit("arena", "0");
    toReturn.error = false;
    return toReturn;
};
handlers["arenaRefresh"] = ArenaRefresh;
let ArenaRefresh2 = function (args, context) {
    let toReturn = { error: true,
        currentSeason: 0, dailyLimitLeft: 0, position: -1, score: 1500, lastSeasonPosition: -1, lastSeasonScore: 0, lastSeasonRewardGem: 0, rankerList: [],
        minAndroidVersion: "0.0.0", miniOSVersion: "0.0.0",
        currentSeasonWeekly: 0, star: -1, lastSeasonStar: -1, lastSeasonRewardGemWeekly: 0
    };
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "setting_json"
        ]
    }).Data;
    if (getTitleDataResult["setting_json"] != null) {
        let convertedData = JSON.parse(getTitleDataResult["setting_json"]);
        toReturn.minAndroidVersion = convertedData.MinAndroidVersion;
        toReturn.miniOSVersion = convertedData.MiniOSVersion;
    }
    let serverIdModifier = GetServerIdModifier(currentPlayerId);
    let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
        PlayFabId: currentPlayerId,
        StatisticName: "arena" + serverIdModifier,
        MaxResultsCount: 1
    });
    let currentLeaderboardEntry = getLeaderboardAroundUserResult.Leaderboard[0];
    if (currentLeaderboardEntry != null) {
        toReturn.currentSeason = getLeaderboardAroundUserResult.Version;
        toReturn.position = currentLeaderboardEntry.Position;
        toReturn.score = currentLeaderboardEntry.StatValue;
        if (toReturn.score == 0)
            toReturn.score = 0;
    }
    let getLeaderboardResult = server.GetLeaderboard({
        StatisticName: "arena" + serverIdModifier,
        StartPosition: 0,
        MaxResultsCount: 25
    });
    for (let i = 0; i < getLeaderboardResult.Leaderboard.length; i++) {
        toReturn.rankerList.push({
            "name": getLeaderboardResult.Leaderboard[i].DisplayName,
            "score": getLeaderboardResult.Leaderboard[i].StatValue,
            "position": getLeaderboardResult.Leaderboard[i].Position
        });
    }
    let getLeaderboardAroundUserWeeklyResult = server.GetLeaderboardAroundUser({
        PlayFabId: currentPlayerId,
        StatisticName: "star" + serverIdModifier,
        MaxResultsCount: 1
    });
    let currentLeaderboardEntryWeekly = getLeaderboardAroundUserWeeklyResult.Leaderboard[0];
    if (currentLeaderboardEntryWeekly != null) {
        toReturn.currentSeasonWeekly = getLeaderboardAroundUserWeeklyResult.Version;
        toReturn.star = currentLeaderboardEntryWeekly.StatValue;
        if (toReturn.star == 0)
            toReturn.star = 0;
    }
    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "arena_reward_last_played_season",
            "arena_reward_last_played_season_weekly"
        ]
    });
    if (getUserInternalDataResult.Data["arena_reward_last_played_season"] != null) {
        let lastSeason = parseInt(getUserInternalDataResult.Data["arena_reward_last_played_season"].Value);
        if (getLeaderboardAroundUserResult.Version == lastSeason + 1) {
            let rewardResult = ReceiveArenaReward(serverIdModifier, getUserInternalDataResult, getLeaderboardAroundUserResult.Version);
            toReturn.lastSeasonPosition = rewardResult.lastSeasonPosition;
            toReturn.lastSeasonScore = rewardResult.lastSeasonScore;
            toReturn.lastSeasonRewardGem = rewardResult.lastSeasonRewardGem;
            log.info("Received arena reward gems: " + rewardResult.lastSeasonRewardGem);
        }
    }
    if (getUserInternalDataResult.Data["arena_reward_last_played_season_weekly"] != null) {
        let lastSeasonWeekly = parseInt(getUserInternalDataResult.Data["arena_reward_last_played_season_weekly"].Value);
        if (getLeaderboardAroundUserWeeklyResult.Version == lastSeasonWeekly + 1) {
            let rewardResult = ReceiveArenaRewardWeekly(serverIdModifier, getUserInternalDataResult, getLeaderboardAroundUserWeeklyResult.Version);
            toReturn.lastSeasonStar = rewardResult.lastSeasonScore;
            toReturn.lastSeasonRewardGemWeekly = rewardResult.lastSeasonRewardGem;
            log.info("Received arena weekly reward gems: " + rewardResult.lastSeasonRewardGem);
        }
    }
    let dodgeResult = UpdateScoreIfDodged2(serverIdModifier);
    if (dodgeResult.updated) {
        toReturn.score = dodgeResult.myNewScore;
        toReturn.star = dodgeResult.myNewStar;
    }
    toReturn.dailyLimitLeft = GetLeftDailyLimit("arena", "0");
    toReturn.error = false;
    return toReturn;
};
handlers["arenaRefresh2"] = ArenaRefresh2;
let ArenaStart = function (args, context) {
    let opponentId = "";
    let toReturn = { error: true, opponentUserData: "", opponentInventoryData: "", opponentBonusStat: 0, reasonOfError: "" };
    if (args.opponentId)
        opponentId = args.opponentId;
    else
        return toReturn;
    if (GetLeftDailyLimit("arena", "0") < 1) {
        log.info("Exceeded daily limit for arena!");
        toReturn.reasonOfError = "exceeded_daily_limit";
        return toReturn;
    }
    let opponentReadOnlyData = server.GetUserReadOnlyData({
        PlayFabId: opponentId,
        Keys: [
            "inventory_data",
            "user_data"
        ]
    }).Data;
    toReturn.opponentUserData = opponentReadOnlyData["user_data"].Value;
    toReturn.opponentInventoryData = opponentReadOnlyData["inventory_data"].Value;
    let updateUserInternalDataResult = server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: {
            "is_playing_arena": "true",
            "arena_opponent_id": opponentId
        }
    });
    let getUserInventoryResult = server.GetUserInventory({
        PlayFabId: opponentId
    });
    toReturn.opponentBonusStat = getUserInventoryResult.VirtualCurrency["BS"].valueOf();
    toReturn.error = false;
    return toReturn;
};
handlers["arenaStart"] = ArenaStart;
let ArenaEnd = function (args, context) {
    let toReturn = { error: true, myNewScore: 0 };
    let didWin = false;
    if (args.didWin)
        didWin = args.didWin;
    let season = 0;
    if (args.season)
        season = args.season;
    let serverIdModifier = GetServerIdModifier(currentPlayerId);
    let isSuspiciousIfWin = false;
    if (args.isSuspiciousIfWin)
        isSuspiciousIfWin = args.isSuspiciousIfWin;
    if (isSuspiciousIfWin) {
        server.UpdatePlayerStatistics({
            PlayFabId: currentPlayerId,
            Statistics: [
                {
                    "StatisticName": "cheat_suspicious_win",
                    "Value": 1
                }
            ]
        });
    }
    let isValidCompletion = false;
    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "is_playing_arena",
            "arena_opponent_id"
        ]
    }).Data;
    if (getUserInternalDataResult["is_playing_arena"] && getUserInternalDataResult["is_playing_arena"].Value == "true")
        isValidCompletion = true;
    if (!isValidCompletion)
        return toReturn;
    if (!UseDailyLimitIfAvail("arena", "0", 1))
        return toReturn;
    let opponentId = "";
    if (getUserInternalDataResult["arena_opponent_id"])
        opponentId = getUserInternalDataResult["arena_opponent_id"].Value;
    toReturn.myNewScore = UpdateScore(serverIdModifier, opponentId, didWin).myNewScore;
    let updateUserInternalDataResult = server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: {
            "is_playing_arena": "false",
            "arena_reward_last_played_season": season.toString()
        }
    });
    toReturn.error = false;
    return toReturn;
};
handlers["arenaEnd"] = ArenaEnd;
let ArenaEnd2 = function (args, context) {
    let toReturn = { error: true, myNewScore: 0 };
    let didWin = false;
    if (args.didWin)
        didWin = args.didWin;
    let season = 0;
    if (args.season)
        season = args.season;
    let seasonWeekly = 0;
    if (args.seasonWeekly)
        seasonWeekly = args.seasonWeekly;
    let serverIdModifier = GetServerIdModifier(currentPlayerId);
    let isSuspiciousIfWin = false;
    if (args.isSuspiciousIfWin)
        isSuspiciousIfWin = args.isSuspiciousIfWin;
    if (isSuspiciousIfWin) {
        server.UpdatePlayerStatistics({
            PlayFabId: currentPlayerId,
            Statistics: [
                {
                    "StatisticName": "cheat_suspicious_win",
                    "Value": 1
                }
            ]
        });
    }
    let isValidCompletion = false;
    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "is_playing_arena",
            "arena_opponent_id",
            "arena_reward_last_played_season",
            "arena_reward_last_played_season_weekly"
        ]
    }).Data;
    if (getUserInternalDataResult["is_playing_arena"] && getUserInternalDataResult["is_playing_arena"].Value == "true")
        isValidCompletion = true;
    if (!isValidCompletion)
        return toReturn;
    if (!UseDailyLimitIfAvail("arena", "0", 1))
        return toReturn;
    let opponentId = "";
    if (getUserInternalDataResult["arena_opponent_id"])
        opponentId = getUserInternalDataResult["arena_opponent_id"].Value;
    toReturn.myNewScore = UpdateScore2(serverIdModifier, opponentId, didWin).myNewScore;
    if (getUserInternalDataResult["arena_reward_last_played_season"])
        season = Math.max(season, parseInt(getUserInternalDataResult["arena_reward_last_played_season"].Value));
    if (getUserInternalDataResult["arena_reward_last_played_season_weekly"])
        seasonWeekly = Math.max(seasonWeekly, parseInt(getUserInternalDataResult["arena_reward_last_played_season_weekly"].Value));
    let updateUserInternalDataResult = server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: {
            "is_playing_arena": "false",
            "arena_reward_last_played_season": season.toString(),
            "arena_reward_last_played_season_weekly": seasonWeekly.toString()
        }
    });
    toReturn.error = false;
    return toReturn;
};
handlers["arenaEnd2"] = ArenaEnd2;
function UpdateScoreIfDodged(serverIdModifier) {
    let toReturn = { updated: false, myNewScore: 0 };
    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "is_playing_arena",
            "arena_opponent_id"
        ]
    }).Data;
    if (getUserInternalDataResult["is_playing_arena"] != null && getUserInternalDataResult["is_playing_arena"].Value == "true")
        toReturn.updated = true;
    if (toReturn.updated) {
        UseDailyLimitIfAvail("arena", "0", 1);
        toReturn = UpdateScore(serverIdModifier, getUserInternalDataResult["arena_opponent_id"].Value, false);
        let updateUserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: {
                "is_playing_arena": "false"
            }
        });
    }
    return toReturn;
}
function UpdateScoreIfDodged2(serverIdModifier) {
    let toReturn = { updated: false, myNewScore: 0, myNewStar: 0 };
    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "is_playing_arena",
            "arena_opponent_id"
        ]
    }).Data;
    if (getUserInternalDataResult["is_playing_arena"] != null && getUserInternalDataResult["is_playing_arena"].Value == "true")
        toReturn.updated = true;
    if (toReturn.updated) {
        UseDailyLimitIfAvail("arena", "0", 1);
        toReturn = UpdateScore2(serverIdModifier, getUserInternalDataResult["arena_opponent_id"].Value, false);
        let updateUserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: {
                "is_playing_arena": "false"
            }
        });
    }
    return toReturn;
}
function UpdateScore(serverIdModifier, opponentId, didWin) {
    let toReturn = { updated: false, myNewScore: 0 };
    let myScore = GetScoreOfPlayer(serverIdModifier, currentPlayerId);
    let opponentScore = GetScoreOfPlayer(serverIdModifier, opponentId);
    let scoreBonusByStageRanking = 0;
    if (didWin) {
        let stageRanking = 0;
        let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
            PlayFabId: currentPlayerId,
            StatisticName: "ranking" + serverIdModifier,
            MaxResultsCount: 1
        });
        let currentLeaderboardEntry = getLeaderboardAroundUserResult.Leaderboard[0];
        if (currentLeaderboardEntry != null)
            stageRanking = currentLeaderboardEntry.Position + 1;
        if (stageRanking != 0) {
            if (stageRanking <= 10)
                scoreBonusByStageRanking = 7;
            else if (stageRanking <= 30)
                scoreBonusByStageRanking = 5;
            else if (stageRanking <= 100)
                scoreBonusByStageRanking = 3;
            else if (stageRanking <= 1000)
                scoreBonusByStageRanking = 1;
        }
    }
    if (didWin) {
        toReturn.myNewScore = myScore + 10 + scoreBonusByStageRanking;
        server.UpdatePlayerStatistics({
            PlayFabId: currentPlayerId,
            Statistics: [
                {
                    "StatisticName": "arena" + serverIdModifier,
                    "Value": 10 + scoreBonusByStageRanking
                },
                {
                    "StatisticName": "arena_all_season" + serverIdModifier,
                    "Value": 10 + scoreBonusByStageRanking
                }
            ]
        });
        if (opponentId != "" && opponentScore > 100) {
            server.UpdatePlayerStatistics({
                PlayFabId: opponentId,
                Statistics: [
                    {
                        "StatisticName": "arena" + serverIdModifier,
                        "Value": Math.max(100 - opponentScore, -3)
                    },
                    {
                        "StatisticName": "arena_all_season" + serverIdModifier,
                        "Value": Math.max(100 - opponentScore, -3)
                    }
                ]
            });
        }
    }
    else {
        toReturn.myNewScore = Math.max(myScore - 3, 1);
        if (myScore > 1) {
            server.UpdatePlayerStatistics({
                PlayFabId: currentPlayerId,
                Statistics: [
                    {
                        "StatisticName": "arena" + serverIdModifier,
                        "Value": Math.max(1 - myScore, -3)
                    },
                    {
                        "StatisticName": "arena_all_season" + serverIdModifier,
                        "Value": Math.max(1 - myScore, -3)
                    }
                ]
            });
        }
        if (opponentId != "") {
            server.UpdatePlayerStatistics({
                PlayFabId: opponentId,
                Statistics: [
                    {
                        "StatisticName": "arena" + serverIdModifier,
                        "Value": 10
                    },
                    {
                        "StatisticName": "arena_all_season" + serverIdModifier,
                        "Value": 10
                    }
                ]
            });
        }
    }
    toReturn.updated = true;
    return toReturn;
}
function UpdateScore2(serverIdModifier, opponentId, didWin) {
    let toReturn = { updated: false, myNewScore: 0, myNewStar: 0 };
    let myScore = GetScoreOfPlayer(serverIdModifier, currentPlayerId);
    let opponentScore = GetScoreOfPlayer(serverIdModifier, opponentId);
    let scoreBonusByStageRanking = 0;
    let myStar = GetStarOfPlayer(serverIdModifier, currentPlayerId);
    let starByStageRanking = 1;
    let stageRanking = 0;
    let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
        PlayFabId: currentPlayerId,
        StatisticName: "ranking" + serverIdModifier,
        MaxResultsCount: 1
    });
    let currentLeaderboardEntry = getLeaderboardAroundUserResult.Leaderboard[0];
    if (currentLeaderboardEntry != null)
        stageRanking = currentLeaderboardEntry.Position + 1;
    if (didWin) {
        if (stageRanking != 0) {
            if (stageRanking <= 10)
                scoreBonusByStageRanking = 7;
            else if (stageRanking <= 30)
                scoreBonusByStageRanking = 5;
            else if (stageRanking <= 100)
                scoreBonusByStageRanking = 3;
            else if (stageRanking <= 1000)
                scoreBonusByStageRanking = 1;
        }
        if (stageRanking != 0) {
            if (stageRanking <= 50)
                starByStageRanking = 11;
            else if (stageRanking <= 100)
                starByStageRanking = 9;
            else if (stageRanking <= 500)
                starByStageRanking = 7;
            else if (stageRanking <= 1000)
                starByStageRanking = 5;
            else if (stageRanking <= 5000)
                starByStageRanking = 3;
            else if (stageRanking <= 10000)
                starByStageRanking = 2;
        }
    }
    else {
        if (stageRanking != 0) {
            if (stageRanking <= 100)
                starByStageRanking = 5;
            else if (stageRanking <= 1000)
                starByStageRanking = 3;
            else if (stageRanking <= 10000)
                starByStageRanking = 1;
        }
    }
    if (didWin) {
        toReturn.myNewScore = myScore + 10 + scoreBonusByStageRanking;
        toReturn.myNewStar = myStar + starByStageRanking;
        server.UpdatePlayerStatistics({
            PlayFabId: currentPlayerId,
            Statistics: [
                {
                    "StatisticName": "arena" + serverIdModifier,
                    "Value": 10 + scoreBonusByStageRanking
                },
                {
                    "StatisticName": "arena_all_season" + serverIdModifier,
                    "Value": 10 + scoreBonusByStageRanking
                },
                {
                    "StatisticName": "star" + serverIdModifier,
                    "Value": starByStageRanking
                }
            ]
        });
        if (opponentId != "" && opponentScore > 100) {
            server.UpdatePlayerStatistics({
                PlayFabId: opponentId,
                Statistics: [
                    {
                        "StatisticName": "arena" + serverIdModifier,
                        "Value": Math.max(100 - opponentScore, -3)
                    },
                    {
                        "StatisticName": "arena_all_season" + serverIdModifier,
                        "Value": Math.max(100 - opponentScore, -3)
                    }
                ]
            });
        }
    }
    else {
        toReturn.myNewScore = Math.max(myScore - 3, 1);
        toReturn.myNewStar = myStar + starByStageRanking;
        if (myScore > 1) {
            server.UpdatePlayerStatistics({
                PlayFabId: currentPlayerId,
                Statistics: [
                    {
                        "StatisticName": "arena" + serverIdModifier,
                        "Value": Math.max(1 - myScore, -3)
                    },
                    {
                        "StatisticName": "arena_all_season" + serverIdModifier,
                        "Value": Math.max(1 - myScore, -3)
                    },
                    {
                        "StatisticName": "star" + serverIdModifier,
                        "Value": starByStageRanking
                    }
                ]
            });
        }
        else {
            server.UpdatePlayerStatistics({
                PlayFabId: currentPlayerId,
                Statistics: [
                    {
                        "StatisticName": "star" + serverIdModifier,
                        "Value": starByStageRanking
                    }
                ]
            });
        }
        if (opponentId != "") {
            server.UpdatePlayerStatistics({
                PlayFabId: opponentId,
                Statistics: [
                    {
                        "StatisticName": "arena" + serverIdModifier,
                        "Value": 10
                    },
                    {
                        "StatisticName": "arena_all_season" + serverIdModifier,
                        "Value": 10
                    }
                ]
            });
        }
    }
    toReturn.updated = true;
    return toReturn;
}
function GetScoreOfPlayer(serverIdModifier, playerId) {
    let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
        PlayFabId: playerId,
        StatisticName: "arena" + serverIdModifier,
        MaxResultsCount: 1
    });
    let currentLeaderboardEntry = getLeaderboardAroundUserResult.Leaderboard[0];
    if (currentLeaderboardEntry != null) {
        let currentScore = currentLeaderboardEntry.StatValue;
        if (currentScore == 0)
            currentScore = 0;
        return currentScore;
    }
    else {
        return 0;
    }
}
function GetStarOfPlayer(serverIdModifier, playerId) {
    let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
        PlayFabId: playerId,
        StatisticName: "star" + serverIdModifier,
        MaxResultsCount: 1
    });
    let currentLeaderboardEntry = getLeaderboardAroundUserResult.Leaderboard[0];
    if (currentLeaderboardEntry != null) {
        let currentStar = currentLeaderboardEntry.StatValue;
        if (currentStar == 0)
            currentStar = 0;
        return currentStar;
    }
    else {
        return 0;
    }
}
function ReceiveArenaReward(serverIdModifier, internalDataResult, currentSeason) {
    let toReturn = { lastSeasonPosition: 0, lastSeasonScore: 0, lastSeasonRewardGem: 0 };
    if (internalDataResult.Data["arena_reward_last_played_season"] != null) {
        let lastPlayedSeason = parseInt(internalDataResult.Data["arena_reward_last_played_season"].Value);
        if (currentSeason == lastPlayedSeason + 1) {
            let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
                PlayFabId: currentPlayerId,
                StatisticName: "arena" + serverIdModifier,
                MaxResultsCount: 1,
                Version: lastPlayedSeason
            });
            if (getLeaderboardAroundUserResult.Leaderboard[0] != null) {
                let lastSeasonLeaderboardEntry = getLeaderboardAroundUserResult.Leaderboard[0];
                toReturn.lastSeasonPosition = lastSeasonLeaderboardEntry.Position;
                toReturn.lastSeasonScore = lastSeasonLeaderboardEntry.StatValue;
            }
            if (toReturn.lastSeasonScore != 0) {
                if (toReturn.lastSeasonPosition == 0) {
                    toReturn.lastSeasonRewardGem = 2500;
                }
                else if (toReturn.lastSeasonPosition == 1) {
                    toReturn.lastSeasonRewardGem = 2000;
                }
                else if (toReturn.lastSeasonPosition == 2) {
                    toReturn.lastSeasonRewardGem = 1500;
                }
                else if (toReturn.lastSeasonPosition < 10) {
                    toReturn.lastSeasonRewardGem = 1000;
                }
                else if (toReturn.lastSeasonPosition < 50) {
                    toReturn.lastSeasonRewardGem = 750;
                }
                else if (toReturn.lastSeasonPosition < 100) {
                    toReturn.lastSeasonRewardGem = 250;
                }
                else {
                    toReturn.lastSeasonRewardGem = 50;
                }
                AddVirtualCurrency("GE", toReturn.lastSeasonRewardGem);
            }
            let updateUserInternalDataResult = server.UpdateUserInternalData({
                PlayFabId: currentPlayerId,
                Data: {
                    "arena_reward_last_played_season": currentSeason.toString()
                }
            });
        }
    }
    else {
        let updateUserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: {
                "arena_reward_last_played_season": currentSeason.toString()
            }
        });
    }
    return toReturn;
}
function ReceiveArenaRewardWeekly(serverIdModifier, internalDataResult, currentSeason) {
    let toReturn = { lastSeasonPosition: 0, lastSeasonScore: -1, lastSeasonRewardGem: 0 };
    if (internalDataResult.Data["arena_reward_last_played_season_weekly"] != null) {
        let lastPlayedSeason = parseInt(internalDataResult.Data["arena_reward_last_played_season_weekly"].Value);
        if (currentSeason == lastPlayedSeason + 1) {
            let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
                PlayFabId: currentPlayerId,
                StatisticName: "star" + serverIdModifier,
                MaxResultsCount: 1,
                Version: lastPlayedSeason
            });
            if (getLeaderboardAroundUserResult.Leaderboard[0] != null) {
                let lastSeasonLeaderboardEntry = getLeaderboardAroundUserResult.Leaderboard[0];
                toReturn.lastSeasonScore = lastSeasonLeaderboardEntry.StatValue;
            }
            if (toReturn.lastSeasonScore != -1) {
                if (toReturn.lastSeasonScore >= 2000) {
                    toReturn.lastSeasonRewardGem = 5000;
                }
                else if (toReturn.lastSeasonScore >= 1500) {
                    toReturn.lastSeasonRewardGem = 3000;
                }
                else if (toReturn.lastSeasonScore >= 1000) {
                    toReturn.lastSeasonRewardGem = 2000;
                }
                else if (toReturn.lastSeasonScore >= 750) {
                    toReturn.lastSeasonRewardGem = 1500;
                }
                else if (toReturn.lastSeasonScore >= 500) {
                    toReturn.lastSeasonRewardGem = 1000;
                }
                else if (toReturn.lastSeasonScore >= 250) {
                    toReturn.lastSeasonRewardGem = 750;
                }
                else {
                    toReturn.lastSeasonRewardGem = 500;
                }
                AddVirtualCurrency("GE", toReturn.lastSeasonRewardGem);
            }
            let updateUserInternalDataResult = server.UpdateUserInternalData({
                PlayFabId: currentPlayerId,
                Data: {
                    "arena_reward_last_played_season_weekly": currentSeason.toString()
                }
            });
        }
    }
    else {
        let updateUserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: {
                "arena_reward_last_played_season_weekly": currentSeason.toString()
            }
        });
    }
    return toReturn;
}
let LeftCooldown = function (args, context) {
    let name = "";
    let coolTime = 0;
    let toReturn = { error: true, leftSeconds: 0 };
    if (args && args.name)
        name = args.name;
    else
        return toReturn;
    toReturn.leftSeconds = GetCooldown(name);
    toReturn.error = false;
    return toReturn;
};
handlers["leftCooldown"] = LeftCooldown;
function GetCooldown(name) {
    let coolTime = 300;
    if (name == "tower_deploy")
        coolTime = 3600;
    else if (name == "tower_battle")
        coolTime = 60;
    let currentTimestampSecond = Math.floor(+new Date() / (1000));
    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "cooldown_timestamp_" + name
        ]
    }).Data["cooldown_timestamp_" + name];
    let lastTimestampSecond = 0;
    if (getUserInternalDataResult != null)
        lastTimestampSecond = parseInt(getUserInternalDataResult.Value);
    return Math.max(lastTimestampSecond + coolTime - currentTimestampSecond, 0);
}
function StartCooldown(name) {
    let timestampSecond = Math.floor(+new Date() / (1000));
    let dataToUse = {};
    dataToUse["cooldown_timestamp_" + name] = String(timestampSecond);
    let updateUserInternalDataResult = server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: dataToUse
    });
}
function ForceResetCooldown(name) {
    let dataToUse = {};
    dataToUse["cooldown_timestamp_" + name] = String(0);
    let updateUserInternalDataResult = server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: dataToUse
    });
}
let LeftDailyLimit = function (args, context) {
    let name = "";
    let id = "";
    let toReturn = { error: true, leftCount: 0 };
    if (args.name)
        name = args.name;
    else
        return toReturn;
    if (args.id)
        id = args.id;
    else
        return toReturn;
    toReturn.leftCount = GetLeftDailyLimit(name, id);
    toReturn.error = false;
    return toReturn;
};
handlers["leftDailyLimit"] = LeftDailyLimit;
let DailyAllowedCountAndUsedCounts = function (args, context) {
    let name = "";
    let toReturn = { error: true, result: {} };
    if (args.name)
        name = args.name;
    else
        return toReturn;
    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "daily_limit_use_" + name,
            "daily_limit_last_timestamp"
        ]
    });
    let hasReset = (ResetDailyLimitIfNeeded(getUserInternalDataResult));
    let allowedCount = 0;
    if (name == "arena")
        allowedCount = 30;
    else if (name == "raid")
        allowedCount = 3;
    else if (name == "free_gem")
        allowedCount = 30;
    else if (name == "tower")
        allowedCount = 3;
    else if (name == "raid_rollback")
        allowedCount = 2;
    else if (name == "guild_free_donate")
        allowedCount = 1;
    else if (name == "guild_donate")
        allowedCount = 1;
    else if (name == "guild_raid")
        allowedCount = 3;
    else if (name == "daily_package_1")
        allowedCount = 1;
    else if (name == "daily_package_3")
        allowedCount = 3;
    let dictionaryToUse = {};
    if (getUserInternalDataResult.Data["daily_limit_use_" + name] != null) {
        dictionaryToUse = JSON.parse(getUserInternalDataResult.Data["daily_limit_use_" + name].Value);
    }
    let resultItems = [];
    for (let tmp in dictionaryToUse) {
        let count = dictionaryToUse[tmp];
        if (hasReset)
            count = 0;
        resultItems.push({
            "ProgressId": tmp,
            "AllowedCount": allowedCount,
            "UsedCount": count
        });
    }
    toReturn.result = { "DailyLimitAndUseCountList": resultItems };
    toReturn.error = false;
    return toReturn;
};
handlers["dailyAllowedCountAndUsedCounts"] = DailyAllowedCountAndUsedCounts;
function GetLeftDailyLimit(name, id, allowMinus = false) {
    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "daily_limit_use_" + name,
            "daily_limit_last_timestamp"
        ]
    });
    let hasReset = ResetDailyLimitIfNeeded(getUserInternalDataResult);
    let allowedCount = 0;
    if (name == "arena")
        allowedCount = 30;
    else if (name == "raid")
        allowedCount = 3;
    else if (name == "free_gem")
        allowedCount = 30;
    else if (name == "tower")
        allowedCount = 3;
    else if (name == "raid_rollback")
        allowedCount = 2;
    else if (name == "guild_free_donate")
        allowedCount = 1;
    else if (name == "guild_donate")
        allowedCount = 1;
    else if (name == "guild_raid")
        allowedCount = 3;
    else if (name == "daily_package_1")
        allowedCount = 1;
    else if (name == "daily_package_3")
        allowedCount = 3;
    let usedCount = 0;
    let dictionaryToUse = {};
    if (getUserInternalDataResult.Data["daily_limit_use_" + name] != null) {
        dictionaryToUse = JSON.parse(getUserInternalDataResult.Data["daily_limit_use_" + name].Value);
        if (dictionaryToUse[id] != null)
            usedCount = parseInt(dictionaryToUse[id]);
        if (hasReset)
            usedCount = 0;
    }
    if (allowMinus)
        return allowedCount - usedCount;
    else
        return Math.max(allowedCount - usedCount, 0);
}
function UseDailyLimitIfAvail(name, id, count, allowMinus = false) {
    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "daily_limit_use_" + name,
            "daily_limit_last_timestamp"
        ]
    });
    let hasReset = ResetDailyLimitIfNeeded(getUserInternalDataResult);
    let allowedCount = 0;
    if (name == "arena")
        allowedCount = 30;
    else if (name == "raid")
        allowedCount = 3;
    else if (name == "free_gem")
        allowedCount = 30;
    else if (name == "tower")
        allowedCount = 3;
    else if (name == "raid_rollback")
        allowedCount = 2;
    else if (name == "guild_free_donate")
        allowedCount = 1;
    else if (name == "guild_donate")
        allowedCount = 1;
    else if (name == "guild_raid")
        allowedCount = 3;
    else if (name == "daily_package_1")
        allowedCount = 1;
    else if (name == "daily_package_3")
        allowedCount = 3;
    let usedCount = 0;
    let dictionaryToUse = {};
    if (getUserInternalDataResult.Data["daily_limit_use_" + name] != null) {
        dictionaryToUse = JSON.parse(getUserInternalDataResult.Data["daily_limit_use_" + name].Value);
        if (dictionaryToUse[id] != null)
            usedCount = parseInt(dictionaryToUse[id]);
        if (hasReset)
            usedCount = 0;
    }
    if (allowedCount >= usedCount + count || allowMinus) {
        if (dictionaryToUse[id] != null)
            dictionaryToUse[id] = dictionaryToUse[id] + count;
        else
            dictionaryToUse[id] = count;
        if (hasReset)
            dictionaryToUse[id] = count;
        let dataToUse = {};
        dataToUse["daily_limit_use_" + name] = JSON.stringify(dictionaryToUse);
        let updateUserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: dataToUse
        });
        return true;
    }
    else {
        return false;
    }
}
function ResetDailyLimitIfNeeded(internalDataResult) {
    let currentTimestampDay = Math.floor(+new Date() / (1000 * 60 * 60 * 24));
    let lastTimestampDay = 0;
    if (internalDataResult.Data["daily_limit_last_timestamp"] != null) {
        lastTimestampDay = parseInt(internalDataResult.Data["daily_limit_last_timestamp"].Value);
    }
    if (currentTimestampDay > lastTimestampDay) {
        log.info("Day has been changed. Need to reset daily limit!");
        let dataToUse = {};
        dataToUse["daily_limit_use_arena"] = "{}";
        dataToUse["daily_limit_use_raid"] = "{}";
        dataToUse["daily_limit_use_free_gem"] = "{}";
        dataToUse["daily_limit_use_tower"] = "{}";
        dataToUse["daily_limit_use_raid_rollback"] = "{}";
        dataToUse["daily_limit_use_guild_free_donate"] = "{}";
        dataToUse["daily_limit_use_guild_donate"] = "{}";
        dataToUse["daily_limit_use_guild_raid"] = "{}";
        let updateUserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: dataToUse
        });
        dataToUse = {};
        dataToUse["daily_limit_use_daily_package_1"] = "{}";
        dataToUse["daily_limit_use_daily_package_3"] = "{}";
        dataToUse["daily_limit_last_timestamp"] = currentTimestampDay.toString();
        updateUserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: dataToUse
        });
        return true;
    }
    else {
        return false;
    }
}
function ForceResetDailyLimit(name) {
    let dataToUse = {};
    dataToUse["daily_limit_use_" + name] = "{}";
    let updateUserInternalDataResult = server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: dataToUse
    });
}
let OneTimeReward = function (args, context) {
    let id = "";
    let toReturn = { rewardGem: 0, reasonOfError: "", error: true };
    if (args.id)
        id = args.id;
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "only_once_rewards"
        ]
    }).Data;
    if (getTitleDataResult != null) {
        let convertedData = JSON.parse(getTitleDataResult["only_once_rewards"]);
        let rewardInfo = convertedData[id];
        if (rewardInfo == null) {
            log.info(id + " is not avail!");
            toReturn.reasonOfError = "coupon_failed";
            return toReturn;
        }
        else {
            let getUserInternalDataResult = server.GetUserInternalData({
                PlayFabId: currentPlayerId,
                Keys: [
                    "received_only_once_rewards"
                ]
            }).Data["received_only_once_rewards"];
            let listOfReceivedItems = [];
            if (getUserInternalDataResult != null) {
                listOfReceivedItems = JSON.parse(getUserInternalDataResult.Value);
                if (listOfReceivedItems.some(n => n == id)) {
                    log.info("Already received the reward: " + id);
                    toReturn.reasonOfError = "coupon_already_use";
                    return toReturn;
                }
            }
            listOfReceivedItems.push(id);
            let updateUserInternalDataResult = server.UpdateUserInternalData({
                PlayFabId: currentPlayerId,
                Data: {
                    "received_only_once_rewards": JSON.stringify(listOfReceivedItems)
                }
            });
            if (rewardInfo.Id == "GE") {
                AddVirtualCurrency("GE", rewardInfo.Amount);
                toReturn.rewardGem = rewardInfo.Amount;
            }
            toReturn.error = false;
            return toReturn;
        }
    }
    else {
        log.info("only_once_rewards is not avail!");
        return toReturn;
    }
};
handlers["oneTimeReward"] = OneTimeReward;
let OneTimeReward2 = function (args, context) {
    let id = "";
    let toReturn = { rewardGem: 0, reasonOfError: "", error: true };
    if (args.id)
        id = args.id;
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "coupon_list"
        ]
    }).Data;
    if (getTitleDataResult != null) {
        let convertedData = JSON.parse(getTitleDataResult["coupon_list"]);
        let rewardInfo = convertedData[id];
        if (rewardInfo == null) {
            log.info(id + " is not avail!");
            toReturn.reasonOfError = "coupon_failed";
            return toReturn;
        }
        else {
            let getPlayerTagsResult = server.GetPlayerTags({
                PlayFabId: currentPlayerId
            });
            let serverId = "";
            if (getPlayerTagsResult != null && getPlayerTagsResult.Tags.length > 0) {
                let serverTags = getPlayerTagsResult.Tags.filter(n => n.indexOf("Server") >= 0);
                if (serverTags != null && serverTags.length > 0)
                    serverId = GetServerIdFromTag(serverTags[0]);
            }
            if (serverId != rewardInfo.ServerId) {
                log.info("server id is not validate!");
                return toReturn;
            }
            let currentTimestampSecond = Math.floor(+new Date() / (1000));
            if (currentTimestampSecond < rewardInfo.StartTimeStamp || currentTimestampSecond > rewardInfo.EndTimeStamp) {
                log.info("coupon's date is not validate!");
                return toReturn;
            }
            let getUserInternalDataResult = server.GetUserInternalData({
                PlayFabId: currentPlayerId,
                Keys: [
                    "received_only_once_rewards"
                ]
            }).Data["received_only_once_rewards"];
            let listOfReceivedItems = [];
            if (getUserInternalDataResult != null) {
                listOfReceivedItems = JSON.parse(getUserInternalDataResult.Value);
                if (listOfReceivedItems.some(n => n == id)) {
                    log.info("Already received the reward: " + id);
                    toReturn.reasonOfError = "coupon_already_use";
                    return toReturn;
                }
            }
            listOfReceivedItems.push(id);
            let updateUserInternalDataResult = server.UpdateUserInternalData({
                PlayFabId: currentPlayerId,
                Data: {
                    "received_only_once_rewards": JSON.stringify(listOfReceivedItems)
                }
            });
            if (rewardInfo.Id == "GE") {
                AddVirtualCurrency("GE", rewardInfo.Amount);
                toReturn.rewardGem = rewardInfo.Amount;
            }
            toReturn.error = false;
            return toReturn;
        }
    }
    else {
        log.info("only_once_rewards is not avail!");
        return toReturn;
    }
};
handlers["oneTimeReward2"] = OneTimeReward2;
let GiveEventGemReward = function (args, context) {
    let toReturn = { rewardGem: 0, error: true };
    let amount = 30;
    let addedTime = 259200;
    if (args.amount)
        amount = parseInt(args.amount);
    if (args.addedTime)
        addedTime = parseInt(args.addedTime);
    let attachedItemTemplate = { ItemId: "gem", ItemCode: "GE", Count: amount };
    let mailArgs = { message: "message_mail_event_reward_gem", attachedItem: attachedItemTemplate, addedTime: addedTime };
    toReturn.error = AddOnTimeEventMail(mailArgs, context).error;
    toReturn.rewardGem = amount;
    return toReturn;
};
handlers["giveEventGemReward"] = GiveEventGemReward;
let ProcessGemCashback = function (args, context) {
    let toReturn = { error: true };
    let getUserInventoryResult = server.GetUserInventory({
        PlayFabId: currentPlayerId
    });
    if (getUserInventoryResult.VirtualCurrency["PB"] <= 0)
        return toReturn;
    else {
        let gemAmount = (getUserInventoryResult.VirtualCurrency["PB"] / 2);
        let attachedItemTemplate = { ItemId: "gem", ItemCode: "GE", Count: gemAmount };
        let args = { message: "message_mail_event_reward_gem", attachedItem: attachedItemTemplate };
        toReturn.error = AddMail(args, context).error;
        if (!toReturn.error)
            SubtractVirtualCurrency("PB", getUserInventoryResult.VirtualCurrency["PB"]);
        return toReturn;
    }
};
handlers["processGemCashback"] = ProcessGemCashback;
let RequestGemBonus = function (args, context) {
    let toReturn = { updatedPurchasedGemBundleList: [], bonusGemAmount: 0, error: true };
    let purchasedGemBundleId = "";
    if (args.purchasedGemBundleId)
        purchasedGemBundleId = args.purchasedGemBundleId;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: [
            "purchased_gem_bunlde_list"
        ]
    });
    let getUserInventoryResult = server.GetUserInventory({
        PlayFabId: currentPlayerId
    });
    if (getUserInventoryResult.Inventory.filter(n => n.ItemId == purchasedGemBundleId)[0] == null)
        return toReturn;
    if (getUserReadOnlyDataResult.Data["purchased_gem_bunlde_list"] != null) {
        toReturn.updatedPurchasedGemBundleList = JSON.parse(getUserReadOnlyDataResult.Data["purchased_gem_bunlde_list"].Value);
        if (toReturn.updatedPurchasedGemBundleList.filter(n => n == purchasedGemBundleId)[0] != null) {
            log.info("User already received gem bonus!");
            return toReturn;
        }
    }
    let getCatalogResult = server.GetCatalogItems({});
    let targetGemBundle = getCatalogResult.Catalog.filter(n => n.ItemId == purchasedGemBundleId)[0];
    if (targetGemBundle != null) {
        let gemBonusAmount = targetGemBundle.Bundle.BundledVirtualCurrencies["GE"];
        AddVirtualCurrency("GE", gemBonusAmount);
        toReturn.updatedPurchasedGemBundleList.push(purchasedGemBundleId);
        let updateUserDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "purchased_gem_bunlde_list": JSON.stringify(toReturn.updatedPurchasedGemBundleList)
            }
        });
        toReturn.bonusGemAmount = gemBonusAmount;
        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;
};
handlers["requestGemBonus"] = RequestGemBonus;
let RequestSupplyBox = function (args, context) {
    let toReturn = { error: true, minPassed: 0 };
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["last_supply_timestamp_min"]
    });
    let lastTimestampMin = -1;
    if (getUserReadOnlyDataResult.Data["last_supply_timestamp_min"] != null)
        lastTimestampMin = parseInt(getUserReadOnlyDataResult.Data["last_supply_timestamp_min"].Value);
    let currentTimestampMin = Math.floor(+new Date() / (1000 * 60));
    if (lastTimestampMin != -1)
        toReturn.minPassed = Math.min(currentTimestampMin - lastTimestampMin, 1440);
    else
        toReturn.minPassed = 0;
    if (currentTimestampMin != lastTimestampMin) {
        let updateuserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "last_supply_timestamp_min": currentTimestampMin.toString()
            }
        });
    }
    toReturn.error = false;
    return toReturn;
};
handlers["requestSupplyBox"] = RequestSupplyBox;
let RequestFreeGemDailyLeftLimit = function (args, context) {
    let toReturn = { leftLimit: 0, error: true };
    toReturn.leftLimit = GetLeftDailyLimit("free_gem", "0");
    toReturn.error = false;
    return toReturn;
};
handlers["requestFreeGemDailyLeftLimit"] = RequestFreeGemDailyLeftLimit;
let GetFreeGemAds = function (args, context) {
    let toReturn = { gemCount: 0, nextFreeGemAdsTimeStamp: 0, leftLimit: 0, error: true };
    let setting = null;
    let nextTimestamp = 0;
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "setting_json"
        ]
    }).Data;
    setting = JSON.parse(getTitleDataResult["setting_json"]);
    if (setting == null)
        return toReturn;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: [
            "nextFreeGemAdsTimeStamp"
        ]
    });
    if (getUserReadOnlyDataResult.Data["nextFreeGemAdsTimeStamp"] != null)
        nextTimestamp = parseInt(getUserReadOnlyDataResult.Data["nextFreeGemAdsTimeStamp"].Value);
    let currentTimestamp = Math.floor(+new Date() / 1000);
    if (currentTimestamp >= nextTimestamp) {
        if (!UseDailyLimitIfAvail("free_gem", "0", 1))
            return toReturn;
        toReturn.leftLimit = GetLeftDailyLimit("free_gem", "0");
        let gemReward = Math.min(Math.floor(Math.random() * (setting.FreeGemMaxAmount + 1 - setting.FreeGemMinAmount) + setting.FreeGemMinAmount), setting.FreeGemMaxAmount);
        AddVirtualCurrency("GE", gemReward);
        toReturn.gemCount = gemReward;
        toReturn.nextFreeGemAdsTimeStamp = currentTimestamp + setting.FreeGemCoolTime;
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "nextFreeGemAdsTimeStamp": toReturn.nextFreeGemAdsTimeStamp.toString()
            }
        });
        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;
};
handlers["getFreeGemAds"] = GetFreeGemAds;
let RequestGrowthSupportFree = function (args, context) {
    let toReturn = { rewardGemAmount: 0, updatedReceivedRewardList: [], error: true };
    let growthSupportRewardId = "";
    let rewardRequireStageProgess = 0;
    if (args.growthSupportRewardId)
        growthSupportRewardId = args.growthSupportRewardId;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: [
            "growth_support_free_received_reward_list",
            "user_data"
        ]
    });
    if (getUserReadOnlyDataResult.Data["user_data"] != null) {
        let userData = JSON.parse(getUserReadOnlyDataResult.Data["user_data"].Value);
        rewardRequireStageProgess = parseInt(growthSupportRewardId.substring(growthSupportRewardId.length - 2, growthSupportRewardId.length));
        log.info("user last clear world level: " + userData.StageProgress + "// require world level: " + rewardRequireStageProgess);
        if (rewardRequireStageProgess == 0 || userData.StageProgress < rewardRequireStageProgess)
            return toReturn;
    }
    if (getUserReadOnlyDataResult.Data["growth_support_free_received_reward_list"] != null) {
        toReturn.updatedReceivedRewardList = JSON.parse(getUserReadOnlyDataResult.Data["growth_support_free_received_reward_list"].Value);
        if (toReturn.updatedReceivedRewardList.filter(n => n == growthSupportRewardId)[0] != null) {
            log.info("User already received this reward !");
            return toReturn;
        }
        if (toReturn.updatedReceivedRewardList.length > 8) {
            log.info("User already received all reward !");
            return toReturn;
        }
    }
    let getTitleDataResult = server.GetTitleData({
        Keys: ["growth_support_free_reward_table"]
    }).Data;
    if (getTitleDataResult["growth_support_free_reward_table"] == null)
        return toReturn;
    let growthSupportFreeRewardData = JSON.parse(getTitleDataResult["growth_support_free_reward_table"]);
    if (growthSupportFreeRewardData == null)
        return toReturn;
    let convertedIndex = rewardRequireStageProgess / 10 - 1;
    let targetReward = growthSupportFreeRewardData.GrowthSupportFreeRewardList[convertedIndex];
    if (targetReward == null)
        return toReturn;
    AddVirtualCurrency(targetReward.ItemCode, Number(targetReward.ItemCount));
    toReturn.rewardGemAmount = Number(targetReward.ItemCount);
    toReturn.updatedReceivedRewardList.push(growthSupportRewardId);
    let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Data: {
            "growth_support_free_received_reward_list": unescape(JSON.stringify(toReturn.updatedReceivedRewardList)),
        }
    });
    toReturn.error = false;
    return toReturn;
};
handlers["requestGrowthSupportFree"] = RequestGrowthSupportFree;
let RequestGrowthSupportFirst = function (args, context) {
    let toReturn = { rewardGemAmount: 0, updatedReceivedRewardList: [], error: true };
    let growthSupportRewardId = "";
    if (args.growthSupportRewardId)
        growthSupportRewardId = args.growthSupportRewardId;
    let getUserInventoryResult = server.GetUserInventory({
        PlayFabId: currentPlayerId
    });
    if (getUserInventoryResult.Inventory.filter(n => n.ItemId == "bundle_package_growth_1")[0] != null) {
        let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Keys: [
                "growth_support_1_received_reward_list",
                "user_data"
            ]
        });
        if (getUserReadOnlyDataResult.Data["user_data"] != null) {
            let userData = JSON.parse(getUserReadOnlyDataResult.Data["user_data"].Value);
            let rewardRequireStageProgess = parseInt(growthSupportRewardId.substring(growthSupportRewardId.length - 2, growthSupportRewardId.length));
            log.info("user last clear world level: " + userData.StageProgress + "// require world level: " + rewardRequireStageProgess);
            if (userData.StageProgress < rewardRequireStageProgess)
                return toReturn;
        }
        if (getUserReadOnlyDataResult.Data["growth_support_1_received_reward_list"] != null) {
            toReturn.updatedReceivedRewardList = JSON.parse(getUserReadOnlyDataResult.Data["growth_support_1_received_reward_list"].Value);
            if (toReturn.updatedReceivedRewardList.filter(n => n == growthSupportRewardId)[0] != null) {
                log.info("User already received this reward !");
                return toReturn;
            }
            if (toReturn.updatedReceivedRewardList.length > 8) {
                log.info("User already received all reward !");
                return toReturn;
            }
        }
        AddVirtualCurrency("GE", 4000);
        toReturn.rewardGemAmount = 4000;
        toReturn.updatedReceivedRewardList.push(growthSupportRewardId);
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "growth_support_1_received_reward_list": unescape(JSON.stringify(toReturn.updatedReceivedRewardList)),
            }
        });
        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;
};
handlers["requestGrowthSupportFirst"] = RequestGrowthSupportFirst;
let RequestGrowthSupportSecond = function (args, context) {
    let toReturn = { rewardGemAmount: 0, updatedReceivedRewardList: [], error: true };
    let growthSupportRewardId = "";
    if (args.growthSupportRewardId)
        growthSupportRewardId = args.growthSupportRewardId;
    let getUserInventoryResult = server.GetUserInventory({
        PlayFabId: currentPlayerId
    });
    if (getUserInventoryResult.Inventory.filter(n => n.ItemId == "bundle_package_growth_2")[0] != null) {
        let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Keys: [
                "growth_support_2_received_reward_list",
                "user_data"
            ]
        });
        if (getUserReadOnlyDataResult.Data["user_data"] != null) {
            let userData = JSON.parse(getUserReadOnlyDataResult.Data["user_data"].Value);
            let rewardRequireStageProgess = parseInt(growthSupportRewardId.substring(growthSupportRewardId.length - 2, growthSupportRewardId.length));
            log.info("user last clear world level: " + userData.StageProgress + "// require world level: " + rewardRequireStageProgess);
            if (userData.StageProgress < rewardRequireStageProgess)
                return toReturn;
        }
        if (getUserReadOnlyDataResult.Data["growth_support_2_received_reward_list"] != null) {
            toReturn.updatedReceivedRewardList = JSON.parse(getUserReadOnlyDataResult.Data["growth_support_2_received_reward_list"].Value);
            if (toReturn.updatedReceivedRewardList.filter(n => n == growthSupportRewardId)[0] != null) {
                log.info("User already received this reward !");
                return toReturn;
            }
            if (toReturn.updatedReceivedRewardList.length > 8) {
                log.info("User already received all reward !");
                return toReturn;
            }
        }
        AddVirtualCurrency("GE", 8000);
        toReturn.rewardGemAmount = 8000;
        toReturn.updatedReceivedRewardList.push(growthSupportRewardId);
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "growth_support_2_received_reward_list": unescape(JSON.stringify(toReturn.updatedReceivedRewardList)),
            }
        });
        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;
};
handlers["requestGrowthSupportSecond"] = RequestGrowthSupportSecond;
let RequestVIPReward = function (args, context) {
    let toReturn = { usedVIPPoint: 0, rewardGemAmount: 0, error: true };
    let getTitleDataResult = server.GetTitleData({
        Keys: ["setting_json"]
    }).Data;
    if (getTitleDataResult["setting_json"] != null) {
        let setting = JSON.parse(getTitleDataResult["setting_json"]);
        let currentVITicket = 0;
        let getUserInventoryResult = server.GetUserInventory({
            PlayFabId: currentPlayerId
        });
        currentVITicket = getUserInventoryResult.VirtualCurrency["VP"];
        if (currentVITicket < setting.RequireVIPTicket)
            return toReturn;
        else {
            let totalExchangeCount = Math.floor(currentVITicket / setting.RequireVIPTicket);
            let totalGemReward = totalExchangeCount * setting.VIPRewardGem;
            AddVirtualCurrency("GE", totalGemReward);
            SubtractVirtualCurrency("VP", totalExchangeCount * setting.RequireVIPTicket);
            toReturn.usedVIPPoint = totalExchangeCount * setting.RequireVIPTicket;
            toReturn.rewardGemAmount = totalGemReward;
            toReturn.error = false;
            return toReturn;
        }
    }
    else
        return toReturn;
};
handlers["requestVIPReward"] = RequestVIPReward;
let RequestDailyReward = function (args, context) {
    let toReturn = { itemId: "", count: 0, error: true };
    let currentDailyRewardCount = -1;
    let dailyRewardTable = null;
    let currentInventory = server.GetUserInventory({
        PlayFabId: currentPlayerId
    });
    if (currentInventory.VirtualCurrency["DR"] < 1)
        return toReturn;
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "daily_reward_table"
        ]
    }).Data;
    dailyRewardTable = JSON.parse(getTitleDataResult["daily_reward_table"]);
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["daily_reward_count"]
    });
    if (getUserReadOnlyDataResult.Data["daily_reward_count"] != null)
        currentDailyRewardCount = parseInt(getUserReadOnlyDataResult.Data["daily_reward_count"].Value);
    if (dailyRewardTable != null) {
        currentDailyRewardCount = currentDailyRewardCount + 1;
        if (currentDailyRewardCount >= 25) {
            log.info("Daily reward can't be over 25 days");
            return toReturn;
        }
        let targetDailyReward = dailyRewardTable.DailyRewardList[currentDailyRewardCount];
        if (targetDailyReward != null) {
            if (targetDailyReward.ItemId == "gem")
                AddVirtualCurrency(targetDailyReward.ItemCode, Number(targetDailyReward.ItemCount));
            toReturn.itemId = targetDailyReward.ItemId;
            toReturn.count = targetDailyReward.ItemCount;
            toReturn.error = false;
            SubtractVirtualCurrency("DR", 1);
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "daily_reward_count": currentDailyRewardCount.toString()
                }
            });
            return toReturn;
        }
        else {
            log.info("targetDailyReward is null!");
            return toReturn;
        }
    }
    else {
        log.info("dailyRewardTable is null!");
        return toReturn;
    }
};
handlers["requestDailyReward"] = RequestDailyReward;
let RequestDailyRewardInfo = function (args, context) {
    let toReturn = { userDailyRewardCount: -1, dailyRewardTable: null, dailyRewardTicket: 0, dailyRewardTicketRechargeTimes: 0, error: true };
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["daily_reward_count"]
    });
    if (getUserReadOnlyDataResult.Data["daily_reward_count"] != null)
        toReturn.userDailyRewardCount = parseInt(getUserReadOnlyDataResult.Data["daily_reward_count"].Value);
    if (toReturn.userDailyRewardCount >= 24) {
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "daily_reward_count": "-1"
            }
        });
        toReturn.userDailyRewardCount = -1;
    }
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "daily_reward_table"
        ]
    }).Data;
    toReturn.dailyRewardTable = JSON.parse(getTitleDataResult["daily_reward_table"]);
    if (toReturn.dailyRewardTable != null)
        toReturn.error = false;
    let getUserInventoryResult = server.GetUserInventory({
        PlayFabId: currentPlayerId
    });
    if (getUserInventoryResult.VirtualCurrency["DR"] != null) {
        toReturn.dailyRewardTicket = getUserInventoryResult.VirtualCurrency["DR"].valueOf();
        let serverTimeStamp = Math.floor(+new Date() / 1000);
        toReturn.dailyRewardTicketRechargeTimes = Math.max(0, (Math.round(new Date(getUserInventoryResult.VirtualCurrencyRechargeTimes["DR"].RechargeTime).getTime() / 1000) - serverTimeStamp));
    }
    return toReturn;
};
handlers["requestDailyRewardInfo"] = RequestDailyRewardInfo;
let GiveGemPackageReward = function (args, context) {
    let toReturn = { rewardGemAmount: 0, error: true };
    let rewardedGemBundleList = [];
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: [
            "rewarded_gem_bunlde_list"
        ]
    });
    if (getUserReadOnlyDataResult.Data["rewarded_gem_bunlde_list"] != null)
        rewardedGemBundleList = JSON.parse(getUserReadOnlyDataResult.Data["rewarded_gem_bunlde_list"].Value);
    else {
        rewardedGemBundleList.push({
            "BundleId": "bundle_gem_01",
            "Count": 0
        }, {
            "BundleId": "bundle_gem_02",
            "Count": 0
        }, {
            "BundleId": "bundle_gem_03",
            "Count": 0
        }, {
            "BundleId": "bundle_gem_04",
            "Count": 0
        }, {
            "BundleId": "bundle_gem_05",
            "Count": 0
        });
    }
    if (rewardedGemBundleList != null) {
        let totalGemReward = 0;
        let getUserInventoryResult = server.GetUserInventory({
            PlayFabId: currentPlayerId
        });
        getUserInventoryResult.Inventory.forEach(tmp => {
            if (tmp.ItemId == "bundle_gem_01") {
                let targetItem = rewardedGemBundleList.filter(n => n.BundleId == tmp.ItemId)[0];
                if (targetItem != null) {
                    let rewardBundleGem01Count = tmp.RemainingUses - targetItem.Count;
                    let totalrewardBundleGem01 = 0;
                    if (targetItem.Count == 0)
                        totalrewardBundleGem01 = (rewardBundleGem01Count + 1) * 10;
                    else
                        totalrewardBundleGem01 = rewardBundleGem01Count * 10;
                    totalGemReward = totalGemReward + totalrewardBundleGem01;
                    targetItem.Count = tmp.RemainingUses;
                    log.info("gem_01_count: " + rewardBundleGem01Count + "// total_gem_01_reward: " + totalrewardBundleGem01);
                }
            }
            else if (tmp.ItemId == "bundle_gem_02") {
                let targetItem = rewardedGemBundleList.filter(n => n.BundleId == tmp.ItemId)[0];
                if (targetItem != null) {
                    let rewardBundleGem02Count = tmp.RemainingUses - targetItem.Count;
                    let totalrewardBundleGem02 = 0;
                    if (targetItem.Count == 0)
                        totalrewardBundleGem02 = (rewardBundleGem02Count + 1) * 50;
                    else
                        totalrewardBundleGem02 = rewardBundleGem02Count * 50;
                    totalGemReward = totalGemReward + totalrewardBundleGem02;
                    targetItem.Count = tmp.RemainingUses;
                    log.info("gem_02_count: " + rewardBundleGem02Count + "// total_gem_02_reward: " + totalrewardBundleGem02);
                }
            }
            else if (tmp.ItemId == "bundle_gem_03") {
                let targetItem = rewardedGemBundleList.filter(n => n.BundleId == tmp.ItemId)[0];
                if (targetItem != null) {
                    let rewardBundleGem03Count = tmp.RemainingUses - targetItem.Count;
                    let totalrewardBundleGem03 = 0;
                    if (targetItem.Count == 0)
                        totalrewardBundleGem03 = (rewardBundleGem03Count + 1) * 100;
                    else
                        totalrewardBundleGem03 = rewardBundleGem03Count * 100;
                    totalGemReward = totalGemReward + totalrewardBundleGem03;
                    targetItem.Count = tmp.RemainingUses;
                    log.info("gem_03_count: " + rewardBundleGem03Count + "// total_gem_03_reward: " + totalrewardBundleGem03);
                }
            }
            else if (tmp.ItemId == "bundle_gem_04") {
                let targetItem = rewardedGemBundleList.filter(n => n.BundleId == tmp.ItemId)[0];
                if (targetItem != null) {
                    let rewardBundleGem04Count = tmp.RemainingUses - targetItem.Count;
                    let totalrewardBundleGem04 = 0;
                    if (targetItem.Count == 0)
                        totalrewardBundleGem04 = (rewardBundleGem04Count + 1) * 3000;
                    else
                        totalrewardBundleGem04 = rewardBundleGem04Count * 3000;
                    totalGemReward = totalGemReward + totalrewardBundleGem04;
                    targetItem.Count = tmp.RemainingUses;
                    log.info("gem_04_count: " + rewardBundleGem04Count + "// total_gem_04_reward: " + totalrewardBundleGem04);
                }
            }
            else if (tmp.ItemId == "bundle_gem_05") {
                let targetItem = rewardedGemBundleList.filter(n => n.BundleId == tmp.ItemId)[0];
                if (targetItem != null) {
                    let rewardBundleGem05Count = tmp.RemainingUses - targetItem.Count;
                    let totalrewardBundleGem05 = 0;
                    if (targetItem.Count == 0)
                        totalrewardBundleGem05 = (rewardBundleGem05Count + 1) * 15000;
                    else
                        totalrewardBundleGem05 = rewardBundleGem05Count * 15000;
                    totalGemReward = totalGemReward + totalrewardBundleGem05;
                    targetItem.Count = tmp.RemainingUses;
                    log.info("gem_05_count: " + rewardBundleGem05Count + "// total_gem_05_reward: " + totalrewardBundleGem05);
                }
            }
        });
        if (totalGemReward > 0) {
            log.info("total gem sent: " + totalGemReward);
            let attachedItemTemplate = { ItemId: "gem", ItemCode: "GE", Count: totalGemReward };
            let args = { message: "message_mail_event_reward_gem", attachedItem: attachedItemTemplate };
            toReturn.error = AddMail(args, context).error;
            if (!toReturn.error) {
                let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                    PlayFabId: currentPlayerId,
                    Data: {
                        "rewarded_gem_bunlde_list": JSON.stringify(rewardedGemBundleList)
                    }
                });
            }
            else {
                log.info("Error on add mail !");
                return toReturn;
            }
        }
        else {
            log.info("total gem reward is not over then 0 !");
            return toReturn;
        }
    }
    else
        return toReturn;
};
handlers["giveGemPackageReward"] = GiveGemPackageReward;
let ResetPurchasedGemBundleList = function (args, context) {
    let toReturn = { error: true };
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: [
            "purchased_gem_bunlde_list"
        ]
    });
    if (getUserReadOnlyDataResult.Data["purchased_gem_bunlde_list"] != null) {
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "purchased_gem_bunlde_list": null
            }
        });
        toReturn.error = false;
    }
    return toReturn;
};
handlers["resetPurchasedGemBundleList"] = ResetPurchasedGemBundleList;
let CheckLimitedPackageAvailable = function (args, context) {
    let toReturn = { expiration: 0, error: true };
    let targetPackageId = "";
    if (args.targetPackageId)
        targetPackageId = args.targetPackageId;
    let getUserInventoryResult = server.GetUserInventory({
        PlayFabId: currentPlayerId
    });
    let targetPackageData = getUserInventoryResult.Inventory.find(n => n.ItemId == targetPackageId);
    if (targetPackageData == null)
        toReturn.error = false;
    else {
        if (targetPackageData.Expiration == null)
            toReturn.error = false;
        else {
            let serverTimeStamp = Math.floor(+new Date() / 1000);
            toReturn.expiration = Math.round(new Date(targetPackageData.Expiration).getTime() / 1000);
            if (serverTimeStamp >= toReturn.expiration)
                toReturn.error = false;
        }
    }
    return toReturn;
};
handlers["checkLimitedPackageAvailable"] = CheckLimitedPackageAvailable;
let CheckDailyWeeklyPackageAvailable = function (args, context) {
    let toReturn = { dailyPackage1Avail: [], dailyPackage3Avail: [], weeklyPackage1Avail: [], weeklyPackage3Avail: [], error: true };
    toReturn.dailyPackage1Avail.push(GetLeftDailyLimit("daily_package_1", "bundle_package_daily", false));
    toReturn.dailyPackage1Avail.push(GetLeftDailyLimit("daily_package_1", "bundle_package_daily_2", false));
    toReturn.weeklyPackage1Avail.push(GetLeftWeeklyLimit("weekly_package_1", "bundle_package_weekly_2", false));
    toReturn.weeklyPackage3Avail.push(GetLeftWeeklyLimit("weekly_package_3", "bundle_package_weekly", false));
    toReturn.error = false;
    return toReturn;
};
handlers["checkDailyWeeklyPackageAvailable"] = CheckDailyWeeklyPackageAvailable;
let CheckDailyWeeklyPackageAvailableV2 = function (args, context) {
    let toReturn = { dailyPackage1Avail: [], dailyPackage3Avail: [], weeklyPackage1Avail: [], weeklyPackage3Avail: [], error: true };
    toReturn.dailyPackage3Avail.push(GetLeftDailyLimit("daily_package_3", "bundle_package_daily", false));
    toReturn.dailyPackage3Avail.push(GetLeftDailyLimit("daily_package_3", "bundle_package_daily_2", false));
    toReturn.weeklyPackage3Avail.push(GetLeftWeeklyLimit("weekly_package_3", "bundle_package_weekly", false));
    toReturn.weeklyPackage3Avail.push(GetLeftWeeklyLimit("weekly_package_3", "bundle_package_weekly_2", false));
    toReturn.weeklyPackage1Avail.push(GetLeftWeeklyLimit("weekly_package_1", "bundle_package_weekly_3", false));
    toReturn.weeklyPackage3Avail.push(0);
    toReturn.error = false;
    return toReturn;
};
handlers["checkDailyWeeklyPackageAvailableV2"] = CheckDailyWeeklyPackageAvailableV2;
let UsedDailyWeeklyPackageIfAvail = function (args, context) {
    let toReturn = { error: true };
    let targetPackageType = "";
    if (args.targetPackageType)
        targetPackageType = args.targetPackageType;
    let targetPackageId = "0";
    if (args.targetPackageId)
        targetPackageId = args.targetPackageId;
    let askForUsing = false;
    if (args.askForUsing)
        askForUsing = args.askForUsing;
    if (askForUsing) {
        switch (targetPackageType) {
            case "daily_package_1":
                toReturn.error = !UseDailyLimitIfAvail(targetPackageType, targetPackageId, 1, false);
                break;
            case "daily_package_3":
                toReturn.error = !UseDailyLimitIfAvail(targetPackageType, targetPackageId, 1, false);
                break;
            case "weekly_package_1":
                toReturn.error = !UseWeeklyLimitIfAvail(targetPackageType, targetPackageId, 1, false);
                break;
            case "weekly_package_3":
                toReturn.error = !UseWeeklyLimitIfAvail(targetPackageType, targetPackageId, 1, false);
                break;
        }
    }
    else {
        switch (targetPackageType) {
            case "daily_package_1":
                toReturn.error = !GetLeftDailyLimit(targetPackageType, targetPackageId, false);
                break;
            case "daily_package_3":
                toReturn.error = !GetLeftDailyLimit(targetPackageType, targetPackageId, false);
                break;
            case "weekly_package_1":
                toReturn.error = !GetLeftWeeklyLimit(targetPackageType, targetPackageId, false);
                break;
            case "weekly_package_3":
                toReturn.error = !GetLeftWeeklyLimit(targetPackageType, targetPackageId, false);
                break;
        }
    }
    return toReturn;
};
handlers["useDailyWeeklyPackageIfAvail"] = UsedDailyWeeklyPackageIfAvail;
let UsedDailyWeeklyPackageIfAvailV2 = function (args, context) {
    let toReturn = { error: true };
    let targetPackageId = "0";
    if (args.targetPackageId)
        targetPackageId = args.targetPackageId;
    let targetPeriod = "daily";
    let targetPackageType = "";
    if (targetPackageId.indexOf("daily") != -1) {
        targetPackageType = "daily_package_";
        targetPeriod = "daily";
    }
    else if (targetPackageId.indexOf("weekly") != -1) {
        targetPackageType = "weekly_package_";
        targetPeriod = "weekly";
    }
    let catalogs = server.GetCatalogItems({});
    let targetPackage = catalogs.Catalog.filter(n => n.ItemId == targetPackageId)[0];
    if (targetPackage == null) {
        log.info("Can't find target package in catalogs!");
        return toReturn;
    }
    let customDataJson = JSON.parse(targetPackage.CustomData);
    let limitCount = customDataJson["total_purchasable_per_period"];
    if (limitCount == null) {
        return toReturn;
    }
    else {
        targetPackageType = targetPackageType.concat(limitCount);
    }
    let askForUsing = false;
    if (args.askForUsing)
        askForUsing = args.askForUsing;
    if (askForUsing) {
        switch (targetPeriod) {
            case "daily":
                toReturn.error = !UseDailyLimitIfAvail(targetPackageType, targetPackageId, 1, false);
                break;
            case "weekly":
                toReturn.error = !UseWeeklyLimitIfAvail(targetPackageType, targetPackageId, 1, false);
                break;
        }
    }
    else {
        switch (targetPeriod) {
            case "daily":
                toReturn.error = !GetLeftDailyLimit(targetPackageType, targetPackageId, false);
                break;
            case "weekly":
                toReturn.error = !GetLeftWeeklyLimit(targetPackageType, targetPackageId, false);
                break;
        }
    }
    return toReturn;
};
handlers["useDailyWeeklyPackageIfAvailV2"] = UsedDailyWeeklyPackageIfAvailV2;
function AddItem(itemId) {
    let grantItemsToUserResult = server.GrantItemsToUser({
        PlayFabId: currentPlayerId,
        ItemIds: [itemId]
    });
}
function AddItemMultiple(itemId, itemCount) {
    if (itemCount <= 0)
        return;
    let itemIds = [];
    for (let i = 0; i < itemCount; i++) {
        itemIds.push(itemId);
    }
    let grantItemsToUserResult = server.GrantItemsToUser({
        PlayFabId: currentPlayerId,
        ItemIds: itemIds
    });
}
function ConsumeItem(itemInstanceId, itemId, consumeAmount) {
    let consumeItemResult = server.ConsumeItem({
        PlayFabId: currentPlayerId,
        ItemInstanceId: itemInstanceId,
        ConsumeCount: consumeAmount
    });
}
function AddVirtualCurrency(currencyId, addAmount) {
    if (addAmount <= 0)
        return;
    let addUserVirtualCurrencyResult = server.AddUserVirtualCurrency({
        PlayFabId: currentPlayerId,
        VirtualCurrency: currencyId,
        Amount: addAmount
    });
}
function SubtractVirtualCurrency(currencyId, subtractAmount) {
    let subtractUserVirtualCurrencyResult = server.SubtractUserVirtualCurrency({
        PlayFabId: currentPlayerId,
        VirtualCurrency: currencyId,
        Amount: subtractAmount
    });
}
function AddGachaExp(inventory, priceOfGacha) {
    let toReturn = { gachaLevel: 1, gachaExp: 0 };
    let cumulativeExp = 0;
    let gachaBundles = inventory.Inventory.filter(n => n.ItemId.substr(0, 12) == "bundle_gacha");
    for (let i = 0; i < gachaBundles.length; i++) {
        if (gachaBundles[i].ItemId == "bundle_gacha_normal_10" || gachaBundles[i].ItemId == "bundle_gacha_special_10") {
            cumulativeExp += gachaBundles[i].RemainingUses * 120;
        }
        else if (gachaBundles[i].ItemId == "bundle_gacha_special_01") {
            cumulativeExp += gachaBundles[i].RemainingUses * 12;
        }
        else {
            cumulativeExp += gachaBundles[i].RemainingUses * 10;
        }
    }
    if (priceOfGacha >= 1000) {
        cumulativeExp += priceOfGacha * 12 / 100;
    }
    else {
        cumulativeExp += priceOfGacha / 10;
    }
    let requiredGachaExpList = [0, 100, 250, 500, 1000, 2000, 4500, 9500, 19500, 44500];
    for (toReturn.gachaLevel = 0; toReturn.gachaLevel < requiredGachaExpList.length; toReturn.gachaLevel++) {
        if (cumulativeExp < requiredGachaExpList[toReturn.gachaLevel])
            break;
    }
    if (cumulativeExp >= requiredGachaExpList[requiredGachaExpList.length - 1])
        toReturn.gachaLevel = requiredGachaExpList.length;
    if (toReturn.gachaLevel != requiredGachaExpList.length)
        toReturn.gachaExp = cumulativeExp - requiredGachaExpList[toReturn.gachaLevel - 1];
    else
        toReturn.gachaExp = 0;
    let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Data: {
            "gacha_level": toReturn.gachaLevel.toString(),
            "gacha_exp": toReturn.gachaExp.toString()
        }
    });
    return toReturn;
}
let RequestAttendanceEventInfo = function (args, context) {
    let toReturn = { lastAttendanceTimeStampDay: 0, attendanceEventInfo: null, reasonOfError: "", error: true };
    let attendanceEventData = null;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["last_attendance_event_timestampday"]
    });
    if (getUserReadOnlyDataResult.Data["last_attendance_event_timestampday"] != null)
        toReturn.lastAttendanceTimeStampDay = parseInt(getUserReadOnlyDataResult.Data["last_attendance_event_timestampday"].Value);
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "attendance_event_data"
        ]
    }).Data;
    attendanceEventData = JSON.parse(getTitleDataResult["attendance_event_data"]);
    if (attendanceEventData == null)
        return toReturn;
    let currentTimeStampDay = Math.floor(+new Date() / (1000 * 60 * 60 * 24));
    if (attendanceEventData.StartTimeStampDay > currentTimeStampDay || attendanceEventData.EndTimeStampDay < currentTimeStampDay) {
        toReturn.reasonOfError = "event_expired";
        return toReturn;
    }
    toReturn.attendanceEventInfo = attendanceEventData;
    toReturn.error = false;
    return toReturn;
};
handlers["requestAttendanceEventInfo"] = RequestAttendanceEventInfo;
let RequestAttendanceEventInfoQA = function (args, context) {
    let toReturn = { lastAttendanceTimeStampDay: 0, attendanceEventInfo: null, reasonOfError: "", error: true };
    let attendanceEventData = null;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["last_attendance_event_timestampday"]
    });
    if (getUserReadOnlyDataResult.Data["last_attendance_event_timestampday"] != null)
        toReturn.lastAttendanceTimeStampDay = parseInt(getUserReadOnlyDataResult.Data["last_attendance_event_timestampday"].Value);
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "qa_attendance_event_data"
        ]
    }).Data;
    attendanceEventData = JSON.parse(getTitleDataResult["qa_attendance_event_data"]);
    if (attendanceEventData == null)
        return toReturn;
    let currentTimeStampDay = Math.floor(+new Date() / (1000 * 60 * 60 * 24));
    if (attendanceEventData.StartTimeStampDay > currentTimeStampDay || attendanceEventData.EndTimeStampDay < currentTimeStampDay) {
        toReturn.reasonOfError = "event_expired";
        return toReturn;
    }
    toReturn.attendanceEventInfo = attendanceEventData;
    toReturn.error = false;
    return toReturn;
};
handlers["requestAttendanceEventInfoQA"] = RequestAttendanceEventInfoQA;
let RequestAttendanceEventReward = function (args, context) {
    let toReturn = { itemId: "", itemCode: "", count: 0, lastAttendanceTimeStamp: 0, error: true };
    let selectedRewardIndex = 0;
    let attendanceEventData = null;
    let lastTimestamp = 0;
    let attendanceEventLog = null;
    if (args.selectedRewardIndex)
        selectedRewardIndex = args.selectedRewardIndex;
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "attendance_event_data"
        ]
    }).Data;
    attendanceEventData = JSON.parse(getTitleDataResult["attendance_event_data"]);
    if (attendanceEventData == null)
        return toReturn;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: [
            "attendance_event_log", "last_attendance_event_timestampday"
        ]
    });
    if (getUserReadOnlyDataResult.Data["attendance_event_log"] != null)
        attendanceEventLog = JSON.parse(getUserReadOnlyDataResult.Data["attendance_event_log"].Value);
    else
        attendanceEventLog = { "LogList": [] };
    if (getUserReadOnlyDataResult.Data["last_attendance_event_timestampday"] != null)
        lastTimestamp = parseInt(getUserReadOnlyDataResult.Data["last_attendance_event_timestampday"].Value);
    let currentTimeStampSecond = Math.floor(+new Date() / (1000));
    let currentTimeStampDay = Math.floor(+new Date() / (1000 * 60 * 60 * 24));
    if (currentTimeStampDay > lastTimestamp) {
        if (attendanceEventData.StartTimeStampDay > currentTimeStampDay || attendanceEventData.EndTimeStampDay < currentTimeStampDay) {
            log.info("This event's period is not validate");
            return toReturn;
        }
        if (attendanceEventData.AttendanceEventRewardList.length <= selectedRewardIndex) {
            log.info("Selected reward index value is not validate");
            return toReturn;
        }
        let currentEventLogCount = attendanceEventLog.LogList.filter(n => n.EventId == attendanceEventData.EventId).length;
        if (currentEventLogCount >= attendanceEventData.AttendanceDayLimit) {
            log.info("This user can't get reward over than limit count");
            return toReturn;
        }
        let targetRewardInfo = attendanceEventData.AttendanceEventRewardList[selectedRewardIndex];
        if (targetRewardInfo != null) {
            if (targetRewardInfo.ItemId == "gem")
                AddVirtualCurrency(targetRewardInfo.ItemCode, Number(targetRewardInfo.ItemCount));
            toReturn.itemId = targetRewardInfo.ItemId;
            toReturn.itemCode = targetRewardInfo.ItemCode;
            toReturn.count = targetRewardInfo.ItemCount;
            toReturn.lastAttendanceTimeStamp = currentTimeStampDay;
            attendanceEventLog.LogList.push({ EventId: attendanceEventData.EventId, ItemId: toReturn.itemId, ItemCode: toReturn.itemCode, Count: toReturn.count, ReceivedTime: currentTimeStampSecond });
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "last_attendance_event_timestampday": currentTimeStampDay.toString(),
                    "attendance_event_log": JSON.stringify(attendanceEventLog)
                }
            });
            toReturn.error = false;
            return toReturn;
        }
        else {
            log.info("Target Reward's info is null!");
            return toReturn;
        }
    }
    else
        return toReturn;
};
handlers["requestAttendanceEventReward"] = RequestAttendanceEventReward;
let RequestAttendanceEventRewardQA = function (args, context) {
    let toReturn = { itemId: "", itemCode: "", count: 0, lastAttendanceTimeStamp: 0, error: true };
    let selectedRewardIndex = 0;
    let attendanceEventData = null;
    let lastTimestamp = 0;
    let attendanceEventLog = null;
    if (args.selectedRewardIndex)
        selectedRewardIndex = args.selectedRewardIndex;
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "qa_attendance_event_data"
        ]
    }).Data;
    attendanceEventData = JSON.parse(getTitleDataResult["qa_attendance_event_data"]);
    if (attendanceEventData == null)
        return toReturn;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: [
            "attendance_event_log", "last_attendance_event_timestampday"
        ]
    });
    if (getUserReadOnlyDataResult.Data["attendance_event_log"] != null)
        attendanceEventLog = JSON.parse(getUserReadOnlyDataResult.Data["attendance_event_log"].Value);
    else
        attendanceEventLog = { "LogList": [] };
    if (getUserReadOnlyDataResult.Data["last_attendance_event_timestampday"] != null)
        lastTimestamp = parseInt(getUserReadOnlyDataResult.Data["last_attendance_event_timestampday"].Value);
    let currentTimeStampSecond = Math.floor(+new Date() / (1000));
    let currentTimeStampDay = Math.floor(+new Date() / (1000 * 60 * 60 * 24));
    if (currentTimeStampDay > lastTimestamp) {
        if (attendanceEventData.StartTimeStampDay > currentTimeStampDay || attendanceEventData.EndTimeStampDay < currentTimeStampDay) {
            log.info("This event's period is not validate");
            return toReturn;
        }
        if (attendanceEventData.AttendanceEventRewardList.length <= selectedRewardIndex) {
            log.info("Selected reward index value is not validate");
            return toReturn;
        }
        let currentEventLogCount = attendanceEventLog.LogList.filter(n => n.EventId == attendanceEventData.EventId).length;
        if (currentEventLogCount >= attendanceEventData.AttendanceDayLimit) {
            log.info("This user can't get reward over than limit count");
            return toReturn;
        }
        let targetRewardInfo = attendanceEventData.AttendanceEventRewardList[selectedRewardIndex];
        if (targetRewardInfo != null) {
            if (targetRewardInfo.ItemId == "gem")
                AddVirtualCurrency(targetRewardInfo.ItemCode, Number(targetRewardInfo.ItemCount));
            toReturn.itemId = targetRewardInfo.ItemId;
            toReturn.itemCode = targetRewardInfo.ItemCode;
            toReturn.count = targetRewardInfo.ItemCount;
            toReturn.lastAttendanceTimeStamp = currentTimeStampDay;
            attendanceEventLog.LogList.push({ EventId: attendanceEventData.EventId, ItemId: toReturn.itemId, ItemCode: toReturn.itemCode, Count: toReturn.count, ReceivedTime: currentTimeStampSecond });
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "last_attendance_event_timestampday": currentTimeStampDay.toString(),
                    "attendance_event_log": JSON.stringify(attendanceEventLog)
                }
            });
            toReturn.error = false;
            return toReturn;
        }
        else {
            log.info("Target Reward's info is null!");
            return toReturn;
        }
    }
    else
        return toReturn;
};
handlers["requestAttendanceEventRewardQA"] = RequestAttendanceEventRewardQA;
let GuildCreate = function (args, context) {
    let toReturn = { guildManagementData: {}, guildMemberData: {}, requiredGem: 0, reasonOfError: "", error: true };
    let serverId = "";
    let guildName = "";
    let guildFlagIndex = 0;
    let guildNotice = "";
    let isPrivate = false;
    let requiredStageProgress = 0;
    let guildMasterName = "";
    let guildMasterStageProgress = 1;
    if (args.guildName)
        guildName = args.guildName;
    if (args.guildFlagIndex)
        guildFlagIndex = args.guildFlagIndex;
    if (args.guildNotice)
        guildNotice = args.guildNotice;
    if (args.isPrivate)
        isPrivate = args.isPrivate;
    if (args.requiredStageProgress)
        requiredStageProgress = args.requiredStageProgress;
    if (args.guildMasterName)
        guildMasterName = args.guildMasterName;
    if (args.guildMasterStageProgress)
        guildMasterStageProgress = args.guildMasterStageProgress;
    if (guildName.length > 10) {
        log.info("Guild name is too long!");
        return toReturn;
    }
    if (guildNotice.length > 100) {
        log.info("Notice size is too long!");
        return toReturn;
    }
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_id"]
    });
    if (getUserReadOnlyDataResult.Data["guild_id"] != null) {
        log.info("User already have joined a guild!");
        return toReturn;
    }
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "default_guild_data",
            "setting_json"
        ]
    }).Data;
    let defaultGuildData = JSON.parse(getTitleDataResult["default_guild_data"]);
    let settingInfo = JSON.parse(getTitleDataResult["setting_json"]);
    if (defaultGuildData != null && settingInfo != null && settingInfo.GuildCreatePrice != null) {
        let getUserInventoryResult = server.GetUserInventory({
            PlayFabId: currentPlayerId
        });
        let guildCreatePrice = parseInt(settingInfo.GuildCreatePrice);
        let currentGemCount = 0;
        if (getUserInventoryResult.VirtualCurrency["GE"] != null)
            currentGemCount = getUserInventoryResult.VirtualCurrency["GE"];
        if (currentGemCount < guildCreatePrice) {
            log.info("Not enough gems!");
            return toReturn;
        }
        let getPlayerTagsResult = server.GetPlayerTags({
            PlayFabId: currentPlayerId
        });
        if (getPlayerTagsResult != null && getPlayerTagsResult.Tags.length > 0)
            serverId = GetServerIdFromTag(getPlayerTagsResult.Tags.filter(n => n.indexOf("Server") >= 0)[0]);
        else
            serverId = "";
        let getUserAccountInfoResult = server.GetUserAccountInfo({
            PlayFabId: currentPlayerId
        });
        if (getUserAccountInfoResult.UserInfo.TitleInfo.TitlePlayerAccount.Id != null && getUserAccountInfoResult.UserInfo.TitleInfo.TitlePlayerAccount.Type != null) {
            try {
                let createGroupResult = entity.CreateGroup({
                    Entity: {
                        Id: getUserAccountInfoResult.UserInfo.TitleInfo.TitlePlayerAccount.Id,
                        Type: getUserAccountInfoResult.UserInfo.TitleInfo.TitlePlayerAccount.Type
                    },
                    GroupName: guildName
                });
                defaultGuildData.GroupId = createGroupResult.Group.Id;
            }
            catch (error) {
                let errorData = JSON.parse(JSON.stringify(error));
                if (errorData != null && errorData.apiErrorInfo.apiError.errorCode == 1368) {
                    toReturn.reasonOfError = "guild_name_duplicated";
                    return toReturn;
                }
                else
                    return toReturn;
            }
        }
        else
            return toReturn;
        defaultGuildData.ServerId = serverId;
        defaultGuildData.GuildId = currentPlayerId;
        defaultGuildData.GuildName = guildName;
        defaultGuildData.GuildFlagIndex = guildFlagIndex;
        defaultGuildData.GuildNotice = guildNotice;
        defaultGuildData.IsPrivate = isPrivate;
        defaultGuildData.RequiredStageProgress = requiredStageProgress;
        defaultGuildData.TimestampDay = Math.floor(+new Date() / (1000 * 60 * 60 * 24));
        let guildMemberData = JSON.parse("{\"MemberList\":[]}");
        let guildCacheData = JSON.parse("{\"MemberCacheDataList\":[]}");
        let guildApplicantData = JSON.parse("{\"ApplicantList\":[]}");
        guildMemberData.MemberList.push({ "Id": currentPlayerId, "EntityId": getUserAccountInfoResult.UserInfo.TitleInfo.TitlePlayerAccount.Id,
            "EntityType": getUserAccountInfoResult.UserInfo.TitleInfo.TitlePlayerAccount.Type, "JoinTimeStamp": Math.floor(+new Date() / 1000) });
        guildCacheData.MemberCacheDataList.push({
            "Id": currentPlayerId,
            "Name": guildMasterName,
            "StageProgress": guildMasterStageProgress,
            "LastTimeStamp": Math.floor(+new Date() / (1000 * 60 * 60)),
            "TotalContribution": 0,
            "RaidScore": 0,
            "RaidSeason": -1
        });
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "guild_id": currentPlayerId,
                "guild_management_data": unescape(JSON.stringify(defaultGuildData)),
                "guild_total_exp": "0",
                "guild_master_exp": "0",
                "guild_member_list": unescape(JSON.stringify(guildMemberData)),
                "guild_cache_list": unescape(JSON.stringify(guildCacheData)),
                "guild_applicant_list": unescape(JSON.stringify(guildApplicantData)),
                "apply_guild_list": null
            }
        });
        UpdateGuildRecommendationScore(GetServerIdModifierFromServerId(defaultGuildData.ServerId), defaultGuildData.GuildId, parseInt(defaultGuildData.TimestampDay), 1);
        SubtractVirtualCurrency("GE", guildCreatePrice);
        toReturn.guildManagementData = defaultGuildData;
        toReturn.guildMemberData = guildCacheData;
        toReturn.requiredGem = guildCreatePrice;
        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;
};
handlers["guildCreate"] = GuildCreate;
let GuildDelete = function (args, context) {
    let toReturn = { reasonOfError: "", error: true };
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_management_data", "guild_member_list"]
    });
    if (!IsGuildMaster(getUserReadOnlyDataResult.Data["guild_management_data"])) {
        log.info("This user is not guild master!");
        return toReturn;
    }
    if (getUserReadOnlyDataResult.Data["guild_management_data"] != null && getUserReadOnlyDataResult.Data["guild_member_list"] != null) {
        let guildManagementData = JSON.parse(getUserReadOnlyDataResult.Data["guild_management_data"].Value);
        let guildMemberData = JSON.parse(getUserReadOnlyDataResult.Data["guild_member_list"].Value);
        guildMemberData.MemberList = guildMemberData.MemberList.filter(n => n.Id != currentPlayerId);
        if (guildMemberData.MemberList.length != 0) {
            log.info("Guild member is left!");
            toReturn.reasonOfError = "guild_member_left";
            return toReturn;
        }
        try {
            let deleteGroupResult = entity.DeleteGroup({
                Group: { Id: guildManagementData.GroupId, Type: null }
            });
        }
        catch (error) {
            log.info("Error in entity.DeleteGroup: " + JSON.parse(JSON.stringify(error)));
            return toReturn;
        }
        let lastGuildLeaveTimeStamp = Math.floor(+new Date() / 1000);
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "guild_id": null,
                "guild_management_data": null,
                "guild_total_exp": null,
                "guild_master_exp": null,
                "guild_member_list": null,
                "guild_cache_list": null,
                "guild_applicant_list": null,
                "guild_raid_reward_last_played_season": null,
                "last_guild_leave_timestamp": lastGuildLeaveTimeStamp.toString()
            }
        });
        let serverIdModifier = GetServerIdModifierFromServerId(guildManagementData.ServerId);
        let currentStatValue = server.GetPlayerStatistics({
            PlayFabId: currentPlayerId,
            StatisticNames: ["guild_raid" + serverIdModifier]
        }).Statistics;
        let oldMasterScore = 0;
        if (currentStatValue[0] != null)
            oldMasterScore = currentStatValue[0].Value;
        server.UpdatePlayerStatistics({
            PlayFabId: currentPlayerId,
            Statistics: [
                {
                    "StatisticName": "guild_raid" + serverIdModifier,
                    "Value": oldMasterScore * -1
                },
                {
                    "StatisticName": "guild_recommend" + serverIdModifier,
                    "Value": 0
                }
            ]
        });
        toReturn.error = false;
        return toReturn;
    }
    else {
        log.info("User is not guild master or don't have any guild!");
        return toReturn;
    }
};
handlers["guildDelete"] = GuildDelete;
let GuildGetApplicantList = function (args, context) {
    let toReturn = { applicantList: {}, error: true };
    let applicantListToReturn = [];
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_management_data", "guild_applicant_list"]
    });
    if (!IsGuildMaster(getUserReadOnlyDataResult.Data["guild_management_data"])) {
        log.info("This user is not guild master!");
        return toReturn;
    }
    if (getUserReadOnlyDataResult.Data["guild_applicant_list"] != null) {
        let guildApplicantData = JSON.parse(getUserReadOnlyDataResult.Data["guild_applicant_list"].Value);
        for (let i = 0; i < guildApplicantData.ApplicantList.length; i++) {
            let applicantId = guildApplicantData.ApplicantList[i].Id;
            let userName = "";
            let userLastStageProgress = 1;
            let getUserAccountInfoResult = server.GetUserAccountInfo({
                PlayFabId: applicantId
            });
            userName = getUserAccountInfoResult.UserInfo.TitleInfo.DisplayName;
            let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
                PlayFabId: applicantId,
                Keys: ["guild_id", "user_data"]
            });
            userLastStageProgress = GetStageProgress(getUserReadOnlyDataResult.Data["user_data"]);
            if (getUserReadOnlyDataResult.Data["guild_id"] == null)
                applicantListToReturn.push({ "Id": applicantId, "Name": userName, "StageProgress": userLastStageProgress });
        }
        guildApplicantData.ApplicantList = applicantListToReturn;
        let updateGuildMasterReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "guild_applicant_list": unescape(JSON.stringify(guildApplicantData))
            }
        });
        toReturn.applicantList = { "ApplicantList": applicantListToReturn };
        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;
};
handlers["guildGetApplicantList"] = GuildGetApplicantList;
let GuildApplicantAccept = function (args, context) {
    let toReturn = { targetId: "", applicantList: {}, memberCacheDataList: {}, reasonOfError: "", error: true };
    let applicantId = "";
    let applicantName = "";
    let applicantStageProgress = 1;
    if (args.applicantId)
        applicantId = args.applicantId;
    toReturn.targetId = applicantId;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_management_data", "guild_member_list", "guild_cache_list", "guild_applicant_list", "guild_total_exp", "guild_master_exp"]
    });
    if (!IsGuildMaster(getUserReadOnlyDataResult.Data["guild_management_data"])) {
        log.info("This user is not guild master!");
        return toReturn;
    }
    if (getUserReadOnlyDataResult.Data["guild_management_data"] != null && getUserReadOnlyDataResult.Data["guild_member_list"] != null && getUserReadOnlyDataResult.Data["guild_cache_list"] != null && getUserReadOnlyDataResult.Data["guild_applicant_list"] != null) {
        let guildManagementData = JSON.parse(getUserReadOnlyDataResult.Data["guild_management_data"].Value);
        let guildMemberData = JSON.parse(getUserReadOnlyDataResult.Data["guild_member_list"].Value);
        let guildCacheData = JSON.parse(getUserReadOnlyDataResult.Data["guild_cache_list"].Value);
        let guildApplicantData = JSON.parse(getUserReadOnlyDataResult.Data["guild_applicant_list"].Value);
        if (guildApplicantData.ApplicantList.find(n => n.Id == applicantId) == null) {
            log.info("Can't find in application list!");
            return toReturn;
        }
        let guildLevelInfo = GetGuildLevelAndExp(getUserReadOnlyDataResult.Data["guild_total_exp"], getUserReadOnlyDataResult.Data["guild_master_exp"], null);
        if (guildLevelInfo.error) {
            log.info("guild level info data is wrong!");
            return toReturn;
        }
        if (guildMemberData.MemberList.length >= GetGuildMemberLimitCount(guildLevelInfo.level)) {
            log.info("Guild member count is full!");
            toReturn.reasonOfError = "guild_member_full";
            return toReturn;
        }
        let getApplicationReadOnlyDataResult = server.GetUserReadOnlyData({
            PlayFabId: applicantId,
            Keys: ["guild_id", "guild_management_data", "user_data"]
        });
        if (getApplicationReadOnlyDataResult.Data["guild_id"] != null || getApplicationReadOnlyDataResult.Data["guild_management_data"] != null) {
            log.info("Application already have joined a guild!");
            guildApplicantData.ApplicantList = guildApplicantData.ApplicantList.filter(n => n.Id != applicantId);
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "guild_applicant_list": unescape(JSON.stringify(guildApplicantData))
                }
            });
            toReturn.reasonOfError = "already_have_guild";
            return toReturn;
        }
        let getUserAccountInfoResult = server.GetUserAccountInfo({
            PlayFabId: applicantId
        });
        applicantName = getUserAccountInfoResult.UserInfo.TitleInfo.DisplayName;
        applicantStageProgress = GetStageProgress(getApplicationReadOnlyDataResult.Data["user_data"]);
        let currentTimestamp = Math.floor(+new Date() / (1000 * 60 * 60 * 24));
        guildMemberData.MemberList.push({ "Id": applicantId, "EntityId": getUserAccountInfoResult.UserInfo.TitleInfo.TitlePlayerAccount.Id,
            "EntityType": getUserAccountInfoResult.UserInfo.TitleInfo.TitlePlayerAccount.Type, "JoinTimeStamp": Math.floor(+new Date() / 1000) });
        guildCacheData.MemberCacheDataList.push({
            "Id": applicantId,
            "Name": applicantName,
            "StageProgress": applicantStageProgress,
            "LastTimeStamp": -1,
            "TotalContribution": 0,
            "RaidScore": 0,
            "RaidSeason": -1
        });
        guildApplicantData.ApplicantList = guildApplicantData.ApplicantList.filter(n => n.Id != applicantId);
        for (let i = 0; i < guildMemberData.MemberList.length; i++) {
            if (guildCacheData.MemberCacheDataList.find(n => n.Id == guildMemberData.MemberList[i].Id) == null) {
                guildCacheData.MemberCacheDataList.push({
                    "Id": guildMemberData.MemberList[i].Id,
                    "Name": "???",
                    "Level": -1,
                    "LastTimeStamp": -1,
                    "TotalContribution": 0,
                    "RaidScore": 0,
                    "RaidSeason": -1
                });
            }
        }
        let newCacheDataList = [];
        for (let i = 0; i < guildMemberData.MemberList.length; i++) {
            newCacheDataList.push(guildCacheData.MemberCacheDataList.find(n => n.Id == guildMemberData.MemberList[i].Id));
        }
        guildCacheData.MemberCacheDataList = newCacheDataList;
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "guild_management_data": unescape(JSON.stringify(guildManagementData)),
                "guild_member_list": unescape(JSON.stringify(guildMemberData)),
                "guild_cache_list": unescape(JSON.stringify(guildCacheData)),
                "guild_applicant_list": unescape(JSON.stringify(guildApplicantData)),
            }
        });
        UpdateGuildRecommendationScore(GetServerIdModifierFromServerId(guildManagementData.ServerId), guildManagementData.GuildId, parseInt(guildManagementData.TimestampDay), guildMemberData.MemberList.length);
        let updateMemeberReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: applicantId,
            Data: {
                "guild_id": guildManagementData.GuildId,
                "apply_guild_list": null
            }
        });
        toReturn.memberCacheDataList = { "MemberCacheDataList": guildCacheData.MemberCacheDataList };
        toReturn.applicantList = { "ApplicantList": guildApplicantData.ApplicantList };
        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;
};
handlers["guildApplicantAccept"] = GuildApplicantAccept;
let GuildApplicantRefuse = function (args, context) {
    let toReturn = { targetId: "", applicantList: {}, error: true };
    let applicantId = "";
    if (args.applicantId)
        applicantId = args.applicantId;
    toReturn.targetId = applicantId;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_management_data", "guild_applicant_list"]
    });
    if (getUserReadOnlyDataResult.Data["guild_management_data"] != null && getUserReadOnlyDataResult.Data["guild_applicant_list"] != null) {
        if (!IsGuildMaster(getUserReadOnlyDataResult.Data["guild_management_data"])) {
            log.info("This user is not guild master!");
            return toReturn;
        }
        let guildManagementData = JSON.parse(getUserReadOnlyDataResult.Data["guild_management_data"].Value);
        let guildApplicantData = JSON.parse(getUserReadOnlyDataResult.Data["guild_applicant_list"].Value);
        if (guildApplicantData.ApplicantList.find(n => n.Id == applicantId) == null) {
            log.info("Can't find in application list!");
            return toReturn;
        }
        guildApplicantData.ApplicantList = guildApplicantData.ApplicantList.filter(n => n.Id != applicantId);
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "guild_applicant_list": unescape(JSON.stringify(guildApplicantData))
            }
        });
        let getApplicantReadOnlyDataResult = server.GetUserReadOnlyData({
            PlayFabId: applicantId,
            Keys: ["apply_guild_list"]
        });
        if (getApplicantReadOnlyDataResult.Data["apply_guild_list"] != null) {
            let applyGuildData = JSON.parse(getApplicantReadOnlyDataResult.Data["apply_guild_list"].Value);
            applyGuildData.ApplyGuildList = applyGuildData.ApplyGuildList.filter(n => n.toUpperCase() != guildManagementData.GuildId);
            let updateApplicantReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: applicantId,
                Data: {
                    "apply_guild_list": unescape(JSON.stringify(applyGuildData))
                }
            });
        }
        toReturn.applicantList = { "ApplicantList": guildApplicantData.ApplicantList };
        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;
};
handlers["guildApplicantRefuse"] = GuildApplicantRefuse;
let GuildGetKickList = function (args, context) {
    let toReturn = { kickList: {}, error: true };
    let kickListToReturn = [];
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_management_data"]
    });
    if (!IsGuildMaster(getUserReadOnlyDataResult.Data["guild_management_data"])) {
        log.info("This user is not guild master!");
        return toReturn;
    }
    if (getUserReadOnlyDataResult.Data["guild_management_data"] != null) {
        let guildManagementData = JSON.parse(getUserReadOnlyDataResult.Data["guild_management_data"].Value);
        let getUserAccountInfoAPICallCount = 0;
        for (let i = 0; i < guildManagementData.KickList.length; i++) {
            let kickedUserId = guildManagementData.KickList[i].Id;
            let userName = "";
            if (guildManagementData.KickList[i].UserName != null)
                userName = guildManagementData.KickList[i].UserName;
            if (userName == "") {
                if (getUserAccountInfoAPICallCount < 20) {
                    let getUserAccountInfoResult = server.GetUserAccountInfo({
                        PlayFabId: kickedUserId
                    });
                    userName = getUserAccountInfoResult.UserInfo.TitleInfo.DisplayName;
                    guildManagementData.KickList[i].UserName = getUserAccountInfoResult.UserInfo.TitleInfo.DisplayName;
                    getUserAccountInfoAPICallCount++;
                }
                else
                    userName = "Unknown";
            }
            kickListToReturn.push({ "Id": kickedUserId, "Name": userName });
        }
        if (getUserAccountInfoAPICallCount > 0) {
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "guild_management_data": unescape(JSON.stringify(guildManagementData))
                }
            });
        }
        toReturn.kickList = { "KickMemberList": kickListToReturn };
        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;
};
handlers["guildGetKickList"] = GuildGetKickList;
let GuildKickMember = function (args, context) {
    let toReturn = { targetId: "", memberCacheDataList: {}, error: true };
    let memberId = "";
    let memberName = "";
    if (args.memberId)
        memberId = args.memberId;
    if (args.memberName)
        memberName = args.memberName;
    toReturn.targetId = memberId;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_management_data", "guild_member_list", "guild_cache_list"]
    });
    if (getUserReadOnlyDataResult.Data["guild_management_data"] != null && getUserReadOnlyDataResult.Data["guild_member_list"] != null && getUserReadOnlyDataResult.Data["guild_cache_list"] != null) {
        let guildManagementData = JSON.parse(getUserReadOnlyDataResult.Data["guild_management_data"].Value);
        let guildMemberData = JSON.parse(getUserReadOnlyDataResult.Data["guild_member_list"].Value);
        let guildCacheData = JSON.parse(getUserReadOnlyDataResult.Data["guild_cache_list"].Value);
        if (!IsGuildMaster(getUserReadOnlyDataResult.Data["guild_management_data"])) {
            log.info("This user is not guild master!");
            return toReturn;
        }
        if (guildManagementData.KickList.length > 24) {
            log.info("Kick member count can't over 25!");
            return toReturn;
        }
        let targetMemberInfo = guildMemberData.MemberList.find(n => n.Id == memberId);
        if (targetMemberInfo == null) {
            log.info("Can't find in member list!");
            return toReturn;
        }
        if (memberName == "") {
            let targetMemberCacheInfo = guildCacheData.MemberCacheDataList.find(n => n.Id == memberId);
            if (targetMemberCacheInfo != null)
                memberName = targetMemberCacheInfo.Name;
        }
        guildManagementData.KickList.push({ "Id": memberId, "UserName": memberName });
        guildMemberData.MemberList = guildMemberData.MemberList.filter(n => n.Id != memberId);
        for (let i = 0; i < guildMemberData.MemberList.length; i++) {
            if (guildCacheData.MemberCacheDataList.find(n => n.Id == guildMemberData.MemberList[i].Id) == null) {
                guildCacheData.MemberCacheDataList.push({
                    "Id": guildMemberData.MemberList[i].Id,
                    "Name": "???",
                    "Level": -1,
                    "LastTimeStamp": -1,
                    "TotalContribution": 0,
                    "RaidScore": 0,
                    "RaidSeason": -1
                });
            }
        }
        let newCacheDataList = [];
        for (let i = 0; i < guildMemberData.MemberList.length; i++) {
            newCacheDataList.push(guildCacheData.MemberCacheDataList.find(n => n.Id == guildMemberData.MemberList[i].Id));
        }
        guildCacheData.MemberCacheDataList = newCacheDataList;
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "guild_management_data": unescape(JSON.stringify(guildManagementData)),
                "guild_member_list": unescape(JSON.stringify(guildMemberData)),
                "guild_cache_list": unescape(JSON.stringify(guildCacheData)),
            }
        });
        UpdateGuildRecommendationScore(GetServerIdModifierFromServerId(guildManagementData.ServerId), guildManagementData.GuildId, parseInt(guildManagementData.TimestampDay), guildMemberData.MemberList.length);
        let lastGuildLeaveTimeStamp = Math.floor(+new Date() / 1000);
        let updateMemberReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: memberId,
            Data: {
                "guild_id": null,
                "guild_raid_reward_last_played_season": null,
                "last_guild_leave_timestamp": lastGuildLeaveTimeStamp.toString()
            }
        });
        toReturn.memberCacheDataList = { "MemberCacheDataList": guildCacheData.MemberCacheDataList };
        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;
};
handlers["guildKickMember"] = GuildKickMember;
let GuildKickCancel = function (args, context) {
    let toReturn = { targetId: "", error: true };
    let memberId = "";
    if (args.memberId)
        memberId = args.memberId;
    toReturn.targetId = memberId;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_management_data"]
    });
    if (getUserReadOnlyDataResult.Data["guild_management_data"] != null) {
        if (!IsGuildMaster(getUserReadOnlyDataResult.Data["guild_management_data"])) {
            log.info("This user is not guild master!");
            return toReturn;
        }
        let guildManagementData = JSON.parse(getUserReadOnlyDataResult.Data["guild_management_data"].Value);
        let targetMemberId = guildManagementData.KickList.find(n => n.Id == memberId);
        if (targetMemberId == null) {
            log.info("Can't find in member list!");
            return toReturn;
        }
        guildManagementData.KickList = guildManagementData.KickList.filter(n => n.Id != memberId);
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "guild_management_data": unescape(JSON.stringify(guildManagementData))
            }
        });
        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;
};
handlers["guildKickCancel"] = GuildKickCancel;
let GuildMasterTransfer = function (args, context) {
    let toReturn = { guildId: "", error: true };
    let newGuildMasterId = "";
    if (args.newGuildMasterId)
        newGuildMasterId = args.newGuildMasterId;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: [
            "guild_management_data",
            "guild_total_exp",
            "guild_master_exp",
            "guild_member_list",
            "guild_cache_list",
            "guild_applicant_list"
        ]
    });
    if (!IsGuildMaster(getUserReadOnlyDataResult.Data["guild_management_data"])) {
        log.info("This user is not guild master!");
        return toReturn;
    }
    if (getUserReadOnlyDataResult.Data["guild_management_data"] != null && getUserReadOnlyDataResult.Data["guild_member_list"] != null
        && getUserReadOnlyDataResult.Data["guild_cache_list"] != null && getUserReadOnlyDataResult.Data["guild_applicant_list"] != null) {
        let guildManagementData = JSON.parse(getUserReadOnlyDataResult.Data["guild_management_data"].Value);
        let guildMemberData = JSON.parse(getUserReadOnlyDataResult.Data["guild_member_list"].Value);
        let newGuildMasterInfo = guildMemberData.MemberList.find(n => n.Id == newGuildMasterId);
        let oldGuildMasterInfo = guildMemberData.MemberList.find(n => n.Id == guildManagementData.GuildId);
        if (newGuildMasterInfo == null || oldGuildMasterInfo == null) {
            log.info("New guild master or old guild master is not member of our guild!");
            return toReturn;
        }
        guildManagementData.GuildId = newGuildMasterId;
        let guildTotalExp = 0;
        if (getUserReadOnlyDataResult.Data["guild_total_exp"] != null)
            guildTotalExp = parseInt(getUserReadOnlyDataResult.Data["guild_total_exp"].Value);
        let guildMasterExp = 0;
        if (getUserReadOnlyDataResult.Data["guild_master_exp"] != null)
            guildMasterExp = parseInt(getUserReadOnlyDataResult.Data["guild_master_exp"].Value);
        try {
            let addMembersResult = entity.AddMembers({
                Group: { Id: guildManagementData.GroupId, Type: null },
                Members: [
                    {
                        Id: newGuildMasterInfo.EntityId,
                        Type: newGuildMasterInfo.EntityType
                    }
                ],
                RoleId: "admins"
            });
        }
        catch (error) {
            log.info("Error in entity.AddNewGuildMaster : " + JSON.stringify(error));
            return toReturn;
        }
        try {
            let removeMembersResult = entity.RemoveMembers({
                Group: { Id: guildManagementData.GroupId, Type: null },
                Members: [
                    {
                        Id: oldGuildMasterInfo.EntityId,
                        Type: oldGuildMasterInfo.EntityType
                    }
                ],
            });
        }
        catch (error) {
            log.info("Error in entity.RemoveOldGuildMaster : " + JSON.stringify(error));
            return toReturn;
        }
        let updateNewGuildMasterReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: newGuildMasterId,
            Data: {
                "guild_id": newGuildMasterId,
                "guild_management_data": unescape(JSON.stringify(guildManagementData)),
                "guild_total_exp": guildTotalExp.toString(),
                "guild_master_exp": guildMasterExp.toString(),
                "guild_member_list": getUserReadOnlyDataResult.Data["guild_member_list"].Value,
                "guild_cache_list": getUserReadOnlyDataResult.Data["guild_cache_list"].Value,
                "guild_applicant_list": getUserReadOnlyDataResult.Data["guild_applicant_list"].Value
            }
        });
        let updateOldGuildMasterReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "guild_id": newGuildMasterId,
                "guild_management_data": null,
                "guild_total_exp": null,
                "guild_master_exp": null,
                "guild_member_list": null,
                "guild_cache_list": null,
                "guild_applicant_list": null
            }
        });
        for (let i = 0; i < guildMemberData.MemberList.length; i++) {
            if (guildMemberData.MemberList[i].Id != currentPlayerId && guildMemberData.MemberList[i].Id != newGuildMasterId) {
                let updateMemberReadOnlyDataResult = server.UpdateUserReadOnlyData({
                    PlayFabId: guildMemberData.MemberList[i].Id,
                    Data: {
                        "guild_id": newGuildMasterId
                    }
                });
            }
        }
        let serverIdModifier = GetServerIdModifier(currentPlayerId);
        let currentStatValue = server.GetPlayerStatistics({
            PlayFabId: currentPlayerId,
            StatisticNames: ["guild_raid" + serverIdModifier]
        }).Statistics;
        let oldMasterScore = 0;
        if (currentStatValue[0] != null)
            oldMasterScore = currentStatValue[0].Value;
        server.UpdatePlayerStatistics({
            PlayFabId: currentPlayerId,
            Statistics: [
                {
                    "StatisticName": "guild_raid" + serverIdModifier,
                    "Value": oldMasterScore * -1
                },
                {
                    "StatisticName": "guild_recommend" + serverIdModifier,
                    "Value": 0
                }
            ]
        });
        if (oldMasterScore != 0) {
            server.UpdatePlayerStatistics({
                PlayFabId: newGuildMasterId,
                Statistics: [
                    {
                        "StatisticName": "guild_raid" + serverIdModifier,
                        "Value": oldMasterScore
                    }
                ]
            });
        }
        UpdateGuildRecommendationScore(GetServerIdModifierFromServerId(guildManagementData.ServerId), guildManagementData.GuildId, parseInt(guildManagementData.TimestampDay), guildMemberData.MemberList.length);
        toReturn.guildId = guildManagementData.GuildId;
        toReturn.error = false;
        return toReturn;
    }
    else {
        log.info("User is not guild master or don't have any guild!");
        return toReturn;
    }
};
handlers["guildMasterTransfer"] = GuildMasterTransfer;
let GuildSettingEdit = function (args, context) {
    let toReturn = { error: true };
    let guildNotice = "";
    let guildFlagIndex = 0;
    let requiredStageProgress = 1;
    let isPrivate = false;
    if (args.guildNotice)
        guildNotice = args.guildNotice;
    if (args.guildFlagIndex)
        guildFlagIndex = args.guildFlagIndex;
    if (args.requiredStageProgress)
        requiredStageProgress = args.requiredStageProgress;
    if (args.isPrivate)
        isPrivate = args.isPrivate;
    if (guildNotice.length > 100) {
        log.info("Notice is too long!");
        return toReturn;
    }
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_management_data"]
    });
    if (getUserReadOnlyDataResult.Data["guild_management_data"] != null) {
        if (!IsGuildMaster(getUserReadOnlyDataResult.Data["guild_management_data"])) {
            log.info("This user is not guild master!");
            return toReturn;
        }
        let guildManagementData = JSON.parse(getUserReadOnlyDataResult.Data["guild_management_data"].Value);
        guildManagementData.GuildNotice = guildNotice;
        guildManagementData.GuildFlagIndex = guildFlagIndex;
        guildManagementData.RequiredStageProgress = requiredStageProgress;
        guildManagementData.IsPrivate = isPrivate;
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "guild_management_data": unescape(JSON.stringify(guildManagementData))
            }
        });
        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;
};
handlers["guildSettingEdit"] = GuildSettingEdit;
let GuildJoin = function (args, context) {
    let toReturn = { targetGuildId: "", guildManagementData: {}, memberCacheDataList: {}, guildLevel: 1, guildExp: 0, guildNextExp: 0, dailyLimitFreeDonateLeft: 0, dailyLimitDonateLeft: 0, reasonOfError: "unknown", error: true };
    let guildId = "";
    let userName = "";
    let userStageProgress = 1;
    if (args.guildId)
        guildId = args.guildId;
    if (args.userName)
        userName = args.userName;
    if (args.userStageProgress)
        userStageProgress = args.userStageProgress;
    toReturn.targetGuildId = guildId;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_id", "apply_guild_list"]
    });
    if (getUserReadOnlyDataResult.Data["guild_id"] != null) {
        log.info("User already have joined a guild! Guild Id: " + getUserReadOnlyDataResult.Data["guild_id"].Value);
        return toReturn;
    }
    let applyGuildData = JSON.parse("{\"ApplyGuildList\":[]}");
    if (getUserReadOnlyDataResult.Data["apply_guild_list"] != null) {
        applyGuildData = JSON.parse(getUserReadOnlyDataResult.Data["apply_guild_list"].Value);
        if (applyGuildData.ApplyGuildList.length > 0) {
            let getAppliedGuildReadOnlyDataResult = server.GetUserReadOnlyData({
                PlayFabId: applyGuildData.ApplyGuildList[0],
                Keys: ["guild_management_data"]
            });
            if (getAppliedGuildReadOnlyDataResult.Data["guild_management_data"] != null) {
                let appliedGuildManagementData = JSON.parse(getAppliedGuildReadOnlyDataResult.Data["guild_management_data"].Value);
                if (appliedGuildManagementData.IsPrivate) {
                    toReturn.reasonOfError = "guild_applied_guild_exist";
                    return toReturn;
                }
                else
                    applyGuildData.ApplyGuildList = applyGuildData.ApplyGuildList.filter(n => n.toUpperCase() != guildId);
            }
            else
                applyGuildData.ApplyGuildList = applyGuildData.ApplyGuildList.filter(n => n.toUpperCase() != guildId);
        }
    }
    let getGuildMasterReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: guildId,
        Keys: ["guild_management_data", "guild_total_exp", "guild_master_exp", "guild_member_list", "guild_cache_list"]
    });
    if (getGuildMasterReadOnlyDataResult.Data["guild_management_data"] != null && getGuildMasterReadOnlyDataResult.Data["guild_member_list"] != null && getGuildMasterReadOnlyDataResult.Data["guild_cache_list"]) {
        let guildManagementData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_management_data"].Value);
        let guildMemberData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_member_list"].Value);
        let guildCacheData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_cache_list"].Value);
        let guildLevelInfo = GetGuildLevelAndExp(getGuildMasterReadOnlyDataResult.Data["guild_total_exp"], getGuildMasterReadOnlyDataResult.Data["guild_master_exp"], null);
        if (guildLevelInfo.error) {
            log.info("guild level info data is wrong!");
            return toReturn;
        }
        if (guildMemberData.MemberList.length >= GetGuildMemberLimitCount(guildLevelInfo.level)) {
            log.info("Guild member count is full!");
            toReturn.reasonOfError = "guild_member_full";
            return toReturn;
        }
        if (userStageProgress < guildManagementData.RequiredStageProgress) {
            log.info("User's stage progress is not enough for join this guild!");
            toReturn.reasonOfError = "guild_stage_not_enough";
            return toReturn;
        }
        if (guildManagementData.KickList.find(n => n.Id == currentPlayerId) != null) {
            log.info("This user's id include in kick list!");
            toReturn.reasonOfError = "guild_banned";
            return toReturn;
        }
        let getPlayerTagsResult = server.GetPlayerTags({
            PlayFabId: currentPlayerId
        });
        let serverId = "";
        if (getPlayerTagsResult != null && getPlayerTagsResult.Tags.length > 0)
            serverId = GetServerIdFromTag(getPlayerTagsResult.Tags.filter(n => n.indexOf("Server") >= 0)[0]);
        if (serverId != guildManagementData.ServerId) {
            log.info("Server is not same!");
            return toReturn;
        }
        if (!guildManagementData.IsPrivate) {
            let getUserAccountInfoResult = server.GetUserAccountInfo({
                PlayFabId: currentPlayerId
            });
            let currentTimestamp = Math.floor(+new Date() / (1000 * 60 * 60 * 24));
            guildMemberData.MemberList.push({ "Id": currentPlayerId, "EntityId": getUserAccountInfoResult.UserInfo.TitleInfo.TitlePlayerAccount.Id,
                "EntityType": getUserAccountInfoResult.UserInfo.TitleInfo.TitlePlayerAccount.Type, "JoinTimeStamp": Math.floor(+new Date() / 1000) });
            guildCacheData.MemberCacheDataList.push({
                "Id": currentPlayerId,
                "Name": userName,
                "StageProgress": userStageProgress,
                "LastTimeStamp": Math.floor(+new Date() / (1000 * 60 * 60)),
                "TotalContribution": 0,
                "RaidScore": 0,
                "RaidSeason": -1
            });
            for (let i = 0; i < guildMemberData.MemberList.length; i++) {
                if (guildCacheData.MemberCacheDataList.find(n => n.Id == guildMemberData.MemberList[i].Id) == null) {
                    guildCacheData.MemberCacheDataList.push({
                        "Id": guildMemberData.MemberList[i].Id,
                        "Name": "???",
                        "Level": -1,
                        "LastTimeStamp": -1,
                        "TotalContribution": 0,
                        "RaidScore": 0,
                        "RaidSeason": -1
                    });
                }
            }
            let newCacheDataList = [];
            for (let i = 0; i < guildMemberData.MemberList.length; i++) {
                newCacheDataList.push(guildCacheData.MemberCacheDataList.find(n => n.Id == guildMemberData.MemberList[i].Id));
            }
            guildCacheData.MemberCacheDataList = newCacheDataList;
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "guild_id": guildId,
                    "apply_guild_list": unescape(JSON.stringify(applyGuildData))
                }
            });
            let updateGuildMasterReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: guildId,
                Data: {
                    "guild_member_list": unescape(JSON.stringify(guildMemberData)),
                    "guild_cache_list": unescape(JSON.stringify(guildCacheData))
                }
            });
            UpdateGuildRecommendationScore(GetServerIdModifierFromServerId(guildManagementData.ServerId), guildManagementData.GuildId, parseInt(guildManagementData.TimestampDay), guildMemberData.MemberList.length);
            toReturn.guildManagementData = guildManagementData;
            toReturn.memberCacheDataList = { "MemberCacheDataList": guildCacheData.MemberCacheDataList };
            toReturn.guildLevel = guildLevelInfo.level;
            toReturn.guildExp = guildLevelInfo.exp;
            toReturn.guildNextExp = guildLevelInfo.nextExp;
            toReturn.dailyLimitFreeDonateLeft = GetLeftDailyLimit("guild_free_donate", "0");
            toReturn.dailyLimitDonateLeft = GetLeftDailyLimit("guild_donate", "0");
            toReturn.error = false;
            return toReturn;
        }
        else
            return toReturn;
    }
    else
        return toReturn;
};
handlers["guildJoin"] = GuildJoin;
let GuildPrivateJoin = function (args, context) {
    let toReturn = { targetGuildId: "", applyGuildList: {}, reasonOfError: "unknown", error: true };
    let guildId = "";
    let userName = "";
    let userStageProgress = "";
    if (args.guildId)
        guildId = args.guildId;
    if (args.userName)
        userName = args.userName;
    if (args.userStageProgress)
        userStageProgress = args.userStageProgress;
    toReturn.targetGuildId = guildId;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_id", "apply_guild_list"]
    });
    if (getUserReadOnlyDataResult.Data["guild_id"] != null) {
        log.info("User already have joined a guild. Guild Id: " + getUserReadOnlyDataResult.Data["guild_id"].Value);
        return toReturn;
    }
    let getGuildMasterReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: guildId,
        Keys: ["guild_management_data", "guild_applicant_list"]
    });
    if (getGuildMasterReadOnlyDataResult.Data["guild_management_data"] != null && getGuildMasterReadOnlyDataResult.Data["guild_applicant_list"] != null) {
        let guildManagementData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_management_data"].Value);
        let guildApplicantData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_applicant_list"].Value);
        let getPlayerTagsResult = server.GetPlayerTags({
            PlayFabId: currentPlayerId
        });
        let serverId = "";
        if (getPlayerTagsResult != null && getPlayerTagsResult.Tags.length > 0)
            serverId = GetServerIdFromTag(getPlayerTagsResult.Tags.filter(n => n.indexOf("Server") >= 0)[0]);
        if (serverId != guildManagementData.ServerId) {
            log.info("Server is not same!");
            return toReturn;
        }
        if (guildManagementData.IsPrivate) {
            if (guildApplicantData.ApplicantList.length > 7) {
                log.info("Guild applicant count is full!");
                toReturn.reasonOfError = "guild_applicant_full";
                return toReturn;
            }
            if (guildManagementData.KickList.find(n => n.Id == currentPlayerId)) {
                log.info("This user's id include in kick list!");
                toReturn.reasonOfError = "guild_banned";
                return toReturn;
            }
            if (guildManagementData.RequiredStageProgress > userStageProgress) {
                log.info("User's stage progress is not enough for join this guild!");
                toReturn.reasonOfError = "guild_stage_not_enough";
                return toReturn;
            }
            let applyGuildData = JSON.parse("{\"ApplyGuildList\":[]}");
            if (getUserReadOnlyDataResult.Data["apply_guild_list"] != null) {
                applyGuildData = JSON.parse(getUserReadOnlyDataResult.Data["apply_guild_list"].Value);
                if (applyGuildData.ApplyGuildList.length > 0) {
                    let getAppliedGuildReadOnlyDataResult = server.GetUserReadOnlyData({
                        PlayFabId: applyGuildData.ApplyGuildList[0],
                        Keys: ["guild_management_data"]
                    });
                    if (getAppliedGuildReadOnlyDataResult.Data["guild_management_data"] != null) {
                        let appliedGuildManagementData = JSON.parse(getAppliedGuildReadOnlyDataResult.Data["guild_management_data"].Value);
                        if (appliedGuildManagementData.IsPrivate) {
                            toReturn.reasonOfError = "guild_applied_guild_exist";
                            return toReturn;
                        }
                        else
                            applyGuildData.ApplyGuildList = applyGuildData.ApplyGuildList.filter(n => n.toUpperCase() != guildId);
                    }
                    else
                        applyGuildData.ApplyGuildList = applyGuildData.ApplyGuildList.filter(n => n.toUpperCase() != guildId);
                }
            }
            let isDuplicated = false;
            for (let i = 0; i < guildApplicantData.ApplicantList.length; i++) {
                if (guildApplicantData.ApplicantList[i].Id == currentPlayerId) {
                    guildApplicantData.ApplicantList[i].Name = userName;
                    guildApplicantData.ApplicantList[i].StageProgress = userStageProgress;
                    isDuplicated = true;
                    break;
                }
            }
            if (!isDuplicated)
                guildApplicantData.ApplicantList.push({ "Id": currentPlayerId, "Name": userName, "StageProgress": userStageProgress });
            applyGuildData.ApplyGuildList.push(guildId);
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "apply_guild_list": unescape(JSON.stringify(applyGuildData))
                }
            });
            let updateGuildMasterReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: guildId,
                Data: {
                    "guild_applicant_list": unescape(JSON.stringify(guildApplicantData))
                }
            });
            toReturn.applyGuildList = { "ApplyGuildList": applyGuildData.ApplyGuildList };
            toReturn.error = false;
            return toReturn;
        }
        else
            return toReturn;
    }
    else
        return toReturn;
};
handlers["guildPrivateJoin"] = GuildPrivateJoin;
let GuildPrivateJoinCancel = function (args, context) {
    let toReturn = { targetGuildId: "", applyGuildList: {}, error: true };
    let guildId = "";
    let applyGuildData = JSON.parse("{\"ApplyGuildList\":[]}");
    if (args.guildId)
        guildId = args.guildId;
    toReturn.targetGuildId = guildId;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_id", "apply_guild_list"]
    });
    if (getUserReadOnlyDataResult.Data["guild_id"] != null) {
        log.info("User already have joined a guild. Guild Id: " + getUserReadOnlyDataResult.Data["guild_id"].Value);
        return toReturn;
    }
    if (getUserReadOnlyDataResult.Data["apply_guild_list"] != null) {
        applyGuildData = JSON.parse(getUserReadOnlyDataResult.Data["apply_guild_list"].Value);
        if (!applyGuildData.ApplyGuildList.find(n => n.toUpperCase() == guildId)) {
            log.info("Apply guild id is not exist!");
            return toReturn;
        }
    }
    let getGuildMasterReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: guildId,
        Keys: ["guild_applicant_list"]
    });
    if (getGuildMasterReadOnlyDataResult.Data["guild_applicant_list"] != null) {
        let guildApplicantData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_applicant_list"].Value);
        guildApplicantData.ApplicantList = guildApplicantData.ApplicantList.filter(n => n.Id != currentPlayerId);
        applyGuildData.ApplyGuildList = applyGuildData.ApplyGuildList.filter(n => n.toUpperCase() != guildId);
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "apply_guild_list": unescape(JSON.stringify(applyGuildData))
            }
        });
        let updateGuildMasterReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: guildId,
            Data: {
                "guild_applicant_list": unescape(JSON.stringify(guildApplicantData))
            }
        });
        toReturn.applyGuildList = { "ApplyGuildList": applyGuildData.ApplyGuildList };
        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;
};
handlers["guildPrivateJoinCancel"] = GuildPrivateJoinCancel;
let GuildLeave = function (args, context) {
    let toReturn = { error: true };
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_id"]
    });
    if (getUserReadOnlyDataResult.Data["guild_id"] != null) {
        if (getUserReadOnlyDataResult.Data["guild_id"].Value == currentPlayerId) {
            log.info("Guild Master can't leave guild!");
            return toReturn;
        }
        let getGuildMasterReadOnlyDataResult = server.GetUserReadOnlyData({
            PlayFabId: getUserReadOnlyDataResult.Data["guild_id"].Value,
            Keys: ["guild_management_data", "guild_member_list", "guild_cache_list"]
        });
        if (getGuildMasterReadOnlyDataResult.Data["guild_management_data"] != null && getGuildMasterReadOnlyDataResult.Data["guild_member_list"] != null && getGuildMasterReadOnlyDataResult.Data["guild_cache_list"] != null) {
            let guildManagementData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_management_data"].Value);
            let guildMemberData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_member_list"].Value);
            let guildCacheData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_cache_list"].Value);
            let targetMemberInfo = guildMemberData.MemberList.find(n => n.Id == currentPlayerId);
            if (guildMemberData.MemberList == null)
                return toReturn;
            guildMemberData.MemberList = guildMemberData.MemberList.filter(n => n.Id != currentPlayerId);
            guildCacheData.MemberCacheDataList = guildCacheData.MemberCacheDataList.filter(n => n.Id != currentPlayerId);
            let updateGuildMasterReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: getUserReadOnlyDataResult.Data["guild_id"].Value,
                Data: {
                    "guild_member_list": unescape(JSON.stringify(guildMemberData)),
                    "guild_cache_list": unescape(JSON.stringify(guildCacheData))
                }
            });
            UpdateGuildRecommendationScore(GetServerIdModifierFromServerId(guildManagementData.ServerId), guildManagementData.GuildId, parseInt(guildManagementData.TimestampDay), guildMemberData.MemberList.length);
            let lastGuildLeaveTimeStamp = Math.floor(+new Date() / 1000);
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "guild_id": null,
                    "guild_raid_reward_last_played_season": null,
                    "last_guild_leave_timestamp": lastGuildLeaveTimeStamp.toString()
                }
            });
            toReturn.error = false;
            return toReturn;
        }
        else
            return toReturn;
    }
    else
        return toReturn;
};
handlers["guildLeave"] = GuildLeave;
let GuildRefresh = function (args, context) {
    let toReturn = { guildManagementData: {}, guildLevel: 1, guildExp: 0, guildNextExp: 0, memberList: {}, applicantList: {},
        dailyLimitFreeDonateLeft: 0, dailyLimitDonateLeft: 0, serverTimeStamp: 0, error: true };
    let userName = "";
    let userStageProgress = 1;
    let skipUpdateCache = false;
    if (args.userName)
        userName = args.userName;
    if (args.userStageProgress)
        userStageProgress = args.userStageProgress;
    if (args.skipUpdateCache)
        skipUpdateCache = args.skipUpdateCache;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_id", "last_guild_leave_timestamp"]
    });
    if (getUserReadOnlyDataResult.Data["guild_id"] != null) {
        let getGuildMasterReadOnlyDataResult = server.GetUserReadOnlyData({
            PlayFabId: getUserReadOnlyDataResult.Data["guild_id"].Value,
            Keys: ["guild_management_data", "guild_member_list", "guild_cache_list", "guild_applicant_list", "guild_total_exp", "guild_master_exp"]
        });
        if (getGuildMasterReadOnlyDataResult.Data["guild_management_data"] != null && getGuildMasterReadOnlyDataResult.Data["guild_member_list"] != null
            && getGuildMasterReadOnlyDataResult.Data["guild_cache_list"] != null && getGuildMasterReadOnlyDataResult.Data["guild_applicant_list"] != null) {
            let guildManagementData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_management_data"].Value);
            let guildMemberData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_member_list"].Value);
            let guildCacheData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_cache_list"].Value);
            let guildApplicantData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_applicant_list"].Value);
            let myData = guildMemberData.MemberList.find(n => n.Id == currentPlayerId);
            if (myData != null) {
                for (let i = 0; i < guildMemberData.MemberList.length; i++) {
                    if (guildCacheData.MemberCacheDataList.find(n => n.Id == guildMemberData.MemberList[i].Id) == null) {
                        guildCacheData.MemberCacheDataList.push({
                            "Id": guildMemberData.MemberList[i].Id,
                            "Name": "???",
                            "Level": -1,
                            "LastTimeStamp": -1,
                            "TotalContribution": 0,
                            "RaidScore": 0,
                            "RaidSeason": -1
                        });
                    }
                }
                let newCacheDataList = [];
                for (let i = 0; i < guildMemberData.MemberList.length; i++) {
                    newCacheDataList.push(guildCacheData.MemberCacheDataList.find(n => n.Id == guildMemberData.MemberList[i].Id));
                }
                guildCacheData.MemberCacheDataList = newCacheDataList;
                let changesMadeForCache = false;
                let myCacheData = guildCacheData.MemberCacheDataList.find(n => n.Id == myData.Id);
                if (myCacheData.Name != userName)
                    changesMadeForCache = true;
                myCacheData.Name = userName;
                if (myCacheData.StageProgress != userStageProgress)
                    changesMadeForCache = true;
                myCacheData.StageProgress = userStageProgress;
                if (myCacheData.LastTimeStamp != Math.floor(+new Date() / (1000 * 60 * 60)))
                    changesMadeForCache = true;
                myCacheData.LastTimeStamp = Math.floor(+new Date() / (1000 * 60 * 60));
                let serverIdModifier = GetServerIdModifier(currentPlayerId);
                let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
                    PlayFabId: currentPlayerId,
                    StatisticName: "guild_raid" + serverIdModifier,
                    MaxResultsCount: 1
                });
                let season = getLeaderboardAroundUserResult.Version;
                for (let i = 0; i < guildCacheData.MemberCacheDataList.length; i++) {
                    if (guildCacheData.MemberCacheDataList[i].RaidSeason != season) {
                        guildCacheData.MemberCacheDataList[i].RaidScore = 0;
                        changesMadeForCache = true;
                    }
                    guildCacheData.MemberCacheDataList[i].RaidSeason = season;
                }
                if (!skipUpdateCache && changesMadeForCache) {
                    let updateGuildMasterReadOnlyDataResult = server.UpdateUserReadOnlyData({
                        PlayFabId: getUserReadOnlyDataResult.Data["guild_id"].Value,
                        Data: {
                            "guild_cache_list": unescape(JSON.stringify(guildCacheData))
                        }
                    });
                }
                let levelInfo = GetGuildLevelAndExp(getGuildMasterReadOnlyDataResult.Data["guild_total_exp"], getGuildMasterReadOnlyDataResult.Data["guild_master_exp"], null);
                if (levelInfo.error)
                    return toReturn;
                toReturn.guildManagementData = guildManagementData;
                toReturn.guildLevel = levelInfo.level;
                toReturn.guildExp = levelInfo.exp;
                toReturn.guildNextExp = levelInfo.nextExp;
                toReturn.memberList = { "MemberCacheDataList": guildCacheData.MemberCacheDataList };
                if (currentPlayerId == getUserReadOnlyDataResult.Data["guild_id"].Value)
                    toReturn.applicantList = { "ApplicantList": guildApplicantData.ApplicantList };
                else
                    toReturn.applicantList = { "ApplicantList": [] };
                toReturn.dailyLimitFreeDonateLeft = GetLeftDailyLimit("guild_free_donate", "0");
                toReturn.dailyLimitDonateLeft = GetLeftDailyLimit("guild_donate", "0");
                toReturn.serverTimeStamp = Math.floor(+new Date() / 1000);
                toReturn.error = false;
                return toReturn;
            }
            else {
                log.info("user not exist in member list");
                let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                    PlayFabId: currentPlayerId,
                    Data: {
                        "guild_id": null,
                        "guild_raid_reward_last_played_season": null
                    }
                });
                toReturn.guildManagementData = null;
                toReturn.serverTimeStamp = Math.floor(+new Date() / 1000);
                toReturn.error = true;
                return toReturn;
            }
        }
        else {
            log.info("The guild master has data issues (Likely the guild ownership has been changed)");
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "guild_id": null,
                    "guild_raid_reward_last_played_season": null
                }
            });
            toReturn.guildManagementData = null;
            toReturn.serverTimeStamp = Math.floor(+new Date() / 1000);
            toReturn.error = true;
            return toReturn;
        }
    }
    else {
        log.info("User don't have any guild!");
        toReturn.guildManagementData = null;
        toReturn.serverTimeStamp = Math.floor(+new Date() / 1000);
        toReturn.error = false;
        return toReturn;
    }
};
handlers["guildRefresh"] = GuildRefresh;
let GuildSearchWithId = function (args, context) {
    let toReturn = { targetGuildData: {}, applyGuildList: {}, error: true };
    let guildItemList = [];
    let guildId = "";
    if (args.guildId)
        guildId = args.guildId;
    if (guildId == "")
        return toReturn;
    let userServerIdModifier = GetServerIdModifier(currentPlayerId);
    let serverIdModifier = GetServerIdModifier(guildId);
    if (userServerIdModifier != serverIdModifier)
        return toReturn;
    let getGuildMasterReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: guildId,
        Keys: ["guild_management_data", "guild_member_list", "guild_total_exp", "guild_master_exp"]
    });
    if (getGuildMasterReadOnlyDataResult.Data["guild_management_data"] != null && getGuildMasterReadOnlyDataResult.Data["guild_member_list"] != null) {
        let guildManagementData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_management_data"].Value);
        let guildMemberIdData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_member_list"].Value);
        let guildLevelInfo = GetGuildLevelAndExp(getGuildMasterReadOnlyDataResult.Data["guild_total_exp"], getGuildMasterReadOnlyDataResult.Data["guild_master_exp"], null);
        let guildRanking = 0;
        if (guildLevelInfo.error)
            return toReturn;
        let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
            PlayFabId: guildId,
            StatisticName: "guild_raid" + serverIdModifier,
            MaxResultsCount: 1
        });
        guildRanking = getLeaderboardAroundUserResult.Leaderboard[0].Position;
        guildItemList.push({
            "GuildId": guildId.toUpperCase(),
            "GuildName": guildManagementData.GuildName,
            "GuildNotice": guildManagementData.GuildNotice,
            "GuildRanking": guildRanking,
            "GuildLevel": guildLevelInfo.level,
            "GuildRequiredStageProgress": guildManagementData.RequiredStageProgress,
            "GuildFlagIndex": guildManagementData.GuildFlagIndex,
            "GuildMemberCount": guildMemberIdData.MemberList.length,
            "IsPrivate": guildManagementData.IsPrivate
        });
        let getMyReadOnlyDataResult = server.GetUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Keys: ["apply_guild_list"]
        });
        if (getMyReadOnlyDataResult.Data["apply_guild_list"] != null) {
            let applyGuildData = JSON.parse(getMyReadOnlyDataResult.Data["apply_guild_list"].Value);
            toReturn.applyGuildList = { "ApplyGuildList": applyGuildData.ApplyGuildList };
        }
        else {
            let applyGuildData = JSON.parse("{\"ApplyGuildList\":[]}");
            toReturn.applyGuildList = { "ApplyGuildList": applyGuildData.ApplyGuildList };
        }
        toReturn.targetGuildData = { "GuildDataList": guildItemList };
        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;
};
handlers["guildSearchWithId"] = GuildSearchWithId;
let GuildGetRecommendation = function (args, context) {
    let toReturn = { myGuildId: "", guildList: {}, applyGuildList: {}, leftGuildJoinTime: 0, error: true };
    let guildItemList = [];
    let lastGuildLeaveTimeStamp = 0;
    let maxGuildList = 5;
    let getMyReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_id", "apply_guild_list", "last_guild_leave_timestamp"]
    });
    if (getMyReadOnlyDataResult.Data["last_guild_leave_timestamp"] != null)
        lastGuildLeaveTimeStamp = parseInt(getMyReadOnlyDataResult.Data["last_guild_leave_timestamp"].Value);
    let serverTimeStamp = Math.floor(+new Date() / 1000);
    toReturn.leftGuildJoinTime = (lastGuildLeaveTimeStamp + 172800) - serverTimeStamp;
    if (toReturn.leftGuildJoinTime > 0) {
        toReturn.error = false;
        return toReturn;
    }
    let serverIdModifier = GetServerIdModifier(currentPlayerId);
    let getLeaderboardResult = server.GetLeaderboard({
        StatisticName: "guild_recommend" + serverIdModifier,
        MaxResultsCount: 50,
        StartPosition: 0
    });
    let filteredList = getLeaderboardResult.Leaderboard.filter(n => n.StatValue > 0);
    ShuffleArray(filteredList);
    if (getMyReadOnlyDataResult.Data["apply_guild_list"] != null) {
        let applyGuildData = JSON.parse(getMyReadOnlyDataResult.Data["apply_guild_list"].Value);
        toReturn.applyGuildList = { "ApplyGuildList": applyGuildData.ApplyGuildList };
        if (applyGuildData.ApplyGuildList.length > 0) {
            let getMyAppliedGuildLeaderboardResult = server.GetLeaderboardAroundUser({
                PlayFabId: applyGuildData.ApplyGuildList[0],
                StatisticName: "guild_recommend" + serverIdModifier,
                MaxResultsCount: 1
            });
            if (getMyAppliedGuildLeaderboardResult.Leaderboard.length > 0) {
                let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
                    PlayFabId: getMyAppliedGuildLeaderboardResult.Leaderboard[0].PlayFabId,
                    Keys: ["guild_management_data", "guild_member_list", "guild_total_exp", "guild_master_exp"]
                });
                if (getUserReadOnlyDataResult.Data["guild_management_data"] != null && getUserReadOnlyDataResult.Data["guild_member_list"] != null) {
                    let guildManagementData = JSON.parse(getUserReadOnlyDataResult.Data["guild_management_data"].Value);
                    let guildMemberIdData = JSON.parse(getUserReadOnlyDataResult.Data["guild_member_list"].Value);
                    let guildLevelInfo = GetGuildLevelAndExp(getUserReadOnlyDataResult.Data["guild_total_exp"], getUserReadOnlyDataResult.Data["guild_master_exp"], null);
                    let guildRanking = -1;
                    if (guildLevelInfo.error)
                        return toReturn;
                    let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
                        PlayFabId: guildManagementData.GuildId,
                        StatisticName: "guild_raid" + serverIdModifier,
                        MaxResultsCount: 1
                    });
                    if (getLeaderboardAroundUserResult.Leaderboard[0].StatValue > 0)
                        guildRanking = getLeaderboardAroundUserResult.Leaderboard[0].Position;
                    guildItemList.push({
                        "GuildId": guildManagementData.GuildId,
                        "GuildName": guildManagementData.GuildName,
                        "GuildNotice": guildManagementData.GuildNotice,
                        "GuildRanking": guildRanking,
                        "GuildLevel": guildLevelInfo.level,
                        "GuildRequiredStageProgress": guildManagementData.RequiredStageProgress,
                        "GuildFlagIndex": guildManagementData.GuildFlagIndex,
                        "GuildMemberCount": guildMemberIdData.MemberList.length,
                        "IsPrivate": guildManagementData.IsPrivate
                    });
                    maxGuildList = maxGuildList - 1;
                    filteredList = filteredList.filter(n => n.PlayFabId != guildManagementData.GuildId);
                }
            }
        }
    }
    else {
        let applyGuildData = JSON.parse("{\"ApplyGuildList\":[]}");
        toReturn.applyGuildList = { "ApplyGuildList": applyGuildData.ApplyGuildList };
    }
    for (let i = 0; i < Math.min(maxGuildList, filteredList.length); i++) {
        let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
            PlayFabId: filteredList[i].PlayFabId,
            Keys: ["guild_management_data", "guild_member_list", "guild_total_exp", "guild_master_exp"]
        });
        if (getUserReadOnlyDataResult.Data["guild_management_data"] != null && getUserReadOnlyDataResult.Data["guild_member_list"] != null) {
            let guildManagementData = JSON.parse(getUserReadOnlyDataResult.Data["guild_management_data"].Value);
            let guildMemberIdData = JSON.parse(getUserReadOnlyDataResult.Data["guild_member_list"].Value);
            let guildLevelInfo = GetGuildLevelAndExp(getUserReadOnlyDataResult.Data["guild_total_exp"], getUserReadOnlyDataResult.Data["guild_master_exp"], null);
            let guildRanking = -1;
            if (guildLevelInfo.error)
                return toReturn;
            let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
                PlayFabId: guildManagementData.GuildId,
                StatisticName: "guild_raid" + serverIdModifier,
                MaxResultsCount: 1
            });
            if (getLeaderboardAroundUserResult.Leaderboard[0].StatValue == 0) {
                server.UpdatePlayerStatistics({
                    PlayFabId: guildManagementData.GuildId,
                    Statistics: [
                        {
                            "StatisticName": "guild_recommend" + serverIdModifier,
                            "Value": 1
                        }
                    ]
                });
            }
            if (getLeaderboardAroundUserResult.Leaderboard[0].StatValue > 0)
                guildRanking = getLeaderboardAroundUserResult.Leaderboard[0].Position;
            guildItemList.push({
                "GuildId": guildManagementData.GuildId,
                "GuildName": guildManagementData.GuildName,
                "GuildNotice": guildManagementData.GuildNotice,
                "GuildRanking": guildRanking,
                "GuildLevel": guildLevelInfo.level,
                "GuildRequiredStageProgress": guildManagementData.RequiredStageProgress,
                "GuildFlagIndex": guildManagementData.GuildFlagIndex,
                "GuildMemberCount": guildMemberIdData.MemberList.length,
                "IsPrivate": guildManagementData.IsPrivate
            });
        }
    }
    if (getMyReadOnlyDataResult.Data["guild_id"] != null)
        toReturn.myGuildId = getMyReadOnlyDataResult.Data["guild_id"].Value;
    toReturn.error = false;
    toReturn.guildList = { "GuildDataList": guildItemList };
    return toReturn;
};
handlers["guildGetRecommendation"] = GuildGetRecommendation;
let GuildFreeDonate = function (args, context) {
    let toReturn = { addedExp: 0, updatedMemberTotalExp: 0, guildLevel: 1, guildExp: 0, guildNextExp: 0, dailyLimitDonateLeft: 0, reasonOfError: "unknown", error: true };
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_id"]
    });
    if (getUserReadOnlyDataResult.Data["guild_id"] != null) {
        let guildId = getUserReadOnlyDataResult.Data["guild_id"].Value;
        let getGuildMasterReadOnlyDataResult = server.GetUserReadOnlyData({
            PlayFabId: guildId,
            Keys: ["guild_total_exp", "guild_master_exp", "guild_cache_list", "guild_member_list"]
        });
        if (getGuildMasterReadOnlyDataResult.Data["guild_cache_list"] != null && getGuildMasterReadOnlyDataResult.Data["guild_member_list"] != null) {
            let CheckGuildJoinTimeResult = CheckGuildJoinTime(guildId, getGuildMasterReadOnlyDataResult.Data["guild_member_list"]);
            if (CheckGuildJoinTimeResult.error)
                return toReturn;
            if (!CheckGuildJoinTimeResult.isTimeOver) {
                toReturn.reasonOfError = "guild_join_time_not_enough";
                return toReturn;
            }
            let guildCacheData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_cache_list"].Value);
            let myCacheData = guildCacheData.MemberCacheDataList.find(n => n.Id == currentPlayerId);
            if (myCacheData == null)
                return toReturn;
            let getTitleDataResult = server.GetTitleData({
                Keys: [
                    "balance_guild_exp"
                ]
            }).Data;
            let memberTotalExp = 0;
            if (getGuildMasterReadOnlyDataResult.Data["guild_total_exp"] != null)
                memberTotalExp = parseInt(getGuildMasterReadOnlyDataResult.Data["guild_total_exp"].Value);
            let masterExp = 0;
            if (getGuildMasterReadOnlyDataResult.Data["guild_master_exp"] != null)
                masterExp = parseInt(getGuildMasterReadOnlyDataResult.Data["guild_master_exp"].Value);
            let currentGuildLevelInfo = GetGuildLevelAndExp(getGuildMasterReadOnlyDataResult.Data["guild_total_exp"], getGuildMasterReadOnlyDataResult.Data["guild_master_exp"], getTitleDataResult["balance_guild_exp"]);
            if (currentGuildLevelInfo.error)
                return toReturn;
            if (currentGuildLevelInfo.isMaxLevel) {
                log.info("Guild level is max level!");
                return toReturn;
            }
            let addedExp = 10;
            if (myCacheData.TotalContribution != null)
                myCacheData.TotalContribution = myCacheData.TotalContribution + addedExp;
            else
                myCacheData.TotalContribution = addedExp;
            memberTotalExp = memberTotalExp + addedExp;
            getGuildMasterReadOnlyDataResult.Data["guild_total_exp"].Value = memberTotalExp.toString();
            let updatedGuildLevelInfo = GetGuildLevelAndExp(getGuildMasterReadOnlyDataResult.Data["guild_total_exp"], getGuildMasterReadOnlyDataResult.Data["guild_master_exp"], getTitleDataResult["balance_guild_exp"]);
            if (!UseDailyLimitIfAvail("guild_free_donate", "0", 1)) {
                log.info("Exceeded daily limit for free donate!");
                return toReturn;
            }
            let updateGuildMasterReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: guildId,
                Data: {
                    "guild_total_exp": memberTotalExp.toString(),
                    "guild_cache_list": unescape(JSON.stringify(guildCacheData))
                }
            });
            toReturn.updatedMemberTotalExp = memberTotalExp;
            toReturn.addedExp = addedExp;
            toReturn.guildLevel = updatedGuildLevelInfo.level;
            toReturn.guildExp = updatedGuildLevelInfo.exp;
            toReturn.guildNextExp = updatedGuildLevelInfo.nextExp;
            toReturn.error = false;
            return toReturn;
        }
        else
            return toReturn;
    }
    else
        return toReturn;
};
handlers["guildFreeDonate"] = GuildFreeDonate;
let GuildDonate = function (args, context) {
    let toReturn = { donateAmount: 0, addedExp: 0, updatedMemberTotalExp: 0, guildLevel: 1, guildExp: 0, guildNextExp: 0, dailyLimitDonateLeft: 0, reasonOfError: "unknown", error: true };
    let donateAmount = 0;
    if (args.donateAmount)
        donateAmount = args.donateAmount;
    if (donateAmount < 100 || donateAmount > 1000)
        return toReturn;
    toReturn.donateAmount = donateAmount;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_id"]
    });
    if (getUserReadOnlyDataResult.Data["guild_id"] != null) {
        let guildId = getUserReadOnlyDataResult.Data["guild_id"].Value;
        let getGuildMasterReadOnlyDataResult = server.GetUserReadOnlyData({
            PlayFabId: guildId,
            Keys: ["guild_total_exp", "guild_master_exp", "guild_cache_list", "guild_member_list"]
        });
        if (getGuildMasterReadOnlyDataResult.Data["guild_cache_list"] != null && getGuildMasterReadOnlyDataResult.Data["guild_member_list"] != null) {
            let CheckGuildJoinTimeResult = CheckGuildJoinTime(guildId, getGuildMasterReadOnlyDataResult.Data["guild_member_list"]);
            if (CheckGuildJoinTimeResult.error)
                return toReturn;
            if (!CheckGuildJoinTimeResult.isTimeOver) {
                toReturn.reasonOfError = "guild_join_time_not_enough";
                return toReturn;
            }
            let guildCacheData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_cache_list"].Value);
            let myCacheData = guildCacheData.MemberCacheDataList.find(n => n.Id == currentPlayerId);
            if (myCacheData == null)
                return toReturn;
            let getTitleDataResult = server.GetTitleData({
                Keys: [
                    "balance_guild_exp"
                ]
            }).Data;
            let memberTotalExp = 0;
            if (getGuildMasterReadOnlyDataResult.Data["guild_total_exp"] != null)
                memberTotalExp = parseInt(getGuildMasterReadOnlyDataResult.Data["guild_total_exp"].Value);
            let masterExp = 0;
            if (getGuildMasterReadOnlyDataResult.Data["guild_master_exp"] != null)
                masterExp = parseInt(getGuildMasterReadOnlyDataResult.Data["guild_master_exp"].Value);
            let currentGuildLevelInfo = GetGuildLevelAndExp(getGuildMasterReadOnlyDataResult.Data["guild_total_exp"], getGuildMasterReadOnlyDataResult.Data["guild_master_exp"], getTitleDataResult["balance_guild_exp"]);
            if (currentGuildLevelInfo.error)
                return toReturn;
            if (currentGuildLevelInfo.isMaxLevel) {
                log.info("Guild level is max level!");
                return toReturn;
            }
            if (!UseDailyLimitIfAvail("guild_donate", "0", 1)) {
                log.info("Exceeded daily limit for donate!");
                return toReturn;
            }
            let addedExp = 0;
            SubtractVirtualCurrency("GE", donateAmount);
            addedExp = Math.floor(donateAmount / 10);
            if (myCacheData.TotalContribution != null)
                myCacheData.TotalContribution = myCacheData.TotalContribution + addedExp;
            else
                myCacheData.TotalContribution = addedExp;
            memberTotalExp = memberTotalExp + addedExp;
            getGuildMasterReadOnlyDataResult.Data["guild_total_exp"].Value = memberTotalExp.toString();
            let updatedGuildLevelInfo = GetGuildLevelAndExp(getGuildMasterReadOnlyDataResult.Data["guild_total_exp"], getGuildMasterReadOnlyDataResult.Data["guild_master_exp"], getTitleDataResult["balance_guild_exp"]);
            let updateGuildMasterReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: guildId,
                Data: {
                    "guild_total_exp": memberTotalExp.toString(),
                    "guild_cache_list": unescape(JSON.stringify(guildCacheData))
                }
            });
            toReturn.updatedMemberTotalExp = memberTotalExp;
            toReturn.addedExp = addedExp;
            toReturn.guildLevel = updatedGuildLevelInfo.level;
            toReturn.guildExp = updatedGuildLevelInfo.exp;
            toReturn.guildNextExp = updatedGuildLevelInfo.nextExp;
            toReturn.error = false;
            return toReturn;
        }
        else
            return toReturn;
    }
    else
        return toReturn;
};
handlers["guildDonate"] = GuildDonate;
let GuildGetRank = function (args, context) {
    let toReturn = { error: true, guildRankList: [], guildNameList: [], guildManagementData: [], myGuildMasterName: "", myGuildRank: -1, myGuildScore: 0 };
    let serverIdModifier = GetServerIdModifier(currentPlayerId);
    let getLeaderboardResult = server.GetLeaderboard({
        StatisticName: "guild_raid" + serverIdModifier,
        MaxResultsCount: 10,
        StartPosition: 0
    });
    for (let i = 0; i < getLeaderboardResult.Leaderboard.length; i++) {
        if (getLeaderboardResult.Leaderboard[i].StatValue > 0) {
            let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
                PlayFabId: getLeaderboardResult.Leaderboard[i].PlayFabId,
                Keys: ["guild_management_data"]
            });
            if (getUserReadOnlyDataResult.Data["guild_management_data"] != null) {
                let guildManagemnetData = JSON.parse(getUserReadOnlyDataResult.Data["guild_management_data"].Value);
                toReturn.guildNameList.push(guildManagemnetData.GuildName);
                toReturn.guildRankList.push(getLeaderboardResult.Leaderboard[i]);
            }
        }
    }
    let myGuildId = "";
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_id"]
    });
    if (getUserReadOnlyDataResult.Data["guild_id"] != null)
        myGuildId = getUserReadOnlyDataResult.Data["guild_id"].Value;
    if (myGuildId != "") {
        let getGuildMasterReadOnlyDataResult = server.GetUserReadOnlyData({
            PlayFabId: myGuildId,
            Keys: ["guild_management_data", "guild_cache_list"]
        });
        if (getGuildMasterReadOnlyDataResult.Data["guild_management_data"] != null && getGuildMasterReadOnlyDataResult.Data["guild_cache_list"] != null) {
            let guildManagemnetData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_management_data"].Value);
            toReturn.guildManagementData = guildManagemnetData;
            let getLeaderboardResult = server.GetLeaderboardAroundUser({
                PlayFabId: myGuildId,
                StatisticName: "guild_raid" + serverIdModifier,
                MaxResultsCount: 1
            });
            if (getLeaderboardResult.Leaderboard.length > 0) {
                toReturn.myGuildMasterName = getLeaderboardResult.Leaderboard[0].DisplayName;
                toReturn.myGuildScore = getLeaderboardResult.Leaderboard[0].StatValue;
                if (toReturn.myGuildScore > 0)
                    toReturn.myGuildRank = getLeaderboardResult.Leaderboard[0].Position;
            }
            else {
                let guildCacheData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_cache_list"].Value);
                let guildMasterCacheData = guildCacheData.MemberCacheDataList.find(n => n.Id == myGuildId);
                if (guildMasterCacheData != null)
                    toReturn.myGuildMasterName = guildMasterCacheData.Name;
            }
            toReturn.error = false;
        }
    }
    else {
        toReturn.guildManagementData = null;
        toReturn.error = false;
    }
    return toReturn;
};
handlers["guildGetRank"] = GuildGetRank;
let GuildUpgradeRelic = function (args, context) {
    let toReturn = { targetGuildRelicId: "", upgradeCount: 0, guildRelicList: {}, addedBonusStat: 0, requiredGuildToken: 0, reasonOfError: "unknown", error: true };
    toReturn.upgradeCount = 1;
    if (args.targetGuildRelicId)
        toReturn.targetGuildRelicId = args.targetGuildRelicId;
    if (args.upgradeCount)
        toReturn.upgradeCount = args.upgradeCount;
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "balance_inventory_growth"
        ]
    }).Data;
    let balanceInventoryData = JSON.parse(getTitleDataResult["balance_inventory_growth"]);
    if (balanceInventoryData != null && balanceInventoryData.GuildRelicInfo != null) {
        let targetGuildRelicBalanceInfo = balanceInventoryData.GuildRelicInfo.find(n => n.Id == toReturn.targetGuildRelicId);
        if (targetGuildRelicBalanceInfo != null) {
            toReturn.requiredGuildToken = targetGuildRelicBalanceInfo.Price * toReturn.upgradeCount;
            let getUserInventoryResult = server.GetUserInventory({
                PlayFabId: currentPlayerId
            });
            let currentGuildTokenCount = 0;
            if (getUserInventoryResult.VirtualCurrency["GT"] != null)
                currentGuildTokenCount = getUserInventoryResult.VirtualCurrency["GT"];
            if (currentGuildTokenCount < toReturn.requiredGuildToken) {
                log.info("Not enough guild token!");
                return toReturn;
            }
            let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Keys: ["guild_relic_data"]
            });
            let guildRelicData = JSON.parse("{\"GuildRelicList\":[]}");
            if (getUserReadOnlyDataResult.Data["guild_relic_data"] != null)
                guildRelicData = JSON.parse(getUserReadOnlyDataResult.Data["guild_relic_data"].Value);
            let targetGuildRelicInfo = guildRelicData.GuildRelicList.find(n => n.Id == toReturn.targetGuildRelicId);
            if (targetGuildRelicInfo != null) {
                if (targetGuildRelicInfo.Level == targetGuildRelicBalanceInfo.MaxLevel) {
                    log.info("target guild relic level is max level!");
                    return toReturn;
                }
                else if ((targetGuildRelicInfo.Level + toReturn.upgradeCount) > targetGuildRelicBalanceInfo.MaxLevel) {
                    log.info("Upgraded level can't over max level!");
                    return toReturn;
                }
                targetGuildRelicInfo.Level = targetGuildRelicInfo.Level + toReturn.upgradeCount;
                if (targetGuildRelicInfo.Level >= targetGuildRelicBalanceInfo.MaxLevel)
                    targetGuildRelicInfo.IsMaxLevel = true;
                else
                    targetGuildRelicInfo.IsMaxLevel = false;
            }
            else {
                guildRelicData.GuildRelicList.push({ "Id": targetGuildRelicBalanceInfo.Id, "IsMaxLevel": false, "Level": toReturn.upgradeCount,
                    "RelicType": targetGuildRelicBalanceInfo.RelicType });
            }
            let updateGuildMasterReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "guild_relic_data": unescape(JSON.stringify(guildRelicData))
                }
            });
            if (targetGuildRelicBalanceInfo.Id == "guild_relic_03") {
                AddVirtualCurrency("BS", toReturn.upgradeCount);
                toReturn.addedBonusStat = toReturn.upgradeCount;
            }
            SubtractVirtualCurrency("GT", toReturn.requiredGuildToken);
            toReturn.guildRelicList = { "GuildRelicList": guildRelicData.GuildRelicList };
            toReturn.error = false;
            return toReturn;
        }
        else {
            log.info("targetGuildRelicBalanceInfo is null!");
            return toReturn;
        }
    }
    else {
        log.info("balanceInventoryData or balanceInventoryData.GuildRelicInfo is null!");
        return toReturn;
    }
};
handlers["guildUpgradeRelic"] = GuildUpgradeRelic;
function IsGuildMaster(guildManagementDataJson) {
    let isGuildMaster = false;
    if (guildManagementDataJson != null) {
        let guildManagementData = JSON.parse(guildManagementDataJson.Value);
        if (guildManagementData.GuildId == currentPlayerId)
            isGuildMaster = true;
    }
    return isGuildMaster;
}
function GetStageProgress(userDataJson) {
    let stageProgress = 1;
    if (userDataJson != null) {
        let userData = JSON.parse(userDataJson.Value);
        stageProgress = userData.StageProgress;
    }
    return stageProgress;
}
function GetGuildMemberLimitCount(guildLevel) {
    let guildMemberLimit = 8;
    if (guildLevel >= 15)
        guildMemberLimit = 15;
    else if (guildLevel >= 14)
        guildMemberLimit = 14;
    else if (guildLevel >= 13)
        guildMemberLimit = 13;
    else if (guildLevel >= 12)
        guildMemberLimit = 12;
    else if (guildLevel >= 9)
        guildMemberLimit = 11;
    else if (guildLevel >= 6)
        guildMemberLimit = 10;
    else if (guildLevel >= 3)
        guildMemberLimit = 9;
    return guildMemberLimit;
}
function GetGuildLevelAndExp(totalExpData, masterExpData, guildExpBalanceSheet) {
    let toReturn = { level: 0, exp: 0, nextExp: 0, isMaxLevel: false, error: true };
    if (totalExpData == null)
        return toReturn;
    if (guildExpBalanceSheet == null) {
        let getTitleDataResult = server.GetTitleData({
            Keys: [
                "balance_guild_exp"
            ]
        }).Data;
        if (getTitleDataResult["balance_guild_exp"] == null)
            return toReturn;
        guildExpBalanceSheet = getTitleDataResult["balance_guild_exp"];
    }
    let guildMasterExp = 0;
    if (masterExpData != null)
        guildMasterExp = parseInt(masterExpData.Value);
    let currentExp = parseInt(totalExpData.Value) + guildMasterExp;
    let guildExpBalanceData = JSON.parse(guildExpBalanceSheet);
    for (let i = 0; i < guildExpBalanceData.GuildExpTable.length; i++) {
        if (currentExp < guildExpBalanceData.GuildExpTable[i].TotalExp) {
            toReturn.level = guildExpBalanceData.GuildExpTable[i - 1].Level;
            if (toReturn.level == 1) {
                toReturn.exp = currentExp;
                toReturn.nextExp = guildExpBalanceData.GuildExpTable[i].TotalExp;
            }
            else {
                toReturn.exp = currentExp - guildExpBalanceData.GuildExpTable[i - 1].TotalExp;
                toReturn.nextExp = guildExpBalanceData.GuildExpTable[i].TotalExp - guildExpBalanceData.GuildExpTable[i - 1].TotalExp;
            }
            toReturn.error = false;
            break;
        }
        else {
            if (guildExpBalanceData.GuildExpTable[i].Level >= guildExpBalanceData.MaxGuildLevel) {
                toReturn.level = guildExpBalanceData.MaxGuildLevel;
                toReturn.exp = 0;
                toReturn.nextExp = 0;
                toReturn.isMaxLevel = true;
                toReturn.error = false;
                break;
            }
        }
    }
    return toReturn;
}
function UpdateGuildRecommendationScore(serverIdModifier, guildId, timestamp, guildMemberCount) {
    let guildTimeStampDay = Math.floor(timestamp / 86400);
    let currentTimestampDay = Math.floor(+new Date() / (1000 * 60 * 60 * 24));
    let score = Math.max(30 - Math.min(15, currentTimestampDay - guildTimeStampDay) - guildMemberCount, 1);
    server.UpdatePlayerStatistics({
        PlayFabId: guildId,
        Statistics: [
            {
                "StatisticName": "guild_recommend" + serverIdModifier,
                "Value": score
            }
        ]
    });
}
function ShuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}
function CheckGuildJoinTime(guildId, guildMemberDataJson) {
    let toReturn = { isTimeOver: false, error: true };
    if (guildMemberDataJson == null) {
        let getGuildMasterReadOnlyDataResult = server.GetUserReadOnlyData({
            PlayFabId: guildId,
            Keys: ["guild_member_list"]
        });
        if (getGuildMasterReadOnlyDataResult.Data["guild_member_list"] == null)
            return toReturn;
        guildMemberDataJson = getGuildMasterReadOnlyDataResult.Data["guild_member_list"];
    }
    let guildMemberData = JSON.parse(guildMemberDataJson.Value);
    let myMemberData = guildMemberData.MemberList.find(n => n.Id == currentPlayerId);
    if (myMemberData == null)
        return toReturn;
    if (myMemberData.JoinTimeStamp != null) {
        let currentTimestamp = Math.floor(+new Date() / 1000);
        if (currentTimestamp > (myMemberData.JoinTimeStamp + 86400))
            toReturn.isTimeOver = true;
        else
            log.info("This User is not over 24 hours after join this guild !");
    }
    else
        toReturn.isTimeOver = true;
    toReturn.error = false;
    return toReturn;
}
let GuildRaidRefresh = function (args, context) {
    let toReturn = { error: true, currentSeason: 0, dailyLimitLeft: 0, position: -1, score: 0,
        lastSeasonPosition: -1, lastSeasonScore: 0, lastSeasonRewardGem: 0, lastSeasonRewardExp: 0, lastSeasonRewardGuildToken: 0, guildRankList: [], minAndroidVersion: "0.0.0", miniOSVersion: "0.0.0" };
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "setting_json"
        ]
    }).Data;
    if (getTitleDataResult["setting_json"] != null) {
        let convertedData = JSON.parse(getTitleDataResult["setting_json"]);
        toReturn.minAndroidVersion = convertedData.MinAndroidVersion;
        toReturn.miniOSVersion = convertedData.MiniOSVersion;
    }
    toReturn.dailyLimitLeft = GetLeftDailyLimit("guild_raid", "0");
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_id"]
    });
    if (getUserReadOnlyDataResult.Data["guild_id"] != null) {
        let serverIdModifier = GetServerIdModifier(currentPlayerId);
        let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
            PlayFabId: getUserReadOnlyDataResult.Data["guild_id"].Value,
            StatisticName: "guild_raid" + serverIdModifier,
            MaxResultsCount: 1
        });
        let currentLeaderboardEntry = getLeaderboardAroundUserResult.Leaderboard[0];
        if (currentLeaderboardEntry != null) {
            toReturn.currentSeason = getLeaderboardAroundUserResult.Version;
            toReturn.position = currentLeaderboardEntry.Position;
            toReturn.score = currentLeaderboardEntry.StatValue;
            if (toReturn.score == 0)
                toReturn.score = 0;
        }
        let getUserInternalDataResult = server.GetUserInternalData({
            PlayFabId: currentPlayerId,
            Keys: [
                "guild_raid_reward_last_played_season"
            ]
        });
        if (getUserInternalDataResult.Data["guild_raid_reward_last_played_season"] != null) {
            let lastSeason = parseInt(getUserInternalDataResult.Data["guild_raid_reward_last_played_season"].Value);
            if (getLeaderboardAroundUserResult.Version == lastSeason + 1) {
                let rewardResult = ReceiveGuildRaidReward(serverIdModifier, getUserInternalDataResult, getLeaderboardAroundUserResult.Version, getUserReadOnlyDataResult.Data["guild_id"].Value);
                toReturn.lastSeasonPosition = rewardResult.lastSeasonPosition;
                toReturn.lastSeasonScore = rewardResult.lastSeasonScore;
                toReturn.lastSeasonRewardGem = rewardResult.lastSeasonRewardGem;
                toReturn.lastSeasonRewardExp = rewardResult.lastSeasonRewardExp;
                toReturn.lastSeasonRewardGuildToken = rewardResult.lastSeasonRewardGuildToken;
                log.info("Received guild reward gems: " + rewardResult.lastSeasonRewardGem);
            }
        }
        let getGuildMasterReadOnlyDataResult = server.GetUserReadOnlyData({
            PlayFabId: getUserReadOnlyDataResult.Data["guild_id"].Value,
            Keys: ["guild_cache_list", "guild_member_list"]
        });
        if (getGuildMasterReadOnlyDataResult.Data["guild_cache_list"] != null && getGuildMasterReadOnlyDataResult.Data["guild_member_list"] != null) {
            let guildCacheData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_cache_list"].Value);
            let guildMemberData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_member_list"].Value);
            let myData = guildMemberData.MemberList.find(n => n.Id == currentPlayerId);
            if (myData != null) {
                for (let i = 0; i < guildCacheData.MemberCacheDataList.length; i++) {
                    if (toReturn.currentSeason == guildCacheData.MemberCacheDataList[i].RaidSeason)
                        toReturn.guildRankList.push({ "name": guildCacheData.MemberCacheDataList[i].Name, "score": guildCacheData.MemberCacheDataList[i].RaidScore });
                    else
                        toReturn.guildRankList.push({ "name": guildCacheData.MemberCacheDataList[i].Name, "score": 0 });
                }
            }
            else {
                log.info("user not exist in member list");
                let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                    PlayFabId: currentPlayerId,
                    Data: {
                        "guild_id": null,
                        "guild_raid_reward_last_played_season": null
                    }
                });
                toReturn.error = true;
                return toReturn;
            }
        }
        else {
            log.info("The guild master has data issues (Likely the guild ownership has been changed)");
            toReturn.error = true;
            return toReturn;
        }
    }
    else {
        log.info("User don't have any guild!");
        toReturn.error = true;
        return toReturn;
    }
    toReturn.error = false;
    return toReturn;
};
handlers["guildRaidRefresh"] = GuildRaidRefresh;
let GuildRaidOpen = function (args, context) {
    let toReturn = { error: true, raidIndex: 0, reasonOfError: "unknown" };
    if (GetLeftDailyLimit("guild_raid", "0") <= 0)
        return toReturn;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_id"]
    });
    if (getUserReadOnlyDataResult.Data["guild_id"] != null) {
        let CheckGuildJoinTimeResult = CheckGuildJoinTime(getUserReadOnlyDataResult.Data["guild_id"].Value, null);
        if (CheckGuildJoinTimeResult.error)
            return toReturn;
        if (!CheckGuildJoinTimeResult.isTimeOver) {
            toReturn.reasonOfError = "guild_join_time_not_enough";
            return toReturn;
        }
        let getUserInternalDataResult = server.GetUserInternalData({
            PlayFabId: currentPlayerId,
            Keys: [
                "guild_raid_index"
            ]
        }).Data;
        if (getUserInternalDataResult["guild_raid_index"] != null)
            toReturn.raidIndex = parseInt(getUserInternalDataResult["guild_raid_index"].Value) + 1;
        let updateuserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: {
                "is_open_guild_raid": "true",
                "is_playing_guild_raid": "false",
                "guild_raid_index": toReturn.raidIndex.toString()
            }
        });
        toReturn.error = false;
    }
    return toReturn;
};
handlers["guildRaidOpen"] = GuildRaidOpen;
let GuildRaidStart = function (args, context) {
    let toReturn = { error: true, reasonOfError: "" };
    let updateuserInternalDataResult = server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: {
            "is_open_guild_raid": "true",
            "is_playing_guild_raid": "true"
        }
    });
    toReturn.error = false;
    return toReturn;
};
handlers["guildRaidStart"] = GuildRaidStart;
let GuildRaidEnd = function (args, context) {
    let toReturn = { error: true, reasonOfError: "" };
    let season = 0;
    if (args.season)
        season = args.season;
    let score = 0;
    if (args.score)
        score = args.score;
    let participantList = [];
    if (args.participantList)
        participantList = args.participantList;
    let participantScoreList = [];
    if (args.participantScoreList)
        participantScoreList = args.participantScoreList;
    let checkTimeStampForChanges = false;
    if (args.checkTimeStampForChanges)
        checkTimeStampForChanges = args.checkTimeStampForChanges;
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "setting_json"
        ]
    }).Data;
    let willGetReward = args.willGetReward;
    if (willGetReward) {
        let updateuserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: {
                "is_open_guild_raid": "false",
                "is_playing_guild_raid": "false",
                "guild_raid_reward_last_played_season": season.toString()
            }
        });
        let serverIdModifier = GetServerIdModifier(currentPlayerId);
        let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Keys: ["guild_id", "last_guild_leave_timestamp"]
        });
        if (getUserReadOnlyDataResult.Data["guild_id"] != null) {
            let timestampDay = Math.floor(+new Date() / (1000 * 60 * 60 * 24));
            server.UpdatePlayerStatistics({
                PlayFabId: getUserReadOnlyDataResult.Data["guild_id"].Value,
                Statistics: [
                    {
                        "StatisticName": "guild_raid" + serverIdModifier,
                        "Value": participantScoreList[0]
                    }
                ]
            });
            let getGuildMasterReadOnlyDataResult = server.GetUserReadOnlyData({
                PlayFabId: getUserReadOnlyDataResult.Data["guild_id"].Value,
                Keys: ["guild_member_list", "guild_cache_list"]
            });
            if (getGuildMasterReadOnlyDataResult.Data["guild_member_list"] != null && getGuildMasterReadOnlyDataResult.Data["guild_cache_list"] != null) {
                let guildMemberData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_member_list"].Value);
                let guildCacheData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_cache_list"].Value);
                for (let i = 0; i < guildMemberData.MemberList.length; i++) {
                    if (guildCacheData.MemberCacheDataList.find(n => n.Id == guildMemberData.MemberList[i].Id) == null) {
                        guildCacheData.MemberCacheDataList.push({
                            "Id": guildMemberData.MemberList[i].Id,
                            "Name": "???",
                            "Level": -1,
                            "LastTimeStamp": -1,
                            "TotalContribution": 0,
                            "RaidScore": 0,
                            "RaidSeason": -1
                        });
                    }
                }
                guildCacheData.MemberCacheDataList = guildCacheData.MemberCacheDataList.filter(n => guildMemberData.MemberList.find(k => k.Id == n.Id) != null);
                for (let i = 0; i < 1; i++) {
                    let target = guildCacheData.MemberCacheDataList.find(n => n.Id == participantList[i]);
                    if (target != null) {
                        if (target.RaidSeason != season)
                            target.RaidScore = participantScoreList[i];
                        else
                            target.RaidScore += participantScoreList[i];
                        target.RaidSeason = season;
                        target.LastTimeStamp = Math.floor(+new Date() / (1000 * 60 * 60));
                        log.info(participantList[i] + ": " + participantScoreList[i]);
                    }
                }
                let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                    PlayFabId: getUserReadOnlyDataResult.Data["guild_id"].Value,
                    Data: {
                        "guild_cache_list": unescape(JSON.stringify(guildCacheData))
                    }
                });
            }
        }
        if (!UseDailyLimitIfAvail("guild_raid", "0", 1))
            return toReturn;
    }
    else {
        let updateuserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: {
                "is_open_guild_raid": "false",
                "is_playing_guild_raid": "false"
            }
        });
    }
    let skillUseCount = 0;
    if (args.skillUseCount)
        skillUseCount = args.skillUseCount;
    log.info("Skill Use Count: " + skillUseCount);
    toReturn.error = false;
    return toReturn;
};
handlers["guildRaidEnd"] = GuildRaidEnd;
let GuildRaidLeave = function (args, context) {
    let toReturn = { error: true, reasonOfError: "" };
    let season = 0;
    if (args.season)
        season = args.season;
    let personalScore = 0;
    if (args.personalScore)
        personalScore = args.personalScore;
    let willGetReward = args.willGetReward;
    if (willGetReward) {
        let updateUserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: {
                "guild_raid_reward_last_played_season": season.toString()
            }
        });
    }
    let skillUseCount = 0;
    if (args.skillUseCount)
        skillUseCount = args.skillUseCount;
    log.info("Skill Use Count: " + skillUseCount);
    toReturn.error = false;
    return toReturn;
};
handlers["guildRaidLeave"] = GuildRaidLeave;
let GuildRaidJoin = function (args, context) {
    let toReturn = { error: true, reasonOfError: "" };
    let hostPlayerId = "";
    let hostGuildRaidIndex = 0;
    if (args.hostPlayerId)
        hostPlayerId = args.hostPlayerId;
    if (args.hostGuildRaidIndex)
        hostGuildRaidIndex = args.hostGuildRaidIndex;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_id"]
    });
    if (getUserReadOnlyDataResult.Data["guild_id"] != null) {
        let CheckGuildJoinTimeResult = CheckGuildJoinTime(getUserReadOnlyDataResult.Data["guild_id"].Value, null);
        if (CheckGuildJoinTimeResult.error)
            return toReturn;
        if (!CheckGuildJoinTimeResult.isTimeOver) {
            toReturn.reasonOfError = "guild_join_time_not_enough";
            return toReturn;
        }
        let getHostInternalDataResult = server.GetUserInternalData({
            PlayFabId: hostPlayerId,
            Keys: [
                "is_open_guild_raid",
                "is_playing_guild_raid",
                "guild_raid_index"
            ]
        }).Data;
        if (getHostInternalDataResult["is_playing_guild_raid"] != null && getHostInternalDataResult["is_playing_guild_raid"].Value == "true") {
            toReturn.reasonOfError = "raid_already_playing";
            return toReturn;
        }
        else if (getHostInternalDataResult["is_open_guild_raid"] == null || getHostInternalDataResult["is_open_guild_raid"].Value != "true") {
            toReturn.reasonOfError = "raid_not_open";
            return toReturn;
        }
        else if (getHostInternalDataResult["guild_raid_index"] == null || parseInt(getHostInternalDataResult["guild_raid_index"].Value) != hostGuildRaidIndex) {
            toReturn.reasonOfError = "raid_index_mismatch";
            return toReturn;
        }
        toReturn.error = false;
    }
    return toReturn;
};
handlers["guildRaidJoin"] = GuildRaidJoin;
let GuildRaidFix = function (args, context) {
    let toReturn = { error: true, reasonOfError: "" };
    let serverIdModifier = GetServerIdModifier(currentPlayerId);
    let currentStatValue = server.GetPlayerStatistics({
        PlayFabId: currentPlayerId,
        StatisticNames: ["guild_raid" + serverIdModifier]
    }).Statistics;
    let oldMasterScore = 0;
    if (currentStatValue[0] != null)
        oldMasterScore = currentStatValue[0].Value;
    log.info("old score: " + oldMasterScore);
    server.UpdatePlayerStatistics({
        PlayFabId: currentPlayerId,
        Statistics: [
            {
                "StatisticName": "guild_raid" + serverIdModifier,
                "Value": oldMasterScore * -1
            }
        ]
    });
    let getGuildMasterReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_member_list", "guild_cache_list"]
    });
    if (getGuildMasterReadOnlyDataResult.Data["guild_member_list"] != null && getGuildMasterReadOnlyDataResult.Data["guild_cache_list"] != null) {
        let guildMemberData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_member_list"].Value);
        let guildCacheData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_cache_list"].Value);
        for (let i = 0; i < guildMemberData.MemberList.length; i++) {
            if (guildCacheData.MemberCacheDataList.find(n => n.Id == guildMemberData.MemberList[i].Id) == null) {
                guildCacheData.MemberCacheDataList.push({
                    "Id": guildMemberData.MemberList[i].Id,
                    "Name": "???",
                    "Level": -1,
                    "LastTimeStamp": -1,
                    "TotalContribution": 0,
                    "RaidScore": 0,
                    "RaidSeason": -1
                });
            }
        }
        guildCacheData.MemberCacheDataList = guildCacheData.MemberCacheDataList.filter(n => guildMemberData.MemberList.find(k => k.Id == n.Id) != null);
        for (let i = 0; i < guildCacheData.MemberCacheDataList.length; i++) {
            let target = guildCacheData.MemberCacheDataList[i];
            if (target != null) {
                target.RaidScore = 0;
            }
        }
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "guild_cache_list": unescape(JSON.stringify(guildCacheData))
            }
        });
    }
    toReturn.error = false;
    return toReturn;
};
handlers["guildRaidFix"] = GuildRaidFix;
let GuildRaidDailyLimitReset = function (args, context) {
    let toReturn = { error: true };
    let dataToUse = {};
    dataToUse["daily_limit_use_guild_raid"] = "{}";
    let updateUserInternalDataResult = server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: dataToUse
    });
    toReturn.error = false;
    return toReturn;
};
handlers["guildRaidDailyLimitReset"] = GuildRaidDailyLimitReset;
function ReceiveGuildRaidReward(serverIdModifier, internalDataResult, currentSeason, guildMasterId) {
    let toReturn = { lastSeasonPosition: 0, lastSeasonScore: 0, lastSeasonRewardGem: 0, lastSeasonRewardExp: 0, lastSeasonRewardGuildToken: 0 };
    if (internalDataResult.Data["guild_raid_reward_last_played_season"] != null) {
        let lastPlayedSeason = parseInt(internalDataResult.Data["guild_raid_reward_last_played_season"].Value);
        if (currentSeason == lastPlayedSeason + 1) {
            let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
                PlayFabId: guildMasterId,
                StatisticName: "guild_raid" + serverIdModifier,
                MaxResultsCount: 1,
                Version: lastPlayedSeason
            });
            if (getLeaderboardAroundUserResult.Leaderboard[0] != null) {
                let lastSeasonLeaderboardEntry = getLeaderboardAroundUserResult.Leaderboard[0];
                toReturn.lastSeasonPosition = lastSeasonLeaderboardEntry.Position;
                toReturn.lastSeasonScore = lastSeasonLeaderboardEntry.StatValue;
            }
            if (toReturn.lastSeasonScore != 0) {
                if (toReturn.lastSeasonPosition == 0) {
                    toReturn.lastSeasonRewardGem = 2500;
                }
                else if (toReturn.lastSeasonPosition == 1) {
                    toReturn.lastSeasonRewardGem = 2000;
                }
                else if (toReturn.lastSeasonPosition == 2) {
                    toReturn.lastSeasonRewardGem = 1500;
                }
                else if (toReturn.lastSeasonPosition < 10) {
                    toReturn.lastSeasonRewardGem = 1000;
                }
                else if (toReturn.lastSeasonPosition < 50) {
                    toReturn.lastSeasonRewardGem = 750;
                }
                else if (toReturn.lastSeasonPosition < 100) {
                    toReturn.lastSeasonRewardGem = 250;
                }
                else {
                    toReturn.lastSeasonRewardGem = 50;
                }
                AddVirtualCurrency("GE", toReturn.lastSeasonRewardGem);
            }
            if (currentPlayerId == guildMasterId && toReturn.lastSeasonScore != 0) {
                if (toReturn.lastSeasonPosition == 0) {
                    toReturn.lastSeasonRewardExp = 2500;
                }
                else if (toReturn.lastSeasonPosition == 1) {
                    toReturn.lastSeasonRewardExp = 2000;
                }
                else if (toReturn.lastSeasonPosition == 2) {
                    toReturn.lastSeasonRewardExp = 1500;
                }
                else if (toReturn.lastSeasonPosition < 10) {
                    toReturn.lastSeasonRewardExp = 1000;
                }
                else {
                    toReturn.lastSeasonRewardExp = 500;
                }
                let getGuildMasterReadOnlyDataResult = server.GetUserReadOnlyData({
                    PlayFabId: currentPlayerId,
                    Keys: ["guild_master_exp"]
                });
                let masterExp = 0;
                if (getGuildMasterReadOnlyDataResult.Data["guild_master_exp"] != null)
                    masterExp = parseInt(getGuildMasterReadOnlyDataResult.Data["guild_master_exp"].Value);
                let updateGuildMasterReadOnlyDataResult = server.UpdateUserReadOnlyData({
                    PlayFabId: currentPlayerId,
                    Data: {
                        "guild_master_exp": (masterExp + toReturn.lastSeasonRewardExp).toString()
                    }
                });
            }
            let timestampDay = Math.floor(+new Date() / (1000 * 60 * 60 * 24));
            if (timestampDay >= 18813) {
                if (toReturn.lastSeasonScore != 0) {
                    if (toReturn.lastSeasonPosition == 0) {
                        toReturn.lastSeasonRewardGuildToken = 2500;
                    }
                    else if (toReturn.lastSeasonPosition == 1) {
                        toReturn.lastSeasonRewardGuildToken = 2000;
                    }
                    else if (toReturn.lastSeasonPosition == 2) {
                        toReturn.lastSeasonRewardGuildToken = 1500;
                    }
                    else if (toReturn.lastSeasonPosition < 10) {
                        toReturn.lastSeasonRewardGuildToken = 1000;
                    }
                    else {
                        toReturn.lastSeasonRewardGuildToken = 500;
                    }
                    AddVirtualCurrency("GT", toReturn.lastSeasonRewardGuildToken);
                }
            }
            let updateUserInternalDataResult = server.UpdateUserInternalData({
                PlayFabId: currentPlayerId,
                Data: {
                    "guild_raid_reward_last_played_season": currentSeason.toString()
                }
            });
        }
    }
    else {
        let updateUserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: {
                "guild_raid_reward_last_played_season": currentSeason.toString()
            }
        });
    }
    return toReturn;
}
let RegisterModerator = function (args, context) {
    let toReturn = { error: true };
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["is_moderator"]
    });
    if (getUserReadOnlyDataResult.Data["is_moderator"] == null) {
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "is_moderator": "true"
            }
        });
        toReturn.error = false;
    }
    else {
        if (getUserReadOnlyDataResult.Data["is_moderator"].Value == "false") {
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "is_moderator": "true"
                }
            });
            toReturn.error = false;
        }
    }
    return toReturn;
};
handlers["registerModerator"] = RegisterModerator;
let UnregisterModerator = function (args, context) {
    let toReturn = { error: true };
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["is_moderator"]
    });
    if (getUserReadOnlyDataResult.Data["is_moderator"] != null && getUserReadOnlyDataResult.Data["is_moderator"].Value == "true") {
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "is_moderator": "false"
            }
        });
        toReturn.error = false;
    }
    return toReturn;
};
handlers["unregisterModerator"] = UnregisterModerator;
let RequestChatBan = function (args, context) {
    let toReturn = { error: true, reasonOfError: "", updatedBanData: [] };
    let targetPlayfabId = "";
    let chatLog = "";
    if (args.targetPlayfabId)
        targetPlayfabId = args.targetPlayfabId;
    if (args.chatLog)
        chatLog = args.chatLog;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["is_moderator", "chat_ban_data"]
    });
    if (getUserReadOnlyDataResult.Data["is_moderator"] != null && getUserReadOnlyDataResult.Data["is_moderator"].Value == "true") {
        let getTargetReadOnlyDataResult = server.GetUserReadOnlyData({
            PlayFabId: targetPlayfabId,
            Keys: ["is_chat_banned"]
        });
        if (getTargetReadOnlyDataResult.Data["is_chat_banned"] != null && getTargetReadOnlyDataResult.Data["is_chat_banned"].Value == "true") {
            log.info("Target user is already chat banned !");
            toReturn.reasonOfError = "ban_duplicate";
            return toReturn;
        }
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: targetPlayfabId,
            Data: {
                "is_chat_banned": "true",
                "chat_ban_log": chatLog
            }
        });
        let currentTimestampSecond = Math.floor(+new Date() / (1000));
        if (getUserReadOnlyDataResult.Data["chat_ban_data"] != null) {
            let banData = JSON.parse(getUserReadOnlyDataResult.Data["chat_ban_data"].Value);
            if (banData.BanList.filter(n => n.Id == targetPlayfabId).length > 0) {
                toReturn.reasonOfError = "ban_duplicate";
                return toReturn;
            }
            banData.BanList.push({ Id: targetPlayfabId, BanTime: currentTimestampSecond });
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "chat_ban_data": JSON.stringify(banData)
                }
            });
            toReturn.updatedBanData = banData;
            toReturn.error = false;
        }
        else {
            let defaultBanData = { "BanList": [] };
            defaultBanData.BanList.push({ Id: targetPlayfabId, BanTime: currentTimestampSecond });
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "chat_ban_data": JSON.stringify(defaultBanData)
                }
            });
            toReturn.updatedBanData = defaultBanData;
            toReturn.error = false;
        }
    }
    return toReturn;
};
handlers["requestChatBan"] = RequestChatBan;
let RequestChatBanCancel = function (args, context) {
    let toReturn = { error: true, reasonOfError: "", updatedBanData: [] };
    let targetPlayfabId = "";
    if (args.targetPlayfabId)
        targetPlayfabId = args.targetPlayfabId;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["is_moderator", "chat_ban_data"]
    });
    if (getUserReadOnlyDataResult.Data["is_moderator"] != null && getUserReadOnlyDataResult.Data["is_moderator"].Value == "true") {
        if (getUserReadOnlyDataResult.Data["chat_ban_data"] != null) {
            let banData = JSON.parse(getUserReadOnlyDataResult.Data["chat_ban_data"].Value);
            if (banData.BanList.filter(n => n.Id == targetPlayfabId).length == 0) {
                toReturn.reasonOfError = "ban_not_exist";
                return toReturn;
            }
            banData.BanList = banData.BanList.filter(n => n.Id != targetPlayfabId);
            toReturn.updatedBanData = banData;
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "chat_ban_data": JSON.stringify(banData)
                }
            });
            let updateTargetUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: targetPlayfabId,
                Data: {
                    "is_chat_banned": "false",
                    "chat_ban_log": null
                }
            });
            toReturn.error = false;
        }
    }
    return toReturn;
};
handlers["requestChatBanCancel"] = RequestChatBanCancel;
let DirectlyRequestChatBanCancel = function (args, context) {
    let toReturn = { error: true };
    let updateTargetUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Data: {
            "is_chat_banned": "false",
            "chat_ban_log": null
        }
    });
    toReturn.error = false;
    return toReturn;
};
handlers["directlyRequestChatBanCancel"] = DirectlyRequestChatBanCancel;
let RequestChatLog = function (args, context) {
    let toReturn = { error: true, chatLog: "" };
    let targetPlayfabId = "";
    if (args.targetPlayfabId)
        targetPlayfabId = args.targetPlayfabId;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["is_moderator", "chat_ban_data"]
    });
    if (getUserReadOnlyDataResult.Data["is_moderator"] != null && getUserReadOnlyDataResult.Data["is_moderator"].Value == "true") {
        if (getUserReadOnlyDataResult.Data["chat_ban_data"] != null) {
            let banData = JSON.parse(getUserReadOnlyDataResult.Data["chat_ban_data"].Value);
            if (banData.BanList.filter(n => n.Id == targetPlayfabId).length == 0) {
                log.info("target user is not exist in ban list!");
                return toReturn;
            }
            let getTargetIdReadOnlyDataResult = server.GetUserReadOnlyData({
                PlayFabId: targetPlayfabId,
                Keys: ["chat_ban_log"]
            });
            if (getTargetIdReadOnlyDataResult.Data["chat_ban_log"] == null) {
                log.info("target user's chat ban log is not exist!");
                return toReturn;
            }
            else
                toReturn.chatLog = getTargetIdReadOnlyDataResult.Data["chat_ban_log"].Value;
            toReturn.error = false;
        }
    }
    return toReturn;
};
handlers["requestChatLog"] = RequestChatLog;
let DefaultPlayerSetting = function (args, context) {
    let toReturn = { error: true, userData: "", inventoryData: "" };
    let didGetDefaultItem = server.GetUserData({
        PlayFabId: currentPlayerId,
        Keys: ["is_default_settings_applied"]
    }).Data["is_default_settings_applied"];
    if (didGetDefaultItem == null || didGetDefaultItem.Value == "false") {
        let getTitleDataResult = server.GetTitleData({
            Keys: [
                "default_user_data",
                "default_inventory_data"
            ]
        }).Data;
        if (getTitleDataResult["default_user_data"] != null && getTitleDataResult["default_inventory_data"] != null) {
            log.info(getTitleDataResult["default_user_data"]);
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "is_default_settings_applied": "true",
                    "user_data": getTitleDataResult["default_user_data"],
                    "inventory_data": getTitleDataResult["default_inventory_data"]
                }
            });
            toReturn.userData = getTitleDataResult["default_user_data"];
            toReturn.inventoryData = getTitleDataResult["default_inventory_data"];
            toReturn.error = false;
        }
    }
    return toReturn;
};
handlers["defaultPlayerSetting"] = DefaultPlayerSetting;
let PushNotificationSetting = function (args, context) {
    let toReturn = { error: true };
    let isPushOff = false;
    if (args.isPushOff)
        isPushOff = args.isPushOff;
    if (isPushOff) {
        let addPlayerTagResult = server.AddPlayerTag({
            PlayFabId: currentPlayerId,
            TagName: "PushOff"
        });
    }
    else {
        let removePlayerTagResult = server.RemovePlayerTag({
            PlayFabId: currentPlayerId,
            TagName: "PushOff"
        });
    }
    toReturn.error = false;
    return toReturn;
};
handlers["pushNotificationSetting"] = PushNotificationSetting;
let SyncPlayerData = function (args, context) {
    let toReturn = { error: true, reasonOfError: "", gemsCollided: false, gemsChange: 0, unsyncedGemChangeVersion: -1, syncOrigin: "" };
    let serverIdModifier = GetServerIdModifier(currentPlayerId);
    let gemsChange = 0;
    if (args.gemsChange != null)
        gemsChange = args.gemsChange;
    toReturn.gemsChange = gemsChange;
    let inventoryData = "";
    if (args.inventoryData != null)
        inventoryData = args.inventoryData;
    let userData = "";
    if (args.userData != null)
        userData = args.userData;
    let stageProgress = 0;
    if (args.stageProgress != null)
        stageProgress = args.stageProgress;
    let killedMobsForCurrentStage = -1;
    if (args.killedMobsForCurrentStage != null)
        killedMobsForCurrentStage = args.killedMobsForCurrentStage;
    let dps = 0;
    if (args.dps != null)
        dps = args.dps;
    let isSuspicious = false;
    if (args.isSuspicious != null)
        isSuspicious = args.isSuspicious;
    let progressValue = -1;
    if (args.progressValue != null)
        progressValue = args.progressValue;
    let localCacheLost = false;
    if (args.localCacheLost != null)
        localCacheLost = args.localCacheLost;
    let unsyncedGemChangeVersion = -1;
    if (args.unsyncedGemChangeVersion != null)
        unsyncedGemChangeVersion = args.unsyncedGemChangeVersion;
    toReturn.unsyncedGemChangeVersion = unsyncedGemChangeVersion;
    if (args.syncOrigin != null)
        toReturn.syncOrigin = args.syncOrigin;
    log.info(userData);
    log.info(inventoryData);
    if (isSuspicious) {
        server.UpdatePlayerStatistics({
            PlayFabId: currentPlayerId,
            Statistics: [
                {
                    "StatisticName": "cheaters",
                    "Value": 1
                }
            ]
        });
        let ban = {
            PlayFabId: currentPlayerId,
            Reason: "Cheater"
        };
        server.BanUsers({ Bans: [ban] });
        return toReturn;
    }
    if (localCacheLost) {
        server.UpdatePlayerStatistics({
            PlayFabId: currentPlayerId,
            Statistics: [
                {
                    "StatisticName": "local_cache_lost",
                    "Value": 1
                }
            ]
        });
    }
    let getReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: [
            "progress_value",
            "unsynced_gem_change_version"
        ]
    });
    if (getReadOnlyDataResult.Data["progress_value"] != null && parseInt(getReadOnlyDataResult.Data["progress_value"].Value) > progressValue) {
        toReturn.reasonOfError = "data_collision";
        return toReturn;
    }
    if (gemsChange != 0 && inventoryData != "") {
        var inventoryFix = JSON.parse(inventoryData);
        inventoryFix.UnsyncedGem = 0;
        inventoryData = JSON.stringify(inventoryFix);
    }
    let dataTouse = {};
    dataTouse["user_data"] = userData;
    dataTouse["inventory_data"] = inventoryData;
    dataTouse["progress_value"] = progressValue.toString();
    dataTouse["unsynced_gem_change_version"] = unsyncedGemChangeVersion.toString();
    if (toReturn.syncOrigin == "EnterRaid") {
        let currentTimestampHour = Math.floor(+new Date() / (1000 * 3600));
        let currentTimestampDay = Math.floor(+new Date() / (1000 * 3600 * 24));
        dataTouse["last_confirmed_raid_timestamp_hour"] = currentTimestampHour.toString();
        dataTouse["last_confirmed_raid_timestamp_day"] = currentTimestampDay.toString();
    }
    let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Data: dataTouse
    });
    if (gemsChange > 0) {
        if (gemsChange > 1000000) {
            let getPlayerCombinedInfoResult = server.GetPlayerCombinedInfo({
                PlayFabId: currentPlayerId,
                InfoRequestParameters: {
                    GetCharacterInventories: false,
                    GetCharacterList: false,
                    GetPlayerStatistics: false,
                    GetTitleData: false,
                    GetUserAccountInfo: false,
                    GetUserData: false,
                    GetUserInventory: false,
                    GetUserReadOnlyData: false,
                    GetUserVirtualCurrency: false,
                    GetPlayerProfile: true,
                    ProfileConstraints: {
                        ShowAvatarUrl: false,
                        ShowBannedUntil: false,
                        ShowCampaignAttributions: false,
                        ShowContactEmailAddresses: false,
                        ShowCreated: false,
                        ShowDisplayName: false,
                        ShowLastLogin: false,
                        ShowLinkedAccounts: false,
                        ShowLocations: false,
                        ShowMemberships: false,
                        ShowOrigination: false,
                        ShowPushNotificationRegistrations: false,
                        ShowStatistics: false,
                        ShowTags: false,
                        ShowTotalValueToDateInUsd: true,
                        ShowValuesToDate: false,
                    }
                }
            }).InfoResultPayload;
            let playerValue = 0;
            if (getPlayerCombinedInfoResult != null
                && getPlayerCombinedInfoResult.PlayerProfile != null
                && getPlayerCombinedInfoResult.PlayerProfile.TotalValueToDateInUSD != null) {
                playerValue = getPlayerCombinedInfoResult.PlayerProfile.TotalValueToDateInUSD;
            }
            if (playerValue < 10000) {
                server.UpdatePlayerStatistics({
                    PlayFabId: currentPlayerId,
                    Statistics: [
                        {
                            "StatisticName": "cheaters",
                            "Value": 1
                        }
                    ]
                });
                let ban = {
                    PlayFabId: currentPlayerId,
                    Reason: "Cheater2"
                };
                server.BanUsers({ Bans: [ban] });
                return toReturn;
            }
            else {
                toReturn.gemsCollided = ApplyAsyncedGems(gemsChange, unsyncedGemChangeVersion, getReadOnlyDataResult);
            }
        }
        else if (gemsChange > 100000) {
            server.UpdatePlayerStatistics({
                PlayFabId: currentPlayerId,
                Statistics: [
                    {
                        "StatisticName": "gem_increased_a_lot",
                        "Value": 1
                    }
                ]
            });
            toReturn.gemsCollided = ApplyAsyncedGems(gemsChange, unsyncedGemChangeVersion, getReadOnlyDataResult);
        }
        else {
            toReturn.gemsCollided = ApplyAsyncedGems(gemsChange, unsyncedGemChangeVersion, getReadOnlyDataResult);
        }
    }
    else if (gemsChange < 0) {
        toReturn.gemsCollided = ApplyAsyncedGems(gemsChange, unsyncedGemChangeVersion, getReadOnlyDataResult);
    }
    if (stageProgress != 0 && killedMobsForCurrentStage != -1) {
        server.UpdatePlayerStatistics({
            PlayFabId: currentPlayerId,
            Statistics: [
                {
                    "StatisticName": "ranking" + serverIdModifier,
                    "Value": Math.min(stageProgress, 2100000) * 100000 + Math.min(killedMobsForCurrentStage, 99999)
                },
                {
                    "StatisticName": "dps" + serverIdModifier,
                    "Value": dps
                }
            ]
        });
    }
    toReturn.error = false;
    return toReturn;
};
handlers["syncPlayerData"] = SyncPlayerData;
let UpdateVirtualServer = function (args, context) {
    let toReturn = { serverId: "", error: true };
    let serverId = "";
    if (args && args.serverId)
        serverId = args.serverId;
    let addPlayerTagResult = server.AddPlayerTag({
        PlayFabId: currentPlayerId,
        TagName: "Server" + serverId
    });
    toReturn.serverId = serverId;
    toReturn.error = false;
    return toReturn;
};
handlers["updateVirtualServer"] = UpdateVirtualServer;
let LeaveVirtualServer = function (args, context) {
    let toReturn = { error: true };
    let serverId = "";
    if (args && args.serverId)
        serverId = args.serverId;
    let removePlayerTagResult = server.RemovePlayerTag({
        PlayFabId: currentPlayerId,
        TagName: "Server" + serverId
    });
    toReturn.error = false;
    return toReturn;
};
handlers["leaveVirtualServer"] = LeaveVirtualServer;
let RefreshLastDevice = function (args, context) {
    let toReturn = { willOtherClose: false, otherWasPlayingRecently: false, needClose: false, isChatBanned: false, minAndroidVersionCode: "0.0.0", miniOSVersionCode: "0.0.0", minLoginCode: 0, error: true };
    let deviceTag = "";
    if (args.deviceTag != null)
        deviceTag = args.deviceTag;
    let isForLogin = false;
    if (args.isForLogin != null)
        isForLogin = args.isForLogin;
    let currentTimestampSecond = Math.floor(+new Date() / (1000));
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: [
            "last_device_tag", "last_device_timestamp", "is_chat_banned"
        ]
    });
    if (getUserReadOnlyDataResult.Data["last_device_tag"] != null && getUserReadOnlyDataResult.Data["last_device_tag"].Value != deviceTag) {
        if (isForLogin) {
            if (getUserReadOnlyDataResult.Data["last_device_timestamp"] != null) {
                if (currentTimestampSecond - parseInt(getUserReadOnlyDataResult.Data["last_device_timestamp"].Value) < 60) {
                    toReturn.otherWasPlayingRecently = true;
                }
            }
            toReturn.willOtherClose = true;
        }
        else {
            toReturn.needClose = true;
        }
    }
    if (getUserReadOnlyDataResult.Data["is_chat_banned"] != null && getUserReadOnlyDataResult.Data["is_chat_banned"].Value == "true")
        toReturn.isChatBanned = true;
    if (isForLogin) {
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "last_device_tag": deviceTag
            }
        });
    }
    else {
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "last_device_timestamp": currentTimestampSecond.toString()
            }
        });
    }
    toReturn.minAndroidVersionCode = "1.3.5";
    toReturn.miniOSVersionCode = "0.0.0";
    toReturn.minLoginCode = 0;
    toReturn.error = false;
    return toReturn;
};
handlers["refreshLastDevice"] = RefreshLastDevice;
let RefreshLastDeviceQA = function (args, context) {
    let toReturn = { willOtherClose: false, otherWasPlayingRecently: false, needClose: false, isChatBanned: false, minAndroidVersionCode: "0.0.0", miniOSVersionCode: "0.0.0", minLoginCode: 0, error: true };
    let deviceTag = "";
    if (args.deviceTag != null)
        deviceTag = args.deviceTag;
    let isForLogin = false;
    if (args.isForLogin != null)
        isForLogin = args.isForLogin;
    let currentTimestampSecond = Math.floor(+new Date() / (1000));
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: [
            "last_device_tag", "last_device_timestamp", "is_chat_banned"
        ]
    });
    if (getUserReadOnlyDataResult.Data["last_device_tag"] != null && getUserReadOnlyDataResult.Data["last_device_tag"].Value != deviceTag) {
        if (isForLogin) {
            if (getUserReadOnlyDataResult.Data["last_device_timestamp"] != null) {
                if (currentTimestampSecond - parseInt(getUserReadOnlyDataResult.Data["last_device_timestamp"].Value) < 60) {
                    toReturn.otherWasPlayingRecently = true;
                }
            }
            toReturn.willOtherClose = true;
        }
        else {
            toReturn.needClose = true;
        }
    }
    if (getUserReadOnlyDataResult.Data["is_chat_banned"] != null && getUserReadOnlyDataResult.Data["is_chat_banned"].Value == "true")
        toReturn.isChatBanned = true;
    if (isForLogin) {
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "last_device_tag": deviceTag
            }
        });
    }
    else {
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "last_device_timestamp": currentTimestampSecond.toString()
            }
        });
    }
    toReturn.minAndroidVersionCode = "1.3.5";
    toReturn.miniOSVersionCode = "0.0.0";
    toReturn.minLoginCode = 1;
    toReturn.error = false;
    return toReturn;
};
handlers["refreshLastDeviceQA"] = RefreshLastDeviceQA;
let AddChatBlockList = function (args, context) {
    let toReturn = { updatedChatBlockData: {}, reasonOfError: "", error: true };
    let targetNickname = "";
    let targetId = "";
    if (args.targetNickname != null)
        targetNickname = args.targetNickname;
    if (args.targetId != null)
        targetId = args.targetId;
    if (targetNickname == "" || targetId == "")
        return toReturn;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["chat_block_data"]
    });
    if (getUserReadOnlyDataResult.Data["chat_block_data"] != null) {
        let chatBlockData = JSON.parse(getUserReadOnlyDataResult.Data["chat_block_data"].Value);
        if (chatBlockData.ChatBlockList.length >= 100) {
            toReturn.reasonOfError = "ban_over_limit";
            return toReturn;
        }
        if (chatBlockData.ChatBlockList.filter(n => n.Id == targetId).length > 0) {
            toReturn.reasonOfError = "ban_duplicate";
            return toReturn;
        }
        chatBlockData.ChatBlockList.push({ Id: targetId, Nickname: targetNickname });
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "chat_block_data": JSON.stringify(chatBlockData)
            }
        });
        toReturn.updatedChatBlockData = chatBlockData;
        toReturn.error = false;
    }
    else {
        let defaultChatBlockData = { "ChatBlockList": [] };
        defaultChatBlockData.ChatBlockList.push({ Id: targetId, Nickname: targetNickname });
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "chat_block_data": JSON.stringify(defaultChatBlockData)
            }
        });
        toReturn.updatedChatBlockData = defaultChatBlockData;
        toReturn.error = false;
    }
    return toReturn;
};
handlers["addChatBlockList"] = AddChatBlockList;
let AddBanAndRemoveStat = function (args, context) {
    let toReturn = { error: true };
    let getPlayerTagsResult = server.GetPlayerTags({
        PlayFabId: currentPlayerId
    });
    let serverId = "";
    if (getPlayerTagsResult != null && getPlayerTagsResult.Tags.length > 0) {
        let serverTags = getPlayerTagsResult.Tags.filter(n => n.indexOf("Server") >= 0);
        if (serverTags != null && serverTags.length > 0)
            serverId = GetServerIdFromTag(serverTags[0]);
    }
    if (serverId != "") {
        let statistics = server.GetPlayerStatistics({
            PlayFabId: currentPlayerId,
            StatisticNames: ["arena_" + serverId]
        });
        server.UpdatePlayerStatistics({
            PlayFabId: currentPlayerId,
            Statistics: [
                {
                    "StatisticName": "arena_" + serverId,
                    "Value": statistics.Statistics[0].Value * -1
                },
                {
                    "StatisticName": "ranking_" + serverId,
                    "Value": 0
                }
            ]
        });
    }
    let ban = {
        PlayFabId: currentPlayerId,
        Reason: "Cheater"
    };
    server.BanUsers({ Bans: [ban] });
    toReturn.error = false;
    return toReturn;
};
handlers["addBanAndRemoveStat"] = AddBanAndRemoveStat;
let RemoveChatBlockList = function (args, context) {
    let toReturn = { updatedChatBlockData: {}, error: true };
    let targetId = "";
    if (args.targetId != null)
        targetId = args.targetId;
    if (targetId == "")
        return toReturn;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["chat_block_data"]
    });
    if (getUserReadOnlyDataResult.Data["chat_block_data"] != null) {
        let chatBlockData = JSON.parse(getUserReadOnlyDataResult.Data["chat_block_data"].Value);
        let updatedChatBlockList = chatBlockData.ChatBlockList.filter(n => n.Id != targetId);
        chatBlockData.ChatBlockList = updatedChatBlockList;
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "chat_block_data": JSON.stringify(chatBlockData)
            }
        });
        toReturn.updatedChatBlockData = chatBlockData;
        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;
};
handlers["removeChatBlockList"] = RemoveChatBlockList;
let CheckCheater = function (args, context) {
    let toReturn = { error: true };
    let currentTimestampDay = Math.floor(+new Date() / (1000 * 60 * 60 * 24));
    let limitWeaponId = 16;
    let limitStageProgress = 61;
    if (args.limitWeaponId)
        limitWeaponId = args.limitWeaponId;
    if (args.limitStageProgress)
        limitStageProgress = args.limitStageProgress;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["user_data", "inventory_data"]
    });
    if (getUserReadOnlyDataResult.Data["user_data"] != null && getUserReadOnlyDataResult.Data["inventory_data"] != null) {
        let isWeaponSuspicious = true;
        let isStageSuspicious = true;
        let isOwnedBasicPackage = false;
        let userData = JSON.parse(getUserReadOnlyDataResult.Data["user_data"].Value);
        let inventoryData = JSON.parse(getUserReadOnlyDataResult.Data["inventory_data"].Value);
        let currentBestStageProgress = userData.StageProgress;
        for (let i = 0; i < inventoryData.OwnedWeaponList.length; i++) {
            let weaponId = inventoryData.OwnedWeaponList[i].id;
            let convertedWeaponId = parseInt(weaponId.substring(7));
            if (convertedWeaponId > limitWeaponId) {
                isWeaponSuspicious = false;
                break;
            }
        }
        if (currentBestStageProgress < limitStageProgress)
            isStageSuspicious = false;
        let getUserInventoryResult = server.GetUserInventory({
            PlayFabId: currentPlayerId
        });
        if (getUserInventoryResult.Inventory.filter(n => n.ItemId == "bundle_package_basic_01")[0] != null)
            isOwnedBasicPackage = true;
        if (isWeaponSuspicious && isStageSuspicious && !isOwnedBasicPackage) {
            let getPlayerTagsResult = server.GetPlayerTags({
                PlayFabId: currentPlayerId
            });
            let serverId = "";
            if (getPlayerTagsResult != null && getPlayerTagsResult.Tags.length > 0) {
                let serverTags = getPlayerTagsResult.Tags.filter(n => n.indexOf("Server") >= 0);
                if (serverTags != null && serverTags.length > 0)
                    serverId = GetServerIdFromTag(serverTags[0]);
            }
            if (serverId == "") {
                server.UpdatePlayerStatistics({
                    PlayFabId: currentPlayerId,
                    Statistics: [
                        {
                            "StatisticName": "cheater_stage_" + serverId,
                            "Value": currentTimestampDay
                        }
                    ]
                });
            }
            else {
                server.UpdatePlayerStatistics({
                    PlayFabId: currentPlayerId,
                    Statistics: [
                        {
                            "StatisticName": "cheater_stage_" + serverId,
                            "Value": currentTimestampDay
                        },
                        {
                            "StatisticName": "arena_" + serverId,
                            "Value": 0
                        },
                        {
                            "StatisticName": "arena_all_season_" + serverId,
                            "Value": 0
                        },
                        {
                            "StatisticName": "ranking_" + serverId,
                            "Value": 0
                        }
                    ]
                });
            }
            let ban = {
                PlayFabId: currentPlayerId,
                Reason: "Cheater"
            };
            server.BanUsers({ Bans: [ban] });
        }
        else
            log.info("This user is not cheater");
        toReturn.error = false;
    }
    return toReturn;
};
handlers["checkCheater"] = CheckCheater;
let CheckPet = function (args, context) {
    let toReturn = { error: true };
    let currentTimestampDay = Math.floor(+new Date() / (1000 * 60 * 60 * 24));
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["user_data", "inventory_data"]
    });
    let superCriteria = 700;
    if (args.superCriteria)
        superCriteria = superCriteria;
    if (getUserReadOnlyDataResult.Data["user_data"] != null && getUserReadOnlyDataResult.Data["inventory_data"] != null) {
        let userData = JSON.parse(getUserReadOnlyDataResult.Data["user_data"].Value);
        let inventoryData = JSON.parse(getUserReadOnlyDataResult.Data["inventory_data"].Value);
        let superPet = false;
        for (let i = 0; i < inventoryData.OwnedPetList.length; i++) {
            let petLevel = inventoryData.OwnedPetList[i].level;
            if (petLevel > superCriteria) {
                superPet = true;
            }
        }
        let getUserInventoryResult = server.GetUserInventory({
            PlayFabId: currentPlayerId
        });
        let isOwnedBasicPackage = false;
        if (getUserInventoryResult.Inventory.filter(n => n.ItemId == "bundle_package_basic_01")[0] != null)
            isOwnedBasicPackage = true;
        if (superPet && !isOwnedBasicPackage) {
            let getPlayerTagsResult = server.GetPlayerTags({
                PlayFabId: currentPlayerId
            });
            let serverId = "";
            if (getPlayerTagsResult != null && getPlayerTagsResult.Tags.length > 0) {
                let serverTags = getPlayerTagsResult.Tags.filter(n => n.indexOf("Server") >= 0);
                if (serverTags != null && serverTags.length > 0)
                    serverId = GetServerIdFromTag(serverTags[0]);
            }
            if (serverId == "") {
                server.UpdatePlayerStatistics({
                    PlayFabId: currentPlayerId,
                    Statistics: [
                        {
                            "StatisticName": "cheater_super_pet",
                            "Value": currentTimestampDay
                        }
                    ]
                });
            }
            else {
                server.UpdatePlayerStatistics({
                    PlayFabId: currentPlayerId,
                    Statistics: [
                        {
                            "StatisticName": "cheater_super_pet_" + serverId,
                            "Value": currentTimestampDay
                        },
                        {
                            "StatisticName": "arena_" + serverId,
                            "Value": 0
                        },
                        {
                            "StatisticName": "arena_all_season_" + serverId,
                            "Value": 0
                        },
                        {
                            "StatisticName": "ranking_" + serverId,
                            "Value": 0
                        }
                    ]
                });
            }
            let ban = {
                PlayFabId: currentPlayerId,
                Reason: "Cheater"
            };
            server.BanUsers({ Bans: [ban] });
        }
        else
            log.info("This user is not cheater");
        toReturn.error = false;
    }
    return toReturn;
};
handlers["checkPet"] = CheckPet;
let CheckBestPetOwned = function (args, context) {
    let toReturn = { error: true };
    let limitPetId = "pet_11";
    if (args.limitPetId)
        limitPetId = args.limitPetId;
    let inventorySize = 0;
    let getUserInventoryResult = server.GetUserInventory({
        PlayFabId: currentPlayerId
    });
    inventorySize = getUserInventoryResult.Inventory.length;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["user_data", "inventory_data"]
    });
    let inventoryData = JSON.parse(getUserReadOnlyDataResult.Data["inventory_data"].Value);
    if (getUserReadOnlyDataResult.Data["user_data"] != null && getUserReadOnlyDataResult.Data["inventory_data"] != null) {
        let isOwned = false;
        for (let i = 0; i < inventoryData.OwnedPetList.length; i++) {
            let petId = inventoryData.OwnedPetList[i].id;
            if (petId == limitPetId) {
                isOwned = true;
                break;
            }
        }
        if (isOwned) {
            let getPlayerTagsResult = server.GetPlayerTags({
                PlayFabId: currentPlayerId
            });
            let serverId = "";
            if (getPlayerTagsResult != null && getPlayerTagsResult.Tags.length > 0) {
                let serverTags = getPlayerTagsResult.Tags.filter(n => n.indexOf("Server") >= 0);
                if (serverTags != null && serverTags.length > 0)
                    serverId = GetServerIdFromTag(serverTags[0]);
            }
            if (inventorySize == 0) {
                if (serverId != "") {
                    server.UpdatePlayerStatistics({
                        PlayFabId: currentPlayerId,
                        Statistics: [
                            {
                                "StatisticName": "owned_" + limitPetId + "_" + serverId,
                                "Value": inventorySize
                            },
                            {
                                "StatisticName": "arena_" + serverId,
                                "Value": 0
                            },
                            {
                                "StatisticName": "arena_all_season_" + serverId,
                                "Value": 0
                            },
                            {
                                "StatisticName": "ranking_" + serverId,
                                "Value": 0
                            }
                        ]
                    });
                    let ban = {
                        PlayFabId: currentPlayerId,
                        Reason: "Cheater"
                    };
                    server.BanUsers({ Bans: [ban] });
                }
            }
            else {
                server.UpdatePlayerStatistics({
                    PlayFabId: currentPlayerId,
                    Statistics: [
                        {
                            "StatisticName": "owned_" + limitPetId + "_" + serverId,
                            "Value": inventorySize
                        }
                    ]
                });
            }
        }
        else
            log.info("This user is not owned !");
        toReturn.error = false;
    }
    return toReturn;
};
handlers["checkBestPetOwned"] = CheckBestPetOwned;
let CheckLegendary = function (args, context) {
    let toReturn = { error: true };
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["inventory_data"]
    });
    if (getUserReadOnlyDataResult.Data["inventory_data"] != null) {
        let inventoryData = JSON.parse(getUserReadOnlyDataResult.Data["inventory_data"].Value);
        let isAllMax = true;
        if (inventoryData.OwnedLegendaryRelicList == null || inventoryData.OwnedLegendaryRelicList.length < 7) {
            isAllMax = false;
        }
        else {
            for (let i = 0; i < inventoryData.OwnedLegendaryRelicList.length; i++) {
                if (inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_01" &&
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 < 99) {
                    isAllMax = false;
                }
                if (inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_02" &&
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 < 1000) {
                    isAllMax = false;
                }
                if (inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_03" &&
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 < 50) {
                    isAllMax = false;
                }
                if (inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_04" &&
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 < 300) {
                    isAllMax = false;
                }
                if (inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_05" &&
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 < 300) {
                    isAllMax = false;
                }
                if (inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_06" &&
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 < 300) {
                    isAllMax = false;
                }
                if (inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_07" &&
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 < 200) {
                    isAllMax = false;
                }
            }
        }
        if (isAllMax) {
            server.UpdatePlayerStatistics({
                PlayFabId: currentPlayerId,
                Statistics: [
                    {
                        "StatisticName": "finished_legendary",
                        "Value": 1
                    }
                ]
            });
        }
        toReturn.error = false;
    }
    return toReturn;
};
handlers["checkLegendary"] = CheckLegendary;
let CheckLegendary2 = function (args, context) {
    let toReturn = { error: true };
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["inventory_data"]
    });
    if (getUserReadOnlyDataResult.Data["inventory_data"] != null) {
        let inventoryData = JSON.parse(getUserReadOnlyDataResult.Data["inventory_data"].Value);
        let isAllMax = true;
        if (inventoryData.OwnedLegendaryRelicList == null || inventoryData.OwnedLegendaryRelicList.length < 7) {
            isAllMax = false;
        }
        else {
            for (let i = 0; i < inventoryData.OwnedLegendaryRelicList.length; i++) {
                if (inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_01" &&
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 < 99) {
                    isAllMax = false;
                }
                if (inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_02" &&
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 < 1000) {
                    isAllMax = false;
                }
                if (inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_03" &&
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 < 50) {
                    isAllMax = false;
                }
                if (inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_04" &&
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 < 300) {
                    isAllMax = false;
                }
                if (inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_05" &&
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 < 300) {
                    isAllMax = false;
                }
                if (inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_06" &&
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 < 300) {
                    isAllMax = false;
                }
                if (inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_07" &&
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 < 200) {
                    isAllMax = false;
                }
            }
        }
        let is02Max = false;
        if (inventoryData.OwnedLegendaryRelicList == null || inventoryData.OwnedLegendaryRelicList.length < 7) {
            is02Max = false;
        }
        else {
            for (let i = 0; i < inventoryData.OwnedLegendaryRelicList.length; i++) {
                if (inventoryData.OwnedLegendaryRelicList[i].id == "legendary_relic_02" &&
                    inventoryData.OwnedLegendaryRelicList[i].level + inventoryData.OwnedLegendaryRelicList[i].count - 1 >= 1000) {
                    is02Max = true;
                }
            }
        }
        if (is02Max && !isAllMax) {
            server.UpdatePlayerStatistics({
                PlayFabId: currentPlayerId,
                Statistics: [
                    {
                        "StatisticName": "finished_legendary",
                        "Value": 2
                    }
                ]
            });
        }
        toReturn.error = false;
    }
    return toReturn;
};
handlers["checkLegendary2"] = CheckLegendary2;
let CheckArenaWeeklyBug = function (args, context) {
    let toReturn = { error: false, removedGems: 0, resultGems: 0, expectedResultGems: 0 };
    let getTitleDataResult = server.GetTitleInternalData({
        Keys: [
            "to_roll_back"
        ]
    });
    var listToUse = JSON.parse(getTitleDataResult.Data["to_roll_back"]);
    if (listToUse != null) {
        let resultOfFilter = listToUse.Data.filter(n => n.id == currentPlayerId);
        if (resultOfFilter.length > 0) {
            toReturn.removedGems = (resultOfFilter[0].count) * 500;
        }
    }
    if (toReturn.removedGems != 0) {
        let getUserInventoryResult = server.GetUserInventory({
            PlayFabId: currentPlayerId
        });
        toReturn.expectedResultGems = getUserInventoryResult.VirtualCurrency["GE"] - toReturn.removedGems;
    }
    return toReturn;
};
handlers["checkArenaWeeklyBug"] = CheckArenaWeeklyBug;
let MoveServer = function (args, context) {
    let toReturn = { error: false };
    let fromServerId = "";
    let toServerId = "";
    let markSJP = false;
    if (args && args.fromServerId)
        fromServerId = args.fromServerId;
    if (args && args.toServerId)
        toServerId = args.toServerId;
    if (args && args.markSJP)
        markSJP = true;
    let statistics = server.GetPlayerStatistics({
        PlayFabId: currentPlayerId,
        StatisticNames: [
            "ranking_" + fromServerId,
            "arena_" + fromServerId,
            "arena_all_season_" + fromServerId,
            "star_" + fromServerId
        ]
    });
    let rankingOriginalValue = 0;
    let rankingOriginal = statistics.Statistics.filter(n => n.StatisticName == "ranking_" + fromServerId);
    if (rankingOriginal.length > 0)
        rankingOriginalValue = rankingOriginal[0].Value;
    let arenaOriginalValue = 0;
    let arenaOriginal = statistics.Statistics.filter(n => n.StatisticName == "arena_" + fromServerId);
    if (arenaOriginal.length > 0)
        arenaOriginalValue = arenaOriginal[0].Value;
    let arenaAllSeasonOriginalValue = 0;
    let arenaAllSeasonOriginal = statistics.Statistics.filter(n => n.StatisticName == "arena_all_season_" + fromServerId);
    if (arenaAllSeasonOriginal.length > 0)
        arenaAllSeasonOriginalValue = arenaAllSeasonOriginal[0].Value;
    let starOriginalValue = 0;
    let starOriginal = statistics.Statistics.filter(n => n.StatisticName == "star_" + fromServerId);
    if (starOriginal.length > 0)
        starOriginalValue = starOriginal[0].Value;
    log.info(rankingOriginalValue.toString());
    log.info(arenaOriginalValue.toString());
    log.info(arenaAllSeasonOriginalValue.toString());
    log.info(starOriginalValue.toString());
    server.UpdatePlayerStatistics({
        PlayFabId: currentPlayerId,
        Statistics: [
            {
                "StatisticName": "arena_" + fromServerId,
                "Value": arenaOriginalValue * -1
            },
            {
                "StatisticName": "arena_all_season_" + fromServerId,
                "Value": arenaAllSeasonOriginalValue * -1
            },
            {
                "StatisticName": "ranking_" + fromServerId,
                "Value": 0
            },
            {
                "StatisticName": "star_" + fromServerId,
                "Value": starOriginalValue * -1
            },
            {
                "StatisticName": "arena_" + toServerId,
                "Value": arenaOriginalValue
            },
            {
                "StatisticName": "arena_all_season_" + toServerId,
                "Value": arenaAllSeasonOriginalValue
            },
            {
                "StatisticName": "ranking_" + toServerId,
                "Value": rankingOriginalValue
            },
            {
                "StatisticName": "star_" + toServerId,
                "Value": starOriginalValue
            }
        ]
    });
    let removePlayerTagResult = server.RemovePlayerTag({
        PlayFabId: currentPlayerId,
        TagName: "Server" + fromServerId
    });
    let addPlayerTagResult = server.AddPlayerTag({
        PlayFabId: currentPlayerId,
        TagName: "Server" + toServerId
    });
    if (markSJP) {
        let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
            PlayFabId: currentPlayerId,
            StatisticName: "arena_" + fromServerId,
            MaxResultsCount: 1,
            Version: 110
        });
        if (getLeaderboardAroundUserResult.Leaderboard[0] != null) {
            let lastSeasonLeaderboardEntry = getLeaderboardAroundUserResult.Leaderboard[0];
            let lastSeasonPosition = lastSeasonLeaderboardEntry.Position;
            let lastSeasonScore = lastSeasonLeaderboardEntry.StatValue;
            let lastSeasonRewardGem = 0;
            if (lastSeasonScore != 0) {
                if (lastSeasonPosition == 0) {
                    lastSeasonRewardGem = 2500;
                }
                else if (lastSeasonPosition == 1) {
                    lastSeasonRewardGem = 2000;
                }
                else if (lastSeasonPosition == 2) {
                    lastSeasonRewardGem = 1500;
                }
                else if (lastSeasonPosition < 10) {
                    lastSeasonRewardGem = 1000;
                }
                else if (lastSeasonPosition < 50) {
                    lastSeasonRewardGem = 750;
                }
                else if (lastSeasonPosition < 100) {
                    lastSeasonRewardGem = 250;
                }
                else {
                    lastSeasonRewardGem = 50;
                }
                GiveEventGemReward({ amount: lastSeasonRewardGem }, context);
            }
        }
        getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
            PlayFabId: currentPlayerId,
            StatisticName: "star_" + fromServerId,
            MaxResultsCount: 1,
            Version: 7
        });
        if (getLeaderboardAroundUserResult.Leaderboard[0] != null) {
            let lastSeasonLeaderboardEntry = getLeaderboardAroundUserResult.Leaderboard[0];
            let lastSeasonScore = lastSeasonLeaderboardEntry.StatValue;
            let lastSeasonRewardGem = 0;
            if (lastSeasonScore != 0) {
                if (lastSeasonScore >= 2000) {
                    lastSeasonRewardGem = 5000;
                }
                else if (lastSeasonScore >= 1500) {
                    lastSeasonRewardGem = 3000;
                }
                else if (lastSeasonScore >= 1000) {
                    lastSeasonRewardGem = 2000;
                }
                else if (lastSeasonScore >= 750) {
                    lastSeasonRewardGem = 1500;
                }
                else if (lastSeasonScore >= 500) {
                    lastSeasonRewardGem = 1000;
                }
                else if (lastSeasonScore >= 250) {
                    lastSeasonRewardGem = 750;
                }
                else {
                    lastSeasonRewardGem = 500;
                }
                GiveEventGemReward({ amount: lastSeasonRewardGem }, context);
            }
        }
    }
    toReturn.error = false;
    return toReturn;
};
handlers["moveServer"] = MoveServer;
let GiveLastSeasonReward = function (args, context) {
    let toReturn = { error: false, gemsRewarded: 0 };
    let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
        PlayFabId: currentPlayerId,
        StatisticName: "star_02",
        MaxResultsCount: 1,
        Version: 7
    });
    if (getLeaderboardAroundUserResult.Leaderboard[0] != null) {
        let lastSeasonLeaderboardEntry = getLeaderboardAroundUserResult.Leaderboard[0];
        let lastSeasonScore = lastSeasonLeaderboardEntry.StatValue;
        let lastSeasonRewardGem = 0;
        if (lastSeasonScore != 0) {
            if (lastSeasonScore >= 2000) {
                lastSeasonRewardGem = 5000;
            }
            else if (lastSeasonScore >= 1500) {
                lastSeasonRewardGem = 3000;
            }
            else if (lastSeasonScore >= 1000) {
                lastSeasonRewardGem = 2000;
            }
            else if (lastSeasonScore >= 750) {
                lastSeasonRewardGem = 1500;
            }
            else if (lastSeasonScore >= 500) {
                lastSeasonRewardGem = 1000;
            }
            else if (lastSeasonScore >= 250) {
                lastSeasonRewardGem = 750;
            }
            else {
                lastSeasonRewardGem = 500;
            }
            GiveEventGemReward({ amount: lastSeasonRewardGem }, context);
            toReturn.gemsRewarded = lastSeasonRewardGem;
        }
    }
    toReturn.error = false;
    return toReturn;
};
handlers["giveLastSeasonReward"] = GiveLastSeasonReward;
let FixJapanese = function (args, context) {
    let toReturn = { error: false, previousSeason: 0, newSeason: 0, previousSeasonWeekly: 0, newSeasonWeekly: 0 };
    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "arena_reward_last_played_season",
            "arena_reward_last_played_season_weekly"
        ]
    });
    let dataToUse = {};
    let needUpdate = false;
    if (getUserInternalDataResult.Data["arena_reward_last_played_season"] != null) {
        toReturn.previousSeason = parseInt(getUserInternalDataResult.Data["arena_reward_last_played_season"].Value);
        if (toReturn.previousSeason > 22) {
            toReturn.newSeason = 21;
            dataToUse["arena_reward_last_played_season"] = 21;
            needUpdate = true;
        }
    }
    if (getUserInternalDataResult.Data["arena_reward_last_played_season_weekly"] != null) {
        toReturn.previousSeasonWeekly = parseInt(getUserInternalDataResult.Data["arena_reward_last_played_season_weekly"].Value);
        if (toReturn.previousSeasonWeekly > 3) {
            toReturn.newSeasonWeekly = 3;
            dataToUse["arena_reward_last_played_season_weekly"] = 3;
            needUpdate = true;
        }
    }
    if (needUpdate) {
        let updateUserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: dataToUse
        });
    }
    toReturn.error = false;
    return toReturn;
};
handlers["fixJapanese"] = FixJapanese;
let LiappUserKey = function (args, context) {
    let toReturn = { error: false, userKey: "" };
    toReturn.userKey = currentPlayerId + "_liapp";
    return toReturn;
};
handlers["liappUserKey"] = LiappUserKey;
let LiappAuth = function (args, context) {
    let toReturn = { error: false, response: "404" };
    let token = "";
    if (args && args.token)
        token = args.token;
    let url = "http://mobirix.lockincomp.com/auth/LiappClientAuthChecker.php";
    let content = "user_key=" + encodeURI(currentPlayerId + "_liapp") + "&token=" + encodeURI(token);
    let httpMethod = "post";
    let contentType = "application/x-www-form-urlencoded";
    toReturn.response = http.request(url, httpMethod, content, contentType);
    if (toReturn.response == "404" || toReturn.response == "300" || toReturn.response == "400") {
        server.UpdatePlayerStatistics({
            PlayFabId: currentPlayerId,
            Statistics: [
                {
                    "StatisticName": "liapp_auth_failed",
                    "Value": parseInt(toReturn.response)
                }
            ]
        });
    }
    return toReturn;
};
handlers["liappAuth"] = LiappAuth;
function GetServerIdModifier(playerId) {
    let getPlayerTagsResult = server.GetPlayerTags({
        PlayFabId: playerId
    });
    let serverId = "";
    if (getPlayerTagsResult != null && getPlayerTagsResult.Tags.length > 0) {
        let serverTags = getPlayerTagsResult.Tags.filter(n => n.indexOf("Server") >= 0);
        if (serverTags != null && serverTags.length > 0)
            serverId = GetServerIdFromTag(serverTags[0]);
        else
            serverId = "";
    }
    else
        serverId = "";
    return (serverId == "") ? "" : "_" + serverId;
}
function GetServerIdModifierFromServerId(serverId) {
    return (serverId == "") ? "" : "_" + serverId;
}
function GetServerIdFromTag(serverId) {
    let convertedServerId = serverId.substring(serverId.length - 2);
    if (convertedServerId == "er")
        convertedServerId = "";
    return convertedServerId;
}
function ApplyAsyncedGems(gemsChange, unsyncedGemChangeVersion, getReadOnlyDataResult) {
    if (gemsChange == 0)
        return false;
    let didCollide = false;
    if (getReadOnlyDataResult.Data["unsynced_gem_change_version"] != null
        && parseInt(getReadOnlyDataResult.Data["unsynced_gem_change_version"].Value) != -1
        && parseInt(getReadOnlyDataResult.Data["unsynced_gem_change_version"].Value) == unsyncedGemChangeVersion) {
        server.UpdatePlayerStatistics({
            PlayFabId: currentPlayerId,
            Statistics: [
                {
                    "StatisticName": "gem_changes_collision",
                    "Value": 1
                }
            ]
        });
        didCollide = true;
        return didCollide;
    }
    let getUserInventoryResult = server.GetUserInventory({
        PlayFabId: currentPlayerId
    });
    if (getUserInventoryResult.VirtualCurrency["GE"] + gemsChange < 0) {
        server.UpdatePlayerStatistics({
            PlayFabId: currentPlayerId,
            Statistics: [
                {
                    "StatisticName": "gem_changes_minus",
                    "Value": 1
                }
            ]
        });
        return didCollide;
    }
    if (gemsChange > 0) {
        let addUserVirtualCurrencyResult = server.AddUserVirtualCurrency({
            PlayFabId: currentPlayerId,
            VirtualCurrency: "GE",
            Amount: gemsChange
        });
    }
    else {
        let subtractUserVirtualCurrencyResult = server.SubtractUserVirtualCurrency({
            PlayFabId: currentPlayerId,
            VirtualCurrency: "GE",
            Amount: gemsChange * -1
        });
    }
    return didCollide;
}
let RequestAttachedItem = function (args, context) {
    let mailId = "";
    let toReturn = { itemId: "", count: 0, error: true };
    if (args.mailId)
        mailId = args.mailId;
    log.info("Mail Id: " + mailId);
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["inbox"]
    });
    if (getUserReadOnlyDataResult.Data["inbox"] != null) {
        let inbox = DeleteOldMail(JSON.parse(getUserReadOnlyDataResult.Data["inbox"].Value), false);
        let targetMailInfo = inbox.MailList.find(n => n.Id == mailId);
        if (targetMailInfo != null) {
            let attachedItemInfo = targetMailInfo.AttachedItem;
            if (attachedItemInfo != null) {
                if (attachedItemInfo.ItemId == "gem" || attachedItemInfo.ItemId == "guild_token")
                    AddVirtualCurrency(attachedItemInfo.ItemCode, Number(attachedItemInfo.Count));
                else
                    return toReturn;
                toReturn.itemId = attachedItemInfo.ItemId;
                toReturn.count = attachedItemInfo.Count;
                toReturn.error = false;
                let updatedMailList = inbox.MailList.filter(n => n.Id != mailId);
                inbox.MailList = updatedMailList;
                let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                    PlayFabId: currentPlayerId,
                    Data: {
                        "inbox": JSON.stringify(inbox)
                    }
                });
                return toReturn;
            }
            else
                return toReturn;
        }
        else
            return toReturn;
    }
    else
        return toReturn;
};
handlers["requestAttachedItem"] = RequestAttachedItem;
let RequestAttachedItemAll = function (args, context) {
    let toReturn = { receiveItemList: {}, error: true };
    let receiveItemList = [];
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["inbox"]
    });
    if (getUserReadOnlyDataResult.Data["inbox"] != null) {
        let inbox = DeleteOldMail(JSON.parse(getUserReadOnlyDataResult.Data["inbox"].Value), false);
        let updatedMailList = inbox.MailList;
        let totalGem = 0;
        let totalGuildToken = 0;
        if (inbox.MailList.length > 0) {
            for (let i = 0; i < inbox.MailList.length; i++) {
                let targetMailInfo = inbox.MailList[i];
                if (targetMailInfo != null) {
                    let attachedItemInfo = targetMailInfo.AttachedItem;
                    if (attachedItemInfo != null) {
                        if (attachedItemInfo.ItemId == "gem")
                            totalGem = totalGem + Number(attachedItemInfo.Count);
                        else if (attachedItemInfo.ItemId == "guild_token")
                            totalGuildToken = totalGuildToken + Number(attachedItemInfo.Count);
                        else
                            return toReturn;
                        let targetItem = receiveItemList.filter(n => n.ItemId == attachedItemInfo.ItemId)[0];
                        if (targetItem != null)
                            targetItem.Count = parseInt(targetItem.Count) + parseInt(attachedItemInfo.Count);
                        else
                            receiveItemList.push({ "ItemId": attachedItemInfo.ItemId, "ItemCode": attachedItemInfo.ItemCode, "Count": attachedItemInfo.Count });
                        updatedMailList = updatedMailList.filter(n => n.Id != targetMailInfo.Id);
                    }
                }
            }
            if (totalGem > 0)
                AddVirtualCurrency("GE", totalGem);
            if (totalGuildToken > 0)
                AddVirtualCurrency("GT", totalGuildToken);
            toReturn.receiveItemList = { "ItemList": receiveItemList };
            inbox.MailList = updatedMailList;
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "inbox": JSON.stringify(inbox)
                }
            });
            toReturn.error = false;
        }
        else
            log.info("inbox is empty now!");
        return toReturn;
    }
    else
        return toReturn;
};
handlers["requestAttachedItemAll"] = RequestAttachedItemAll;
let AddOnTimeEventMail = function (args, context) {
    let message = "";
    let attachedItem = null;
    let addedTime = 259200;
    let toReturn = { error: true };
    if (args.message)
        message = args.message;
    if (args.attachedItem)
        attachedItem = args.attachedItem;
    if (args.addedTime)
        addedTime = args.addedTime;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["inbox"]
    });
    if (getUserReadOnlyDataResult.Data["inbox"] != null) {
        let inbox = JSON.parse(getUserReadOnlyDataResult.Data["inbox"].Value);
        inbox = DeleteOldMail(inbox, true);
        let mailId = 0;
        for (let i = 0; i < inbox.MailList.length; i++) {
            if (Number(inbox.MailList[i].Id) >= mailId) {
                mailId = Number(inbox.MailList[i].Id);
            }
        }
        mailId += 1;
        let currentTimestampSecond = Math.floor(+new Date() / (1000));
        let expiredTime = currentTimestampSecond + addedTime;
        inbox.MailList.push({ Id: mailId, Message: message, AttachedItem: attachedItem, ExpiredTime: expiredTime });
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "inbox": JSON.stringify(inbox)
            }
        });
        toReturn.error = false;
    }
    else {
        let mailId = 0;
        let currentTimestampSecond = Math.floor(+new Date() / (1000));
        let expiredTime = currentTimestampSecond + addedTime;
        let defaultInbox = { "MailList": [] };
        defaultInbox.MailList.push({ Id: mailId, Message: message, AttachedItem: attachedItem, ExpiredTime: expiredTime });
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "inbox": JSON.stringify(defaultInbox)
            }
        });
        toReturn.error = false;
    }
    return toReturn;
};
handlers["addOnTimeEventMail"] = AddOnTimeEventMail;
let AddMail = function (args, context) {
    let message = "";
    let attachedItem = null;
    let toReturn = { error: true };
    if (args.message)
        message = args.message;
    if (args.attachedItem)
        attachedItem = args.attachedItem;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["inbox"]
    });
    if (getUserReadOnlyDataResult.Data["inbox"] != null) {
        let inbox = JSON.parse(getUserReadOnlyDataResult.Data["inbox"].Value);
        inbox = DeleteOldMail(inbox, true);
        let mailId = 0;
        for (let i = 0; i < inbox.MailList.length; i++) {
            if (Number(inbox.MailList[i].Id) >= mailId) {
                mailId = Number(inbox.MailList[i].Id);
            }
        }
        mailId += 1;
        let currentTimestampSecond = Math.floor(+new Date() / (1000));
        let expiredTime = currentTimestampSecond + 2592000;
        inbox.MailList.push({ Id: mailId, Message: message, AttachedItem: attachedItem, ExpiredTime: expiredTime });
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "inbox": JSON.stringify(inbox)
            }
        });
        toReturn.error = false;
    }
    else {
        let mailId = 0;
        let currentTimestampSecond = Math.floor(+new Date() / (1000));
        let expiredTime = currentTimestampSecond + 2592000;
        let defaultInbox = { "MailList": [] };
        defaultInbox.MailList.push({ Id: mailId, Message: message, AttachedItem: attachedItem, ExpiredTime: expiredTime });
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "inbox": JSON.stringify(defaultInbox)
            }
        });
        toReturn.error = false;
    }
    return toReturn;
};
handlers["addMail"] = AddMail;
let AddMailWithId = function (args, context) {
    let targetId = "";
    let message = "";
    let attachedItem = null;
    let toReturn = { error: true };
    if (args.Id)
        targetId = args.Id;
    if (args.message)
        message = args.message;
    if (args.attachedItem)
        attachedItem = args.attachedItem;
    if (targetId == "")
        return toReturn;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: targetId,
        Keys: ["inbox"]
    });
    if (getUserReadOnlyDataResult.Data["inbox"] != null) {
        let inbox = JSON.parse(getUserReadOnlyDataResult.Data["inbox"].Value);
        let mailId = 0;
        for (let i = 0; i < inbox.MailList.length; i++) {
            if (Number(inbox.MailList[i].Id) >= mailId) {
                mailId = Number(inbox.MailList[i].Id);
            }
        }
        mailId += 1;
        let currentTimestampSecond = Math.floor(+new Date() / (1000));
        let expiredTime = currentTimestampSecond + 604800;
        inbox.MailList.push({ Id: mailId, Message: message, AttachedItem: attachedItem, ExpiredTime: expiredTime });
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: targetId,
            Data: {
                "inbox": JSON.stringify(inbox)
            }
        });
        toReturn.error = false;
    }
    else {
        let mailId = 0;
        let currentTimestampSecond = Math.floor(+new Date() / (1000));
        let expiredTime = currentTimestampSecond + 604800;
        let defaultInbox = { "MailList": [] };
        defaultInbox.MailList.push({ Id: mailId, Message: message, AttachedItem: attachedItem, ExpiredTime: expiredTime });
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: targetId,
            Data: {
                "inbox": JSON.stringify(defaultInbox)
            }
        });
        toReturn.error = false;
    }
    return toReturn;
};
handlers["addMailWithId"] = AddMailWithId;
let AddTextMail = function (args, context) {
    let message = "";
    let sender = "";
    let toReturn = { reasonOfError: "", error: true };
    if (args.message)
        message = args.message;
    if (args.sender)
        sender = args.sender;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["inbox", "mute_text_mail"]
    });
    let mailId = 0;
    let currentTimestampSecond = Math.floor(+new Date() / (1000));
    let expiredTime = currentTimestampSecond + 86400;
    let isMute = false;
    if (getUserReadOnlyDataResult.Data["mute_text_mail"] != null)
        isMute = JSON.parse(getUserReadOnlyDataResult.Data["mute_text_mail"].Value);
    if (isMute) {
        toReturn.reasonOfError = "recipient_is_mute";
        toReturn.error = true;
        return toReturn;
    }
    else {
        message = "<s>" + sender + "<m>" + message;
        if (message.length > 100) {
            toReturn.reasonOfError = "length_is_over";
            toReturn.error = true;
            return toReturn;
        }
        if (getUserReadOnlyDataResult.Data["inbox"] != null) {
            let inbox = JSON.parse(getUserReadOnlyDataResult.Data["inbox"].Value);
            inbox = DeleteOldMail(inbox, true);
            for (let i = 0; i < inbox.MailList.length; i++) {
                if (Number(inbox.MailList[i].Id) >= mailId) {
                    mailId = Number(inbox.MailList[i].Id);
                }
            }
            mailId += 1;
            inbox.MailList.push({ Id: mailId, Message: message, AttachedItem: null, ExpiredTime: expiredTime });
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "inbox": JSON.stringify(inbox)
                }
            });
            toReturn.error = false;
        }
        else {
            let defaultInbox = { "MailList": [] };
            defaultInbox.MailList.push({ Id: mailId, Message: message, AttachedItem: null, ExpiredTime: expiredTime });
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "inbox": JSON.stringify(defaultInbox)
                }
            });
            toReturn.error = false;
        }
        return toReturn;
    }
};
handlers["addTextMail"] = AddTextMail;
let RequestDeleteMail = function (args, context) {
    let mailId = "";
    let toReturn = { error: true };
    if (args.mailId)
        mailId = args.mailId;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["inbox"]
    });
    if (getUserReadOnlyDataResult.Data["inbox"] != null) {
        let inbox = JSON.parse(getUserReadOnlyDataResult.Data["inbox"].Value);
        let targetMailInfo = inbox.MailList.find(n => n.Id == mailId);
        if (targetMailInfo != null) {
            let attachedItemInfo = targetMailInfo.AttachedItem;
            let updatedMailList = inbox.MailList.filter(n => n.Id != mailId);
            inbox.MailList = updatedMailList;
            DeleteOldMail(inbox, false);
            toReturn.error = false;
            return toReturn;
        }
        else
            return toReturn;
    }
    else
        return toReturn;
};
handlers["requestDeleteMail"] = RequestDeleteMail;
function DeleteOldMail(currentInbox, skipUpdate) {
    if (currentInbox != null) {
        let currentTimestampSecond = Math.floor(+new Date() / (1000));
        let updatedMailList = currentInbox.MailList;
        for (let i = 0; i < currentInbox.MailList.length; i++) {
            if (Number(currentInbox.MailList[i].ExpiredTime) < currentTimestampSecond) {
                updatedMailList = updatedMailList.filter(n => n.Id != currentInbox.MailList[i].Id);
            }
        }
        currentInbox.MailList = updatedMailList;
        if (!skipUpdate) {
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "inbox": JSON.stringify(currentInbox)
                }
            });
        }
        return currentInbox;
    }
}
let RaidRefresh = function (args, context) {
    let toReturn = { error: true, dailyLimitLeft: 0, dayPassed: 0, hourPassed: 0, currentTimestampHour: 0, currentTimestampDay: 0, minAndroidVersion: "0.0.0", miniOSVersion: "0.0.0",
        goldReward: "0", heartReward: "0", gemReward: 0, raidTicketReward: 0 };
    let goldRewardWeight = "0";
    let heartRewardWeight = "0";
    let gemRewardWeight = 0;
    let raidTicketRewardWeight = 0;
    if (args && args.goldRewardWeight)
        goldRewardWeight = args.goldRewardWeight;
    if (args && args.heartRewardWeight)
        heartRewardWeight = args.heartRewardWeight;
    if (args && args.gemRewardWeight)
        gemRewardWeight = Number(args.gemRewardWeight);
    if (args && args.raidTicketRewardWeight)
        raidTicketRewardWeight = Number(args.raidTicketRewardWeight);
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "setting_json"
        ]
    }).Data;
    if (getTitleDataResult["setting_json"] != null) {
        let convertedData = JSON.parse(getTitleDataResult["setting_json"]);
        toReturn.minAndroidVersion = convertedData.MinAndroidVersion;
        toReturn.miniOSVersion = convertedData.MiniOSVersion;
    }
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: [
            "last_raid_timestamp_second",
            "last_raid_timestamp_hour",
            "last_raid_timestamp_day",
            "last_confirmed_raid_timestamp_hour",
            "last_confirmed_raid_timestamp_day"
        ]
    });
    let lastTimestampHour = -1;
    if (getUserReadOnlyDataResult.Data["last_raid_timestamp_hour"] != null)
        lastTimestampHour = parseInt(getUserReadOnlyDataResult.Data["last_raid_timestamp_hour"].Value);
    let currentTimestampHour = Math.floor(+new Date() / (1000 * 3600));
    toReturn.hourPassed = Math.min(currentTimestampHour - lastTimestampHour, 24);
    let lastTimestampDay = -1;
    if (getUserReadOnlyDataResult.Data["last_raid_timestamp_day"] != null)
        lastTimestampDay = parseInt(getUserReadOnlyDataResult.Data["last_raid_timestamp_day"].Value);
    let currentTimestampDay = Math.floor(+new Date() / (1000 * 3600 * 24));
    toReturn.dayPassed = Math.min(currentTimestampDay - lastTimestampDay, 1);
    let lastTimestampSecond = -1;
    if (getUserReadOnlyDataResult.Data["last_raid_timestamp_second"] != null)
        lastTimestampSecond = parseInt(getUserReadOnlyDataResult.Data["last_raid_timestamp_second"].Value);
    let currentTimestampSecond = Math.floor(+new Date() / (1000));
    let halfMinutePassed = false;
    if (currentTimestampSecond > lastTimestampSecond + 30)
        halfMinutePassed = true;
    let dataToUse = {};
    dataToUse["last_raid_timestamp_hour"] = currentTimestampHour.toString();
    dataToUse["last_raid_timestamp_day"] = currentTimestampDay.toString();
    if (halfMinutePassed) {
        dataToUse["last_raid_timestamp_second"] = currentTimestampSecond.toString();
        let lastSuccessfulTimestampHour = -1;
        if (getUserReadOnlyDataResult.Data["last_confirmed_raid_timestamp_hour"] != null)
            lastSuccessfulTimestampHour = parseInt(getUserReadOnlyDataResult.Data["last_confirmed_raid_timestamp_hour"].Value);
        let lastSuccessfulTimestampDay = -1;
        if (getUserReadOnlyDataResult.Data["last_confirmed_raid_timestamp_day"] != null)
            lastSuccessfulTimestampDay = parseInt(getUserReadOnlyDataResult.Data["last_confirmed_raid_timestamp_day"].Value);
        dataToUse["last_confirmed_raid_timestamp_hour"] = lastTimestampHour.toString();
        dataToUse["last_confirmed_raid_timestamp_day"] = lastTimestampDay.toString();
        if (lastSuccessfulTimestampHour != -1 && lastSuccessfulTimestampDay != -1) {
            if (lastTimestampHour != lastSuccessfulTimestampHour || lastTimestampDay != lastSuccessfulTimestampDay) {
                if (UseDailyLimitIfAvail("raid_rollback", "0", 1, false)) {
                    log.info("Need to rollback raid reward timestamps");
                    lastTimestampHour = Math.min(lastTimestampHour, Math.max(lastTimestampHour - 24, lastSuccessfulTimestampHour));
                    lastTimestampDay = Math.min(lastTimestampDay, Math.max(lastTimestampDay - 1, lastSuccessfulTimestampDay));
                    toReturn.hourPassed = Math.min(currentTimestampHour - lastTimestampHour, 24);
                    toReturn.dayPassed = Math.min(currentTimestampDay - lastTimestampDay, 1);
                }
                server.UpdatePlayerStatistics({
                    PlayFabId: currentPlayerId,
                    Statistics: [
                        {
                            "StatisticName": "raid_rollback",
                            "Value": 1
                        }
                    ]
                });
            }
        }
    }
    let updateuserReadOnlyDataResult = server.UpdateUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Data: dataToUse
    });
    toReturn.currentTimestampHour = currentTimestampHour;
    toReturn.currentTimestampDay = currentTimestampDay;
    toReturn.dailyLimitLeft = GetLeftDailyLimit("raid", "0", true);
    toReturn.error = false;
    if (toReturn.hourPassed > 0 || toReturn.dayPassed > 0) {
        toReturn.goldReward = (BigInt(goldRewardWeight) * BigInt(toReturn.hourPassed)).toString();
        toReturn.heartReward = (BigInt(heartRewardWeight) * BigInt(toReturn.hourPassed)).toString();
        toReturn.gemReward = gemRewardWeight * toReturn.dayPassed;
        toReturn.raidTicketReward = raidTicketRewardWeight * toReturn.dayPassed;
    }
    return toReturn;
};
handlers["raidRefresh"] = RaidRefresh;
let RaidOpen = function (args, context) {
    let toReturn = { error: true, raidIndex: 0, reasonOfError: "" };
    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "raid_index"
        ]
    }).Data;
    if (getUserInternalDataResult["raid_index"] != null)
        toReturn.raidIndex = parseInt(getUserInternalDataResult["raid_index"].Value) + 1;
    let updateuserInternalDataResult = server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: {
            "is_open_raid": "true",
            "is_playing_raid": "false",
            "raid_index": toReturn.raidIndex.toString()
        }
    });
    toReturn.error = false;
    return toReturn;
};
handlers["raidOpen"] = RaidOpen;
let RaidStart = function (args, context) {
    let toReturn = { error: true, reasonOfError: "" };
    let updateuserInternalDataResult = server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: {
            "is_open_raid": "true",
            "is_playing_raid": "true"
        }
    });
    toReturn.error = false;
    return toReturn;
};
handlers["raidStart"] = RaidStart;
let RaidEnd = function (args, context) {
    let toReturn = { error: true, reasonOfError: "" };
    let willGetReward = args.willGetReward;
    if (willGetReward) {
        if (!UseDailyLimitIfAvail("raid", "0", 1, true))
            return toReturn;
    }
    let skillUseCount = 0;
    if (args.skillUseCount)
        skillUseCount = args.skillUseCount;
    let updateuserInternalDataResult = server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: {
            "is_open_raid": "false",
            "is_playing_raid": "false"
        }
    });
    toReturn.error = false;
    return toReturn;
};
handlers["raidEnd"] = RaidEnd;
let RaidLeave = function (args, context) {
    let toReturn = { error: true, reasonOfError: "" };
    let willGetReward = args.willGetReward;
    if (willGetReward) {
        if (!UseDailyLimitIfAvail("raid", "0", 1, true))
            return toReturn;
    }
    let skillUseCount = 0;
    if (args.skillUseCount)
        skillUseCount = args.skillUseCount;
    toReturn.error = false;
    return toReturn;
};
handlers["raidLeave"] = RaidLeave;
let RaidJoin = function (args, context) {
    let toReturn = { error: true, reasonOfError: "" };
    let hostPlayerId = "";
    let hostRaidIndex = 0;
    if (args.hostPlayerId)
        hostPlayerId = args.hostPlayerId;
    if (args.hostRaidIndex)
        hostRaidIndex = args.hostRaidIndex;
    let getHostInternalDataResult = server.GetUserInternalData({
        PlayFabId: hostPlayerId,
        Keys: [
            "is_open_raid",
            "is_playing_raid",
            "raid_index"
        ]
    }).Data;
    if (getHostInternalDataResult["is_playing_raid"] != null && getHostInternalDataResult["is_playing_raid"].Value == "true") {
        toReturn.reasonOfError = "raid_already_playing";
        return toReturn;
    }
    else if (getHostInternalDataResult["is_open_raid"] == null || getHostInternalDataResult["is_open_raid"].Value != "true") {
        toReturn.reasonOfError = "raid_not_open";
        return toReturn;
    }
    else if (getHostInternalDataResult["raid_index"] == null || parseInt(getHostInternalDataResult["raid_index"].Value) != hostRaidIndex) {
        toReturn.reasonOfError = "raid_index_mismatch";
        return toReturn;
    }
    toReturn.error = false;
    return toReturn;
};
handlers["raidJoin"] = RaidJoin;
let RequestAttachedItemSystemMailItemAll = function (args, context) {
    let toReturn = { receiveItemList: {}, updatedSystemPost: "", error: true };
    let receiveItemList = [];
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["inbox_system", "inventory_data"]
    });
    if (getUserReadOnlyDataResult.Data["inbox_system"] != null && getUserReadOnlyDataResult.Data["inventory_data"] != null) {
        let inventoryData = JSON.parse(getUserReadOnlyDataResult.Data["inventory_data"].Value);
        let systemInbox = DeleteOldSystemMail(JSON.parse(getUserReadOnlyDataResult.Data["inbox_system"].Value), false);
        let updatedMailList = systemInbox.MailList;
        let totalGold = BigInt(0);
        let totalHeart = BigInt(0);
        let totalRaidTicket = BigInt(0);
        if (systemInbox.MailList.length > 0) {
            for (let i = 0; i < systemInbox.MailList.length; i++) {
                let targetMailInfo = systemInbox.MailList[i];
                if (targetMailInfo != null) {
                    let attachedItemInfo = targetMailInfo.AttachedItem;
                    if (attachedItemInfo != null) {
                        if (attachedItemInfo.ItemId == "gold")
                            totalGold = totalGold + BigInt(attachedItemInfo.Count);
                        else if (attachedItemInfo.ItemId == "heart")
                            totalHeart = totalHeart + BigInt(attachedItemInfo.Count);
                        else if (attachedItemInfo.ItemId == "ticket_raid")
                            totalRaidTicket = totalRaidTicket + BigInt(attachedItemInfo.Count);
                        else if (attachedItemInfo.ItemId == "weapon" || attachedItemInfo.ItemId == "class" || attachedItemInfo.ItemId == "pet" || attachedItemInfo.ItemId == "mercenary")
                            inventoryData = AddItemToInventoryData(inventoryData, attachedItemInfo.ItemId, attachedItemInfo.ItemCode, Number(attachedItemInfo.Count));
                        else
                            return toReturn;
                        let targetItem = receiveItemList.filter(n => n.ItemId == attachedItemInfo.ItemId)[0];
                        if (targetItem != null) {
                            if (attachedItemInfo.ItemId == "weapon" || attachedItemInfo.ItemId == "class" || attachedItemInfo.ItemId == "pet" || attachedItemInfo.ItemId == "mercenary") {
                                if (attachedItemInfo.ItemCode == targetItem.ItemCode)
                                    targetItem.Count = BigInt(targetItem.Count) + BigInt(attachedItemInfo.Count);
                                else
                                    receiveItemList.push({ "ItemId": attachedItemInfo.ItemId, "ItemCode": attachedItemInfo.ItemCode, "Count": attachedItemInfo.Count });
                            }
                            else
                                targetItem.Count = BigInt(targetItem.Count) + BigInt(attachedItemInfo.Count);
                        }
                        else
                            receiveItemList.push({ "ItemId": attachedItemInfo.ItemId, "ItemCode": attachedItemInfo.ItemCode, "Count": attachedItemInfo.Count });
                        updatedMailList = updatedMailList.filter(n => n.Id != targetMailInfo.Id);
                    }
                }
            }
            systemInbox.MailList = updatedMailList;
            if (totalGold > 0)
                inventoryData = AddBigIntCurrency(inventoryData, "gold", totalGold);
            if (totalHeart > 0)
                inventoryData = AddBigIntCurrency(inventoryData, "heart", totalHeart);
            if (totalRaidTicket > 0)
                inventoryData = AddBigIntCurrency(inventoryData, "ticket_raid", totalRaidTicket);
            inventoryData.TimeStamp = Math.floor(+new Date() / 1000) + 3600 * 24;
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "inventory_data": JSON.stringify(inventoryData),
                    "inbox_system": JSON.stringify(systemInbox),
                    "progress_value": "1"
                }
            });
            toReturn.receiveItemList = { "ItemList": receiveItemList };
            toReturn.updatedSystemPost = JSON.stringify(systemInbox);
            toReturn.error = false;
        }
        else
            log.info("system inbox is empty now!");
        return toReturn;
    }
    else
        return toReturn;
};
handlers["requestAttachedItemSystemMailItemAll"] = RequestAttachedItemSystemMailItemAll;
let AddSystemMail = function (args, context) {
    let message = "";
    let attachedItem = null;
    let toReturn = { error: true };
    if (args.message)
        message = args.message;
    if (args.attachedItem)
        attachedItem = args.attachedItem;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["inbox_system"]
    });
    if (getUserReadOnlyDataResult.Data["inbox_system"] != null) {
        let inbox = JSON.parse(getUserReadOnlyDataResult.Data["inbox_system"].Value);
        inbox = DeleteOldMail(inbox, true);
        let mailId = 0;
        for (let i = 0; i < inbox.MailList.length; i++) {
            if (Number(inbox.MailList[i].Id) >= mailId) {
                mailId = Number(inbox.MailList[i].Id);
            }
        }
        mailId += 1;
        let currentTimestampSecond = Math.floor(+new Date() / (1000));
        let expiredTime = currentTimestampSecond + 2592000;
        inbox.MailList.push({ Id: mailId, Message: message, AttachedItem: attachedItem, ExpiredTime: expiredTime });
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "inbox_system": JSON.stringify(inbox)
            }
        });
        toReturn.error = false;
    }
    else {
        let mailId = 0;
        let currentTimestampSecond = Math.floor(+new Date() / (1000));
        let expiredTime = currentTimestampSecond + 2592000;
        let defaultInbox = { "MailList": [] };
        defaultInbox.MailList.push({ Id: mailId, Message: message, AttachedItem: attachedItem, ExpiredTime: expiredTime });
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "inbox_system": JSON.stringify(defaultInbox)
            }
        });
        toReturn.error = false;
    }
    return toReturn;
};
handlers["addSystemMail"] = AddSystemMail;
function DeleteOldSystemMail(currentInbox, skipUpdate) {
    if (currentInbox != null) {
        let currentTimestampSecond = Math.floor(+new Date() / (1000));
        let updatedMailList = currentInbox.MailList;
        for (let i = 0; i < currentInbox.MailList.length; i++) {
            if (Number(currentInbox.MailList[i].ExpiredTime) < currentTimestampSecond) {
                updatedMailList = updatedMailList.filter(n => n.Id != currentInbox.MailList[i].Id);
            }
        }
        currentInbox.MailList = updatedMailList;
        if (!skipUpdate) {
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "inbox_system": JSON.stringify(currentInbox)
                }
            });
        }
        return currentInbox;
    }
}
function AddBigIntCurrency(inventoryData, currencyId, amount) {
    if (inventoryData != null) {
        if (currencyId == "gold") {
            let originGold = inventoryData.Gold;
            inventoryData.Gold = (BigInt(originGold) + amount).toString();
        }
        else if (currencyId == "heart") {
            let originHeart = inventoryData.Heart;
            inventoryData.Heart = (BigInt(originHeart) + amount).toString();
        }
        else if (currencyId == "ticket_raid") {
            let originTicketRaid = inventoryData.TicketRaid;
            inventoryData.TicketRaid = (BigInt(originTicketRaid) + amount).toString();
        }
    }
    return inventoryData;
}
function AddItemToInventoryData(inventoryData, itemId, itemCode, amount) {
    if (inventoryData != null) {
        if (itemId == "weapon") {
            let targetWeapon = inventoryData.OwnedWeaponList.find(n => n.id == itemCode);
            if (targetWeapon != null)
                targetWeapon.count += amount;
            else {
                targetWeapon = {
                    "id": itemCode,
                    "level": 1,
                    "count": amount,
                    "isNew": true
                };
                inventoryData.OwnedWeaponList.push(targetWeapon);
            }
        }
        else if (itemId == "class") {
            let targetClass = inventoryData.OwnedClassList.find(n => n.id == itemCode);
            if (targetClass != null)
                targetClass.count += amount;
            else {
                targetClass = {
                    "id": itemCode,
                    "level": 1,
                    "count": amount,
                    "isNew": true
                };
                inventoryData.OwnedClassList.push(targetClass);
            }
        }
        else if (itemId == "pet") {
            let targetPet = inventoryData.OwnedPetList.find(n => n.id == itemCode);
            if (targetPet != null)
                targetPet.count += amount;
            else {
                targetPet = {
                    "id": itemCode,
                    "level": 1,
                    "count": amount,
                    "isNew": true,
                    "goldLevelUpCount": 0,
                    "heartLevelUpCount": 0,
                    "mergeLevelUpCount": 0
                };
                inventoryData.OwnedPetList.push(targetPet);
            }
        }
        else if (itemId == "mercenary") {
            if (inventoryData.OwnedMercenaryList != null) {
                let targetMercenary = inventoryData.OwnedMercenaryList.find(n => n.id == itemCode);
                if (targetMercenary != null)
                    targetMercenary.count += amount;
                else {
                    targetMercenary = {
                        "id": itemCode,
                        "level": 1,
                        "count": amount,
                        "isNew": true
                    };
                    inventoryData.OwnedMercenaryList.push(targetMercenary);
                }
            }
            else {
                let OwnedMercenaryList = [];
                let targetMercenary = {
                    "id": itemCode,
                    "level": 1,
                    "count": amount,
                    "isNew": true
                };
                OwnedMercenaryList.push(targetMercenary);
                inventoryData["OwnedMercenaryList"] = OwnedMercenaryList;
            }
        }
    }
    return inventoryData;
}
let TowerRefresh = function (args, context) {
    let toReturn = { error: true, collectedGems: 0, collectedFeathers: 0, currentFloor: 0, secondLeft: 0, minAndroidVersion: "0.0.0", miniOSVersion: "0.0.0",
        rewardRate: 0, rewardFeatherRate: 0, defenceRecords: [], cooldownLeft: 0, cooldownDeployLeft: 0, dailyLimitLeft: 0 };
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "setting_json"
        ]
    }).Data;
    if (getTitleDataResult["setting_json"] != null) {
        let convertedData = JSON.parse(getTitleDataResult["setting_json"]);
        toReturn.minAndroidVersion = convertedData.MinAndroidVersion;
        toReturn.miniOSVersion = convertedData.MiniOSVersion;
    }
    let currentTimestampSecond = Math.floor(+new Date() / (1000));
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: [
            "tower_data_1",
            "tower_last_timestamp_second"
        ]
    });
    let lastTimestampSecond = 0;
    if (getUserReadOnlyDataResult.Data["tower_last_timestamp_second"] != null)
        lastTimestampSecond = parseInt(getUserReadOnlyDataResult.Data["tower_last_timestamp_second"].Value);
    let towerDataList = GetTowerDataList(getUserReadOnlyDataResult);
    towerDataList = ProcessSettlementTowerReward(towerDataList, lastTimestampSecond);
    toReturn.collectedGems = towerDataList[0].CollectedGems;
    toReturn.collectedFeathers = towerDataList[0].CollectedFeathers;
    toReturn.currentFloor = towerDataList[0].Floor;
    toReturn.secondLeft = Math.floor(Math.max(towerDataList[0].DeployedTimestamp + 14400 - currentTimestampSecond, 0));
    toReturn.defenceRecords = towerDataList[0].BattleRecord;
    toReturn.rewardRate = GetTowerRewardRate(toReturn.currentFloor);
    toReturn.rewardFeatherRate = GetTowerRewardFeatherRate(toReturn.currentFloor);
    toReturn.cooldownLeft = GetCooldown("tower_battle");
    toReturn.cooldownDeployLeft = GetCooldown("tower_deploy");
    let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Data: {
            "tower_data_1": unescape(JSON.stringify(towerDataList[0])),
            "tower_last_timestamp_second": currentTimestampSecond.toString()
        },
        Permission: "Public"
    });
    toReturn.dailyLimitLeft = GetLeftDailyLimit("tower", "0", true);
    toReturn.error = false;
    return toReturn;
};
handlers["towerRefresh"] = TowerRefresh;
let TowerRefreshFloors = function (args, context) {
    let toReturn = { error: true, floorPlayersCount: [], floorPlayers: [] };
    let serverIdModifier = GetServerIdModifier(currentPlayerId);
    let currentTimestamp = Math.floor(+new Date() / (1000));
    toReturn.floorPlayersCount.push(0);
    toReturn.floorPlayers.push({});
    for (let i = 2; i <= 11; i++) {
        let getLeaderboardResult = server.GetLeaderboard({
            StatisticName: "tower_floor_" + i.toString() + serverIdModifier,
            StartPosition: 0,
            MaxResultsCount: 100
        });
        let playerInfoForFloor = getLeaderboardResult.Leaderboard.filter(n => n.StatValue + 240 > currentTimestamp / 60);
        toReturn.floorPlayersCount.push(playerInfoForFloor.length);
        let playerListForFloor = {};
        for (let j = 0; j < playerInfoForFloor.length; j++) {
            playerListForFloor[playerInfoForFloor[j].PlayFabId] = playerInfoForFloor[j].DisplayName;
        }
        toReturn.floorPlayers.push(playerListForFloor);
    }
    toReturn.error = false;
    return toReturn;
};
handlers["towerRefreshFloors"] = TowerRefreshFloors;
let TowerDeploy = function (args, context) {
    let toReturn = { error: true, reasonOfError: "", collectedGems: 0, collectedFeathers: 0, currentFloor: 0, secondLeft: 0, rewardRate: 0, rewardFeatherRate: 0, defenceRecords: [] };
    let towerDeployCooldown = GetCooldown("tower_deploy");
    if (towerDeployCooldown != 0) {
        log.info("Cooldown is not complete!");
        toReturn.reasonOfError = "left_cooldown";
        return toReturn;
    }
    if (!UseDailyLimitIfAvail("tower", "0", 1))
        return toReturn;
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: [
            "tower_data_1"
        ]
    });
    let towerDataList = GetTowerDataList(getUserReadOnlyDataResult);
    if (towerDataList[0].Floor != 0)
        return toReturn;
    let currentTimestampSecond = Math.floor(+new Date() / (1000));
    let dataToUse = {};
    towerDataList[0] = {
        "Floor": 1,
        "DeployedTimestamp": 0,
        "CollectedGems": 0,
        "collectedFeathers": 0,
        "BattleRecord": [
            {
                "Name": "",
                "Win": true,
                "Floor": 1,
                "IsDefense": false,
                "TimeStamp": Math.floor(currentTimestampSecond / 60)
            }
        ]
    };
    towerDataList[0].DeployedTimestamp = currentTimestampSecond;
    dataToUse["tower_data_1"] = unescape(JSON.stringify(towerDataList[0]));
    let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Data: dataToUse,
        Permission: "Public"
    });
    let serverIdModifier = GetServerIdModifier(currentPlayerId);
    UpdateTowerLeaderboard(serverIdModifier, towerDataList, currentPlayerId);
    toReturn.collectedGems = towerDataList[0].CollectedGems;
    toReturn.collectedFeathers = towerDataList[0].CollectedFeathers;
    toReturn.currentFloor = towerDataList[0].Floor;
    toReturn.secondLeft = Math.floor(Math.max(towerDataList[0].DeployedTimestamp + 14400 - currentTimestampSecond, 0));
    toReturn.defenceRecords = towerDataList[0].BattleRecord;
    toReturn.rewardRate = GetTowerRewardRate(toReturn.currentFloor);
    toReturn.rewardFeatherRate = GetTowerRewardFeatherRate(toReturn.currentFloor);
    toReturn.error = false;
    return toReturn;
};
handlers["towerDeploy"] = TowerDeploy;
let TowerRetreat = function (args, context) {
    let toReturn = { error: true, rewardGem: 0, rewardFeather: 0, collectedGems: 0, collectedFeathers: 0, currentFloor: 0, secondLeft: 0, rewardRate: 0, rewardFeatherRate: 0, defenceRecords: [], cooldownDeployLeft: 0, dailyLimitLeft: 0 };
    StartCooldown("tower_deploy");
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: [
            "tower_data_1",
            "tower_last_timestamp_second"
        ]
    });
    let lastTimestampSecond = 0;
    if (getUserReadOnlyDataResult.Data["tower_last_timestamp_second"] != null)
        lastTimestampSecond = parseInt(getUserReadOnlyDataResult.Data["tower_last_timestamp_second"].Value);
    let towerDataList = GetTowerDataList(getUserReadOnlyDataResult);
    if (towerDataList[0].Floor == 0)
        return toReturn;
    towerDataList = ProcessSettlementTowerReward(towerDataList, lastTimestampSecond);
    toReturn.rewardGem = Math.floor(towerDataList[0].CollectedGems);
    toReturn.rewardFeather = Math.floor(towerDataList[0].CollectedFeathers);
    AddVirtualCurrency("GE", toReturn.rewardGem);
    AddVirtualCurrency("FE", toReturn.rewardFeather);
    towerDataList[0] = {
        "Floor": 0,
        "DeployedTimestamp": 0,
        "CollectedGems": 0,
        "collectedFeathers": 0,
        "BattleRecord": []
    };
    let currentTimestampSecond = Math.floor(+new Date() / (1000));
    let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Data: {
            "tower_data_1": unescape(JSON.stringify(towerDataList[0])),
            "tower_last_timestamp_second": currentTimestampSecond.toString()
        },
        Permission: "Public"
    });
    let serverIdModifier = GetServerIdModifier(currentPlayerId);
    UpdateTowerLeaderboard(serverIdModifier, towerDataList, currentPlayerId);
    toReturn.collectedGems = towerDataList[0].CollectedGems;
    toReturn.collectedFeathers = towerDataList[0].CollectedFeathers;
    toReturn.currentFloor = towerDataList[0].Floor;
    toReturn.secondLeft = Math.floor(Math.max(towerDataList[0].DeployedTimestamp + 14400 - currentTimestampSecond, 0));
    toReturn.defenceRecords = towerDataList[0].BattleRecord;
    toReturn.rewardRate = GetTowerRewardRate(toReturn.currentFloor);
    toReturn.rewardFeatherRate = GetTowerRewardFeatherRate(toReturn.currentFloor);
    toReturn.cooldownDeployLeft = GetCooldown("tower_deploy");
    toReturn.dailyLimitLeft = GetLeftDailyLimit("tower", "0", true);
    toReturn.error = false;
    return toReturn;
};
handlers["towerRetreat"] = TowerRetreat;
let TowerBattleStart = function (args, context) {
    let toReturn = { error: true, opponentUserData: "", opponentInventoryData: "", opponentBonusStat: 0, canSkipBecauseOpponentFinished: false, reasonOfError: "" };
    let opponentId = "";
    if (args.opponentId)
        opponentId = args.opponentId;
    else
        return toReturn;
    if (GetCooldown("tower_battle") != 0) {
        log.info("Cooldown is not complete!");
        toReturn.reasonOfError = "left_cooldown";
        return toReturn;
    }
    StartCooldown("tower_battle");
    let opponentReadOnlyData = server.GetUserReadOnlyData({
        PlayFabId: opponentId,
        Keys: [
            "inventory_data",
            "user_data",
            "tower_data_1",
            "tower_last_timestamp_second"
        ]
    });
    toReturn.opponentUserData = opponentReadOnlyData.Data["user_data"].Value;
    toReturn.opponentInventoryData = opponentReadOnlyData.Data["inventory_data"].Value;
    let opponentTowerDataList = GetTowerDataList(opponentReadOnlyData);
    let serverIdModifier = GetServerIdModifier(opponentId);
    UpdateTowerLeaderboard(serverIdModifier, opponentTowerDataList, opponentId);
    let currentTimestampSecond = Math.floor(+new Date() / (1000));
    if (opponentTowerDataList[0].DeployedTimestamp + 14400 <= currentTimestampSecond) {
        toReturn.canSkipBecauseOpponentFinished = true;
        toReturn.error = false;
        return toReturn;
    }
    let updateUserInternalDataResult = server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: {
            "is_playing_tower_battle": "true",
            "tower_battle_opponent_id": opponentId
        }
    });
    let getUserInventoryResult = server.GetUserInventory({
        PlayFabId: opponentId
    });
    toReturn.opponentBonusStat = getUserInventoryResult.VirtualCurrency["BS"].valueOf();
    toReturn.error = false;
    return toReturn;
};
handlers["towerBattleStart"] = TowerBattleStart;
let TowerBattleEnd = function (args, context) {
    let toReturn = { error: true };
    let didWin = false;
    if (args.isWin)
        didWin = args.isWin;
    let isSuspiciousIfWin = false;
    if (args.isSuspiciousIfWin)
        isSuspiciousIfWin = args.isSuspiciousIfWin;
    if (isSuspiciousIfWin) {
        server.UpdatePlayerStatistics({
            PlayFabId: currentPlayerId,
            Statistics: [
                {
                    "StatisticName": "Cheater3",
                    "Value": 1
                }
            ]
        });
    }
    let serverIdModifier = GetServerIdModifier(currentPlayerId);
    let isValidCompletion = false;
    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "is_playing_tower_battle",
            "tower_battle_opponent_id"
        ]
    }).Data;
    if (getUserInternalDataResult["is_playing_tower_battle"] && getUserInternalDataResult["is_playing_tower_battle"].Value == "true")
        isValidCompletion = true;
    if (!isValidCompletion)
        return toReturn;
    let opponentId = "";
    if (getUserInternalDataResult["tower_battle_opponent_id"])
        opponentId = getUserInternalDataResult["tower_battle_opponent_id"].Value;
    let updateUserInternalDataResult = server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: {
            "is_playing_tower_battle": "false"
        }
    });
    let getOpponentReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: opponentId,
        Keys: [
            "tower_data_1",
            "tower_last_timestamp_second"
        ]
    });
    let opponentTowerDataList = GetTowerDataList(getOpponentReadOnlyDataResult);
    let opponentLastTimestampSecond = 0;
    if (getOpponentReadOnlyDataResult.Data["tower_last_timestamp_second"] != null)
        opponentLastTimestampSecond = parseInt(getOpponentReadOnlyDataResult.Data["tower_last_timestamp_second"].Value);
    let getMyReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: [
            "tower_data_1",
            "tower_last_timestamp_second"
        ]
    });
    let myTowerDataList = GetTowerDataList(getMyReadOnlyDataResult);
    let targetFloor = myTowerDataList[0].Floor + 1;
    if (targetFloor <= 1 || targetFloor > 11)
        return toReturn;
    let myLastTimestampSecond = 0;
    if (getMyReadOnlyDataResult.Data["tower_last_timestamp_second"] != null)
        myLastTimestampSecond = parseInt(getMyReadOnlyDataResult.Data["tower_last_timestamp_second"].Value);
    if (didWin) {
        opponentTowerDataList = ProcessSettlementTowerReward(opponentTowerDataList, opponentLastTimestampSecond);
        myTowerDataList = ProcessSettlementTowerReward(myTowerDataList, myLastTimestampSecond);
        opponentTowerDataList[0].Floor = targetFloor - 1;
        UpdateTowerLeaderboard(serverIdModifier, opponentTowerDataList, opponentId);
        myTowerDataList[0].Floor = targetFloor;
        UpdateTowerLeaderboard(serverIdModifier, myTowerDataList, currentPlayerId);
    }
    let currentTimestampSecond = Math.floor(+new Date() / (1000));
    let getMyAccountInfoResult = server.GetUserAccountInfo({
        PlayFabId: currentPlayerId
    });
    opponentTowerDataList[0].BattleRecord.push({
        "Name": getMyAccountInfoResult.UserInfo.TitleInfo.DisplayName,
        "Win": !didWin,
        "Floor": opponentTowerDataList[0].Floor,
        "IsDefense": true,
        "TimeStamp": Math.floor(currentTimestampSecond / 60)
    });
    if (opponentTowerDataList[0].BattleRecord.length > 15)
        opponentTowerDataList[0].BattleRecord.shift();
    let getOpponetAccountInfoResult = server.GetUserAccountInfo({
        PlayFabId: opponentId
    });
    myTowerDataList[0].BattleRecord.push({
        "Name": getOpponetAccountInfoResult.UserInfo.TitleInfo.DisplayName,
        "Win": didWin,
        "Floor": myTowerDataList[0].Floor,
        "IsDefense": false,
        "TimeStamp": Math.floor(currentTimestampSecond / 60)
    });
    if (myTowerDataList[0].BattleRecord.length > 15)
        myTowerDataList[0].BattleRecord.shift();
    if (didWin) {
        let updateOpponentReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: opponentId,
            Data: {
                "tower_data_1": unescape(JSON.stringify(opponentTowerDataList[0])),
                "tower_last_timestamp_second": currentTimestampSecond.toString()
            },
            Permission: "Public"
        });
        let updateMyReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "tower_data_1": unescape(JSON.stringify(myTowerDataList[0])),
                "tower_last_timestamp_second": currentTimestampSecond.toString()
            },
            Permission: "Public"
        });
    }
    else {
        let updateOpponentReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: opponentId,
            Data: {
                "tower_data_1": unescape(JSON.stringify(opponentTowerDataList[0])),
            },
            Permission: "Public"
        });
        let updateMyReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "tower_data_1": unescape(JSON.stringify(myTowerDataList[0])),
            },
            Permission: "Public"
        });
    }
    toReturn.error = false;
    return toReturn;
};
handlers["towerBattleEnd"] = TowerBattleEnd;
let TowerAdvanceWithNoBattle = function (args, context) {
    let toReturn = { error: true, collectedGems: 0, collectedFeathers: 0, currentFloor: 0, secondLeft: 0, rewardRate: 0, rewardFeatherRate: 0, defenceRecords: [] };
    let serverIdModifier = GetServerIdModifier(currentPlayerId);
    let getMyReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: [
            "tower_data_1",
            "tower_last_timestamp_second"
        ]
    });
    let myTowerDataList = GetTowerDataList(getMyReadOnlyDataResult);
    if (myTowerDataList[0].Floor >= 11 || myTowerDataList[0].Floor <= 0)
        return toReturn;
    let myLastTimestampSecond = 0;
    if (getMyReadOnlyDataResult.Data["tower_last_timestamp_second"] != null)
        myLastTimestampSecond = parseInt(getMyReadOnlyDataResult.Data["tower_last_timestamp_second"].Value);
    myTowerDataList = ProcessSettlementTowerReward(myTowerDataList, myLastTimestampSecond);
    myTowerDataList[0].Floor = myTowerDataList[0].Floor + 1;
    UpdateTowerLeaderboard(serverIdModifier, myTowerDataList, currentPlayerId);
    let currentTimestampSecond = Math.floor(+new Date() / (1000));
    myTowerDataList[0].BattleRecord.push({
        "Name": "",
        "Win": true,
        "Floor": myTowerDataList[0].Floor,
        "IsDefense": false,
        "TimeStamp": Math.floor(currentTimestampSecond / 60)
    });
    if (myTowerDataList[0].BattleRecord.length > 15)
        myTowerDataList[0].BattleRecord.shift();
    let updateMyReadOnlyDataResult = server.UpdateUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Data: {
            "tower_data_1": unescape(JSON.stringify(myTowerDataList[0])),
            "tower_last_timestamp_second": currentTimestampSecond.toString()
        },
        Permission: "Public"
    });
    toReturn.collectedGems = myTowerDataList[0].CollectedGems;
    toReturn.collectedFeathers = myTowerDataList[0].CollectedFeathers;
    toReturn.currentFloor = myTowerDataList[0].Floor;
    toReturn.secondLeft = Math.floor(Math.max(myTowerDataList[0].DeployedTimestamp + 14400 - currentTimestampSecond, 0));
    toReturn.defenceRecords = myTowerDataList[0].BattleRecord;
    toReturn.rewardRate = GetTowerRewardRate(toReturn.currentFloor);
    toReturn.rewardFeatherRate = GetTowerRewardFeatherRate(toReturn.currentFloor);
    toReturn.error = false;
    return toReturn;
};
handlers["towerAdvanceWithNoBattle"] = TowerAdvanceWithNoBattle;
let TowerRemoveFeather = function (args, context) {
    let toReturn = { error: true };
    let currentInventory = server.GetUserInventory({
        PlayFabId: currentPlayerId
    });
    log.info(currentInventory.VirtualCurrency["FE"].toString());
    if (currentInventory.VirtualCurrency["FE"] != 0)
        SubtractVirtualCurrency("FE", currentInventory.VirtualCurrency["FE"]);
    toReturn.error = false;
    return toReturn;
};
handlers["towerRemoveFeather"] = TowerRemoveFeather;
function UpdateTowerLeaderboard(serverIdModifier, towerDataList, playerId) {
    let floor2Count = 0;
    let floor3Count = 0;
    let floor4Count = 0;
    let floor5Count = 0;
    let floor6Count = 0;
    let floor7Count = 0;
    let floor8Count = 0;
    let floor9Count = 0;
    let floor10Count = 0;
    let floor11Count = 0;
    let currentTimestampSecond = Math.floor(+new Date() / (1000));
    let floorToCheck = 0;
    let timeStampToCheck = 0;
    if (towerDataList[0] != null) {
        floorToCheck = towerDataList[0].Floor;
        timeStampToCheck = towerDataList[0].DeployedTimestamp;
    }
    if (currentTimestampSecond <= timeStampToCheck + 14400) {
        if (floorToCheck == 2)
            floor2Count += Math.floor(timeStampToCheck / 60);
        else if (floorToCheck == 3)
            floor3Count += Math.floor(timeStampToCheck / 60);
        else if (floorToCheck == 4)
            floor4Count += Math.floor(timeStampToCheck / 60);
        else if (floorToCheck == 5)
            floor5Count += Math.floor(timeStampToCheck / 60);
        else if (floorToCheck == 6)
            floor6Count += Math.floor(timeStampToCheck / 60);
        else if (floorToCheck == 7)
            floor7Count += Math.floor(timeStampToCheck / 60);
        else if (floorToCheck == 8)
            floor8Count += Math.floor(timeStampToCheck / 60);
        else if (floorToCheck == 9)
            floor9Count += Math.floor(timeStampToCheck / 60);
        else if (floorToCheck == 10)
            floor10Count += Math.floor(timeStampToCheck / 60);
        else if (floorToCheck == 11)
            floor11Count += Math.floor(timeStampToCheck / 60);
    }
    server.UpdatePlayerStatistics({
        PlayFabId: playerId,
        Statistics: [
            {
                "StatisticName": "tower_floor_2" + serverIdModifier,
                "Value": floor2Count
            },
            {
                "StatisticName": "tower_floor_3" + serverIdModifier,
                "Value": floor3Count
            },
            {
                "StatisticName": "tower_floor_4" + serverIdModifier,
                "Value": floor4Count
            },
            {
                "StatisticName": "tower_floor_5" + serverIdModifier,
                "Value": floor5Count
            },
            {
                "StatisticName": "tower_floor_6" + serverIdModifier,
                "Value": floor6Count
            },
            {
                "StatisticName": "tower_floor_7" + serverIdModifier,
                "Value": floor7Count
            },
            {
                "StatisticName": "tower_floor_8" + serverIdModifier,
                "Value": floor8Count
            },
            {
                "StatisticName": "tower_floor_9" + serverIdModifier,
                "Value": floor9Count
            },
            {
                "StatisticName": "tower_floor_10" + serverIdModifier,
                "Value": floor10Count
            },
            {
                "StatisticName": "tower_floor_11" + serverIdModifier,
                "Value": floor11Count
            }
        ]
    });
}
function ProcessSettlementTowerReward(towerDataList, lastTimestampSecond) {
    let currentTimestampSecond = Math.floor(+new Date() / (1000));
    if (lastTimestampSecond != 0 && currentTimestampSecond > lastTimestampSecond) {
        let passedSeconds = Math.max(Math.min(currentTimestampSecond - lastTimestampSecond, 14400), 0);
        if (towerDataList[0].DeployedTimestamp + 14400 <= currentTimestampSecond)
            passedSeconds = Math.max(Math.min(towerDataList[0].DeployedTimestamp + 14400 - lastTimestampSecond, 14400), 0);
        let reward = GetTowerRewardRate(towerDataList[0].Floor) * passedSeconds / 3600;
        let rewardFeather = GetTowerRewardFeatherRate(towerDataList[0].Floor) * passedSeconds / 3600;
        towerDataList[0].CollectedGems += reward;
        towerDataList[0].CollectedFeathers += rewardFeather;
    }
    return towerDataList;
}
function GetTowerDataList(readOnlyDataResult) {
    let towerDataList = [];
    let towerData = {};
    if (readOnlyDataResult.Data["tower_data_1"] != null) {
        towerData = JSON.parse(readOnlyDataResult.Data["tower_data_1"].Value);
        if (towerData["CollectedGems"] == null)
            towerData["CollectedGems"] = 0;
        if (towerData["CollectedFeathers"] == null)
            towerData["CollectedFeathers"] = 0;
    }
    else {
        towerData = {
            "Floor": 0,
            "DeployedTimestamp": 0,
            "CollectedGems": 0,
            "CollectedFeathers": 0,
            "BattleRecord": []
        };
    }
    towerDataList.push(towerData);
    return towerDataList;
}
function GetTowerRewardRate(floor) {
    let rewardFactorByFloor = 0;
    if (floor == 1)
        rewardFactorByFloor = 15;
    else if (floor == 2)
        rewardFactorByFloor = 20;
    else if (floor == 3)
        rewardFactorByFloor = 25;
    else if (floor == 4)
        rewardFactorByFloor = 30;
    else if (floor == 5)
        rewardFactorByFloor = 35;
    else if (floor == 6)
        rewardFactorByFloor = 40;
    else if (floor == 7)
        rewardFactorByFloor = 45;
    else if (floor == 8)
        rewardFactorByFloor = 50;
    else if (floor == 9)
        rewardFactorByFloor = 55;
    else if (floor == 10)
        rewardFactorByFloor = 60;
    else if (floor == 11)
        rewardFactorByFloor = 70;
    return rewardFactorByFloor;
}
function GetTowerRewardFeatherRate(floor) {
    let rewardFactorByFloor = 0;
    if (floor == 1)
        rewardFactorByFloor = 20;
    else if (floor == 2)
        rewardFactorByFloor = 25;
    else if (floor == 3)
        rewardFactorByFloor = 35;
    else if (floor == 4)
        rewardFactorByFloor = 45;
    else if (floor == 5)
        rewardFactorByFloor = 55;
    else if (floor == 6)
        rewardFactorByFloor = 65;
    else if (floor == 7)
        rewardFactorByFloor = 75;
    else if (floor == 8)
        rewardFactorByFloor = 85;
    else if (floor == 9)
        rewardFactorByFloor = 95;
    else if (floor == 10)
        rewardFactorByFloor = 105;
    else if (floor == 11)
        rewardFactorByFloor = 115;
    return rewardFactorByFloor;
}
let LeftWeeklyLimit = function (args, context) {
    let name = "";
    let id = "";
    let toReturn = { error: true, leftCount: 0 };
    if (args.name)
        name = args.name;
    else
        return toReturn;
    if (args.id)
        id = args.id;
    else
        return toReturn;
    toReturn.leftCount = GetLeftWeeklyLimit(name, id);
    toReturn.error = false;
    return toReturn;
};
handlers["leftWeeklyLimit"] = LeftWeeklyLimit;
let WeeklyAllowedCountAndUsedCounts = function (args, context) {
    let name = "";
    let toReturn = { error: true, result: {} };
    if (args.name)
        name = args.name;
    else
        return toReturn;
    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "weekly_limit_use_" + name,
            "weekly_limit_last_timestamp"
        ]
    });
    let hasReset = (ResetWeeklyLimitIfNeeded(getUserInternalDataResult));
    let allowedCount = 0;
    if (name == "weekly_package_1")
        allowedCount = 1;
    else if (name == "weekly_package_3")
        allowedCount = 3;
    let dictionaryToUse = {};
    if (getUserInternalDataResult.Data["weekly_limit_use_" + name] != null) {
        dictionaryToUse = JSON.parse(getUserInternalDataResult.Data["weekly_limit_use_" + name].Value);
    }
    let resultItems = [];
    for (let tmp in dictionaryToUse) {
        let count = dictionaryToUse[tmp];
        if (hasReset)
            count = 0;
        resultItems.push({
            "ProgressId": tmp,
            "AllowedCount": allowedCount,
            "UsedCount": count
        });
    }
    toReturn.result = { "WeeklyLimitAndUseCountList": resultItems };
    toReturn.error = false;
    return toReturn;
};
handlers["weeklyAllowedCountAndUsedCounts"] = WeeklyAllowedCountAndUsedCounts;
function GetLeftWeeklyLimit(name, id, allowMinus = false) {
    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "weekly_limit_use_" + name,
            "weekly_limit_last_timestamp"
        ]
    });
    let hasReset = ResetWeeklyLimitIfNeeded(getUserInternalDataResult);
    let allowedCount = 0;
    if (name == "weekly_package_1")
        allowedCount = 1;
    else if (name == "weekly_package_3")
        allowedCount = 3;
    let usedCount = 0;
    let dictionaryToUse = {};
    if (getUserInternalDataResult.Data["weekly_limit_use_" + name] != null) {
        dictionaryToUse = JSON.parse(getUserInternalDataResult.Data["weekly_limit_use_" + name].Value);
        if (dictionaryToUse[id] != null)
            usedCount = parseInt(dictionaryToUse[id]);
        if (hasReset)
            usedCount = 0;
    }
    if (allowMinus)
        return allowedCount - usedCount;
    else
        return Math.max(allowedCount - usedCount, 0);
}
function UseWeeklyLimitIfAvail(name, id, count, allowMinus = false) {
    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "weekly_limit_use_" + name,
            "weekly_limit_last_timestamp"
        ]
    });
    let hasReset = ResetWeeklyLimitIfNeeded(getUserInternalDataResult);
    let allowedCount = 0;
    if (name == "weekly_package_1")
        allowedCount = 1;
    else if (name == "weekly_package_3")
        allowedCount = 3;
    let usedCount = 0;
    let dictionaryToUse = {};
    if (getUserInternalDataResult.Data["weekly_limit_use_" + name] != null) {
        dictionaryToUse = JSON.parse(getUserInternalDataResult.Data["weekly_limit_use_" + name].Value);
        if (dictionaryToUse[id] != null)
            usedCount = parseInt(dictionaryToUse[id]);
        if (hasReset)
            usedCount = 0;
    }
    if (allowedCount >= usedCount + count || allowMinus) {
        if (dictionaryToUse[id] != null)
            dictionaryToUse[id] = dictionaryToUse[id] + count;
        else
            dictionaryToUse[id] = count;
        if (hasReset)
            dictionaryToUse[id] = count;
        let dataToUse = {};
        dataToUse["weekly_limit_use_" + name] = JSON.stringify(dictionaryToUse);
        let updateUserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: dataToUse
        });
        return true;
    }
    else {
        return false;
    }
}
function ResetWeeklyLimitIfNeeded(internalDataResult) {
    let currentTimestampWeek = Math.floor((Math.floor(+new Date() / (1000 * 60 * 60 * 24)) - 4) / 7);
    let lastTimestampWeek = 0;
    if (internalDataResult.Data["weekly_limit_last_timestamp"] != null) {
        lastTimestampWeek = parseInt(internalDataResult.Data["weekly_limit_last_timestamp"].Value);
    }
    if (currentTimestampWeek > lastTimestampWeek) {
        log.info("Week has been changed. Need to reset weekly limit!");
        let dataToUse = {};
        dataToUse["weekly_limit_use_weekly_package_1"] = "{}";
        dataToUse["weekly_limit_use_weekly_package_3"] = "{}";
        dataToUse["weekly_limit_last_timestamp"] = currentTimestampWeek.toString();
        let updateUserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: dataToUse
        });
        return true;
    }
    else {
        return false;
    }
}
function ForceResetWeeklyLimit(name) {
    let dataToUse = {};
    dataToUse["weekly_limit_use_" + name] = "{}";
    let updateUserInternalDataResult = server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: dataToUse
    });
}
let ResetWingStat = function (args, context) {
    let targetWingId = "";
    let lockStatCount = 0;
    let isLockOwnedStat1 = false;
    let isLockOwnedStat2 = false;
    let isLockEquipmentStat1 = false;
    let isLockEquipmentStat2 = false;
    let isLockEquipmentStat3 = false;
    let toReturn = { error: true, requiredGem: 0 };
    if (args.targetWingId)
        targetWingId = args.targetWingId;
    if (args.isLockOwnedStat1)
        isLockOwnedStat1 = args.isLockOwnedStat1;
    if (args.isLockOwnedStat2)
        isLockOwnedStat2 = args.isLockOwnedStat2;
    if (args.isLockEquipmentStat1)
        isLockEquipmentStat1 = args.isLockEquipmentStat1;
    if (args.isLockEquipmentStat2)
        isLockEquipmentStat2 = args.isLockEquipmentStat2;
    if (args.isLockEquipmentStat3)
        isLockEquipmentStat3 = args.isLockEquipmentStat3;
    if (targetWingId != "") {
        let currentInventory = server.GetUserInventory({
            PlayFabId: currentPlayerId
        });
        let targetWingInstance = currentInventory.Inventory.filter(n => n.ItemId == targetWingId)[0];
        let tmpWingCustomData = GetWingCustomData(null, targetWingInstance);
        let tmpOwnedInfo = tmpWingCustomData.ownedStatInfo;
        let tmpEquipmentInfo = tmpWingCustomData.equipmentStatInfo;
        let getTitleDataResult = server.GetTitleData({
            Keys: [
                "balance_inventory_growth",
                "setting_json"
            ]
        }).Data;
        let tmpBalanceGrowthSheet = JSON.parse(getTitleDataResult["balance_inventory_growth"]);
        let settingInfo = JSON.parse(getTitleDataResult["setting_json"]);
        if (tmpBalanceGrowthSheet != null && tmpBalanceGrowthSheet.WingInfo != null && settingInfo != null && settingInfo.WingResetStatPriceList != null) {
            if (!isLockOwnedStat1) {
                var resetResult = GetResetOwnedStat(tmpBalanceGrowthSheet.WingInfo);
                tmpOwnedInfo.Stat1Rank = resetResult.rank;
                tmpOwnedInfo.Stat1SubRank = resetResult.subRank;
            }
            else
                lockStatCount = lockStatCount + 1;
            if (!isLockOwnedStat2) {
                var resetResult = GetResetOwnedStat(tmpBalanceGrowthSheet.WingInfo);
                tmpOwnedInfo.Stat2Rank = resetResult.rank;
                tmpOwnedInfo.Stat2SubRank = resetResult.subRank;
            }
            else
                lockStatCount = lockStatCount + 1;
            if (!isLockEquipmentStat1)
                tmpEquipmentInfo.Stat1Rank = GetResetEquipmentStat(tmpBalanceGrowthSheet.WingInfo);
            else
                lockStatCount = lockStatCount + 1;
            if (!isLockEquipmentStat2)
                tmpEquipmentInfo.Stat2Rank = GetResetEquipmentStat(tmpBalanceGrowthSheet.WingInfo);
            else
                lockStatCount = lockStatCount + 1;
            if (!isLockEquipmentStat3)
                tmpEquipmentInfo.Stat3Rank = GetResetEquipmentStat(tmpBalanceGrowthSheet.WingInfo);
            else
                lockStatCount = lockStatCount + 1;
            lockStatCount = Math.min(lockStatCount, 4);
            let requiredGem = parseInt(settingInfo.WingResetStatPriceList[lockStatCount]);
            if (requiredGem > currentInventory.VirtualCurrency["GE"].valueOf()) {
                log.info("Don't have enough gem!" + requiredGem);
                return toReturn;
            }
            SetWingCustomData(targetWingInstance.ItemInstanceId, tmpOwnedInfo, tmpEquipmentInfo);
            SubtractVirtualCurrency("GE", requiredGem);
            toReturn.error = false;
            toReturn.requiredGem = requiredGem;
            return toReturn;
        }
        else {
            log.info("Balance sheet is null!");
            return toReturn;
        }
    }
    else {
        log.info("Can't get valid parameters for wing!");
        return toReturn;
    }
};
handlers["resetWingStat"] = ResetWingStat;
let OpenWingBody = function (args, context) {
    let toReturn = { error: true, updatedLevel: 0, requiredFeather: 0 };
    let purchasedWingCount = 0;
    let currentInventory = server.GetUserInventory({
        PlayFabId: currentPlayerId
    });
    for (let i = 0; i < currentInventory.Inventory.length; i++) {
        if (currentInventory.Inventory[i].ItemId == "wing_body") {
            log.info("User already open wing body!");
            return toReturn;
        }
        else if (currentInventory.Inventory[i].ItemId.includes("wing_")) {
            purchasedWingCount = purchasedWingCount + 1;
        }
    }
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "setting_json"
        ]
    }).Data;
    let settingInfo = JSON.parse(getTitleDataResult["setting_json"]);
    if (settingInfo != null && settingInfo.WingUpgradePrice != null && settingInfo.WingBodyOpenCount != null) {
        if (purchasedWingCount < settingInfo.WingBodyOpenCount) {
            log.info("Purchased wing's count is not enough!");
            return toReturn;
        }
        let requiredFeather = parseInt(settingInfo.WingUpgradePrice);
        if (requiredFeather > currentInventory.VirtualCurrency["FE"].valueOf()) {
            log.info("Don't have enough Feather! required: " + requiredFeather);
            return toReturn;
        }
        let resultGrantItemsToUser = server.GrantItemsToUser({
            PlayFabId: currentPlayerId,
            ItemIds: ["wing_body"]
        });
        SetWingBodyCustomLevelData(resultGrantItemsToUser.ItemGrantResults[0].ItemInstanceId, 1);
        toReturn.error = false;
        toReturn.requiredFeather = requiredFeather;
        toReturn.updatedLevel = 1;
        return toReturn;
    }
    else {
        log.info("Setting info is null!");
        return toReturn;
    }
};
handlers["openWingBody"] = OpenWingBody;
let UpgradeWingBody = function (args, context) {
    let toReturn = { error: true, previousLevel: 0, updatedLevel: 0, isUpgradeSuccess: false, requiredFeather: 0 };
    let currentInventory = server.GetUserInventory({
        PlayFabId: currentPlayerId
    });
    let wingBodyId = "wing_body";
    let targetWingInstance = currentInventory.Inventory.filter(n => n.ItemId == wingBodyId)[0];
    if (targetWingInstance != null) {
        let wingBodyLevel = GetWingBodyLevel(targetWingInstance);
        let getTitleDataResult = server.GetTitleData({
            Keys: [
                "setting_json"
            ]
        }).Data;
        let settingInfo = JSON.parse(getTitleDataResult["setting_json"]);
        if (settingInfo != null && settingInfo.WingUpgradePrice != null && settingInfo.WingMaxLevel != null) {
            toReturn.previousLevel = wingBodyLevel;
            if (wingBodyLevel >= settingInfo.WingMaxLevel) {
                log.info("Wing body's level is max");
                return toReturn;
            }
            let requiredFeather = parseInt(settingInfo.WingUpgradePrice);
            if (requiredFeather > currentInventory.VirtualCurrency["FE"].valueOf()) {
                log.info("Don't have enough Feather!" + requiredFeather);
                return toReturn;
            }
            let randomNumber = Math.min(Math.floor(Math.random() * 100), 99);
            log.info("randomNumber: " + randomNumber);
            if (randomNumber < Math.max(100 - wingBodyLevel, 20)) {
                wingBodyLevel = wingBodyLevel + 1;
                SetWingBodyCustomLevelData(targetWingInstance.ItemInstanceId, wingBodyLevel);
                toReturn.isUpgradeSuccess = true;
            }
            SubtractVirtualCurrency("FE", requiredFeather);
            toReturn.error = false;
            toReturn.requiredFeather = requiredFeather;
            toReturn.updatedLevel = wingBodyLevel;
            return toReturn;
        }
        else {
            log.info("Setting info is null!");
            return toReturn;
        }
    }
    else {
        log.info("Can't find target wing in inventory!");
        return toReturn;
    }
};
handlers["upgradeWingBody"] = UpgradeWingBody;
function GetWingCustomData(defaultCustomData, wingInstance) {
    let tmpDefaultCustomData = null;
    let toReturn = { ownedStatInfo: null, equipmentStatInfo: null };
    if (wingInstance.CustomData == null) {
        if (defaultCustomData == null) {
            let tmpTitleData = server.GetTitleData({
                Keys: [
                    "default_custom_data"
                ]
            }).Data;
            tmpDefaultCustomData = JSON.parse(tmpTitleData["default_custom_data"]);
        }
        else
            tmpDefaultCustomData = defaultCustomData;
        toReturn.ownedStatInfo = tmpDefaultCustomData.OwnedStatInfo;
        toReturn.equipmentStatInfo = tmpDefaultCustomData.EquipmentStatInfo;
    }
    else {
        if (wingInstance.CustomData.OwnedStatInfo != null && wingInstance.CustomData.EquipmentStatInfo != null) {
            toReturn.ownedStatInfo = JSON.parse(wingInstance.CustomData.OwnedStatInfo);
            toReturn.equipmentStatInfo = JSON.parse(wingInstance.CustomData.EquipmentStatInfo);
        }
        else {
            if (defaultCustomData == null) {
                let tmpTitleData = server.GetTitleData({
                    Keys: [
                        "default_custom_data"
                    ]
                }).Data;
                tmpDefaultCustomData = JSON.parse(tmpTitleData["default_custom_data"]);
            }
            else
                tmpDefaultCustomData = defaultCustomData;
            toReturn.ownedStatInfo = tmpDefaultCustomData.OwnedStatInfo;
            toReturn.equipmentStatInfo = tmpDefaultCustomData.EquipmentStatInfo;
        }
    }
    return toReturn;
}
function SetWingCustomData(wingInstanceId, ownedStatInfo, equipmentStatInfo) {
    let customDataUpdateResult = server.UpdateUserInventoryItemCustomData({
        PlayFabId: currentPlayerId,
        ItemInstanceId: wingInstanceId,
        Data: {
            "OwnedStatInfo": unescape(JSON.stringify(ownedStatInfo)),
            "EquipmentStatInfo": unescape(JSON.stringify(equipmentStatInfo))
        }
    });
}
function GetWingBodyLevel(wingBodyInstance) {
    let level = 0;
    if (wingBodyInstance != null && wingBodyInstance.CustomData != null && wingBodyInstance.CustomData.Level != null)
        level = parseInt(wingBodyInstance.CustomData.Level);
    return level;
}
function SetWingBodyCustomLevelData(wingBodyInstanceId, level) {
    let customDataUpdateResult = server.UpdateUserInventoryItemCustomData({
        PlayFabId: currentPlayerId,
        ItemInstanceId: wingBodyInstanceId,
        Data: {
            "Level": level.toString()
        }
    });
}
function GetResetOwnedStat(wingInfo) {
    let toReturn = { rank: "E", subRank: "0" };
    let randomNumber = Math.min(Math.floor(Math.random() * 1000), 999);
    let summonInfoList = [];
    for (let i = 0; i < wingInfo.length; i++) {
        let summonPower = 0;
        for (let j = i; j < wingInfo.length; j++) {
            summonPower = summonPower + parseInt(wingInfo[j].SummonPower);
        }
        summonInfoList.push({ rank: wingInfo[i].Rank, summonPower: summonPower });
    }
    for (let i = summonInfoList.length - 1; i >= 0; i--) {
        if (randomNumber < summonInfoList[i].summonPower) {
            toReturn.rank = summonInfoList[i].rank;
            toReturn.subRank = Math.min(Math.floor(Math.random() * 4), 3).toString();
            return toReturn;
        }
    }
}
function GetResetEquipmentStat(wingInfo) {
    let toReturn = "E";
    let randomNumber = Math.min(Math.floor(Math.random() * 1000), 999);
    let summonInfoList = [];
    for (let i = 0; i < wingInfo.length; i++) {
        let summonPower = 0;
        for (let j = i; j < wingInfo.length; j++) {
            summonPower = summonPower + parseInt(wingInfo[j].SummonPower);
        }
        summonInfoList.push({ rank: wingInfo[i].Rank, summonPower: summonPower });
    }
    for (let i = summonInfoList.length - 1; i >= 0; i--) {
        if (randomNumber < summonInfoList[i].summonPower) {
            toReturn = summonInfoList[i].rank;
            return toReturn;
        }
    }
}
function GetEntityToken(params, context) {
    var getTokenRequest = {};
    var getTokenResponse = entity.GetEntityToken(getTokenRequest);
    var entityId = getTokenResponse.Entity.Id;
    var entityType = getTokenResponse.Entity.Type;
}
handlers.GetEntityToken = GetEntityToken;
function GetObjects(params, context) {
    var getObjRequest = {
        Entity: {
            Id: params.entityId,
            Type: params.entityType
        }
    };
    var getObjResponse = entity.GetObjects(getObjRequest);
    var entityId = getObjResponse.Entity.Id;
    var entityType = getObjResponse.Entity.Type;
    var entityObjs = getObjResponse.Objects["testKey"];
}
handlers.GetObjects = GetObjects;
