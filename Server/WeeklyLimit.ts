///////////////////////////////////////////////////////////////////////////////////////////////////////
// Dungeon Knight
///////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////// Cloud Script Handler Functions /////////////////////////

interface ICheckLeftWeeklyLimit{
    error: boolean;
    leftCount: number;
}

let LeftWeeklyLimit = function (args: any, context: IPlayFabContext): ICheckLeftWeeklyLimit{
    
    let name = "";
    let id = "";

    let toReturn: ICheckLeftWeeklyLimit = {error: true, leftCount: 0};

    if(args.name)
        name = args.name;
    else
        return toReturn;

    if(args.id)
        id = args.id;
    else
        return toReturn;

    toReturn.leftCount = GetLeftWeeklyLimit(name, id);
    toReturn.error = false;
    return toReturn;
}

handlers["leftWeeklyLimit"] = LeftWeeklyLimit;

interface IWeeklyAllowedCountAndUsedCounts{
    result: {};
    error: boolean;
}

let WeeklyAllowedCountAndUsedCounts = function (args: any, context: IPlayFabContext): IWeeklyAllowedCountAndUsedCounts{
    
    let name = "";

    let toReturn: IWeeklyAllowedCountAndUsedCounts = {error: true, result: {}};

    if(args.name)
        name = args.name;
    else
        return toReturn;

    // Get required values
    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "weekly_limit_use_" + name,
            "weekly_limit_last_timestamp"
        ]
    });

    // Need to reset weekly limit
    let hasReset = (ResetWeeklyLimitIfNeeded(getUserInternalDataResult));

    let allowedCount = 0;
    // Get allowed counts for a week
    if(name == "weekly_package_1")
        allowedCount = 1;
    else if(name == "weekly_package_3")
        allowedCount = 3;

    let dictionaryToUse = {};
    if(getUserInternalDataResult.Data["weekly_limit_use_" + name] != null)
    {
        dictionaryToUse = JSON.parse(getUserInternalDataResult.Data["weekly_limit_use_" + name].Value) as {};
    }

    let resultItems = [];
    for(let tmp in dictionaryToUse)
    {
        let count = dictionaryToUse[tmp] as number;

        if(hasReset) // Need to check this because getUserInternalDataResult could be old data if weekly limit has been reset
            count = 0;

        resultItems.push(
            {
                "ProgressId": tmp,
                "AllowedCount": allowedCount,
                "UsedCount": count
            }
        )
    }

    toReturn.result = {"WeeklyLimitAndUseCountList": resultItems};
    toReturn.error = false;
    return toReturn;
}

handlers["weeklyAllowedCountAndUsedCounts"] = WeeklyAllowedCountAndUsedCounts;

///////////////////////// HELPER FUNCTIONS (NOT DIRECTLY CALLABLE FROM THE CLIENT) /////////////////////////

function GetLeftWeeklyLimit(name: string, id: string, allowMinus: boolean = false): number{

    // Get required values
    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "weekly_limit_use_" + name,
            "weekly_limit_last_timestamp"
        ]
    });

    // Need to reset weekly limit
    let hasReset = ResetWeeklyLimitIfNeeded(getUserInternalDataResult);

    // Get allowed counts for a week
    let allowedCount = 0;
    if(name == "weekly_package_1")
        allowedCount = 1;
    else if(name == "weekly_package_3")
        allowedCount = 3;

    let usedCount = 0;
    let dictionaryToUse = {};
    if(getUserInternalDataResult.Data["weekly_limit_use_" + name] != null)
    {
        dictionaryToUse = JSON.parse(getUserInternalDataResult.Data["weekly_limit_use_" + name].Value) as {};
        if(dictionaryToUse[id] != null)
            usedCount = parseInt(dictionaryToUse[id]);

        if(hasReset) // Need to check this because getUserInternalDataResult could be old data if weekly limit has been reset
            usedCount = 0;
    }

    if(allowMinus)
        return allowedCount - usedCount;
    else
        return Math.max(allowedCount - usedCount, 0);
}

function UseWeeklyLimitIfAvail(name: string, id: string, count: number, allowMinus: boolean = false): boolean{
    
    // Get counts
    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "weekly_limit_use_" + name,
            "weekly_limit_last_timestamp"
        ]
    });

    let hasReset = ResetWeeklyLimitIfNeeded(getUserInternalDataResult);

    // Get allowed counts for a week
    let allowedCount = 0;
    if(name == "weekly_package_1")
        allowedCount = 1;
    else if(name == "weekly_package_3")
        allowedCount = 3;
    
    let usedCount = 0;
    let dictionaryToUse = {};
    if(getUserInternalDataResult.Data["weekly_limit_use_" + name] != null)
    {
        dictionaryToUse = JSON.parse(getUserInternalDataResult.Data["weekly_limit_use_" + name].Value) as {};
        if(dictionaryToUse[id] != null)
            usedCount = parseInt(dictionaryToUse[id]);
        
        if(hasReset) // Need to check this because getUserInternalDataResult could be old data if weekly limit has been reset
            usedCount = 0;
    }

    if(allowedCount >= usedCount + count || allowMinus)
    {
        // Increment count if not over allowed count
        if(dictionaryToUse[id] != null)
            dictionaryToUse[id] = dictionaryToUse[id] + count;
        else
            dictionaryToUse[id] = count;

        if(hasReset) // Need to check this because getUserInternalDataResult could be old data if weekly limit has been reset
            dictionaryToUse[id] = count;
        
        // Save the dictionary as string
        let dataToUse = {};
        dataToUse["weekly_limit_use_" + name] = JSON.stringify(dictionaryToUse);
        let updateUserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: dataToUse
        });
        return true;
    }
    else
    {
        return false;
    }
}

function ResetWeeklyLimitIfNeeded(internalDataResult: PlayFabServerModels.GetUserDataResult): boolean{

    // Get current time stamp
    let currentTimestampWeek = Math.floor((Math.floor(+ new Date() / (1000 * 60 * 60 * 24)) - 4) / 7);

    let lastTimestampWeek = 0;
    if(internalDataResult.Data["weekly_limit_last_timestamp"] != null) // Need to check last time stamp that was previously exist before
    {
        // Receive reward based on passed hours
        lastTimestampWeek = parseInt(internalDataResult.Data["weekly_limit_last_timestamp"].Value);
    }

    if(currentTimestampWeek > lastTimestampWeek)
    {
        log.info("Week has been changed. Need to reset weekly limit!");
        // Need to reset all the weekly use count to 0, and also save the current timestamp of this week
        let dataToUse = {};
        dataToUse["weekly_limit_use_weekly_package_1"] = "{}";
        dataToUse["weekly_limit_use_weekly_package_3"] = "{}";
        dataToUse["weekly_limit_last_timestamp"] = currentTimestampWeek.toString();

        let updateUserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: dataToUse
        });

        return true;
    }
    else
    {
        return false;
    }
}

function ForceResetWeeklyLimit(name: string){
    // Reset the weekly use count of given name to 0
    let dataToUse = {};
    dataToUse["weekly_limit_use_" + name] = "{}";
    let updateUserInternalDataResult = server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: dataToUse
    });
}