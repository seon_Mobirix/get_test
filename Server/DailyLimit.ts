///////////////////////////////////////////////////////////////////////////////////////////////////////
// Dungeon Knight
///////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////// Cloud Script Handler Functions /////////////////////////

interface ICheckLeftDailyLimit{
    error: boolean;
    leftCount: number;
}

let LeftDailyLimit = function (args: any, context: IPlayFabContext): ICheckLeftDailyLimit{
    
    let name = "";
    let id = "";

    let toReturn: ICheckLeftDailyLimit = {error: true, leftCount: 0};

    if(args.name)
        name = args.name;
    else
        return toReturn;

    if(args.id)
        id = args.id;
    else
        return toReturn;

    toReturn.leftCount = GetLeftDailyLimit(name, id);
    toReturn.error = false;
    return toReturn;
}

handlers["leftDailyLimit"] = LeftDailyLimit;

interface IDailyAllowedCountAndUsedCounts{
    result: {};
    error: boolean;
}

let DailyAllowedCountAndUsedCounts = function (args: any, context: IPlayFabContext): IDailyAllowedCountAndUsedCounts{
    
    let name = "";

    let toReturn: IDailyAllowedCountAndUsedCounts = {error: true, result: {}};

    if(args.name)
        name = args.name;
    else
        return toReturn;

    // Get required values
    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "daily_limit_use_" + name,
            "daily_limit_last_timestamp"
        ]
    });

    // Need to reset daily limit
    let hasReset = (ResetDailyLimitIfNeeded(getUserInternalDataResult));

    let allowedCount = 0;
    // Get allowed counts for a day
    if(name == "arena")
        allowedCount = 30;
    else if(name == "raid")
        allowedCount = 3;
    else if(name == "free_gem")
        allowedCount = 30;
    else if(name == "tower")
        allowedCount = 3;
    else if(name == "raid_rollback")
        allowedCount = 2;
    else if(name == "guild_free_donate")
        allowedCount = 1;
    else if(name == "guild_donate")
        allowedCount = 1;
    else if(name == "guild_raid")
        allowedCount = 3;
    else if(name == "daily_package_1")
        allowedCount = 1;
    else if(name == "daily_package_3")
        allowedCount = 3;

    let dictionaryToUse = {};
    if(getUserInternalDataResult.Data["daily_limit_use_" + name] != null)
    {
        dictionaryToUse = JSON.parse(getUserInternalDataResult.Data["daily_limit_use_" + name].Value) as {};
    }

    let resultItems = [];
    for(let tmp in dictionaryToUse)
    {
        let count = dictionaryToUse[tmp] as number;

        if(hasReset) // Need to check this because getUserInternalDataResult could be old data if daily limit has been reset
            count = 0;

        resultItems.push(
            {
                "ProgressId": tmp,
                "AllowedCount": allowedCount,
                "UsedCount": count
            }
        )
    }

    toReturn.result = {"DailyLimitAndUseCountList": resultItems};
    toReturn.error = false;
    return toReturn;
}

handlers["dailyAllowedCountAndUsedCounts"] = DailyAllowedCountAndUsedCounts;

///////////////////////// HELPER FUNCTIONS (NOT DIRECTLY CALLABLE FROM THE CLIENT) /////////////////////////

function GetLeftDailyLimit(name: string, id: string, allowMinus: boolean = false): number{

    // Get required values
    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "daily_limit_use_" + name,
            "daily_limit_last_timestamp"
        ]
    });

    // Need to reset daily limit
    let hasReset = ResetDailyLimitIfNeeded(getUserInternalDataResult);

    // Get allowed counts for a day
    let allowedCount = 0;
    if(name == "arena")
        allowedCount = 30;
    else if(name == "raid")
        allowedCount = 3;
    else if(name == "free_gem")
        allowedCount = 30;
    else if(name == "tower")
        allowedCount = 3;
    else if(name == "raid_rollback")
        allowedCount = 2;
    else if(name == "guild_free_donate")
        allowedCount = 1;
    else if(name == "guild_donate")
        allowedCount = 1;
    else if(name == "guild_raid")
        allowedCount = 3;
    else if(name == "daily_package_1")
        allowedCount = 1;
    else if(name == "daily_package_3")
        allowedCount = 3;

    let usedCount = 0;
    let dictionaryToUse = {};
    if(getUserInternalDataResult.Data["daily_limit_use_" + name] != null)
    {
        dictionaryToUse = JSON.parse(getUserInternalDataResult.Data["daily_limit_use_" + name].Value) as {};
        if(dictionaryToUse[id] != null)
            usedCount = parseInt(dictionaryToUse[id]);

        if(hasReset) // Need to check this because getUserInternalDataResult could be old data if daily limit has been reset
            usedCount = 0;
    }

    if(allowMinus)
        return allowedCount - usedCount;
    else
        return Math.max(allowedCount - usedCount, 0);
}

function UseDailyLimitIfAvail(name: string, id: string, count: number, allowMinus: boolean = false): boolean{
    
    // Get counts
    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "daily_limit_use_" + name,
            "daily_limit_last_timestamp"
        ]
    });

    let hasReset = ResetDailyLimitIfNeeded(getUserInternalDataResult);

    // Get allowed counts for a day
    let allowedCount = 0;
    if(name == "arena")
        allowedCount = 30;
    else if(name == "raid")
        allowedCount = 3;
    else if(name == "free_gem")
        allowedCount = 30;
    else if(name == "tower")
        allowedCount = 3;
    else if(name == "raid_rollback")
        allowedCount = 2;
    else if(name == "guild_free_donate")
        allowedCount = 1;
    else if(name == "guild_donate")
        allowedCount = 1;
    else if(name == "guild_raid")
        allowedCount = 3;
    else if(name == "daily_package_1")
        allowedCount = 1;
    else if(name == "daily_package_3")
        allowedCount = 3;
    
    let usedCount = 0;
    let dictionaryToUse = {};
    if(getUserInternalDataResult.Data["daily_limit_use_" + name] != null)
    {
        dictionaryToUse = JSON.parse(getUserInternalDataResult.Data["daily_limit_use_" + name].Value) as {};
        if(dictionaryToUse[id] != null)
            usedCount = parseInt(dictionaryToUse[id]);
        
        if(hasReset) // Need to check this because getUserInternalDataResult could be old data if daily limit has been reset
            usedCount = 0;
    }

    if(allowedCount >= usedCount + count || allowMinus)
    {
        // Increment count if not over allowed count
        if(dictionaryToUse[id] != null)
            dictionaryToUse[id] = dictionaryToUse[id] + count;
        else
            dictionaryToUse[id] = count;

        if(hasReset) // Need to check this because getUserInternalDataResult could be old data if daily limit has been reset
            dictionaryToUse[id] = count;
        
        // Save the dictionary as string
        let dataToUse = {};
        dataToUse["daily_limit_use_" + name] = JSON.stringify(dictionaryToUse);
        let updateUserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: dataToUse
        });
        return true;
    }
    else
    {
        return false;
    }
}

function ResetDailyLimitIfNeeded(internalDataResult: PlayFabServerModels.GetUserDataResult): boolean{

    // Get current time stamp
    let currentTimestampDay = Math.floor(+ new Date() / (1000 * 60 * 60 * 24));

    let lastTimestampDay = 0;
    if(internalDataResult.Data["daily_limit_last_timestamp"] != null) // Need to check last time stamp that was previously exist before
    {
        // Receive reward based on passed hours
        lastTimestampDay = parseInt(internalDataResult.Data["daily_limit_last_timestamp"].Value);
    }

    if(currentTimestampDay > lastTimestampDay)
    {
        log.info("Day has been changed. Need to reset daily limit!");
        // Need to reset all the daily use count to 0, and also save the current timestamp of today
        let dataToUse = {};
        dataToUse["daily_limit_use_arena"] = "{}";
        dataToUse["daily_limit_use_raid"] = "{}";
        dataToUse["daily_limit_use_free_gem"] = "{}";
        dataToUse["daily_limit_use_tower"] = "{}";
        dataToUse["daily_limit_use_raid_rollback"] = "{}";
        dataToUse["daily_limit_use_guild_free_donate"] = "{}";
        dataToUse["daily_limit_use_guild_donate"] = "{}";
        dataToUse["daily_limit_use_guild_raid"] = "{}";

        let updateUserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: dataToUse
        });

        dataToUse = {};
        dataToUse["daily_limit_use_daily_package_1"] = "{}";
        dataToUse["daily_limit_use_daily_package_3"] = "{}";
        dataToUse["daily_limit_last_timestamp"] = currentTimestampDay.toString();

        updateUserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: dataToUse
        });

        return true;
    }
    else
    {
        return false;
    }
}

function ForceResetDailyLimit(name: string){
    // Reset the daily use count of given name to 0
    let dataToUse = {};
    dataToUse["daily_limit_use_" + name] = "{}";
    let updateUserInternalDataResult = server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: dataToUse
    });
}