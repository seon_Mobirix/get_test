///////////////////////////////////////////////////////////////////////////////////////////////////////
// Dungeon Knight
///////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////// Cloud Script Handler Functions /////////////////////////
interface IAddUsedCouponTag{
    error: boolean;
}

let AddUsedCouponTag = function(args: any, context: IPlayFabContext): IAddUsedCouponTag
{
    let toReturn: IAddUsedCouponTag = {error: true};
    let couponId = "";

    if(args && args.couponId)
        couponId = args.couponId;

    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "received_only_once_rewards"
        ]
    }).Data["received_only_once_rewards"];
    
    let listOfReceivedItems = [];
    
    if(getUserInternalDataResult != null)
    {
        listOfReceivedItems = JSON.parse(getUserInternalDataResult.Value);
        if(listOfReceivedItems.some(n => n == couponId))
        {
            // Add Tag if user used target coupon
            let addPlayerTagResult = server.AddPlayerTag({
                PlayFabId: currentPlayerId,
                TagName: couponId
            });

            toReturn.error = false;
        }
    }
    
    return toReturn;    
}
handlers["addUsedCouponTag"] = AddUsedCouponTag;

interface IRemoveUsedCouponTag
{
    error: boolean;
}

let RemoveUsedCouponTag = function (args: any, context: IPlayFabContext): IRemoveUsedCouponTag 
{
    let toReturn : IRemoveUsedCouponTag = {error: true};
    let couponId = "";

    if(args && args.couponId)
        couponId = args.couponId;

    let removePlayerTagResult = server.RemovePlayerTag({
        PlayFabId: currentPlayerId,
        TagName: couponId
    });

    toReturn.error = false;
    return toReturn;
}

handlers["removeUsedCouponTag"] = RemoveUsedCouponTag;