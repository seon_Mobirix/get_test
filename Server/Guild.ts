///////////////////////////////////////////////////////////////////////////////////////////////////////
// Dungeon Knight
///////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////// Cloud Script Handler Functions /////////////////////////

///////////////////////// For Guild Master /////////////////////////
interface IGuildCreate{
    guildManagementData: {};
    guildMemberData: {};
    requiredGem: number;
    reasonOfError: string;
    error: boolean;
}

let GuildCreate = function(args: any, context: IPlayFabContext): IGuildCreate{

    let toReturn: IGuildCreate = {guildManagementData: {}, guildMemberData: {}, requiredGem: 0, reasonOfError: "", error: true};  
    
    // Data from server for guild info
    let serverId = "";

    // Data from client for guild info
    let guildName = "";
    let guildFlagIndex = 0;
    let guildNotice = "";
    let isPrivate = false;
    let requiredStageProgress = 0;

    // Data from client for guild master info
    let guildMasterName = "";
    let guildMasterStageProgress = 1;

    if(args.guildName)
        guildName = args.guildName;
    if(args.guildFlagIndex)
        guildFlagIndex = args.guildFlagIndex;
    if(args.guildNotice)
        guildNotice = args.guildNotice;
    if(args.isPrivate)
        isPrivate = args.isPrivate;
    if(args.requiredStageProgress)
        requiredStageProgress = args.requiredStageProgress;
    if(args.guildMasterName)
        guildMasterName = args.guildMasterName;
    if(args.guildMasterStageProgress)
        guildMasterStageProgress = args.guildMasterStageProgress;

    // Check guild name limit
    if(guildName.length > 10)
    {
        log.info("Guild name is too long!");
        return toReturn;
    }

    // Check notice limit
    if(guildNotice.length > 100)
    {
        log.info("Notice size is too long!");
        return toReturn;
    }

    // Check user already have a guild or not
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_id"]
    });

    if(getUserReadOnlyDataResult.Data["guild_id"] != null)
    {
        log.info("User already have joined a guild!");
        return toReturn;
    }

    // Get default guild data foramt and guild create price info
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "default_guild_data",
            "setting_json"
        ]
    }).Data;

    let defaultGuildData = JSON.parse(getTitleDataResult["default_guild_data"]);
    let settingInfo = JSON.parse(getTitleDataResult["setting_json"]);

    if(defaultGuildData != null && settingInfo != null && settingInfo.GuildCreatePrice != null)
    {
        let getUserInventoryResult = server.GetUserInventory({
            PlayFabId: currentPlayerId
        });

        let guildCreatePrice = parseInt(settingInfo.GuildCreatePrice);
        let currentGemCount = 0;

        if(getUserInventoryResult.VirtualCurrency["GE"] != null)
            currentGemCount = getUserInventoryResult.VirtualCurrency["GE"];
        
        // Check gem amount
        if(currentGemCount < guildCreatePrice)
        {
            log.info("Not enough gems!");
            return toReturn;
        }

        // Get guild master's server
        let getPlayerTagsResult = server.GetPlayerTags({
            PlayFabId: currentPlayerId
        });

        if(getPlayerTagsResult != null && getPlayerTagsResult.Tags.length > 0)
            serverId = GetServerIdFromTag(getPlayerTagsResult.Tags.filter(n => n.indexOf("Server") >= 0)[0]);
        else
            serverId = "";

        // Create group for preventing duplicate guild name
        let getUserAccountInfoResult = server.GetUserAccountInfo({ 
            PlayFabId: currentPlayerId 
        });

        if(getUserAccountInfoResult.UserInfo.TitleInfo.TitlePlayerAccount.Id != null && getUserAccountInfoResult.UserInfo.TitleInfo.TitlePlayerAccount.Type != null)
        {
            try {
                let createGroupResult = entity.CreateGroup({
                    Entity: {
                        Id: getUserAccountInfoResult.UserInfo.TitleInfo.TitlePlayerAccount.Id,
                        Type: getUserAccountInfoResult.UserInfo.TitleInfo.TitlePlayerAccount.Type
                    },
                    GroupName: guildName
                });

                defaultGuildData.GroupId = createGroupResult.Group.Id;
            } catch (error) {
                let errorData = JSON.parse(JSON.stringify(error));
                
                if(errorData != null && errorData.apiErrorInfo.apiError.errorCode == 1368)
                {
                    toReturn.reasonOfError = "guild_name_duplicated";
                    return toReturn;
                }
                else
                    return toReturn;
            }            
        }
        else
            return toReturn;

        // Set guild management data
        defaultGuildData.ServerId = serverId;
        defaultGuildData.GuildId = currentPlayerId;
        defaultGuildData.GuildName = guildName;
        defaultGuildData.GuildFlagIndex = guildFlagIndex;
        defaultGuildData.GuildNotice = guildNotice;
        defaultGuildData.IsPrivate = isPrivate;
        defaultGuildData.RequiredStageProgress = requiredStageProgress;

        // Set guild create time for recomandtion guild function
        defaultGuildData.TimestampDay = Math.floor(+ new Date() / (1000 * 60 * 60 * 24));

        let guildMemberData: any = JSON.parse("{\"MemberList\":[]}");
        let guildCacheData: any = JSON.parse("{\"MemberCacheDataList\":[]}");
        let guildApplicantData: any = JSON.parse("{\"ApplicantList\":[]}");

        // Add guild master in memberlist
        guildMemberData.MemberList.push({"Id": currentPlayerId, "EntityId": getUserAccountInfoResult.UserInfo.TitleInfo.TitlePlayerAccount.Id, 
        "EntityType": getUserAccountInfoResult.UserInfo.TitleInfo.TitlePlayerAccount.Type, "JoinTimeStamp": Math.floor(+new Date() / 1000)});
        
        // Add guild master in guild cache list
        guildCacheData.MemberCacheDataList.push(
            {
                "Id": currentPlayerId, 
                "Name": guildMasterName, 
                "StageProgress": guildMasterStageProgress, 
                "LastTimeStamp": Math.floor(+ new Date() / (1000 * 60 * 60)), 
                "TotalContribution": 0,
                "RaidScore": 0,
                "RaidSeason": -1
            }
        );

        // Update guild info
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "guild_id": currentPlayerId,
                "guild_management_data": unescape(JSON.stringify(defaultGuildData)),
                "guild_total_exp": "0",
                "guild_master_exp": "0",
                "guild_member_list": unescape(JSON.stringify(guildMemberData)),
                "guild_cache_list": unescape(JSON.stringify(guildCacheData)),
                "guild_applicant_list": unescape(JSON.stringify(guildApplicantData)),    
                "apply_guild_list": null
            }
        });

        // Update guild recommendation score
        UpdateGuildRecommendationScore(GetServerIdModifierFromServerId(defaultGuildData.ServerId), defaultGuildData.GuildId, parseInt(defaultGuildData.TimestampDay), 1);

        // Spend gem for create guild
        SubtractVirtualCurrency("GE", guildCreatePrice);

        toReturn.guildManagementData = defaultGuildData;
        toReturn.guildMemberData = guildCacheData;
        toReturn.requiredGem = guildCreatePrice;
        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;
}
handlers["guildCreate"] = GuildCreate;

interface GuildDelete{
    reasonOfError: string;
    error: boolean;
}

let GuildDelete = function(args: any, context: IPlayFabContext): GuildDelete{

    let toReturn: GuildDelete = {reasonOfError: "", error: true};  

    // Get guild data
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_management_data", "guild_member_list"]
    });

    // Check user is guild master or not    
    if(!IsGuildMaster(getUserReadOnlyDataResult.Data["guild_management_data"])) 
    {
        log.info("This user is not guild master!");
        return toReturn;
    }

    if(getUserReadOnlyDataResult.Data["guild_management_data"] != null && getUserReadOnlyDataResult.Data["guild_member_list"] != null)
    {
        let guildManagementData = JSON.parse(getUserReadOnlyDataResult.Data["guild_management_data"].Value);
        let guildMemberData = JSON.parse(getUserReadOnlyDataResult.Data["guild_member_list"].Value);
        guildMemberData.MemberList = guildMemberData.MemberList.filter(n => n.Id != currentPlayerId);

        // Check this user is last memeber of guild
        if(guildMemberData.MemberList.length != 0)
        {
            log.info("Guild member is left!");
            toReturn.reasonOfError = "guild_member_left";
            return toReturn;
        }

        // Delete group
        try {
            let deleteGroupResult = entity.DeleteGroup({
                Group: {Id: guildManagementData.GroupId, Type: null}
            });
        } catch (error) {
            log.info("Error in entity.DeleteGroup: " + JSON.parse(JSON.stringify(error)));
            return toReturn;
        }

        // Get guild delete time stamp
        let lastGuildLeaveTimeStamp = Math.floor(+new Date() / 1000);

        // Delete guild data and update guild leave time stamp
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "guild_id": null,
                "guild_management_data": null,
                "guild_total_exp": null,
                "guild_master_exp": null,
                "guild_member_list": null,
                "guild_cache_list": null,
                "guild_applicant_list": null,
                "guild_raid_reward_last_played_season": null,
                "last_guild_leave_timestamp": lastGuildLeaveTimeStamp.toString()
            }
        });

        let serverIdModifier = GetServerIdModifierFromServerId(guildManagementData.ServerId);

        // Get old total score
        let currentStatValue = server.GetPlayerStatistics({
            PlayFabId: currentPlayerId,
            StatisticNames: ["guild_raid" + serverIdModifier]
        }).Statistics;

        let oldMasterScore = 0;
        if(currentStatValue[0] != null)
            oldMasterScore = currentStatValue[0].Value;

        // Remove guild related leaderboard
        server.UpdatePlayerStatistics({
            PlayFabId: currentPlayerId,
            Statistics: [
                {
                    "StatisticName": "guild_raid" + serverIdModifier,
                    "Value": oldMasterScore * -1
                },
                {
                    "StatisticName": "guild_recommend" + serverIdModifier,
                    "Value": 0
                }
            ]
        });

        toReturn.error = false;
        return toReturn;
    }
    else
    {
        log.info("User is not guild master or don't have any guild!");
        return toReturn;
    }
}
handlers["guildDelete"] = GuildDelete;

interface IGuildGetApplicantList{
    applicantList: {};
    error: boolean;
}

let GuildGetApplicantList = function(args: any, context: IPlayFabContext): IGuildGetApplicantList{
    let toReturn: IGuildGetApplicantList = {applicantList: {}, error: true};  
    let applicantListToReturn = [];

    // Get guild applicant data
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_management_data", "guild_applicant_list"]
    });

    // Check user is guild master or not    
    if(!IsGuildMaster(getUserReadOnlyDataResult.Data["guild_management_data"])) 
    {
        log.info("This user is not guild master!");
        return toReturn;
    }

    if(getUserReadOnlyDataResult.Data["guild_applicant_list"] != null)
    {
        let guildApplicantData = JSON.parse(getUserReadOnlyDataResult.Data["guild_applicant_list"].Value);

        for(let i = 0; i < guildApplicantData.ApplicantList.length; i++)
        {
            let applicantId = guildApplicantData.ApplicantList[i].Id;
            let userName = "";
            let userLastStageProgress = 1;

            // Get applied user name
            let getUserAccountInfoResult = server.GetUserAccountInfo({
                PlayFabId: applicantId
            });

            userName = getUserAccountInfoResult.UserInfo.TitleInfo.DisplayName

            // Get applied user last stage progress
            let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
                PlayFabId: applicantId,
                Keys: ["guild_id", "user_data"]
            });

            userLastStageProgress = GetStageProgress(getUserReadOnlyDataResult.Data["user_data"]);

            // Check applicant already have a guild
            if(getUserReadOnlyDataResult.Data["guild_id"] == null)
                applicantListToReturn.push({"Id": applicantId, "Name": userName, "StageProgress": userLastStageProgress});
        }

        guildApplicantData.ApplicantList = applicantListToReturn;

        // Synchronization application data
        let updateGuildMasterReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "guild_applicant_list": unescape(JSON.stringify(guildApplicantData))
            }
        });

        toReturn.applicantList = {"ApplicantList": applicantListToReturn};
        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;
}
handlers["guildGetApplicantList"] = GuildGetApplicantList;

interface IGuildApplicantAccept{
    targetId: string;
    applicantList: {};
    memberCacheDataList: {};
    reasonOfError: string;
    error: boolean;
}

let GuildApplicantAccept = function(args: any, context: IPlayFabContext): IGuildApplicantAccept{
    let toReturn: IGuildApplicantAccept = {targetId: "", applicantList: {}, memberCacheDataList: {}, reasonOfError: "", error: true}; 
    let applicantId = "";
    let applicantName = "";
    let applicantStageProgress = 1;
    
    if(args.applicantId)
        applicantId = args.applicantId;

    // For log
    toReturn.targetId = applicantId;

    // Get guild data
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_management_data", "guild_member_list", "guild_cache_list", "guild_applicant_list", "guild_total_exp", "guild_master_exp"]
    });

    // Check user is guild master or not
    if(!IsGuildMaster(getUserReadOnlyDataResult.Data["guild_management_data"])) 
    {
        log.info("This user is not guild master!");
        return toReturn;
    }

    if(getUserReadOnlyDataResult.Data["guild_management_data"] != null && getUserReadOnlyDataResult.Data["guild_member_list"] != null && getUserReadOnlyDataResult.Data["guild_cache_list"] != null && getUserReadOnlyDataResult.Data["guild_applicant_list"] != null)
    {
        let guildManagementData = JSON.parse(getUserReadOnlyDataResult.Data["guild_management_data"].Value);
        let guildMemberData = JSON.parse(getUserReadOnlyDataResult.Data["guild_member_list"].Value);
        let guildCacheData = JSON.parse(getUserReadOnlyDataResult.Data["guild_cache_list"].Value);
        let guildApplicantData = JSON.parse(getUserReadOnlyDataResult.Data["guild_applicant_list"].Value);
        
        // Check application id in application list
        if(guildApplicantData.ApplicantList.find(n => n.Id == applicantId) == null)
        {
            log.info("Can't find in application list!");
            return toReturn;
        }

        // Get guild level and exp info
        let guildLevelInfo = GetGuildLevelAndExp(getUserReadOnlyDataResult.Data["guild_total_exp"], getUserReadOnlyDataResult.Data["guild_master_exp"], null);

        if(guildLevelInfo.error)
        {
            log.info("guild level info data is wrong!")
            return toReturn;
        }

        // Check limit member count
        if(guildMemberData.MemberList.length >= GetGuildMemberLimitCount(guildLevelInfo.level))
        {
            log.info("Guild member count is full!")
            toReturn.reasonOfError = "guild_member_full";
            return toReturn;
        }

        // Check applicant already have a guild or not
        let getApplicationReadOnlyDataResult = server.GetUserReadOnlyData({
            PlayFabId: applicantId,
            Keys: ["guild_id", "guild_management_data", "user_data"]
        });

        if(getApplicationReadOnlyDataResult.Data["guild_id"] != null || getApplicationReadOnlyDataResult.Data["guild_management_data"] != null)
        {
            log.info("Application already have joined a guild!");

            guildApplicantData.ApplicantList = guildApplicantData.ApplicantList.filter(n => n.Id != applicantId);

            // Update guild data info in guild master
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "guild_applicant_list": unescape(JSON.stringify(guildApplicantData))
                }
            });

            toReturn.reasonOfError = "already_have_guild";
            return toReturn;
        }

        // Get applicant account info
        let getUserAccountInfoResult = server.GetUserAccountInfo({
            PlayFabId: applicantId
        });

        applicantName = getUserAccountInfoResult.UserInfo.TitleInfo.DisplayName

        // Get applicant stage progress
        applicantStageProgress = GetStageProgress(getApplicationReadOnlyDataResult.Data["user_data"]);

        // Add member and remove from applicant list and kick list
        let currentTimestamp = Math.floor(+ new Date() / (1000 * 60 * 60 * 24));

        guildMemberData.MemberList.push({"Id": applicantId, "EntityId": getUserAccountInfoResult.UserInfo.TitleInfo.TitlePlayerAccount.Id, 
        "EntityType": getUserAccountInfoResult.UserInfo.TitleInfo.TitlePlayerAccount.Type, "JoinTimeStamp": Math.floor(+new Date() / 1000)});

        guildCacheData.MemberCacheDataList.push(
            {
                "Id": applicantId, 
                "Name": applicantName, 
                "StageProgress": applicantStageProgress,
                "LastTimeStamp": -1, 
                "TotalContribution": 0,
                "RaidScore": 0,
                "RaidSeason": -1
            }
        );
        guildApplicantData.ApplicantList = guildApplicantData.ApplicantList.filter(n => n.Id != applicantId);

        // Check for the case where guildCacheData is not matching with guild member list
        for(let i = 0; i < guildMemberData.MemberList.length; i ++)
        {
            if(guildCacheData.MemberCacheDataList.find(n => n.Id == guildMemberData.MemberList[i].Id) == null)
            {
                guildCacheData.MemberCacheDataList.push(
                    {
                        "Id": guildMemberData.MemberList[i].Id, 
                        "Name": "???", 
                        "Level": -1, 
                        "LastTimeStamp": -1,
                        "TotalContribution": 0,
                        "RaidScore": 0,
                        "RaidSeason": -1
                    }
                );
            }
        }

        // Only leave member data cache where member list exists
        let newCacheDataList = [];
        for(let i = 0; i < guildMemberData.MemberList.length; i ++)
        {
            newCacheDataList.push(guildCacheData.MemberCacheDataList.find(n => n.Id == guildMemberData.MemberList[i].Id));
        }
        guildCacheData.MemberCacheDataList = newCacheDataList;

        // Update guild data in guild master
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "guild_management_data": unescape(JSON.stringify(guildManagementData)),
                "guild_member_list": unescape(JSON.stringify(guildMemberData)),
                "guild_cache_list": unescape(JSON.stringify(guildCacheData)),
                "guild_applicant_list": unescape(JSON.stringify(guildApplicantData)),
            }
        });

        // Update guild recommendation score
        UpdateGuildRecommendationScore(GetServerIdModifierFromServerId(guildManagementData.ServerId), guildManagementData.GuildId, parseInt(guildManagementData.TimestampDay), guildMemberData.MemberList.length);

        // Update new member's read only data remove apply guild list
        let updateMemeberReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: applicantId,
            Data: {
                "guild_id": guildManagementData.GuildId,
                "apply_guild_list": null
            }
        });
        
        toReturn.memberCacheDataList = {"MemberCacheDataList": guildCacheData.MemberCacheDataList};
        toReturn.applicantList = {"ApplicantList": guildApplicantData.ApplicantList};
        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;    
}
handlers["guildApplicantAccept"] = GuildApplicantAccept;

interface IGuildApplicantRefuse{
    targetId: string;
    applicantList: {};
    error: boolean;
}

let GuildApplicantRefuse = function(args: any, context: IPlayFabContext): IGuildApplicantRefuse{
    let toReturn: IGuildApplicantRefuse = {targetId: "", applicantList: {}, error: true}; 
    let applicantId = "";
    
    if(args.applicantId)
        applicantId = args.applicantId;

    // For log
    toReturn.targetId = applicantId;

    // Get guild data
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_management_data", "guild_applicant_list"]
    });

    if(getUserReadOnlyDataResult.Data["guild_management_data"] != null && getUserReadOnlyDataResult.Data["guild_applicant_list"] != null)
    {
        // Check user is guild master or not
        if(!IsGuildMaster(getUserReadOnlyDataResult.Data["guild_management_data"]))
        {
            log.info("This user is not guild master!");
            return toReturn;
        }
        
        let guildManagementData = JSON.parse(getUserReadOnlyDataResult.Data["guild_management_data"].Value);
        let guildApplicantData = JSON.parse(getUserReadOnlyDataResult.Data["guild_applicant_list"].Value);
        
        // Check application id in application list
        if(guildApplicantData.ApplicantList.find(n => n.Id == applicantId) == null)
        {
            log.info("Can't find in application list!");
            return toReturn;
        }

        // Remove from applicion list
        guildApplicantData.ApplicantList = guildApplicantData.ApplicantList.filter(n => n.Id != applicantId);

        // Update guild data info in guild master
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "guild_applicant_list": unescape(JSON.stringify(guildApplicantData))
            }
        });

        // Get applicant apply guild list
        let getApplicantReadOnlyDataResult = server.GetUserReadOnlyData({
            PlayFabId: applicantId,
            Keys: ["apply_guild_list"]
        });

        if(getApplicantReadOnlyDataResult.Data["apply_guild_list"] != null)
        {
            let applyGuildData = JSON.parse(getApplicantReadOnlyDataResult.Data["apply_guild_list"].Value);

            applyGuildData.ApplyGuildList = applyGuildData.ApplyGuildList.filter(n => n.toUpperCase() != guildManagementData.GuildId);
            
            // Update applicant apply guild list
            let updateApplicantReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: applicantId,
                Data: {
                    "apply_guild_list": unescape(JSON.stringify(applyGuildData))
                }
            });
        }
        
        toReturn.applicantList = {"ApplicantList": guildApplicantData.ApplicantList};
        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;    
}
handlers["guildApplicantRefuse"] = GuildApplicantRefuse;

interface IGuildGetKickList{
    kickList: {};
    error: boolean;
}

let GuildGetKickList = function(args: any, context: IPlayFabContext): IGuildGetKickList{
    let toReturn: IGuildGetKickList = {kickList: {}, error: true};
    let kickListToReturn = [];

    // Get guild management data
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_management_data"]
    });

    // Check user is guild master or not    
    if(!IsGuildMaster(getUserReadOnlyDataResult.Data["guild_management_data"])) 
    {
        log.info("This user is not guild master!");
        return toReturn;
    }

    if(getUserReadOnlyDataResult.Data["guild_management_data"] != null)
    {
        let guildManagementData = JSON.parse(getUserReadOnlyDataResult.Data["guild_management_data"].Value);
        let getUserAccountInfoAPICallCount = 0;

        for(let i = 0; i < guildManagementData.KickList.length; i++)
        {
            let kickedUserId = guildManagementData.KickList[i].Id;
            let userName = ""

            if(guildManagementData.KickList[i].UserName != null)
                userName = guildManagementData.KickList[i].UserName;
            
            if(userName == "")
            {
                if(getUserAccountInfoAPICallCount < 20)
                {
                    // Get kick user name
                    let getUserAccountInfoResult = server.GetUserAccountInfo({
                        PlayFabId: kickedUserId
                    });

                    userName = getUserAccountInfoResult.UserInfo.TitleInfo.DisplayName;
                    guildManagementData.KickList[i].UserName = getUserAccountInfoResult.UserInfo.TitleInfo.DisplayName;
                    getUserAccountInfoAPICallCount ++;
                }
                else
                    userName = "Unknown";
            }

            kickListToReturn.push({"Id": kickedUserId, "Name": userName});
        }

        if(getUserAccountInfoAPICallCount > 0)
        {
            // Update guild data info in guild master
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "guild_management_data": unescape(JSON.stringify(guildManagementData))
                }
            });
        }

        toReturn.kickList = {"KickMemberList": kickListToReturn};
        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;
}
handlers["guildGetKickList"] = GuildGetKickList;

interface IGuildKickMember{
    targetId: string;
    memberCacheDataList: {};
    error: boolean;
}

let GuildKickMember = function(args: any, context: IPlayFabContext): IGuildKickMember{
    let toReturn: IGuildKickMember = {targetId: "", memberCacheDataList: {}, error: true}; 
    let memberId = "";
    let memberName = "";
    
    if(args.memberId)
        memberId = args.memberId;
    if(args.memberName)
        memberName = args.memberName;

    // For log
    toReturn.targetId = memberId;

    // Get guild data
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_management_data", "guild_member_list", "guild_cache_list"]
    });

    if(getUserReadOnlyDataResult.Data["guild_management_data"] != null && getUserReadOnlyDataResult.Data["guild_member_list"] != null && getUserReadOnlyDataResult.Data["guild_cache_list"] != null)
    {
        let guildManagementData = JSON.parse(getUserReadOnlyDataResult.Data["guild_management_data"].Value);
        let guildMemberData = JSON.parse(getUserReadOnlyDataResult.Data["guild_member_list"].Value);
        let guildCacheData = JSON.parse(getUserReadOnlyDataResult.Data["guild_cache_list"].Value);
        
        // Check user is guild master or not
        if(!IsGuildMaster(getUserReadOnlyDataResult.Data["guild_management_data"])) 
        {
            log.info("This user is not guild master!");
            return toReturn;
        }

        // Check limit kick member count (limit 25)
        if(guildManagementData.KickList.length > 24)
        {
            log.info("Kick member count can't over 25!");
            return toReturn;
        }
        
        // Check member id in member list
        let targetMemberInfo = guildMemberData.MemberList.find(n => n.Id == memberId);
        if(targetMemberInfo == null)
        {
            log.info("Can't find in member list!");
            return toReturn;
        }
        
        // Get member name
        if(memberName == "")
        {
            let targetMemberCacheInfo = guildCacheData.MemberCacheDataList.find(n => n.Id == memberId);
            if(targetMemberCacheInfo != null)
                memberName = targetMemberCacheInfo.Name; 
        }

        // Add to kick list
        guildManagementData.KickList.push({"Id": memberId, "UserName": memberName});

        // Remove from member list
        guildMemberData.MemberList = guildMemberData.MemberList.filter(n => n.Id != memberId);

        // Check for the case where guildCacheData is not matching with guild member list
        for(let i = 0; i < guildMemberData.MemberList.length; i ++)
        {
            if(guildCacheData.MemberCacheDataList.find(n => n.Id == guildMemberData.MemberList[i].Id) == null)
            {
                guildCacheData.MemberCacheDataList.push(
                    {
                        "Id": guildMemberData.MemberList[i].Id, 
                        "Name": "???", 
                        "Level": -1, 
                        "LastTimeStamp": -1,
                        "TotalContribution": 0,
                        "RaidScore": 0,
                        "RaidSeason": -1
                    }
                );
            }
        }

        // Only leave member data cache where member list exists
        let newCacheDataList = [];
        for(let i = 0; i < guildMemberData.MemberList.length; i ++)
        {
            newCacheDataList.push(guildCacheData.MemberCacheDataList.find(n => n.Id == guildMemberData.MemberList[i].Id));
        }
        guildCacheData.MemberCacheDataList = newCacheDataList;

        // Update guild data info in guild master
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "guild_management_data": unescape(JSON.stringify(guildManagementData)),
                "guild_member_list": unescape(JSON.stringify(guildMemberData)),
                "guild_cache_list": unescape(JSON.stringify(guildCacheData)),
            }
        });
        
        // Update guild recommendation score
        UpdateGuildRecommendationScore(GetServerIdModifierFromServerId(guildManagementData.ServerId), guildManagementData.GuildId, parseInt(guildManagementData.TimestampDay), guildMemberData.MemberList.length);

        // Get kick time stamp
        let lastGuildLeaveTimeStamp = Math.floor(+new Date() / 1000);

        // Remove guild id in member's read only data
        let updateMemberReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: memberId,
            Data: {
                "guild_id": null,
                "guild_raid_reward_last_played_season": null,
                "last_guild_leave_timestamp": lastGuildLeaveTimeStamp.toString()
            }
        });
        
        toReturn.memberCacheDataList = {"MemberCacheDataList": guildCacheData.MemberCacheDataList};
        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;
}
handlers["guildKickMember"] = GuildKickMember;

interface IGuildKickCancel{
    targetId: string;
    error: boolean;
}

let GuildKickCancel = function(args: any, context: IPlayFabContext): IGuildKickCancel{
    let toReturn: IGuildKickCancel = {targetId: "", error: true}; 
    let memberId = "";
    
    if(args.memberId)
        memberId = args.memberId;

    toReturn.targetId = memberId;

    // Get guild data
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_management_data"]
    });

    if(getUserReadOnlyDataResult.Data["guild_management_data"] != null)
    {        
        // Check user is guild master or not
        if(!IsGuildMaster(getUserReadOnlyDataResult.Data["guild_management_data"])) 
        {
            log.info("This user is not guild master!");
            return toReturn;
        }
        
        let guildManagementData = JSON.parse(getUserReadOnlyDataResult.Data["guild_management_data"].Value);

        // Check member id in member list
        let targetMemberId = guildManagementData.KickList.find(n => n.Id == memberId);
        
        if(targetMemberId == null)
        {
            log.info("Can't find in member list!");
            return toReturn;
        }

        // Remove from kick list
        guildManagementData.KickList = guildManagementData.KickList.filter(n => n.Id != memberId);

        // Update guild data info in guild master
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "guild_management_data": unescape(JSON.stringify(guildManagementData))
            }
        });
        
        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;
}
handlers["guildKickCancel"] = GuildKickCancel;

interface IGuildMasterTransfer{
    guildId: string;
    error: boolean;    
}

let GuildMasterTransfer = function(args: any, context: IPlayFabContext): IGuildMasterTransfer{
    let toReturn: IGuildMasterTransfer = {guildId: "", error: true};
    let newGuildMasterId = "";

    if(args.newGuildMasterId)
        newGuildMasterId = args.newGuildMasterId;

    // Get guild data
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: [
            "guild_management_data", 
            "guild_total_exp", 
            "guild_master_exp",
            "guild_member_list",
            "guild_cache_list", 
            "guild_applicant_list"
           ]
    });

    // Check user is guild master or not
    if(!IsGuildMaster(getUserReadOnlyDataResult.Data["guild_management_data"])) 
    {
        log.info("This user is not guild master!");
        return toReturn;
    }

    if(getUserReadOnlyDataResult.Data["guild_management_data"] != null && getUserReadOnlyDataResult.Data["guild_member_list"] != null 
    && getUserReadOnlyDataResult.Data["guild_cache_list"] != null && getUserReadOnlyDataResult.Data["guild_applicant_list"] != null)
    {
        let guildManagementData = JSON.parse(getUserReadOnlyDataResult.Data["guild_management_data"].Value);
        let guildMemberData = JSON.parse(getUserReadOnlyDataResult.Data["guild_member_list"].Value);

        let newGuildMasterInfo = guildMemberData.MemberList.find(n => n.Id == newGuildMasterId);
        let oldGuildMasterInfo = guildMemberData.MemberList.find(n => n.Id == guildManagementData.GuildId);

        // Check this new guild master is member of our guild or not
        if(newGuildMasterInfo == null || oldGuildMasterInfo == null)
        {
            log.info("New guild master or old guild master is not member of our guild!");
            return toReturn;
        }

        guildManagementData.GuildId = newGuildMasterId;        
        
        let guildTotalExp = 0;
        if(getUserReadOnlyDataResult.Data["guild_total_exp"] != null)
            guildTotalExp = parseInt(getUserReadOnlyDataResult.Data["guild_total_exp"].Value);

        let guildMasterExp = 0;
        if(getUserReadOnlyDataResult.Data["guild_master_exp"] != null)
            guildMasterExp = parseInt(getUserReadOnlyDataResult.Data["guild_master_exp"].Value);

        // Add new guild master
        try {
            let addMembersResult = entity.AddMembers({
                Group: {Id: guildManagementData.GroupId, Type: null},
                Members: [
                    {
                        Id: newGuildMasterInfo.EntityId,
                        Type: newGuildMasterInfo.EntityType
                    }
                ],
                RoleId: "admins"
            });
        } catch (error) {
            log.info("Error in entity.AddNewGuildMaster : " + JSON.stringify(error));
            return toReturn;
        }

        // Delete old guild master role (admis to memebers)
        try {
            let removeMembersResult = entity.RemoveMembers({
                Group: {Id: guildManagementData.GroupId, Type: null},
                Members: [
                    {
                        Id: oldGuildMasterInfo.EntityId,
                        Type: oldGuildMasterInfo.EntityType
                    }
                ],
            });
        } catch (error) {
            log.info("Error in entity.RemoveOldGuildMaster : " + JSON.stringify(error));
            return toReturn;
        }
    
        // Update new guild master
        let updateNewGuildMasterReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: newGuildMasterId,
            Data: {
                "guild_id": newGuildMasterId,
                "guild_management_data": unescape(JSON.stringify(guildManagementData)),
                "guild_total_exp": guildTotalExp.toString(),
                "guild_master_exp": guildMasterExp.toString(),
                "guild_member_list": getUserReadOnlyDataResult.Data["guild_member_list"].Value,
                "guild_cache_list": getUserReadOnlyDataResult.Data["guild_cache_list"].Value,
                "guild_applicant_list": getUserReadOnlyDataResult.Data["guild_applicant_list"].Value                
            }
        });

        // Update old Guild master
        let updateOldGuildMasterReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "guild_id": newGuildMasterId,
                "guild_management_data": null,
                "guild_total_exp": null,
                "guild_master_exp": null,
                "guild_member_list": null,
                "guild_cache_list": null,
                "guild_applicant_list": null
            }
        });

        // Update guild member
        for(let i = 0; i < guildMemberData.MemberList.length; i++)
        {
            // Exclude old guild master and new guild master
            if(guildMemberData.MemberList[i].Id != currentPlayerId && guildMemberData.MemberList[i].Id != newGuildMasterId)
            {
                let updateMemberReadOnlyDataResult = server.UpdateUserReadOnlyData({
                    PlayFabId: guildMemberData.MemberList[i].Id,
                    Data: {
                        "guild_id": newGuildMasterId
                    }
                });
            }
        }

        let serverIdModifier = GetServerIdModifier(currentPlayerId);

        // Get old total score
        let currentStatValue = server.GetPlayerStatistics({
            PlayFabId: currentPlayerId,
            StatisticNames: ["guild_raid" + serverIdModifier]
        }).Statistics;

        let oldMasterScore = 0;
        if(currentStatValue[0] != null)
            oldMasterScore = currentStatValue[0].Value;

        // Remove guild related leaderboard
        server.UpdatePlayerStatistics({
            PlayFabId: currentPlayerId,
            Statistics: [
                {
                    "StatisticName": "guild_raid" + serverIdModifier,
                    "Value": oldMasterScore * -1
                },
                {
                    "StatisticName": "guild_recommend" + serverIdModifier,
                    "Value": 0
                }
            ]
        });

        // Update new guild master's leaderboard
        if(oldMasterScore != 0)
        {
            server.UpdatePlayerStatistics({
                PlayFabId: newGuildMasterId,
                Statistics: [
                    {
                        "StatisticName": "guild_raid" + serverIdModifier,
                        "Value": oldMasterScore
                    }
                ]
            });
        }
        
        UpdateGuildRecommendationScore(GetServerIdModifierFromServerId(guildManagementData.ServerId), guildManagementData.GuildId, parseInt(guildManagementData.TimestampDay), guildMemberData.MemberList.length);

        // This guild's id is new guild's id
        toReturn.guildId = guildManagementData.GuildId;
        toReturn.error = false;
        return toReturn;
    }
    else
    {
        log.info("User is not guild master or don't have any guild!");
        return toReturn;
    }
}
handlers["guildMasterTransfer"] = GuildMasterTransfer;

interface IGuildSettingEdit{
    error: boolean;
}

let GuildSettingEdit = function(args: any, context: IPlayFabContext): IGuildSettingEdit{
    let toReturn: IGuildSettingEdit = {error: true};
    let guildNotice = "";
    let guildFlagIndex = 0;
    let requiredStageProgress = 1;
    let isPrivate = false;
    
    if(args.guildNotice)
        guildNotice = args.guildNotice;
    if(args.guildFlagIndex)
        guildFlagIndex = args.guildFlagIndex;
    if(args.requiredStageProgress)
        requiredStageProgress = args.requiredStageProgress;
    if(args.isPrivate)
        isPrivate = args.isPrivate;

    // Check notice limit
    if(guildNotice.length > 100)
    {
        log.info("Notice is too long!");
        return toReturn;
    }

    // Get guild id
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_management_data"]
    });

    if(getUserReadOnlyDataResult.Data["guild_management_data"] != null)
    {
        // Check user is guild master or not
        if(!IsGuildMaster(getUserReadOnlyDataResult.Data["guild_management_data"])) 
        {
            log.info("This user is not guild master!");
            return toReturn;
        }

        // Set new private value
        let guildManagementData = JSON.parse(getUserReadOnlyDataResult.Data["guild_management_data"].Value);

        guildManagementData.GuildNotice = guildNotice;
        guildManagementData.GuildFlagIndex = guildFlagIndex;
        guildManagementData.RequiredStageProgress = requiredStageProgress;
        guildManagementData.IsPrivate = isPrivate;

        // Update guild data
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "guild_management_data": unescape(JSON.stringify(guildManagementData))
            }
        });

        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;
}
handlers["guildSettingEdit"] = GuildSettingEdit;

///////////////////////// For Guild Members /////////////////////////
interface IGuildJoin{
    targetGuildId: string;
    guildManagementData: {};
    memberCacheDataList: {};
    guildLevel: number;
    guildExp: number;
    guildNextExp: number;
    dailyLimitFreeDonateLeft: number;
    dailyLimitDonateLeft: number;
    reasonOfError: string;
    error: boolean;
}

let GuildJoin = function(args: any, context: IPlayFabContext): IGuildJoin{

    let toReturn: IGuildJoin = {targetGuildId: "", guildManagementData: {}, memberCacheDataList: {}, guildLevel: 1, guildExp: 0, guildNextExp: 0, dailyLimitFreeDonateLeft: 0, dailyLimitDonateLeft: 0, reasonOfError: "unknown", error: true};  
    let guildId = "";
    let userName = "";
    let userStageProgress = 1;

    if(args.guildId)
        guildId = args.guildId;
    if(args.userName)
        userName = args.userName;
    if(args.userStageProgress)
        userStageProgress = args.userStageProgress;

    // For log
    toReturn.targetGuildId = guildId;

    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_id", "apply_guild_list"]
    });

    // Check user already have a guild or not
    if(getUserReadOnlyDataResult.Data["guild_id"] != null)
    {
        log.info("User already have joined a guild! Guild Id: " + getUserReadOnlyDataResult.Data["guild_id"].Value);
        return toReturn;
    }

    let applyGuildData: any = JSON.parse("{\"ApplyGuildList\":[]}");
    if(getUserReadOnlyDataResult.Data["apply_guild_list"] != null)
    {
        applyGuildData = JSON.parse(getUserReadOnlyDataResult.Data["apply_guild_list"].Value);
        
        // Check applied guild exist
        if(applyGuildData.ApplyGuildList.length > 0)
        {
            // Check applied guild still alive
            let getAppliedGuildReadOnlyDataResult = server.GetUserReadOnlyData({
                PlayFabId: applyGuildData.ApplyGuildList[0],
                Keys: ["guild_management_data"]
            });

            if(getAppliedGuildReadOnlyDataResult.Data["guild_management_data"] != null)
            {
                let appliedGuildManagementData = JSON.parse(getAppliedGuildReadOnlyDataResult.Data["guild_management_data"].Value);

                if(appliedGuildManagementData.IsPrivate)
                {
                    toReturn.reasonOfError = "guild_applied_guild_exist";
                    return toReturn;
                }
                else
                    applyGuildData.ApplyGuildList = applyGuildData.ApplyGuildList.filter(n => n.toUpperCase() != guildId);
            }
            else
                applyGuildData.ApplyGuildList = applyGuildData.ApplyGuildList.filter(n => n.toUpperCase() != guildId);
        }
    }

    // Get target guild info
    let getGuildMasterReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: guildId,
        Keys: ["guild_management_data", "guild_total_exp", "guild_master_exp", "guild_member_list", "guild_cache_list"]
    });

    if(getGuildMasterReadOnlyDataResult.Data["guild_management_data"] != null && getGuildMasterReadOnlyDataResult.Data["guild_member_list"] != null && getGuildMasterReadOnlyDataResult.Data["guild_cache_list"])
    {
        let guildManagementData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_management_data"].Value);
        let guildMemberData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_member_list"].Value);
        let guildCacheData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_cache_list"].Value);

        // Get guild level and exp info
        let guildLevelInfo = GetGuildLevelAndExp(getGuildMasterReadOnlyDataResult.Data["guild_total_exp"], getGuildMasterReadOnlyDataResult.Data["guild_master_exp"], null);

        if(guildLevelInfo.error)
        {
            log.info("guild level info data is wrong!")

            return toReturn;
        }

        // Check limit member count
        if(guildMemberData.MemberList.length >= GetGuildMemberLimitCount(guildLevelInfo.level))
        {
            log.info("Guild member count is full!")

            toReturn.reasonOfError = "guild_member_full";
            return toReturn;
        }

        // Check need stage progress
        if(userStageProgress < guildManagementData.RequiredStageProgress)
        {
            log.info("User's stage progress is not enough for join this guild!");

            toReturn.reasonOfError = "guild_stage_not_enough";
            return toReturn;
        }

        // Check banned user
        if(guildManagementData.KickList.find(n => n.Id == currentPlayerId) != null)
        {
            log.info("This user's id include in kick list!");

            toReturn.reasonOfError = "guild_banned";
            return toReturn;
        }

        let getPlayerTagsResult = server.GetPlayerTags({
            PlayFabId: currentPlayerId
        });

        let serverId = "";
        if(getPlayerTagsResult != null && getPlayerTagsResult.Tags.length > 0)
            serverId = GetServerIdFromTag(getPlayerTagsResult.Tags.filter(n => n.indexOf("Server") >= 0)[0]);

        // Check server id
        if(serverId != guildManagementData.ServerId)
        {
            log.info("Server is not same!");
            return toReturn;
        }

        // Check private guild
        if(!guildManagementData.IsPrivate)
        {
            // Get applicant account info
            let getUserAccountInfoResult = server.GetUserAccountInfo({
                PlayFabId: currentPlayerId
            });

            let currentTimestamp = Math.floor(+ new Date() / (1000 * 60 * 60 * 24));
            
            guildMemberData.MemberList.push({"Id": currentPlayerId, "EntityId": getUserAccountInfoResult.UserInfo.TitleInfo.TitlePlayerAccount.Id, 
            "EntityType": getUserAccountInfoResult.UserInfo.TitleInfo.TitlePlayerAccount.Type, "JoinTimeStamp": Math.floor(+new Date() / 1000)});
            
            guildCacheData.MemberCacheDataList.push(
                {
                    "Id": currentPlayerId, 
                    "Name": userName, 
                    "StageProgress": userStageProgress, 
                    "LastTimeStamp": Math.floor(+ new Date() / (1000 * 60 * 60)), 
                    "TotalContribution": 0,
                    "RaidScore": 0,
                    "RaidSeason": -1
                }
            );

            // Check for the case where guildCacheData is not matching with guild member list
            for(let i = 0; i < guildMemberData.MemberList.length; i ++)
            {
                if(guildCacheData.MemberCacheDataList.find(n => n.Id == guildMemberData.MemberList[i].Id) == null)
                {
                    guildCacheData.MemberCacheDataList.push(
                        {
                            "Id": guildMemberData.MemberList[i].Id, 
                            "Name": "???", 
                            "Level": -1, 
                            "LastTimeStamp": -1,
                            "TotalContribution": 0,
                            "RaidScore": 0,
                            "RaidSeason": -1
                        }
                    );
                }
            }
            
            // Only leave member data cache where member list exists
            let newCacheDataList = [];
            for(let i = 0; i < guildMemberData.MemberList.length; i ++)
            {
                newCacheDataList.push(guildCacheData.MemberCacheDataList.find(n => n.Id == guildMemberData.MemberList[i].Id));
            }
            guildCacheData.MemberCacheDataList = newCacheDataList;

            // Update user data
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "guild_id": guildId,
                    "apply_guild_list": unescape(JSON.stringify(applyGuildData))
                }
            });

            // Update guild data info in guild master
            let updateGuildMasterReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: guildId,
                Data: {
                    "guild_member_list": unescape(JSON.stringify(guildMemberData)),
                    "guild_cache_list": unescape(JSON.stringify(guildCacheData))
                }
            });

            // Update guild recommendation score
            UpdateGuildRecommendationScore(GetServerIdModifierFromServerId(guildManagementData.ServerId), guildManagementData.GuildId, parseInt(guildManagementData.TimestampDay), guildMemberData.MemberList.length);

            toReturn.guildManagementData = guildManagementData;
            toReturn.memberCacheDataList = {"MemberCacheDataList": guildCacheData.MemberCacheDataList};
            toReturn.guildLevel = guildLevelInfo.level;
            toReturn.guildExp = guildLevelInfo.exp;
            toReturn.guildNextExp = guildLevelInfo.nextExp;
            toReturn.dailyLimitFreeDonateLeft = GetLeftDailyLimit("guild_free_donate", "0");
            toReturn.dailyLimitDonateLeft = GetLeftDailyLimit("guild_donate", "0");

            toReturn.error = false;
            return toReturn;
        }
        else
            return toReturn;
    }
    else
        return toReturn;
}
handlers["guildJoin"] = GuildJoin;

interface IGuildPrivateJoin{
    targetGuildId: string;
    applyGuildList: {};
    reasonOfError: string;
    error: boolean;
}

let GuildPrivateJoin = function(args: any, context: IPlayFabContext): IGuildPrivateJoin{

    let toReturn: IGuildPrivateJoin = {targetGuildId: "", applyGuildList: {}, reasonOfError: "unknown", error: true};  
    let guildId = "";
    let userName = "";
    let userStageProgress = "";

    if(args.guildId)
        guildId = args.guildId;
    if(args.userName)
        userName = args.userName;
    if(args.userStageProgress)
        userStageProgress = args.userStageProgress;

    // For log
    toReturn.targetGuildId = guildId;

    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_id", "apply_guild_list"]
    });

    // Check user already have a guild or not
    if(getUserReadOnlyDataResult.Data["guild_id"] != null)
    {
        log.info("User already have joined a guild. Guild Id: " + getUserReadOnlyDataResult.Data["guild_id"].Value);
        return toReturn;
    }

    // Get target guild info
    let getGuildMasterReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: guildId,
        Keys: ["guild_management_data", "guild_applicant_list"]
    });

    if(getGuildMasterReadOnlyDataResult.Data["guild_management_data"] != null && getGuildMasterReadOnlyDataResult.Data["guild_applicant_list"] != null)
    {
        let guildManagementData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_management_data"].Value);
        let guildApplicantData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_applicant_list"].Value);

        let getPlayerTagsResult = server.GetPlayerTags({
            PlayFabId: currentPlayerId
        });

        let serverId = "";
        if(getPlayerTagsResult != null && getPlayerTagsResult.Tags.length > 0)
            serverId = GetServerIdFromTag(getPlayerTagsResult.Tags.filter(n => n.indexOf("Server") >= 0)[0]);

        // Check server id
        if(serverId != guildManagementData.ServerId)
        {
            log.info("Server is not same!");
            return toReturn;
        }

        // Check private guild
        if(guildManagementData.IsPrivate)
        {
            // Check limit applicant count (limit 8)
            if(guildApplicantData.ApplicantList.length > 7)
            {
                log.info("Guild applicant count is full!")

                toReturn.reasonOfError = "guild_applicant_full";
                return toReturn;
            }

            // Check user in kick list
            if(guildManagementData.KickList.find(n => n.Id == currentPlayerId))
            {
                log.info("This user's id include in kick list!");

                toReturn.reasonOfError = "guild_banned";
                return toReturn;
            }

            // Check user stage progress
            if(guildManagementData.RequiredStageProgress > userStageProgress)
            {
                log.info("User's stage progress is not enough for join this guild!");
                
                toReturn.reasonOfError = "guild_stage_not_enough";
                return toReturn;
            }
            
            let applyGuildData: any = JSON.parse("{\"ApplyGuildList\":[]}");
            if(getUserReadOnlyDataResult.Data["apply_guild_list"] != null)
            {
                applyGuildData = JSON.parse(getUserReadOnlyDataResult.Data["apply_guild_list"].Value);
                
                // Check apply guild exist
                if(applyGuildData.ApplyGuildList.length > 0)
                {
                    // Check applied guild still alive
                    let getAppliedGuildReadOnlyDataResult = server.GetUserReadOnlyData({
                        PlayFabId: applyGuildData.ApplyGuildList[0],
                        Keys: ["guild_management_data"]
                    });

                    if(getAppliedGuildReadOnlyDataResult.Data["guild_management_data"] != null)
                    {
                        let appliedGuildManagementData = JSON.parse(getAppliedGuildReadOnlyDataResult.Data["guild_management_data"].Value);

                        if(appliedGuildManagementData.IsPrivate)
                        {
                            toReturn.reasonOfError = "guild_applied_guild_exist";
                            return toReturn;
                        }
                        else
                            applyGuildData.ApplyGuildList = applyGuildData.ApplyGuildList.filter(n => n.toUpperCase() != guildId);
                    }
                    else
                        applyGuildData.ApplyGuildList = applyGuildData.ApplyGuildList.filter(n => n.toUpperCase() != guildId);
                }
            }
            
            let isDuplicated = false;

            for(let i = 0; i < guildApplicantData.ApplicantList.length; i++)
            {
                if(guildApplicantData.ApplicantList[i].Id == currentPlayerId)
                {
                    guildApplicantData.ApplicantList[i].Name = userName;
                    guildApplicantData.ApplicantList[i].StageProgress = userStageProgress;
                    isDuplicated = true;
                    break;
                }
            }

            if(!isDuplicated)
                guildApplicantData.ApplicantList.push({"Id": currentPlayerId, "Name": userName, "StageProgress": userStageProgress});
            
            applyGuildData.ApplyGuildList.push(guildId);

            // Update apply guild list
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "apply_guild_list": unescape(JSON.stringify(applyGuildData))
                }
            });

            // Update guild data info in guild master
            let updateGuildMasterReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: guildId,
                Data: {
                    "guild_applicant_list": unescape(JSON.stringify(guildApplicantData))
                }
            });

            toReturn.applyGuildList = {"ApplyGuildList": applyGuildData.ApplyGuildList};
            toReturn.error = false;
            return toReturn;
        }
        else
            return toReturn;
    }
    else
        return toReturn;
}
handlers["guildPrivateJoin"] = GuildPrivateJoin;

interface IGuildPrivateJoinCancel{
    targetGuildId: string;
    applyGuildList: {};
    error: boolean;
}

let GuildPrivateJoinCancel = function(args: any, context: IPlayFabContext): IGuildPrivateJoinCancel{

    let toReturn: IGuildPrivateJoinCancel = {targetGuildId: "", applyGuildList: {}, error: true};  
    let guildId = "";
    let applyGuildData: any = JSON.parse("{\"ApplyGuildList\":[]}");

    if(args.guildId)
        guildId = args.guildId;

    // For log
    toReturn.targetGuildId = guildId;

    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_id", "apply_guild_list"]
    });

    // Check user already have a guild or not
    if(getUserReadOnlyDataResult.Data["guild_id"] != null)
    {
        log.info("User already have joined a guild. Guild Id: " + getUserReadOnlyDataResult.Data["guild_id"].Value);
        return toReturn;
    }

    if(getUserReadOnlyDataResult.Data["apply_guild_list"] != null)
    {
        applyGuildData = JSON.parse(getUserReadOnlyDataResult.Data["apply_guild_list"].Value);
        
        // Check apply guild id
        if(!applyGuildData.ApplyGuildList.find(n => n.toUpperCase() == guildId))
        {
            log.info("Apply guild id is not exist!");
            return toReturn;
        }   
    }

    // Get target guild info
    let getGuildMasterReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: guildId,
        Keys: ["guild_applicant_list"]
    });

    if(getGuildMasterReadOnlyDataResult.Data["guild_applicant_list"] != null)
    {
        let guildApplicantData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_applicant_list"].Value);

        // Remove from applicion list
        guildApplicantData.ApplicantList = guildApplicantData.ApplicantList.filter(n => n.Id != currentPlayerId);
            
        // Remove from apply guild list
        applyGuildData.ApplyGuildList = applyGuildData.ApplyGuildList.filter(n => n.toUpperCase() != guildId);

        // Update apply guild list
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "apply_guild_list": unescape(JSON.stringify(applyGuildData))
            }
        });

        // Update guild data info in guild master
        let updateGuildMasterReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: guildId,
            Data: {
                "guild_applicant_list": unescape(JSON.stringify(guildApplicantData))
            }
        });

        toReturn.applyGuildList = {"ApplyGuildList": applyGuildData.ApplyGuildList};
        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;
}
handlers["guildPrivateJoinCancel"] = GuildPrivateJoinCancel;

interface IGuildLeave{
    error: boolean;
}

let GuildLeave = function(args: any, context: IPlayFabContext): IGuildLeave{
    
    let toReturn: IGuildLeave = {error: true};  

    // Get current guild id and guild master id
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_id"]
    });

    if(getUserReadOnlyDataResult.Data["guild_id"] != null)
    {
        // Check user is guild master or not
        if(getUserReadOnlyDataResult.Data["guild_id"].Value == currentPlayerId) 
        {
            log.info("Guild Master can't leave guild!");
            return toReturn;
        }

        let getGuildMasterReadOnlyDataResult = server.GetUserReadOnlyData({
            PlayFabId: getUserReadOnlyDataResult.Data["guild_id"].Value,
            Keys: ["guild_management_data", "guild_member_list", "guild_cache_list"]
        });
        
        if(getGuildMasterReadOnlyDataResult.Data["guild_management_data"] != null && getGuildMasterReadOnlyDataResult.Data["guild_member_list"] != null && getGuildMasterReadOnlyDataResult.Data["guild_cache_list"] != null)
        {
            let guildManagementData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_management_data"].Value);
            let guildMemberData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_member_list"].Value);
            let guildCacheData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_cache_list"].Value);
            
            let targetMemberInfo = guildMemberData.MemberList.find(n => n.Id == currentPlayerId);
            
            if(guildMemberData.MemberList == null)
                return toReturn;

            guildMemberData.MemberList = guildMemberData.MemberList.filter(n => n.Id != currentPlayerId);
            guildCacheData.MemberCacheDataList = guildCacheData.MemberCacheDataList.filter(n => n.Id != currentPlayerId);

            // Remove from guild
            let updateGuildMasterReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: getUserReadOnlyDataResult.Data["guild_id"].Value,
                Data: {
                    "guild_member_list": unescape(JSON.stringify(guildMemberData)),
                    "guild_cache_list": unescape(JSON.stringify(guildCacheData))
                }
            });

            // Update guild recommendation score
            UpdateGuildRecommendationScore(GetServerIdModifierFromServerId(guildManagementData.ServerId), guildManagementData.GuildId, parseInt(guildManagementData.TimestampDay), guildMemberData.MemberList.length);

            // Get guild leave time stamp
            let lastGuildLeaveTimeStamp = Math.floor(+new Date() / 1000);

            // Delete guild info and save last guild leave time stamp
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "guild_id": null,
                    "guild_raid_reward_last_played_season": null,
                    "last_guild_leave_timestamp": lastGuildLeaveTimeStamp.toString()
                }
            });
            
            toReturn.error = false;
            return toReturn;
        }
        else
            return toReturn;
    }
    else
        return toReturn;
}
handlers["guildLeave"] = GuildLeave;

interface IGuildRefresh{
    guildManagementData: {};
    guildLevel: number;
    guildExp: number;
    guildNextExp: number;
    memberList: {};
    applicantList: {};
    dailyLimitFreeDonateLeft: number;
    dailyLimitDonateLeft: number;
    serverTimeStamp: number;
    error: boolean;
}

let GuildRefresh = function(args: any, context: IPlayFabContext): IGuildRefresh{
    let toReturn: IGuildRefresh = {guildManagementData: {}, guildLevel: 1, guildExp: 0, guildNextExp: 0, memberList: {}, applicantList: {}, 
    dailyLimitFreeDonateLeft: 0, dailyLimitDonateLeft: 0, serverTimeStamp: 0, error: true};  

    let userName = "";
    let userStageProgress = 1;
    let skipUpdateCache = false;

    if(args.userName)
        userName = args.userName;
    if(args.userStageProgress)
        userStageProgress = args.userStageProgress;
    if(args.skipUpdateCache)
        skipUpdateCache = args.skipUpdateCache;

    // Get guild master id
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_id", "last_guild_leave_timestamp"]
    });

    if(getUserReadOnlyDataResult.Data["guild_id"] != null)
    {
        // Get guild data
        let getGuildMasterReadOnlyDataResult = server.GetUserReadOnlyData({
            PlayFabId: getUserReadOnlyDataResult.Data["guild_id"].Value,
            Keys: ["guild_management_data", "guild_member_list", "guild_cache_list", "guild_applicant_list", "guild_total_exp", "guild_master_exp"]
        });
        
        if(getGuildMasterReadOnlyDataResult.Data["guild_management_data"] != null && getGuildMasterReadOnlyDataResult.Data["guild_member_list"] != null 
        && getGuildMasterReadOnlyDataResult.Data["guild_cache_list"] != null && getGuildMasterReadOnlyDataResult.Data["guild_applicant_list"] != null)
        {
            let guildManagementData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_management_data"].Value);
            let guildMemberData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_member_list"].Value);
            let guildCacheData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_cache_list"].Value);
            let guildApplicantData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_applicant_list"].Value);
            
            let myData = guildMemberData.MemberList.find(n => n.Id == currentPlayerId);            
            if(myData != null) // Check whether this player is a member of this guild
            {
                // Check for the case where guildCacheData is not matching with guild member list
                for(let i = 0; i < guildMemberData.MemberList.length; i ++)
                {
                    if(guildCacheData.MemberCacheDataList.find(n => n.Id == guildMemberData.MemberList[i].Id) == null)
                    {
                        guildCacheData.MemberCacheDataList.push(
                            {
                                "Id": guildMemberData.MemberList[i].Id, 
                                "Name": "???", 
                                "Level": -1, 
                                "LastTimeStamp": -1,
                                "TotalContribution": 0,
                                "RaidScore": 0,
                                "RaidSeason": -1
                            }
                        );
                    }
                }

                // Only leave member data cache where member list exists
                let newCacheDataList = [];
                for(let i = 0; i < guildMemberData.MemberList.length; i ++)
                {
                    newCacheDataList.push(guildCacheData.MemberCacheDataList.find(n => n.Id == guildMemberData.MemberList[i].Id));
                }
                guildCacheData.MemberCacheDataList = newCacheDataList;
                
                // Will only save guild cache when actual changes are made
                let changesMadeForCache = false; 

                // Update my cache data
                let myCacheData = guildCacheData.MemberCacheDataList.find(n => n.Id == myData.Id);
                
                if(myCacheData.Name != userName)
                    changesMadeForCache = true;
                myCacheData.Name = userName;
                if(myCacheData.StageProgress != userStageProgress)
                    changesMadeForCache = true;
                myCacheData.StageProgress = userStageProgress;
                if(myCacheData.LastTimeStamp != Math.floor(+ new Date() / (1000 * 60 * 60)))
                    changesMadeForCache = true;
                myCacheData.LastTimeStamp = Math.floor(+ new Date() / (1000 * 60 * 60));
                
                // Upate cache data of all members to renew the raid statistics

                let serverIdModifier = GetServerIdModifier(currentPlayerId);
                let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
                    PlayFabId: currentPlayerId,
                    StatisticName: "guild_raid" + serverIdModifier,
                    MaxResultsCount: 1
                });
                let season = getLeaderboardAroundUserResult.Version;
                
                for(let i = 0; i < guildCacheData.MemberCacheDataList.length ; i ++)
                {
                    if(guildCacheData.MemberCacheDataList[i].RaidSeason != season)
                    {
                        guildCacheData.MemberCacheDataList[i].RaidScore = 0;
                        changesMadeForCache = true;
                    }

                    guildCacheData.MemberCacheDataList[i].RaidSeason = season;
                }

                if(!skipUpdateCache && changesMadeForCache) // Check this to prevent hitting playfab limits & Cache save is actually needed
                {
                    let updateGuildMasterReadOnlyDataResult = server.UpdateUserReadOnlyData({
                        PlayFabId: getUserReadOnlyDataResult.Data["guild_id"].Value,
                        Data: {
                            "guild_cache_list": unescape(JSON.stringify(guildCacheData))
                        }
                    });
                }

                // Get guild level and exp info
                let levelInfo = GetGuildLevelAndExp(getGuildMasterReadOnlyDataResult.Data["guild_total_exp"], getGuildMasterReadOnlyDataResult.Data["guild_master_exp"], null);

                if(levelInfo.error)
                    return toReturn;
                
                toReturn.guildManagementData = guildManagementData;
                toReturn.guildLevel = levelInfo.level;
                toReturn.guildExp = levelInfo.exp;
                toReturn.guildNextExp = levelInfo.nextExp;
                toReturn.memberList = {"MemberCacheDataList": guildCacheData.MemberCacheDataList};

                // If user is guild master then return applicant list.
                if(currentPlayerId == getUserReadOnlyDataResult.Data["guild_id"].Value)
                    toReturn.applicantList = {"ApplicantList": guildApplicantData.ApplicantList};
                else
                    toReturn.applicantList = {"ApplicantList": []};
                
                toReturn.dailyLimitFreeDonateLeft = GetLeftDailyLimit("guild_free_donate", "0");
                toReturn.dailyLimitDonateLeft = GetLeftDailyLimit("guild_donate", "0");                
                toReturn.serverTimeStamp = Math.floor(+new Date() / 1000);
                toReturn.error = false;
                return toReturn;
            }
            else
            {
                log.info("user not exist in member list");

                let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                    PlayFabId: currentPlayerId,
                    Data: {
                        "guild_id": null,
                        "guild_raid_reward_last_played_season": null
                    }
                });
                
                toReturn.guildManagementData = null;
                toReturn.serverTimeStamp = Math.floor(+new Date() / 1000);
                toReturn.error = true;
                
                return toReturn;
            }
        }
        else
        {
            log.info("The guild master has data issues (Likely the guild ownership has been changed)");

            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "guild_id": null,
                    "guild_raid_reward_last_played_season": null
                }
            });

            toReturn.guildManagementData = null;
            toReturn.serverTimeStamp = Math.floor(+new Date() / 1000);
            toReturn.error = true;

            return toReturn;
        }
    }
    else
    {
        log.info("User don't have any guild!");

        toReturn.guildManagementData = null;
        toReturn.serverTimeStamp = Math.floor(+new Date() / 1000);
        toReturn.error = false;
        
        return toReturn;
    }
}
handlers["guildRefresh"] = GuildRefresh;

interface IGuilSearchdWithId{
    targetGuildData: {};
    applyGuildList: {};
    error: boolean;
}

let GuildSearchWithId = function(args: any, context: IPlayFabContext): IGuilSearchdWithId{
    let toReturn: IGuilSearchdWithId = {targetGuildData: {}, applyGuildList: {}, error: true};
    let guildItemList = [];
    let guildId = "";
    
    if(args.guildId)
        guildId = args.guildId;

    if(guildId == "")
        return toReturn;

    // Get user server info
    let userServerIdModifier = GetServerIdModifier(currentPlayerId);

    // Get target guild server info
    let serverIdModifier = GetServerIdModifier(guildId);

    if(userServerIdModifier != serverIdModifier)
        return toReturn;

    // Get guild data
    let getGuildMasterReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: guildId,
        Keys: ["guild_management_data", "guild_member_list", "guild_total_exp", "guild_master_exp"]
    });
    
    if(getGuildMasterReadOnlyDataResult.Data["guild_management_data"] != null && getGuildMasterReadOnlyDataResult.Data["guild_member_list"] != null)
    {
        let guildManagementData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_management_data"].Value);
        let guildMemberIdData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_member_list"].Value);
        let guildLevelInfo = GetGuildLevelAndExp(getGuildMasterReadOnlyDataResult.Data["guild_total_exp"], getGuildMasterReadOnlyDataResult.Data["guild_master_exp"], null);
        let guildRanking = 0;

        if(guildLevelInfo.error)
            return toReturn;

        let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
            PlayFabId: guildId,
            StatisticName: "guild_raid" + serverIdModifier,
            MaxResultsCount: 1
        });
        guildRanking = getLeaderboardAroundUserResult.Leaderboard[0].Position;
        
        guildItemList.push({
            "GuildId": guildId.toUpperCase(),
            "GuildName": guildManagementData.GuildName,
            "GuildNotice": guildManagementData.GuildNotice,
            "GuildRanking": guildRanking,
            "GuildLevel": guildLevelInfo.level,
            "GuildRequiredStageProgress": guildManagementData.RequiredStageProgress,
            "GuildFlagIndex": guildManagementData.GuildFlagIndex,
            "GuildMemberCount": guildMemberIdData.MemberList.length,
            "IsPrivate": guildManagementData.IsPrivate
        });

        let getMyReadOnlyDataResult = server.GetUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Keys: ["apply_guild_list"]
        });
    
        if(getMyReadOnlyDataResult.Data["apply_guild_list"] != null)
        {
            let applyGuildData = JSON.parse(getMyReadOnlyDataResult.Data["apply_guild_list"].Value);
            toReturn.applyGuildList = {"ApplyGuildList": applyGuildData.ApplyGuildList};
        }
        else
        {
            let applyGuildData: any = JSON.parse("{\"ApplyGuildList\":[]}");
            toReturn.applyGuildList = {"ApplyGuildList": applyGuildData.ApplyGuildList};
        }

        toReturn.targetGuildData = {"GuildDataList": guildItemList};
        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;
}
handlers["guildSearchWithId"] = GuildSearchWithId;

interface IGuildGetRecommendation{
    myGuildId: string;
    guildList: {};
    applyGuildList: {};
    leftGuildJoinTime: number;
    error: boolean;
}

let GuildGetRecommendation = function(args: any, context: IPlayFabContext): IGuildGetRecommendation{
    let toReturn: IGuildGetRecommendation = {myGuildId: "", guildList: {}, applyGuildList: {}, leftGuildJoinTime: 0, error: true};
    let guildItemList = [];
    let lastGuildLeaveTimeStamp = 0;
    let maxGuildList = 5;

    // Check last leave guild timestamp
    let getMyReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_id" ,"apply_guild_list", "last_guild_leave_timestamp"]
    });

    if(getMyReadOnlyDataResult.Data["last_guild_leave_timestamp"] != null)
        lastGuildLeaveTimeStamp = parseInt(getMyReadOnlyDataResult.Data["last_guild_leave_timestamp"].Value);

    // Get server time
    let serverTimeStamp = Math.floor(+new Date() / 1000);
    toReturn.leftGuildJoinTime = (lastGuildLeaveTimeStamp + 172800) - serverTimeStamp;

    if(toReturn.leftGuildJoinTime > 0)
    {
        toReturn.error = false;
        return toReturn;
    }

    let serverIdModifier = GetServerIdModifier(currentPlayerId);

    let getLeaderboardResult = server.GetLeaderboard({
        StatisticName: "guild_recommend" + serverIdModifier,
        MaxResultsCount: 50,
        StartPosition: 0
    });

    let filteredList = getLeaderboardResult.Leaderboard.filter(n => n.StatValue > 0);
    ShuffleArray(filteredList);

    // Get applied guild info
    if(getMyReadOnlyDataResult.Data["apply_guild_list"] != null)
    {
        let applyGuildData = JSON.parse(getMyReadOnlyDataResult.Data["apply_guild_list"].Value);
        toReturn.applyGuildList = {"ApplyGuildList": applyGuildData.ApplyGuildList};

        if(applyGuildData.ApplyGuildList.length > 0)
        {
            // Get my applied guild
            let getMyAppliedGuildLeaderboardResult = server.GetLeaderboardAroundUser({
                PlayFabId: applyGuildData.ApplyGuildList[0],
                StatisticName: "guild_recommend" + serverIdModifier,
                MaxResultsCount: 1
            });
            
            if(getMyAppliedGuildLeaderboardResult.Leaderboard.length > 0)
            {
                let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
                    PlayFabId: getMyAppliedGuildLeaderboardResult.Leaderboard[0].PlayFabId,
                    Keys: ["guild_management_data", "guild_member_list", "guild_total_exp", "guild_master_exp"]
                });
            
                if(getUserReadOnlyDataResult.Data["guild_management_data"] != null && getUserReadOnlyDataResult.Data["guild_member_list"] != null)
                {
                    let guildManagementData = JSON.parse(getUserReadOnlyDataResult.Data["guild_management_data"].Value);
                    let guildMemberIdData = JSON.parse(getUserReadOnlyDataResult.Data["guild_member_list"].Value);
                    let guildLevelInfo = GetGuildLevelAndExp(getUserReadOnlyDataResult.Data["guild_total_exp"], getUserReadOnlyDataResult.Data["guild_master_exp"],  null);
                    let guildRanking = -1;
        
                    if(guildLevelInfo.error)
                        return toReturn;
        
                    let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
                        PlayFabId: guildManagementData.GuildId,
                        StatisticName: "guild_raid" + serverIdModifier,
                        MaxResultsCount: 1
                    });
    
                    if(getLeaderboardAroundUserResult.Leaderboard[0].StatValue > 0)
                        guildRanking = getLeaderboardAroundUserResult.Leaderboard[0].Position;
        
                    guildItemList.push({
                        "GuildId": guildManagementData.GuildId,
                        "GuildName": guildManagementData.GuildName,
                        "GuildNotice": guildManagementData.GuildNotice,
                        "GuildRanking": guildRanking,
                        "GuildLevel": guildLevelInfo.level,
                        "GuildRequiredStageProgress": guildManagementData.RequiredStageProgress,
                        "GuildFlagIndex": guildManagementData.GuildFlagIndex,
                        "GuildMemberCount": guildMemberIdData.MemberList.length,
                        "IsPrivate": guildManagementData.IsPrivate
                    });

                    maxGuildList = maxGuildList - 1;
                    filteredList = filteredList.filter(n => n.PlayFabId != guildManagementData.GuildId);
                }
            }
        }
    }
    else
    {
        let applyGuildData: any = JSON.parse("{\"ApplyGuildList\":[]}");
        toReturn.applyGuildList = {"ApplyGuildList": applyGuildData.ApplyGuildList};
    }

    for(let i = 0; i < Math.min(maxGuildList, filteredList.length); i ++)
    {
        let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
            PlayFabId: filteredList[i].PlayFabId,
            Keys: ["guild_management_data", "guild_member_list", "guild_total_exp", "guild_master_exp"]
        });
    
        if(getUserReadOnlyDataResult.Data["guild_management_data"] != null && getUserReadOnlyDataResult.Data["guild_member_list"] != null)
        {
            let guildManagementData = JSON.parse(getUserReadOnlyDataResult.Data["guild_management_data"].Value);
            let guildMemberIdData = JSON.parse(getUserReadOnlyDataResult.Data["guild_member_list"].Value);
            let guildLevelInfo = GetGuildLevelAndExp(getUserReadOnlyDataResult.Data["guild_total_exp"], getUserReadOnlyDataResult.Data["guild_master_exp"], null);
            let guildRanking = -1;

            if(guildLevelInfo.error)
                return toReturn;

            let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
                PlayFabId: guildManagementData.GuildId,
                StatisticName: "guild_raid" + serverIdModifier,
                MaxResultsCount: 1
            });
            
            // Adjust recommendation score if the guild is not particiaptes guild battles actively
            if(getLeaderboardAroundUserResult.Leaderboard[0].StatValue == 0)
            {
                server.UpdatePlayerStatistics({
                    PlayFabId: guildManagementData.GuildId,
                    Statistics: [
                        {
                            "StatisticName": "guild_recommend" + serverIdModifier,
                            "Value": 1
                        }
                    ]
                });
            }
            
            if(getLeaderboardAroundUserResult.Leaderboard[0].StatValue > 0)
                guildRanking = getLeaderboardAroundUserResult.Leaderboard[0].Position;

            guildItemList.push({
                "GuildId": guildManagementData.GuildId,
                "GuildName": guildManagementData.GuildName,
                "GuildNotice": guildManagementData.GuildNotice,
                "GuildRanking": guildRanking,
                "GuildLevel": guildLevelInfo.level,
                "GuildRequiredStageProgress": guildManagementData.RequiredStageProgress,
                "GuildFlagIndex": guildManagementData.GuildFlagIndex,
                "GuildMemberCount": guildMemberIdData.MemberList.length,
                "IsPrivate": guildManagementData.IsPrivate
            });
        }   
    }

    if(getMyReadOnlyDataResult.Data["guild_id"] != null)
        toReturn.myGuildId = getMyReadOnlyDataResult.Data["guild_id"].Value;

    toReturn.error = false;
    toReturn.guildList = {"GuildDataList": guildItemList};
    return toReturn;
}
handlers["guildGetRecommendation"] = GuildGetRecommendation;

interface IGuildFreeDonate{
    addedExp: number;
    updatedMemberTotalExp: number;
    guildLevel: number;
    guildExp: number;
    guildNextExp: number;
    dailyLimitDonateLeft: number;
    reasonOfError: string;
    error: boolean;
}

let GuildFreeDonate = function(args: any, context: IPlayFabContext): IGuildFreeDonate{
    
    let toReturn: IGuildFreeDonate = {addedExp: 0, updatedMemberTotalExp: 0, guildLevel: 1, guildExp: 0, guildNextExp: 0, dailyLimitDonateLeft: 0, reasonOfError: "unknown", error: true};

    // Get current guild id
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_id"]
    });

    if(getUserReadOnlyDataResult.Data["guild_id"] != null)
    {
        let guildId = getUserReadOnlyDataResult.Data["guild_id"].Value;

        // Get target guild info
        let getGuildMasterReadOnlyDataResult = server.GetUserReadOnlyData({
            PlayFabId: guildId,
            Keys: ["guild_total_exp", "guild_master_exp", "guild_cache_list", "guild_member_list"]
        });

        if(getGuildMasterReadOnlyDataResult.Data["guild_cache_list"] != null && getGuildMasterReadOnlyDataResult.Data["guild_member_list"] != null)
        {
            // Check over 24 hours after join this guild
            let CheckGuildJoinTimeResult = CheckGuildJoinTime(guildId, getGuildMasterReadOnlyDataResult.Data["guild_member_list"]);

            if(CheckGuildJoinTimeResult.error)
                return toReturn;
            
            if(!CheckGuildJoinTimeResult.isTimeOver)
            {
                toReturn.reasonOfError = "guild_join_time_not_enough";
                return toReturn;
            }

            let guildCacheData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_cache_list"].Value);
            let myCacheData = guildCacheData.MemberCacheDataList.find(n => n.Id == currentPlayerId);
            
            // Check my cache data exist in guildCacheData
            if(myCacheData == null)
                return toReturn;

            // Get guild exp table
            let getTitleDataResult = server.GetTitleData({
                Keys: [
                    "balance_guild_exp"
                ]
            }).Data
    
            // Get current guild level and exp info
            let memberTotalExp = 0;
            if(getGuildMasterReadOnlyDataResult.Data["guild_total_exp"] != null)
                memberTotalExp = parseInt(getGuildMasterReadOnlyDataResult.Data["guild_total_exp"].Value);
            
            let masterExp = 0;
            if(getGuildMasterReadOnlyDataResult.Data["guild_master_exp"] != null)
                masterExp = parseInt(getGuildMasterReadOnlyDataResult.Data["guild_master_exp"].Value);

            let currentGuildLevelInfo = GetGuildLevelAndExp(getGuildMasterReadOnlyDataResult.Data["guild_total_exp"], getGuildMasterReadOnlyDataResult.Data["guild_master_exp"], getTitleDataResult["balance_guild_exp"]);

            if(currentGuildLevelInfo.error)
                return toReturn;
            
            // Check max level
            if(currentGuildLevelInfo.isMaxLevel)
            {
                log.info("Guild level is max level!")
                return toReturn;
            }
            
            let addedExp = 10;

            // Add contribution in my cache data
            if(myCacheData.TotalContribution != null)
                myCacheData.TotalContribution = myCacheData.TotalContribution + addedExp;
            else
                myCacheData.TotalContribution = addedExp;

            // Add exp to guild
            memberTotalExp = memberTotalExp + addedExp;
            
            // For return to client about new guild level info
            getGuildMasterReadOnlyDataResult.Data["guild_total_exp"].Value = memberTotalExp.toString();
            let updatedGuildLevelInfo = GetGuildLevelAndExp(getGuildMasterReadOnlyDataResult.Data["guild_total_exp"], getGuildMasterReadOnlyDataResult.Data["guild_master_exp"], getTitleDataResult["balance_guild_exp"]);
    
            // Use daily limit if possible
            if(!UseDailyLimitIfAvail("guild_free_donate", "0", 1))
            {
                log.info("Exceeded daily limit for free donate!");
                return toReturn;
            }

            // Update guild data
            let updateGuildMasterReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: guildId,
                Data: {
                    "guild_total_exp": memberTotalExp.toString(),
                    "guild_cache_list": unescape(JSON.stringify(guildCacheData))
                }
            });

            // For Log
            toReturn.updatedMemberTotalExp = memberTotalExp;

            toReturn.addedExp = addedExp;
            toReturn.guildLevel = updatedGuildLevelInfo.level;
            toReturn.guildExp = updatedGuildLevelInfo.exp;
            toReturn.guildNextExp = updatedGuildLevelInfo.nextExp;
            toReturn.error = false;
            return toReturn;
        }
        else
            return toReturn;
    }
    else
        return toReturn;
}
handlers["guildFreeDonate"] = GuildFreeDonate;

interface IGuildDonate{
    donateAmount: number;
    addedExp: number;
    updatedMemberTotalExp: number;
    guildLevel: number;
    guildExp: number;
    guildNextExp: number;
    dailyLimitDonateLeft: number;
    reasonOfError: string;
    error: boolean;
}

let GuildDonate = function(args: any, context: IPlayFabContext): IGuildDonate{
    
    let toReturn: IGuildDonate = {donateAmount: 0, addedExp: 0, updatedMemberTotalExp: 0, guildLevel: 1, guildExp: 0, guildNextExp: 0, dailyLimitDonateLeft: 0, reasonOfError: "unknown", error: true};
    let donateAmount = 0;

    if(args.donateAmount)
        donateAmount = args.donateAmount;

    // Check donate validate value
    if(donateAmount < 100 || donateAmount > 1000)
        return toReturn;

    toReturn.donateAmount = donateAmount;

    // Get current guild id
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_id"]
    });

    if(getUserReadOnlyDataResult.Data["guild_id"] != null)
    {
        let guildId = getUserReadOnlyDataResult.Data["guild_id"].Value;

        // Get target guild info
        let getGuildMasterReadOnlyDataResult = server.GetUserReadOnlyData({
            PlayFabId: guildId,
            Keys: ["guild_total_exp", "guild_master_exp", "guild_cache_list", "guild_member_list"]
        });

        if(getGuildMasterReadOnlyDataResult.Data["guild_cache_list"] != null && getGuildMasterReadOnlyDataResult.Data["guild_member_list"] != null)
        {
            // Check over 24 hours after join this guild
            let CheckGuildJoinTimeResult = CheckGuildJoinTime(guildId, getGuildMasterReadOnlyDataResult.Data["guild_member_list"]);

            if(CheckGuildJoinTimeResult.error)
                return toReturn;
            
            if(!CheckGuildJoinTimeResult.isTimeOver)
            {
                toReturn.reasonOfError = "guild_join_time_not_enough";
                return toReturn;
            }

            let guildCacheData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_cache_list"].Value);
            let myCacheData = guildCacheData.MemberCacheDataList.find(n => n.Id == currentPlayerId);

            // Check my cache data exist in guildCacheData
            if(myCacheData == null)
                return toReturn;

            // Get guild exp table
            let getTitleDataResult = server.GetTitleData({
                Keys: [
                    "balance_guild_exp"
                ]
            }).Data
    
            // Get current guild level and exp info
            let memberTotalExp = 0;
            if(getGuildMasterReadOnlyDataResult.Data["guild_total_exp"] != null)
                memberTotalExp = parseInt(getGuildMasterReadOnlyDataResult.Data["guild_total_exp"].Value);
            
            let masterExp = 0;
            if(getGuildMasterReadOnlyDataResult.Data["guild_master_exp"] != null)
                masterExp = parseInt(getGuildMasterReadOnlyDataResult.Data["guild_master_exp"].Value);

            let currentGuildLevelInfo = GetGuildLevelAndExp(getGuildMasterReadOnlyDataResult.Data["guild_total_exp"], getGuildMasterReadOnlyDataResult.Data["guild_master_exp"], getTitleDataResult["balance_guild_exp"]);

            if(currentGuildLevelInfo.error)
                return toReturn;
            
            // Check max level
            if(currentGuildLevelInfo.isMaxLevel)
            {
                log.info("Guild level is max level!")
                return toReturn;
            }

            // Use daily limit if possible
            if(!UseDailyLimitIfAvail("guild_donate", "0", 1))
            {
                log.info("Exceeded daily limit for donate!");
                return toReturn;
            }
            
            // Caculate added exp
            let addedExp = 0;
            
            // Consume gem
            SubtractVirtualCurrency("GE", donateAmount);

            // Add guild token
            //AddVirtualCurrency("GT", Math.floor(donateAmount / 10));

            addedExp = Math.floor(donateAmount / 10);

            // Add contribution in my cache data
            if(myCacheData.TotalContribution != null)
                myCacheData.TotalContribution = myCacheData.TotalContribution + addedExp;
            else
                myCacheData.TotalContribution = addedExp;

            // Add exp to guild
            memberTotalExp = memberTotalExp + addedExp;
            
            // For return to client about new guild level info
            getGuildMasterReadOnlyDataResult.Data["guild_total_exp"].Value = memberTotalExp.toString();
            let updatedGuildLevelInfo = GetGuildLevelAndExp(getGuildMasterReadOnlyDataResult.Data["guild_total_exp"], getGuildMasterReadOnlyDataResult.Data["guild_master_exp"], getTitleDataResult["balance_guild_exp"]);
    
            // Update guild data
            let updateGuildMasterReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: guildId,
                Data: {
                    "guild_total_exp": memberTotalExp.toString(),
                    "guild_cache_list": unescape(JSON.stringify(guildCacheData))
                }
            });

            // For log
            toReturn.updatedMemberTotalExp = memberTotalExp;

            toReturn.addedExp = addedExp;
            toReturn.guildLevel = updatedGuildLevelInfo.level;
            toReturn.guildExp = updatedGuildLevelInfo.exp;
            toReturn.guildNextExp = updatedGuildLevelInfo.nextExp;
            toReturn.error = false;
            return toReturn;
        }
        else
            return toReturn;
    }
    else
        return toReturn;
}
handlers["guildDonate"] = GuildDonate;

interface IGuildGetRank{
    error: boolean;
    guildRankList: Array<PlayFabServerModels.PlayerLeaderboardEntry>;
    guildNameList: Array<string>;
    guildManagementData: any;
    myGuildMasterName: string;
    myGuildRank: number;
    myGuildScore: number;
}

let GuildGetRank = function (args: any, context: IPlayFabContext): IGuildGetRank{
    let toReturn: IGuildGetRank = {error: true, guildRankList: [], guildNameList: [], guildManagementData: [], myGuildMasterName: "", myGuildRank: -1, myGuildScore: 0};
    
    // Get top 10 guild ranking
    let serverIdModifier = GetServerIdModifier(currentPlayerId);
    let getLeaderboardResult = server.GetLeaderboard({
        StatisticName: "guild_raid" + serverIdModifier,
        MaxResultsCount: 10,
        StartPosition: 0
    });

    for(let i = 0; i < getLeaderboardResult.Leaderboard.length; i ++)
    {
        if(getLeaderboardResult.Leaderboard[i].StatValue > 0)
        {
            let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
                PlayFabId: getLeaderboardResult.Leaderboard[i].PlayFabId,
                Keys: ["guild_management_data"]
            });
            
            // Get guild name
            if(getUserReadOnlyDataResult.Data["guild_management_data"] != null)
            {
                let guildManagemnetData = JSON.parse(getUserReadOnlyDataResult.Data["guild_management_data"].Value);
                toReturn.guildNameList.push(guildManagemnetData.GuildName);
                toReturn.guildRankList.push(getLeaderboardResult.Leaderboard[i]);
            }
        }
    }

    // Get my guild ranking info
    let myGuildId = "";
    
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_id"]
    });

    if(getUserReadOnlyDataResult.Data["guild_id"] != null)
        myGuildId = getUserReadOnlyDataResult.Data["guild_id"].Value;

    if(myGuildId != "")
    {
        let getGuildMasterReadOnlyDataResult = server.GetUserReadOnlyData({
            PlayFabId: myGuildId,
            Keys: ["guild_management_data", "guild_cache_list"]
        });

        // Get guild name
        if(getGuildMasterReadOnlyDataResult.Data["guild_management_data"] != null && getGuildMasterReadOnlyDataResult.Data["guild_cache_list"] != null)
        {
            let guildManagemnetData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_management_data"].Value);
            toReturn.guildManagementData = guildManagemnetData;

            // Get my guild's rank
            let getLeaderboardResult = server.GetLeaderboardAroundUser({
                PlayFabId: myGuildId,
                StatisticName: "guild_raid" + serverIdModifier,
                MaxResultsCount: 1
            });

            if(getLeaderboardResult.Leaderboard.length > 0)
            {
                toReturn.myGuildMasterName = getLeaderboardResult.Leaderboard[0].DisplayName;
                toReturn.myGuildScore = getLeaderboardResult.Leaderboard[0].StatValue;

                if(toReturn.myGuildScore > 0)
                    toReturn.myGuildRank = getLeaderboardResult.Leaderboard[0].Position;
            }
            else
            {
                // Find guild master name
                let guildCacheData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_cache_list"].Value);
                let guildMasterCacheData = guildCacheData.MemberCacheDataList.find(n => n.Id == myGuildId);

                if(guildMasterCacheData != null)
                    toReturn.myGuildMasterName = guildMasterCacheData.Name;
            }
            
            toReturn.error = false;
        }
    }
    else
    {
        toReturn.guildManagementData = null;
        toReturn.error = false;
    }

    return toReturn;
}
handlers["guildGetRank"] = GuildGetRank;

interface IGuildUpgradeRelic{
    targetGuildRelicId: string;
    upgradeCount: number;
    guildRelicList: {};
    addedBonusStat: number;
    requiredGuildToken: number;
    reasonOfError: string;
    error: boolean;
}

let GuildUpgradeRelic = function(args: any, context: IPlayFabContext): IGuildUpgradeRelic{
    
    let toReturn: IGuildUpgradeRelic = {targetGuildRelicId: "", upgradeCount: 0, guildRelicList: {}, addedBonusStat: 0, requiredGuildToken: 0, reasonOfError: "unknown", error: true};
    toReturn.upgradeCount = 1;

    if(args.targetGuildRelicId)
        toReturn.targetGuildRelicId = args.targetGuildRelicId;
    if(args.upgradeCount)
        toReturn.upgradeCount = args.upgradeCount;

    // Get guild relic balance info
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "balance_inventory_growth"
        ]
    }).Data;

    let balanceInventoryData = JSON.parse(getTitleDataResult["balance_inventory_growth"]);

    if(balanceInventoryData != null && balanceInventoryData.GuildRelicInfo != null)
    {
        let targetGuildRelicBalanceInfo = balanceInventoryData.GuildRelicInfo.find(n => n.Id == toReturn.targetGuildRelicId);
        
        if(targetGuildRelicBalanceInfo != null)
        {
            toReturn.requiredGuildToken = targetGuildRelicBalanceInfo.Price * toReturn.upgradeCount;

            // Get inventory data
            let getUserInventoryResult = server.GetUserInventory({
                PlayFabId: currentPlayerId
            });
    
            let currentGuildTokenCount = 0;
    
            if(getUserInventoryResult.VirtualCurrency["GT"] != null)
                currentGuildTokenCount = getUserInventoryResult.VirtualCurrency["GT"];
            
            // Check guild token amount
            if(currentGuildTokenCount < toReturn.requiredGuildToken)
            {
                log.info("Not enough guild token!");
                return toReturn;
            }

            // Get user guild relic data
            let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Keys: ["guild_relic_data"]
            });

            let guildRelicData: any = JSON.parse("{\"GuildRelicList\":[]}");
            
            if(getUserReadOnlyDataResult.Data["guild_relic_data"] != null)
                guildRelicData = JSON.parse(getUserReadOnlyDataResult.Data["guild_relic_data"].Value);

            let targetGuildRelicInfo = guildRelicData.GuildRelicList.find(n => n.Id == toReturn.targetGuildRelicId);

            if(targetGuildRelicInfo != null)
            {
                // Upgrade logic
                // Check max level
                if(targetGuildRelicInfo.Level == targetGuildRelicBalanceInfo.MaxLevel)
                {
                    log.info("target guild relic level is max level!");
                    return toReturn;
                }
                else if((targetGuildRelicInfo.Level + toReturn.upgradeCount) > targetGuildRelicBalanceInfo.MaxLevel)
                {
                    log.info("Upgraded level can't over max level!");
                    return toReturn;
                }

                targetGuildRelicInfo.Level = targetGuildRelicInfo.Level + toReturn.upgradeCount;
                
                // if guild relic's level is max level after upgarde then set max level.
                if(targetGuildRelicInfo.Level >= targetGuildRelicBalanceInfo.MaxLevel)
                    targetGuildRelicInfo.IsMaxLevel = true;
                else
                    targetGuildRelicInfo.IsMaxLevel = false;
            }
            else
            {
                // Unlock logic
                guildRelicData.GuildRelicList.push({"Id": targetGuildRelicBalanceInfo.Id, "IsMaxLevel": false, "Level": toReturn.upgradeCount, 
                "RelicType": targetGuildRelicBalanceInfo.RelicType});
            }

            // Update guild relic data
            let updateGuildMasterReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "guild_relic_data": unescape(JSON.stringify(guildRelicData))
                }
            });

            if(targetGuildRelicBalanceInfo.Id == "guild_relic_03")
            {
                AddVirtualCurrency("BS", toReturn.upgradeCount);
                toReturn.addedBonusStat = toReturn.upgradeCount;
            }
            
            SubtractVirtualCurrency("GT", toReturn.requiredGuildToken);

            toReturn.guildRelicList = {"GuildRelicList": guildRelicData.GuildRelicList};
            toReturn.error = false;
            return toReturn;
        }
        else
        {
            log.info("targetGuildRelicBalanceInfo is null!");
            return toReturn;
        }
    }
    else
    {
        log.info("balanceInventoryData or balanceInventoryData.GuildRelicInfo is null!");
        return toReturn;
    }
}
handlers["guildUpgradeRelic"] = GuildUpgradeRelic;

///////////////////////// HELPER FUNCTIONS (NOT DIRECTLY CALLABLE FROM THE CLIENT) ///////////////////////

function IsGuildMaster(guildManagementDataJson: PlayFabServerModels.UserDataRecord): boolean
{
    let isGuildMaster = false;

    // Check user is guild master or not
    if(guildManagementDataJson != null)
    {
        let guildManagementData = JSON.parse(guildManagementDataJson.Value);

        if(guildManagementData.GuildId == currentPlayerId)
            isGuildMaster = true;
    }

    return isGuildMaster;
} 

function GetStageProgress(userDataJson: PlayFabServerModels.UserDataRecord): number
{
    let stageProgress = 1;

    if(userDataJson != null)
    {
        let userData = JSON.parse(userDataJson.Value);
        stageProgress = userData.StageProgress;
    }

    return stageProgress;
}

function GetGuildMemberLimitCount(guildLevel: number): number{

    // Default value is 8
    let guildMemberLimit = 8;

    // guild member limit depend on guild level (max 15)
    if(guildLevel >= 15)
        guildMemberLimit = 15;
    else if(guildLevel >= 14)
        guildMemberLimit = 14;
    else if(guildLevel >= 13)
        guildMemberLimit = 13;
    else if(guildLevel >= 12)
        guildMemberLimit = 12;
    else if(guildLevel >= 9)
        guildMemberLimit = 11;
    else if(guildLevel >= 6)
        guildMemberLimit = 10;
    else if(guildLevel >= 3)
        guildMemberLimit = 9;
    
    return guildMemberLimit;
}

interface IGetGuildLevelAndExp{
   level: number;
   exp: number;
   nextExp: number;
   isMaxLevel: boolean;
   error: boolean;
}

function GetGuildLevelAndExp(totalExpData: PlayFabServerModels.UserDataRecord, masterExpData: PlayFabServerModels.UserDataRecord, guildExpBalanceSheet: any): IGetGuildLevelAndExp
{
    let toReturn: IGetGuildLevelAndExp = {level: 0, exp: 0, nextExp: 0, isMaxLevel: false, error: true};

    if(totalExpData == null)
        return toReturn;

    // If guildExpBalanceSheet is null then try to get guild exp balance sheet
    if(guildExpBalanceSheet == null)
    {
        let getTitleDataResult = server.GetTitleData({
            Keys: [
                "balance_guild_exp"
            ]
        }).Data;
        
        if(getTitleDataResult["balance_guild_exp"] == null)
            return toReturn;
        
        guildExpBalanceSheet = getTitleDataResult["balance_guild_exp"];
    }

    let guildMasterExp = 0;
    if(masterExpData != null)
        guildMasterExp = parseInt(masterExpData.Value);
    
    let currentExp = parseInt(totalExpData.Value) + guildMasterExp;
    let guildExpBalanceData =  JSON.parse(guildExpBalanceSheet);

    // Calculate current level
    for(let i = 0; i < guildExpBalanceData.GuildExpTable.length ; i++)
    {
        if(currentExp < guildExpBalanceData.GuildExpTable[i].TotalExp)
        {
            toReturn.level = guildExpBalanceData.GuildExpTable[i - 1].Level;

            if(toReturn.level == 1)
            {
                toReturn.exp = currentExp;
                toReturn.nextExp = guildExpBalanceData.GuildExpTable[i].TotalExp;
            }
            else
            {
                toReturn.exp = currentExp - guildExpBalanceData.GuildExpTable[i - 1].TotalExp;
                toReturn.nextExp = guildExpBalanceData.GuildExpTable[i].TotalExp - guildExpBalanceData.GuildExpTable[i - 1].TotalExp;
            }
            
            toReturn.error = false;
            break;
        }
        else
        {
            // Check max level
            if(guildExpBalanceData.GuildExpTable[i].Level >= guildExpBalanceData.MaxGuildLevel)
            {
                toReturn.level = guildExpBalanceData.MaxGuildLevel;
                toReturn.exp = 0;
                toReturn.nextExp = 0;
                toReturn.isMaxLevel = true;
                toReturn.error = false;
                break;
            }
        }
    }

    return toReturn;
}

function UpdateGuildRecommendationScore(serverIdModifier: string, guildId: string, timestamp: number, guildMemberCount: number){
    
    let guildTimeStampDay = Math.floor(timestamp / 86400);
    let currentTimestampDay = Math.floor(+ new Date() / (1000 * 60 * 60 * 24));
    let score = Math.max(30 - Math.min(15, currentTimestampDay - guildTimeStampDay) - guildMemberCount, 1);

    server.UpdatePlayerStatistics({
        PlayFabId: guildId,
        Statistics: [
            {
                "StatisticName": "guild_recommend" + serverIdModifier,
                "Value": score
            }
        ]
    });
}

function ShuffleArray(array: any[]) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}

interface ICheckGuildJoinTime{
    isTimeOver: boolean;
    error: boolean;
 }

function CheckGuildJoinTime(guildId: string, guildMemberDataJson: PlayFabServerModels.UserDataRecord): ICheckGuildJoinTime
{
    let toReturn: ICheckGuildJoinTime = {isTimeOver: false, error: true};

    if(guildMemberDataJson == null)
    {
        // Get target guild info
        let getGuildMasterReadOnlyDataResult = server.GetUserReadOnlyData({
            PlayFabId: guildId,
            Keys: ["guild_member_list"]
        });
        
        if(getGuildMasterReadOnlyDataResult.Data["guild_member_list"] == null)
            return toReturn;
        
        guildMemberDataJson = getGuildMasterReadOnlyDataResult.Data["guild_member_list"];
    }

    let guildMemberData = JSON.parse(guildMemberDataJson.Value);
    let myMemberData = guildMemberData.MemberList.find(n => n.Id == currentPlayerId);
    
    // Check my member data exist in guildMemberList
    if(myMemberData == null)
        return toReturn;

    // Check over 24 hours after join this guild
    if(myMemberData.JoinTimeStamp != null)
    {
        let currentTimestamp = Math.floor(+ new Date() / 1000);

        if(currentTimestamp > (myMemberData.JoinTimeStamp + 86400))
            toReturn.isTimeOver = true;
        else
            log.info("This User is not over 24 hours after join this guild !");
    }
    else
        toReturn.isTimeOver = true;

    toReturn.error = false;
    return toReturn
}