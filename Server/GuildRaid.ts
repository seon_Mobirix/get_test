///////////////////////////////////////////////////////////////////////////////////////////////////////
// Dungeon Knight
///////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////// Cloud Script Handler Functions /////////////////////////

interface IGuildRaidRefresh{
    error: boolean;
    dailyLimitLeft: number;
    currentSeason: number;
    position: number;
    score: number;
    lastSeasonPosition: number;
    lastSeasonScore: number;
    lastSeasonRewardGem: number;
    lastSeasonRewardExp: number;
    lastSeasonRewardGuildToken: number;
    guildRankList: any[];
    minAndroidVersion: string;
    miniOSVersion: string;
}

let GuildRaidRefresh = function (args: any, context: IPlayFabContext): IGuildRaidRefresh{
    let toReturn: IGuildRaidRefresh = {error: true, currentSeason: 0, dailyLimitLeft: 0, position: -1, score:0, 
        lastSeasonPosition: -1, lastSeasonScore: 0, lastSeasonRewardGem: 0, lastSeasonRewardExp:0, lastSeasonRewardGuildToken: 0, guildRankList: [], minAndroidVersion: "0.0.0", miniOSVersion: "0.0.0"};

    // Get title data
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "setting_json"        
        ]
    }).Data;

    if(getTitleDataResult["setting_json"] != null)
    {
        let convertedData = JSON.parse(getTitleDataResult["setting_json"]); 
        
        toReturn.minAndroidVersion = convertedData.MinAndroidVersion;
        toReturn.miniOSVersion = convertedData.MiniOSVersion;
    }

    // Get left daily limit
    toReturn.dailyLimitLeft = GetLeftDailyLimit("guild_raid", "0");

    // Get guild master id
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_id"]
    });

    if(getUserReadOnlyDataResult.Data["guild_id"] != null)
    {
        let serverIdModifier = GetServerIdModifier(currentPlayerId);
        
        // Get guild's leaderboard
        let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
            PlayFabId: getUserReadOnlyDataResult.Data["guild_id"].Value,
            StatisticName: "guild_raid" + serverIdModifier,
            MaxResultsCount: 1
        });
        let currentLeaderboardEntry = getLeaderboardAroundUserResult.Leaderboard[0];
        if(currentLeaderboardEntry != null)
        {
            toReturn.currentSeason = getLeaderboardAroundUserResult.Version;
            toReturn.position = currentLeaderboardEntry.Position;
            toReturn.score = currentLeaderboardEntry.StatValue;
            if(toReturn.score == 0)
                toReturn.score = 0;
        }

        // Recent played season
        let getUserInternalDataResult = server.GetUserInternalData({
            PlayFabId: currentPlayerId,
            Keys: [
                "guild_raid_reward_last_played_season"
            ]
        });

        // Receive reward if a new season started
        if(getUserInternalDataResult.Data["guild_raid_reward_last_played_season"] != null)
        {
            let lastSeason = parseInt(getUserInternalDataResult.Data["guild_raid_reward_last_played_season"].Value);
            if(getLeaderboardAroundUserResult.Version == lastSeason + 1)
            {
                let rewardResult = ReceiveGuildRaidReward(serverIdModifier, getUserInternalDataResult, getLeaderboardAroundUserResult.Version, getUserReadOnlyDataResult.Data["guild_id"].Value);
                toReturn.lastSeasonPosition = rewardResult.lastSeasonPosition;
                toReturn.lastSeasonScore = rewardResult.lastSeasonScore;
                toReturn.lastSeasonRewardGem = rewardResult.lastSeasonRewardGem;
                toReturn.lastSeasonRewardExp = rewardResult.lastSeasonRewardExp;
                toReturn.lastSeasonRewardGuildToken = rewardResult.lastSeasonRewardGuildToken;
                
                log.info("Received guild reward gems: " + rewardResult.lastSeasonRewardGem);
            }
        }
        
        // List of guild members' scores
        let getGuildMasterReadOnlyDataResult = server.GetUserReadOnlyData({
            PlayFabId: getUserReadOnlyDataResult.Data["guild_id"].Value,
            Keys: ["guild_cache_list", "guild_member_list"]
        });
        
        if(getGuildMasterReadOnlyDataResult.Data["guild_cache_list"] != null && getGuildMasterReadOnlyDataResult.Data["guild_member_list"] != null)
        {
            let guildCacheData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_cache_list"].Value);
            let guildMemberData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_member_list"].Value);
            
            let myData = guildMemberData.MemberList.find(n => n.Id == currentPlayerId);            
            if(myData != null) // Check whether this player is a member of this guild
            {
                for(let i = 0; i < guildCacheData.MemberCacheDataList.length; i++)
                {
                    if(toReturn.currentSeason == guildCacheData.MemberCacheDataList[i].RaidSeason)
                        toReturn.guildRankList.push({"name": guildCacheData.MemberCacheDataList[i].Name, "score": guildCacheData.MemberCacheDataList[i].RaidScore});
                    else
                        toReturn.guildRankList.push({"name": guildCacheData.MemberCacheDataList[i].Name, "score": 0});
                }
            }
            else
            {
                log.info("user not exist in member list");
                let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                    PlayFabId: currentPlayerId,
                    Data: {
                        "guild_id": null,
                        "guild_raid_reward_last_played_season": null
                    }
                });
                toReturn.error = true;
                return toReturn;
            }
        }
        else
        {
            log.info("The guild master has data issues (Likely the guild ownership has been changed)");
            toReturn.error = true;
            return toReturn;
        }
    }
    else
    {
        log.info("User don't have any guild!");
        toReturn.error = true;
        return toReturn;
    }

    toReturn.error = false;
    return toReturn;
}

handlers["guildRaidRefresh"] = GuildRaidRefresh;

interface IGuildRaidOpen{
    error: boolean;
    raidIndex: number;
    reasonOfError: string;
}

let GuildRaidOpen = function (args: any, context: IPlayFabContext): IGuildRaidOpen{
    let toReturn: IGuildRaidOpen = {error: true, raidIndex: 0, reasonOfError: "unknown"};

    if(GetLeftDailyLimit("guild_raid", "0") <= 0)
        return toReturn;

    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_id"]
    });

    // Check user already have a guild or not
    if(getUserReadOnlyDataResult.Data["guild_id"] != null)
    {
        let CheckGuildJoinTimeResult = CheckGuildJoinTime(getUserReadOnlyDataResult.Data["guild_id"].Value, null);

        if(CheckGuildJoinTimeResult.error)
            return toReturn;
        
        if(!CheckGuildJoinTimeResult.isTimeOver)
        {
            toReturn.reasonOfError = "guild_join_time_not_enough";
            return toReturn;
        }

        let getUserInternalDataResult = server.GetUserInternalData({
            PlayFabId: currentPlayerId,
            Keys: [
                "guild_raid_index"
            ]
        }).Data;
    
        if(getUserInternalDataResult["guild_raid_index"] != null)
           toReturn.raidIndex = parseInt(getUserInternalDataResult["guild_raid_index"].Value) + 1;
    
        let updateuserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: {
                "is_open_guild_raid": "true",
                "is_playing_guild_raid": "false",
                "guild_raid_index": toReturn.raidIndex.toString()
            }
        });
    
        toReturn.error = false;
    }
    
    return toReturn;
}

handlers["guildRaidOpen"] = GuildRaidOpen;

interface IGuildRaidStart{
    error: boolean;
    reasonOfError: string;
}

let GuildRaidStart = function (args: any, context: IPlayFabContext): IGuildRaidStart{
    let toReturn: IGuildRaidStart = {error: true, reasonOfError: ""};

    let updateuserInternalDataResult = server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: {
            "is_open_guild_raid": "true",
            "is_playing_guild_raid": "true"
        }
    });

    toReturn.error = false;

    return toReturn;
}

handlers["guildRaidStart"] = GuildRaidStart;

interface IGuildRaidEnd{
    error: boolean;
    reasonOfError: string;
}

let GuildRaidEnd = function (args: any, context: IPlayFabContext): IGuildRaidEnd{
    let toReturn: IGuildRaidEnd = {error: true, reasonOfError: ""};

    let season = 0;
    if(args.season)
        season = args.season;

    let score = 0;
    if(args.score)
        score = args.score;

    let participantList = [];
    if(args.participantList)
        participantList = args.participantList;

    let participantScoreList = [];
    if(args.participantScoreList)
        participantScoreList = args.participantScoreList;
    
    let checkTimeStampForChanges = false;
    if(args.checkTimeStampForChanges)
        checkTimeStampForChanges = args.checkTimeStampForChanges;

    // Get title data
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "setting_json"        
        ]
    }).Data;

    let willGetReward = args.willGetReward;

    if(willGetReward)
    {
        let updateuserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: {
                "is_open_guild_raid": "false",
                "is_playing_guild_raid": "false",
                "guild_raid_reward_last_played_season": season.toString()
            }
        });

        let serverIdModifier = GetServerIdModifier(currentPlayerId);

        // Get guild master id
        let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Keys: ["guild_id", "last_guild_leave_timestamp"]
        });

        if(getUserReadOnlyDataResult.Data["guild_id"] != null)
        {
            let timestampDay = Math.floor(+ new Date() / (1000 * 60 * 60 * 24));

            // Update guild master's score
            server.UpdatePlayerStatistics({
                PlayFabId: getUserReadOnlyDataResult.Data["guild_id"].Value,
                Statistics: [
                    {
                        "StatisticName": "guild_raid" + serverIdModifier,
                        "Value": participantScoreList[0]
                    }
                ]
            });

            // Get guild data
            let getGuildMasterReadOnlyDataResult = server.GetUserReadOnlyData({
                PlayFabId: getUserReadOnlyDataResult.Data["guild_id"].Value,
                Keys: ["guild_member_list", "guild_cache_list"]
            });
            
            if(getGuildMasterReadOnlyDataResult.Data["guild_member_list"] != null && getGuildMasterReadOnlyDataResult.Data["guild_cache_list"] != null)
            {
                let guildMemberData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_member_list"].Value);
                let guildCacheData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_cache_list"].Value);
                
                // Check for the case where guildCacheData is not matching with guild member list
                for(let i = 0; i < guildMemberData.MemberList.length; i ++)
                {
                    if(guildCacheData.MemberCacheDataList.find(n => n.Id == guildMemberData.MemberList[i].Id) == null)
                    {
                        guildCacheData.MemberCacheDataList.push(
                            {
                                "Id": guildMemberData.MemberList[i].Id, 
                                "Name": "???", 
                                "Level": -1, 
                                "LastTimeStamp": -1,
                                "TotalContribution": 0,
                                "RaidScore": 0,
                                "RaidSeason": -1
                            }
                        );
                    }
                }

                // Only leave member data cache where member list exists
                guildCacheData.MemberCacheDataList = guildCacheData.MemberCacheDataList.filter(n => guildMemberData.MemberList.find(k => k.Id == n.Id) != null);
                
                // Update cache value of participants
                for(let i = 0; i < 1; i ++)
                {
                    let target = guildCacheData.MemberCacheDataList.find(n => n.Id == participantList[i]);
                    if(target != null)
                    {
                        if(target.RaidSeason != season)
                            target.RaidScore = participantScoreList[i];
                        else
                            target.RaidScore += participantScoreList[i];
                            
                        target.RaidSeason = season;
                        
                        target.LastTimeStamp = Math.floor(+ new Date() / (1000 * 60 * 60));
                        
                        // TODO: remove this
                        log.info(participantList[i] + ": " + participantScoreList[i]);
                    }
                }
                
                // Update guild cache
                let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                    PlayFabId: getUserReadOnlyDataResult.Data["guild_id"].Value,
                    Data: {
                        "guild_cache_list": unescape(JSON.stringify(guildCacheData))
                    }
                });
            }
        }

        if(!UseDailyLimitIfAvail("guild_raid", "0", 1))
            return toReturn;
    }
    else
    {
        let updateuserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: {
                "is_open_guild_raid": "false",
                "is_playing_guild_raid": "false"
            }
        });
    }

    let skillUseCount = 0;
    if(args.skillUseCount)
        skillUseCount = args.skillUseCount;

    log.info("Skill Use Count: " + skillUseCount);

    toReturn.error = false;

    return toReturn;
}

handlers["guildRaidEnd"] = GuildRaidEnd;

interface IGuildRaidLeave{
    error: boolean;
    reasonOfError: string;
}

let GuildRaidLeave = function (args: any, context: IPlayFabContext): IGuildRaidLeave{
    let toReturn: IGuildRaidLeave = {error: true, reasonOfError: ""};

    let season = 0;
    if(args.season)
        season = args.season;

    let personalScore = 0;
    if(args.personalScore)
        personalScore = args.personalScore;

    let willGetReward = args.willGetReward;

    if(willGetReward)
    {
        let updateUserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: {
                "guild_raid_reward_last_played_season": season.toString()
            }
        });
    }

    let skillUseCount = 0;
    if(args.skillUseCount)
        skillUseCount = args.skillUseCount;

    log.info("Skill Use Count: " + skillUseCount);

    toReturn.error = false;

    return toReturn;
}

handlers["guildRaidLeave"] = GuildRaidLeave;

interface IGuildRaidJoin{
    error: boolean;
    reasonOfError: string;
}

let GuildRaidJoin = function (args: any, context: IPlayFabContext): IGuildRaidJoin{
    let toReturn: IGuildRaidJoin = {error: true, reasonOfError: ""};

    let hostPlayerId = "";
    let hostGuildRaidIndex = 0;

    if(args.hostPlayerId)
        hostPlayerId = args.hostPlayerId;

    if(args.hostGuildRaidIndex)
        hostGuildRaidIndex = args.hostGuildRaidIndex;

    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Keys: ["guild_id"]
        });
    
    // Check user already have a guild or not
    if(getUserReadOnlyDataResult.Data["guild_id"] != null)
    {
        let CheckGuildJoinTimeResult = CheckGuildJoinTime(getUserReadOnlyDataResult.Data["guild_id"].Value, null);

        if(CheckGuildJoinTimeResult.error)
            return toReturn;
        
        if(!CheckGuildJoinTimeResult.isTimeOver)
        {
            toReturn.reasonOfError = "guild_join_time_not_enough";
            return toReturn;
        }

        let getHostInternalDataResult = server.GetUserInternalData({
            PlayFabId: hostPlayerId,
            Keys: [
                "is_open_guild_raid",
                "is_playing_guild_raid",
                "guild_raid_index"
            ]
        }).Data;
    
        if(getHostInternalDataResult["is_playing_guild_raid"] != null && getHostInternalDataResult["is_playing_guild_raid"].Value == "true")
        {
            toReturn.reasonOfError = "raid_already_playing";
            return toReturn;
        }
        else if(getHostInternalDataResult["is_open_guild_raid"] == null || getHostInternalDataResult["is_open_guild_raid"].Value != "true")
        {
            toReturn.reasonOfError = "raid_not_open";
            return toReturn;
        }
        else if(getHostInternalDataResult["guild_raid_index"] == null || parseInt(getHostInternalDataResult["guild_raid_index"].Value) != hostGuildRaidIndex)
        {
            toReturn.reasonOfError = "raid_index_mismatch";
            return toReturn;
        }
    
        toReturn.error = false;
    }

    return toReturn;
}

handlers["guildRaidJoin"] = GuildRaidJoin;

interface IGuildRaidFix{
    error: boolean;
    reasonOfError: string;
}

let GuildRaidFix = function (args: any, context: IPlayFabContext): IGuildRaidFix{
    let toReturn: IGuildRaidFix = {error: true, reasonOfError: ""};

    let serverIdModifier = GetServerIdModifier(currentPlayerId);

    // Get old total score
    let currentStatValue = server.GetPlayerStatistics({
        PlayFabId: currentPlayerId,
        StatisticNames: ["guild_raid" + serverIdModifier]
    }).Statistics;

    let oldMasterScore = 0;
    if(currentStatValue[0] != null)
        oldMasterScore = currentStatValue[0].Value;

    log.info("old score: " + oldMasterScore);

    // Update guild master's score
    server.UpdatePlayerStatistics({
        PlayFabId: currentPlayerId,
        Statistics: [
            {
                "StatisticName": "guild_raid" + serverIdModifier,
                "Value": oldMasterScore * -1
            }
        ]
    });

    // Get guild data
    let getGuildMasterReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["guild_member_list", "guild_cache_list"]
    });
            
    if(getGuildMasterReadOnlyDataResult.Data["guild_member_list"] != null && getGuildMasterReadOnlyDataResult.Data["guild_cache_list"] != null)
    {
        let guildMemberData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_member_list"].Value);
        let guildCacheData = JSON.parse(getGuildMasterReadOnlyDataResult.Data["guild_cache_list"].Value);
        
        // Check for the case where guildCacheData is not matching with guild member list
        for(let i = 0; i < guildMemberData.MemberList.length; i ++)
        {
            if(guildCacheData.MemberCacheDataList.find(n => n.Id == guildMemberData.MemberList[i].Id) == null)
            {
                guildCacheData.MemberCacheDataList.push(
                    {
                        "Id": guildMemberData.MemberList[i].Id, 
                        "Name": "???", 
                        "Level": -1, 
                        "LastTimeStamp": -1,
                        "TotalContribution": 0,
                        "RaidScore": 0,
                        "RaidSeason": -1
                    }
                );
            }
        }

        // Only leave member data cache where member list exists
        guildCacheData.MemberCacheDataList = guildCacheData.MemberCacheDataList.filter(n => guildMemberData.MemberList.find(k => k.Id == n.Id) != null);
        
        for(let i = 0; i < guildCacheData.MemberCacheDataList.length; i ++)
        {
            let target = guildCacheData.MemberCacheDataList[i];
            if(target != null)
            {
                target.RaidScore = 0;
                /*
                let dataToUse = {};;
                dataToUse["daily_limit_use_guild_raid"] = "{}";
                
                let updateUserInternalDataResult = server.UpdateUserInternalData({
                    PlayFabId: target.Id,
                    Data: dataToUse
                });
                */
            }
        }
        
        // Update guild cache
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "guild_cache_list": unescape(JSON.stringify(guildCacheData))
            }
        });
    }

    toReturn.error = false;

    return toReturn;
}

handlers["guildRaidFix"] = GuildRaidFix;

interface IGuildRaidDailyLimitReset{
    error: boolean;
}

let GuildRaidDailyLimitReset = function (args: any, context: IPlayFabContext): IGuildRaidDailyLimitReset{
    let toReturn: IGuildRaidDailyLimitReset = {error: true};
    
    let dataToUse = {};
    dataToUse["daily_limit_use_guild_raid"] = "{}";
    
    let updateUserInternalDataResult = server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: dataToUse
    });

    toReturn.error = false;
    return toReturn;
}

handlers["guildRaidDailyLimitReset"] = GuildRaidDailyLimitReset;

///////////////////////// HELPER FUNCTIONS (NOT DIRECTLY CALLABLE FROM THE CLIENT) /////////////////////////

interface IReceiveGuildRaidReward{
    lastSeasonPosition: number;
    lastSeasonScore: number;
    lastSeasonRewardGem: number;
    lastSeasonRewardExp: number;
    lastSeasonRewardGuildToken: number;
}

function ReceiveGuildRaidReward(serverIdModifier: string, internalDataResult: PlayFabServerModels.GetUserDataResult, currentSeason: number, guildMasterId: string): IReceiveGuildRaidReward{
    
    let toReturn: IReceiveGuildRaidReward = {lastSeasonPosition: 0, lastSeasonScore: 0, lastSeasonRewardGem: 0, lastSeasonRewardExp: 0, lastSeasonRewardGuildToken: 0};

    if(internalDataResult.Data["guild_raid_reward_last_played_season"] != null)
    {
        // Receive reward if current season is higher than last received season
        let lastPlayedSeason = parseInt(internalDataResult.Data["guild_raid_reward_last_played_season"].Value);
        if(currentSeason == lastPlayedSeason + 1)
        {
            let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
                PlayFabId: guildMasterId,
                StatisticName: "guild_raid" + serverIdModifier,
                MaxResultsCount: 1,
                Version: lastPlayedSeason
            });

            if(getLeaderboardAroundUserResult.Leaderboard[0] != null) // Only give reward when play record exists
            {
                let lastSeasonLeaderboardEntry = getLeaderboardAroundUserResult.Leaderboard[0];
                toReturn.lastSeasonPosition = lastSeasonLeaderboardEntry.Position;
                toReturn.lastSeasonScore = lastSeasonLeaderboardEntry.StatValue;
            }

            if(toReturn.lastSeasonScore != 0)
            {
                // Give reward based on last season's performance
                if(toReturn.lastSeasonPosition == 0)
                {
                    toReturn.lastSeasonRewardGem = 2500;
                }
                else if(toReturn.lastSeasonPosition == 1)
                {
                    toReturn.lastSeasonRewardGem = 2000;
                }
                else if(toReturn.lastSeasonPosition == 2)
                {
                    toReturn.lastSeasonRewardGem = 1500;
                }
                else if(toReturn.lastSeasonPosition < 10)
                {
                    toReturn.lastSeasonRewardGem = 1000;
                }
                else if(toReturn.lastSeasonPosition < 50)
                {
                    toReturn.lastSeasonRewardGem = 750;
                }
                else if(toReturn.lastSeasonPosition < 100)
                {
                    toReturn.lastSeasonRewardGem = 250;
                }
                else
                {
                    toReturn.lastSeasonRewardGem = 50;
                }

                AddVirtualCurrency("GE", toReturn.lastSeasonRewardGem);
            }
            
            if(currentPlayerId == guildMasterId && toReturn.lastSeasonScore != 0)
            {
                // Give reward based on last season's performance
                if(toReturn.lastSeasonPosition == 0)
                {
                    toReturn.lastSeasonRewardExp = 2500;
                }
                else if(toReturn.lastSeasonPosition == 1)
                {
                    toReturn.lastSeasonRewardExp = 2000;
                }
                else if(toReturn.lastSeasonPosition == 2)
                {
                    toReturn.lastSeasonRewardExp = 1500;
                }
                else if(toReturn.lastSeasonPosition < 10)
                {
                    toReturn.lastSeasonRewardExp = 1000;
                }
                else
                {
                    toReturn.lastSeasonRewardExp = 500;
                }
                
                let getGuildMasterReadOnlyDataResult = server.GetUserReadOnlyData({
                    PlayFabId: currentPlayerId,
                    Keys: ["guild_master_exp"]
                });

                let masterExp = 0;
                if(getGuildMasterReadOnlyDataResult.Data["guild_master_exp"] != null)
                    masterExp = parseInt(getGuildMasterReadOnlyDataResult.Data["guild_master_exp"].Value);

                let updateGuildMasterReadOnlyDataResult = server.UpdateUserReadOnlyData({
                    PlayFabId: currentPlayerId,
                    Data: {
                        "guild_master_exp": (masterExp + toReturn.lastSeasonRewardExp).toString()
                    }
                });
            }

            let timestampDay = Math.floor(+ new Date() / (1000 * 60 * 60 * 24));
            if(timestampDay >= 18813)
            {
                if(toReturn.lastSeasonScore != 0)
                {
                    // Give reward based on last season's performance
                    if(toReturn.lastSeasonPosition == 0)
                    {
                        toReturn.lastSeasonRewardGuildToken = 2500;
                    }
                    else if(toReturn.lastSeasonPosition == 1)
                    {
                        toReturn.lastSeasonRewardGuildToken = 2000;
                    }
                    else if(toReturn.lastSeasonPosition == 2)
                    {
                        toReturn.lastSeasonRewardGuildToken = 1500;
                    }
                    else if(toReturn.lastSeasonPosition < 10)
                    {
                        toReturn.lastSeasonRewardGuildToken = 1000;
                    }
                    else
                    {
                        toReturn.lastSeasonRewardGuildToken = 500;
                    }

                    AddVirtualCurrency("GT", toReturn.lastSeasonRewardGuildToken);
                }
            }

            let updateUserInternalDataResult = server.UpdateUserInternalData({
                PlayFabId: currentPlayerId,
                Data: {
                    "guild_raid_reward_last_played_season": currentSeason.toString()
                }
            });
        }
    }
    else
    {
        let updateUserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: {
                "guild_raid_reward_last_played_season": currentSeason.toString()
            }
        });
    }

    return toReturn;
}