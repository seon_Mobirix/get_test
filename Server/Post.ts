///////////////////////////////////////////////////////////////////////////////////////////////////////
// Dungeon Knight
///////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////// Cloud Script Handler Functions /////////////////////////
interface IRequestAttachedItem{
    itemId: string;
    count: number;
    error: boolean;
}

let RequestAttachedItem = function(args: any, context: IPlayFabContext): IRequestAttachedItem{
    let mailId = "";
    let toReturn: IRequestAttachedItem = {itemId: "", count: 0, error: true};

    if(args.mailId)
        mailId = args.mailId;

    log.info("Mail Id: " + mailId);

    // Get mail list
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["inbox"]
    });

    if(getUserReadOnlyDataResult.Data["inbox"] != null)
    {
        let inbox = DeleteOldMail(JSON.parse(getUserReadOnlyDataResult.Data["inbox"].Value), false);
        let targetMailInfo = inbox.MailList.find(n => n.Id == mailId);

        if(targetMailInfo != null)
        {
            let attachedItemInfo = targetMailInfo.AttachedItem;

            if(attachedItemInfo != null)
            {                    
                if(attachedItemInfo.ItemId == "gem" || attachedItemInfo.ItemId == "guild_token")
                    AddVirtualCurrency(attachedItemInfo.ItemCode, Number(attachedItemInfo.Count));
                else
                    return toReturn;

                toReturn.itemId = attachedItemInfo.ItemId;
                toReturn.count = attachedItemInfo.Count;
                toReturn.error = false;

                let updatedMailList = inbox.MailList.filter(n => n.Id != mailId);
                inbox.MailList = updatedMailList;

                // Update inbox after received mail
                let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                    PlayFabId: currentPlayerId,
                    Data: {
                        "inbox": JSON.stringify(inbox)
                    }
                });

                return toReturn;
            }
            else
                return toReturn;
        }
        else
            return toReturn;
    }
    else
        return toReturn;
}
handlers["requestAttachedItem"] = RequestAttachedItem;

interface IRequestAttachedItemAll{
    receiveItemList: {};
    error: boolean;
}

let RequestAttachedItemAll = function(args: any, context: IPlayFabContext): IRequestAttachedItemAll{
    let toReturn: IRequestAttachedItemAll = {receiveItemList: {}, error: true};
    let receiveItemList = [];

    // Get mail list
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["inbox"]
    });

    if(getUserReadOnlyDataResult.Data["inbox"] != null)
    {
        let inbox = DeleteOldMail(JSON.parse(getUserReadOnlyDataResult.Data["inbox"].Value), false);
        let updatedMailList = inbox.MailList;
        
        let totalGem = 0;
        let totalGuildToken = 0;

        if(inbox.MailList.length > 0)
        {
            for(let i = 0; i < inbox.MailList.length ; i ++)
            {
                let targetMailInfo = inbox.MailList[i];

                if(targetMailInfo != null)
                {
                    let attachedItemInfo = targetMailInfo.AttachedItem;

                    if(attachedItemInfo != null)
                    {
                        if(attachedItemInfo.ItemId == "gem")
                            totalGem = totalGem + Number(attachedItemInfo.Count);
                        else if(attachedItemInfo.ItemId == "guild_token")
                            totalGuildToken = totalGuildToken + Number(attachedItemInfo.Count);
                        else
                            return toReturn;

                        let targetItem = receiveItemList.filter(n => n.ItemId == attachedItemInfo.ItemId)[0];
                        if(targetItem != null)
                            targetItem.Count = parseInt(targetItem.Count) + parseInt(attachedItemInfo.Count);
                        else
                            receiveItemList.push({"ItemId": attachedItemInfo.ItemId, "ItemCode": attachedItemInfo.ItemCode, "Count": attachedItemInfo.Count});
                        
                        updatedMailList = updatedMailList.filter(n => n.Id != targetMailInfo.Id);
                    }
                }        
            }

            if(totalGem > 0)
                AddVirtualCurrency("GE", totalGem);

            if(totalGuildToken > 0)
                AddVirtualCurrency("GT", totalGuildToken);

            toReturn.receiveItemList = {"ItemList": receiveItemList};
            inbox.MailList = updatedMailList;
            
            // Update inbox after received mail
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "inbox": JSON.stringify(inbox)
                }
            });

            toReturn.error = false;
        }
        else
            log.info("inbox is empty now!");

        return toReturn;
    }
    else
        return toReturn;
}
handlers["requestAttachedItemAll"] = RequestAttachedItemAll;

interface IAddOnTimeEventMail{
    error: boolean;
}

let AddOnTimeEventMail = function(args: any, context: IPlayFabContext): IAddOnTimeEventMail{
    let message = "";
    let attachedItem: {} = null;
    let addedTime = 259200;
    let toReturn: IAddOnTimeEventMail = {error: true};

    if(args.message)
        message = args.message;
    if(args.attachedItem)
        attachedItem = args.attachedItem as {};
    if(args.addedTime)
        addedTime = args.addedTime;

    // Get mail list
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["inbox"]
    });

    if(getUserReadOnlyDataResult.Data["inbox"] != null)
    {
        let inbox = JSON.parse(getUserReadOnlyDataResult.Data["inbox"].Value);

        inbox = DeleteOldMail(inbox, true);

        let mailId = 0;
        
        for(let i = 0; i < inbox.MailList.length ; i ++)
        {
            if(Number(inbox.MailList[i].Id) >= mailId)
            {
                mailId = Number(inbox.MailList[i].Id);
            }
        }

        mailId += 1;
        // Expired time will be 3 days later or depend on addedTime
        let currentTimestampSecond = Math.floor(+ new Date() / (1000));
        let expiredTime = currentTimestampSecond + addedTime;

        inbox.MailList.push({Id: mailId, Message: message, AttachedItem: attachedItem, ExpiredTime: expiredTime});

        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "inbox": JSON.stringify(inbox)
            }
        });

        toReturn.error = false;
    }
    else
    {
        let mailId = 0;
        // Expired time will be 3 days later or depend on addedTime
        let currentTimestampSecond = Math.floor(+ new Date() / (1000));
        let expiredTime = currentTimestampSecond + addedTime;

        let defaultInbox = {"MailList":[]};

        defaultInbox.MailList.push({Id: mailId, Message: message, AttachedItem: attachedItem, ExpiredTime: expiredTime});

        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "inbox": JSON.stringify(defaultInbox)
            }
        });

        toReturn.error = false;
    }

    return toReturn;
}
handlers["addOnTimeEventMail"] = AddOnTimeEventMail;

interface IAddMail{
    error: boolean;
}

let AddMail = function(args: any, context: IPlayFabContext): IAddMail{
    let message = "";
    let attachedItem: {} = null;
    let toReturn: IAddMail = {error: true};

    if(args.message)
        message = args.message;
    if(args.attachedItem)
        attachedItem = args.attachedItem as {};

    // Get mail list
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["inbox"]
    });

    if(getUserReadOnlyDataResult.Data["inbox"] != null)
    {
        let inbox = JSON.parse(getUserReadOnlyDataResult.Data["inbox"].Value);

        inbox = DeleteOldMail(inbox, true);

        let mailId = 0;
        
        for(let i = 0; i < inbox.MailList.length ; i ++)
        {
            if(Number(inbox.MailList[i].Id) >= mailId)
            {
                mailId = Number(inbox.MailList[i].Id);
            }
        }

        mailId += 1;
        // Expired time will be 30days later
        let currentTimestampSecond = Math.floor(+ new Date() / (1000));
        let expiredTime = currentTimestampSecond + 2592000;
        // log.info("mailId: " + mailId + " / expiredTime: " + expiredTime);

        inbox.MailList.push({Id: mailId, Message: message, AttachedItem: attachedItem, ExpiredTime: expiredTime});

        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "inbox": JSON.stringify(inbox)
            }
        });

        toReturn.error = false;
    }
    else
    {
        let mailId = 0;
        // Expired time will be 30days later
        let currentTimestampSecond = Math.floor(+ new Date() / (1000));
        let expiredTime = currentTimestampSecond + 2592000;

        let defaultInbox = {"MailList":[]};

        defaultInbox.MailList.push({Id: mailId, Message: message, AttachedItem: attachedItem, ExpiredTime: expiredTime});

        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "inbox": JSON.stringify(defaultInbox)
            }
        });

        toReturn.error = false;
    }

    return toReturn;
}
handlers["addMail"] = AddMail;

let AddMailWithId = function(args: any, context: IPlayFabContext): IAddMail{
    let targetId = "";
    let message = "";
    let attachedItem: {} = null;
    let toReturn: IAddMail = {error: true};

    if(args.Id)
        targetId = args.Id
    if(args.message)
        message = args.message;
    if(args.attachedItem)
        attachedItem = args.attachedItem as {};

    // Check target id is null or not
    if(targetId == "")
        return toReturn;

    // Get mail list
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: targetId,
        Keys: ["inbox"]
    });

    if(getUserReadOnlyDataResult.Data["inbox"] != null)
    {
        let inbox = JSON.parse(getUserReadOnlyDataResult.Data["inbox"].Value);

        let mailId = 0;
        
        for(let i = 0; i < inbox.MailList.length ; i ++)
        {
            if(Number(inbox.MailList[i].Id) >= mailId)
            {
                mailId = Number(inbox.MailList[i].Id);
            }
        }

        mailId += 1;
        // Expired time will be 7days later
        let currentTimestampSecond = Math.floor(+ new Date() / (1000));
        let expiredTime = currentTimestampSecond + 604800;
        // log.info("mailId: " + mailId + " / expiredTime: " + expiredTime);

        inbox.MailList.push({Id: mailId, Message: message, AttachedItem: attachedItem, ExpiredTime: expiredTime});

        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: targetId,
            Data: {
                "inbox": JSON.stringify(inbox)
            }
        });

        toReturn.error = false;
    }
    else
    {
        let mailId = 0;
        // Expired time will be 7days later
        let currentTimestampSecond = Math.floor(+ new Date() / (1000));
        let expiredTime = currentTimestampSecond + 604800;

        let defaultInbox = {"MailList":[]};

        defaultInbox.MailList.push({Id: mailId, Message: message, AttachedItem: attachedItem, ExpiredTime: expiredTime});

        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: targetId,
            Data: {
                "inbox": JSON.stringify(defaultInbox)
            }
        });

        toReturn.error = false;
    }

    return toReturn;
}
handlers["addMailWithId"] = AddMailWithId;

interface IAddTextMail{
    reasonOfError: string;
    error: boolean;
}

let AddTextMail = function(args: any, context: IPlayFabContext): IAddTextMail{
    let message = "";
    let sender = "";
    let toReturn: IAddTextMail = {reasonOfError: "", error: true};

    if(args.message)
        message = args.message;
    if(args.sender)
        sender = args.sender;

    // Get mail list
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["inbox", "mute_text_mail"]
    });

    let mailId = 0;

    // Expired time will be 1 day later
    let currentTimestampSecond = Math.floor(+ new Date() / (1000));
    let expiredTime = currentTimestampSecond + 86400;
    let isMute = false;

    if(getUserReadOnlyDataResult.Data["mute_text_mail"] != null)
        isMute = JSON.parse(getUserReadOnlyDataResult.Data["mute_text_mail"].Value);

    // Mute option
    if(isMute)
    {
        toReturn.reasonOfError = "recipient_is_mute";
        toReturn.error = true;

        return toReturn;
    }
    else
    {
        // Make a message
        message = "<s>" + sender + "<m>" + message;

        // Check mesasge length
        if(message.length > 100)
        {
            toReturn.reasonOfError = "length_is_over";
            toReturn.error = true;

            return toReturn;
        }

        if(getUserReadOnlyDataResult.Data["inbox"] != null)
        {
            let inbox = JSON.parse(getUserReadOnlyDataResult.Data["inbox"].Value);

            inbox = DeleteOldMail(inbox, true);

            for(let i = 0; i < inbox.MailList.length ; i ++)
            {
                if(Number(inbox.MailList[i].Id) >= mailId)
                {
                    mailId = Number(inbox.MailList[i].Id);
                }
            }

            mailId += 1; 
            inbox.MailList.push({Id: mailId, Message: message, AttachedItem: null, ExpiredTime: expiredTime});

            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "inbox": JSON.stringify(inbox)
                }
            });

            toReturn.error = false;
        }
        else
        {
            let defaultInbox = {"MailList":[]};
            defaultInbox.MailList.push({Id: mailId, Message: message, AttachedItem: null, ExpiredTime: expiredTime});

            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "inbox": JSON.stringify(defaultInbox)
                }
            });

            toReturn.error = false;
        }

        // log.info("mailId: " + mailId + " / expiredTime: " + expiredTime);
        return toReturn;
    }    
}
handlers["addTextMail"] = AddTextMail;

interface IRequestDeleteMail{
    error: boolean;
}

let RequestDeleteMail = function(args: any, context: IPlayFabContext): IRequestDeleteMail{
    let mailId = "";
    let toReturn: IRequestDeleteMail = {error: true};

    if(args.mailId)
        mailId = args.mailId;

    // log.info("Delete Mail ID : " + mailId);

    // Get mail list
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["inbox"]
    });

    if(getUserReadOnlyDataResult.Data["inbox"] != null)
    {
        let inbox = JSON.parse(getUserReadOnlyDataResult.Data["inbox"].Value);

        let targetMailInfo = inbox.MailList.find(n => n.Id == mailId);

        if(targetMailInfo != null)
        {
            let attachedItemInfo = targetMailInfo.AttachedItem;

            // Delete target mail
            let updatedMailList = inbox.MailList.filter(n => n.Id != mailId);
            inbox.MailList = updatedMailList;

            DeleteOldMail(inbox, false);

            toReturn.error = false;
            return toReturn;
        }
        else
            return toReturn;
    }
    else
        return toReturn;
}

handlers["requestDeleteMail"] = RequestDeleteMail;

///////////////////////// HELPER FUNCTIONS (NOT DIRECTLY CALLABLE FROM THE CLIENT) /////////////////////////
function DeleteOldMail(currentInbox: any, skipUpdate: boolean): any
{
    if(currentInbox != null)
    {
        let currentTimestampSecond = Math.floor(+ new Date() / (1000));
        let updatedMailList = currentInbox.MailList;

        for(let i = 0; i < currentInbox.MailList.length ; i ++)
        {
            if(Number(currentInbox.MailList[i].ExpiredTime) < currentTimestampSecond)
            {
                updatedMailList = updatedMailList.filter(n => n.Id != currentInbox.MailList[i].Id);
            }
        }

        currentInbox.MailList = updatedMailList;

        if(!skipUpdate)
        {
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "inbox": JSON.stringify(currentInbox)
                }
            });
        }   

        return currentInbox;
    }
}   
 
