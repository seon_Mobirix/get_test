
///////////////////////////////////////////////////////////////////////////////////////////////////////
// Dungeon Knight
///////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////// Cloud Script Handler Functions /////////////////////////
interface IRequestAttendanceEventInfo{
    lastAttendanceTimeStampDay: number;
    attendanceEventInfo: any;
    reasonOfError: string;
    error: boolean;
}

let RequestAttendanceEventInfo = function(args: any, context: IPlayFabContext): IRequestAttendanceEventInfo{

    let toReturn: IRequestAttendanceEventInfo = {lastAttendanceTimeStampDay: 0, attendanceEventInfo: null, reasonOfError: "", error: true};  
    let attendanceEventData = null;

    // Get user attendance event data
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["last_attendance_event_timestampday"]
    });

    if(getUserReadOnlyDataResult.Data["last_attendance_event_timestampday"] != null)
        toReturn.lastAttendanceTimeStampDay = parseInt(getUserReadOnlyDataResult.Data["last_attendance_event_timestampday"].Value);

    // Get attendance Event reward table
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "attendance_event_data"
        ]
    }).Data;

    attendanceEventData = JSON.parse(getTitleDataResult["attendance_event_data"]);

    if(attendanceEventData == null)
        return toReturn;

    let currentTimeStampDay = Math.floor(+ new Date() / (1000 * 60 * 60 * 24));

    // Check event day vaildate
    if(attendanceEventData.StartTimeStampDay > currentTimeStampDay || attendanceEventData.EndTimeStampDay < currentTimeStampDay)
    {
        toReturn.reasonOfError = "event_expired";    
        return toReturn;
    }
    
    toReturn.attendanceEventInfo = attendanceEventData;
    toReturn.error = false;

    return toReturn;
}
handlers["requestAttendanceEventInfo"] = RequestAttendanceEventInfo;

let RequestAttendanceEventInfoQA = function(args: any, context: IPlayFabContext): IRequestAttendanceEventInfo{

    let toReturn: IRequestAttendanceEventInfo = {lastAttendanceTimeStampDay: 0, attendanceEventInfo: null, reasonOfError: "", error: true};  
    let attendanceEventData = null;

    // Get user attendance event data
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["last_attendance_event_timestampday"]
    });

    if(getUserReadOnlyDataResult.Data["last_attendance_event_timestampday"] != null)
        toReturn.lastAttendanceTimeStampDay = parseInt(getUserReadOnlyDataResult.Data["last_attendance_event_timestampday"].Value);

    // Get attendance Event reward table
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "qa_attendance_event_data"
        ]
    }).Data;

    attendanceEventData = JSON.parse(getTitleDataResult["qa_attendance_event_data"]);

    if(attendanceEventData == null)
        return toReturn;

    let currentTimeStampDay = Math.floor(+ new Date() / (1000 * 60 * 60 * 24));

    // Check event day vaildate
    if(attendanceEventData.StartTimeStampDay > currentTimeStampDay || attendanceEventData.EndTimeStampDay < currentTimeStampDay)
    {
        toReturn.reasonOfError = "event_expired";    
        return toReturn;
    }
    
    toReturn.attendanceEventInfo = attendanceEventData;
    toReturn.error = false;

    return toReturn;
}
handlers["requestAttendanceEventInfoQA"] = RequestAttendanceEventInfoQA;

interface IRequestAttendanceEventReward{
    itemId: string;
    itemCode: string;
    count: number;
    lastAttendanceTimeStamp: number;
    error: boolean;
}

let RequestAttendanceEventReward = function(args: any, context: IPlayFabContext): IRequestAttendanceEventReward{

    let toReturn: IRequestAttendanceEventReward = {itemId: "", itemCode: "", count: 0, lastAttendanceTimeStamp: 0, error: true};
    let selectedRewardIndex = 0;
    let attendanceEventData = null;
    let lastTimestamp = 0;
    let attendanceEventLog = null;

    // Get user selected reward index
    if(args.selectedRewardIndex)
        selectedRewardIndex = args.selectedRewardIndex;

    // Get attendance Event reward table
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "attendance_event_data"
        ]
    }).Data;

    attendanceEventData = JSON.parse(getTitleDataResult["attendance_event_data"]);

    if(attendanceEventData == null)
        return toReturn;

    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: [
            "attendance_event_log", "last_attendance_event_timestampday"
        ]
    });

    if(getUserReadOnlyDataResult.Data["attendance_event_log"] != null)
        attendanceEventLog = JSON.parse(getUserReadOnlyDataResult.Data["attendance_event_log"].Value);
    else
        attendanceEventLog = {"LogList":[]};

    if(getUserReadOnlyDataResult.Data["last_attendance_event_timestampday"] != null)
        lastTimestamp = parseInt(getUserReadOnlyDataResult.Data["last_attendance_event_timestampday"].Value);

    let currentTimeStampSecond = Math.floor(+ new Date() / (1000));
    let currentTimeStampDay = Math.floor(+ new Date() / (1000 * 60 * 60 * 24));
    
    if(currentTimeStampDay > lastTimestamp)
    {   
        // Check event day vaildate
        if(attendanceEventData.StartTimeStampDay > currentTimeStampDay || attendanceEventData.EndTimeStampDay < currentTimeStampDay)
        {
            log.info("This event's period is not validate");
            return toReturn;
        }

        // Check event reward list index vaildate
        if(attendanceEventData.AttendanceEventRewardList.length <= selectedRewardIndex)
        {
            log.info("Selected reward index value is not validate");
            return toReturn;
        }

        let currentEventLogCount = attendanceEventLog.LogList.filter(n => n.EventId == attendanceEventData.EventId).length;

        // Check event max attendance limit count
        if(currentEventLogCount >= attendanceEventData.AttendanceDayLimit)
        {
            log.info("This user can't get reward over than limit count");
            return toReturn;
        }

        let targetRewardInfo = attendanceEventData.AttendanceEventRewardList[selectedRewardIndex];

        if(targetRewardInfo != null)
        {
            // Give target reward to user
            if(targetRewardInfo.ItemId == "gem")
                AddVirtualCurrency(targetRewardInfo.ItemCode, Number(targetRewardInfo.ItemCount));
            
            toReturn.itemId = targetRewardInfo.ItemId;
            toReturn.itemCode = targetRewardInfo.ItemCode;
            toReturn.count = targetRewardInfo.ItemCount;
            toReturn.lastAttendanceTimeStamp = currentTimeStampDay;
            
            // Update attendance event log
            attendanceEventLog.LogList.push({EventId: attendanceEventData.EventId, ItemId: toReturn.itemId, ItemCode: toReturn.itemCode, Count: toReturn.count, ReceivedTime: currentTimeStampSecond});

            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "last_attendance_event_timestampday": currentTimeStampDay.toString(),
                    "attendance_event_log": JSON.stringify(attendanceEventLog)
                }
            });
    
            toReturn.error = false;
            return toReturn;
        }
        else
        {
            log.info("Target Reward's info is null!");
            return toReturn;
        }
    }
    else
        return toReturn;
}
handlers["requestAttendanceEventReward"] = RequestAttendanceEventReward;

let RequestAttendanceEventRewardQA = function(args: any, context: IPlayFabContext): IRequestAttendanceEventReward{

    let toReturn: IRequestAttendanceEventReward = {itemId: "", itemCode: "", count: 0, lastAttendanceTimeStamp: 0, error: true};
    let selectedRewardIndex = 0;
    let attendanceEventData = null;
    let lastTimestamp = 0;
    let attendanceEventLog = null;

    // Get user selected reward index
    if(args.selectedRewardIndex)
        selectedRewardIndex = args.selectedRewardIndex;

    // Get attendance Event reward table
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "qa_attendance_event_data"
        ]
    }).Data;

    attendanceEventData = JSON.parse(getTitleDataResult["qa_attendance_event_data"]);

    if(attendanceEventData == null)
        return toReturn;

    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: [
            "attendance_event_log", "last_attendance_event_timestampday"
        ]
    });

    if(getUserReadOnlyDataResult.Data["attendance_event_log"] != null)
        attendanceEventLog = JSON.parse(getUserReadOnlyDataResult.Data["attendance_event_log"].Value);
    else
        attendanceEventLog = {"LogList":[]};

    if(getUserReadOnlyDataResult.Data["last_attendance_event_timestampday"] != null)
        lastTimestamp = parseInt(getUserReadOnlyDataResult.Data["last_attendance_event_timestampday"].Value);

    let currentTimeStampSecond = Math.floor(+ new Date() / (1000));
    let currentTimeStampDay = Math.floor(+ new Date() / (1000 * 60 * 60 * 24));
    
    if(currentTimeStampDay > lastTimestamp)
    {   
        // Check event day vaildate
        if(attendanceEventData.StartTimeStampDay > currentTimeStampDay || attendanceEventData.EndTimeStampDay < currentTimeStampDay)
        {
            log.info("This event's period is not validate");
            return toReturn;
        }

        // Check event reward list index vaildate
        if(attendanceEventData.AttendanceEventRewardList.length <= selectedRewardIndex)
        {
            log.info("Selected reward index value is not validate");
            return toReturn;
        }

        let currentEventLogCount = attendanceEventLog.LogList.filter(n => n.EventId == attendanceEventData.EventId).length;

        // Check event max attendance limit count
        if(currentEventLogCount >= attendanceEventData.AttendanceDayLimit)
        {
            log.info("This user can't get reward over than limit count");
            return toReturn;
        }

        let targetRewardInfo = attendanceEventData.AttendanceEventRewardList[selectedRewardIndex];

        if(targetRewardInfo != null)
        {
            // Give target reward to user
            if(targetRewardInfo.ItemId == "gem")
                AddVirtualCurrency(targetRewardInfo.ItemCode, Number(targetRewardInfo.ItemCount));
            
            toReturn.itemId = targetRewardInfo.ItemId;
            toReturn.itemCode = targetRewardInfo.ItemCode;
            toReturn.count = targetRewardInfo.ItemCount;
            toReturn.lastAttendanceTimeStamp = currentTimeStampDay;
            
            // Update attendance event log
            attendanceEventLog.LogList.push({EventId: attendanceEventData.EventId, ItemId: toReturn.itemId, ItemCode: toReturn.itemCode, Count: toReturn.count, ReceivedTime: currentTimeStampSecond});

            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "last_attendance_event_timestampday": currentTimeStampDay.toString(),
                    "attendance_event_log": JSON.stringify(attendanceEventLog)
                }
            });
    
            toReturn.error = false;
            return toReturn;
        }
        else
        {
            log.info("Target Reward's info is null!");
            return toReturn;
        }
    }
    else
        return toReturn;
}
handlers["requestAttendanceEventRewardQA"] = RequestAttendanceEventRewardQA;