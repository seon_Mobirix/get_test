///////////////////////////////////////////////////////////////////////////////////////////////////////
// Dungeon Knight
///////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////// Cloud Script Handler Functions /////////////////////////

interface IRegisterModerator 
{
    error: boolean;
}

let RegisterModerator = function (args: any, context: IPlayFabContext): IRegisterModerator 
{
    let toReturn : IRegisterModerator = {error: true};

    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["is_moderator"]
    });

    if(getUserReadOnlyDataResult.Data["is_moderator"] == null)
    {
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "is_moderator": "true"
            }
        });

        toReturn.error = false;
    }
    else
    {
        if(getUserReadOnlyDataResult.Data["is_moderator"].Value == "false")
        {
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "is_moderator": "true"
                }
            });
            
            toReturn.error = false;
        }
    }

    return toReturn;
}

handlers["registerModerator"] = RegisterModerator;

interface IUnregisterModerator 
{
    error: boolean;
}

let UnregisterModerator = function (args: any, context: IPlayFabContext): IUnregisterModerator 
{
    let toReturn : IUnregisterModerator = {error: true};

    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["is_moderator"]
    });

    if(getUserReadOnlyDataResult.Data["is_moderator"] != null && getUserReadOnlyDataResult.Data["is_moderator"].Value == "true") 
    {
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "is_moderator": "false"
            }
        });

        toReturn.error = false;
    }

    return toReturn;
}

handlers["unregisterModerator"] = UnregisterModerator;

interface IRequestChatBan 
{
    error: boolean;
    reasonOfError: string;
    updatedBanData: any;
}

let RequestChatBan = function (args: any, context: IPlayFabContext): IRequestChatBan 
{
    let toReturn : IRequestChatBan = {error: true, reasonOfError: "", updatedBanData: []};
    let targetPlayfabId = "";
    let chatLog = "";
    
    // Get target playerFabId
    if(args.targetPlayfabId)
        targetPlayfabId = args.targetPlayfabId;
    
    if(args.chatLog)
        chatLog = args.chatLog;

    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["is_moderator", "chat_ban_data"]
    });

    // Check moderator
    if(getUserReadOnlyDataResult.Data["is_moderator"] != null && getUserReadOnlyDataResult.Data["is_moderator"].Value == "true") 
    {
        let getTargetReadOnlyDataResult = server.GetUserReadOnlyData({
            PlayFabId: targetPlayfabId,
            Keys: ["is_chat_banned"]
        });

        // Check target player already banned
        if(getTargetReadOnlyDataResult.Data["is_chat_banned"] != null && getTargetReadOnlyDataResult.Data["is_chat_banned"].Value == "true")
        {
            log.info("Target user is already chat banned !");
            toReturn.reasonOfError = "ban_duplicate";
            return toReturn;
        }

        // Set target player chat block
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: targetPlayfabId,
            Data: {
                "is_chat_banned": "true",
                "chat_ban_log": chatLog
            }
        });

        let currentTimestampSecond = Math.floor(+ new Date() / (1000));

        // Save log in ban list
        if(getUserReadOnlyDataResult.Data["chat_ban_data"] != null)
        {
            let banData = JSON.parse(getUserReadOnlyDataResult.Data["chat_ban_data"].Value);

            // Check duplicate target playfab id
            if(banData.BanList.filter(n => n.Id == targetPlayfabId).length > 0)
            {
                toReturn.reasonOfError = "ban_duplicate";
                return toReturn;
            }

            banData.BanList.push({Id: targetPlayfabId, BanTime: currentTimestampSecond});

            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "chat_ban_data": JSON.stringify(banData)
                }
            });

            toReturn.updatedBanData = banData;
            toReturn.error = false;
        }
        else
        {
            let defaultBanData = {"BanList":[]};

            defaultBanData.BanList.push({Id: targetPlayfabId, BanTime: currentTimestampSecond});

            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "chat_ban_data": JSON.stringify(defaultBanData)
                }
            });

            toReturn.updatedBanData = defaultBanData;
            toReturn.error = false;
        }
    }

    return toReturn;
}

handlers["requestChatBan"] = RequestChatBan;

interface IRequestChatBanCancel
{
    error: boolean;
    reasonOfError: string;
    updatedBanData: any;
}

let RequestChatBanCancel = function (args: any, context: IPlayFabContext): IRequestChatBanCancel 
{
    let toReturn : IRequestChatBanCancel = {error: true, reasonOfError: "", updatedBanData: []};
    let targetPlayfabId = "";
    
    // Get target playerFabId
    if(args.targetPlayfabId)
        targetPlayfabId = args.targetPlayfabId;

    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["is_moderator", "chat_ban_data"]
    });

    // Check moderator
    if(getUserReadOnlyDataResult.Data["is_moderator"] != null && getUserReadOnlyDataResult.Data["is_moderator"].Value == "true") 
    {
        if(getUserReadOnlyDataResult.Data["chat_ban_data"] != null)
        {
            let banData = JSON.parse(getUserReadOnlyDataResult.Data["chat_ban_data"].Value);

            // Check target playfab id in ban list
            if(banData.BanList.filter(n => n.Id == targetPlayfabId).length == 0)
            {
                toReturn.reasonOfError = "ban_not_exist";
                return toReturn;
            }
            
            // Remove target playfab id
            banData.BanList = banData.BanList.filter(n => n.Id != targetPlayfabId);
            toReturn.updatedBanData = banData;

            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "chat_ban_data": JSON.stringify(banData)
                }
            });

            // Set target player chat block cancel
            let updateTargetUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: targetPlayfabId,
                Data: {
                    "is_chat_banned": "false",
                    "chat_ban_log": null
                }
            });

            toReturn.error = false;
        }
    }

    return toReturn;
}

handlers["requestChatBanCancel"] = RequestChatBanCancel;

interface IDirectlyRequestChatBanCancel
{
    error: boolean;
}

let DirectlyRequestChatBanCancel = function (args: any, context: IPlayFabContext): IDirectlyRequestChatBanCancel 
{
    let toReturn : IDirectlyRequestChatBanCancel = {error: true};

    // Set player chat block cancel
    let updateTargetUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Data: {
            "is_chat_banned": "false",
            "chat_ban_log": null
        }
    });

    toReturn.error = false;

    return toReturn;
}

handlers["directlyRequestChatBanCancel"] = DirectlyRequestChatBanCancel;

interface IRequestChatLog
{
    error: boolean;
    chatLog: string;
}

let RequestChatLog = function (args: any, context: IPlayFabContext): IRequestChatLog 
{
    let toReturn : IRequestChatLog = {error: true, chatLog: ""};
    let targetPlayfabId = "";
    
    // Get target playerFabId
    if(args.targetPlayfabId)
        targetPlayfabId = args.targetPlayfabId;

    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["is_moderator", "chat_ban_data"]
    });

    // Check moderator
    if(getUserReadOnlyDataResult.Data["is_moderator"] != null && getUserReadOnlyDataResult.Data["is_moderator"].Value == "true") 
    {
        if(getUserReadOnlyDataResult.Data["chat_ban_data"] != null)
        {
            let banData = JSON.parse(getUserReadOnlyDataResult.Data["chat_ban_data"].Value);

            // Check target playfab id in ban list
            if(banData.BanList.filter(n => n.Id == targetPlayfabId).length == 0)
            {
                log.info("target user is not exist in ban list!");
                return toReturn;
            }
            
            // Get target id's chat log data
            let getTargetIdReadOnlyDataResult = server.GetUserReadOnlyData({
                PlayFabId: targetPlayfabId,
                Keys: ["chat_ban_log"]
            });

            if(getTargetIdReadOnlyDataResult.Data["chat_ban_log"] == null)
            {
                log.info("target user's chat ban log is not exist!");
                return toReturn;
            }
            else
                toReturn.chatLog = getTargetIdReadOnlyDataResult.Data["chat_ban_log"].Value;
            
            toReturn.error = false;
        }
    }

    return toReturn;
}

handlers["requestChatLog"] = RequestChatLog;

///////////////////////// HELPER FUNCTIONS (NOT DIRECTLY CALLABLE FROM THE CLIENT) /////////////////////////