///////////////////////////////////////////////////////////////////////////////////////////////////////
// Dungeon Knight
///////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////// Cloud Script Handler Functions /////////////////////////
interface ITowerRefresh{
    error: boolean;
    collectedGems: number;
    collectedFeathers: number;
    currentFloor: number;
    secondLeft: number;
    minAndroidVersion: string;
    miniOSVersion: string;
    rewardRate: number;
    rewardFeatherRate: number;
    defenceRecords: any[];
    cooldownLeft: number;
    cooldownDeployLeft: number;
    dailyLimitLeft: number;
}

let TowerRefresh = function (args: any, context: IPlayFabContext): ITowerRefresh{
    let toReturn: ITowerRefresh = {error: true, collectedGems: 0, collectedFeathers: 0, currentFloor: 0, secondLeft: 0, minAndroidVersion: "0.0.0", miniOSVersion: "0.0.0",
        rewardRate: 0, rewardFeatherRate: 0, defenceRecords: [], cooldownLeft: 0, cooldownDeployLeft: 0, dailyLimitLeft: 0};
    
    // Get title data
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "setting_json"
        ]
    }).Data;

    if(getTitleDataResult["setting_json"] != null)
    {
        let convertedData = JSON.parse(getTitleDataResult["setting_json"]); 
        
        toReturn.minAndroidVersion = convertedData.MinAndroidVersion;
        toReturn.miniOSVersion = convertedData.MiniOSVersion;
    }

    let currentTimestampSecond = Math.floor(+ new Date() / (1000));

    // Get needed read only data first
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData(
        {
            PlayFabId: currentPlayerId,
            Keys: [
                "tower_data_1",
                "tower_last_timestamp_second"
            ]
        }
    );

    // Last timestamp for settlement
    let lastTimestampSecond = 0;
    if(getUserReadOnlyDataResult.Data["tower_last_timestamp_second"] != null)
        lastTimestampSecond = parseInt(getUserReadOnlyDataResult.Data["tower_last_timestamp_second"].Value);

    // Build tower data list
    let towerDataList = GetTowerDataList(getUserReadOnlyDataResult);
    
    // Settlement rewards
    towerDataList = ProcessSettlementTowerReward(towerDataList, lastTimestampSecond);
    
    toReturn.collectedGems = towerDataList[0].CollectedGems;
    toReturn.collectedFeathers = towerDataList[0].CollectedFeathers;
    toReturn.currentFloor = towerDataList[0].Floor;
    toReturn.secondLeft = Math.floor(Math.max(towerDataList[0].DeployedTimestamp + 14400 - currentTimestampSecond, 0));
    toReturn.defenceRecords = towerDataList[0].BattleRecord;
    toReturn.rewardRate = GetTowerRewardRate(toReturn.currentFloor);
    toReturn.rewardFeatherRate = GetTowerRewardFeatherRate(toReturn.currentFloor);

    // Left cooldown
    toReturn.cooldownLeft = GetCooldown("tower_battle");
    toReturn.cooldownDeployLeft = GetCooldown("tower_deploy");

    // Update data
    let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Data: {
            "tower_data_1": unescape(JSON.stringify(towerDataList[0])),
            "tower_last_timestamp_second": currentTimestampSecond.toString()
        },
        Permission : "Public"
    });

    toReturn.dailyLimitLeft = GetLeftDailyLimit("tower", "0", true);
    toReturn.error = false;
    return toReturn;
}

handlers["towerRefresh"] = TowerRefresh;

interface ITowerRefreshFloors{
    error: boolean;
    floorPlayersCount: any[];
    floorPlayers: any[];
}

let TowerRefreshFloors = function (args: any, context: IPlayFabContext): ITowerRefreshFloors{
    let toReturn: ITowerRefreshFloors = {error: true, floorPlayersCount: [], floorPlayers: []}

    let serverIdModifier = GetServerIdModifier(currentPlayerId);

    let currentTimestamp = Math.floor(+ new Date() / (1000));

    toReturn.floorPlayersCount.push(0);
    toReturn.floorPlayers.push({});
    for(let i = 2; i <= 11; i++)
    {
        let getLeaderboardResult = server.GetLeaderboard({
            StatisticName: "tower_floor_"  + i.toString() + serverIdModifier,
            StartPosition: 0,
            MaxResultsCount: 100
        });
        
        let playerInfoForFloor = getLeaderboardResult.Leaderboard.filter(n => n.StatValue + 240 > currentTimestamp / 60);

        // Add floor's player count
        toReturn.floorPlayersCount.push(playerInfoForFloor.length);
        
        // Add floor's player names
        let playerListForFloor = {};
        for(let j = 0; j < playerInfoForFloor.length; j++)
        {
            playerListForFloor[playerInfoForFloor[j].PlayFabId] = playerInfoForFloor[j].DisplayName;
        }
        toReturn.floorPlayers.push(playerListForFloor);
    }

    toReturn.error = false;
    return toReturn;
}

handlers["towerRefreshFloors"] = TowerRefreshFloors;

interface ITowerDeploy{
    error: boolean;
    reasonOfError: string;
    collectedGems: number;
    collectedFeathers: number;
    currentFloor: number;
    secondLeft: number;
    rewardRate: number;
    rewardFeatherRate: number;
    defenceRecords: any[];
}

let TowerDeploy = function (args: any, context: IPlayFabContext): ITowerDeploy{
    
    let toReturn: ITowerDeploy = {error: true, reasonOfError: "", collectedGems: 0, collectedFeathers: 0, currentFloor: 0, secondLeft: 0, rewardRate: 0, rewardFeatherRate: 0, defenceRecords: []};

    // Check for cooldown
    let towerDeployCooldown = GetCooldown("tower_deploy");
    if(towerDeployCooldown != 0)
    {
        log.info("Cooldown is not complete!");
        toReturn.reasonOfError = "left_cooldown";
        return toReturn;
    }

    // Check for daily limits
    if(!UseDailyLimitIfAvail("tower", "0", 1))
        return toReturn;

    // Need previous record
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData(
        {
            PlayFabId: currentPlayerId,
            Keys: [
                "tower_data_1"
            ]
        }
    );

    let towerDataList = GetTowerDataList(getUserReadOnlyDataResult);

    // Check floor is 0 or null. Otherwise it is already deployed. 
    if(towerDataList[0].Floor != 0)
        return toReturn;

    let currentTimestampSecond = Math.floor(+ new Date() / (1000));
    
    let dataToUse = {};

    towerDataList[0] = {
        "Floor": 1,
        "DeployedTimestamp": 0,
        "CollectedGems": 0,
        "collectedFeathers": 0,
        "BattleRecord": [
            {
                "Name": "",
                "Win": true,
                "Floor": 1,
                "IsDefense": false,
                "TimeStamp": Math.floor(currentTimestampSecond / 60)
            }
        ]
    }
    towerDataList[0].DeployedTimestamp = currentTimestampSecond;

    dataToUse["tower_data_1"] = unescape(JSON.stringify(towerDataList[0]));

    let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Data: dataToUse,
        Permission : "Public"
    });

    // Update floor information in the tower
    let serverIdModifier = GetServerIdModifier(currentPlayerId);
    UpdateTowerLeaderboard(serverIdModifier, towerDataList, currentPlayerId);

    toReturn.collectedGems = towerDataList[0].CollectedGems;
    toReturn.collectedFeathers = towerDataList[0].CollectedFeathers;
    toReturn.currentFloor = towerDataList[0].Floor;
    toReturn.secondLeft = Math.floor(Math.max(towerDataList[0].DeployedTimestamp + 14400 - currentTimestampSecond, 0));
    toReturn.defenceRecords = towerDataList[0].BattleRecord;
    toReturn.rewardRate = GetTowerRewardRate(toReturn.currentFloor);
    toReturn.rewardFeatherRate = GetTowerRewardFeatherRate(toReturn.currentFloor);

    toReturn.error = false;
    return toReturn;
}

handlers["towerDeploy"] = TowerDeploy;

interface ITowerRetreat{
    error: boolean;
    rewardGem: number;
    rewardFeather: number;
    collectedGems: number;
    collectedFeathers: number;
    currentFloor: number;
    secondLeft: number;
    rewardRate: number;
    rewardFeatherRate: number;
    defenceRecords: any[];
    cooldownDeployLeft: number;
    dailyLimitLeft: number;
}

let TowerRetreat = function (args: any, context: IPlayFabContext): ITowerRetreat{

    let toReturn: ITowerRetreat = {error: true, rewardGem: 0, rewardFeather: 0, collectedGems: 0, collectedFeathers: 0, currentFloor: 0, secondLeft: 0, rewardRate: 0, rewardFeatherRate: 0, defenceRecords: [], cooldownDeployLeft: 0, dailyLimitLeft: 0};

    // Start cooldown
    StartCooldown("tower_deploy");

    // Get required information
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData(
        {
            PlayFabId: currentPlayerId,
            Keys: [
                "tower_data_1",
                "tower_last_timestamp_second"
            ]
        }
    );

    // Last timestamp for settlement
    let lastTimestampSecond = 0;
    if(getUserReadOnlyDataResult.Data["tower_last_timestamp_second"] != null)
        lastTimestampSecond = parseInt(getUserReadOnlyDataResult.Data["tower_last_timestamp_second"].Value);

    // Build user's tower data list
    let towerDataList = GetTowerDataList(getUserReadOnlyDataResult);

    // Check floor is 0 or null. If so, it is not yet deployed.
    if(towerDataList[0].Floor == 0)
        return toReturn;

    // Give collected gems so far
    towerDataList = ProcessSettlementTowerReward(towerDataList, lastTimestampSecond);
    toReturn.rewardGem = Math.floor(towerDataList[0].CollectedGems);
    toReturn.rewardFeather = Math.floor(towerDataList[0].CollectedFeathers);
    AddVirtualCurrency("GE", toReturn.rewardGem);
    AddVirtualCurrency("FE", toReturn.rewardFeather);

    // Reset data of the user
    towerDataList[0] = {
        "Floor": 0,
        "DeployedTimestamp": 0,
        "CollectedGems": 0,
        "collectedFeathers": 0,
        "BattleRecord": []
    };
    
    let currentTimestampSecond = Math.floor(+ new Date() / (1000));

    // Update data
    let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Data: {
            "tower_data_1": unescape(JSON.stringify(towerDataList[0])),
            "tower_last_timestamp_second": currentTimestampSecond.toString()
        },
        Permission : "Public"
    });

    // Update floor information in the tower
    let serverIdModifier = GetServerIdModifier(currentPlayerId);
    UpdateTowerLeaderboard(serverIdModifier, towerDataList, currentPlayerId);

    toReturn.collectedGems = towerDataList[0].CollectedGems;
    toReturn.collectedFeathers = towerDataList[0].CollectedFeathers;
    toReturn.currentFloor = towerDataList[0].Floor;
    toReturn.secondLeft = Math.floor(Math.max(towerDataList[0].DeployedTimestamp + 14400 - currentTimestampSecond, 0));
    toReturn.defenceRecords = towerDataList[0].BattleRecord;
    toReturn.rewardRate = GetTowerRewardRate(toReturn.currentFloor);
    toReturn.rewardFeatherRate = GetTowerRewardFeatherRate(toReturn.currentFloor);

    // Left cooldown
    toReturn.cooldownDeployLeft = GetCooldown("tower_deploy");
    toReturn.dailyLimitLeft = GetLeftDailyLimit("tower", "0", true);

    toReturn.error = false;
    return toReturn;
}

handlers["towerRetreat"] = TowerRetreat;

interface ITowerBattleStart{
    error: boolean;
    opponentUserData: string;
    opponentInventoryData: string;
    opponentBonusStat: number;
    canSkipBecauseOpponentFinished: boolean;
    reasonOfError: string;
}

let TowerBattleStart = function (args: any, context: IPlayFabContext): ITowerBattleStart{
    
    let toReturn: ITowerBattleStart = {error: true, opponentUserData: "", opponentInventoryData: "", opponentBonusStat: 0, canSkipBecauseOpponentFinished: false, reasonOfError: ""};

    let opponentId = "";
    if(args.opponentId)
        opponentId = args.opponentId;
    else
        return toReturn;

    // Check for cooldown
    if(GetCooldown("tower_battle") != 0)
    {
        log.info("Cooldown is not complete!");
        toReturn.reasonOfError = "left_cooldown";
        return toReturn;
    }

    // Start cooldown
    StartCooldown("tower_battle");

    // Get opponent's data
    let opponentReadOnlyData = server.GetUserReadOnlyData({
        PlayFabId: opponentId,
        Keys: [
            "inventory_data",
            "user_data",
            "tower_data_1",
            "tower_last_timestamp_second"
        ]
    });

    toReturn.opponentUserData = opponentReadOnlyData.Data["user_data"].Value;
    toReturn.opponentInventoryData = opponentReadOnlyData.Data["inventory_data"].Value;

    // Build my tower data
    let opponentTowerDataList = GetTowerDataList(opponentReadOnlyData); 
    
    // Update leaderboard of opponent
    let serverIdModifier = GetServerIdModifier(opponentId);
    UpdateTowerLeaderboard(serverIdModifier, opponentTowerDataList, opponentId);

    // If the target is supposed to be left now, then the player can skip the battle
    let currentTimestampSecond = Math.floor(+ new Date() / (1000));
    if(opponentTowerDataList[0].DeployedTimestamp + 14400 <= currentTimestampSecond)
    {
        toReturn.canSkipBecauseOpponentFinished = true;
        toReturn.error = false;
        return toReturn;
    }

    // Save that arena started, and also the opponent's id
    let updateUserInternalDataResult = server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: {
            "is_playing_tower_battle": "true",
            "tower_battle_opponent_id": opponentId
        }
    });

    // Get opponent inventory to get bonus stat
    let getUserInventoryResult = server.GetUserInventory(
        {
            PlayFabId: opponentId
        }
    );
    toReturn.opponentBonusStat = getUserInventoryResult.VirtualCurrency["BS"].valueOf();

    toReturn.error = false;
    return toReturn;
}

handlers["towerBattleStart"] = TowerBattleStart;

interface ITowerBattleEnd{
    error: boolean;
}

let TowerBattleEnd = function (args: any, context: IPlayFabContext): ITowerBattleEnd
{
    let toReturn: ITowerBattleEnd = {error: true};

    let didWin = false;
    if(args.isWin)
        didWin = args.isWin;

    let isSuspiciousIfWin = false;
    if(args.isSuspiciousIfWin)
        isSuspiciousIfWin = args.isSuspiciousIfWin;

    if(isSuspiciousIfWin)
    {
        server.UpdatePlayerStatistics({
            PlayFabId: currentPlayerId,
            Statistics: [
                {
                    "StatisticName": "Cheater3",
                    "Value": 1
                }
            ]
        });
    }

    let serverIdModifier = GetServerIdModifier(currentPlayerId);
    
    // Check validity of the match. The match should have started before this API is called!
    let isValidCompletion = false;

    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "is_playing_tower_battle",
            "tower_battle_opponent_id"
        ]
    }).Data;

    if(getUserInternalDataResult["is_playing_tower_battle"] && getUserInternalDataResult["is_playing_tower_battle"].Value == "true")
        isValidCompletion = true;

    if(!isValidCompletion)
        return toReturn;

    let opponentId = "";
    if(getUserInternalDataResult["tower_battle_opponent_id"])
        opponentId = getUserInternalDataResult["tower_battle_opponent_id"].Value;

    // Reset is_playing_arena flag
    let updateUserInternalDataResult = server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: {
            "is_playing_tower_battle": "false"
        }
    });

    // Get opponent's read only data
    let getOpponentReadOnlyDataResult = server.GetUserReadOnlyData(
        {
            PlayFabId: opponentId,
            Keys: [
                "tower_data_1",
                "tower_last_timestamp_second"
            ]
        }
    );

    // Build tower data of opponent
    let opponentTowerDataList = GetTowerDataList(getOpponentReadOnlyDataResult);

    // Last timestamp for settlement
    let opponentLastTimestampSecond = 0;
    if(getOpponentReadOnlyDataResult.Data["tower_last_timestamp_second"] != null)
        opponentLastTimestampSecond = parseInt(getOpponentReadOnlyDataResult.Data["tower_last_timestamp_second"].Value);

    // Get my read only data
    let getMyReadOnlyDataResult = server.GetUserReadOnlyData(
        {
            PlayFabId: currentPlayerId,
            Keys: [
                "tower_data_1",
                "tower_last_timestamp_second"
            ]
        }
    );

    // Build my tower data
    let myTowerDataList = GetTowerDataList(getMyReadOnlyDataResult); 

    // Get target floor
    let targetFloor = myTowerDataList[0].Floor + 1;
    if(targetFloor <= 1 || targetFloor > 11)
        return toReturn;

    // Last timestamp for settlement
    let myLastTimestampSecond = 0;
    if(getMyReadOnlyDataResult.Data["tower_last_timestamp_second"] != null)
        myLastTimestampSecond = parseInt(getMyReadOnlyDataResult.Data["tower_last_timestamp_second"].Value);

    if(didWin)
    {
        // Need to swap the floor of tower and the opponent's
        // Opponent's team goes to (target_floor - 1)
        // My team goes to target_floor

        // Need to do settlement before changing the floors
        opponentTowerDataList = ProcessSettlementTowerReward(opponentTowerDataList, opponentLastTimestampSecond);
        myTowerDataList = ProcessSettlementTowerReward(myTowerDataList, myLastTimestampSecond);

        // Change opponent team's floor
        opponentTowerDataList[0].Floor = targetFloor - 1;
        
        // Update leaderboard for opponent
        UpdateTowerLeaderboard(serverIdModifier, opponentTowerDataList, opponentId);

        // Change my team's floor
        myTowerDataList[0].Floor = targetFloor;
        
        // Update leaderboard for tower
        UpdateTowerLeaderboard(serverIdModifier, myTowerDataList, currentPlayerId);
    }

    let currentTimestampSecond = Math.floor(+ new Date() / (1000));

    // Save the battle record of opponent
    let getMyAccountInfoResult = server.GetUserAccountInfo({
        PlayFabId: currentPlayerId
    });
    opponentTowerDataList[0].BattleRecord.push({
        "Name": getMyAccountInfoResult.UserInfo.TitleInfo.DisplayName,
        "Win": !didWin,
        "Floor": opponentTowerDataList[0].Floor,
        "IsDefense": true,
        "TimeStamp": Math.floor(currentTimestampSecond / 60)
    });
    if(opponentTowerDataList[0].BattleRecord.length > 15)
        opponentTowerDataList[0].BattleRecord.shift();

    // Save the battle record of tower
    let getOpponetAccountInfoResult = server.GetUserAccountInfo({
        PlayFabId: opponentId
    });
    myTowerDataList[0].BattleRecord.push({
        "Name": getOpponetAccountInfoResult.UserInfo.TitleInfo.DisplayName,
        "Win": didWin,
        "Floor": myTowerDataList[0].Floor,
        "IsDefense": false,
        "TimeStamp": Math.floor(currentTimestampSecond / 60)
    });
    if(myTowerDataList[0].BattleRecord.length > 15)
        myTowerDataList[0].BattleRecord.shift();
    
    // Update opponent & my read only data
    if(didWin)
    {
        let updateOpponentReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: opponentId,
            Data: {
                "tower_data_1": unescape(JSON.stringify(opponentTowerDataList[0])),
                "tower_last_timestamp_second": currentTimestampSecond.toString()
            },
            Permission : "Public"
        });

        let updateMyReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "tower_data_1": unescape(JSON.stringify(myTowerDataList[0])),
                "tower_last_timestamp_second": currentTimestampSecond.toString()
            },
            Permission : "Public"
        });
    }
    else
    {
        let updateOpponentReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: opponentId,
            Data: {
                "tower_data_1": unescape(JSON.stringify(opponentTowerDataList[0])),
            },
            Permission : "Public"
        });

        let updateMyReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "tower_data_1": unescape(JSON.stringify(myTowerDataList[0])),
            },
            Permission : "Public"
        });
    }

    toReturn.error = false;
    return toReturn;
}

handlers["towerBattleEnd"] = TowerBattleEnd;

interface ITowerAdvanceWithNoBattle{
    error: boolean;
    collectedGems: number;
    collectedFeathers: number;
    currentFloor: number;
    secondLeft: number;
    rewardRate: number;
    rewardFeatherRate: number;
    defenceRecords: any[];
} 

let TowerAdvanceWithNoBattle = function (args: any, context: IPlayFabContext): ITowerAdvanceWithNoBattle
{
    let toReturn: ITowerAdvanceWithNoBattle = {error: true, collectedGems: 0, collectedFeathers: 0, currentFloor: 0, secondLeft: 0, rewardRate: 0, rewardFeatherRate: 0, defenceRecords: []};

    let serverIdModifier = GetServerIdModifier(currentPlayerId);
    
    // Get my read only data
    let getMyReadOnlyDataResult = server.GetUserReadOnlyData(
        {
            PlayFabId: currentPlayerId,
            Keys: [
                "tower_data_1",
                "tower_last_timestamp_second"
            ]
        }
    );

    // Build my tower data
    let myTowerDataList = GetTowerDataList(getMyReadOnlyDataResult); 
    
    // Check floor
    if(myTowerDataList[0].Floor >= 11 || myTowerDataList[0].Floor <= 0)
        return toReturn;

    // Last timestamp for settlement
    let myLastTimestampSecond = 0;
    if(getMyReadOnlyDataResult.Data["tower_last_timestamp_second"] != null)
        myLastTimestampSecond = parseInt(getMyReadOnlyDataResult.Data["tower_last_timestamp_second"].Value);

    // Settlement
    myTowerDataList = ProcessSettlementTowerReward(myTowerDataList, myLastTimestampSecond);

    // Change my team's floor
    myTowerDataList[0].Floor = myTowerDataList[0].Floor + 1;
        
    // Update leaderboard for tower
    UpdateTowerLeaderboard(serverIdModifier, myTowerDataList, currentPlayerId);

    // Check timestamp
    let currentTimestampSecond = Math.floor(+ new Date() / (1000));

    // Save the battle record of tower
    myTowerDataList[0].BattleRecord.push({
        "Name": "",
        "Win": true,
        "Floor": myTowerDataList[0].Floor,
        "IsDefense": false,
        "TimeStamp": Math.floor(currentTimestampSecond / 60)
    });
    if(myTowerDataList[0].BattleRecord.length > 15)
        myTowerDataList[0].BattleRecord.shift();
    
    let updateMyReadOnlyDataResult = server.UpdateUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Data: {
            "tower_data_1": unescape(JSON.stringify(myTowerDataList[0])),
            "tower_last_timestamp_second": currentTimestampSecond.toString()
        },
        Permission : "Public"
    });
    
    toReturn.collectedGems = myTowerDataList[0].CollectedGems;
    toReturn.collectedFeathers = myTowerDataList[0].CollectedFeathers;
    toReturn.currentFloor = myTowerDataList[0].Floor;
    toReturn.secondLeft = Math.floor(Math.max(myTowerDataList[0].DeployedTimestamp + 14400 - currentTimestampSecond, 0));
    toReturn.defenceRecords = myTowerDataList[0].BattleRecord;
    toReturn.rewardRate = GetTowerRewardRate(toReturn.currentFloor);
    toReturn.rewardFeatherRate = GetTowerRewardFeatherRate(toReturn.currentFloor);

    toReturn.error = false;
    return toReturn;
}

handlers["towerAdvanceWithNoBattle"] = TowerAdvanceWithNoBattle;

interface ITowerRemoveFeather{
    error: boolean;
}

let TowerRemoveFeather = function (args: any, context: IPlayFabContext): ITowerRemoveFeather{
    let toReturn: ITowerRemoveFeather = {error: true};

    // Get user inventory
    let currentInventory = server.GetUserInventory(
        {
            PlayFabId: currentPlayerId
        }
    );

    log.info(currentInventory.VirtualCurrency["FE"].toString());

    if(currentInventory.VirtualCurrency["FE"] != 0)
        SubtractVirtualCurrency("FE", currentInventory.VirtualCurrency["FE"]);

    toReturn.error = false;
    return toReturn;
}

handlers["towerRemoveFeather"] = TowerRemoveFeather;

///////////////////////// HELPER FUNCTIONS (NOT DIRECTLY CALLABLE FROM THE CLIENT) /////////////////////////

function UpdateTowerLeaderboard(serverIdModifier: string, towerDataList: any[], playerId: string)
{
    let floor2Count = 0;
    let floor3Count = 0;
    let floor4Count = 0;
    let floor5Count = 0;
    let floor6Count = 0;
    let floor7Count = 0;
    let floor8Count = 0;
    let floor9Count = 0;
    let floor10Count = 0;
    let floor11Count = 0;

    let currentTimestampSecond = Math.floor(+ new Date() / (1000));

    let floorToCheck = 0;
    let timeStampToCheck = 0;
    if(towerDataList[0] != null)
    {
        floorToCheck = towerDataList[0].Floor;
        timeStampToCheck = towerDataList[0].DeployedTimestamp;
    }

    if(currentTimestampSecond <= timeStampToCheck + 14400)
    {
        if(floorToCheck == 2)
            floor2Count += Math.floor(timeStampToCheck / 60);
        else if(floorToCheck == 3)
            floor3Count += Math.floor(timeStampToCheck / 60);
        else if(floorToCheck == 4)
            floor4Count += Math.floor(timeStampToCheck / 60);
        else if(floorToCheck == 5)
            floor5Count += Math.floor(timeStampToCheck / 60);
        else if(floorToCheck == 6)
            floor6Count += Math.floor(timeStampToCheck / 60);
        else if(floorToCheck == 7)
            floor7Count += Math.floor(timeStampToCheck / 60);
        else if(floorToCheck == 8)
            floor8Count += Math.floor(timeStampToCheck / 60);
        else if(floorToCheck == 9)
            floor9Count += Math.floor(timeStampToCheck / 60);
        else if(floorToCheck == 10)
            floor10Count += Math.floor(timeStampToCheck / 60);
        else if(floorToCheck == 11)
            floor11Count += Math.floor(timeStampToCheck / 60);
    }

    // Update floor information
    server.UpdatePlayerStatistics({
        PlayFabId: playerId,
        Statistics: [
            {
                "StatisticName": "tower_floor_2" + serverIdModifier,
                "Value": floor2Count
            },
            {
                "StatisticName": "tower_floor_3" + serverIdModifier,
                "Value": floor3Count
            },
            {
                "StatisticName": "tower_floor_4" + serverIdModifier,
                "Value": floor4Count
            },
            {
                "StatisticName": "tower_floor_5" + serverIdModifier,
                "Value": floor5Count
            },
            {
                "StatisticName": "tower_floor_6" + serverIdModifier,
                "Value": floor6Count
            },
            {
                "StatisticName": "tower_floor_7" + serverIdModifier,
                "Value": floor7Count
            },
            {
                "StatisticName": "tower_floor_8" + serverIdModifier,
                "Value": floor8Count
            },
            {
                "StatisticName": "tower_floor_9" + serverIdModifier,
                "Value": floor9Count
            },
            {
                "StatisticName": "tower_floor_10" + serverIdModifier,
                "Value": floor10Count
            },
            {
                "StatisticName": "tower_floor_11" + serverIdModifier,
                "Value": floor11Count
            }
        ]
    });
}

function ProcessSettlementTowerReward(towerDataList: any[], lastTimestampSecond: number): any[]{
    
    let currentTimestampSecond = Math.floor(+ new Date() / (1000));

    if(lastTimestampSecond != 0 && currentTimestampSecond > lastTimestampSecond)
    {
        let passedSeconds = Math.max(Math.min(currentTimestampSecond - lastTimestampSecond, 14400), 0);

        if(towerDataList[0].DeployedTimestamp + 14400 <= currentTimestampSecond)
            passedSeconds = Math.max(Math.min(towerDataList[0].DeployedTimestamp + 14400 - lastTimestampSecond, 14400), 0);

        let reward = GetTowerRewardRate(towerDataList[0].Floor) * passedSeconds / 3600;
        // log.info("Reward is " + (reward) + " for " + (passedSeconds) + " seconds.");

        let rewardFeather = GetTowerRewardFeatherRate(towerDataList[0].Floor) * passedSeconds / 3600;
        // log.info("Reward is " + (reward) + " for " + (passedSeconds) + " seconds.");

        towerDataList[0].CollectedGems += reward;
        towerDataList[0].CollectedFeathers += rewardFeather;
    }

    return towerDataList;
}

function GetTowerDataList(readOnlyDataResult: PlayFabServerModels.GetUserDataResult): any[]{

    let towerDataList = [];
    let towerData = {};
    if(readOnlyDataResult.Data["tower_data_1"] != null)
    {
        towerData = JSON.parse(readOnlyDataResult.Data["tower_data_1"].Value);
        if(towerData["CollectedGems"] == null)
            towerData["CollectedGems"] = 0;
        if(towerData["CollectedFeathers"] == null)
            towerData["CollectedFeathers"] = 0;
    }
    else
    {
        towerData = {
            "Floor": 0,
            "DeployedTimestamp": 0,
            "CollectedGems": 0,
            "CollectedFeathers": 0,
            "BattleRecord": []
        };
    }

    towerDataList.push(towerData);

    return towerDataList;
}

function GetTowerRewardRate(floor: number){

    let rewardFactorByFloor = 0;
    if(floor == 1)
        rewardFactorByFloor = 15;
    else if(floor == 2)
        rewardFactorByFloor = 20;
    else if(floor == 3)
        rewardFactorByFloor = 25;
    else if(floor == 4)
        rewardFactorByFloor = 30;
    else if(floor == 5)
        rewardFactorByFloor = 35;
    else if(floor == 6)
        rewardFactorByFloor = 40;
    else if(floor == 7)
        rewardFactorByFloor = 45;
    else if(floor == 8)
        rewardFactorByFloor = 50;
    else if(floor == 9)
        rewardFactorByFloor = 55;
    else if(floor == 10)
        rewardFactorByFloor = 60;
    else if(floor == 11)
        rewardFactorByFloor = 70;
    // log.info("Reward Factor By Floor : " + (rewardFactorByFloor));

    return rewardFactorByFloor;
}

function GetTowerRewardFeatherRate(floor: number){

    let rewardFactorByFloor = 0;
    if(floor == 1)
        rewardFactorByFloor = 20;
    else if(floor == 2)
        rewardFactorByFloor = 25;
    else if(floor == 3)
        rewardFactorByFloor = 35;
    else if(floor == 4)
        rewardFactorByFloor = 45;
    else if(floor == 5)
        rewardFactorByFloor = 55;
    else if(floor == 6)
        rewardFactorByFloor = 65;
    else if(floor == 7)
        rewardFactorByFloor = 75;
    else if(floor == 8)
        rewardFactorByFloor = 85;
    else if(floor == 9)
        rewardFactorByFloor = 95;
    else if(floor == 10)
        rewardFactorByFloor = 105;
    else if(floor == 11)
        rewardFactorByFloor = 115;
    // log.info("Reward Factor By Floor : " + (rewardFactorByFloor));

    return rewardFactorByFloor;
}