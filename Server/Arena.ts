///////////////////////////////////////////////////////////////////////////////////////////////////////
// Dungeon Knight
///////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////// Cloud Script Handler Functions /////////////////////////

interface IArenaRefresh{
    error: boolean;
    dailyLimitLeft: number;
    currentSeason: number;
    position: number;
    score: number;
    lastSeasonPosition: number;
    lastSeasonScore: number;
    lastSeasonRewardGem: number;
    rankerList: any[];
    minAndroidVersion: string;
    miniOSVersion: string;
}

let ArenaRefresh = function (args: any, context: IPlayFabContext): IArenaRefresh{
    let toReturn: IArenaRefresh = {error: true, currentSeason: 0, dailyLimitLeft: 0, position: -1, score:1500, lastSeasonPosition: -1, lastSeasonScore: 0, 
        lastSeasonRewardGem: 0, rankerList: [], minAndroidVersion: "0.0.0", miniOSVersion: "0.0.0"};

    // Get title data
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "setting_json"        
        ]
    }).Data;

    if(getTitleDataResult["setting_json"] != null)
    {
        let convertedData = JSON.parse(getTitleDataResult["setting_json"]); 
        
        toReturn.minAndroidVersion = convertedData.MinAndroidVersion;
        toReturn.miniOSVersion = convertedData.MiniOSVersion;
    }

    let serverIdModifier = GetServerIdModifier(currentPlayerId);
    
    // Get my leaderboard
    let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
        PlayFabId: currentPlayerId,
        StatisticName: "arena" + serverIdModifier,
        MaxResultsCount: 1
    });

    let currentLeaderboardEntry = getLeaderboardAroundUserResult.Leaderboard[0];
    if(currentLeaderboardEntry != null)
    {
        toReturn.currentSeason = getLeaderboardAroundUserResult.Version;
        toReturn.position = currentLeaderboardEntry.Position;
        toReturn.score = currentLeaderboardEntry.StatValue;
        if(toReturn.score == 0)
            toReturn.score = 0;
    }

    // Get the leaderboard
    let getLeaderboardResult = server.GetLeaderboard({
        StatisticName: "arena" + serverIdModifier,
        StartPosition: 0,
        MaxResultsCount: 25
    });

    for(let i = 0; i < getLeaderboardResult.Leaderboard.length; i++)
    {
        toReturn.rankerList.push({
            "name": getLeaderboardResult.Leaderboard[i].DisplayName,
            "score": getLeaderboardResult.Leaderboard[i].StatValue,
            "position": getLeaderboardResult.Leaderboard[i].Position
        });
    }
    
    // Recent played season
    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "arena_reward_last_played_season"
        ]
    });

    // Receive reward if new season started
    if(getUserInternalDataResult.Data["arena_reward_last_played_season"] != null)
    {
        let lastSeason = parseInt(getUserInternalDataResult.Data["arena_reward_last_played_season"].Value);
        if(getLeaderboardAroundUserResult.Version == lastSeason + 1)
        {
            let rewardResult = ReceiveArenaReward(serverIdModifier, getUserInternalDataResult, getLeaderboardAroundUserResult.Version);
            toReturn.lastSeasonPosition = rewardResult.lastSeasonPosition;
            toReturn.lastSeasonScore = rewardResult.lastSeasonScore;
            toReturn.lastSeasonRewardGem = rewardResult.lastSeasonRewardGem;
            
            log.info("Received reward gems: " + rewardResult.lastSeasonRewardGem);
        }
    }

    // Check whether this player quits improperly
    let dodgeResult = UpdateScoreIfDodged(serverIdModifier);
    if(dodgeResult.updated)
        toReturn.score = dodgeResult.myNewScore;

    toReturn.dailyLimitLeft = GetLeftDailyLimit("arena", "0");

    toReturn.error = false;
    return toReturn;
}

handlers["arenaRefresh"] = ArenaRefresh;

interface IArenaRefresh2{
    error: boolean;
    dailyLimitLeft: number;
    currentSeason: number;
    position: number;
    score: number;
    lastSeasonPosition: number;
    lastSeasonScore: number;
    lastSeasonRewardGem: number;
    rankerList: any[];
    currentSeasonWeekly: number;
    star: number;
    lastSeasonStar: number;
    lastSeasonRewardGemWeekly: number;
    minAndroidVersion: string;
    miniOSVersion: string;
}

let ArenaRefresh2 = function (args: any, context: IPlayFabContext): IArenaRefresh2{
    let toReturn: IArenaRefresh2 = {error: true, 
        currentSeason: 0, dailyLimitLeft: 0, position: -1, score: 1500, lastSeasonPosition: -1, lastSeasonScore: 0, lastSeasonRewardGem: 0, rankerList: [], 
        minAndroidVersion: "0.0.0", miniOSVersion: "0.0.0",
        currentSeasonWeekly: 0, star: -1, lastSeasonStar: -1, lastSeasonRewardGemWeekly: 0
    };

    // Get title data
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "setting_json"        
        ]
    }).Data;

    if(getTitleDataResult["setting_json"] != null)
    {
        let convertedData = JSON.parse(getTitleDataResult["setting_json"]); 
        
        toReturn.minAndroidVersion = convertedData.MinAndroidVersion;
        toReturn.miniOSVersion = convertedData.MiniOSVersion;
    }

    let serverIdModifier = GetServerIdModifier(currentPlayerId);
    
    // Get my leaderboard
    let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
        PlayFabId: currentPlayerId,
        StatisticName: "arena" + serverIdModifier,
        MaxResultsCount: 1
    });
    let currentLeaderboardEntry = getLeaderboardAroundUserResult.Leaderboard[0];
    if(currentLeaderboardEntry != null)
    {
        toReturn.currentSeason = getLeaderboardAroundUserResult.Version;
        toReturn.position = currentLeaderboardEntry.Position;
        toReturn.score = currentLeaderboardEntry.StatValue;
        if(toReturn.score == 0)
            toReturn.score = 0;
    }

    // Get the leaderboard
    let getLeaderboardResult = server.GetLeaderboard({
        StatisticName: "arena" + serverIdModifier,
        StartPosition: 0,
        MaxResultsCount: 25
    });

    for(let i = 0; i < getLeaderboardResult.Leaderboard.length; i++)
    {
        toReturn.rankerList.push({
            "name": getLeaderboardResult.Leaderboard[i].DisplayName,
            "score": getLeaderboardResult.Leaderboard[i].StatValue,
            "position": getLeaderboardResult.Leaderboard[i].Position
        });
    }

    // Get my weekly leaderboard
    let getLeaderboardAroundUserWeeklyResult = server.GetLeaderboardAroundUser({
        PlayFabId: currentPlayerId,
        StatisticName: "star" + serverIdModifier,
        MaxResultsCount: 1
    });
    let currentLeaderboardEntryWeekly = getLeaderboardAroundUserWeeklyResult.Leaderboard[0];
    if(currentLeaderboardEntryWeekly != null)
    {
        toReturn.currentSeasonWeekly = getLeaderboardAroundUserWeeklyResult.Version;
        toReturn.star = currentLeaderboardEntryWeekly.StatValue;
        if(toReturn.star == 0)
            toReturn.star = 0;
    }
    
    // Recent played season
    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "arena_reward_last_played_season",
            "arena_reward_last_played_season_weekly"
        ]
    });

    // Receive reward if new season started
    if(getUserInternalDataResult.Data["arena_reward_last_played_season"] != null)
    {
        let lastSeason = parseInt(getUserInternalDataResult.Data["arena_reward_last_played_season"].Value);
        if(getLeaderboardAroundUserResult.Version == lastSeason + 1)
        {
            let rewardResult = ReceiveArenaReward(serverIdModifier, getUserInternalDataResult, getLeaderboardAroundUserResult.Version);
            toReturn.lastSeasonPosition = rewardResult.lastSeasonPosition;
            toReturn.lastSeasonScore = rewardResult.lastSeasonScore;
            toReturn.lastSeasonRewardGem = rewardResult.lastSeasonRewardGem;

            log.info("Received arena reward gems: " + rewardResult.lastSeasonRewardGem);
        }
    }

    // Receive reeward if new weekly season started
    if(getUserInternalDataResult.Data["arena_reward_last_played_season_weekly"] != null)
    {
        let lastSeasonWeekly = parseInt(getUserInternalDataResult.Data["arena_reward_last_played_season_weekly"].Value);
        if(getLeaderboardAroundUserWeeklyResult.Version == lastSeasonWeekly + 1)
        {
            let rewardResult = ReceiveArenaRewardWeekly(serverIdModifier, getUserInternalDataResult, getLeaderboardAroundUserWeeklyResult.Version);
            toReturn.lastSeasonStar = rewardResult.lastSeasonScore;
            toReturn.lastSeasonRewardGemWeekly = rewardResult.lastSeasonRewardGem;

            log.info("Received arena weekly reward gems: " + rewardResult.lastSeasonRewardGem);
        }
    }

    // Check whether this player quits improperly
    let dodgeResult = UpdateScoreIfDodged2(serverIdModifier);
    if(dodgeResult.updated)
    {
        toReturn.score = dodgeResult.myNewScore;
        toReturn.star = dodgeResult.myNewStar;
    }
        

    toReturn.dailyLimitLeft = GetLeftDailyLimit("arena", "0");

    toReturn.error = false;
    return toReturn;
}

handlers["arenaRefresh2"] = ArenaRefresh2;

interface IArenaStart{
    error: boolean;
    opponentUserData: string;
    opponentInventoryData: string;
    opponentBonusStat: number;
    reasonOfError: string;
}

let ArenaStart = function (args: any, context: IPlayFabContext): IArenaStart{
    
    let opponentId = "";

    let toReturn: IArenaStart = {error: true, opponentUserData: "", opponentInventoryData: "", opponentBonusStat: 0, reasonOfError: ""};

    if(args.opponentId)
        opponentId = args.opponentId;
    else
        return toReturn;

    // Check daily limit
    if(GetLeftDailyLimit("arena", "0") < 1)
    {
        log.info("Exceeded daily limit for arena!");
        toReturn.reasonOfError = "exceeded_daily_limit";
        return toReturn;
    }

    // Get opponent's data
    let opponentReadOnlyData = server.GetUserReadOnlyData({
        PlayFabId: opponentId,
        Keys: [
            "inventory_data",
            "user_data"
        ]
    }).Data;

    toReturn.opponentUserData = opponentReadOnlyData["user_data"].Value;
    toReturn.opponentInventoryData = opponentReadOnlyData["inventory_data"].Value;

    // Save that arena started, and also the id of opponent
    let updateUserInternalDataResult = server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: {
            "is_playing_arena": "true",
            "arena_opponent_id": opponentId
        }
    });

    // Get opponent inventory to get bonus stat
    let getUserInventoryResult = server.GetUserInventory(
        {
            PlayFabId: opponentId
        }
    );
    toReturn.opponentBonusStat = getUserInventoryResult.VirtualCurrency["BS"].valueOf();

    toReturn.error = false;
    return toReturn;
}

handlers["arenaStart"] = ArenaStart;

interface IArenaEnd{
    myNewScore: number;
    error: boolean;
}

let ArenaEnd = function (args: any, context: IPlayFabContext): IArenaEnd
{
    let toReturn: IArenaEnd = {error: true, myNewScore: 0};

    let didWin = false;
    if(args.didWin)
        didWin = args.didWin;

    let season = 0;
    if(args.season)
        season = args.season;

    let serverIdModifier = GetServerIdModifier(currentPlayerId);

    let isSuspiciousIfWin = false;
    if(args.isSuspiciousIfWin)
        isSuspiciousIfWin = args.isSuspiciousIfWin;

    if(isSuspiciousIfWin)
    {
        server.UpdatePlayerStatistics({
            PlayFabId: currentPlayerId,
            Statistics: [
                {
                    "StatisticName": "cheat_suspicious_win",
                    "Value": 1
                }
            ]
        });
    }

    // Check validity of the match. The match should have started before this API is called!
    let isValidCompletion = false;

    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "is_playing_arena",
            "arena_opponent_id"
        ]
    }).Data;

    if(getUserInternalDataResult["is_playing_arena"] && getUserInternalDataResult["is_playing_arena"].Value == "true")
        isValidCompletion = true;

    if(!isValidCompletion)
        return toReturn;

    // Use daily limit here
    if(!UseDailyLimitIfAvail("arena", "0", 1))
        return toReturn;
    
    let opponentId = "";
    if(getUserInternalDataResult["arena_opponent_id"])
        opponentId = getUserInternalDataResult["arena_opponent_id"].Value;

    // Update score with the result
    toReturn.myNewScore = UpdateScore(serverIdModifier, opponentId, didWin).myNewScore;

    // Reset is_playing_arena flag
    let updateUserInternalDataResult = server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: {
            "is_playing_arena": "false",
            "arena_reward_last_played_season": season.toString()
        }
    });

    toReturn.error = false;
    return toReturn;
}

handlers["arenaEnd"] = ArenaEnd;

let ArenaEnd2 = function (args: any, context: IPlayFabContext): IArenaEnd
{
    let toReturn: IArenaEnd = {error: true, myNewScore: 0};

    let didWin = false;
    if(args.didWin)
        didWin = args.didWin;

    let season = 0;
    if(args.season)
        season = args.season;

    let seasonWeekly = 0;
    if(args.seasonWeekly)
        seasonWeekly = args.seasonWeekly;

    let serverIdModifier = GetServerIdModifier(currentPlayerId);

    let isSuspiciousIfWin = false;
    if(args.isSuspiciousIfWin)
        isSuspiciousIfWin = args.isSuspiciousIfWin;

    if(isSuspiciousIfWin)
    {
        server.UpdatePlayerStatistics({
            PlayFabId: currentPlayerId,
            Statistics: [
                {
                    "StatisticName": "cheat_suspicious_win",
                    "Value": 1
                }
            ]
        });
    }

    // Check validity of the match. The match should have started before this API is called!
    let isValidCompletion = false;

    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "is_playing_arena",
            "arena_opponent_id",
            "arena_reward_last_played_season",
            "arena_reward_last_played_season_weekly"
        ]
    }).Data;

    if(getUserInternalDataResult["is_playing_arena"] && getUserInternalDataResult["is_playing_arena"].Value == "true")
        isValidCompletion = true;

    if(!isValidCompletion)
        return toReturn;

    // Use daily limit here
    if(!UseDailyLimitIfAvail("arena", "0", 1))
        return toReturn;
    
    let opponentId = "";
    if(getUserInternalDataResult["arena_opponent_id"])
        opponentId = getUserInternalDataResult["arena_opponent_id"].Value;

    // Update score with the result
    toReturn.myNewScore = UpdateScore2(serverIdModifier, opponentId, didWin).myNewScore;

    // For checking season
    if(getUserInternalDataResult["arena_reward_last_played_season"])
        season = Math.max(season, parseInt(getUserInternalDataResult["arena_reward_last_played_season"].Value));

    if(getUserInternalDataResult["arena_reward_last_played_season_weekly"])
        seasonWeekly = Math.max(seasonWeekly, parseInt(getUserInternalDataResult["arena_reward_last_played_season_weekly"].Value));

    // Reset is_playing_arena flag
    let updateUserInternalDataResult = server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: {
            "is_playing_arena": "false",
            "arena_reward_last_played_season": season.toString(),
            "arena_reward_last_played_season_weekly": seasonWeekly.toString()
        }
    });

    toReturn.error = false;
    return toReturn;
}

handlers["arenaEnd2"] = ArenaEnd2;

///////////////////////// HELPER FUNCTIONS (NOT DIRECTLY CALLABLE FROM THE CLIENT) /////////////////////////

interface IUpdateScore{
    updated: boolean;
    myNewScore: number;
}

interface IUpdateScore2{
    updated: boolean;
    myNewScore: number;
    myNewStar: number;
}

function UpdateScoreIfDodged(serverIdModifier: string): IUpdateScore{

    let toReturn: IUpdateScore = {updated: false, myNewScore: 0};

    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "is_playing_arena",
            "arena_opponent_id"
        ]
    }).Data;

    if(getUserInternalDataResult["is_playing_arena"]!= null && getUserInternalDataResult["is_playing_arena"].Value == "true")
        toReturn.updated = true;

    // Give penalty for quitting battle abnormally last time (eg. Turn off the phone)
    if(toReturn.updated)
    {
        // Use daily limit here
        UseDailyLimitIfAvail("arena", "0", 1);
        
        // Update score
        toReturn = UpdateScore(serverIdModifier, getUserInternalDataResult["arena_opponent_id"].Value, false);

        // Set as not playing
        let updateUserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: {
                "is_playing_arena": "false"
            }
        });
    }

    return toReturn;
}

function UpdateScoreIfDodged2(serverIdModifier: string): IUpdateScore2{

    let toReturn: IUpdateScore2 = {updated: false, myNewScore: 0, myNewStar: 0};

    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "is_playing_arena",
            "arena_opponent_id"
        ]
    }).Data;

    if(getUserInternalDataResult["is_playing_arena"]!= null && getUserInternalDataResult["is_playing_arena"].Value == "true")
        toReturn.updated = true;

    // Give penalty for quitting battle abnormally last time (eg. Turn off the phone)
    if(toReturn.updated)
    {
        // Use daily limit here
        UseDailyLimitIfAvail("arena", "0", 1);
        
        // Update score
        toReturn = UpdateScore2(serverIdModifier, getUserInternalDataResult["arena_opponent_id"].Value, false);

        // Set as not playing
        let updateUserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: {
                "is_playing_arena": "false"
            }
        });
    }

    return toReturn;
}

function UpdateScore(serverIdModifier:string, opponentId: string, didWin: boolean): IUpdateScore{

    let toReturn: IUpdateScore = {updated: false, myNewScore: 0};

    let myScore = GetScoreOfPlayer(serverIdModifier, currentPlayerId);
    let opponentScore = GetScoreOfPlayer(serverIdModifier, opponentId);

    let scoreBonusByStageRanking = 0;

    // Check score bonus by stage ranking
    if(didWin)
    {
        let stageRanking = 0;
        let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
            PlayFabId: currentPlayerId,
            StatisticName: "ranking" + serverIdModifier,
            MaxResultsCount: 1
        });
        let currentLeaderboardEntry = getLeaderboardAroundUserResult.Leaderboard[0];
        if(currentLeaderboardEntry != null)
            stageRanking = currentLeaderboardEntry.Position + 1;

        if(stageRanking != 0)
        {
            if(stageRanking <= 10)
                scoreBonusByStageRanking = 7;
            else if(stageRanking <= 30)
                scoreBonusByStageRanking = 5;
            else if(stageRanking <= 100)
                scoreBonusByStageRanking = 3;
            else if(stageRanking <= 1000)
                scoreBonusByStageRanking = 1;
        }
    }
    
    if(didWin)
    {
        toReturn.myNewScore = myScore + 10 + scoreBonusByStageRanking;
        server.UpdatePlayerStatistics({
            PlayFabId: currentPlayerId,
            Statistics: [
                {
                    "StatisticName": "arena" + serverIdModifier,
                    "Value": 10 + scoreBonusByStageRanking
                },
                {
                    "StatisticName": "arena_all_season" + serverIdModifier,
                    "Value": 10 + scoreBonusByStageRanking
                }
            ]
        });

        if(opponentId != "" && opponentScore > 100)
        {
            server.UpdatePlayerStatistics({
                PlayFabId: opponentId,
                Statistics: [
                    {
                        "StatisticName": "arena" + serverIdModifier,
                        "Value": Math.max(100 - opponentScore, -3)
                    },
                    {
                        "StatisticName": "arena_all_season" + serverIdModifier,
                        "Value": Math.max(100 - opponentScore, -3)
                    }
                ]
            });
        }
    }
    else
    {
        toReturn.myNewScore = Math.max(myScore - 3, 1);
        if(myScore > 1)
        {
            server.UpdatePlayerStatistics({
                PlayFabId: currentPlayerId,
                Statistics: [
                    {
                        "StatisticName": "arena" + serverIdModifier,
                        "Value": Math.max(1 - myScore, -3)
                    },
                    {
                        "StatisticName": "arena_all_season" + serverIdModifier,
                        "Value": Math.max(1 - myScore, -3)
                    }
                ]
            });
        }

        if(opponentId != "")
        {
            server.UpdatePlayerStatistics({
                PlayFabId: opponentId,
                Statistics: [
                    {
                        "StatisticName": "arena" + serverIdModifier,
                        "Value": 10
                    },
                    {
                        "StatisticName": "arena_all_season" + serverIdModifier,
                        "Value": 10
                    }
                ]
            });
        }
    }

    toReturn.updated = true;
    return toReturn;
}

function UpdateScore2(serverIdModifier:string, opponentId: string, didWin: boolean): IUpdateScore2{

    let toReturn: IUpdateScore2 = {updated: false, myNewScore: 0, myNewStar: 0};

    let myScore = GetScoreOfPlayer(serverIdModifier, currentPlayerId);
    let opponentScore = GetScoreOfPlayer(serverIdModifier, opponentId);
    let scoreBonusByStageRanking = 0;

    let myStar = GetStarOfPlayer(serverIdModifier, currentPlayerId);
    let starByStageRanking = 1;

    // Check score bonus by stage ranking
    
    let stageRanking = 0;
    let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
        PlayFabId: currentPlayerId,
        StatisticName: "ranking" + serverIdModifier,
        MaxResultsCount: 1
    });
    let currentLeaderboardEntry = getLeaderboardAroundUserResult.Leaderboard[0];
    if(currentLeaderboardEntry != null)
        stageRanking = currentLeaderboardEntry.Position + 1;

    if(didWin)
    {
        if(stageRanking != 0)
        {
            if(stageRanking <= 10)
                scoreBonusByStageRanking = 7;
            else if(stageRanking <= 30)
                scoreBonusByStageRanking = 5;
            else if(stageRanking <= 100)
                scoreBonusByStageRanking = 3;
            else if(stageRanking <= 1000)
                scoreBonusByStageRanking = 1;
        }

        if(stageRanking != 0)
        {
            if(stageRanking <= 50)
                starByStageRanking = 11;
            else if(stageRanking <= 100)
                starByStageRanking = 9;
            else if(stageRanking <= 500)
                starByStageRanking = 7;
            else if(stageRanking <= 1000)
                starByStageRanking = 5;
            else if(stageRanking <= 5000)
                starByStageRanking = 3;
            else if(stageRanking <= 10000)
                starByStageRanking = 2;
        }
    }
    else
    {
        if(stageRanking != 0)
        {
            if(stageRanking <= 100)
                starByStageRanking = 5;
            else if(stageRanking <= 1000)
                starByStageRanking = 3;
            else if(stageRanking <= 10000)
                starByStageRanking = 1;
        }
    }

    if(didWin)
    {
        toReturn.myNewScore = myScore + 10 + scoreBonusByStageRanking;
        toReturn.myNewStar = myStar + starByStageRanking;

        server.UpdatePlayerStatistics({
            PlayFabId: currentPlayerId,
            Statistics: [
                {
                    "StatisticName": "arena" + serverIdModifier,
                    "Value": 10 + scoreBonusByStageRanking
                },
                {
                    "StatisticName": "arena_all_season" + serverIdModifier,
                    "Value": 10 + scoreBonusByStageRanking
                },
                {
                    "StatisticName": "star" + serverIdModifier,
                    "Value": starByStageRanking
                }
            ]
        });

        if(opponentId != "" && opponentScore > 100)
        {
            server.UpdatePlayerStatistics({
                PlayFabId: opponentId,
                Statistics: [
                    {
                        "StatisticName": "arena" + serverIdModifier,
                        "Value": Math.max(100 - opponentScore, -3)
                    },
                    {
                        "StatisticName": "arena_all_season" + serverIdModifier,
                        "Value": Math.max(100 - opponentScore, -3)
                    }
                ]
            });
        }
    }
    else
    {
        toReturn.myNewScore = Math.max(myScore - 3, 1);
        toReturn.myNewStar = myStar + starByStageRanking;
        if(myScore > 1)
        {
            server.UpdatePlayerStatistics({
                PlayFabId: currentPlayerId,
                Statistics: [
                    {
                        "StatisticName": "arena" + serverIdModifier,
                        "Value": Math.max(1 - myScore, -3)
                    },
                    {
                        "StatisticName": "arena_all_season" + serverIdModifier,
                        "Value": Math.max(1 - myScore, -3)
                    },
                    {
                        "StatisticName": "star" + serverIdModifier,
                        "Value": starByStageRanking
                    }
                ]
            });
        }
        else
        {
            server.UpdatePlayerStatistics({
                PlayFabId: currentPlayerId,
                Statistics: [
                    {
                        "StatisticName": "star" + serverIdModifier,
                        "Value": starByStageRanking
                    }
                ]
            });
        }

        if(opponentId != "")
        {
            server.UpdatePlayerStatistics({
                PlayFabId: opponentId,
                Statistics: [
                    {
                        "StatisticName": "arena" + serverIdModifier,
                        "Value": 10
                    },
                    {
                        "StatisticName": "arena_all_season" + serverIdModifier,
                        "Value": 10
                    }
                ]
            });
        }
    }

    toReturn.updated = true;
    return toReturn;
}

function GetScoreOfPlayer(serverIdModifier: string, playerId: string): number{
    // Get current leaderboard data object
    let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
        PlayFabId: playerId,
        StatisticName: "arena" + serverIdModifier,
        MaxResultsCount: 1
    });

    // Use first element
    let currentLeaderboardEntry = getLeaderboardAroundUserResult.Leaderboard[0];

    if(currentLeaderboardEntry != null)
    {
        // Get current score
        let currentScore = currentLeaderboardEntry.StatValue;
        if(currentScore == 0)
            currentScore = 0;
        return currentScore;
    }
    else
    {
        return 0;
    }  
}

function GetStarOfPlayer(serverIdModifier: string, playerId: string): number{
    // Get current leaderboard data object
    let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
        PlayFabId: playerId,
        StatisticName: "star" + serverIdModifier,
        MaxResultsCount: 1
    });

    // Use first element
    let currentLeaderboardEntry = getLeaderboardAroundUserResult.Leaderboard[0];

    if(currentLeaderboardEntry != null)
    {
        // Get current star
        let currentStar = currentLeaderboardEntry.StatValue;
        if(currentStar == 0)
            currentStar = 0;
        return currentStar;
    }
    else
    {
        return 0;
    }  
}

interface IReceiveArenaReward{
    lastSeasonPosition: number;
    lastSeasonScore: number;
    lastSeasonRewardGem: number;
}

function ReceiveArenaReward(serverIdModifier: string, internalDataResult: PlayFabServerModels.GetUserDataResult, currentSeason: number): IReceiveArenaReward{
    
    let toReturn: IReceiveArenaReward = {lastSeasonPosition: 0, lastSeasonScore: 0, lastSeasonRewardGem: 0};

    if(internalDataResult.Data["arena_reward_last_played_season"] != null)
    {
        // Receive reward if current season is higher than last received season
        let lastPlayedSeason = parseInt(internalDataResult.Data["arena_reward_last_played_season"].Value);
        if(currentSeason == lastPlayedSeason + 1)
        {
            let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
                PlayFabId: currentPlayerId,
                StatisticName: "arena" + serverIdModifier,
                MaxResultsCount: 1,
                Version: lastPlayedSeason
            });

            if(getLeaderboardAroundUserResult.Leaderboard[0] != null) // Only give reward when play record exists
            {
                let lastSeasonLeaderboardEntry = getLeaderboardAroundUserResult.Leaderboard[0];
                toReturn.lastSeasonPosition = lastSeasonLeaderboardEntry.Position;
                toReturn.lastSeasonScore = lastSeasonLeaderboardEntry.StatValue;
            }

            if(toReturn.lastSeasonScore != 0)
            {
                // Give reward based on last season's performance
                if(toReturn.lastSeasonPosition == 0)
                {
                    toReturn.lastSeasonRewardGem = 2500;
                }
                else if(toReturn.lastSeasonPosition == 1)
                {
                    toReturn.lastSeasonRewardGem = 2000;
                }
                else if(toReturn.lastSeasonPosition == 2)
                {
                    toReturn.lastSeasonRewardGem = 1500;
                }
                else if(toReturn.lastSeasonPosition < 10)
                {
                    toReturn.lastSeasonRewardGem = 1000;
                }
                else if(toReturn.lastSeasonPosition < 50)
                {
                    toReturn.lastSeasonRewardGem = 750;
                }
                else if(toReturn.lastSeasonPosition < 100)
                {
                    toReturn.lastSeasonRewardGem = 250;
                }
                else
                {
                    toReturn.lastSeasonRewardGem = 50;
                }

                AddVirtualCurrency("GE", toReturn.lastSeasonRewardGem);
            }
            
            let updateUserInternalDataResult = server.UpdateUserInternalData({
                PlayFabId: currentPlayerId,
                Data: {
                    "arena_reward_last_played_season": currentSeason.toString()
                }
            });
        }
    }
    else
    {
        let updateUserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: {
                "arena_reward_last_played_season": currentSeason.toString()
            }
        });
    }

    return toReturn;
}

function ReceiveArenaRewardWeekly(serverIdModifier: string, internalDataResult: PlayFabServerModels.GetUserDataResult, currentSeason: number): IReceiveArenaReward{
    
    let toReturn: IReceiveArenaReward = {lastSeasonPosition: 0, lastSeasonScore: -1, lastSeasonRewardGem: 0};

    if(internalDataResult.Data["arena_reward_last_played_season_weekly"] != null)
    {
        // Receive reward if current season is higher than last received season
        let lastPlayedSeason = parseInt(internalDataResult.Data["arena_reward_last_played_season_weekly"].Value);
        if(currentSeason == lastPlayedSeason + 1)
        {
            let getLeaderboardAroundUserResult = server.GetLeaderboardAroundUser({
                PlayFabId: currentPlayerId,
                StatisticName: "star" + serverIdModifier,
                MaxResultsCount: 1,
                Version: lastPlayedSeason
            });

            if(getLeaderboardAroundUserResult.Leaderboard[0] != null) // Only give reward when play record exists
            {
                let lastSeasonLeaderboardEntry = getLeaderboardAroundUserResult.Leaderboard[0];
                toReturn.lastSeasonScore = lastSeasonLeaderboardEntry.StatValue;
            }

            if(toReturn.lastSeasonScore != -1)
            {
                // Give reward based on last season's performance
                if(toReturn.lastSeasonScore >= 2000) // Grandmaster
                {
                    toReturn.lastSeasonRewardGem = 5000;
                }
                else if(toReturn.lastSeasonScore >= 1500) // Master
                {
                    toReturn.lastSeasonRewardGem = 3000;
                }
                else if(toReturn.lastSeasonScore >= 1000) // Diamond
                {
                    toReturn.lastSeasonRewardGem = 2000;
                }
                else if(toReturn.lastSeasonScore >= 750) // Platinum
                {
                    toReturn.lastSeasonRewardGem = 1500;
                }
                else if(toReturn.lastSeasonScore >= 500) // Gold
                {
                    toReturn.lastSeasonRewardGem = 1000;
                }
                else if(toReturn.lastSeasonScore >= 250) // Silver
                {
                    toReturn.lastSeasonRewardGem = 750;
                }
                else // Bronze
                {
                    toReturn.lastSeasonRewardGem = 500;
                }

                AddVirtualCurrency("GE", toReturn.lastSeasonRewardGem);
            }
            
            let updateUserInternalDataResult = server.UpdateUserInternalData({
                PlayFabId: currentPlayerId,
                Data: {
                    "arena_reward_last_played_season_weekly": currentSeason.toString()
                }
            });
        }
    }
    else
    {
        let updateUserInternalDataResult = server.UpdateUserInternalData({
            PlayFabId: currentPlayerId,
            Data: {
                "arena_reward_last_played_season_weekly": currentSeason.toString()
            }
        });
    }

    return toReturn;
}