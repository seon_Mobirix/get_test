///////////////////////////////////////////////////////////////////////////////////////////////////////
// Dungeon Knight
///////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////// Cloud Script Handler Functions /////////////////////////

interface IRaidRefresh{
    error: boolean;
    dailyLimitLeft: number;
    dayPassed: number;
    hourPassed: number;
    currentTimestampHour: number;
    currentTimestampDay: number;
    minAndroidVersion: string;
    miniOSVersion: string;
    goldReward: string;
    heartReward: string;
    gemReward: number;
    raidTicketReward: number;
}

let RaidRefresh = function (args: any, context: IPlayFabContext): IRaidRefresh{
    let toReturn: IRaidRefresh = {error: true, dailyLimitLeft: 0, dayPassed: 0, hourPassed: 0, currentTimestampHour: 0, currentTimestampDay: 0, minAndroidVersion: "0.0.0", miniOSVersion: "0.0.0",
    goldReward: "0", heartReward: "0", gemReward: 0, raidTicketReward: 0};

    let goldRewardWeight = "0";
    let heartRewardWeight = "0";
    let gemRewardWeight = 0;
    let raidTicketRewardWeight = 0;

    // Get data related with raid trohpy reward
    if(args && args.goldRewardWeight)
        goldRewardWeight = args.goldRewardWeight;
    if(args && args.heartRewardWeight)
        heartRewardWeight = args.heartRewardWeight;
    if(args && args.gemRewardWeight)
        gemRewardWeight = Number(args.gemRewardWeight);
    if(args && args.raidTicketRewardWeight)
        raidTicketRewardWeight = Number(args.raidTicketRewardWeight);

    // Get title data
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "setting_json"
        ]
    }).Data;

    if(getTitleDataResult["setting_json"] != null)
    {
        let convertedData = JSON.parse(getTitleDataResult["setting_json"]); 
        
        toReturn.minAndroidVersion = convertedData.MinAndroidVersion;
        toReturn.miniOSVersion = convertedData.MiniOSVersion;
    }

    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: [
            "last_raid_timestamp_second", 
            "last_raid_timestamp_hour", 
            "last_raid_timestamp_day", 
            "last_confirmed_raid_timestamp_hour", 
            "last_confirmed_raid_timestamp_day"
        ]
    });

    let lastTimestampHour = -1;
    if(getUserReadOnlyDataResult.Data["last_raid_timestamp_hour"] != null)
        lastTimestampHour = parseInt(getUserReadOnlyDataResult.Data["last_raid_timestamp_hour"].Value);
    let currentTimestampHour = Math.floor(+ new Date() / (1000 * 3600));
    toReturn.hourPassed = Math.min(currentTimestampHour - lastTimestampHour, 24);

    let lastTimestampDay = -1;
    if(getUserReadOnlyDataResult.Data["last_raid_timestamp_day"] != null)
        lastTimestampDay = parseInt(getUserReadOnlyDataResult.Data["last_raid_timestamp_day"].Value);
    let currentTimestampDay = Math.floor(+ new Date() / (1000 * 3600 * 24));
    toReturn.dayPassed = Math.min(currentTimestampDay - lastTimestampDay, 1);

    let lastTimestampSecond = -1;
    if(getUserReadOnlyDataResult.Data["last_raid_timestamp_second"] != null)
        lastTimestampSecond = parseInt(getUserReadOnlyDataResult.Data["last_raid_timestamp_second"].Value);
    let currentTimestampSecond = Math.floor(+ new Date() / (1000));
    let halfMinutePassed = false;
    if(currentTimestampSecond > lastTimestampSecond + 30)
        halfMinutePassed = true;

    let dataToUse = {};
    dataToUse["last_raid_timestamp_hour"] = currentTimestampHour.toString();
    dataToUse["last_raid_timestamp_day"] = currentTimestampDay.toString();

    if(halfMinutePassed) // When more than a half minute passed
    {
        // Save timestamp
        dataToUse["last_raid_timestamp_second"] = currentTimestampSecond.toString();

        let lastSuccessfulTimestampHour = -1;
        if(getUserReadOnlyDataResult.Data["last_confirmed_raid_timestamp_hour"] != null)
            lastSuccessfulTimestampHour = parseInt(getUserReadOnlyDataResult.Data["last_confirmed_raid_timestamp_hour"].Value);
        let lastSuccessfulTimestampDay = -1;
        if(getUserReadOnlyDataResult.Data["last_confirmed_raid_timestamp_day"] != null)
            lastSuccessfulTimestampDay = parseInt(getUserReadOnlyDataResult.Data["last_confirmed_raid_timestamp_day"].Value);
        
        // This time save these values
        dataToUse["last_confirmed_raid_timestamp_hour"] = lastTimestampHour.toString();
        dataToUse["last_confirmed_raid_timestamp_day"] = lastTimestampDay.toString();
        
        // If mismatch of timestamps happens, this means we need to roll back those values (to protect players)
        // (and also not rollbacked before!)
        if(lastSuccessfulTimestampHour != -1 && lastSuccessfulTimestampDay != -1)
        {
            if(lastTimestampHour != lastSuccessfulTimestampHour || lastTimestampDay != lastSuccessfulTimestampDay)
            {
                if(UseDailyLimitIfAvail("raid_rollback", "0", 1, false))
                {
                    log.info("Need to rollback raid reward timestamps");

                    // Run rollback logic
                    lastTimestampHour = Math.min(lastTimestampHour, Math.max(lastTimestampHour - 24, lastSuccessfulTimestampHour));
                    lastTimestampDay =  Math.min(lastTimestampDay, Math.max(lastTimestampDay - 1, lastSuccessfulTimestampDay));
                    toReturn.hourPassed = Math.min(currentTimestampHour - lastTimestampHour, 24);
                    toReturn.dayPassed = Math.min(currentTimestampDay - lastTimestampDay, 1);
                }

                server.UpdatePlayerStatistics({
                    PlayFabId: currentPlayerId,
                    Statistics: [
                        {
                            "StatisticName": "raid_rollback",
                            "Value": 1
                        }
                    ]
                });
            }
        }
    }

    // Save progress anyway
    let updateuserReadOnlyDataResult = server.UpdateUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Data: dataToUse
    });

    toReturn.currentTimestampHour = currentTimestampHour;
    toReturn.currentTimestampDay = currentTimestampDay;
    
    toReturn.dailyLimitLeft = GetLeftDailyLimit("raid", "0", true);
    toReturn.error = false;

    // Values for just logging purpose
    if(toReturn.hourPassed > 0 || toReturn.dayPassed > 0)
    {
        toReturn.goldReward = (BigInt(goldRewardWeight) * BigInt(toReturn.hourPassed)).toString();
        toReturn.heartReward = (BigInt(heartRewardWeight) * BigInt(toReturn.hourPassed)).toString();
        toReturn.gemReward = gemRewardWeight * toReturn.dayPassed;
        toReturn.raidTicketReward = raidTicketRewardWeight * toReturn.dayPassed;
    }

    return toReturn;
}

handlers["raidRefresh"] = RaidRefresh;

interface IRaidOpen{
    error: boolean;
    raidIndex: number;
    reasonOfError: string;
}

let RaidOpen = function (args: any, context: IPlayFabContext): IRaidOpen{
    let toReturn: IRaidOpen = {error: true, raidIndex: 0, reasonOfError: ""};

    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "raid_index"
        ]
    }).Data;

    if(getUserInternalDataResult["raid_index"] != null)
       toReturn.raidIndex = parseInt(getUserInternalDataResult["raid_index"].Value) + 1;

    let updateuserInternalDataResult = server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: {
            "is_open_raid": "true",
            "is_playing_raid": "false",
            "raid_index": toReturn.raidIndex.toString()
        }
    });

    toReturn.error = false;

    return toReturn;
}

handlers["raidOpen"] = RaidOpen;

interface IRaidStart{
    error: boolean;
    reasonOfError: string;
}

let RaidStart = function (args: any, context: IPlayFabContext): IRaidStart{
    let toReturn: IRaidStart = {error: true, reasonOfError: ""};

    let updateuserInternalDataResult = server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: {
            "is_open_raid": "true",
            "is_playing_raid": "true"
        }
    });

    toReturn.error = false;

    return toReturn;
}

handlers["raidStart"] = RaidStart;

interface IRaidEnd{
    error: boolean;
    reasonOfError: string;
}

let RaidEnd = function (args: any, context: IPlayFabContext): IRaidEnd{
    let toReturn: IRaidEnd = {error: true, reasonOfError: ""};

    let willGetReward = args.willGetReward;

    if(willGetReward)
    {
        if(!UseDailyLimitIfAvail("raid", "0", 1, true))
            return toReturn;
    }

    let skillUseCount = 0;
    if(args.skillUseCount)
        skillUseCount = args.skillUseCount;

    let updateuserInternalDataResult = server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: {
            "is_open_raid": "false",
            "is_playing_raid": "false"
        }
    });

    toReturn.error = false;

    return toReturn;
}

handlers["raidEnd"] = RaidEnd;

interface IRaidLeave{
    error: boolean;
    reasonOfError: string;
}

let RaidLeave = function (args: any, context: IPlayFabContext): IRaidLeave{
    let toReturn: IRaidLeave = {error: true, reasonOfError: ""};

    let willGetReward = args.willGetReward;

    if(willGetReward)
    {
        if(!UseDailyLimitIfAvail("raid", "0", 1, true))
            return toReturn;
    }

    let skillUseCount = 0;
    if(args.skillUseCount)
        skillUseCount = args.skillUseCount;

    toReturn.error = false;

    return toReturn;
}

handlers["raidLeave"] = RaidLeave;

interface IRaidJoin{
    error: boolean;
    reasonOfError: string;
}

let RaidJoin = function (args: any, context: IPlayFabContext): IRaidJoin{
    let toReturn: IRaidJoin = {error: true, reasonOfError: ""};

    let hostPlayerId = "";
    let hostRaidIndex = 0;

    if(args.hostPlayerId)
        hostPlayerId = args.hostPlayerId;

    if(args.hostRaidIndex)
        hostRaidIndex = args.hostRaidIndex;

    let getHostInternalDataResult = server.GetUserInternalData({
        PlayFabId: hostPlayerId,
        Keys: [
            "is_open_raid",
            "is_playing_raid",
            "raid_index"
        ]
    }).Data;

    if(getHostInternalDataResult["is_playing_raid"] != null && getHostInternalDataResult["is_playing_raid"].Value == "true")
    {
        toReturn.reasonOfError = "raid_already_playing";
        return toReturn;
    }
    else if(getHostInternalDataResult["is_open_raid"] == null || getHostInternalDataResult["is_open_raid"].Value != "true")
    {
        toReturn.reasonOfError = "raid_not_open";
        return toReturn;
    }
    else if(getHostInternalDataResult["raid_index"] == null || parseInt(getHostInternalDataResult["raid_index"].Value) != hostRaidIndex)
    {
        toReturn.reasonOfError = "raid_index_mismatch";
        return toReturn;
    }

    toReturn.error = false;

    return toReturn;
}

handlers["raidJoin"] = RaidJoin;