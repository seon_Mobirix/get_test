///////////////////////////////////////////////////////////////////////////////////////////////////////
// FANTASYxDUNGEONS
///////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////// Cloud Script Handler Functions /////////////////////////

interface ICheckLeftCooldown{
    error: boolean;
    leftSeconds: number;
}

let LeftCooldown = function (args: any, context: IPlayFabContext): ICheckLeftCooldown{
    
    let name = "";
    let coolTime = 0;

    let toReturn: ICheckLeftCooldown = {error: true, leftSeconds: 0};

    if(args && args.name)
        name = args.name;
    else
        return toReturn;

    toReturn.leftSeconds = GetCooldown(name);
    toReturn.error = false;
    return toReturn;
}

handlers["leftCooldown"] = LeftCooldown;

///////////////////////// HELPER FUNCTIONS (NOT DIRECTLY CALLABLE FROM THE CLIENT) /////////////////////////

function GetCooldown(name: string): number{
        
    let coolTime = 300;
    if(name == "tower_deploy")
        coolTime = 3600;
    else if(name == "tower_battle")
        coolTime = 60;

    // Get current time stamp
    let currentTimestampSecond = Math.floor(+ new Date() / (1000));

    // Get last time stamp with the name
    let getUserInternalDataResult = server.GetUserInternalData({
        PlayFabId: currentPlayerId,
        Keys: [
            "cooldown_timestamp_" + name
        ]
    }).Data["cooldown_timestamp_" + name];

    let lastTimestampSecond = 0;
    if(getUserInternalDataResult != null)
        lastTimestampSecond = parseInt(getUserInternalDataResult.Value);

    // Return left cool time
    return Math.max(lastTimestampSecond + coolTime - currentTimestampSecond, 0);
}

function StartCooldown(name: string){

    // Get time stamp to use
    let timestampSecond = Math.floor(+ new Date() / (1000));

    // Update last time stamp
    let dataToUse = {};
    dataToUse["cooldown_timestamp_" + name] = String(timestampSecond);
    let updateUserInternalDataResult = server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: dataToUse
    });
}

function ForceResetCooldown(name: string){
    // Update last time stamp
    let dataToUse = {};
    dataToUse["cooldown_timestamp_" + name] = String(0);
    let updateUserInternalDataResult = server.UpdateUserInternalData({
        PlayFabId: currentPlayerId,
        Data: dataToUse
    });
}