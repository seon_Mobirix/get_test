///////////////////////////////////////////////////////////////////////////////////////////////////////
// Dungeon Knight
///////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////// Cloud Script Handler Functions /////////////////////////
interface IRequestAttachedItemSystemMailItemAll{
    receiveItemList: {};
    updatedSystemPost: string;
    error: boolean;
}

let RequestAttachedItemSystemMailItemAll = function(args: any, context: IPlayFabContext): IRequestAttachedItemSystemMailItemAll{
    let toReturn: IRequestAttachedItemSystemMailItemAll = {receiveItemList: {}, updatedSystemPost: "", error: true};
    let receiveItemList = [];

    // Get system mail list and inventory data
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["inbox_system", "inventory_data"]
    });

    if(getUserReadOnlyDataResult.Data["inbox_system"] != null && getUserReadOnlyDataResult.Data["inventory_data"] != null)
    {
        let inventoryData = JSON.parse(getUserReadOnlyDataResult.Data["inventory_data"].Value);
        let systemInbox = DeleteOldSystemMail(JSON.parse(getUserReadOnlyDataResult.Data["inbox_system"].Value), false);
        let updatedMailList = systemInbox.MailList;
        
        let totalGold = BigInt(0);
        let totalHeart = BigInt(0);
        let totalRaidTicket = BigInt(0);

        if(systemInbox.MailList.length > 0)
        {
            for(let i = 0; i < systemInbox.MailList.length ; i ++)
            {
                let targetMailInfo = systemInbox.MailList[i];

                if(targetMailInfo != null)
                {
                    let attachedItemInfo = targetMailInfo.AttachedItem;

                    if(attachedItemInfo != null)
                    {
                        if(attachedItemInfo.ItemId == "gold")
                            totalGold = totalGold + BigInt(attachedItemInfo.Count);
                        else if(attachedItemInfo.ItemId == "heart")
                            totalHeart = totalHeart + BigInt(attachedItemInfo.Count);
                        else if(attachedItemInfo.ItemId == "ticket_raid")
                            totalRaidTicket = totalRaidTicket + BigInt(attachedItemInfo.Count);
                        else if(attachedItemInfo.ItemId == "weapon" || attachedItemInfo.ItemId == "class" || attachedItemInfo.ItemId == "pet" || attachedItemInfo.ItemId == "mercenary")
                            inventoryData = AddItemToInventoryData(inventoryData, attachedItemInfo.ItemId, attachedItemInfo.ItemCode, Number(attachedItemInfo.Count));
                        else
                            return toReturn;

                        let targetItem = receiveItemList.filter(n => n.ItemId == attachedItemInfo.ItemId)[0];
                        if(targetItem != null)
                        {
                            // Check if item id is weapon or class or pet then check itemcode also for item counting
                            if(attachedItemInfo.ItemId == "weapon" || attachedItemInfo.ItemId == "class" || attachedItemInfo.ItemId == "pet" || attachedItemInfo.ItemId == "mercenary")
                            {
                                if(attachedItemInfo.ItemCode == targetItem.ItemCode)
                                    targetItem.Count = BigInt(targetItem.Count) + BigInt(attachedItemInfo.Count);
                                else
                                    receiveItemList.push({"ItemId": attachedItemInfo.ItemId, "ItemCode": attachedItemInfo.ItemCode, "Count": attachedItemInfo.Count});
                            }
                            else
                                targetItem.Count = BigInt(targetItem.Count) + BigInt(attachedItemInfo.Count);
                        }
                        else
                            receiveItemList.push({"ItemId": attachedItemInfo.ItemId, "ItemCode": attachedItemInfo.ItemCode, "Count": attachedItemInfo.Count});
                        
                        updatedMailList = updatedMailList.filter(n => n.Id != targetMailInfo.Id);
                    }
                }        
            }

            systemInbox.MailList = updatedMailList;

            if(totalGold > 0)
                inventoryData = AddBigIntCurrency(inventoryData, "gold", totalGold);
            if(totalHeart > 0)
                inventoryData = AddBigIntCurrency(inventoryData, "heart", totalHeart);
            if(totalRaidTicket > 0)
                inventoryData = AddBigIntCurrency(inventoryData, "ticket_raid", totalRaidTicket);

            // Update inventory data time stamp
            inventoryData.TimeStamp = Math.floor(+ new Date() / 1000) + 3600 * 24;
            
            // Update system inbox and inventory data after received mail
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "inventory_data": JSON.stringify(inventoryData),
                    "inbox_system": JSON.stringify(systemInbox),
                    "progress_value": "1"
                }
            });
            
            toReturn.receiveItemList = {"ItemList": receiveItemList};
            toReturn.updatedSystemPost = JSON.stringify(systemInbox);
            toReturn.error = false;
        }
        else
            log.info("system inbox is empty now!");

        return toReturn;
    }
    else
        return toReturn;
}
handlers["requestAttachedItemSystemMailItemAll"] = RequestAttachedItemSystemMailItemAll;

interface IAddSystemMail{
    error: boolean;
}

let AddSystemMail = function(args: any, context: IPlayFabContext): IAddSystemMail{
    let message = "";
    let attachedItem: {} = null;
    let toReturn: IAddSystemMail = {error: true};

    if(args.message)
        message = args.message;
    if(args.attachedItem)
        attachedItem = args.attachedItem as {};

    // Get system mail list
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["inbox_system"]
    });

    if(getUserReadOnlyDataResult.Data["inbox_system"] != null)
    {
        let inbox = JSON.parse(getUserReadOnlyDataResult.Data["inbox_system"].Value);

        inbox = DeleteOldMail(inbox, true);

        let mailId = 0;
        
        for(let i = 0; i < inbox.MailList.length ; i ++)
        {
            if(Number(inbox.MailList[i].Id) >= mailId)
            {
                mailId = Number(inbox.MailList[i].Id);
            }
        }

        mailId += 1;
        // Expired time will be 30days later
        let currentTimestampSecond = Math.floor(+ new Date() / (1000));
        let expiredTime = currentTimestampSecond + 2592000;

        inbox.MailList.push({Id: mailId, Message: message, AttachedItem: attachedItem, ExpiredTime: expiredTime});

        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "inbox_system": JSON.stringify(inbox)
            }
        });

        toReturn.error = false;
    }
    else
    {
        let mailId = 0;
        // Expired time will be 30days later
        let currentTimestampSecond = Math.floor(+ new Date() / (1000));
        let expiredTime = currentTimestampSecond + 2592000;

        let defaultInbox = {"MailList":[]};

        defaultInbox.MailList.push({Id: mailId, Message: message, AttachedItem: attachedItem, ExpiredTime: expiredTime});

        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "inbox_system": JSON.stringify(defaultInbox)
            }
        });

        toReturn.error = false;
    }

    return toReturn;
}
handlers["addSystemMail"] = AddSystemMail;

///////////////////////// HELPER FUNCTIONS (NOT DIRECTLY CALLABLE FROM THE CLIENT) /////////////////////////
function DeleteOldSystemMail(currentInbox: any, skipUpdate: boolean): any
{
    if(currentInbox != null)
    {
        let currentTimestampSecond = Math.floor(+ new Date() / (1000));
        let updatedMailList = currentInbox.MailList;

        for(let i = 0; i < currentInbox.MailList.length ; i ++)
        {
            if(Number(currentInbox.MailList[i].ExpiredTime) < currentTimestampSecond)
            {
                updatedMailList = updatedMailList.filter(n => n.Id != currentInbox.MailList[i].Id);
            }
        }

        currentInbox.MailList = updatedMailList;

        if(!skipUpdate)
        {
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "inbox_system": JSON.stringify(currentInbox)
                }
            });
        }   

        return currentInbox;
    }
}

function AddBigIntCurrency(inventoryData: any, currencyId: string, amount: bigint): any
{
    if(inventoryData != null)
    {
        if(currencyId == "gold")
        {
            let originGold = inventoryData.Gold;
            inventoryData.Gold = (BigInt(originGold) + amount).toString();
        }
        else if(currencyId == "heart")
        {
            let originHeart = inventoryData.Heart;
            inventoryData.Heart = (BigInt(originHeart) + amount).toString();
        }
        else if(currencyId == "ticket_raid")
        {
            let originTicketRaid  = inventoryData.TicketRaid;
            inventoryData.TicketRaid = (BigInt(originTicketRaid) + amount).toString();
        }
    }

    return inventoryData;
}

function AddItemToInventoryData(inventoryData: any, itemId: string, itemCode: string, amount: number): any
{
    if(inventoryData != null)
    {
        if(itemId == "weapon")
        {
            let targetWeapon = inventoryData.OwnedWeaponList.find(n => n.id == itemCode);
            if(targetWeapon != null)
                targetWeapon.count += amount;
            else
            {
                targetWeapon = {
                    "id": itemCode,
                    "level": 1,
                    "count": amount,
                    "isNew": true
                };
                inventoryData.OwnedWeaponList.push(targetWeapon);
            }
        }
        else if(itemId == "class")
        {
            let targetClass = inventoryData.OwnedClassList.find(n => n.id == itemCode);
            if(targetClass != null)
                targetClass.count += amount;
            else
            {
                targetClass = {
                    "id": itemCode,
                    "level": 1,
                    "count": amount,
                    "isNew": true
                };
                inventoryData.OwnedClassList.push(targetClass);
            }
        }
        else if(itemId == "pet")
        {
            let targetPet = inventoryData.OwnedPetList.find(n => n.id == itemCode);
            if(targetPet != null)
                targetPet.count += amount;
            else
            {
                targetPet = {
                    "id": itemCode,
                    "level": 1,
                    "count": amount,
                    "isNew": true,
                    "goldLevelUpCount": 0,
                    "heartLevelUpCount": 0,
                    "mergeLevelUpCount": 0
                };
                inventoryData.OwnedPetList.push(targetPet);
            }
        }
        else if(itemId == "mercenary")
        {
            if(inventoryData.OwnedMercenaryList != null)
            {
                let targetMercenary = inventoryData.OwnedMercenaryList.find(n => n.id == itemCode);
                if(targetMercenary != null)
                    targetMercenary.count += amount;
                else
                {
                    targetMercenary = {
                        "id": itemCode,
                        "level": 1,
                        "count": amount,
                        "isNew": true
                    };
                    inventoryData.OwnedMercenaryList.push(targetMercenary);
                }
            }
            else
            {
                let OwnedMercenaryList = [];
                let targetMercenary = {
                    "id": itemCode,
                    "level": 1,
                    "count": amount,
                    "isNew": true
                };

                OwnedMercenaryList.push(targetMercenary);

                inventoryData["OwnedMercenaryList"] = OwnedMercenaryList;
            }
        }
    }

    return inventoryData;
}