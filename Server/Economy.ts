///////////////////////////////////////////////////////////////////////////////////////////////////////
// Dungeon Knight
///////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////// Cloud Script Handler Functions /////////////////////////
interface IOneTimeReward{
    rewardGem: number;
    error: boolean;
    reasonOfError: string;
}

let OneTimeReward = function(args: any, context: IPlayFabContext): IOneTimeReward{
    let id = "";
    
    let toReturn: IOneTimeReward = {rewardGem: 0, reasonOfError: "", error: true};

    if(args.id)
        id = args.id;

    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "only_once_rewards"
        ]
    }).Data;

    if(getTitleDataResult != null)
    {
        let convertedData = JSON.parse(getTitleDataResult["only_once_rewards"]); 
        let rewardInfo = convertedData[id];

        if(rewardInfo == null)
        {
            log.info(id + " is not avail!");
            toReturn.reasonOfError = "coupon_failed";
            return toReturn;
        }
        else
        {
            let getUserInternalDataResult = server.GetUserInternalData({
                PlayFabId: currentPlayerId,
                Keys: [
                    "received_only_once_rewards"
                ]
            }).Data["received_only_once_rewards"];
            
            let listOfReceivedItems = [];
            if(getUserInternalDataResult != null)
            {
                listOfReceivedItems = JSON.parse(getUserInternalDataResult.Value);
                if(listOfReceivedItems.some(n => n == id))
                {
                    log.info("Already received the reward: " + id);
                    toReturn.reasonOfError = "coupon_already_use";
                    return toReturn;
                }
            }

            listOfReceivedItems.push(id);

            let updateUserInternalDataResult = server.UpdateUserInternalData({
                PlayFabId: currentPlayerId,
                Data: {
                    "received_only_once_rewards":  JSON.stringify(listOfReceivedItems)
                }
            });
            
            // Actually give the reward
            if(rewardInfo.Id == "GE")
            {
                AddVirtualCurrency("GE", rewardInfo.Amount);
                toReturn.rewardGem = rewardInfo.Amount;
            }

            toReturn.error = false;
            return toReturn;
        }
    }
    else
    {
        log.info("only_once_rewards is not avail!");
        return toReturn;
    }
}

handlers["oneTimeReward"] = OneTimeReward;

let OneTimeReward2 = function(args: any, context: IPlayFabContext): IOneTimeReward{
    let id = "";
    let toReturn: IOneTimeReward = {rewardGem: 0, reasonOfError: "", error: true};

    if(args.id)
        id = args.id;

    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "coupon_list"
        ]
    }).Data;

    if(getTitleDataResult != null)
    {
        let convertedData = JSON.parse(getTitleDataResult["coupon_list"]); 
        let rewardInfo = convertedData[id];

        if(rewardInfo == null)
        {
            log.info(id + " is not avail!");
            toReturn.reasonOfError = "coupon_failed";
            return toReturn;
        }
        else
        {
            // Get server id
            let getPlayerTagsResult = server.GetPlayerTags({
                PlayFabId: currentPlayerId
            });

            let serverId = "";
            if(getPlayerTagsResult != null && getPlayerTagsResult.Tags.length > 0)
            {
                let serverTags = getPlayerTagsResult.Tags.filter(n => n.indexOf("Server") >= 0);
                if(serverTags != null && serverTags.length > 0)
                    serverId = GetServerIdFromTag(serverTags[0]);
            }
            
            //Check server validate
            if(serverId != rewardInfo.ServerId)
            {
                log.info("server id is not validate!");
                return toReturn;
            }

            //Check date validate
            let currentTimestampSecond = Math.floor(+ new Date() / (1000));

            if(currentTimestampSecond < rewardInfo.StartTimeStamp || currentTimestampSecond > rewardInfo.EndTimeStamp)
            {
                log.info("coupon's date is not validate!");
                return toReturn;
            }

            let getUserInternalDataResult = server.GetUserInternalData({
                PlayFabId: currentPlayerId,
                Keys: [
                    "received_only_once_rewards"
                ]
            }).Data["received_only_once_rewards"];
            
            let listOfReceivedItems = [];
            if(getUserInternalDataResult != null)
            {
                listOfReceivedItems = JSON.parse(getUserInternalDataResult.Value);
                if(listOfReceivedItems.some(n => n == id))
                {
                    log.info("Already received the reward: " + id);
                    toReturn.reasonOfError = "coupon_already_use";
                    return toReturn;
                }
            }

            listOfReceivedItems.push(id);

            let updateUserInternalDataResult = server.UpdateUserInternalData({
                PlayFabId: currentPlayerId,
                Data: {
                    "received_only_once_rewards":  JSON.stringify(listOfReceivedItems)
                }
            });
            
            // Actually give the reward
            if(rewardInfo.Id == "GE")
            {
                AddVirtualCurrency("GE", rewardInfo.Amount);
                toReturn.rewardGem = rewardInfo.Amount;
            }

            toReturn.error = false;
            return toReturn;
        }
    }
    else
    {
        log.info("only_once_rewards is not avail!");
        return toReturn;
    }
}

handlers["oneTimeReward2"] = OneTimeReward2;

interface IGiveEventGemReward{
    rewardGem: number;
    error: boolean;
}

let GiveEventGemReward = function(args: any, context: IPlayFabContext): IGiveEventGemReward{
    let toReturn: IGiveEventGemReward = {rewardGem: 0, error: true};

    let amount = 30;
    let addedTime = 259200;

    if(args.amount)
        amount = parseInt(args.amount);
    if(args.addedTime)
        addedTime = parseInt(args.addedTime);

    let attachedItemTemplate = {ItemId: "gem", ItemCode: "GE", Count: amount};
    let mailArgs = {message: "message_mail_event_reward_gem", attachedItem: attachedItemTemplate, addedTime: addedTime};

    toReturn.error = AddOnTimeEventMail(mailArgs, context).error;
    toReturn.rewardGem = amount;

    return toReturn;
}   

handlers["giveEventGemReward"] = GiveEventGemReward;

interface IProcessGemCashback{
    error: boolean;
}

let ProcessGemCashback = function (args: any, context: IPlayFabContext): IProcessGemCashback{

    let toReturn: IProcessGemCashback = {error: true};
    
    let getUserInventoryResult = server.GetUserInventory(
        {
            PlayFabId: currentPlayerId
        }
    );

    if(getUserInventoryResult.VirtualCurrency["PB"] <= 0)
        return toReturn;
    else
    {
        // Add gem cash back mail (50%)
        let gemAmount = (getUserInventoryResult.VirtualCurrency["PB"] / 2);
        let attachedItemTemplate = {ItemId: "gem", ItemCode: "GE", Count: gemAmount};
        let args = {message: "message_mail_event_reward_gem", attachedItem: attachedItemTemplate};

        toReturn.error = AddMail(args, context).error;

        // Subtract virtual currency
        if(!toReturn.error)
            SubtractVirtualCurrency("PB", getUserInventoryResult.VirtualCurrency["PB"]);

        return toReturn;
    }
}

handlers["processGemCashback"] = ProcessGemCashback;

interface IRequestGemBonus{
    updatedPurchasedGemBundleList: string[];
    bonusGemAmount: number;
    error: boolean;
}

let RequestGemBonus = function (args: any, context: IPlayFabContext): IRequestGemBonus{
    
    let toReturn: IRequestGemBonus = {updatedPurchasedGemBundleList: [], bonusGemAmount: 0, error: true};
    let purchasedGemBundleId = "";

    if(args.purchasedGemBundleId)
        purchasedGemBundleId = args.purchasedGemBundleId;

    let getUserReadOnlyDataResult = server.GetUserReadOnlyData(
        {
            PlayFabId: currentPlayerId,
            Keys: [
                "purchased_gem_bunlde_list"
            ]
        }
    );

    let getUserInventoryResult = server.GetUserInventory(
        {
            PlayFabId: currentPlayerId
        }
    );

    // Check user have this gem package
    if(getUserInventoryResult.Inventory.filter(n => n.ItemId == purchasedGemBundleId)[0] == null)
        return toReturn;

    // Check already get bonus
    if(getUserReadOnlyDataResult.Data["purchased_gem_bunlde_list"] != null)
    {
        toReturn.updatedPurchasedGemBundleList = JSON.parse(getUserReadOnlyDataResult.Data["purchased_gem_bunlde_list"].Value);

        if(toReturn.updatedPurchasedGemBundleList.filter(n => n == purchasedGemBundleId)[0] != null)
        {
            log.info("User already received gem bonus!");
            return toReturn;
        }
    }
    
    let getCatalogResult = server.GetCatalogItems({});
    let targetGemBundle = getCatalogResult.Catalog.filter(n => n.ItemId == purchasedGemBundleId)[0];

    if(targetGemBundle != null)
    {
        let gemBonusAmount = targetGemBundle.Bundle.BundledVirtualCurrencies["GE"];
        AddVirtualCurrency("GE", gemBonusAmount);

        toReturn.updatedPurchasedGemBundleList.push(purchasedGemBundleId);

        let updateUserDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "purchased_gem_bunlde_list" : JSON.stringify(toReturn.updatedPurchasedGemBundleList)
            }
        });

        toReturn.bonusGemAmount = gemBonusAmount;
        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;
}

handlers["requestGemBonus"] = RequestGemBonus;

interface IRequestSupplyBox{
    error: boolean;
    minPassed: number;
}

let RequestSupplyBox = function(args: any, context: IPlayFabContext): IRequestSupplyBox{

    let toReturn: IRequestSupplyBox = {error: true, minPassed:0};

    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["last_supply_timestamp_min"]
    });

    let lastTimestampMin = -1;
    if(getUserReadOnlyDataResult.Data["last_supply_timestamp_min"] != null)
        lastTimestampMin = parseInt(getUserReadOnlyDataResult.Data["last_supply_timestamp_min"].Value);

    let currentTimestampMin = Math.floor(+ new Date() / (1000 * 60));

    if(lastTimestampMin != -1)
        toReturn.minPassed = Math.min(currentTimestampMin - lastTimestampMin, 1440);
    else
        toReturn.minPassed = 0;

    if(currentTimestampMin != lastTimestampMin)
    {
        let updateuserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "last_supply_timestamp_min": currentTimestampMin.toString()
            }
        });
    }
    
    toReturn.error = false;

    return toReturn;

}
handlers["requestSupplyBox"] = RequestSupplyBox;

interface IRequestFreeGemDailyLeftLimit{
    leftLimit: number;
    error: boolean;
}

let RequestFreeGemDailyLeftLimit = function(args: any, context: IPlayFabContext): IRequestFreeGemDailyLeftLimit{

    let toReturn: IRequestFreeGemDailyLeftLimit = {leftLimit: 0, error: true};    
    
    toReturn.leftLimit = GetLeftDailyLimit("free_gem", "0");
    toReturn.error = false;
    return toReturn;

}
handlers["requestFreeGemDailyLeftLimit"] = RequestFreeGemDailyLeftLimit;

interface IGetFreeGemAds{
    gemCount: number;
    nextFreeGemAdsTimeStamp: number;
    leftLimit: number;
    error: boolean;
}

let GetFreeGemAds = function(args: any, context: IPlayFabContext): IGetFreeGemAds{

    let toReturn: IGetFreeGemAds = {gemCount: 0, nextFreeGemAdsTimeStamp: 0, leftLimit: 0, error: true};    
    let setting = null;
    let nextTimestamp = 0;

     // Get setting json
     let getTitleDataResult = server.GetTitleData({
        Keys: [
            "setting_json"
        ]
    }).Data;

    setting = JSON.parse(getTitleDataResult["setting_json"]);

    if(setting == null)
        return toReturn;

    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: [
            "nextFreeGemAdsTimeStamp"
        ]
    });

    if(getUserReadOnlyDataResult.Data["nextFreeGemAdsTimeStamp"] != null)
        nextTimestamp = parseInt(getUserReadOnlyDataResult.Data["nextFreeGemAdsTimeStamp"].Value);

    let currentTimestamp = Math.floor(+ new Date() / 1000);
    
    // Check cool time
    if(currentTimestamp >= nextTimestamp)
    {   
        // Use daily limit here
        if(!UseDailyLimitIfAvail("free_gem", "0", 1))
            return toReturn;
        
        // Get daily limit
        toReturn.leftLimit = GetLeftDailyLimit("free_gem", "0");

        // Give random reward between min and max value from setting
        let gemReward = Math.min(Math.floor(Math.random() * (setting.FreeGemMaxAmount + 1 - setting.FreeGemMinAmount) + setting.FreeGemMinAmount), setting.FreeGemMaxAmount);

        AddVirtualCurrency("GE", gemReward);

        toReturn.gemCount = gemReward;
        toReturn.nextFreeGemAdsTimeStamp = currentTimestamp + setting.FreeGemCoolTime;

        // Update last free gem ad time stamp
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "nextFreeGemAdsTimeStamp": toReturn.nextFreeGemAdsTimeStamp.toString()
            }
        });

        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;

}
handlers["getFreeGemAds"] = GetFreeGemAds;

interface IRequestGrowthSupportFree{
    rewardGemAmount: number;
    updatedReceivedRewardList: any;
    error: boolean;
}

let RequestGrowthSupportFree = function(args: any, context: IPlayFabContext): IRequestGrowthSupportFree{

    let toReturn: IRequestGrowthSupportFree = {rewardGemAmount: 0, updatedReceivedRewardList: [], error: true};
    let growthSupportRewardId = "";
    let rewardRequireStageProgess = 0;

    if(args.growthSupportRewardId)
        growthSupportRewardId = args.growthSupportRewardId;

    // Get user received reward list and level
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData(
        {
            PlayFabId: currentPlayerId,
            Keys: [
                "growth_support_free_received_reward_list",
                "user_data"
            ]
        }
    );

    // Check user's last clear world
    if(getUserReadOnlyDataResult.Data["user_data"] != null)
    {
        let userData = JSON.parse(getUserReadOnlyDataResult.Data["user_data"].Value);
        rewardRequireStageProgess = parseInt(growthSupportRewardId.substring(growthSupportRewardId.length - 2, growthSupportRewardId.length));

        log.info("user last clear world level: " + userData.StageProgress + "// require world level: " + rewardRequireStageProgess);

        if(rewardRequireStageProgess == 0 || userData.StageProgress < rewardRequireStageProgess)
            return toReturn;
    }

    if(getUserReadOnlyDataResult.Data["growth_support_free_received_reward_list"] != null)
    {
        toReturn.updatedReceivedRewardList = JSON.parse(getUserReadOnlyDataResult.Data["growth_support_free_received_reward_list"].Value);

        // Check user already received this reward or not
        if(toReturn.updatedReceivedRewardList.filter(n => n == growthSupportRewardId)[0] != null)
        {
            log.info("User already received this reward !");
            return toReturn;
        }

        // Check maxium reward count
        if(toReturn.updatedReceivedRewardList.length > 8)
        {
            log.info("User already received all reward !");
            return toReturn;
        }
    }

    // Get reward from reward table
    let getTitleDataResult = server.GetTitleData({
        Keys: ["growth_support_free_reward_table"]
    }).Data;

    if(getTitleDataResult["growth_support_free_reward_table"] == null)
        return toReturn;

    // Get growth support free reward data
    let growthSupportFreeRewardData = JSON.parse(getTitleDataResult["growth_support_free_reward_table"]);

    if(growthSupportFreeRewardData == null)
        return toReturn;

    // stage progress 10 ~ 80 -> 0 ~ 7
    let convertedIndex = rewardRequireStageProgess / 10 - 1;
    let targetReward = growthSupportFreeRewardData.GrowthSupportFreeRewardList[convertedIndex];

    if(targetReward == null)
        return toReturn;

    // Add reward
    AddVirtualCurrency(targetReward.ItemCode, Number(targetReward.ItemCount));
    
    toReturn.rewardGemAmount = Number(targetReward.ItemCount);
    toReturn.updatedReceivedRewardList.push(growthSupportRewardId);

    let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Data: {
            "growth_support_free_received_reward_list": unescape(JSON.stringify(toReturn.updatedReceivedRewardList)),
        }
    });

    toReturn.error = false;
    return toReturn;
}
handlers["requestGrowthSupportFree"] = RequestGrowthSupportFree;

interface IRequestGrowthSupportFirst{
    rewardGemAmount: number;
    updatedReceivedRewardList: any;
    error: boolean;
}

let RequestGrowthSupportFirst = function(args: any, context: IPlayFabContext): IRequestGrowthSupportFirst{

    let toReturn: IRequestGrowthSupportFirst = {rewardGemAmount: 0, updatedReceivedRewardList: [], error: true};
    let growthSupportRewardId = "";

    if(args.growthSupportRewardId)
        growthSupportRewardId = args.growthSupportRewardId;

    let getUserInventoryResult = server.GetUserInventory(
        {
            PlayFabId: currentPlayerId
        }
    );

    // Check user owned growth support 1 pakcage or not
    if(getUserInventoryResult.Inventory.filter(n => n.ItemId == "bundle_package_growth_1")[0] != null)
    {
        // Get user received reward list and level
        let getUserReadOnlyDataResult = server.GetUserReadOnlyData(
            {
                PlayFabId: currentPlayerId,
                Keys: [
                    "growth_support_1_received_reward_list",
                    "user_data"
                ]
            }
        );

        // Check user's last clear world
        if(getUserReadOnlyDataResult.Data["user_data"] != null)
        {
            let userData = JSON.parse(getUserReadOnlyDataResult.Data["user_data"].Value);
           
            let rewardRequireStageProgess = parseInt(growthSupportRewardId.substring(growthSupportRewardId.length - 2, growthSupportRewardId.length));

            log.info("user last clear world level: " + userData.StageProgress + "// require world level: " + rewardRequireStageProgess);

            if(userData.StageProgress < rewardRequireStageProgess)
                return toReturn;
        }

        if(getUserReadOnlyDataResult.Data["growth_support_1_received_reward_list"] != null)
        {
            toReturn.updatedReceivedRewardList = JSON.parse(getUserReadOnlyDataResult.Data["growth_support_1_received_reward_list"].Value);

            // Check user already received this reward or not
            if(toReturn.updatedReceivedRewardList.filter(n => n == growthSupportRewardId)[0] != null)
            {
                log.info("User already received this reward !");
                return toReturn;
            }

            // Check maxium reward count
            if(toReturn.updatedReceivedRewardList.length > 8)
            {
                log.info("User already received all reward !");
                return toReturn;
            }
        }
        
        // Add reward
        AddVirtualCurrency("GE", 4000);

        toReturn.rewardGemAmount = 4000;
        toReturn.updatedReceivedRewardList.push(growthSupportRewardId);

        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "growth_support_1_received_reward_list": unescape(JSON.stringify(toReturn.updatedReceivedRewardList)),
            }
        });

        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;

}
handlers["requestGrowthSupportFirst"] = RequestGrowthSupportFirst;

interface IRequestGrowthSupportSecond{
    rewardGemAmount: number;
    updatedReceivedRewardList: any;
    error: boolean;
}

let RequestGrowthSupportSecond = function(args: any, context: IPlayFabContext): IRequestGrowthSupportSecond{

    let toReturn: IRequestGrowthSupportSecond = {rewardGemAmount: 0, updatedReceivedRewardList: [], error: true};
    let growthSupportRewardId = "";

    if(args.growthSupportRewardId)
        growthSupportRewardId = args.growthSupportRewardId;

    let getUserInventoryResult = server.GetUserInventory(
        {
            PlayFabId: currentPlayerId
        }
    );

    // Check user owned growth support 2 pakcage or not
    if(getUserInventoryResult.Inventory.filter(n => n.ItemId == "bundle_package_growth_2")[0] != null)
    {
        // Get user received reward list and level
        let getUserReadOnlyDataResult = server.GetUserReadOnlyData(
            {
                PlayFabId: currentPlayerId,
                Keys: [
                    "growth_support_2_received_reward_list",
                    "user_data"
                ]
            }
        );

        // Check user's last clear world level
        if(getUserReadOnlyDataResult.Data["user_data"] != null)
        {
            let userData = JSON.parse(getUserReadOnlyDataResult.Data["user_data"].Value);
           
            let rewardRequireStageProgess = parseInt(growthSupportRewardId.substring(growthSupportRewardId.length - 2, growthSupportRewardId.length));

            log.info("user last clear world level: " + userData.StageProgress + "// require world level: " + rewardRequireStageProgess);

            if(userData.StageProgress < rewardRequireStageProgess)
                return toReturn;
        }

        if(getUserReadOnlyDataResult.Data["growth_support_2_received_reward_list"] != null)
        {
            toReturn.updatedReceivedRewardList = JSON.parse(getUserReadOnlyDataResult.Data["growth_support_2_received_reward_list"].Value);

            // Check uesr already received this reward or not
            if(toReturn.updatedReceivedRewardList.filter(n => n == growthSupportRewardId)[0] != null)
            {
                log.info("User already received this reward !");
                return toReturn;
            }

            // Check maxium reward count
            if(toReturn.updatedReceivedRewardList.length > 8)
            {
                log.info("User already received all reward !");
                return toReturn;
            }
        }
        
        // Add reward
        AddVirtualCurrency("GE", 8000);

        toReturn.rewardGemAmount = 8000;
        toReturn.updatedReceivedRewardList.push(growthSupportRewardId);

        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "growth_support_2_received_reward_list": unescape(JSON.stringify(toReturn.updatedReceivedRewardList)),
            }
        });

        toReturn.error = false;
        return toReturn;
    }
    else
        return toReturn;

}
handlers["requestGrowthSupportSecond"] = RequestGrowthSupportSecond;

interface IRequestVIPReward{
    usedVIPPoint: number;
    rewardGemAmount: number;
    error: boolean;
}

let RequestVIPReward = function(args: any, context: IPlayFabContext): IRequestVIPReward{

    let toReturn: IRequestVIPReward = {usedVIPPoint: 0, rewardGemAmount: 0, error: true};

    // Get title data
    let getTitleDataResult = server.GetTitleData({
        Keys: ["setting_json"]
    }).Data;

    if(getTitleDataResult["setting_json"] != null)
    {
        let setting = JSON.parse(getTitleDataResult["setting_json"]);
        let currentVITicket = 0;

        let getUserInventoryResult = server.GetUserInventory(
            {
                PlayFabId: currentPlayerId
            }
        );
        
        currentVITicket = getUserInventoryResult.VirtualCurrency["VP"];

        if(currentVITicket < setting.RequireVIPTicket)
            return toReturn;
        else
        {
            let totalExchangeCount = Math.floor(currentVITicket / setting.RequireVIPTicket);
            let totalGemReward = totalExchangeCount * setting.VIPRewardGem;
            
            // Add reward
            AddVirtualCurrency("GE", totalGemReward);

            // Use VIP Ticket
            SubtractVirtualCurrency("VP", totalExchangeCount * setting.RequireVIPTicket);
            
            toReturn.usedVIPPoint = totalExchangeCount * setting.RequireVIPTicket;
            toReturn.rewardGemAmount = totalGemReward;
            toReturn.error = false;

            return toReturn;
        }
    }
    else
        return toReturn;
}

handlers["requestVIPReward"] = RequestVIPReward;

interface IRequestDailyReward{
    itemId: string;
    count: number;
    error: boolean;
}

let RequestDailyReward = function(args: any, context: IPlayFabContext): IRequestDailyReward{
    let toReturn: IRequestDailyReward = {itemId: "", count: 0, error: true};
    let currentDailyRewardCount = -1;
    let dailyRewardTable = null;

    // Check current daily reward ticket
    let currentInventory = server.GetUserInventory(
        {
            PlayFabId: currentPlayerId
        }
    );

    if(currentInventory.VirtualCurrency["DR"] < 1)
        return toReturn;

    // Get daily reard table
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "daily_reward_table"
        ]
    }).Data;

    dailyRewardTable = JSON.parse(getTitleDataResult["daily_reward_table"]);

    // Get daily reward info
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["daily_reward_count"]
    });

    if(getUserReadOnlyDataResult.Data["daily_reward_count"] != null)
        currentDailyRewardCount = parseInt(getUserReadOnlyDataResult.Data["daily_reward_count"].Value);

    if(dailyRewardTable != null)
    {
        // Daily reward (-1 ~ 23 to 0 ~ 24)
        currentDailyRewardCount = currentDailyRewardCount + 1;

        // Check over 25 days
        if(currentDailyRewardCount >= 25)
        {
            log.info("Daily reward can't be over 25 days");
            return toReturn;
        }

        let targetDailyReward = dailyRewardTable.DailyRewardList[currentDailyRewardCount];

        if(targetDailyReward != null)
        {
            if(targetDailyReward.ItemId == "gem")
                AddVirtualCurrency(targetDailyReward.ItemCode, Number(targetDailyReward.ItemCount));

            toReturn.itemId = targetDailyReward.ItemId;
            toReturn.count = targetDailyReward.ItemCount;

            toReturn.error = false;
            SubtractVirtualCurrency("DR", 1);
            
            let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                PlayFabId: currentPlayerId,
                Data: {
                    "daily_reward_count": currentDailyRewardCount.toString()
                }
            });

            return toReturn;
        }
        else
        {
            log.info("targetDailyReward is null!");
            return toReturn;
        }
    }
    else
    {
        log.info("dailyRewardTable is null!");
        return toReturn;
    }
}

handlers["requestDailyReward"] = RequestDailyReward;

interface IRequestDailyRewardInfo{
    userDailyRewardCount: number;
    dailyRewardTable: any;
    dailyRewardTicket: number;
    dailyRewardTicketRechargeTimes: number;
    error: boolean;
}

let RequestDailyRewardInfo = function(args: any, context: IPlayFabContext): IRequestDailyRewardInfo{

    let toReturn: IRequestDailyRewardInfo = {userDailyRewardCount: -1, dailyRewardTable: null, dailyRewardTicket: 0, dailyRewardTicketRechargeTimes: 0, error: true};  

    // Get daily reward info
    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: ["daily_reward_count"]
    });

    if(getUserReadOnlyDataResult.Data["daily_reward_count"] != null)
        toReturn.userDailyRewardCount = parseInt(getUserReadOnlyDataResult.Data["daily_reward_count"].Value);

    // Reset daily reward count
    if(toReturn.userDailyRewardCount >= 24)
    {
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "daily_reward_count": "-1"
            }
        });

        toReturn.userDailyRewardCount = -1;
    }

    // Get daily reward table
    let getTitleDataResult = server.GetTitleData({
        Keys: [
            "daily_reward_table"
        ]
    }).Data;

    toReturn.dailyRewardTable = JSON.parse(getTitleDataResult["daily_reward_table"]);

    if(toReturn.dailyRewardTable != null)
        toReturn.error = false;

    // Get daily reward ticket info
    let getUserInventoryResult = server.GetUserInventory(
        {
            PlayFabId: currentPlayerId
        }
    );

    if(getUserInventoryResult.VirtualCurrency["DR"] != null)
    {
        toReturn.dailyRewardTicket = getUserInventoryResult.VirtualCurrency["DR"].valueOf();
        let serverTimeStamp = Math.floor(+new Date() / 1000);
        toReturn.dailyRewardTicketRechargeTimes = Math.max(0, (Math.round(new Date(getUserInventoryResult.VirtualCurrencyRechargeTimes["DR"].RechargeTime).getTime()/1000) - serverTimeStamp));
    }
        
    return toReturn;
}
handlers["requestDailyRewardInfo"] = RequestDailyRewardInfo;

interface IGiveGemPackageReward{
    rewardGemAmount: number;
    error: boolean;
}

let GiveGemPackageReward = function(args: any, context: IPlayFabContext): IGiveGemPackageReward{

    let toReturn: IGiveGemPackageReward = {rewardGemAmount: 0, error: true};
    let rewardedGemBundleList = [];

    let getUserReadOnlyDataResult = server.GetUserReadOnlyData(
        {
            PlayFabId: currentPlayerId,
            Keys: [
                "rewarded_gem_bunlde_list"
            ]
        }
    );

    if(getUserReadOnlyDataResult.Data["rewarded_gem_bunlde_list"] != null)
        rewardedGemBundleList = JSON.parse(getUserReadOnlyDataResult.Data["rewarded_gem_bunlde_list"].Value);
    else
    {
        // Add default values
        rewardedGemBundleList.push(
            {
                "BundleId": "bundle_gem_01",
                "Count": 0
            },
            {
                "BundleId": "bundle_gem_02",
                "Count": 0
            },
            {
                "BundleId": "bundle_gem_03",
                "Count": 0
            },
            {
                "BundleId": "bundle_gem_04",
                "Count": 0
            },
            {
                "BundleId": "bundle_gem_05",
                "Count": 0
            }
        );
    }

    if(rewardedGemBundleList != null)
    {
        let totalGemReward = 0;
        let getUserInventoryResult = server.GetUserInventory(
            {
                PlayFabId: currentPlayerId
            }
        );
            
        // Get current total gem bundle count
        getUserInventoryResult.Inventory.forEach(tmp => {
            if(tmp.ItemId == "bundle_gem_01")
            {                
                let targetItem = rewardedGemBundleList.filter(n => n.BundleId == tmp.ItemId)[0];
                if(targetItem != null)
                {
                    let rewardBundleGem01Count = tmp.RemainingUses - targetItem.Count;
                    let totalrewardBundleGem01 = 0;
                    
                    if(targetItem.Count == 0)
                        totalrewardBundleGem01 = (rewardBundleGem01Count + 1) * 10;
                    else
                        totalrewardBundleGem01 = rewardBundleGem01Count * 10;

                    totalGemReward = totalGemReward + totalrewardBundleGem01;
                    targetItem.Count = tmp.RemainingUses;

                    log.info("gem_01_count: " + rewardBundleGem01Count + "// total_gem_01_reward: " + totalrewardBundleGem01);
                }
            }
            else if(tmp.ItemId == "bundle_gem_02")
            {
                let targetItem = rewardedGemBundleList.filter(n => n.BundleId == tmp.ItemId)[0];
                if(targetItem != null)
                {
                    let rewardBundleGem02Count = tmp.RemainingUses - targetItem.Count;
                    let totalrewardBundleGem02 = 0;
                    
                    if(targetItem.Count == 0)
                        totalrewardBundleGem02 = (rewardBundleGem02Count + 1) * 50;
                    else
                        totalrewardBundleGem02 = rewardBundleGem02Count * 50;

                    totalGemReward = totalGemReward + totalrewardBundleGem02;
                    targetItem.Count = tmp.RemainingUses;

                    log.info("gem_02_count: " + rewardBundleGem02Count + "// total_gem_02_reward: " + totalrewardBundleGem02);
                }
            }
            else if(tmp.ItemId == "bundle_gem_03")
            {
                let targetItem = rewardedGemBundleList.filter(n => n.BundleId == tmp.ItemId)[0];
                if(targetItem != null)
                {
                    let rewardBundleGem03Count = tmp.RemainingUses - targetItem.Count;
                    let totalrewardBundleGem03 = 0;

                    if(targetItem.Count == 0)
                        totalrewardBundleGem03 = (rewardBundleGem03Count + 1) * 100;
                    else
                        totalrewardBundleGem03 = rewardBundleGem03Count * 100;

                    totalGemReward = totalGemReward + totalrewardBundleGem03;
                    targetItem.Count = tmp.RemainingUses;

                    log.info("gem_03_count: " + rewardBundleGem03Count + "// total_gem_03_reward: " + totalrewardBundleGem03);
                }
            } 
            else if(tmp.ItemId == "bundle_gem_04")
            {
                let targetItem = rewardedGemBundleList.filter(n => n.BundleId == tmp.ItemId)[0];
                if(targetItem != null)
                {
                    let rewardBundleGem04Count = tmp.RemainingUses - targetItem.Count;
                    let totalrewardBundleGem04 = 0;

                    if(targetItem.Count == 0)
                        totalrewardBundleGem04 = (rewardBundleGem04Count + 1) * 3000;
                    else
                        totalrewardBundleGem04 = rewardBundleGem04Count * 3000;
                    
                    totalGemReward = totalGemReward + totalrewardBundleGem04;
                    targetItem.Count = tmp.RemainingUses;

                    log.info("gem_04_count: " + rewardBundleGem04Count + "// total_gem_04_reward: " + totalrewardBundleGem04);
                }
            }
            else if(tmp.ItemId == "bundle_gem_05")
            {
                let targetItem = rewardedGemBundleList.filter(n => n.BundleId == tmp.ItemId)[0];
                if(targetItem != null)
                {
                    let rewardBundleGem05Count = tmp.RemainingUses - targetItem.Count;
                    let totalrewardBundleGem05 = 0;

                    if(targetItem.Count == 0)
                        totalrewardBundleGem05 = (rewardBundleGem05Count + 1) * 15000;
                    else
                        totalrewardBundleGem05 = rewardBundleGem05Count * 15000;

                    totalGemReward = totalGemReward + totalrewardBundleGem05;
                    targetItem.Count = tmp.RemainingUses;

                    log.info("gem_05_count: " + rewardBundleGem05Count + "// total_gem_05_reward: " + totalrewardBundleGem05);
                }
            }
        });

        // Give gem reward to mail
        if(totalGemReward > 0)
        {
            log.info("total gem sent: " + totalGemReward);

            let attachedItemTemplate = {ItemId: "gem", ItemCode: "GE", Count: totalGemReward};
            let args = {message: "message_mail_event_reward_gem", attachedItem: attachedItemTemplate};

            toReturn.error = AddMail(args, context).error;

            if(!toReturn.error)
            {
                // Update rewarded_gem_bunlde_list
                let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
                    PlayFabId: currentPlayerId,
                    Data: {
                        "rewarded_gem_bunlde_list": JSON.stringify(rewardedGemBundleList)
                    }
                });
            }
            else
            {
                log.info("Error on add mail !");
                return toReturn;
            }
        }
        else
        {
            log.info("total gem reward is not over then 0 !");
            return toReturn;
        }
    }
    else
     return toReturn;
}
handlers["giveGemPackageReward"] = GiveGemPackageReward;

interface IResetPurchasedGemBundleList{
    error: boolean;
}

let ResetPurchasedGemBundleList = function(args: any, context: IPlayFabContext): IResetPurchasedGemBundleList{

    let toReturn: IResetPurchasedGemBundleList = {error: true};

    let getUserReadOnlyDataResult = server.GetUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Keys: [
            "purchased_gem_bunlde_list"
        ]
    });

    if(getUserReadOnlyDataResult.Data["purchased_gem_bunlde_list"] != null)
    {
        let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
            PlayFabId: currentPlayerId,
            Data: {
                "purchased_gem_bunlde_list": null
            }
        });

        toReturn.error = false;
    }
    
    return toReturn;
}
handlers["resetPurchasedGemBundleList"] = ResetPurchasedGemBundleList;

interface ICheckLimitedPackageAvailable{
    expiration: number;
    error: boolean;
}

let CheckLimitedPackageAvailable = function(args: any, context: IPlayFabContext): ICheckLimitedPackageAvailable{

    let toReturn: ICheckLimitedPackageAvailable = {expiration: 0, error: true};
    let targetPackageId = "";

    if(args.targetPackageId)
        targetPackageId = args.targetPackageId;

    let getUserInventoryResult = server.GetUserInventory(
        {
            PlayFabId: currentPlayerId
        }
    );
    
    let targetPackageData = getUserInventoryResult.Inventory.find(n => n.ItemId == targetPackageId);
    
    // Check user have this package or not
    if(targetPackageData == null)
        toReturn.error = false;
    else
    {
        // Check this package is limited or not
        if(targetPackageData.Expiration == null)
            toReturn.error = false;
        else
        {
            let serverTimeStamp = Math.floor(+new Date() / 1000);
            toReturn.expiration = Math.round(new Date(targetPackageData.Expiration).getTime()/1000);
            
            // Check remain time
            if(serverTimeStamp >= toReturn.expiration)
                toReturn.error = false;
        }
    }
    
    return toReturn;
}
handlers["checkLimitedPackageAvailable"] = CheckLimitedPackageAvailable;

interface ICheckDailyWeeklyPackageAvailable{
    dailyPackage1Avail: any[];
    dailyPackage3Avail: any[];
    weeklyPackage1Avail: any[];
    weeklyPackage3Avail: any[];
    error: boolean;
}

let CheckDailyWeeklyPackageAvailable = function(args: any, context: IPlayFabContext): ICheckDailyWeeklyPackageAvailable{

    let toReturn: ICheckDailyWeeklyPackageAvailable = {dailyPackage1Avail: [], dailyPackage3Avail: [], weeklyPackage1Avail: [], weeklyPackage3Avail: [], error: true};
    
    toReturn.dailyPackage1Avail.push(GetLeftDailyLimit("daily_package_1", "bundle_package_daily", false));
    toReturn.dailyPackage1Avail.push(GetLeftDailyLimit("daily_package_1", "bundle_package_daily_2", false));
    // toReturn.dailyPackage3Avail.push(GetLeftDailyLimit("daily_package_3", "0", false));
    toReturn.weeklyPackage1Avail.push(GetLeftWeeklyLimit("weekly_package_1", "bundle_package_weekly_2", false));
    toReturn.weeklyPackage3Avail.push(GetLeftWeeklyLimit("weekly_package_3", "bundle_package_weekly", false));
    
    toReturn.error = false;

    return toReturn;
}
handlers["checkDailyWeeklyPackageAvailable"] = CheckDailyWeeklyPackageAvailable;

let CheckDailyWeeklyPackageAvailableV2 = function(args: any, context: IPlayFabContext): ICheckDailyWeeklyPackageAvailable{

    let toReturn: ICheckDailyWeeklyPackageAvailable = {dailyPackage1Avail: [], dailyPackage3Avail: [], weeklyPackage1Avail: [], weeklyPackage3Avail: [], error: true};
    
    toReturn.dailyPackage3Avail.push(GetLeftDailyLimit("daily_package_3", "bundle_package_daily", false));
    toReturn.dailyPackage3Avail.push(GetLeftDailyLimit("daily_package_3", "bundle_package_daily_2", false));
    toReturn.weeklyPackage3Avail.push(GetLeftWeeklyLimit("weekly_package_3", "bundle_package_weekly", false));
    toReturn.weeklyPackage3Avail.push(GetLeftWeeklyLimit("weekly_package_3", "bundle_package_weekly_2", false));
    toReturn.weeklyPackage1Avail.push(GetLeftWeeklyLimit("weekly_package_1", "bundle_package_weekly_3", false));
    // TODO : remove below temp code
    toReturn.weeklyPackage3Avail.push(0);

    toReturn.error = false;

    return toReturn;
}
handlers["checkDailyWeeklyPackageAvailableV2"] = CheckDailyWeeklyPackageAvailableV2;

interface IUsedDailyWeeklyPackageIfAvail{
    error: boolean;
}

let UsedDailyWeeklyPackageIfAvail = function(args: any, context: IPlayFabContext): IUsedDailyWeeklyPackageIfAvail{

    let toReturn: IUsedDailyWeeklyPackageIfAvail = {error: true};
    
    let targetPackageType = "";
    if(args.targetPackageType)
        targetPackageType = args.targetPackageType;

    let targetPackageId = "0";
    if(args.targetPackageId)
        targetPackageId = args.targetPackageId;

    let askForUsing = false;
    if(args.askForUsing)
        askForUsing = args.askForUsing;
    
    if(askForUsing)
    {
        switch(targetPackageType)
        {
            case "daily_package_1":
                toReturn.error = !UseDailyLimitIfAvail(targetPackageType, targetPackageId, 1, false);
                break;
            case "daily_package_3":
                toReturn.error = !UseDailyLimitIfAvail(targetPackageType, targetPackageId, 1, false);
                break;
            case "weekly_package_1":
                toReturn.error = !UseWeeklyLimitIfAvail(targetPackageType, targetPackageId, 1, false);
                break;
            case "weekly_package_3":
                toReturn.error = !UseWeeklyLimitIfAvail(targetPackageType, targetPackageId, 1, false);
                break;
        }
    }
    else
    {
        switch(targetPackageType)
        {
            case "daily_package_1":
                toReturn.error = !GetLeftDailyLimit(targetPackageType, targetPackageId, false);
                break;
            case "daily_package_3":
                toReturn.error = !GetLeftDailyLimit(targetPackageType, targetPackageId, false);
                break;
            case "weekly_package_1":
                toReturn.error = !GetLeftWeeklyLimit(targetPackageType, targetPackageId, false);
                break;
            case "weekly_package_3":
                toReturn.error = !GetLeftWeeklyLimit(targetPackageType, targetPackageId, false);
                break;
        }
    }
    
    return toReturn;
}
handlers["useDailyWeeklyPackageIfAvail"] = UsedDailyWeeklyPackageIfAvail;

let UsedDailyWeeklyPackageIfAvailV2 = function (args: any, context: IPlayFabContext): IUsedDailyWeeklyPackageIfAvail {

    let toReturn: IUsedDailyWeeklyPackageIfAvail = { error: true };

    let targetPackageId = "0";
    if (args.targetPackageId)
        targetPackageId = args.targetPackageId;

    let targetPeriod = "daily";
    let targetPackageType = "";
    if (targetPackageId.indexOf("daily") != -1) {
        targetPackageType = "daily_package_";
        targetPeriod = "daily";
    }
    else if (targetPackageId.indexOf("weekly") != -1) {
        targetPackageType = "weekly_package_";
        targetPeriod = "weekly";
    }
    
    let catalogs = server.GetCatalogItems({});
    let targetPackage = catalogs.Catalog.filter(n => n.ItemId == targetPackageId)[0];

    if(targetPackage == null)
    {
        log.info("Can't find target package in catalogs!");
        return toReturn;
    }

    let customDataJson = JSON.parse(targetPackage.CustomData);

    let limitCount = customDataJson["total_purchasable_per_period"];

    if (limitCount == null) // not consumable by period (not daily or weekly)
    {
        return toReturn;
    }
    else 
    {
        targetPackageType = targetPackageType.concat(limitCount);
    }

    let askForUsing = false;
    if (args.askForUsing)
        askForUsing = args.askForUsing;

    if (askForUsing) {
        switch (targetPeriod) {
            case "daily":
                toReturn.error = !UseDailyLimitIfAvail(targetPackageType, targetPackageId, 1, false);
                break;
            case "weekly":
                toReturn.error = !UseWeeklyLimitIfAvail(targetPackageType, targetPackageId, 1, false);
                break;
        }
    }
    else {
        switch (targetPeriod) {
            case "daily":
                toReturn.error = !GetLeftDailyLimit(targetPackageType, targetPackageId, false);
                break;
            case "weekly":
                toReturn.error = !GetLeftWeeklyLimit(targetPackageType, targetPackageId, false);
                break;
        }
    }
    return toReturn;
}
handlers["useDailyWeeklyPackageIfAvailV2"] = UsedDailyWeeklyPackageIfAvailV2;

///////////////////////// HELPER FUNCTIONS (NOT DIRECTLY CALLABLE FROM THE CLIENT) /////////////////////////

function AddItem(itemId: string)
{
    let grantItemsToUserResult = server.GrantItemsToUser({
        PlayFabId: currentPlayerId,
        ItemIds: [itemId]
    });

    // log.info(currentPlayerId + "'s newly added item: " + grantItemsToUserResult.ItemGrantResults[0].ItemId);
}

function AddItemMultiple(itemId: string, itemCount: number)
{
    if(itemCount <= 0)
        return;
        
    let itemIds = [];
    for(let i = 0; i < itemCount; i++)
    {
        itemIds.push(itemId)
    }
        
    let grantItemsToUserResult = server.GrantItemsToUser({
        PlayFabId: currentPlayerId,
        ItemIds: itemIds
    });

    // log.info(currentPlayerId + "'s newly added item: " + itemId + "Count: " + itemCount);
}

function ConsumeItem(itemInstanceId: string, itemId: string, consumeAmount: number) {
    let consumeItemResult = server.ConsumeItem(
        {
            PlayFabId: currentPlayerId,
            ItemInstanceId: itemInstanceId,
            ConsumeCount: consumeAmount
        }
    );

    // log.info(currentPlayerId + " consumed item: " + itemInstanceId + " (" + itemId + ")" + ", (" + consumeAmount + ")");
}

function AddVirtualCurrency(currencyId: string, addAmount: number){

    // Prevent api error
    if(addAmount <= 0)
        return;

    let addUserVirtualCurrencyResult = server.AddUserVirtualCurrency({
        PlayFabId: currentPlayerId,
        VirtualCurrency: currencyId,
        Amount: addAmount
    });

    // log.info(currentPlayerId + "'s " + currencyId + " is added by " + addAmount);
}

function SubtractVirtualCurrency(currencyId: string, subtractAmount: number){
    let subtractUserVirtualCurrencyResult = server.SubtractUserVirtualCurrency({
        PlayFabId: currentPlayerId,
        VirtualCurrency: currencyId,
        Amount: subtractAmount
    });

    // log.info(currentPlayerId + "'s " + currencyId + " is subtracted by " + subtractAmount);
}

interface IAddGachaExp{
    gachaLevel: number;
    gachaExp: number;
}

function AddGachaExp(inventory: PlayFabServerModels.GetUserInventoryResult, priceOfGacha: number): IAddGachaExp{

    let toReturn: IAddGachaExp = {gachaLevel: 1, gachaExp: 0};

    // Get cumulative exp from the previous bundle purchase records
    let cumulativeExp = 0;
    let gachaBundles = inventory.Inventory.filter(n => n.ItemId.substr(0, 12) == "bundle_gacha");

    for(let i = 0; i < gachaBundles.length; i ++)
    {
        if(gachaBundles[i].ItemId == "bundle_gacha_normal_10" || gachaBundles[i].ItemId == "bundle_gacha_special_10")
        {
            cumulativeExp += gachaBundles[i].RemainingUses * 120;
        }
        else if(gachaBundles[i].ItemId == "bundle_gacha_special_01")
        {
            cumulativeExp += gachaBundles[i].RemainingUses * 12;
        }
        else
        {
            cumulativeExp += gachaBundles[i].RemainingUses * 10;
        }
    }

    // Also add this time's gacha exp to cumulative exp
    if(priceOfGacha >= 1000)
    {
        cumulativeExp += priceOfGacha * 12 / 100;
    }
    else
    {
        cumulativeExp += priceOfGacha / 10;
    }

    // List of exp needed
    let requiredGachaExpList = [0, 100, 250, 500, 1000, 2000, 4500, 9500, 19500, 44500];
    for(toReturn.gachaLevel = 0; toReturn.gachaLevel < requiredGachaExpList.length; toReturn.gachaLevel ++)
    {
        if(cumulativeExp < requiredGachaExpList[toReturn.gachaLevel])
            break;
    }
    if(cumulativeExp >= requiredGachaExpList[requiredGachaExpList.length -1])
        toReturn.gachaLevel = requiredGachaExpList.length;
    
    if(toReturn.gachaLevel != requiredGachaExpList.length)
        toReturn.gachaExp = cumulativeExp - requiredGachaExpList[toReturn.gachaLevel - 1];
    else
        toReturn.gachaExp = 0;

    // Save the cache that will be used by server
    let updateUserReadOnlyDataResult = server.UpdateUserReadOnlyData({
        PlayFabId: currentPlayerId,
        Data: {
            "gacha_level": toReturn.gachaLevel.toString(),
            "gacha_exp": toReturn.gachaExp.toString()
        }
    });

    return toReturn;
}