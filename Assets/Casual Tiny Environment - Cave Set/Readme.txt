            ▶ >>JJ Studio <<◀

Environment optimized for Mobile Action RPG implementations!

---------------------------------------------------------------
Casual Tiny Environment - Cave Set


■ Cave 1
Verts: 57365
Tris: 25898
Textures : 
 Diffuse map_ 2048x2048-1, 512x512-1
 Light map_ 1024x1024-1

■ Cave 2
Verts: 53058
Tris: 21918
Textures : 
 Diffuse map_ 2048x2048-1, 256x256-1
 Light map_ 1024x1024-1

■ Cave 3
Verts: 22842
Tris: 9081
Textures : 
 Diffuse map_ 2048x2048-1,  512x512-1
 Light map_ 1024x1024-1

■ Cave 4
Verts: 39040 
Tris: 14843 
Textures : 
 Diffuse map_ 2048x2048-1,  512x512-1
 Light map_ 1024x1024-1

■ Cave 5
Verts: 29082 
Tris: 11937 
Textures : 
 Diffuse map_ 2048x2048-1, 1024x1024-1, 512x512-1
 Light map_ 1024x1024-1

■ Cave 6
Verts: 26117 
Tris: 10159 
Textures : 
 Diffuse map_ 2048x2048-1, 512x512-1, 128x256-1
 Light map_ 1024x1024-1

■ Cave 7
Verts: 32847 
Tris: 14247 
Textures : 
 Diffuse map_ 2048x2048-1,  512x512-1
 Light map_ 1024x1024-1


3D Modeling file format is FBX.
If you want to rebake light map,
You must activate 'Light' in the environment in the scene file.
---------------------------------------------------------------



Thank you for your purchase!
If you have any questions mail me at 2018studiojj@gmail.com


▼Check out our other Assets▼
https://assetstore.unity.com/publishers/38479


