            ▶ >>JJ Studio <<◀

Monster Pack optimized for Mobile Action RPG implementations!

---------------------------------------------------------------
Casual Tiny Monster - Spinel Dragon Pack

■ Spinel Dragon
Polys: 6522
Verts: 4456
Textures: 2048x2048-1

Fx_Prefaps(x17)

Animations(x4) :
(Animation Type : Generic)
Idle, Attack1, Attack2, Attack3
---------------------------------------------------------------


Thank you for your purchase!
If you have any questions mail me at 2018studiojj@gmail.com


▼Check out our other Assets▼
https://assetstore.unity.com/publishers/38479

