            ▶ >>JJ Studio <<◀

Monster Pack optimized for Mobile Action RPG implementations!

---------------------------------------------------------------
Casual Tiny Monster - Spirit king Pack

■ Spiritking 1
Polys: 892
Verts: 863
Textures: 512x512-1

■ Spiritking 2
Polys: 576
Verts: 533
Textures: 512x512-1

■ Spiritking 3
Polys: 641
Verts: 643
Textures: 512x512-1
 
■ Spiritking 4
Polys: 932
Verts: 885
Textures: 512x512-1

■ Spiritking 5
Polys: 982
Verts: 755
Textures: 512x512-1

Fx_Prefaps(x3)

Animations(x5) :
(Animation Type_Generic)
Idle, Walk, Attack, Skill, Death
---------------------------------------------------------------



Thank you for your purchase!
If you have any questions mail me at 2018studiojj@gmail.com


▼Check out our other Assets▼
https://assetstore.unity.com/publishers/38479
