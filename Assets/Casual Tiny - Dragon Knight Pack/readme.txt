            ▶ >>JJ Studio <<◀

Character Pack optimized for Mobile Action RPG implementations!

---------------------------------------------------------------
Casual Tiny Character - Dragon Knight Pack

■ Dragonknight_01
Polys: 1872
Verts: 1410
Textures: 512x512-1, 256x256-1

■ Dragonknight_02
Polys: 2396
Verts: 2119
Textures: 512x512-1, 256x256-1 

■ Dragonknight_03
Polys: 2168
Verts: 1766
Textures: 512x512-1, 256x256-1
 
■ Dragonknight_04
Polys: 2622
Verts: 2158
Textures: 512x512-1, 256x256-1 

■ Dragonknight_05
Polys: 2258
Verts: 1820
Textures: 512x512-1, 256x256-1

■ Dragonknight_06
Polys: 2992
Verts: 2460
Textures: 512x512-1, 256x256-1 

Fx_Prefaps(x9)

Animations(x5) :
(Animation Type_Generic)
Idle, Walk, Attack, Active, Passive
---------------------------------------------------------------



Thank you for your purchase!
If you have any questions mail me at 2018studiojj@gmail.com


▼Check out our other Assets▼
https://assetstore.unity.com/publishers/38479