            ▶ >>JJ Studio <<◀

Character Pack optimized for Mobile Action RPG implementations!

---------------------------------------------------------------
Casual Tiny Character - Warlock Pack

■ Warlock 1
Polys: 1814
Verts: 1331
Textures: 512x512-1, 256x256-1

■ Warlock 2
Polys: 1729
Verts: 1271
Textures: 512x512-1, 256x256-1 

■ Warlock 3
Polys: 1634
Verts: 1217
Textures: 512x512-1, 256x256-1 
 
■ Warlock 4
Polys: 1906
Verts: 1418
Textures: 512x512-1, 256x256-1

■ Warlock 5
Polys: 1890
Verts: 1436
Textures: 512x512-1, 256x256-1

■ Warlock 6
Polys: 2128
Verts: 1774
Textures: 512x512-1, 256x256-1

Fx_Prefaps(x13)

Animations(x5) :
(Animation Type_Generic)
Idle, Walk, Attack, Active, Passive
---------------------------------------------------------------



Thank you for your purchase!
If you have any questions mail me at 2018studiojj@gmail.com


▼Check out our other Assets▼
https://assetstore.unity.com/publishers/38479