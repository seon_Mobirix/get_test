            ▶ >>JJ Studio <<◀

Monster Pack optimized for Mobile Action RPG implementations!

---------------------------------------------------------------
Casual Tiny Monster - Mummy Pack

■ Mummy 1
Polys: 688
Verts: 670
Textures: 512x512-1

■ Mummy 2
Polys: 894
Verts: 845
Textures: 512x512-1

■ Mummy 3
Polys: 1066
Verts: 985
Textures: 512x512-1
 
■ Mummy 4
Polys: 982
Verts: 991
Textures: 512x512-1

■ Mummy 5
Polys: 610
Verts: 574
Textures: 512x512-1

Fx_Prefaps(x16)

Animations(x5) :
(Animation Type_Generic)
Idle, Walk, Attack, Skill, Death
---------------------------------------------------------------



Thank you for your purchase!
If you have any questions mail me at 2018studiojj@gmail.com


▼Check out our other Assets▼
https://assetstore.unity.com/publishers/38479
