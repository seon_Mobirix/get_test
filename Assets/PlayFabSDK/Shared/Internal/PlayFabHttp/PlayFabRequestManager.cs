﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlayFab.Public;
using PlayFab.SharedModels;
using UnityEngine;
using PlayFab.Internal;
using PlayFab;
using Newtonsoft.Json;

class PlayFabRequestManager : SingletonMonoBehaviour<PlayFabRequestManager>
{
    const float CloudScriptExecutionTime = 5.0f;
    const int CloudScriptexecutionAPIRequestsIssued = 20;

    public class MakeApiCallData<TResult> : CallNodeInterface where TResult : PlayFabResultCommon
    {     
        public string apiEndpoint;
        public string fullUrl;
        public PlayFabRequestCommon request;
        public AuthType authType;
        public Action<TResult> resultCallback;
        public Action<PlayFabError> errorCallback;
        public object customData;
        public Dictionary<string, string> extraHeaders;
        public bool allowQueueing;
        public PlayFabAuthenticationContext authenticationContext;
        public PlayFabApiSettings apiSettings;
        public IPlayFabInstanceApi instanceApi;

        public long id;
        Action<CallNodeInterface> _finishCallback;
        public void MakeCallback(Action<CallNodeInterface> finishCallback)
        {
            _finishCallback = finishCallback;
            Log($"url({id:0000}):{(string.IsNullOrEmpty(apiEndpoint) ? fullUrl : apiEndpoint)}, info : {request.ToJson()}", NetType.Send);

            PlayFabHttp._exMakeApiCall<TResult>(apiEndpoint, fullUrl, request, authType, 
                tResult => ResultCallback(true, tResult, null),
                error => ResultCallback(false, null, error),
                customData, extraHeaders, allowQueueing, authenticationContext, apiSettings, instanceApi);
        }
        public void ResultCallback(bool isSuccess, TResult reult, PlayFabError err)
        {
            if (isSuccess)
            {
                Log($"url({id:0000}):{(string.IsNullOrEmpty(apiEndpoint) ? fullUrl : apiEndpoint)}, info : {reult.ToJson()}", NetType.Recive);
                resultCallback?.Invoke(reult);
            }
            else
            {
                Log($"url({id:0000}):{(string.IsNullOrEmpty(apiEndpoint) ? fullUrl : apiEndpoint)}, Error({err.Error}) : {err.ErrorMessage}", NetType.Recive_Err);
                errorCallback?.Invoke(err);
            }
            _finishCallback?.Invoke(this);
        }
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    public interface CallNodeInterface
    {
        void MakeCallback(Action<CallNodeInterface> finishCallback);
        string ToString();
    }

    public class timeNode
    {
        public float time;
        public CallNodeInterface node;
        public timeNode(CallNodeInterface node)
        {
            time = Time.unscaledTime;
            this.node = node;
        }

        public bool IsDelect()
        {
            return (Time.unscaledTime - time) >= CloudScriptExecutionTime;
        }
    }

    List<CallNodeInterface> _playNodes = new List<CallNodeInterface>();
    List<CallNodeInterface> _prcNodes = new List<CallNodeInterface>();
    List<timeNode> _timeNodes = new List<timeNode>();
    List<timeNode> _delectTimenodes = new List<timeNode>();
    long addNodeCount = 0;
    public void _MakeApiCall<TResult>(string apiEndpoint, string fullUrl,
            PlayFabRequestCommon request, AuthType authType, Action<TResult> resultCallback,
            Action<PlayFabError> errorCallback, object customData, Dictionary<string, string> extraHeaders, bool allowQueueing, PlayFabAuthenticationContext authenticationContext, PlayFabApiSettings apiSettings, IPlayFabInstanceApi instanceApi)
        where TResult : PlayFabResultCommon
    {
        Log($"url({addNodeCount:0000}):{(string.IsNullOrEmpty(apiEndpoint) ? fullUrl : apiEndpoint)} RequestAdd", NetType.Send_Add);
        StartCoroutine(UpdateCorutine(new MakeApiCallData<TResult>()
        {
            allowQueueing = allowQueueing,
            fullUrl = fullUrl,
            apiEndpoint = apiEndpoint,
            apiSettings = apiSettings,
            authenticationContext = authenticationContext,
            authType = authType,
            customData = customData,
            errorCallback = errorCallback,
            extraHeaders = extraHeaders,
            instanceApi = instanceApi,
            request = request,
            resultCallback = resultCallback,

            id = addNodeCount++
        }));
    }

    IEnumerator UpdateCorutine(CallNodeInterface node)
    {
        _playNodes.Add(node);
        if (_playNodes.Count > 1)
            yield break;
                
        while (_playNodes.Count > 0)
        {
            if (!IsCheckNodeCount())
                yield return NodeTimeCheckProcess();

            UpdateCorutine_1(_playNodes[0]);

            if (_prcNodes.Count >= 3)
                yield return new WaitUntil(() => _prcNodes.Count < 3);
        }
    }

    void UpdateCorutine_1(CallNodeInterface node)
    {
        _timeNodes.Add(new timeNode(node));
        _prcNodes.Add(node);
        node.MakeCallback(p =>
        {
            _prcNodes.Remove(p);            
        });
        _playNodes.Remove(node);
    }
    
    bool IsCheckNodeCount()
    {
        _timeNodes.ForEach(p =>
        {
            if (p.IsDelect()) _delectTimenodes.Add(p);
        });
        if (_delectTimenodes.Count > 0)
        {
            _delectTimenodes.ForEach(p => _timeNodes.Remove(p));
            _delectTimenodes.Clear();
        }
        var ret = (_timeNodes.Count < CloudScriptexecutionAPIRequestsIssued);
        if (!ret)
            Debug.LogError($"PlayFabRequest Count Over in 5Sec : LastRequest({_timeNodes[_timeNodes.Count - 1].node.ToString()})");
        return (_timeNodes.Count < CloudScriptexecutionAPIRequestsIssued);
    }

    IEnumerator NodeTimeCheckProcess()
    {
        do
        {
            yield return null;
            if (IsCheckNodeCount())
                break;
        }
        while (_timeNodes.Count >= CloudScriptexecutionAPIRequestsIssued);
    }

    public enum NetType
    {
        None = 0,
        Send_Add,
        Send,
        Recive,
        Recive_Err,
    }
    static public void Log(string msg, NetType netType = NetType.None)
    {
#if UNITY_EDITOR
        switch(netType)
        {
            case NetType.None:
                Debug.Log(msg);
                break;
            case NetType.Send_Add:
                WriteUnityLogWithColor(msg, "[Net] >>>> ", "#BDB76B", p => Debug.Log(p));
                break;
            case NetType.Send:
                WriteUnityLogWithColor(msg, "[Net] >>>> ", "orange", p => Debug.Log(p));
                break;
            case NetType.Recive:
                WriteUnityLogWithColor(msg, "[Net] <<<< ", "#EEE8AA", p => Debug.Log(p));
                break;
            case NetType.Recive_Err:
                WriteUnityLogWithColor(msg, "[Net] <<<< ", "#DC143C", p => Debug.Log(p));
                break;
        }
#else
        Debug.Log(msg);
#endif
    }

    /// <summary>
	/// 너무 긴 로그 텍스트에 대해서 컬러를 잃어버리지 않게 추가 처리.
	/// </summary>
	static void WriteUnityLogWithColor(string msg, string tag, string colorValue, System.Action<string> debugLogFunction)
    {
        //// 긴 텍스트는 위쪽 콘솔에서 color를 잃어버리므로 적절히 자른 요약본을 컬러로 표시. 또 본문을 이어서 표시.
        int pos = 0;

        // 라인엔딩이 있으면 콘솔에서 색상이 사라지므로 한줄 한줄 분리하여 색을 넣어줘야 한다.
        msg = msg.Replace("\r\n", $"</color>\r\n<color={colorValue}>");

        // longMsg 출력
        const int maxAddCount = 100;
        const int SegmentSize = 11000;
        int startLogPos = 0;
        pos = 0;
                
        for (int i = 0; i < maxAddCount && startLogPos < msg.Length; i++)
        {
            pos += Mathf.Min(msg.Length - pos, SegmentSize);

            while (pos < msg.Length && !(msg[pos] == ',' || msg[pos] == '\"')) // 시퀀스문자에서 끊으면 출력오류나므로 스페이스문자에서 끊는다.
                pos++;

            string frontMsg = (i == 0) ? string.Empty : $"<color=#ffffff>(===============>)</color> ";

            // 메시지 출력
            string viewMsg = msg.Substring(startLogPos, pos - startLogPos);

            if (colorValue == null)
                debugLogFunction?.Invoke(viewMsg);
            else
            {
                if(string.IsNullOrEmpty(tag))
                    debugLogFunction?.Invoke($"{frontMsg}<color={colorValue}>{viewMsg}</color>");
                else
                    debugLogFunction?.Invoke($"{frontMsg}<color={colorValue}>[{tag}]{viewMsg}</color>");
            }
            startLogPos = pos;
        }
    }
}
