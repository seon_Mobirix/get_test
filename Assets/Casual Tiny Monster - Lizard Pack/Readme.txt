            ▶ >>JJ Studio <<◀

Monster Pack optimized for Mobile Action RPG implementations!

---------------------------------------------------------------
Casual Tiny Monster - Lizard Pack

■ Lizard 1
Polys: 932
Verts: 772
Textures: 512x512-1, 256x256-1

■ Lizard 2
Polys: 1018
Verts: 846
Textures: 512x512-1, 256x256-1

■ Lizard 3
Polys: 932
Verts: 772
Textures: 512x512-1, 256x256-1
 
■ Lizard 4
Polys: 1018
Verts: 846
Textures: 512x512-1, 256x256-1 

Fx_Prefaps(x1)

Animations(x5) :
(Animation Type : Generic)
Idle, Walk, Attack, Skill, Death
---------------------------------------------------------------



Thank you for your purchase!
If you have any questions mail me at 2018studiojj@gmail.com


▼Check out our other Assets▼
https://assetstore.unity.com/publishers/38479


