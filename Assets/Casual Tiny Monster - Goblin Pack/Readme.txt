            ▶ >>JJ Studio <<◀

Monster Pack optimized for Mobile Action RPG implementations!

---------------------------------------------------------------
Casual Tiny Monster - Goblin Pack

■ Goblin axe 1
Polys: 526
Verts: 439
Textures: 512x512-1, 256x256-1

■ Goblin bomb 1
Polys: 432
Verts: 392
Textures: 512x512-1, 256x256-1

■ Goblin fighter 1
Polys: 478
Verts: 445
Textures: 512x512-1, 256x256-1
 
■ Goblin shman 1
Polys: 560
Verts: 481
Textures: 512x512-1, 256x256-1 

■ Goblin axe 2
Polys: 526
Verts: 439
Textures: 512x512-1, 256x256-1

■ Goblin bomb 2
Polys: 432
Verts: 392
Textures: 512x512-1, 256x256-1

■ Goblin fighter 2
Polys: 478
Verts: 445
Textures: 512x512-1, 256x256-1
 
■ Goblin shman 2
Polys: 560
Verts: 481
Textures: 512x512-1, 256x256-1 

Animations(x5) :
(Animation Type :Generic)
Idle, Walk, Attack, Skill, Death
---------------------------------------------------------------

Thank you for your purchase!
If you have any questions mail me at 2018studiojj@gmail.com


▼Check out our other Assets▼
https://assetstore.unity.com/publishers/38479
