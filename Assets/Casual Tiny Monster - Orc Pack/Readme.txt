            ▶ >>JJ Studio <<◀

Monster Pack optimized for Mobile Action RPG implementations!

---------------------------------------------------------------
Casual Tiny Monster - Orc Pack

■ Orc 1
Polys: 1012
Verts: 744
Textures: 512x512-1

■ Orc 2
Polys: 847
Verts: 768
Textures: 512x512-1

■ Orc 3
Polys: 939
Verts: 782
Textures: 512x512-1
 
■ Orge 1
Polys: 714
Verts: 688
Textures: 512x512-1


Fx_Prefaps(x6)

Animations(x5) :
(Animation Type_Generic)
Idle, Walk, Attack, Skill, Death
---------------------------------------------------------------


Thank you for your purchase!
If you have any questions mail me at 2018studiojj@gmail.com


▼Check out our other Assets▼
https://assetstore.unity.com/publishers/38479
