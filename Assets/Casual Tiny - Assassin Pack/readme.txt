            ▶ >>JJ Studio <<◀

Character Pack optimized for Mobile Action RPG implementations!

---------------------------------------------------------------
Casual Tiny Character - Assassin Pack

■ Assassin 1
Polys: 1846
Verts: 1456
Textures: 512x512-1, 256x256-1

■ Assassin 2
Polys: 2465
Verts: 1955
Textures: 512x512-1, 256x256-1

■ Assassin 3
Polys: 2602
Verts: 1985
Textures: 512x512-1, 256x256-1
 
■ Assassin 4
Polys: 3115
Verts: 2799
Textures: 512x512-1, 256x256-1 

■ Assassin 5
Polys: 3966
Verts: 3706
Textures: 512x512-1, 256x256-1

■ Assassin 6
Polys: 3878
Verts: 2964
Textures: 512x512-1, 256x256-1

Fx_Prefaps(x12)

Animations(x5) :
(Animation Type_Generic)
Idle, Walk, Attack, Active, Passive
---------------------------------------------------------------



Thank you for your purchase!
If you have any questions mail me at 2018studiojj@gmail.com


▼Check out our other Assets▼
https://assetstore.unity.com/publishers/38479