﻿using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using UnityEditor.AnimatedValues;
using System.Collections;

namespace UnityEditor.UI
{
    [CustomEditor(typeof(AdvancedText), false)]
    [CanEditMultipleObjects]
    public class AdvancedTextEditor : TextEditor
    {
        [MenuItem("GameObject/UI/Advanced Text", false, 3)]
        static void CreatePanel()
        {
           GameObject spriteObject = new GameObject("Text");
            if (Selection.activeGameObject != null)
            {
                spriteObject.transform.parent = Selection.activeGameObject.transform;
                spriteObject.layer = Selection.activeGameObject.layer;
            }
            else
            {
                Canvas mainCanvas = GameObject.FindObjectOfType<Canvas>();
                if (mainCanvas != null)
                {
                    spriteObject.transform.parent = mainCanvas.transform;
                    spriteObject.layer = mainCanvas.gameObject.layer;
                }
            }
            AdvancedText t = spriteObject.AddComponent<AdvancedText>();
            t.supportRichText = false;
            t.raycastTarget = false;
            Selection.objects = new Object[] { spriteObject };
        }

        SerializedProperty m_EffectType;
        protected override void OnEnable()
        {
            base.OnEnable();
            m_EffectType = serializedObject.FindProperty("m_EffectType");
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            EditorGUILayout.PropertyField(m_EffectType);
            serializedObject.ApplyModifiedProperties();
        }
    }
}
