﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using UnityEngine.Serialization;
using System;

namespace UnityEngine.UI
{
    [RequireComponent(typeof(RectTransform))]
    [RequireComponent(typeof(CanvasRenderer))]
    [AddComponentMenu("UI/AdvancedText", 12)]
    public class AdvancedText : Text
    {
        public enum TextEffectType
        {
            NONE,
            OUTLINE,
            OUTLINE_SHADOW
        }

        [SerializeField]
        private TextEffectType m_EffectType = TextEffectType.NONE;
        public TextEffectType effectType
        {
            get { return m_EffectType; }
            set
            {
                if (m_EffectType != value)
                {
                    m_EffectType = value;
                    SetVerticesDirty();
                }
            }
        }

        private Color m_EffectColor = Color.black;

        readonly UIVertex[] m_TempVerts = new UIVertex[4];
        
        protected override void OnPopulateMesh(VertexHelper toFill)
        {
            if (font == null)
                return;

            // We don't care if we the font Texture changes while we are doing our Update.
            // The end result of cachedTextGenerator will be valid for this instance.
            // Otherwise we can get issues like Case 619238.
            m_DisableFontTextureRebuiltCallback = true;

            Vector2 extents = rectTransform.rect.size;

            var settings = GetGenerationSettings(extents);
            cachedTextGenerator.PopulateWithErrors(text, settings, gameObject);

            // Apply the offset to the vertices
            IList<UIVertex> verts = cachedTextGenerator.verts;
            float unitsPerPixel = 1 / pixelsPerUnit;
            int vertCount = verts.Count;

            // We have no verts to process just return (case 1037923)
            if (vertCount <= 0)
            {
                toFill.Clear();
                return;
            }

            Vector2 roundingOffset = (verts.Count > 0) ? new Vector2(verts[0].position.x, verts[0].position.y) * unitsPerPixel : Vector2.zero;
            roundingOffset = PixelAdjustPoint(roundingOffset) - roundingOffset;
            toFill.Clear();

            bool needOffest = roundingOffset != Vector2.zero;

            // Draw shadow
            for (int i = 0; i < vertCount; i += 4)
            {
                if (verts[i].position == verts[i + 1].position)
                    continue;

                for (int j = 0; j < 4; ++j)
                {
                    m_TempVerts[j] = verts[i + j];
                    m_TempVerts[j].position *= unitsPerPixel;
                    if (needOffest)
                    {
                        m_TempVerts[j].position.x += roundingOffset.x;
                        m_TempVerts[j].position.y += roundingOffset.y;
                    }
                }

                if (m_EffectType != TextEffectType.NONE)
                {
                    Vector2 bottomLeft = m_TempVerts[0].uv0;
                    Vector2 topRight = m_TempVerts[2].uv0;
                    if (bottomLeft.x > topRight.x)
                    {
                        bottomLeft = m_TempVerts[2].uv0;
                        topRight = m_TempVerts[0].uv0;
                    }
                    Vector4 uvBounds = new Vector4(bottomLeft.x, bottomLeft.y, topRight.x, topRight.y);
                    m_TempVerts[0].tangent = uvBounds;
                    m_TempVerts[1].tangent = uvBounds;
                    m_TempVerts[2].tangent = uvBounds;
                    m_TempVerts[3].tangent = uvBounds;

                    if (m_EffectType == TextEffectType.OUTLINE_SHADOW)
                    {
                        ApplyColor(m_TempVerts, m_EffectColor);

                        if(this.fontSize >= 64)
                        {
                            ApplyOffestX(m_TempVerts, 0f);
                            ApplyOffestY(m_TempVerts, -5f);
                        }
                        else if(this.fontSize >= 32)
                        {
                            ApplyOffestX(m_TempVerts, 0f);
                            ApplyOffestY(m_TempVerts, -3f);
                        }
                        else
                        {
                            ApplyOffestX(m_TempVerts, 0f);
                            ApplyOffestY(m_TempVerts, -2f);
                        }

                        toFill.AddUIVertexQuad(m_TempVerts);
                    }
                };
            };

            // Draw font
            for (int i = 0; i < vertCount; i += 4)
            {
                if (verts[i].position == verts[i + 1].position)
                    continue;

                for (int j = 0; j < 4; ++j)
                {
                    m_TempVerts[j] = verts[i + j];
                    m_TempVerts[j].position *= unitsPerPixel;
                    if (needOffest)
                    {
                        m_TempVerts[j].position.x += roundingOffset.x;
                        m_TempVerts[j].position.y += roundingOffset.y;
                    }
                }

                if (m_EffectType != TextEffectType.NONE)
                {
                    Vector2 bottomLeft = m_TempVerts[0].uv0;
                    Vector2 topRight = m_TempVerts[2].uv0;
                    if (bottomLeft.x > topRight.x)
                    {
                        bottomLeft = m_TempVerts[2].uv0;
                        topRight = m_TempVerts[0].uv0;
                    }
                    Vector4 uvBounds = new Vector4(bottomLeft.x, bottomLeft.y, topRight.x, topRight.y);
                    m_TempVerts[0].tangent = uvBounds;
                    m_TempVerts[1].tangent = uvBounds;
                    m_TempVerts[2].tangent = uvBounds;
                    m_TempVerts[3].tangent = uvBounds;
                }

                toFill.AddUIVertexQuad(m_TempVerts);
            };

            m_DisableFontTextureRebuiltCallback = false;
        }

        void ApplyColor(UIVertex[] vertexs, Color32 effectColor)
        {
            vertexs[0].color = effectColor;
            vertexs[1].color = effectColor;
            vertexs[2].color = effectColor;
            vertexs[3].color = effectColor;
        }

        void ApplyOffestX(UIVertex[] vertexs, float v)
        {
            vertexs[0].position.x += v;
            vertexs[1].position.x += v;
            vertexs[2].position.x += v;
            vertexs[3].position.x += v;
        }

        void ApplyOffestY(UIVertex[] vertexs, float v)
        {
            vertexs[0].position.y += v;
            vertexs[1].position.y += v;
            vertexs[2].position.y += v;
            vertexs[3].position.y += v;
        }
    }
}
