﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniTutorialController : MonoBehaviour
{
    public static MiniTutorialController Instance;

    [SerializeField]
    private Transform tutorial1 = null;

    [SerializeField]
    private Transform tutorial2 = null;

    [SerializeField]
    private Transform tutorial3 = null;

    public bool NeedTutorial1Stop = false;
    public bool NeedTutorial2Stop = false;
    public bool NeedTutorial3Stop = false;

    private void Awake(){
		MiniTutorialController.Instance = this;
	}

    public void CheckTutorial()
    {
        if(!PlayerPrefs.HasKey("tutorial_ever"))
        {
            StopAllCoroutines();
            StartCoroutine(TutorialCoroutine());
            PlayerPrefs.SetInt("tutorial_ever", 1);
        }   
    }

    private float passed = 0f;
    private IEnumerator TutorialCoroutine()
    {
        yield return new WaitForSeconds(15f);

        if(ServerNetworkManager.Instance.User.StageProgress == 0 || ServerNetworkManager.Instance.User.StageProgress == 1)
        {
            tutorial2.gameObject.SetActive(true);
            while(passed < 30f && !NeedTutorial1Stop)
            {
                passed += Time.deltaTime;
                yield return -1;
            }
            tutorial2.gameObject.SetActive(false);
        }
        passed = 0f;

        if(
            !PlayerPrefs.HasKey("buff_ever") 
            && ServerNetworkManager.Instance.User.AttackBuffTier == (int)BuffTier.NONE
            && ServerNetworkManager.Instance.User.SpeedBuffTier == (int)BuffTier.NONE
            && ServerNetworkManager.Instance.User.GoldBuffTier == (int)BuffTier.NONE
        )
        {
            tutorial1.gameObject.SetActive(true);
            while(passed < 30f && !NeedTutorial2Stop)
            {
                passed += Time.deltaTime;
                yield return -1;
            }
            tutorial1.gameObject.SetActive(false);
        }
        passed = 0f;

        if(!PlayerPrefs.HasKey("ui_camera_follow_off"))
        {
            tutorial3.gameObject.SetActive(true);
            while(passed < 30f && !NeedTutorial3Stop)
            {
                passed += Time.deltaTime;
                yield return -1;
            }
            tutorial3.gameObject.SetActive(false);
        }

    }
}
