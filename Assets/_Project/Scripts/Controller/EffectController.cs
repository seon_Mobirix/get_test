﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EffectController : MonoBehaviour
{
    public static EffectController Instance;

    [SerializeField]
    private List<Transform> hitEffects;

    [SerializeField]
    private List<Transform> goldEffects;

    private void Awake(){
		EffectController.Instance = this;
	}

    public void ShowHitEffect(Transform target, bool isHeroAttack)
    {
        QuickPool.PoolsManager.Spawn(
            hitEffects[0].gameObject, 
            target.position 
                + new Vector3(
                    Random.Range(-0.25f, 0.5f), 
                    Random.Range(1f, 1.5f) * target.localScale.y, 
                    Random.Range(-0.25f, 0.5f) * target.localScale.z)
                - Camera.main.transform.forward * 2f, 
            Quaternion.identity
        );
       
        if(isHeroAttack && IngameMainController.Instance.IsBerserk)
        {
            ShowGoldEffect(target, true);
        }
    }

    public void ShowHitEffect(Transform target, GameObject hitEffect)
    {
        QuickPool.PoolsManager.Spawn(
            hitEffect.gameObject, 
            target.position 
                + new Vector3(
                    Random.Range(-0.25f, 0.5f), 
                    Random.Range(1f, 1.5f) * target.localScale.y, 
                    Random.Range(-0.25f, 0.5f) * target.localScale.z)
                - Camera.main.transform.forward * 2f, 
            Quaternion.identity
        );
    }

    public void ShowHitEffect(Transform attacker)
    {
        QuickPool.PoolsManager.Spawn(
            hitEffects[0].gameObject, 
            attacker.position
                + attacker.forward * 5f
                + new Vector3(Random.Range(-0.25f, 0.5f), Random.Range(1f, 1.5f), Random.Range(-0.25f, 0.5f))
                - Camera.main.transform.forward * 2f,
            Quaternion.identity
        );
    }

    public void ShowGoldEffect(Transform target, bool isMany)
    {
        if(!isMany)
            QuickPool.PoolsManager.Spawn(goldEffects[0].gameObject, target.position + target.localScale.y * Vector3.up * 2f, Quaternion.Euler(-90f, 0f, 0f));
        else
            QuickPool.PoolsManager.Spawn(goldEffects[0].gameObject, target.position + target.localScale.y * Vector3.up * 2f, Quaternion.Euler(-90f, 0f, 0f));
    }
}
