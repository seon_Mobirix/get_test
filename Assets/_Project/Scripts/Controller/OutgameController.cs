﻿using System;
using System.Linq;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Text.RegularExpressions;

using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Advertisements;

using CodeStage.AntiCheat.Storage;

public class OutgameController : MonoBehaviour 
{
    public static OutgameController Instance;

    [Title ("Config Text")]

    [SerializeField]
	private TextAsset filterText = null;
	private string[] filterLlist = null;

    [Title ("Interact Audio")]

    [SerializeField]
    private AudioSource audioSource;
    [SerializeField]
    private AudioClip loadingSound;
    [SerializeField]
    private AudioClip bgmSound;
    
    [Title ("Interact Views")]
    
    [SerializeField]
    private UIViewMain uiViewOutgameMain;

    [SerializeField]
    private UIViewFade uiViewFade;

    [SerializeField]
    private UIViewPowerSaving uiViewPowerSaving;

    [SerializeField]
    private UIViewLoadingInitial uiViewOutgameLoadingInitial;

    [Title ("Interact Popups")]

    [SerializeField]
    private UIPopupToast uiPopupToast;

    [SerializeField]
    private UIPopupToast uiPopupToastInOverFade;
    
    [SerializeField]
    private UIPopupGDPR uiPopupGDPR;

    [SerializeField]
    private UIPopupTermsAndPrivacy uiPopupTermsAndPrivacy;

    [SerializeField]
    private UIPopupOpening uiPopupOpening;

    [SerializeField]
    private UIPopupSelectAuthMethod uiAuthMethodSelect;

    [SerializeField]
    private UIPopupNicknameCreate uiPopupNicknameCreate;

    [SerializeField]
    public UIPopupStatInfo uiPopupStatInfo;
    
    [SerializeField]
    private UIPopupNotice uiPopupNotice;

    [SerializeField]
    private UIPopupInbox2 uiPopupInbox2;

    [SerializeField]
    private UIPopupOption uiPopupOption;

    [SerializeField]
    private UIPopupNicknameChange uiPopupNicknameChange;

    [SerializeField]
    private UIPopupCoupon uiPopupCoupon;

    [SerializeField]
    private UIPopupSelectAuthMethod uiPopupRelogin;

    [SerializeField]
    private UIPopupReceivedReward uiPopupReceivedReward;

    [SerializeField]
    private UIPopupSummon2 uiPopupSummon2;

    [SerializeField]
    private UIPopupSummonResult uiPopupSummonResult;

    [SerializeField]
    private UIPopupSummonLevelInfo uiPopupSummonLevelInfo;

    [SerializeField]
    private UIPopupRanking uiPopupRanking;

    [SerializeField]
    private UIPopupArena uiPopupArena;

    [SerializeField]
    private PopupArenaContinueBattle uiPopupArenaContinueBattle;

    [SerializeField]
    private UIPopupRaid uiPopupRaid;

    [SerializeField]
    private UIPopupRaidTicketPurchase uiPopupRaidTicketPurchase;

    [SerializeField]
    private UIPopupEquipment uiPopupEquipment;

    [SerializeField]
    private UIPopupUpgrade uiPopupUpgrade;

    [SerializeField]
    private UIPopupAchievement uiPopupAchievement;
    
    [SerializeField]
    private UIPopupPet uiPopupPet;

    [SerializeField]
    public UIPopupChat uiPopupChat;

    [SerializeField]
    public UIPopupReport uiPopupReport;

    [SerializeField]
    public UIPopupChatBlock uiPopupChatBlock;

    [SerializeField]
    public UIPopupWorldInfo uiPopupWorldInfo;

    [SerializeField]
    public UIPopupGachaInfo uiPopupGachaInfo;

    [SerializeField]
    public UIPopupShop uiPopupShop;

    [SerializeField]
    public UIPopupGrowthSupport2 uiPopupGrowthSupport2;

    [SerializeField]
    public UIPopupGrowthSupportPurchase uiPopupGrowthSupportPurchase;

    [SerializeField]
    public UIPopupRestReward uiPopupRestReward;

    [SerializeField]
    public UIPopupDailyReward uiPopupDailyReward;

    [SerializeField]
    public UIPopupGenericInfo uiPopupGenericInfo;

    [SerializeField]
    public UIPopupMoreGame uiPopupMoreGame;

    [SerializeField]
    public UIPopupAttendanceEvent uiPopupAttendanceEvent;

    [SerializeField]
    public UIPopupTower uiPopupTower;

    [SerializeField]
    public UIPopupTowerPlayer uiPopupTowerPlayer;

    [SerializeField]
    public UIPopupWing uiPopupWing;

    [SerializeField]
    public UIPopupGuild uiPopupGuild;

    [SerializeField]
    public UIPopupGuildCreate uiPopupGuildCreate;

    [SerializeField]
    public UIPopupGuildSetting uiPopupGuildSetting;

    [SerializeField]
    public UIPopupApplication uiPopupApplication;

    [SerializeField]
    public UIPopupGuildKickList uiPopupGuildKickList;

    [SerializeField]
    public UIPopupGuildDonate uiPopupGuildDonate;

    private LinkedList<UIPopup> openedPopupStack = new LinkedList<UIPopup>();

    // For data sync cool time for option popup
    public int TimeSyncPlayerData = 0;

    // For cool time for chat popup
    public int TimeChatFunction = 0;

    // For cool time for tower popup
    public int TimeTowerFunction = 0;

    // For cool time for guild popup
    public int TimeGuildFunction = 0;
    public int TimeGuildRaidFunction = 0;
    public int TimeGuildRelicFunction = 0;

    // For re-focusing behavior
    [SerializeField]
    private double lastLostFocusTime = -1f;

    private double refocusLimit = 120d;

    // For complying google's request
    [SerializeField]
    private bool explicitSocialSignout;

    public bool ExplicitSocialSignout{
        get{
            return explicitSocialSignout;
        }
    }

    // For tower reddot related with battle record
    private double lastCloseTowerPopupTimeStampxMinute = -1d;
    public double LastCloseTowerPopupTimeStampMinute{
        get{
            if(lastCloseTowerPopupTimeStampxMinute != -1d)
                return lastCloseTowerPopupTimeStampxMinute;
            else
            {
                if(PlayerPrefs.HasKey("last_close_tower_popup_timestamp"))
                    lastCloseTowerPopupTimeStampxMinute = double.Parse(PlayerPrefs.GetString("last_close_tower_popup_timestamp"), NumberStyles.Any, CultureInfo.InvariantCulture);
                else
                    lastCloseTowerPopupTimeStampxMinute = 0d;

                return lastCloseTowerPopupTimeStampxMinute;
            }
        }
        set{
            PlayerPrefs.SetString("last_close_tower_popup_timestamp", value.ToString());
            lastCloseTowerPopupTimeStampxMinute = value;
        }
    }

    private void Awake()
	{
		OutgameController.Instance = this;
	}
    
    private void Start()
    {
        if(Application.genuineCheckAvailable && !Application.genuine)
        {
            UIManager.Instance.ShowAlert("Application is not genuine.", null, null, true);
            return;
        }

        if(Application.sandboxType == ApplicationSandboxType.SandboxBroken)
        {
            UIManager.Instance.ShowAlert("Application is running on a rooted device.", null, null, true);
            return;
        }

#if !UNITY_EDITOR && !BYPASS_LIAPP
        LiappManager.Instance.StartLiapp(
            () => {
                // Need to retrieve auth key before trying log in.
                if(ServerNetworkManager.Instance.IsLoggedIn)
                {
                    RefreshLastDeviceForLogin(true);
                }
                else
                {
                    if(PlayerPrefs.HasKey("explicit_sign_out")){
                        explicitSocialSignout = true;
                    }

                    AskPolicyAgree();
                }
            }
        );
#else
        // Need to retrieve auth key before trying log in.
        if(ServerNetworkManager.Instance.IsLoggedIn)
        {
            RefreshLastDeviceForLogin(true);
        }
        else
        {
            if(PlayerPrefs.HasKey("explicit_sign_out")){
                explicitSocialSignout = true;
            }

            AskPolicyAgree();
        }
#endif

    }

    private bool isClosing = false;
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape) && !isClosing)
		{
			if(!UIManager.Instance.IsCanvasShowing("Loading Canvas") && !UIManager.Instance.IsCanvasShowing("Fade Canvas"))
            {
                if(openedPopupStack.Count == 0)
                {
                    UIManager.Instance.ShowAlertLocalized(
                        "message_quit_check", 
                        OnQuit, 
                        () => {}
                    );
                }
                else if(openedPopupStack.Last() != null)
                {
                    var targetPopup = openedPopupStack.Last();
                    targetPopup.Close();
                }
            }
		}
    }

    private IEnumerator LogicOnEverySecondCoroutine()
    {
        var lastLocalCacheSaveTime = (int)Time.realtimeSinceStartup;
        var lastTime = (int)Time.realtimeSinceStartup;
        while(true)
        {
            var currentTime = (int)Time.realtimeSinceStartup;

            if(currentTime - lastTime >= 1)
            {
                if(BuffManager.Instance.AttackBuffTimeLeftSecond > 0 || BuffManager.Instance.GoldBuffTimeLeftSecond > 0 
                || BuffManager.Instance.SpeedBuffTimeLeftSecond > 0|| BuffManager.Instance.AutoEnableTimeLeftSecond > 0)
                    uiViewOutgameMain.RefreshLeftBuffHorizontalBar();

                uiViewOutgameMain.RefreshFreeGem();

                // Update achievement related with play time
                UpdateAchievement("achievement_02", 1);

                if((!uiViewPowerSaving.IsOn && currentTime - lastLocalCacheSaveTime >= 3) || currentTime - lastLocalCacheSaveTime >= 10)
                {
                    // Save current progress locally
                    SaveLocalCache(false);

                    lastLocalCacheSaveTime = currentTime;
                }

                // Check battery and internet connection
                if(uiViewPowerSaving.IsOn)
                {
                    uiViewPowerSaving.UpdateBatteryInfo();
                    uiViewPowerSaving.UpdateWifiInfo();
                }

                lastTime = currentTime;
            }
            
            yield return -1;
        }
    }

    private IEnumerator LogicOnEverySomeSecondsCoroutine()
    {
        var lastLocalCacheSaveTime = (int)Time.realtimeSinceStartup;
        var lastTime = (int)Time.realtimeSinceStartup;
        while(true)
        {
            var currentTime = (int)Time.realtimeSinceStartup;

            if(currentTime - lastTime >= ServerNetworkManager.Instance.Setting.MultiDeviceCheckPeriod)
            {
                RefreshLastDeviceBackground();
                lastTime = currentTime;
            }

            yield return -1;
        }
    }

    private IEnumerator LogicOnEverySomeMinutesCoroutine()
    {
        var lastTimeForGetRanking = (int)Time.realtimeSinceStartup;
        var lastTimeForAutoSync = (int)Time.realtimeSinceStartup;
        var lastTimeForTowerRefresh = (int)Time.realtimeSinceStartup;
        while(true)
        {
            var currentTime = (int)Time.realtimeSinceStartup;

            if(currentTime - lastTimeForGetRanking >= ServerNetworkManager.Instance.Setting.GetRankingPeriod)
            {
                CheckRanking();
                lastTimeForGetRanking = currentTime;
            }
            else if(currentTime - lastTimeForAutoSync >= ServerNetworkManager.Instance.Setting.IdleAutoSyncPeriod)
            {
                if(uiViewPowerSaving.IsOn)
                    SyncPlayerData(false, "LogicOnEverySomeMinutesCoroutine", null, true);
                lastTimeForAutoSync = currentTime;
            }
            else if(currentTime - lastTimeForTowerRefresh >= ServerNetworkManager.Instance.Setting.TowerDataRefreshPeriod)
            {
                RefreshTowerBackground();
                lastTimeForTowerRefresh = currentTime;
            }

            yield return new WaitForSeconds(5f);
        }
    }

    private void OnQuit()
    {
        isClosing = true;
        Application.Quit();
    }

    private void OnApplicationPause(bool pauseStatus)
    {
        if(!pauseStatus)
        {
            if(ServerNetworkManager.Instance.IsLoggedIn)
            {
                if(!UIManager.Instance.IsCanvasShowing("Loading Canvas") && !UIManager.Instance.IsCanvasShowing("Fade Canvas"))
                {
                    RefreshForApplicationRefocused();
#if UNITY_ANDROID
                    // Check for the case when authentication lost after login.
                    if(OptionManager.Instance.LoginMode == OptionManager.LoginModeType.PLATFORM_DEFAULT && !SocialPlatformManager.Instance.IsSocialAuthenticatedProperly)
                    {
                        this.ExplicitSignOutSocial();
                    }
#endif
                }
            }
        }
        else
        {
            SaveLocalCache(true);
            lastLostFocusTime = DateTimeOffset.Now.ToUnixTimeSeconds();
        }
    }

    public void RefreshForApplicationRefocused()
    {
        if(DateTimeOffset.Now.ToUnixTimeSeconds() - lastLostFocusTime >= refocusLimit && refocusLimit != -1d)
        {
            SceneChangeManager.Instance.ForceShowPromotion = true;
            ServerNetworkManager.Instance.Logout();
            ChatManager.Instance.Disconnect();
            SceneChangeManager.Instance.LoadMainGame(true, true);
        }
        refocusLimit = 45d;
    }

#region UI

    public void ShowGDPRPopup()
    {
        uiPopupGDPR.Open();
    }

    public void ShowTermsAndPrivacyPopup()
    {
        uiPopupTermsAndPrivacy.Open();
    }

    public void ShowOpeningPopup()
    {
        uiPopupOpening.Open();
    }

    public void CheckEnjoyingPopup()
    {
        if(!PlayerPrefs.HasKey("is_reviewed_market") && !PlayerPrefs.HasKey("is_not_enjoying"))
        {
            UIManager.Instance.ShowAlertLocalized(
                "message_check_enjoying",
                () => {
                    CheckReviewPopup();
                },
                () => {
                    PlayerPrefs.SetInt("is_not_enjoying", 1);
                },
                false,
                true
            );
        }
    }

    private void CheckReviewPopup()
    {
        UIManager.Instance.ShowAlertLocalized(
            "message_review_market",
            () => {
                PageOpenController.Instance.OpenReview();
                PlayerPrefs.SetInt("is_reviewed_market", 1);
            },
            () =>{}
        );
    }

    public void CheckServerLanguage()
    {
        if(!PlayerPrefs.HasKey("is_server_lanugage_set") || PlayerPrefs.GetInt("is_server_lanugage_set") != 1)
        {
            ChangeServerLanguage();
        }
    }

    public void ShowDailyRewardPopup()
    {
        ShowLoading();

        ServerNetworkManager.Instance.GetDailyRewardInfo(
            (dailyRewardTicketRechargeTimes) => {
                HideLoading();
                uiPopupDailyReward.OpenWithValues(dailyRewardTicketRechargeTimes);
            },
            () => {
                HideLoading();
                UIManager.Instance.ShowAlertLocalized("message_etc", () => {}, null);
            }
        );
    }

    public void RefreshDailyRewardPopup(Action<int> onSucces)
    {
        ShowLoading();

        ServerNetworkManager.Instance.GetDailyRewardInfo(
            (dailyRewardTicketRechargeTimes) => {
                HideLoading();
                onSucces(dailyRewardTicketRechargeTimes);
            },
            () => {
                HideLoading();
                UIManager.Instance.ShowAlertLocalized("message_etc", () => {}, null);
            }
        );
    }

    public void ShowStatInfoPopup()
    {
        uiPopupStatInfo.Open();
    }
    
    public void ShowNicknameChangePopup()
    {
        uiPopupNicknameChange.Open();
    }

    public void ShowCouponPopup()
    {
        uiPopupCoupon.Open();
    }
    
    public void ShowInboxPopup()
    {
        ShowLoading();
        
        UpdateMailList(
            () => {
                HideLoading();
                uiPopupInbox2.OpenWithValues(0);
            },
            () => {
                HideLoading();
                UIManager.Instance.ShowAlert(
                    LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_etc") +  " (Failed to update user inbox data.)",
                    null, 
                    null
                );
            }
        );
    }

    public void ShowReloginPopup()
    {
        if(OptionManager.Instance.LoginMode == OptionManager.LoginModeType.GUEST && !SocialPlatformManager.Instance.IsSocialAuthenticatedProperly)
        {
            UIManager.Instance.ShowAlertLocalized(
                "message_guest_mode", 
                () => {
                    uiPopupRelogin.OpenWithValues(false);
                }, 
                () => {}
            );
        }
        else
        {
            uiPopupRelogin.OpenWithValues(false);
        }
    }

    public void ShowOptionPopup()
    {
        uiPopupOption.OpenWithValues(0);
    }

    public void ShowSummon2Popup()
    {
        uiPopupSummon2.OpenWithValues(0);
    }

    public void ShowSummonLevelInfoPopup()
    {
        uiPopupSummonLevelInfo.Open();
    }

    public void ShowRankingPopup()
    {
        InitForRanking();
    }

    public void ShowArenaPopup()
    {
        InitForArena();
    }

    public void ShowEquipmentPopup()
    {
        uiViewOutgameMain.SetHeartAmountInfoActive(true);
        uiPopupEquipment.Open();
    }

    public void ShowUpgradePopup()
    {
        uiPopupUpgrade.OpenWithValues(0);
    }

    public void ShowPetPopup()
    {
        uiViewOutgameMain.SetHeartAmountInfoActive(true);
        uiPopupPet.Open();
    }

    public void ShowAchievementPopup()
    {
        uiPopupAchievement.Open();
    }

    public void ShowWorldInfoPopup()
    {
        uiPopupWorldInfo.Open();
    }

    public void ShowGachaInfoPopup(SummonType summonType, int summonLevel)
    {
        uiPopupGachaInfo.OpenWithValues(summonType, summonLevel);
    }

    public void ShowShopPopup(int tabId, bool showLastItems = false)
    {
        uiPopupShop.OpenWithValues(tabId, showLastItems);
    }

    public void ShowGrowthSupportPopup()
    {
        var timeLeft = Mathf.Max(2f + OutgameController.Instance.TimeSyncPlayerData - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
        else
        {
            SyncPlayerData(true, "ShowGrowthSupportPopup", () => {
                uiPopupGrowthSupport2.Open();
            });
        }
    }

    public void ShowGrowthSupportPurchasePopup(string packageId)
    {
        #if UNITY_EDITOR
            //Nothing to do
        #else
            if(!PurchaseManager.Instance.IsInited)
                return;
        #endif

        var packageInfo = ServerNetworkManager.Instance.Catalog.GrowthPackageList.FirstOrDefault(n => n.Id == packageId);
        var gemRewardInfo = packageInfo.GrowthRewardList.FirstOrDefault(n => n.ItemId == "gem");

        if(packageInfo != null && gemRewardInfo != null)
            uiPopupGrowthSupportPurchase.OpenWithValues(packageId, packageInfo.VIPReward, gemRewardInfo.Amount, packageInfo.RealMoneyPrice);
    }

    public void ShowNoticePopup()
    {
        ShowLoading();

        ServerNetworkManager.Instance.GetTitleNews(
            () => {
                HideLoading();
                uiPopupNotice.Open();                
            },
            () => {
                HideLoading();
                UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
            }
        );
    }

    public void ShowAttendancePopup()
    {
        ShowLoading();

        ServerNetworkManager.Instance.RefreshAttendanceEventInfo(
            () => {
                HideLoading();
                uiPopupAttendanceEvent.OpenWithValues(
                    () => {
                        uiViewOutgameMain.RefreshMenuGroup();
                    }
                );
            },
            (error) => {
                HideLoading();

                uiViewOutgameMain.RefreshMenuGroup();

                if(error == "event_expired")
                    UIManager.Instance.ShowAlertLocalized("message_event_expired", null, null);
                else
                    UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
            }
        );
    }

    public void ShowTowerPopup()
    {
        ShowLoading();
        
        ServerNetworkManager.Instance.RefreshTower(
            (meetMinimumVersion) => {    
                ServerNetworkManager.Instance.RefreshTowerFloors(
                    () => {
                        HideLoading();

                        uiViewOutgameMain.SetFeatherAmountInfoActive(true);

                        uiPopupTower.Open();

                        // Also need to refresh floors
                        uiPopupTower.RefreshFloors();
                    },
                    () => {
                        HideLoading();
                        UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
                    }
                );

                if(!meetMinimumVersion)
                {
                    // The user needs to update the application!
                    UIManager.Instance.ShowAlert(
                        LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_below_min_version"),
                        () => {
                            PageOpenController.Instance.OpenMarketPage();
                            Application.Quit();
                        },
                        null,
                        true
                    );
                }
            },
            () => {
                HideLoading();
                UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
            }
        );
    }

    public void ShowTowerPlayerPopup(int floor)
    {
        if(floor == 1)
            return;
        
        if(ServerNetworkManager.Instance.TowerFloorPlayers[floor - 1].CurrentPlayers.Count() == 0)
            return;

        uiPopupTowerPlayer.OpenWithValues(floor);
    }

    public void ShowWingPopup()
    {
        SyncPlayerData(true, "ShowWingPopup", () => {

            uiViewOutgameMain.SetFeatherAmountInfoActive(true);
            var selectedWingModel = ServerNetworkManager.Instance.PurchasedWingList.FirstOrDefault(n => n.IsEquipment == true);

            if(selectedWingModel != null)
                uiPopupWing.OpenWithValues(selectedWingModel.WingId, false);
            else
            {
                if(ServerNetworkManager.Instance.PurchasedWingList.Count > 0)
                    uiPopupWing.OpenWithValues(ServerNetworkManager.Instance.PurchasedWingList[0].WingId, false);
                else
                    uiPopupWing.OpenWithValues("wing_01", true);
            }
            
        });
    }

    public void ShowGuildPopup()
    {
        GetGuildRankingList(
            ()=>{
                uiViewOutgameMain.SetGuildTokenAmountInfoActive(true);
                uiViewOutgameMain.HideRightTopContent();
                uiPopupGuild.OpenWithValues(0);
            }
        );
    }

    public void ShowGuildCreatePopup()
    {
        SyncPlayerData(true, "ShowGuildCreatePopup", () => {  
            uiPopupGuildCreate.Open();
        });
    }

    public void ShowGuildSettingPopup()
    {
        if(ServerNetworkManager.Instance.GuildData != null && ServerNetworkManager.Instance.GuildData.GuildManagementData != null)
            uiPopupGuildSetting.Open();
        else
            Debug.Log("Can't load guild management data !");
    }

    public void ShowGuildDonatePopup()
    {
       uiPopupGuildDonate.Open();
    }

    public void ShowReportPopup()
    {
        uiPopupReport.OpenWithValues(0);
    }

    public void ShowChatBlockPopup()
    {
        uiPopupChatBlock.OpenWithValues(0);
    }

    public void ShowRaidTicketPurchasePopup()
    {
        uiPopupRaidTicketPurchase.Open();
    }

    public void ShowMoreGame()
    {
        uiPopupMoreGame.Open();
    }

    public void ShowGenericInfo(string text)
    {
        uiPopupGenericInfo.OpenWithValues(text);
    }

    public void ShowToast(string text, bool isImportant = true)
    {
        uiPopupToast.ShowToast(text, isImportant);
    }

    public void ShowToastInOverFade(string text, bool isImportant = true)
    {
        uiPopupToastInOverFade.ShowToast(text, isImportant);
    }

    public void ShowPowerSavingCanvas()
    {
        uiViewPowerSaving.ShowPowerSavingMode();
    }

    // Control for popup closing with back button
    public void PushPopupInStack(UIPopup popup)
    {
        if(!openedPopupStack.Contains(popup))
            openedPopupStack.AddLast(popup);
    }

    public void PopPopupInStack(UIPopup popup)
    {
        if(openedPopupStack.Contains(popup))
            openedPopupStack.Remove(popup);
    }

    private void HideLoading()
    {
        UIManager.Instance.HideCanvas("Loading Canvas");
    }

    private void ShowLoading()
    {
        UIManager.Instance.ShowCanvas("Loading Canvas");
    }

#endregion

#region Logic For Auth Key

    private void RetrieveAuthKey()
    {   
        if(OptionManager.Instance.LoginMode == OptionManager.LoginModeType.PLATFORM_DEFAULT)
        {
            ShowLoading();

            SocialPlatformManager.Instance.LoginSocialService(OnRetrieveAuthkey, OnFailedRetrieveAuthkey);
        }
        else if(OptionManager.Instance.LoginMode == OptionManager.LoginModeType.GUEST)
        {
            OnRetrieveAuthkey();
        }
        else
        {
            HideLoading();
            uiAuthMethodSelect.OpenWithValues(true);
        }
    }

    private void OnRetrieveAuthkey()
    {
        HideLoading();
        
        if(OptionManager.Instance.LoginMode == OptionManager.LoginModeType.PLATFORM_DEFAULT && SocialPlatformManager.Instance.IsSocialAuthenticatedProperly)
        {
            ServerNetworkManager.Instance.AuthKey = Social.localUser.id;
        }
        else
        {
            ServerNetworkManager.Instance.AuthKey = SystemInfo.deviceUniqueIdentifier;
        }

        Debug.Log("OutgameController.OnRetrieveAuthkey: " + ServerNetworkManager.Instance.AuthKey);
        
        ShowOpening();
    }

    private void OnFailedRetrieveAuthkey()
    {
        Debug.Log("OutgameController.OnFailedRetrieveAuthkey");

        StartCoroutine(RetryRetriveAuthKey());
    }

    private IEnumerator RetryRetriveAuthKey()
    {
        bool shouldReset = true;
        float passed = 0f;
        while(passed < 5f)
        {
            yield return -1f;
            passed += Time.deltaTime;
            if(SocialPlatformManager.Instance.IsSocialAuthenticatedProperly)
            {
                OnRetrieveAuthkey();
                shouldReset = false;
                break;
            }
        }
        
        if(shouldReset)
        {
            HideLoading();

            if(Application.platform == RuntimePlatform.IPhonePlayer)
            {
                UIManager.Instance.ShowAlertLocalized(
                    "message_game_center_login", 
                    () => {
                        ShowLoading();
                        OnFailedRetrieveAuthkey();
                    }, 
                    null,
                    true
                );
            }
            else
            {
                OptionManager.Instance.LoginMode = OptionManager.LoginModeType.UNSET;
                SceneChangeManager.Instance.LoadMainGame(true);
            }
        }
    }

    public void BindAuthToExistingGuestAccount()
    {
        ShowLoading();
        SocialPlatformManager.Instance.LoginSocialService(OnRetrieveAuthkeyForBinding, OnFailedRetrieveAuthkeyForBinding);
    }

    private void OnRetrieveAuthkeyForBinding()
    {
        if(SocialPlatformManager.Instance.IsSocialAuthenticatedProperly)
        {
            this.LinkAccountWithCurrentAuthKey();
        }
    }
    
    private void OnFailedRetrieveAuthkeyForBinding()
    {
        Debug.Log("OutgameController.OnFailedRetrieveAuthkeyForBinding");
        
        HideLoading();
        UIManager.Instance.ShowAlertLocalized(
            "message_social_login_fail", 
            null, 
            null
        );
    }

    public void ExplicitSignOutSocial()
    {
        SyncPlayerData(true, "ExplicitSignOutSocial", () => {
            SocialPlatformManager.Instance.LogoutSocialService();

            PlayerPrefs.SetInt("push_registered", 0);
            PlayerPrefs.SetInt("explicit_sign_out", 1);
            
            explicitSocialSignout = true;
            if(OptionManager.Instance.LoginMode == OptionManager.LoginModeType.PLATFORM_DEFAULT)
            {
                OptionManager.Instance.LoginMode = OptionManager.LoginModeType.UNSET;
            }
            ServerNetworkManager.Instance.Logout();
            ChatManager.Instance.Disconnect();
            ChatManager.Instance.RecentPlayers.Clear();
            SceneChangeManager.Instance.LoadMainGame(true, true);
        });
    }

#endregion

#region Logic For GDPR

    public void AskPolicyAgree()
    {
        bool isEUCountry = false;
        string deviceCountry = SB.SupportTools.getNationalCode();
        string[] euMemberCountries = { "AT", "BE", "BG", "CY", "CZ", "DE", "DK", "EE", "ES", "FI", "FR", "GB", "GR", "HR", "HU", "IE", "IT", "LT", "LU", "LV", "MT", "NL", "PL", "PT", "RO", "SE", "SI", "SK", "IS", "LI", "NO" };

        if(euMemberCountries.Contains(deviceCountry))
            isEUCountry = true;

        // Check policy agreement
        if(OptionManager.Instance.Language == OptionManager.LanguageModeType.ko && !PlayerPrefs.HasKey("is_agree_terms_and_privacy"))
            ShowTermsAndPrivacyPopup();
        else if(isEUCountry && !PlayerPrefs.HasKey("is_agree_gdpr"))
            ShowGDPRPopup();
        else
            RetrieveAuthKey();
    }

    public void SetGDPRAgree()
    {
        PlayerPrefs.SetInt("is_agree_gdpr", 1);
        RetrieveAuthKey();
    }

    public void SetTermsAndPrivacyAgree()
    {
        PlayerPrefs.SetInt("is_agree_terms_and_privacy", 1);
        RetrieveAuthKey();
    }

#endregion

#region Logic For Opening

    public void ShowOpening()
    {
        // Check opening
        if(!PlayerPrefs.HasKey("is_opening_seen"))
            ShowOpeningPopup();
        else
            LogIn();
    }

    public void SetOpeningSeen()
    {
        PlayerPrefs.SetInt("is_opening_seen", 1);
        LogIn();
    }

#endregion

#region Logic For More Game
    public void OpenMoreGame()
    {
        bool showMoreGame = false;
        if(PlayerPrefs.HasKey("app_open_count") && PlayerPrefs.GetInt("app_open_count") >= 20)
            showMoreGame = true;
		
        if(showMoreGame)
        {
            bool isMoreViewOK = SB.MoreManager.Instance.isMoreViewOk();

            if (isMoreViewOK)
                OutgameController.Instance.ShowMoreGame();
            else
                SB.MoreManager.openMoregameDeveloperPage();
        }

        if(PlayerPrefs.HasKey("app_open_count"))
            PlayerPrefs.SetInt("app_open_count", PlayerPrefs.GetInt("app_open_count") + 1);
        else
            PlayerPrefs.SetInt("app_open_count", 0);
    }
#endregion

#region Logic No Server Buff Ads

    public void ShowBuffAds(BuffType buffType)
    {
#if UNITY_ANDROID
        refocusLimit = -1d;
#endif
        ShowLoading();

        InterstitialRewardedManager.Instance.Init();
        InterstitialRewardedManager.Instance.ShowAds(
            "ads_buff",
            (showResult) => {
                if(showResult == ShowResult.Finished)
                {
                    HideLoading();
                    
                    BuffManager.Instance.StartTemporrayBuff(buffType);
                    NumberMainController.Instance.Refresh();

                    // For log event
                    ServerNetworkManager.Instance.User.BuffAndRestRewardCount = ServerNetworkManager.Instance.User.BuffAndRestRewardCount + 1;
                    
                    var tmpAchievementModel = ServerNetworkManager.Instance.User.AchievementInfo.FirstOrDefault(n => n.Id == "achievement_12");
                    if(tmpAchievementModel != null)
                        AnalyticsController.Instance.AdRewardEvent(tmpAchievementModel.Count + ServerNetworkManager.Instance.User.BuffAndRestRewardCount);
                    else
                        AnalyticsController.Instance.AdRewardEvent(ServerNetworkManager.Instance.User.BuffAndRestRewardCount);

                    uiViewOutgameMain.Refresh();
                }
                else if(showResult == ShowResult.Skipped)
                {
                    HideLoading();
                    ShowToast(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_no_ad_cancel"));
                }
                else
                {
                    HideLoading();
                    ShowToast(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_no_ad_avail"));
                }
            }
        );
    }

#endregion

#region Logic No Server Summon

    public (bool isEnoughGem, List<string> summonItemIdList, List<bool> newInfoList, List<string> goodSummonIdList) Summon(SummonType summonType, int count)
    {
        if(summonType == SummonType.LEGEMDARY_RELIC)
        {
            bool isAllMax = true;

            foreach (var tmp in BalanceInfoManager.Instance.LegendaryRelicInfoList)
            {
                var targetLegendaryRelic = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.Id == tmp.Id);
                if(targetLegendaryRelic != null)
                {
                    // Check level + count vs target legnedary relic max level
                    if((targetLegendaryRelic.Level + targetLegendaryRelic.Count - 1) < int.Parse(tmp.MaxLevel))
                        isAllMax = false;
                }
                else
                    isAllMax = false;
            }

            if(isAllMax)
            {
                UIManager.Instance.ShowAlertLocalized("message_max_reached", null, null);
                return (false, null, null, null);
            }
        }

        (bool isEnoughGem, List<string> summonItemIdList, List<bool> newInfoList, List<string> goodSummonIdList) summonResult = (false, null, null, null);
        if(summonType == SummonType.WEAPON)
            summonResult = SummonController.Instance.SummonWeapons(count);
        else if(summonType == SummonType.CLASS)
            summonResult = SummonController.Instance.SummonClass(count);
        else if(summonType == SummonType.RELIC)
            summonResult = SummonController.Instance.SummonRelic(count);
        else if(summonType == SummonType.LEGEMDARY_RELIC)
            summonResult = SummonController.Instance.SummonLegendaryRelic(count);
        else if(summonType == SummonType.PET)
            summonResult = SummonController.Instance.SummonPet(count);
        else if(summonType == SummonType.MERCENARY)
            summonResult = SummonController.Instance.SummonMercenary(count);

        return (summonResult.isEnoughGem, summonResult.summonItemIdList, summonResult.newInfoList, summonResult.goodSummonIdList);
    }

    public void OnSummonAndSync(SummonType summonType, int count, List<string> summonItemIdList, List<bool> newInfoList, List<string> goodSummonIdList)
    {
        uiViewOutgameMain.Refresh();
        uiPopupSummon2.Refresh();
        
        var whiteItemList = new List<bool>();
        foreach(var tmp in summonItemIdList)
        {
            int convertedIndex = int.Parse(tmp.Substring(tmp.Length - 2));
            switch(summonType)
            {
                case SummonType.WEAPON:
                    if(convertedIndex > 16)
                        whiteItemList.Add(true);
                    else
                        whiteItemList.Add(false);
                    break;
                case SummonType.CLASS:
                    if(convertedIndex > 16)
                        whiteItemList.Add(true);
                    else
                        whiteItemList.Add(false);
                    break;
                case SummonType.MERCENARY:
                    if(convertedIndex > 11)
                        whiteItemList.Add(true);
                    else
                        whiteItemList.Add(false);
                    break;
                case SummonType.PET:
                    if(convertedIndex > 8)
                        whiteItemList.Add(true);
                    else
                        whiteItemList.Add(false);
                    break;
                case SummonType.RELIC:
                    whiteItemList.Add(false);
                    break;
                case SummonType.LEGEMDARY_RELIC:
                    whiteItemList.Add(false);
                    break;
            }
        }

        uiPopupSummonResult.OpenWithValues(summonType, summonItemIdList, newInfoList, whiteItemList);

        // Update achievement related with summon
        UpdateAchievement("achievement_11", count);

        // Send chat message that newly added good items
        foreach (var tmp in goodSummonIdList)
        {
            OutgameController.Instance.SendSummonResultMessage(tmp);
        }
    }

#endregion

#region Logic No Server Achievement
    public void UpdateAchievement(string achievementId, int count)
    {
        var tmpAchievementModel = ServerNetworkManager.Instance.User.AchievementInfo.FirstOrDefault(n => n.Id == achievementId);
        var tmpAchievementInfoModel = BalanceInfoManager.Instance.AchievementInfoList.FirstOrDefault(n => n.Id == achievementId);

        if(tmpAchievementModel != null && tmpAchievementInfoModel != null)
        {
            if(!tmpAchievementModel.IsMaxLevel)
                tmpAchievementModel.Count = tmpAchievementModel.Count + count;

            // For analytics log
            if(tmpAchievementModel.Id == "achievement_11")
                AnalyticsController.Instance.SummonEvent(tmpAchievementModel.Count);
            else if(tmpAchievementModel.Id == "achievement_12")
                AnalyticsController.Instance.AdRewardEvent(tmpAchievementModel.Count + ServerNetworkManager.Instance.User.BuffAndRestRewardCount);
        }

        if(uiPopupAchievement != null && uiPopupAchievement.gameObject.activeSelf)
            uiPopupAchievement.Refresh();
    }

    public void RequestAchievementReward(string achievementId, Action<int> onSuccess)
    {
        bool isError = false;
        int totalGemReward = 0;
        var tmpAchievementModel = ServerNetworkManager.Instance.User.AchievementInfo.FirstOrDefault(n => n.Id == achievementId);
        var tmpAchievementInfoModel = BalanceInfoManager.Instance.AchievementInfoList.FirstOrDefault(n => n.Id == achievementId);

        if(tmpAchievementModel != null && tmpAchievementInfoModel != null)
        {
            if(tmpAchievementModel.IsMaxLevel)
                isError = true;
            else
            {
                while(tmpAchievementModel.Count >= tmpAchievementModel.Goal)
                {
                    totalGemReward = totalGemReward + tmpAchievementInfoModel.GemReward;
                    tmpAchievementModel.Level = tmpAchievementModel.Level + 1;
                    
                    // Update next goal
                    if(tmpAchievementInfoModel.IsIncreaseGoal)
                        tmpAchievementModel.Goal = tmpAchievementModel.Goal + Mathf.Min(tmpAchievementInfoModel.BasicGoal * tmpAchievementModel.Level , tmpAchievementInfoModel.MaxIncreaseGoal);
                    else
                        tmpAchievementModel.Goal = tmpAchievementModel.Goal + tmpAchievementInfoModel.BasicGoal;

                    // Check target achievement' level reached max level
                    if(tmpAchievementInfoModel.MaxLevel != -1)
                    {
                        if(tmpAchievementModel.Level >= tmpAchievementInfoModel.MaxLevel)
                        {
                            tmpAchievementModel.IsMaxLevel = true;
                            break;
                        }
                    }
                }

                // Give Reward Processes
                ServerNetworkManager.Instance.UnsyncedGem += totalGemReward;
                ServerNetworkManager.Instance.User.UnsyncedGemChangeCount ++;
            }
        }
        else
            isError = true;

        if(isError)
            Debug.Log("OutgameController.RequesAchievementReward: Error");
        else
        {     
            uiViewOutgameMain.Refresh();
            onSuccess?.Invoke(totalGemReward);
        }
    }
#endregion

#region Logic No Server Related With Equipment
    public void EquipWeapon(string weaponId, Action onSuccess)
    {
        // Check user have this weapon
        var tmpWeaponModel = ServerNetworkManager.Instance.Inventory.WeaponList.FirstOrDefault(n => n.Id == weaponId);

        if(tmpWeaponModel != null && tmpWeaponModel.Count > 0)
        {
            ServerNetworkManager.Instance.Inventory.SelectedWeapon = tmpWeaponModel;

            // Need to recalculate numbers
            NumberMainController.Instance.Refresh();

            // Refresh hero visual
            IngameMainController.Instance.RefreshHeroVisual();

            onSuccess?.Invoke();
        }
    }

    public void MergeWeapon(string weaponId, Action<bool> onSuccess)
    {
        bool isError = false;

        var tmpWeaponModel = ServerNetworkManager.Instance.Inventory.WeaponList.FirstOrDefault(n => n.Id == weaponId);
        int nextTierWeaponCount = 0;
        int convertedWeaponIndex = 0;

        // Check user have this weapon or not
        if(tmpWeaponModel != null)
        {
            convertedWeaponIndex = int.Parse(tmpWeaponModel.Id.Substring(tmpWeaponModel.Id.Length - 2));

            // Check this weapon is last rank or not
            if(convertedWeaponIndex >= ServerNetworkManager.Instance.Setting.TotalWeaponCount)
                isError = true;
            else
            {
                // Check user equip this item
                if(ServerNetworkManager.Instance.Inventory.SelectedWeapon.Id == weaponId)
                {
                    // Check item count
                    if(tmpWeaponModel.Count > 5)
                    {
                        var currentCount = tmpWeaponModel.Count - 1;
                        tmpWeaponModel.Count = currentCount % 5 + 1;
                        nextTierWeaponCount = currentCount / 5;
                    }
                    else
                        isError = true;
                }
                else
                {
                    // Check item count
                    if(tmpWeaponModel.Count >= 5)
                    {
                        var currentCount = tmpWeaponModel.Count;
                        tmpWeaponModel.Count = currentCount % 5;
                        nextTierWeaponCount = currentCount / 5;
                    }
                    else
                        isError = true;
                }
            }
        }
        else
            isError = true;

        if(isError)
            Debug.Log("OutgameController.MergeWeapon: Error");
        else
        {
            // Add next rank weapon count
            string nextWeaponId;
            int nextRankWeaponIndex = convertedWeaponIndex + 1;

            if(nextRankWeaponIndex < 10)
                nextWeaponId = string.Format("weapon_0{0}", nextRankWeaponIndex);
            else
                nextWeaponId = string.Format("weapon_{0}", nextRankWeaponIndex);

            var tmpNextRankWeaponModel = ServerNetworkManager.Instance.Inventory.WeaponList.FirstOrDefault(n => n.Id == nextWeaponId);
            
            // Check user have next rank item or not
            if(tmpNextRankWeaponModel != null)
                tmpNextRankWeaponModel.Count = tmpNextRankWeaponModel.Count + nextTierWeaponCount;
            else
            {
                tmpNextRankWeaponModel = new WeaponModel();
                tmpNextRankWeaponModel.Id = nextWeaponId;
                tmpNextRankWeaponModel.Level = 1;
                tmpNextRankWeaponModel.Count = nextTierWeaponCount;

                ServerNetworkManager.Instance.Inventory.WeaponList.Add(tmpNextRankWeaponModel);
            }

            uiViewOutgameMain.RefreshTopbar();
            uiViewOutgameMain.RefreshRightHorizontalbar();

            onSuccess?.Invoke(true);
        }
    }

    public void UpgradeWeapon(string weaponId, Action<bool> onSuccess)
    {
        bool isError = false;
        bool isUpgradeSuccess = false;

        var tmpWeaponModel = ServerNetworkManager.Instance.Inventory.WeaponList.FirstOrDefault(n => n.Id == weaponId);

        // Check user have this weapon or not
        if(tmpWeaponModel != null && tmpWeaponModel.Count > 0)
        {
            // Check upgrade max level
            if(tmpWeaponModel.Level >= ServerNetworkManager.Instance.Setting.MaxItemLevel)
                isError = true;
            else
            {
                // Check upgrade method
                if(tmpWeaponModel.Level > 100)
                {
                    var requiredGem = tmpWeaponModel.LevelupCost;

                    // Check user current gems
                    if(ServerNetworkManager.Instance.TotalGem >= requiredGem)
                    {
                        tmpWeaponModel.Level = tmpWeaponModel.Level + 1;
                        ServerNetworkManager.Instance.UnsyncedGem -= (int)(requiredGem);
                        ServerNetworkManager.Instance.User.UnsyncedGemChangeCount ++;
                        isUpgradeSuccess = true;
                    }
                    else
                        isError = true;
                }
                else
                {
                    var requiredGold = tmpWeaponModel.LevelupCost;

                    // Check user current gold
                    if(ServerNetworkManager.Instance.Inventory.Gold >= requiredGold)
                    {
                        // Upgrade process
                        var upgradeResult = UnityEngine.Random.Range(0f, 100f);
                        
                        if(upgradeResult <= tmpWeaponModel.LevelUpChancePercent)
                        {
                            tmpWeaponModel.Level = tmpWeaponModel.Level + 1;
                            ServerNetworkManager.Instance.Inventory.Gold -= requiredGold;
                            isUpgradeSuccess = true;
                        }
                        else
                        {
                            ServerNetworkManager.Instance.Inventory.Gold -= requiredGold;
                            isUpgradeSuccess = false;
                        }
                    }
                    else
                        isError = true;
                }
            }
        }
        else
            isError = true;

        if(isError)
            Debug.Log("OutgameController.UpgradeWeapon: Error");
        else
        {     
            if(isUpgradeSuccess)
                NumberMainController.Instance.Refresh();

            // Refresh the view
            uiViewOutgameMain.RefreshTopbar();
            uiViewOutgameMain.RefreshRightHorizontalbar();

            onSuccess?.Invoke(isUpgradeSuccess);
        }
    }

    public void EquipClass(string classId, Action onSuccess)
    {
        //Check user have this class
        var tmpClassModel = ServerNetworkManager.Instance.Inventory.ClassList.FirstOrDefault(n => n.Id == classId);

        if(tmpClassModel != null && tmpClassModel.Count > 0)
        {
            ServerNetworkManager.Instance.Inventory.SelectedClass = tmpClassModel;
            
            // Need to recalculate numbers
            NumberMainController.Instance.Refresh();

            // Refresh hero visual
            IngameMainController.Instance.RefreshHeroVisual();

            onSuccess?.Invoke();
        }
    }

    public void MergeClass(string classId, Action<bool> onSuccess)
    {
        bool isFalse = false;

        var tmpClassModel = ServerNetworkManager.Instance.Inventory.ClassList.FirstOrDefault(n => n.Id == classId);
        int nextTierClassCount = 0;
        int convertedClassIndex = 0;

        // Check user have this weapon or not
        if(tmpClassModel != null)
        {
            convertedClassIndex = int.Parse(tmpClassModel.Id.Substring(tmpClassModel.Id.Length - 2));

            // Check this weapon is last rank or not
            if(convertedClassIndex >= ServerNetworkManager.Instance.Setting.TotalClassCount)
                isFalse = true;
            else
            {
                // Check user equip this item
                if(ServerNetworkManager.Instance.Inventory.SelectedClass.Id == classId)
                {
                    // Check item count
                    if(tmpClassModel.Count > 5)
                    {
                        var currentCount = tmpClassModel.Count - 1;
                        tmpClassModel.Count = currentCount % 5 + 1;
                        nextTierClassCount = currentCount / 5;
                    }
                    else
                        isFalse = true;
                }
                else
                {
                    // Check item count
                    if(tmpClassModel.Count >= 5)
                    {
                        var currentCount = tmpClassModel.Count;
                        tmpClassModel.Count = currentCount % 5;
                        nextTierClassCount = currentCount / 5;
                    }
                    else
                        isFalse = true;
                }
            }
        }
        else
            isFalse = true;

        if(isFalse)
            Debug.Log("OutgameController.MergeClass: Error");
        else
        {
            // Add next rank class count
            string nextClassId;
            int nextRankClassIndex = convertedClassIndex + 1;

            if(nextRankClassIndex < 10)
                nextClassId = string.Format("class_0{0}", nextRankClassIndex);
            else
                nextClassId = string.Format("class_{0}", nextRankClassIndex);

            var tmpNextRankClassModel = ServerNetworkManager.Instance.Inventory.ClassList.FirstOrDefault(n => n.Id == nextClassId);
            
            // Check user have next rank item or not
            if(tmpNextRankClassModel != null)
                tmpNextRankClassModel.Count = tmpNextRankClassModel.Count + nextTierClassCount;
            else
            {
                tmpNextRankClassModel = new ClassModel();
                tmpNextRankClassModel.Id = nextClassId;
                tmpNextRankClassModel.Level = 1;
                tmpNextRankClassModel.Count = nextTierClassCount;

                ServerNetworkManager.Instance.Inventory.ClassList.Add(tmpNextRankClassModel);
            }

            uiViewOutgameMain.RefreshTopbar();
            uiViewOutgameMain.RefreshRightHorizontalbar();

            onSuccess?.Invoke(true);
        }
    }

    public void UpgradeClass(string classId, Action<bool> onSuccess)
    {
        bool isError = false;
        bool isUpgradeSuccess = false;

        var tmpClassModel = ServerNetworkManager.Instance.Inventory.ClassList.FirstOrDefault(n => n.Id == classId);

        // Check user have this class or not
        if(tmpClassModel != null && tmpClassModel.Count > 0)
        {
            // Check upgrade max level
            if(tmpClassModel.Level >= ServerNetworkManager.Instance.Setting.MaxItemLevel)
                isError = true;
            else
            {
                // Check upgrade method
                if(tmpClassModel.Level > 100)
                {
                    var requiredGem = tmpClassModel.LevelupCost;

                    // Check user current gems
                    if(ServerNetworkManager.Instance.TotalGem >= requiredGem)
                    {
                        tmpClassModel.Level = tmpClassModel.Level + 1;
                        ServerNetworkManager.Instance.UnsyncedGem -= (int)(requiredGem);
                        ServerNetworkManager.Instance.User.UnsyncedGemChangeCount ++;
                        isUpgradeSuccess = true;
                    }
                    else
                        isError = true;
                }
                else
                {
                    var requiredGold = tmpClassModel.LevelupCost;

                    // Check user current gold
                    if(ServerNetworkManager.Instance.Inventory.Gold >= requiredGold)
                    {
                        // Upgrade process
                        var upgradeResult = UnityEngine.Random.Range(0f, 100f);
                        
                        if(upgradeResult <= tmpClassModel.LevelUpChancePercent)
                        {
                            tmpClassModel.Level = tmpClassModel.Level + 1;
                            ServerNetworkManager.Instance.Inventory.Gold -= requiredGold;
                            isUpgradeSuccess = true;
                        }
                        else
                        {
                            ServerNetworkManager.Instance.Inventory.Gold -= requiredGold;
                            isUpgradeSuccess = false;
                        }
                    }
                    else
                        isError = true;
                }
            }
        }
        else
            isError = true;

        if(isError)
            Debug.Log("OutgameController.UpgradeClass: Error");
        else
        {
            if(isUpgradeSuccess)
                NumberMainController.Instance.Refresh();

            // Refresh the view
            uiViewOutgameMain.RefreshTopbar();
            uiViewOutgameMain.RefreshRightHorizontalbar();

            onSuccess?.Invoke(isUpgradeSuccess);
        }
    }

    public void EquipMercenary(string mercenaryId, Action onSuccess)
    {
        //Check user have this mercenary
        var tmpMercenaryModel = ServerNetworkManager.Instance.Inventory.MercenaryList.FirstOrDefault(n => n.Id == mercenaryId);

        if(tmpMercenaryModel != null && tmpMercenaryModel.Count > 0)
        {
            ServerNetworkManager.Instance.Inventory.SelectedMercenary = tmpMercenaryModel;
            
            // Need to recalculate numbers
            NumberMainController.Instance.Refresh();

            // Refresh mercenary visual
            IngameMainController.Instance.ResetMercenary(false);

            onSuccess?.Invoke();
        }
    }
    
    public void MergeMercenary(string mercenaryId, Action<bool> onSuccess)
    {
        bool isFalse = false;

        var tmpMercenaryModel = ServerNetworkManager.Instance.Inventory.MercenaryList.FirstOrDefault(n => n.Id == mercenaryId);
        int nextTierMercenaryCount = 0;
        int convertedMercenaryIndex = 0;

        // Check user have this mercenary or not
        if(tmpMercenaryModel != null)
        {
            convertedMercenaryIndex = int.Parse(tmpMercenaryModel.Id.Substring(tmpMercenaryModel.Id.Length - 2));

            // Check this mercenary is last rank or not
            if(convertedMercenaryIndex >= ServerNetworkManager.Instance.Setting.TotalMercenaryCount)
                isFalse = true;
            else
            {
                // Check user equip this item
                if(ServerNetworkManager.Instance.Inventory.SelectedMercenary != null && ServerNetworkManager.Instance.Inventory.SelectedMercenary.Id == mercenaryId)
                {
                    // Check item count
                    if(tmpMercenaryModel.Count > 5)
                    {
                        var currentCount = tmpMercenaryModel.Count - 1;
                        tmpMercenaryModel.Count = currentCount % 5 + 1;
                        nextTierMercenaryCount = currentCount / 5;
                    }
                    else
                        isFalse = true;
                }
                else
                {
                    // Check item count
                    if(tmpMercenaryModel.Count >= 5)
                    {
                        var currentCount = tmpMercenaryModel.Count;
                        tmpMercenaryModel.Count = currentCount % 5;
                        nextTierMercenaryCount = currentCount / 5;
                    }
                    else
                        isFalse = true;
                }
            }
        }
        else
            isFalse = true;

        if(isFalse)
            Debug.Log("OutgameController.MergeMercenary: Error");
        else
        {
            // Add next rank mercenary count
            string nextMercenaryId;
            int nextRankMercenaryIndex = convertedMercenaryIndex + 1;

            if(nextRankMercenaryIndex < 10)
                nextMercenaryId = string.Format("mercenary_0{0}", nextRankMercenaryIndex);
            else
                nextMercenaryId = string.Format("mercenary_{0}", nextRankMercenaryIndex);

            var tmpNextRankMercenaryModel = ServerNetworkManager.Instance.Inventory.MercenaryList.FirstOrDefault(n => n.Id == nextMercenaryId);
            
            // Check user have next rank item or not
            if(tmpNextRankMercenaryModel != null)
                tmpNextRankMercenaryModel.Count = tmpNextRankMercenaryModel.Count + nextTierMercenaryCount;
            else
            {
                tmpNextRankMercenaryModel = new MercenaryModel();
                tmpNextRankMercenaryModel.Id = nextMercenaryId;
                tmpNextRankMercenaryModel.Level = 1;
                tmpNextRankMercenaryModel.Count = nextTierMercenaryCount;

                ServerNetworkManager.Instance.Inventory.MercenaryList.Add(tmpNextRankMercenaryModel);
            }

            uiViewOutgameMain.RefreshTopbar();
            uiViewOutgameMain.RefreshRightHorizontalbar();

            onSuccess?.Invoke(true);
        }
    }

    public void UpgradeMercenary(string mercenaryId, Action<bool> onSuccess)
    {
        bool isError = false;
        bool isUpgradeSuccess = false;

        var tmpMercenaryModel = ServerNetworkManager.Instance.Inventory.MercenaryList.FirstOrDefault(n => n.Id == mercenaryId);

        // Check user have this mercenary or not
        if(tmpMercenaryModel != null && tmpMercenaryModel.Count > 0)
        {
            // Check upgrade max level
            if(tmpMercenaryModel.Level >= ServerNetworkManager.Instance.Setting.MaxMercenaryLevel)
                isError = true;
            else
            {
                var requiredHeart = tmpMercenaryModel.LevelupCost;

                // Check user current heart
                if(ServerNetworkManager.Instance.Inventory.Heart >= requiredHeart)
                {
                    // Upgrade process
                    var upgradeResult = UnityEngine.Random.Range(0f, 100f);
                    
                    if(upgradeResult <= tmpMercenaryModel.LevelUpChancePercent)
                    {
                        tmpMercenaryModel.Level = tmpMercenaryModel.Level + 1;
                        ServerNetworkManager.Instance.Inventory.Heart -= requiredHeart;
                        isUpgradeSuccess = true;
                    }
                    else
                    {
                        ServerNetworkManager.Instance.Inventory.Heart -= requiredHeart;
                        isUpgradeSuccess = false;
                    }
                }
                else
                    isError = true;
            }
        }
        else
            isError = true;

        if(isError)
            Debug.Log("OutgameController.UpgradeMercenary: Error");
        else
        {
            if(isUpgradeSuccess)
                NumberMainController.Instance.Refresh();

            // Refresh the view
            uiViewOutgameMain.RefreshTopbar();
            uiViewOutgameMain.RefreshRightHorizontalbar();

            onSuccess?.Invoke(isUpgradeSuccess);
        }
    }

    public void UpgradeRelic(string relicId, Action<bool> onSuccess)
    {
        bool isError = false;
        bool isUpgradeSuccess = false;

        var tmpRelicModel = ServerNetworkManager.Instance.Inventory.RelicList.FirstOrDefault(n => n.Id == relicId);
        var tmpRelicBalanceInfo = BalanceInfoManager.Instance.RelicInfoList.FirstOrDefault(n => n.Id == relicId);

        // Check user have this relic or not
        if(tmpRelicModel != null && (tmpRelicModel.Count - 1)> 0)
        {
            // Check relic balance info is null or not
            if(tmpRelicBalanceInfo != null)
            {
                // Check upgrade max level
                if(int.Parse(tmpRelicBalanceInfo.MaxLevel) != - 1 && tmpRelicModel.Level >= int.Parse(tmpRelicBalanceInfo.MaxLevel))
                    isError = true;
                else
                {
                    // Check user current relic count
                    if((tmpRelicModel.Count - 1) > 0)
                    {
                        // Upgrade process
                        var upgradeResult = UnityEngine.Random.Range(0f, 100f);
                        
                        if(upgradeResult <= tmpRelicModel.LevelUpChancePercent)
                        {
                            tmpRelicModel.Level = tmpRelicModel.Level + 1;
                            tmpRelicModel.Count = tmpRelicModel.Count - 1;
                            isUpgradeSuccess = true;
                        }
                        else
                        {
                            tmpRelicModel.Count = tmpRelicModel.Count - 1;
                            isUpgradeSuccess = false;
                        }
                    }
                    else
                        isError = true;
                }
            }
            else
                isError = true;
        }
        else
            isError = true;

        if(isError)
            Debug.Log("OutgameController.UpgradeRelic: Error");
        else
        {
            if(isUpgradeSuccess)
                NumberMainController.Instance.Refresh();            
            
            // Update achievement related with upgrade relic
            UpdateAchievement("achievement_10", 1);

            uiViewOutgameMain.RefreshTopbar();
            uiViewOutgameMain.RefreshRightHorizontalbar();
            
            onSuccess?.Invoke(isUpgradeSuccess);
        }
    }

    public void RefundRelic(string relicId, Action onSuccess)
    {
        bool isError = false;

        var tmpRelicModel = ServerNetworkManager.Instance.Inventory.RelicList.FirstOrDefault(n => n.Id == relicId);
        var tmpRelicBalanceInfo = BalanceInfoManager.Instance.RelicInfoList.FirstOrDefault(n => n.Id == relicId);
        int refundGem = 0;

        // Check user have this relic or not
        if(tmpRelicModel != null && (tmpRelicModel.Count - 1)> 0)
        {
            // Check relic balance info is null or not
            if(tmpRelicBalanceInfo != null)
            {
                // Check max level
                if(int.Parse(tmpRelicBalanceInfo.MaxLevel) != - 1 && tmpRelicModel.Level >= int.Parse(tmpRelicBalanceInfo.MaxLevel))
                {
                    int refundRelicCount = tmpRelicModel.Count - 1;
                    refundGem = refundRelicCount * ServerNetworkManager.Instance.Setting.RefundRelicPrice;

                    // RefundProcess
                    ServerNetworkManager.Instance.UnsyncedGem += refundGem;
                    ServerNetworkManager.Instance.User.UnsyncedGemChangeCount ++;
                    tmpRelicModel.Count = 1;
                }
                else
                    isError = true;
            }
            else
                isError = true;
        }
        else
            isError = true;

        if(isError)
            Debug.Log("OutgameController.RefundRelic: Error");
        else
        {    
            uiViewOutgameMain.RefreshTopbar();
            uiViewOutgameMain.RefreshRightHorizontalbar();
            
            UIManager.Instance.ShowAlert(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_thank_you_with_rewards"), refundGem), null, null);

            onSuccess?.Invoke();
        }
    }
    
    public void UpgradeLegendaryRelic(string legendaryRelicId, Action onSuccess)
    {
        bool isError = false;

        var tmpLegendaryRelicModel = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.Id == legendaryRelicId);
        var tmpLegendaryRelicBalanceInfo = BalanceInfoManager.Instance.LegendaryRelicInfoList.FirstOrDefault(n => n.Id == legendaryRelicId);

        // Check user have this legendary relic or not
        if(tmpLegendaryRelicModel != null && (tmpLegendaryRelicModel.Count - 1)> 0)
        {
            // Check legendary relic balance info is null or not
            if(tmpLegendaryRelicBalanceInfo != null)
            {
                // Check upgrade max level
                if(int.Parse(tmpLegendaryRelicBalanceInfo.MaxLevel) != - 1 && tmpLegendaryRelicModel.Level >= int.Parse(tmpLegendaryRelicBalanceInfo.MaxLevel))
                    isError = true;
                else
                {
                    // Check user current legendary relic count
                    if((tmpLegendaryRelicModel.Count - 1) > 0)
                    {
                        // Upgrade process
                        tmpLegendaryRelicModel.Level = tmpLegendaryRelicModel.Level + 1;
                        tmpLegendaryRelicModel.Count = tmpLegendaryRelicModel.Count - 1;

                        // Update achievement related with upgrade legendary relic
                        UpdateAchievement("achievement_10", 1);
                    }
                    else
                        isError = true;
                }
            }
            else
                isError = true;
        }
        else
            isError = true;

        if(isError)
            Debug.Log("OutgameController.UpgradeLegendaryRelic: Error");
        else
        {
            NumberMainController.Instance.Refresh();         

            uiViewOutgameMain.RefreshTopbar();
            uiViewOutgameMain.RefreshRightHorizontalbar();
            
            onSuccess?.Invoke();
        }
    }

    public void RefundLegendaryRelic(string legendaryRelicId, Action onSuccess)
    {
        bool isError = false;

        var tmpLegendaryRelicModel = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.Id == legendaryRelicId);
        var tmpLegendaryRelicBalanceInfo = BalanceInfoManager.Instance.LegendaryRelicInfoList.FirstOrDefault(n => n.Id == legendaryRelicId);
        int refundGem = 0;

        // Check user have this legendary relic or not
        if(tmpLegendaryRelicModel != null && (tmpLegendaryRelicModel.Count - 1)> 0)
        {
            // Check legendary relic balance info is null or not
            if(tmpLegendaryRelicBalanceInfo != null)
            {
                // Check max level
                if(int.Parse(tmpLegendaryRelicBalanceInfo.MaxLevel) != - 1 && tmpLegendaryRelicModel.Level >= int.Parse(tmpLegendaryRelicBalanceInfo.MaxLevel))
                {
                    int refundLegendaryRelicCount = tmpLegendaryRelicModel.Count - 1;
                    refundGem = refundLegendaryRelicCount * ServerNetworkManager.Instance.Setting.RefundLegendaryRelicPrice;

                    // Refund Process
                    ServerNetworkManager.Instance.UnsyncedGem += refundGem;
                    ServerNetworkManager.Instance.User.UnsyncedGemChangeCount ++;
                    tmpLegendaryRelicModel.Count = 1;
                }
                else
                    isError = true;
            }
            else
                isError = true;
        }
        else
            isError = true;

        if(isError)
            Debug.Log("OutgameController.RequestLegendaryRelicRefund: Error");
        else
        {    
            uiViewOutgameMain.RefreshTopbar();
            uiViewOutgameMain.RefreshRightHorizontalbar();
            
            UIManager.Instance.ShowAlert(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_thank_you_with_rewards"), refundGem), null, null);

            onSuccess?.Invoke();
        }
    }

#endregion

#region Logic No Server Upgrade Stat
    public void UpgradeStat(StatType statType, Action onSuccess)
    {
        var userUpgradeInfo = ServerNetworkManager.Instance.User.Upgrade;
       
        BigInteger currentGold = ServerNetworkManager.Instance.Inventory.Gold;
        BigInteger requiredGold = 0;
        int currentLevel = 1;
        bool isSuccess = true;

        switch(statType)
        {
            case StatType.ATTACK_POWER:
                requiredGold = userUpgradeInfo.BaseAttackLevelupPrice;
                currentLevel = userUpgradeInfo.BaseAttackLevel;
                
                // Check max level
                if(currentLevel >= ServerNetworkManager.Instance.Setting.MaxStatLevel)
                    isSuccess = false;
            break;
            case StatType.CRITICAL_POWER:
                requiredGold = userUpgradeInfo.CriticalAttackLevelupPrice;
                currentLevel = userUpgradeInfo.CriticalAttackLevel;

                // Check max level
                if(currentLevel >= ServerNetworkManager.Instance.Setting.MaxStatLevel)
                    isSuccess = false;
            break;
            case StatType.CRITICAL_ATTACK_CHANCE:
                requiredGold = userUpgradeInfo.CriticalChanceLevelupPrice;
                currentLevel = userUpgradeInfo.CriticalChanceLevel;
                
                // Check max values
                if(userUpgradeInfo.CriticalChance >= 1f)
                    isSuccess = false;
            break;
            case StatType.SUPER_POWER:
                requiredGold = userUpgradeInfo.SuperAttackLevelupPrice;
                currentLevel = userUpgradeInfo.SuperAttackLevel;
                
                // Check max level
                if(currentLevel >= ServerNetworkManager.Instance.Setting.MaxStatLevel)
                    isSuccess = false;
            break;
            case StatType.SUPER_ATTACK_CHANCE:
                requiredGold = userUpgradeInfo.SuperChanceLevelupPrice;
                currentLevel = userUpgradeInfo.SuperChanceLevel;

                // Check max values
                if(userUpgradeInfo.SuperChance >= 1f)
                    isSuccess = false;
            break;
            case StatType.ULTIMATE_POWER:
                requiredGold = userUpgradeInfo.UltimateAttackLevelupPrice;
                currentLevel = userUpgradeInfo.UltimateAttackLevel;

                if(userUpgradeInfo.SuperChance < 1f)
                    isSuccess = false;

                // Check max level
                if(currentLevel >= ServerNetworkManager.Instance.Setting.MaxStatLevel)
                    isSuccess = false;
            break;
            case StatType.ULTIMATE_ATTACK_CHANCE:
                requiredGold = userUpgradeInfo.UltimateChanceLevelupPrice;
                currentLevel = userUpgradeInfo.UltimateChanceLevel;

                if(userUpgradeInfo.SuperChance < 1f)
                    isSuccess = false;

                // Check max values
                if(userUpgradeInfo.UltimateChance >= 1f)
                    isSuccess = false;
            break;
            case StatType.HYPER_POWER:
                requiredGold = userUpgradeInfo.HyperAttackLevelupPrice;
                currentLevel = userUpgradeInfo.HyperAttackLevel;

                if(userUpgradeInfo.UltimateChance < 1f)
                    isSuccess = false;

                // Check max level
                if(currentLevel >= ServerNetworkManager.Instance.Setting.MaxStatLevel)
                    isSuccess = false;
            break;
            case StatType.HYPER_ATTACK_CHANCE:
                requiredGold = userUpgradeInfo.HyperChanceLevelupPrice;
                currentLevel = userUpgradeInfo.HyperChanceLevel;

                if(userUpgradeInfo.UltimateChance < 1f)
                    isSuccess = false;

                // Check max values
                if(userUpgradeInfo.HyperChance >= 1f)
                    isSuccess = false;
            break;
        }

        // Check currency vaildate
        if(currentGold < requiredGold)
            isSuccess = false;
        
        if(isSuccess)
        {
            // Logic for upgrade stat
            switch(statType)
            {
                case StatType.ATTACK_POWER:
                    userUpgradeInfo.BaseAttackLevel = userUpgradeInfo.BaseAttackLevel + 1;
                    UpdateAchievement("achievement_03", 1);
                break;
                case StatType.CRITICAL_POWER:
                    userUpgradeInfo.CriticalAttackLevel = userUpgradeInfo.CriticalAttackLevel + 1;
                    UpdateAchievement("achievement_04", 1);
                break;
                case StatType.CRITICAL_ATTACK_CHANCE:
                    userUpgradeInfo.CriticalChanceLevel = userUpgradeInfo.CriticalChanceLevel + 1;
                    UpdateAchievement("achievement_05", 1);
                break;
                case StatType.SUPER_POWER:
                    userUpgradeInfo.SuperAttackLevel = userUpgradeInfo.SuperAttackLevel + 1;
                    UpdateAchievement("achievement_06", 1);
                break;
                case StatType.SUPER_ATTACK_CHANCE:
                    userUpgradeInfo.SuperChanceLevel = userUpgradeInfo.SuperChanceLevel + 1;
                    UpdateAchievement("achievement_07", 1);
                break;
                case StatType.ULTIMATE_POWER:
                    userUpgradeInfo.UltimateAttackLevel = userUpgradeInfo.UltimateAttackLevel + 1;
                break;
                case StatType.ULTIMATE_ATTACK_CHANCE:
                    userUpgradeInfo.UltimateChanceLevel = userUpgradeInfo.UltimateChanceLevel + 1;
                break;
                case StatType.HYPER_POWER:
                    userUpgradeInfo.HyperAttackLevel = userUpgradeInfo.HyperAttackLevel + 1;
                break;
                case StatType.HYPER_ATTACK_CHANCE:
                    userUpgradeInfo.HyperChanceLevel = userUpgradeInfo.HyperChanceLevel + 1;
                break;
            }

            ServerNetworkManager.Instance.Inventory.Gold -= requiredGold;

            // Need to recalculate numbers
            NumberMainController.Instance.Refresh();

            // Refresh the view
            uiViewOutgameMain.RefreshTopbar();
            uiViewOutgameMain.RefreshRightHorizontalbar();

            onSuccess?.Invoke();
        }
        else
           Debug.Log("OutgameController.UpgradeStat: Error");
    }
#endregion

#region Logic No Server Upgrade Skill
    public void UpgradeSkill(int skillIndex, Action onSuccess)
    {
        string skillId = $"skill_0{skillIndex + 1}";
        bool isSuccess = true;
        var tmpModel = ServerNetworkManager.Instance.User.SkillInfo.FirstOrDefault(n => n.Id == skillId);
        var tmpSkillBalanceInfo = BalanceInfoManager.Instance.SkillInfoList.FirstOrDefault(n => n.Id == skillId);
        
        BigInteger currentGold = ServerNetworkManager.Instance.Inventory.Gold;

        // Check null
        if(tmpModel == null || tmpSkillBalanceInfo == null)
            isSuccess = false;

        // Check upgrade max level
        if(tmpModel.Level >= ServerNetworkManager.Instance.Setting.MaxSkillLevel)
            isSuccess = false;
        
        if(currentGold < tmpModel.GoldLevelupCost)
            isSuccess = false;
        
        if(isSuccess)
        {
            ServerNetworkManager.Instance.Inventory.Gold -= tmpModel.GoldLevelupCost;
            
            tmpModel.Level ++;
            tmpModel.GoldLevelUpCount ++;

            // Need to recalculate numbers
            NumberMainController.Instance.Refresh();

            // Refresh the view
            uiViewOutgameMain.RefreshTopbar();
            uiViewOutgameMain.RefreshRightHorizontalbar();
            
            // Refresh skill buttons if user unlock this skill
            if(tmpModel.Level == 1)
            {
                uiViewOutgameMain.RefreshSkillButtons();
            }

            onSuccess?.Invoke();
        }
        else
            Debug.Log("OutgameController.UpgradeSkill: Error");
    }
#endregion

#region Logic No Server Pet
    public void SetMainPet(string petId)
    {
        // Find the target to add
        var target = ServerNetworkManager.Instance.Inventory.PetList.FirstOrDefault(n => n.Id == petId);
        if(target == null)
            return;
        
        // Check pet already in the selected list
        if(!ServerNetworkManager.Instance.Inventory.SelectedPetList.Contains(target))
            return;

        // Set main pet
        ServerNetworkManager.Instance.Inventory.MainPet = target;     

        // Need to affect ingame's pet list
        IngameMainController.Instance.ResetPets(false);
    }

    public void AddPetToSlot(string petId)
    {
        // Find the target to add
        var target = ServerNetworkManager.Instance.Inventory.PetList.FirstOrDefault(n => n.Id == petId);
        if(target == null)
            return;
        
        // Should not have the pet already in the selected list
        if(ServerNetworkManager.Instance.Inventory.SelectedPetList.Contains(target))
            return;

        // Add the pet
        ServerNetworkManager.Instance.Inventory.SelectedPetList.Add(target);

        // If main pet is null then set this pet as main pet
        if(ServerNetworkManager.Instance.Inventory.MainPet == null)
            ServerNetworkManager.Instance.Inventory.MainPet = target;

        // Need to affect ingame's pet list
        IngameMainController.Instance.ResetPets(false);

        // Need to recalculate numbers
        NumberMainController.Instance.Refresh();

        // Refresh the view
        uiViewOutgameMain.RefreshTopbar();
    }

    public void RemovePetFromSlot(string petId)
    {
        // Find the target to add
        var target = ServerNetworkManager.Instance.Inventory.PetList.FirstOrDefault(n => n.Id == petId);
        if(target == null)
            return;
        
        // Should already have been added in the selected list
        if(!ServerNetworkManager.Instance.Inventory.SelectedPetList.Contains(target))
            return;
        
        // Add the pet
        ServerNetworkManager.Instance.Inventory.SelectedPetList.Remove(target);

        // If target pet is main pet then change main pet
        if(ServerNetworkManager.Instance.Inventory.MainPet != null && target.Id == ServerNetworkManager.Instance.Inventory.MainPet.Id)
        {
            if(ServerNetworkManager.Instance.Inventory.SelectedPetList.Count > 0)
                ServerNetworkManager.Instance.Inventory.MainPet = ServerNetworkManager.Instance.Inventory.SelectedPetList[0];
            else
                ServerNetworkManager.Instance.Inventory.MainPet = null;
        }

        // Need to affect ingame's pet list
        IngameMainController.Instance.ResetPets(false);

        // Need to recalculate numbers
        NumberMainController.Instance.Refresh();

        // Refresh the view
        uiViewOutgameMain.RefreshTopbar();

    }

    public bool GoldLevelUpPet(string petId)
    {
        // Find the target
        var target = ServerNetworkManager.Instance.Inventory.PetList.FirstOrDefault(n => n.Id == petId);
        if(target == null)
            return false;

        // Check max level
        if(target.Level >= ServerNetworkManager.Instance.Setting.MaxPetLevel)
            return false;

        var requiredGold = target.GoldLevelupCost;

        // Do level up
        if(ServerNetworkManager.Instance.Inventory.Gold >= requiredGold)
        {
            ServerNetworkManager.Instance.Inventory.Gold -= requiredGold;
            target.Level ++;
            target.GoldLevelUpCount ++;

            // Need to recalculate numbers
            NumberMainController.Instance.Refresh();

            // Refresh the view
            uiViewOutgameMain.RefreshTopbar();

            return true;
        }

        return false;

    }

    public bool PetLevelUpPet(string petId)
    {
        // Find the target
        var target = ServerNetworkManager.Instance.Inventory.PetList.FirstOrDefault(n => n.Id == petId);
        if(target == null)
            return false;
        
        // Check max level
        if(target.Level > (ServerNetworkManager.Instance.Setting.MaxPetLevel - 10))
            return false;

        var requiredPet = target.PetLevelupCost;

        // Do level up
        if(target.Count >= requiredPet)
        {
            target.Count -= requiredPet;
            target.Level += 10;
            target.PetLevelUpCount ++;

            // Need to recalculate numbers
            NumberMainController.Instance.Refresh();

            // Refresh the view
            uiViewOutgameMain.RefreshTopbar();

            return true;
        }

        return false;
    }

    public bool HeartLevelUpPet(string petId)
    {
        // Find the target
        var target = ServerNetworkManager.Instance.Inventory.PetList.FirstOrDefault(n => n.Id == petId);
        if(target == null)
            return false;

        // Check max level
        if(target.Level > (ServerNetworkManager.Instance.Setting.MaxPetLevel - 5))
            return false;

        var requiredHeart = target.HeartLevelupCost;

        // Do level up
        if(ServerNetworkManager.Instance.Inventory.Heart >= requiredHeart)
        {
            ServerNetworkManager.Instance.Inventory.Heart -= requiredHeart;
            target.Level += 5;
            target.HeartLevelUpCount ++;

            // Need to recalculate numbers
            NumberMainController.Instance.Refresh();

            // Refresh the view
            uiViewOutgameMain.RefreshTopbar();

            return true;
        }

        return false;
    }

    public void SellPet(string petId)
    {
        // Find the target
        var target = ServerNetworkManager.Instance.Inventory.PetList.FirstOrDefault(n => n.Id == petId);
        if(target == null)
            return;

        // Do sell if avail
        if(target.Count > 1)
        {
            ServerNetworkManager.Instance.Inventory.Heart += target.PetSellReward;
            target.Count --;
            uiViewOutgameMain.Refresh();
        }
    }

#endregion

#region Logic No Server Trophy

    public void AddTrophyToSlot(string trophyId)
    {
        // Find the target to add
        var target = ServerNetworkManager.Instance.Inventory.TrophyList.FirstOrDefault(n => n.Id == trophyId);
        if(target == null)
            return;
        
        // Should not have the trophy already in the selected list
        if(ServerNetworkManager.Instance.Inventory.SelectedTrophyList.Contains(target))
            return;
        
        // Add the trophy
        ServerNetworkManager.Instance.Inventory.SelectedTrophyList.Add(target);

        // Refresh the view
        uiViewOutgameMain.RefreshTopbar();
    }

    public void RemoveTrophyFromSlot(string trophyId)
    {
        // Find the target to add
        var target = ServerNetworkManager.Instance.Inventory.TrophyList.FirstOrDefault(n => n.Id == trophyId);
        if(target == null)
            return;
        
        // Should already have been added in the selected list
        if(!ServerNetworkManager.Instance.Inventory.SelectedTrophyList.Contains(target))
            return;
        
        // Add the trophy
        ServerNetworkManager.Instance.Inventory.SelectedTrophyList.Remove(target);

        // Refresh the view
        uiViewOutgameMain.RefreshTopbar();
    }

    public bool TrophyLevelUpTrophy(string trophyId)
    {
        // Find the target
        var target = ServerNetworkManager.Instance.Inventory.TrophyList.FirstOrDefault(n => n.Id == trophyId);
        if(target == null)
            return false;

        // Check max level
        if(target.Level >= ServerNetworkManager.Instance.Setting.MaxTrophyLevel)
            return false;

        // Do level up
        if(target.Count >= target.TrophyLevelupCost)
        {
            target.Count -= target.TrophyLevelupCost;
            target.Level ++;
            target.TrophyLevelUpCount ++;

            // Refresh the view
            uiViewOutgameMain.RefreshTopbar();
            
            return true;
        }

        return false;
    }

    public bool HeartLevelUpTrophy(string trophyId)
    {
        // Find the target
        var target = ServerNetworkManager.Instance.Inventory.TrophyList.FirstOrDefault(n => n.Id == trophyId);
        if(target == null)
            return false;
            
        // Check max level
        if(target.Level >= ServerNetworkManager.Instance.Setting.MaxTrophyLevel)
            return false;

        // Do level up
        if(ServerNetworkManager.Instance.Inventory.Heart >= target.HeartLevelupCost)
        {
            ServerNetworkManager.Instance.Inventory.Heart -= target.HeartLevelupCost;
            target.Level ++;
            target.HeartLevelUpCount ++;

            // Refresh the view
            uiViewOutgameMain.RefreshTopbar();

            return true;
        }

        return false;
    }

    public void SellTrophy(string trophyId)
    {
        // Find the target
        var target = ServerNetworkManager.Instance.Inventory.TrophyList.FirstOrDefault(n => n.Id == trophyId);
        if(target == null)
            return;

        // Do sell if avail
        if(target.Count > 1)
        {
            ServerNetworkManager.Instance.Inventory.Heart += target.TrophySellReward;
            target.Count --;
        }
    }

#endregion

#region Logic No Server Select Stage
    public void SelectStage(int stageNumber, Action onSuccess)
    {
        bool isError = false;

        if(stageNumber > ServerNetworkManager.Instance.User.StageProgress)
            isError = true;
        else
        {
            ServerNetworkManager.Instance.User.SelectedStage = stageNumber;
            IngameMainController.Instance.InitBattle(false);
        }

        if(isError)
            Debug.Log("OutgameController.SelectStage: Error");
        else
        {     
            uiViewOutgameMain.Refresh();
            onSuccess?.Invoke();
        }
    }
#endregion

#region Logic No Server Apply Package Buff Value
    public void ApplyPackageBuffValue(BuffTier buffTier)
    {
        ServerNetworkManager.Instance.User.AutoEnabledTier = (int)buffTier;
        ServerNetworkManager.Instance.User.AttackBuffTier = (int)buffTier;
        ServerNetworkManager.Instance.User.GoldBuffTier = (int)buffTier;
        ServerNetworkManager.Instance.User.SpeedBuffTier = (int)buffTier;
    }
#endregion

#region Logic For Server Login

    private void LogIn()
	{
        uiViewOutgameLoadingInitial.Show();
		ServerNetworkManager.Instance.Login(
            () => {
                if(ServerNetworkManager.Instance.User.ServerId == "")
                {
                    if(OptionManager.Instance.Language == OptionManager.LanguageModeType.ko)
                        SelectServer(3, () => {RefreshLastDeviceForLogin(false);});
                    else if(OptionManager.Instance.Language == OptionManager.LanguageModeType.ja)
                        SelectServer(4, () => {RefreshLastDeviceForLogin(false);});
                    else
                        SelectServer(2, () => {RefreshLastDeviceForLogin(false);});
                }
                else
                    RefreshLastDeviceForLogin(false);
            }, 
            OnLogInFailure
        );
        
        if(OptionManager.Instance.BGMVolume != 0f)
        {
            audioSource.clip = loadingSound;
            audioSource.loop = false;
            audioSource.Play();
        }
	}

    private void RefreshLastDeviceForLogin(bool wasAlreadyLoggedIn)
    {
        if(wasAlreadyLoggedIn)
        {
            OnLogInComplete(wasAlreadyLoggedIn);
        }
        else
        {
            ServerNetworkManager.Instance.RefreshLastDevice(
                true,
                (willOtherClose, needWaitForOtherClose, needClose) => {
                    if(willOtherClose)
                    {
                        if(needWaitForOtherClose)
                        {
                            UIManager.Instance.HideCanvas("Loading Initial Canvas");
                            UIManager.Instance.ShowAlertLocalized(
                                "message_new_device", 
                                () => {
                                    UIManager.Instance.ShowAlertLocalized("message_new_device_3", null, null, true);
                                    StartCoroutine(ReloginAfter(30));
                                },
                                null,
                                true
                            );
                        }
                        else
                        {
                            OnLogInComplete(wasAlreadyLoggedIn);
                            UIManager.Instance.ShowAlertLocalized(
                                "message_new_device", 
                                () => {
                                    UIManager.Instance.ShowAlertLocalized("message_new_device_2", null, null);
                                }, 
                                null
                            );
                        }

                        // Remove stat lock key
                        if(PlayerPrefs.HasKey("stat_lock_list_" + ServerNetworkManager.Instance.User.PlayFabId))
                            PlayerPrefs.DeleteKey("stat_lock_list_" + ServerNetworkManager.Instance.User.PlayFabId);
                    }
                    else if(needClose)
                    {
                        UIManager.Instance.HideCanvas("Loading Initial Canvas");
                        UIManager.Instance.ShowAlertLocalized("message_other_device_login", null, null, true);
                        StartCoroutine(ApplicationEndAfter());
                    }
                    else
                    {
                        OnLogInComplete(wasAlreadyLoggedIn);
                    }
                },
                (isBanned) => {
                    if(isBanned)
                    {
                        Debug.Log("OutgameController.SyncPlayerData: Account is banned");
                    
                        UIManager.Instance.ShowAlertLocalized(
                            "message_banned", 
                            () => {
                                PageOpenController.Instance.OpenHelp();
                                Application.Quit();
                            }, 
                            null
                        );
                    }
                }
            );
        }
        
    }

	private void OnLogInComplete(bool wasAlreadyLoggedIn)
	{
#region Basic Visual And Audio Changes

        // When log in process is finished, initial loading image should be hidden
        uiViewOutgameLoadingInitial.Hide();

        // Refresh main menu to show data from login
        uiViewOutgameMain.Refresh();

        // Start updating values that are needed to updated every 1 seconds
        uiViewOutgameMain.StartUpdateTime();

#endregion

#region Logic For Log In

        // Prepare ads
        InterstitialRewardedManager.Instance.Init();

        // Change language
        CheckServerLanguage();

        RefreshFreeGemDailyLeftLimitBackground();

        RefreshNoticeBackground();

        if(!ChatManager.Instance.IsConnected)
            ConnectPhotonChat();
        else
            ChatOn();

        // Apply package buff value
        if(ServerNetworkManager.Instance.PurchasedBasicPackageList.Exists(n => n == "bundle_package_basic_05"))
            ApplyPackageBuffValue(BuffTier.BUFF_TIER_5);
        else if(ServerNetworkManager.Instance.PurchasedBasicPackageList.Exists(n => n == "bundle_package_basic_04"))
            ApplyPackageBuffValue(BuffTier.BUFF_TIER_4);
        else if(ServerNetworkManager.Instance.PurchasedBasicPackageList.Exists(n => n == "bundle_package_basic_03"))
            ApplyPackageBuffValue(BuffTier.BUFF_TIER_3);
        else if(ServerNetworkManager.Instance.PurchasedBasicPackageList.Exists(n => n == "bundle_package_basic_02"))
            ApplyPackageBuffValue(BuffTier.BUFF_TIER_2);
        else if(ServerNetworkManager.Instance.PurchasedBasicPackageList.Exists(n => n == "bundle_package_basic_01"))
            ApplyPackageBuffValue(BuffTier.BUFF_TIER_1);

        // Start logic that is running every 1 second
        StartCoroutine(LogicOnEverySecondCoroutine());

        // Start logic that is running every 30 seconds
        StartCoroutine(LogicOnEverySomeSecondsCoroutine());

        // Start logic that is running every 10 mins
        StartCoroutine(LogicOnEverySomeMinutesCoroutine());

        // Re-login reward
        if(!wasAlreadyLoggedIn)
            CheckSupplyBox();

        // Get entire server ranking
        CheckRanking();

        // Check for liapp
        CheckLiappValidity();

        // Show mini Tutorial
        MiniTutorialController.Instance.CheckTutorial();

#endregion

#region Popups or Battle Results For Log In

        if(ServerNetworkManager.Instance.User.NickName == "") // Ask nickname change if this if this is the first time since the application start.
        {
            uiPopupNicknameCreate.OpenWithValues(StartBattle);
        }
        else
        {
            StartBattle();

            if(InterSceneManager.Instance.DidArenaBattle)
            {
                ArenaEnd(InterSceneManager.Instance.DidArenaWin, ServerNetworkManager.Instance.Arena.Season, ServerNetworkManager.Instance.Arena.SeasonWeekly, false, null, null);
            }
            else if(InterSceneManager.Instance.DidRaidBattle)
            {
                if(InterSceneManager.Instance.DidRaidDodge && InterSceneManager.Instance.DidRaidDodgeBecauseOfHost)
                    UIManager.Instance.ShowAlertLocalized("message_raid_master_gone", null, null);
                    
                RaidEnd(
                    InterSceneManager.Instance.DidRaidDodge, 
                    InterSceneManager.Instance.DidRaidWin, 
                    RaidManager.Instance.CurrentFloor, 
                    InterSceneManager.Instance.MyRaidRank, 
                    InterSceneManager.Instance.TotalRaidPlayMemberCount,
                    InterSceneManager.Instance.RaidSkillUseCount,
                    null, 
                    null
                );
            }
            else if(InterSceneManager.Instance.DidTowerBattle)
            {
                TowerBattleEnd(InterSceneManager.Instance.DidTowerWin, false);
            }
            else if(InterSceneManager.Instance.DidGuildRaidBattle)
            {
                if(InterSceneManager.Instance.DidRaidDodge && InterSceneManager.Instance.DidRaidDodgeBecauseOfHost)
                    UIManager.Instance.ShowAlertLocalized("message_raid_master_gone", null, null);
                
                if(InterSceneManager.Instance.DidRaidDodge)
                    InterSceneManager.Instance.GuildRaidScore = 0;
                
                var participantList = new List<string>();
                foreach(var tmp in InterSceneManager.Instance.GuildRaidParticipants)
                    participantList.Add(tmp);

                var participantScoreList = new List<int>();
                foreach(var tmp in InterSceneManager.Instance.GuildRaidParticipantScores)
                    participantScoreList.Add(tmp);

                GuildRaidEnd(
                    InterSceneManager.Instance.DidRaidDodge, 
                    InterSceneManager.Instance.GuildRaidScore, 
                    participantList, 
                    participantScoreList, 
                    ServerNetworkManager.Instance.GuildRaidData.Season,
                    InterSceneManager.Instance.RaidSkillUseCount,
                    null, 
                    null
                );
            }
            
            // Ask for review when getting good result
            if(InterSceneManager.Instance.GoodSummonCount >= 1)
            {
                InterSceneManager.Instance.GoodSummonCount = 0;
                CheckEnjoyingPopup();
            }
        }

#endregion

	}

    private void StartBattle()
    {
        // Actually start battle
        IngameMainController.Instance.InitBattle();

        // Refresh main menu to show data as battle started
        uiViewOutgameMain.Refresh();
    }

    private void OnLogInFailure(string reason)
    {
        Debug.Log("OutgameController.OnLogInFailure: " + reason);
        
        HideLoading();
        UIManager.Instance.HideCanvas("Loading Initial Canvas");

        if(reason == "below_min_version")
        {
            // The user needs to update the application!
            UIManager.Instance.ShowAlert(
                LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_" + reason),
                () => {
                    PageOpenController.Instance.OpenMarketPage();
                    Application.Quit();
                },
                null,
                true
            );
        }
        else if(reason == "in_maintenance")
        {
            UIManager.Instance.ShowAlert(
                LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_" + reason),
                () => {
                    Application.Quit();
                },
                null,
                true
            );
        }
        else if(reason == "banned")
        {			
			UIManager.Instance.ShowAlertLocalized("message_banned", () => {
                PageOpenController.Instance.OpenHelp();
				Application.Quit();
			}, null, true);
        }
        else{
            UIManager.Instance.ShowAlert(
                LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_etc"),
                () => {
                    Application.Quit();
                },
                null,
                true
            );
        }
    }

    private void LinkAccountWithCurrentAuthKey()
    {
#if UNITY_IPHONE && !UNITY_EDITOR
        ServerNetworkManager.Instance.LinkAccountWithGameCenterAuthkey(Social.localUser.id, OnLinkAccountWithCurrentAuthKey, OnLinkAccountWithCurrentAuthKeyFailed);
#else
        ServerNetworkManager.Instance.LinkAccountWithCustomAuthkey(Social.localUser.id, OnLinkAccountWithCurrentAuthKey, OnLinkAccountWithCurrentAuthKeyFailed);
#endif
    }

    private void OnLinkAccountWithCurrentAuthKey()
    {
        HideLoading();

        OptionManager.Instance.LoginMode = OptionManager.LoginModeType.PLATFORM_DEFAULT;
        SceneChangeManager.Instance.LoadMainGame(true, true);

    }

    private void OnLinkAccountWithCurrentAuthKeyFailed()
    {
        HideLoading();
        
        Debug.Log("OutgameController.OnLinkAccountWithCurrentAuthKeyFailed");
        UIManager.Instance.ShowAlertLocalized(
            "message_link_fail", 
            null, 
            null
        );
    }
    
#endregion

#region Logic For Server Rest Reward
    private BigInteger receiveGold = 0;
    
    private void CheckSupplyBox()
    {
        receiveGold = 0;

        ServerNetworkManager.Instance.RefreshSupplyBox(
            (minsPassed) => {
                if(minsPassed != 0)
                {
                    var currentTimeStampSecond = DateTimeOffset.Now.ToUnixTimeSeconds();

                    // Get local's passed time
                    var secondsPassedLocal = 0d;
                    if(ServerNetworkManager.Instance.User.TimeStamp != -1d)
                        secondsPassedLocal = System.Math.Max(currentTimeStampSecond - ServerNetworkManager.Instance.User.TimeStamp, 0d);
                    var minsPassedLocal = (int)(secondsPassedLocal / 60d);

                    // Give gold based on the mins passed
                    receiveGold = NumberMainController.Instance.FinalSupplyBoxIncome * Mathf.Min(minsPassed, minsPassedLocal) / 60;

                    // Check user have basic package
                    bool isOwnedBasicPackage = false;
                    if(ServerNetworkManager.Instance.PurchasedBasicPackageList.Count > 0)
                    {
                        isOwnedBasicPackage = true;
                        receiveGold = receiveGold * 2;
                    }

                    if(receiveGold != 0)
                    {
                        ServerNetworkManager.Instance.Inventory.Gold += receiveGold;

                        uiViewOutgameMain.RefreshTopbar();
                        uiPopupRestReward.OpenWithValues(Mathf.Min(minsPassed, minsPassedLocal), receiveGold, isOwnedBasicPackage, GetDoubleSupplyBox);
                    }
                }
            }, 
            null
        );
    }

    private void GetDoubleSupplyBox()
    {
#if UNITY_ANDROID
        refocusLimit = -1d;
#endif
        ShowLoading();

        InterstitialRewardedManager.Instance.Init();
        InterstitialRewardedManager.Instance.ShowAds(
            "ads_double_rest_reward",
            (showResult) => {
                if(showResult == ShowResult.Finished)
                {
                    uiPopupRestReward.Close();

                    HideLoading();
                    
                    ServerNetworkManager.Instance.Inventory.Gold += receiveGold;
                    uiViewOutgameMain.RefreshTopbar();

                    // For log event
                    ServerNetworkManager.Instance.User.BuffAndRestRewardCount = ServerNetworkManager.Instance.User.BuffAndRestRewardCount + 1;
                    
                    var tmpAchievementModel = ServerNetworkManager.Instance.User.AchievementInfo.FirstOrDefault(n => n.Id == "achievement_12");
                    if(tmpAchievementModel != null)
                        AnalyticsController.Instance.AdRewardEvent(tmpAchievementModel.Count + ServerNetworkManager.Instance.User.BuffAndRestRewardCount);
                    else
                        AnalyticsController.Instance.AdRewardEvent(ServerNetworkManager.Instance.User.BuffAndRestRewardCount);
                }
                else if(showResult == ShowResult.Skipped)
                {
                    HideLoading();
                    ShowToast(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_no_ad_cancel"));
                }
                else
                {
                    HideLoading();
                    ShowToast(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_no_ad_avail"));
                }
            }
        );
    }

#endregion

#region Logic For Server Coupon
    public void RedeemCoupon(string couponCode, Action onSuccess)
    {
        UIManager.Instance.ShowCanvas("Loading Canvas");
    
        if(couponCode.Length < 12)
		{
			ServerNetworkManager.Instance.OneTimeReward(
				couponCode,
				() => {
						ServerNetworkManager.Instance.GetInventory(
							()=>{
								UIManager.Instance.HideCanvas("Loading Canvas");
								uiViewOutgameMain.Refresh();
								UIManager.Instance.ShowAlertLocalized(
									"message_thank_you",
									null, 
									null
								);
								
                                onSuccess?.Invoke();
							}, 
							()=>{
								UIManager.Instance.HideCanvas("Loading Canvas");
							}
						);
					}, 
				(reasonOfError) => {
						UIManager.Instance.HideCanvas("Loading Canvas");
						
                        if(reasonOfError != "")
                            UIManager.Instance.ShowAlertLocalized("message_" + reasonOfError, null, null);
                        else
                            UIManager.Instance.ShowAlertLocalized("message_coupon_failed", null, null);
					}
			);
		}
		else
		{
			ServerNetworkManager.Instance.RedeemCoupon(
				couponCode,
				() => {
						ServerNetworkManager.Instance.GetInventory(
							()=>{
								UIManager.Instance.HideCanvas("Loading Canvas");
								uiViewOutgameMain.Refresh();
								UIManager.Instance.ShowAlertLocalized(
									"message_thank_you",
									null, 
									null
								);
								
                                onSuccess?.Invoke();
							}, 
							()=>{
								UIManager.Instance.HideCanvas("Loading Canvas");
							}
						);
					}, 
				() => {
						UIManager.Instance.HideCanvas("Loading Canvas");
						UIManager.Instance.ShowAlertLocalized("message_coupon_failed", null, null);
					}
			);
		}
    }
#endregion

#region Logic For Server Virtual Server

    public void SelectServer(int serverIndex, Action onComplete)
    {
        ShowLoading();

        var serverId = "";
        if(serverIndex != 1)
            serverId = serverIndex.ToString("D2");

        ServerNetworkManager.Instance.JoinVirtualServer(
            serverId, 
            (resultServerId) => {
                HideLoading();
                ServerNetworkManager.Instance.User.ServerId = resultServerId;
                onComplete?.Invoke();
            },
            () => {
                HideLoading();
                onComplete?.Invoke();
            }
        );
    }

#endregion

#region Logic For Server Free Gem

    public void RefreshFreeGemDailyLeftLimitBackground()
    {
        ServerNetworkManager.Instance.GetFreeGemDailyLeftLimit(
            () => {
                if(uiViewOutgameMain != null && uiViewOutgameMain.isActiveAndEnabled)
                    uiViewOutgameMain.RefreshMenuGroup();
            }, 
            null
        );
    }

    public void ShowFreeGemPopup()
    {
        ShowLoading();

        // Get free gem daily limit
        ServerNetworkManager.Instance.GetFreeGemDailyLeftLimit(
            () => {
                HideLoading();
                
                // Check free gem daily limit
                if(ServerNetworkManager.Instance.FreeGemDailyLeftLimit > 0)
                {
                    if(ServerNetworkManager.Instance.PurchasedBasicPackageList.Exists(n => n == "bundle_package_basic_01"))
                        ReceiveFreeGem();
                    else
                    {
                        var setting = ServerNetworkManager.Instance.Setting;
                        string text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "desc_free_gem"), 
                        setting.FreeGemMinAmount, setting.FreeGemMaxAmount, setting.FreeGemDailyLimit, ServerNetworkManager.Instance.FreeGemDailyLeftLimit);
                        UIManager.Instance.ShowAlert(text, ShowFreeGemAds, () => {});
                    }
                }
                else
                    UIManager.Instance.ShowAlertLocalized("message_free_gem_daily_limit_over", null, null);
  
            }, () => {
                HideLoading();

                UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
            }
        );
    }   

    public void ShowFreeGemAds()
    {
#if UNITY_ANDROID
        refocusLimit = -1d;
#endif
        ShowLoading();
        InterstitialRewardedManager.Instance.Init();
        InterstitialRewardedManager.Instance.ShowAds("free_gem", OnShowFreeGemAds);
    }

    private void OnShowFreeGemAds(ShowResult showResult)
    {
        HideLoading();
                    
        // Once see the ad, give the reward for that!
        if(showResult == ShowResult.Finished)
            ReceiveFreeGem();
        else if(showResult == ShowResult.Skipped)
            ShowToast(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_no_ad_cancel"));
        else
            ShowToast(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_no_ad_avail"));
    }

    public void ReceiveFreeGem()
    {
        ShowLoading();
		ServerNetworkManager.Instance.GetFreeGemAds(OnReceiveFreeGem, HideLoading);
    }

    private void OnReceiveFreeGem(int gemCount)
    {
        HideLoading();
        UpdateAchievement("achievement_12", 1);

        uiViewOutgameMain.Refresh();

        string toastText = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_thank_you_with_rewards"), gemCount);

        if(ServerNetworkManager.Instance.PurchasedBasicPackageList.Exists(n => n == "bundle_package_basic_01"))
        {
            string additionalText = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "desc_free_gem_limit"), 
            ServerNetworkManager.Instance.Setting.FreeGemDailyLimit, ServerNetworkManager.Instance.FreeGemDailyLeftLimit);

            toastText = $"{toastText} {additionalText}";
        }   

        ShowToast(toastText);
    }

#endregion

#region Logic For Server Nickname
    
    public void CreateNickName(string newNickName, Action onSuccess)
    {
        if(!this.CheckText(newNickName))
        {
            ShowToastInOverFade(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_nickname_change_failed"));
        }
        else
        {
            ShowLoading();

            ServerNetworkManager.Instance.SetPlayerName(newNickName, true,
                () => {
                        HideLoading();

                        uiViewOutgameMain.Refresh();
                        
                        if(GameObject.FindObjectOfType<UIPopupNicknameCreate>() != null && GameObject.FindObjectOfType<UIPopupNicknameCreate>().gameObject.activeSelf)
                            uiPopupNicknameCreate.Close();

                        onSuccess?.Invoke();
                    }, 
                (isDuplicated) => {
                        HideLoading();
                        
                        if(isDuplicated)
                            ShowToastInOverFade(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_nickname_duplicated"));
                        else
                            ShowToastInOverFade(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_nickname_change_failed"));
                    }
            );
        }
    }

    public void ChangeNickName(string newNickName)
    {
        if(ServerNetworkManager.Instance.TotalGem < 500)
        {
            UIManager.Instance.ShowAlertLocalized("message_not_enough_gem", null, null);
            return;
        }

        if(!this.CheckText(newNickName))
        {
            ShowToastInOverFade(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_nickname_change_failed"));
        }
        else
        {
            ShowLoading();

            ServerNetworkManager.Instance.SetPlayerName(newNickName, false,
                () => {
                        SyncPlayerData(true, "AfterChangeNickname", () => {
                            HideLoading();
                        
                            uiPopupOption.Refresh();
                            uiViewOutgameMain.Refresh();

                            if(GameObject.FindObjectOfType<UIPopupNicknameChange>() != null && GameObject.FindObjectOfType<UIPopupNicknameChange>().gameObject.activeSelf)
                                uiPopupNicknameChange.Close();
                        });
                    }, 
                (isDuplicated) => {
                        HideLoading();

                        if(GameObject.FindObjectOfType<UIPopupNicknameChange>() != null && GameObject.FindObjectOfType<UIPopupNicknameChange>().gameObject.activeSelf)
                        {
                            if(isDuplicated)
                                UIManager.Instance.ShowAlertLocalized("message_nickname_duplicated", null, null);                    
                            else
                                UIManager.Instance.ShowAlertLocalized("message_nickname_change_failed", null, null);                    
                        }    
                    }
            );
        }
    }

#endregion

#region Logic For Server Language

    public void ChangeServerLanguage()
    {
        
        var language = OptionManager.Instance.Language.ToString();
    
        if(OptionManager.Instance.Language == OptionManager.LanguageModeType.zh_chs)
            language = "zh-Hans";
        else if(OptionManager.Instance.Language == OptionManager.LanguageModeType.zh_cht)
            language = "zh-Hant";

        ServerNetworkManager.Instance.SetProfileLanguage(
            language,
            () => {
                PlayerPrefs.SetInt("is_server_lanugage_set", 1);
            }, 
            () => {
                PlayerPrefs.SetInt("is_server_lanugage_set", 1);
            }
        );
    }

#endregion

#region Logic For Serer Last Device

    public void RefreshLastDeviceBackground()
    {
        if(Application.internetReachability == NetworkReachability.NotReachable)
        {
            // This is the case when network connection is lost!

            // Need also save local cache
            SaveLocalCache(true);

            // Re login
            SceneChangeManager.Instance.ForceShowPromotion = true;
            ServerNetworkManager.Instance.Logout();
            ChatManager.Instance.Disconnect();
            SceneChangeManager.Instance.LoadMainGame(true, true);
        }
        else
        {
            ServerNetworkManager.Instance.RefreshLastDevice(
                false,
                (willOtherClose, needWaitForOtherClose, needClose) => {
                    if(needClose)
                    {
                        UIManager.Instance.ShowAlertLocalized("message_other_device_login", null, null);
                        StartCoroutine(ApplicationEndAfter());
                    }
                },
                (isBanned) => {
                    if(isBanned)
                    {
                        Debug.Log("OutgameController.SyncPlayerData: Account is banned");
                    
                        UIManager.Instance.ShowAlertLocalized(
                            "message_banned", 
                            () => {
                                PageOpenController.Instance.OpenHelp();
                                Application.Quit();
                            }, 
                            null
                        );
                    }
                }
            );
        }
    }

    private IEnumerator ReloginAfter(float second)
    {
        ShowLoading();
        yield return new WaitForSeconds(second);

        // Re login
        SceneChangeManager.Instance.ForceShowPromotion = true;
        ServerNetworkManager.Instance.Logout();
        ChatManager.Instance.Disconnect();
        SceneChangeManager.Instance.LoadMainGame(true, true);
    }

    private IEnumerator ApplicationEndAfter()
    {
        ShowLoading();
        yield return new WaitForSeconds(5f);
        Application.Quit();
    }

#endregion

#region Logic For Server Purchase
    public void CheckLimitedPackageAvailable(string packageId)
    {
        ShowLoading();
        ServerNetworkManager.Instance.UseDailyWeeklyPackageIfAvailAndAsked(
            packageId,
            false,
            () => {
                PurchaseNormalPackage(packageId);
            },
            () => {
                HideLoading();
                UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
            }
        );
    }

    public void PurchaseNormalPackage(string packageId)
    {
        if(ServerNetworkManager.Instance.Catalog.NormalPackageList.Exists(n => n.Id == packageId))
        {     
            if(!ServerNetworkManager.Instance.PurchasedBasicPackageList.Exists(n => n == packageId) && !ServerNetworkManager.Instance.PurchasedLimitedPackageList.Exists(n => n == packageId))
            {
                PurchaseManager.Instance.Purchase(packageId);
            }
        }
    }

    public void PurchaseGrowthPackage(string packageId)
    {
        if(ServerNetworkManager.Instance.Catalog.GrowthPackageList.Exists(n => n.Id == packageId))
        {     
            if(!ServerNetworkManager.Instance.PurchasedGrowthPackageList.Exists(n => n == packageId))
            {
                PurchaseManager.Instance.Purchase(packageId);
            }
        }
    }

    public void PurchaseGemBundle(string id)
    {
        if(ServerNetworkManager.Instance.Catalog.GemBundleList.Exists(n => n.ItemId == id))
        {     
            PurchaseManager.Instance.Purchase(id);
        }
    }

    public void OnPurchseShopItem(string itemId)
    {
        // Do refresh shown info by getting values from server.
        ShowLoading();

        if(itemId.Contains("bundle_gem_") && !ServerNetworkManager.Instance.PurchasedGemBundleList.Exists(n => n == itemId))
        {
            // Check gem bonus
            ServerNetworkManager.Instance.RequestGemBonus(itemId,
                () => {
                    RefreshInventoryAfterPurchasShopItem(itemId);
                },
                () => {
                    HideLoading();
                    UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
                }
            );
        }
        else if(itemId.Contains("bundle_package_basic_"))
        {
            if(itemId == "bundle_package_basic_01")
                ApplyPackageBuffValue(BuffTier.BUFF_TIER_1);
            else if(itemId == "bundle_package_basic_02")
                ApplyPackageBuffValue(BuffTier.BUFF_TIER_2);
            else if(itemId == "bundle_package_basic_03")
                ApplyPackageBuffValue(BuffTier.BUFF_TIER_3);
            else if(itemId == "bundle_package_basic_04")
                ApplyPackageBuffValue(BuffTier.BUFF_TIER_4);
            else if(itemId == "bundle_package_basic_05")
                ApplyPackageBuffValue(BuffTier.BUFF_TIER_5);

            RefreshInventoryAfterPurchasShopItem(itemId);
        }
        else if(itemId.Contains("bundle_package_daily") || itemId.Contains("bundle_package_weekly"))
        {
            ServerNetworkManager.Instance.UseDailyWeeklyPackageIfAvailAndAsked(
                itemId,
                true,
                () => {
                    RefreshInventoryAfterPurchasShopItem(itemId);
                },
                () => {
                    RefreshInventoryAfterPurchasShopItem(itemId);
                }
            );
        }
        else if(itemId.Contains("bundle_package_growth_"))
        {
            RefreshInventoryAfterPurchasGrowthSupportItem(itemId);
        }
        else
        {
            RefreshInventoryAfterPurchasShopItem(itemId);
        }
    }

    public void PurchaseGoldBundle(string id)
    {
        Debug.Log("OutgameController.PurchaseGoldBundle: " + id);

        if(ServerNetworkManager.Instance.Catalog.GoldBundleList.Exists(n => n.ItemId == id))
        {
            var targetItem = ServerNetworkManager.Instance.Catalog.GoldBundleList.FirstOrDefault(n => n.ItemId == id);

            if(targetItem != null)
            {
                if(ServerNetworkManager.Instance.TotalGem < targetItem.Price)
                {
                    string message = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_not_enough_gem");
                    UIManager.Instance.ShowAlert(message, null, null);
                    
                    return;
                }
                
                var totalGoldReward = NumberMainController.Instance.FinalBestGoldIncome * targetItem.GivingAmount;
                var totalGoldRewardString = LanguageManager.Instance.NumberToString(totalGoldReward);
		        var givingCreditInfo = string.Format("{0} " + LanguageManager.Instance.GetTextData(LanguageDataType.UI, "item_gold"), totalGoldRewardString);
                
                ShowConfirmMessageBeforePurchase(givingCreditInfo, () => {
                    HideLoading();

                    ServerNetworkManager.Instance.UnsyncedGem -= targetItem.Price;
                    ServerNetworkManager.Instance.User.UnsyncedGemChangeCount ++;

                    ServerNetworkManager.Instance.Inventory.Gold += totalGoldReward;

                    RefreshInventoryAfterPurchasShopItem(targetItem.ItemId);
                });
            }
        }
    }

    public void PurchaseRaidTicket(int amount)
    {
        Debug.Log("OutgameController.PurchaseRaidTicket: " + amount);

        if(ServerNetworkManager.Instance.TotalGem < amount)
        {
            string message = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_not_enough_gem");
            UIManager.Instance.ShowAlert(message, null, null);
            
            return;
        }

        ShowLoading();

        ServerNetworkManager.Instance.UnsyncedGem -= amount;
        ServerNetworkManager.Instance.User.UnsyncedGemChangeCount ++;

        ServerNetworkManager.Instance.Inventory.TicketRaid += amount;

        ServerNetworkManager.Instance.GetInventory(
            ()=>{
                HideLoading();
                
                uiViewOutgameMain.Refresh();
                uiPopupRaid.Refresh();
                uiPopupRaidTicketPurchase.Close();

                var itemName = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "item_raid_ticket");
                var message = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_purchase_success"), itemName);
                
                UIManager.Instance.ShowAlert(
                    message,
                    null, 
                    null
                );
            }, 
            () => {
                HideLoading();

                uiPopupRaidTicketPurchase.Close();
            }
        );
    }

    private void RefreshInventoryAfterPurchasShopItem(string itemId)
    {
        ServerNetworkManager.Instance.GetInventory(
            ()=>{
                HideLoading();

                // Need to recalculate numbers
                NumberMainController.Instance.Refresh();
                
                uiViewOutgameMain.Refresh();
                uiPopupShop.Refresh();

                var itemName = LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, itemId);
                var message = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_purchase_success"), itemName);
                
                UIManager.Instance.ShowAlert(
                    message,
                    null, 
                    null
                );
            }, 
            () => {
                HideLoading();
            }
        );
    }

    private void RefreshInventoryAfterPurchasGrowthSupportItem(string itemId)
    {
        ServerNetworkManager.Instance.GetInventory(
            ()=>{
                HideLoading();
                uiViewOutgameMain.Refresh();
                uiPopupGrowthSupport2.Refresh();

                uiPopupGrowthSupportPurchase.Close();

                var itemName = LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, itemId);
                var message = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_purchase_success"), itemName);
                
                UIManager.Instance.ShowAlert(
                    message,
                    null, 
                    null
                );
            }, 
            () => {
                HideLoading();
            }
        );
    }

    private void ShowConfirmMessageBeforePurchase(string givingCreditInfo, Action onSuccess)
    {
        string messageString = string.Format(
                LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_confirm_purchase"),
                givingCreditInfo
            );
        
        UIManager.Instance.ShowAlert(
            messageString,
            () => {
                ShowLoading();
                onSuccess?.Invoke();
            },
            () => {}
        );
    }

#endregion

#region Logic For Server Growth Support Package

    public void RequestGrowthSupportPackageReward(int packageNumber, int targetLevel)
    {
        ShowLoading();

        string index = "";
					
        if(targetLevel < 10)
            index = string.Format("0{0}", targetLevel);
        else
            index = targetLevel.ToString();

		var growthSupportRewardId = string.Format("growth_support_0{0}_{1}", packageNumber, index);

        if(packageNumber == 0)
        {
            ServerNetworkManager.Instance.RequestGrowthSupportFreeReward(growthSupportRewardId,
                () => {
                    HideLoading();
                    uiViewOutgameMain.Refresh();
                    uiPopupGrowthSupport2.Refresh();

                    var text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_growth_support_reward_success"), targetLevel);
                    UIManager.Instance.ShowAlert(text, null, null);
                },
                () => {
                    HideLoading();

                    UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
                }
            );
        }
        if(packageNumber == 1)
        {
            ServerNetworkManager.Instance.RequestGrowthSupportFirstReward(growthSupportRewardId,
                () => {
                    HideLoading();
                    uiViewOutgameMain.Refresh();
                    uiPopupGrowthSupport2.Refresh();

                    var text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_growth_support_reward_success"), targetLevel);
                    UIManager.Instance.ShowAlert(text, null, null);
                },
                () => {
                    HideLoading();

                    UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
                }
            );
        }  
        else if(packageNumber == 2)
        {
            ServerNetworkManager.Instance.RequestGrowthSupportSecondReward(growthSupportRewardId,
                () => {
                    HideLoading();
                    uiViewOutgameMain.Refresh();
                    uiPopupGrowthSupport2.Refresh();

                    var text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_growth_support_reward_success"), targetLevel);
                    UIManager.Instance.ShowAlert(text, null, null);
                },
                () => {
                    HideLoading();

                    UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
                }
            );
        }
    }

#endregion

#region Logic For Server Daily Reward
    public void RequestDailyReward()
    {
        ShowLoading();

        ServerNetworkManager.Instance.RequestDailyReward(
            () => {
                HideLoading();
                uiPopupDailyReward.Close();
                uiViewOutgameMain.Refresh();
            },
            () => {
                HideLoading();
                UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
            }
        );
    }
#endregion

#region Logic For Server VIP Reward
    public void RequestVIPReward(Action onSucces)
    {
        int currentVIPReward = ServerNetworkManager.Instance.Inventory.VipPoint;

        int availableRewardReqeustCount = currentVIPReward / ServerNetworkManager.Instance.Setting.RequireVIPTicket;
        int totalVIPRewardGem = availableRewardReqeustCount * ServerNetworkManager.Instance.Setting.VIPRewardGem;

        if(availableRewardReqeustCount > 0)
        {
            string text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_vip_exchange_confirm"), availableRewardReqeustCount * ServerNetworkManager.Instance.Setting.RequireVIPTicket, totalVIPRewardGem);
            
            UIManager.Instance.ShowAlert(text, () => {
                ShowLoading();
                ServerNetworkManager.Instance.RequestVIPReward(() => {
                    HideLoading();
                    uiViewOutgameMain.RefreshTopbar();
                    onSucces?.Invoke();
                }, () => {
                    HideLoading();
                    UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
                });
            }, () => {});
        }
    }
#endregion

#region Logic For Server Attendance Event Reward

    public void RequestAttendanceEventReward(int selectedRewardIndex, Action onSucces)
    {
        var attendanceEventInfo = ServerNetworkManager.Instance.AttendanceEventInfo;

        if(attendanceEventInfo == null)
            return;

        //Check reward index validate
        if(selectedRewardIndex >= attendanceEventInfo.AttendanceEventRewardList.Count)
            return;

        ShowLoading();
        
        ServerNetworkManager.Instance.RequestAttendanceEventReward(selectedRewardIndex, () => {
            HideLoading();

            uiViewOutgameMain.Refresh();
            onSucces?.Invoke();
        }, () => {
            HideLoading();

            uiViewOutgameMain.RefreshMenuGroup();
            UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
        });
    }

#endregion

#region Logic For Server Post
    public void UpdateMailList(Action onSuccess, Action onFailure)
    {
        ServerNetworkManager.Instance.GetUserMailList(onSuccess, onFailure);
    }

    public void RequestAttachedItem(int mailId)
    {
        ShowLoading();

        ServerNetworkManager.Instance.RequestAttachedItem(mailId, (itemId, count) => {
            HideLoading();

            var itemName = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "item_" + itemId);
            var successText = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI , "message_mail_get_success"), itemName, count);
            UIManager.Instance.ShowAlert(successText, null, null);
            
            uiViewOutgameMain.Refresh();
            uiPopupInbox2.Refresh();
            
        }, () => {
            HideLoading();
            UIManager.Instance.ShowAlertLocalized("message_mail_get_fail", null, null);
        });
    }

    public void RequestAttachedItemAll()
    {
        // Check mail count
        if(ServerNetworkManager.Instance.Post.MailList.Count <= 0)
        {
            UIManager.Instance.ShowAlertLocalized("message_mail_get_fail", null, null);
            return;
        }

        ShowLoading();

        ServerNetworkManager.Instance.RequestAttachedItemAll((receivedInfo) => {
            HideLoading();

            if(receivedInfo.ItemList.Count == 0)
                UIManager.Instance.ShowAlertLocalized("message_mail_get_fail", null, null);
            else
                uiPopupReceivedReward.OpenWithValues(receivedInfo);

            uiViewOutgameMain.Refresh();
            uiPopupInbox2.Refresh();
           
        }, () => {
            HideLoading();
            UIManager.Instance.ShowAlertLocalized("message_mail_get_fail", null, null);
        });
    }

    public void RequestDeleteMail(int mailId)
    {
        ShowLoading();

        ServerNetworkManager.Instance.RequestDeleteMail(mailId, () => {
            HideLoading();            
            uiPopupInbox2.Refresh();
        }, () => {
            HideLoading();
            UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
        });
    }

    public void RequestAttachedItemSystemMailItemAll()
    {
        // Check system mail count
        if(ServerNetworkManager.Instance.SystemPost.MailList.Count <= 0)
        {
            UIManager.Instance.ShowAlertLocalized("message_mail_get_fail", null, null);
            return;
        }

        // Save origin auto setting value
        var originValue = UIManager.Instance.IsAutoOn;
        UIManager.Instance.IsAutoOn = false;

        // Sync latest player data
        SyncPlayerData(true, "RequestAttachedItemSystemMailItemAll", () => {
            ShowLoading();

            ServerNetworkManager.Instance.RequestAttachedItemSystemMailItemAll((receivedInfo) => {
                HideLoading();

                if(receivedInfo.ItemList.Count == 0)
                {
                    UIManager.Instance.ShowAlertLocalized("message_mail_get_fail", null, null);
                    UIManager.Instance.IsAutoOn = originValue;
                }
                else
                {
                    // For relogin
                    ObscuredPrefs.DeleteKey("dt_us_1" + ServerNetworkManager.Instance.User.PlayFabId);
                    ObscuredPrefs.DeleteKey("dt_iv_1" + ServerNetworkManager.Instance.User.PlayFabId);
                    ObscuredPrefs.DeleteKey("dt_iv_2" + ServerNetworkManager.Instance.User.PlayFabId);
                    ObscuredPrefs.SetBool("is_recently_reset_by_device_change", true);
                    
                    UIManager.Instance.ShowAlertLocalized("message_restart_system_mail_sync", null, null, true);
                    StartCoroutine(ReloginAfter(5));
                }

                uiViewOutgameMain.Refresh();
                uiPopupInbox2.Refresh();           
            }, () => {
                HideLoading();
                UIManager.Instance.ShowAlertLocalized("message_mail_get_fail", null, null);
                UIManager.Instance.IsAutoOn = originValue;
            });
        });
    }

    public void RequestDeleteSystemMail(int mailId)
    {
        ShowLoading();

        ServerNetworkManager.Instance.RequestDeleteSystemMail(mailId, () => {
            HideLoading();            
            uiPopupInbox2.Refresh();
        }, () => {
            HideLoading();
            UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
        });
    }

#endregion

#region Logic For Server Push
    public void SetPushNotificationOn()
    {
        if(!OptionManager.Instance.IsPushOff)
            return;

        ShowLoading();
        
        ServerNetworkManager.Instance.SetPushNotificationOff(false, () => {
            HideLoading();

            var currentTime = System.DateTime.Now.ToString("yyyy-MM-dd");
	
			string text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_push_agree"), currentTime);
			UIManager.Instance.ShowAlert(text, null , null);
        }, () => {
            HideLoading();
            UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
        });
    }

    public void SetPushNotificationOff()
    {
        if(OptionManager.Instance.IsPushOff)
            return;
        
        ShowLoading();
        
        ServerNetworkManager.Instance.SetPushNotificationOff(true, () => {
            HideLoading();

            var currentTime = System.DateTime.Now.ToString("yyyy-MM-dd");
	
			string text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_push_disagree"), currentTime);
			UIManager.Instance.ShowAlert(text, null , null);
        }, () => {
            HideLoading();
            UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
        });
    }
#endregion

#region Logic For Server Player Data

    public void SyncPlayerData(bool forceSync = false, string syncOrigin = "", Action onComplete = null, bool dontBlockEvenIfFailed = false)
    {
        if(!ServerNetworkManager.Instance.IsLoggedIn)
            return;

        // Unless it is forced to sync, don't sync within 60 seconds
        var timeLeft = Mathf.Max(60 + OutgameController.Instance.TimeSyncPlayerData - (int)Time.realtimeSinceStartup, 0);
        if(!forceSync && timeLeft > 0f)
        {
            onComplete?.Invoke();
            return;
        }

        ShowLoading();

        ServerNetworkManager.Instance.SyncPlayerData(
            false,
            syncOrigin,
            () => {
                TimeSyncPlayerData = (int)Time.realtimeSinceStartup;

                HideLoading();
                onComplete?.Invoke();
            },
            (error) => {
                if(error == "ban")
                {
                    Debug.Log("OutgameController.SyncPlayerData: Account is banned");

                    UIManager.Instance.ShowAlertLocalized("message_banned", () => {
                        PageOpenController.Instance.OpenHelp();
                        Application.Quit();
                    }, null);
                }
                else if(error == "data_collision")
                {
                    if(dontBlockEvenIfFailed)
                    {
                        HideLoading();
                        onComplete?.Invoke();
                    }
                    else
                    {
                        UIManager.Instance.ShowAlertLocalized("message_data_collision", null, null);
                    }
                }
                else
                {
                    if(dontBlockEvenIfFailed)
                    {
                        HideLoading();
                        onComplete?.Invoke();
                    }
                    else
                    {
                        UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
                    }
                }
            }
        );
    }

    public void SaveLocalCache(bool forceSave)
    {
        var cacheInventoryJson = ServerInventoryModel.FromLocalModel(ServerNetworkManager.Instance.Inventory).ToJson();
        
        ObscuredPrefs.SetString("dt_iv_1" + ServerNetworkManager.Instance.User.PlayFabId, cacheInventoryJson.Substring(0, cacheInventoryJson.Length / 2));
        ObscuredPrefs.SetString("dt_iv_2" + ServerNetworkManager.Instance.User.PlayFabId, cacheInventoryJson.Substring(cacheInventoryJson.Length / 2, cacheInventoryJson.Length - cacheInventoryJson.Length / 2));
        ObscuredPrefs.SetString("dt_us_1" + ServerNetworkManager.Instance.User.PlayFabId, ServerUserModel.FromLocalModel(ServerNetworkManager.Instance.User).ToJson());
    
        if(forceSave)
            ObscuredPrefs.Save();
    }

#endregion

#region Logic For Server Refresh Notice
    public void RefreshNoticeBackground()
    {
        ServerNetworkManager.Instance.GetTitleNews(
            () => {
                if(uiViewOutgameMain != null && uiViewOutgameMain.isActiveAndEnabled)
                    uiViewOutgameMain.RefreshMenuGroup();
            }, 
            null
        );
    }
#endregion

#region Logic For Server Arena

    public void InitForArena()
    {
        ShowLoading();
        ServerNetworkManager.Instance.RefreshArena(
            (meetMinimumVersion, receivedGems, receivedGemsWeekly) => {
                HideLoading();

                // Open arena popup
                uiPopupArena.Open();
                
                // Show received gems
                if(receivedGems != 0 || receivedGemsWeekly != 0)
                {
                    var rewards = new RewardListModel();
                    if(receivedGems != 0)
                    {    
                        var reward = new RewardModel();
                        reward.ItemId = "gem";
                        reward.Count = receivedGems;
                        rewards.ItemList.Add(reward);
                    }
                    if(receivedGemsWeekly != 0)
                    {
                        var reward = new RewardModel();
                        reward.ItemId = "gem";
                        reward.Count = receivedGemsWeekly;
                        rewards.ItemList.Add(reward);
                    }
                    
                    uiPopupReceivedReward.OpenWithValues(rewards);
                }

                // Refresh all
                uiViewOutgameMain.Refresh();

                if(!meetMinimumVersion)
                {
                    // The user needs to update the application!
                    UIManager.Instance.ShowAlert(
                        LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_below_min_version"),
                        () => {
                            PageOpenController.Instance.OpenMarketPage();
                            Application.Quit();
                        },
                        null,
                        true
                    );
                }
            },
            () =>{
                HideLoading();
            }
        );
    }

    public void RefreshArena()
    {
        ShowLoading();
        ServerNetworkManager.Instance.RefreshArena(
            (meetMinimumVersion, receivedGems, receivedGemsWeekly) => {
                HideLoading();
                
                uiViewOutgameMain.Refresh();
                uiPopupArena.Refresh();
                
                // Show received gems
                if(receivedGems != 0 || receivedGemsWeekly != 0)
                {
                    var rewards = new RewardListModel();
                    if(receivedGems != 0)
                    {    
                        var reward = new RewardModel();
                        reward.ItemId = "gem";
                        reward.Count = receivedGems;
                        rewards.ItemList.Add(reward);
                    }
                    if(receivedGemsWeekly != 0)
                    {
                        var reward = new RewardModel();
                        reward.ItemId = "gem";
                        reward.Count = receivedGemsWeekly;
                        rewards.ItemList.Add(reward);
                    }
                    
                    uiPopupReceivedReward.OpenWithValues(rewards);
                }

                // Refresh all
                uiViewOutgameMain.Refresh();

                if(!meetMinimumVersion)
                {
                    // The user needs to update the application!
                    UIManager.Instance.ShowAlert(
                        LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_below_min_version"),
                        () => {
                            PageOpenController.Instance.OpenMarketPage();
                            Application.Quit();
                        },
                        null,
                        true
                    );
                }
            },
            () =>{
                HideLoading();
            }
        );
    }

    public void ArenaStart()
    {
        ShowLoading();

        ServerNetworkManager.Instance.GetArenaOppponent(
            () => {
                ServerNetworkManager.Instance.ReportArenaStart(
                    ServerNetworkManager.Instance.Arena.OpponentId,
                    (userData, inventoryData, bonusStat) => {
                        InterSceneManager.Instance.SetDataForArena(ServerNetworkManager.Instance.Arena.OpponentName, userData, inventoryData, bonusStat);

                        // Start arena battle
                        StartCoroutine(ArenaStartAfter());
                    },
                    (reasonOfError) => {
                        HideLoading();
                        OnArenaStartFailure(reasonOfError);
                    }
                );
            },
            () => {
                HideLoading();
            }
        );
    }

    private void OnArenaStartFailure(string reason)
    {
        var keyTouse = "message_arena_start_fail_" + reason;
        if(reason == "unfinished_last_battle" || reason == "exceeded_daily_limit")
        {
            UIManager.Instance.ShowAlertLocalized(
                keyTouse, 
                () => {
                    RefreshArena();
                },
                null
            );
        }
        else
        {
            UIManager.Instance.ShowAlertLocalized(keyTouse, () => {}, null);
        }
    }
    
    private IEnumerator ArenaStartAfter()
    {
        uiViewFade.FadeOut();

        var isSyncComplete = false;
        SyncPlayerData(true, "ArenaStartAfter", () => {
            isSyncComplete = true;
        });

        yield return new WaitForSeconds(0.75f);

        while(!isSyncComplete)
            yield return -1;

        SceneChangeManager.Instance.LoadArenaGame();
    }

    public void ArenaEnd(bool didWin, int season, int seasonWeekly, bool isSuspiciousIfWin, Action onSuccess, Action onFailure)
    {
        ShowLoading();

        InterSceneManager.Instance.DidArenaBattle = false;
        InterSceneManager.Instance.DidArenaWin = false;

        ServerNetworkManager.Instance.ReportArenaEnd(
            didWin,
            season,
            seasonWeekly,
            isSuspiciousIfWin,
            (myNewScore) => {        
                // Leaderboard updates takes time, so we need to wait for 1 seconds
                // This is the limitation of Playfab
                StartCoroutine(CompleteArenaEnd());

                // Update achievement related with arena
                if(didWin)
                {
                    UpdateAchievement("achievement_08", 1);
                }
                else
                    UpdateAchievement("achievement_09", 1);

            onSuccess?.Invoke();
            },
            () => {
                HideLoading();

                // Inform failure
                UIManager.Instance.ShowAlertLocalized("message_etc", null, null);

                onFailure?.Invoke();
            }
        );
    }

    private IEnumerator CompleteArenaEnd()
    {
        yield return new WaitForSeconds(1f);

        HideLoading();
        
        // Show arena again
        uiViewOutgameMain.HideRightTopContent();
        ShowArenaPopup();
    }

#endregion

#region Logic For Server Raid

    public void EnterRaid(bool isAfterRaidEnd = false)
    {
        ShowLoading();

        BigInteger totalGoldRewardWeightForLog = 0;
        BigInteger totalHeartRewardWeightForLog = 0;
        int totalGemReawrdWeightForLog = 0;
        int totalRaidTicketRewardWeightForLog = 0;

        foreach (var tmp in ServerNetworkManager.Instance.Inventory.SelectedTrophyList)
        {
            if(tmp.Id.Contains("trophy_01_"))
            {
                int goldWeight = (int)(tmp.EffectPowerWithMultiply * 3);
                totalGoldRewardWeightForLog = totalGoldRewardWeightForLog + NumberMainController.Instance.FinalBestGoldIncome * goldWeight;
            }
            else if(tmp.Id.Contains("trophy_02_"))
            {
                totalGemReawrdWeightForLog = totalGemReawrdWeightForLog + (int)(tmp.EffectPowerWithAddition);
            }
            else if(tmp.Id.Contains("trophy_03_"))
            {
                totalHeartRewardWeightForLog = totalHeartRewardWeightForLog + (BigInteger)tmp.EffectPowerWithMultiply * (1 + ServerNetworkManager.Instance.Inventory.TotalHeartCost / 10000 / 24);
            }
            else if(tmp.Id.Contains("trophy_04_"))
            {
                totalRaidTicketRewardWeightForLog = totalRaidTicketRewardWeightForLog + (int)(tmp.EffectPowerWithAddition);
            }
        }

        ServerNetworkManager.Instance.RefreshRaid(totalGoldRewardWeightForLog, totalHeartRewardWeightForLog, totalGemReawrdWeightForLog, totalRaidTicketRewardWeightForLog,
            (meetMinimumVersion, passedDays, passedHours, currentTimestampDay, currentTimestampHour) => {
                HideLoading();

                // Show raid popup if possible
                if(ChatManager.Instance.IsConnected)
                {
                    if(!isAfterRaidEnd)
                        RaidManager.Instance.EnterRaidPrepareRoom(0);

                    uiViewOutgameMain.SetRaidTicketAmountInfoActive(true);
                    uiPopupRaid.Open();
                }
                else
                {
                    StartCoroutine(EnterRaidRetryCoroutine(isAfterRaidEnd));   
                }

                // Give reward
                if(passedDays != 0 || passedHours != 0)
                {
                    RewardListModel rewards = new RewardListModel();

                    foreach (var tmp in ServerNetworkManager.Instance.Inventory.SelectedTrophyList)
                    {
                        if(tmp.Id.Contains("trophy_01_"))
                        {
                            var rewardCount = (int)(tmp.EffectPowerWithMultiply * passedHours);

                            // We use exchange ratio of 3 here.
                            // This exchange ratio is just as '100 Gem to Gold conversion'.
                            int goldWeight = rewardCount * 3;
                            BigInteger totalGoldReward = NumberMainController.Instance.FinalBestGoldIncome * goldWeight;

                            ServerNetworkManager.Instance.Inventory.Gold += totalGoldReward;
                            if(rewards.ItemList.Exists(n => n.ItemId == "gold"))
                            {
                                rewards.ItemList.First(n => n.ItemId == "gold").Count += totalGoldReward;
                            }
                            else
                            {
                                var reward = new RewardModel();
                                reward.ItemId = "gold";
                                reward.Count += totalGoldReward;
                                rewards.ItemList.Add(reward);
                            }
                        }
                        else if(tmp.Id.Contains("trophy_02_"))
                        {
                            var rewardCount = (int)(tmp.EffectPowerWithAddition * passedDays);

                            ServerNetworkManager.Instance.UnsyncedGem += rewardCount;
                            ServerNetworkManager.Instance.User.UnsyncedGemChangeCount ++;

                            if(rewards.ItemList.Exists(n => n.ItemId == "gem"))
                            {
                                rewards.ItemList.First(n => n.ItemId == "gem").Count += rewardCount;
                            }
                            else
                            {
                                var reward = new RewardModel();
                                reward.ItemId = "gem";
                                reward.Count += rewardCount;
                                rewards.ItemList.Add(reward);
                            }
                        }
                        else if(tmp.Id.Contains("trophy_03_"))
                        {
                            var rewardCount = (BigInteger)tmp.EffectPowerWithMultiply * passedHours * (1 + ServerNetworkManager.Instance.Inventory.TotalHeartCost / 10000 / 24);

                            ServerNetworkManager.Instance.Inventory.Heart += rewardCount;
                            if(rewards.ItemList.Exists(n => n.ItemId == "heart"))
                            {
                                rewards.ItemList.First(n => n.ItemId == "heart").Count += rewardCount;
                            }
                            else
                            {
                                var reward = new RewardModel();
                                reward.ItemId = "heart";
                                reward.Count += rewardCount;
                                rewards.ItemList.Add(reward);
                            }
                        }
                        else if(tmp.Id.Contains("trophy_04_"))
                        {
                            var rewardCount = (int)(tmp.EffectPowerWithAddition * passedDays);

                            ServerNetworkManager.Instance.Inventory.TicketRaid += rewardCount;
                            if(rewards.ItemList.Exists(n => n.ItemId == "ticket_raid"))
                            {
                                rewards.ItemList.First(n => n.ItemId == "ticket_raid").Count += rewardCount;
                            }
                            else
                            {
                                var reward = new RewardModel();
                                reward.ItemId = "ticket_raid";
                                reward.Count += rewardCount;
                                rewards.ItemList.Add(reward);
                            }
                        }
                    }

                    // Show reward result after sync
                    // Note: EnterRaid is called only when receiving raid rewards
                    SyncPlayerData(true, "EnterRaid", () => {
                        uiPopupReceivedReward.OpenWithValues(rewards);
                    });

                    // Refresh all
                    uiViewOutgameMain.Refresh();
                }

                if(!meetMinimumVersion)
                {
                    // The user needs to update the application!
                    UIManager.Instance.ShowAlert(
                        LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_below_min_version"),
                        () => {
                            PageOpenController.Instance.OpenMarketPage();
                            Application.Quit();
                        },
                        null,
                        true
                    );
                }
            },
            () =>{
                uiViewOutgameMain.ShowRightTopContent();
                HideLoading();
            }
        );
    }

    private IEnumerator EnterRaidRetryCoroutine(bool isAfterRaidEnd = false)
    {
        ShowLoading();

        // We need to re-connect photon chat
        ConnectPhotonChat();

        var passed = 0f;
        while(!ChatManager.Instance.IsConnected && passed < 5f)
        {
            passed += Time.deltaTime;
            yield return -1;
        }

        HideLoading();

        if(ChatManager.Instance.IsConnected)
        {
            if(!isAfterRaidEnd)
                RaidManager.Instance.EnterRaidPrepareRoom(0);

            uiViewOutgameMain.SetRaidTicketAmountInfoActive(true);
            uiPopupRaid.Open();
        }
        else
        {
            uiViewOutgameMain.ShowRightTopContent();
            UIManager.Instance.ShowAlertLocalized("message_raid_not_ready", null, null);
        }
    }

    public void LeaveRaid()
    {
        RaidManager.Instance.LeaveRaidPrepareRoom(0);
    }

    public void JoinRaid(string raidId)
    {
        if(ChatManager.Instance.IsConnected)
        {
            ShowLoading();
            RaidManager.Instance.JoinRaid(
                raidId,
                () => {
                    InterSceneManager.Instance.SetDataForRaid(false);
                    StartCoroutine(RaidStartAfter());
                },
                (reason) => {
                    HideLoading();
                    UIManager.Instance.ShowAlertLocalized("message_" + reason, null, null);
                }
            );
        }   
        else
            UIManager.Instance.ShowAlertLocalized("message_raid_not_ready", null, null);
    }

    public void CreateRaid(int floor)
    {
        ShowLoading();
        RaidManager.Instance.OpenRaid(
            floor,
            () => {
                InterSceneManager.Instance.SetDataForRaid(false);
                StartCoroutine(RaidStartAfter());
            },
            (reason) => {
                HideLoading();
                UIManager.Instance.ShowAlertLocalized("message_" + reason, null, null);
            }
        );
    }

    private IEnumerator RaidStartAfter()
    {
        uiViewFade.FadeOut();
        
        var isSyncComplete = false;
        SyncPlayerData(true, "RaidStartAfter", () => {
            isSyncComplete = true;
        });

        yield return new WaitForSeconds(0.75f);

        while(!isSyncComplete)
            yield return -1;

        SceneChangeManager.Instance.LoadRaidGame();
    }

    public void RaidEnd(bool didDodge, bool didWin, int floor, int myRaidRank, int totalPlayerCount, int skillUseCount, Action onSuccess, Action onFailure)
    {
        ShowLoading();

        InterSceneManager.Instance.DidRaidBattle = false;
        InterSceneManager.Instance.DidGuildRaidBattle = false;
        InterSceneManager.Instance.DidRaidWin = false;
        InterSceneManager.Instance.DidRaidDodge = false;
        InterSceneManager.Instance.DidRaidDodgeBecauseOfHost = false;
        InterSceneManager.Instance.MyRaidRank = 0;
        InterSceneManager.Instance.TotalRaidPlayMemberCount = 0;

        InterSceneManager.Instance.RaidSkillUseCount = 0;

        InterSceneManager.Instance.GuildRaidScore = 0;
        InterSceneManager.Instance.GuildRaidParticipants.Clear();
        InterSceneManager.Instance.GuildRaidParticipantScores.Clear();

        if(RaidManager.Instance.CurrentHostPlayerId == ServerNetworkManager.Instance.User.PlayFabId)
        {
            RaidManager.Instance.EndRaid(
                didDodge,
                didWin,
                skillUseCount,
                () => {
                    CompleteRaidEnd(didDodge, didWin, floor, myRaidRank, totalPlayerCount);

                    onSuccess?.Invoke();
                },
                (reason) => {
                                       
                    // For preventing cheating (network connection disable by player)
                    if(Application.internetReachability == NetworkReachability.NotReachable)
                        didDodge = true;

                    CompleteRaidEnd(didDodge, didWin, floor, myRaidRank, totalPlayerCount);

                    onFailure?.Invoke();
                }
            );
        }
        else
        {
            RaidManager.Instance.LeaveRaid(
                didDodge,
                didWin,
                skillUseCount,
                () => {
                    CompleteRaidEnd(didDodge, didWin, floor, myRaidRank, totalPlayerCount);

                    onSuccess?.Invoke();
                },
                (reason) => {

                    // For preventing cheating (network connection disable by player)
                    if(Application.internetReachability == NetworkReachability.NotReachable)
                        didDodge = true;
                    
                    CompleteRaidEnd(didDodge, didWin, floor, myRaidRank, totalPlayerCount);

                    onFailure?.Invoke();
                }
            );
        }
    }

    private void CompleteRaidEnd(bool didDodge, bool didWin, int floor, int myRaidRank, int totalPlayerCount)
    {
        HideLoading();

        if(!didDodge && didWin)
        {
            // Give raid reward
            var gemChance = ServerNetworkManager.Instance.Setting.RaidRewardChances[0];
            var heartChance = ServerNetworkManager.Instance.Setting.RaidRewardChances[1];
            var generatorChance = ServerNetworkManager.Instance.Setting.RaidRewardChances[2];
            var randomValue = UnityEngine.Random.Range(0, gemChance + heartChance + generatorChance);
            int rewardMutliplier = 1;
            bool isCashBack = false;

            // Check reward double and ticket cash back
            Debug.Log("totalPlayerCount : " + totalPlayerCount + "// My Rank : " + myRaidRank);

            if(myRaidRank != 0 && totalPlayerCount >= ServerNetworkManager.Instance.Setting.BigRoomSizeForRaid)
            {
                rewardMutliplier = 2;
                isCashBack = true;
            }

            // Need to use tiket when daily limit is all used
            if(ServerNetworkManager.Instance.Raid.DailyLimitLeft <= 0)
            {
                var ticketNeeded = 50 * (ServerNetworkManager.Instance.Raid.DailyLimitLeft * -1 + 1);
            
                // Check cash back
                if(isCashBack)
                    ticketNeeded = ticketNeeded * 3 / 10;

                ServerNetworkManager.Instance.Inventory.TicketRaid -= ticketNeeded;
            }
                
            if(randomValue < gemChance)
            {
                int gemReward = floor * ServerNetworkManager.Instance.Setting.RaidGemReward * rewardMutliplier;

                ServerNetworkManager.Instance.UnsyncedGem += gemReward;
                ServerNetworkManager.Instance.User.UnsyncedGemChangeCount ++;
                
                // Show received gems
                SyncPlayerData(true, "CompleteRaidEnd", () => {
                    uiPopupReceivedReward.OpenWithValues(
                        "gem", 
                        gemReward.ToString(),
                        () => {
                            // Show raid again
                            EnterRaid(true);
                        }
                    );
                });
            }
            else if(randomValue < gemChance + heartChance) 
            {
                BigInteger heartReward = floor * ServerNetworkManager.Instance.Setting.RaidHeartReward * rewardMutliplier * (1 + ServerNetworkManager.Instance.Inventory.TotalHeartCost / 10000);

                ServerNetworkManager.Instance.Inventory.Heart += heartReward;
                
                // Show received heart
                SyncPlayerData(true, "CompleteRaidEnd", () => {
                    uiPopupReceivedReward.OpenWithValues(
                        "heart", 
                        LanguageManager.Instance.NumberToString(heartReward),
                        () => {
                            // Show raid again
                            EnterRaid(true);
                        }
                    );
                });
            }
            else
            {
                // Chooose proper trophy to give
                var totalChance = 0;
                var listToConsiderToGive = new List<TrophyInfoModel>();
                foreach(var tmp in BalanceInfoManager.Instance.TrophyInfoList)
                {
                    if(floor >= int.Parse(tmp.FirstAppearFloor) && (floor <= int.Parse(tmp.LastAppearFloor) || int.Parse(tmp.LastAppearFloor) == -1))
                    {
                        totalChance += int.Parse(tmp.SummonPower);
                        listToConsiderToGive.Add(tmp);
                    }
                }
                var gachaValue = UnityEngine.Random.Range(1, totalChance + 1);
                int i = 0;
                for(; i < listToConsiderToGive.Count; i++)
                {
                    gachaValue -= int.Parse(listToConsiderToGive[i].SummonPower);
                    if(gachaValue <= 0)
                        break;
                }
                var trophyId = listToConsiderToGive[i].Id;

                // Add the trophy count
                TrophyModel itemToChangeCount = ServerNetworkManager.Instance.Inventory.TrophyList.FirstOrDefault(n => n.Id == trophyId);
                if(itemToChangeCount == null)
                {
                    itemToChangeCount = new TrophyModel();
                    itemToChangeCount.Id = trophyId;
                    itemToChangeCount.Count = 0;
                    itemToChangeCount.Level = 1;
                    itemToChangeCount.HeartLevelUpCount = 0;
                    itemToChangeCount.TrophyLevelUpCount = 0;
                    ServerNetworkManager.Instance.Inventory.TrophyList.Add(itemToChangeCount);
                }
                itemToChangeCount.Count = itemToChangeCount.Count + rewardMutliplier;

                // Show received trophy result
                SyncPlayerData(true, "CompleteRaidEnd", () => {
                    uiPopupReceivedReward.OpenWithValues(
                        trophyId, 
                        rewardMutliplier.ToString(),
                        () => {
                            // Show raid again
                            EnterRaid(true);
                        }
                    );
                });
            }

            uiViewOutgameMain.HideRightTopContent();

            // Refresh all
            uiViewOutgameMain.Refresh();
        }
        else
        {
            uiViewOutgameMain.HideRightTopContent();

            // Show raid again
            EnterRaid(true);
        }        
    }

#endregion

#region Logic For Server Guild Raid

    public void EnterGuildRaid(bool isAfterRaidEnd = false)
    {
        ShowLoading();

        ServerNetworkManager.Instance.RefreshRaidGuildRaid(
            (meetMinimumVersion, receivedGems, receivedGuildStones, receivedGuildExp, receivedGuildToken) => {
                HideLoading();

                // Show received gems
                if(receivedGems != 0)
                {
                    var rewards = new RewardListModel();
                    if(receivedGems != 0)
                    {    
                        var reward = new RewardModel();
                        reward.ItemId = "gem";
                        reward.Count = receivedGems;
                        rewards.ItemList.Add(reward);
                    }
                    if(receivedGuildExp != 0)
                    {
                        var reward = new RewardModel();
                        reward.ItemId = "guild_exp";
                        reward.Count = receivedGuildExp;
                        rewards.ItemList.Add(reward);
                    }
                    if(receivedGuildToken != 0)
                    {
                        var reward = new RewardModel();
                        reward.ItemId = "guild_token";
                        reward.Count = receivedGuildToken;
                        rewards.ItemList.Add(reward);
                    }
                    
                    uiViewOutgameMain.Refresh();
                    uiPopupReceivedReward.OpenWithValues(rewards);
                }

                // Show raid popup if possible
                if(ChatManager.Instance.IsConnected)
                {
                    if(!isAfterRaidEnd)
                    {
                        RaidManager.Instance.EnterRaidPrepareRoom(1);
                        if(ServerNetworkManager.Instance.MyGuildId != null && ServerNetworkManager.Instance.MyGuildId != "")
                			ChatManager.Instance.Subscribe("guild_" + ServerNetworkManager.Instance.GuildData.GuildManagementData.GroupId);
                    }

                    if(isAfterRaidEnd)
                        StartCoroutine(OpenGuildRaidAfter(1f));
                    else
                    {
                        uiViewOutgameMain.SetGuildTokenAmountInfoActive(true);
                        uiPopupGuild.OpenWithValues(2);
                    }
                }
                else
                {
                    StartCoroutine(EnterGuildRaidRetryCoroutine(isAfterRaidEnd));   
                }

                if(!meetMinimumVersion)
                {
                    // The user needs to update the application!
                    UIManager.Instance.ShowAlert(
                        LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_below_min_version"),
                        () => {
                            PageOpenController.Instance.OpenMarketPage();
                            Application.Quit();
                        },
                        null,
                        true
                    );
                }
            },
            () =>{
                uiViewOutgameMain.ShowRightTopContent();
                HideLoading();
            }
        );
    }

    private IEnumerator EnterGuildRaidRetryCoroutine(bool isAfterRaidEnd = false)
    {
        ShowLoading();

        // We need to re-connect photon chat
        ConnectPhotonChat();

        var passed = 0f;
        while(!ChatManager.Instance.IsConnected && passed < 5f)
        {
            passed += Time.deltaTime;
            yield return -1;
        }

        HideLoading();

        if(ChatManager.Instance.IsConnected)
        {
            if(!isAfterRaidEnd)
            {
                RaidManager.Instance.EnterRaidPrepareRoom(1);
                if(ServerNetworkManager.Instance.MyGuildId != null && ServerNetworkManager.Instance.MyGuildId != "")
                    ChatManager.Instance.Subscribe("guild_" + ServerNetworkManager.Instance.GuildData.GuildManagementData.GroupId);
            }

            if(isAfterRaidEnd)
                StartCoroutine(OpenGuildRaidAfter(1f));
            else
            {
                uiViewOutgameMain.SetGuildTokenAmountInfoActive(true);
                uiPopupGuild.OpenWithValues(2);
            }
        }
        else
        {
            uiViewOutgameMain.ShowRightTopContent();
            UIManager.Instance.ShowAlertLocalized("message_raid_not_ready", null, null);
        }
    }

    private IEnumerator OpenGuildRaidAfter(float duration)
    {
        ShowLoading();
        yield return new WaitForSeconds(duration);

        HideLoading();
        uiViewOutgameMain.SetGuildTokenAmountInfoActive(true);
        uiPopupGuild.OpenWithValues(2);
    }

    public void LeaveGuildRaid()
    {
        RaidManager.Instance.LeaveRaidPrepareRoom(1);
    }

    public void JoinGuildRaid(string raidId)
    {
        var timeLeft = Mathf.Max(2f + TimeGuildRaidFunction - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
        {
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
            return;
        }

        if(ChatManager.Instance.IsConnected)
        {
            ShowLoading();
            RaidManager.Instance.JoinRaid(
                raidId,
                () => {

                    TimeGuildRaidFunction = (int)Time.realtimeSinceStartup;

                    InterSceneManager.Instance.SetDataForGuildRaid(false);
                    StartCoroutine(GuildRaidStartAfter());
                },
                (reason) => {
                    HideLoading();

                    if(reason == "unknown")
                        UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
                    else if(reason == "guild_join_time_not_enough")
                    {
                        var text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_" + reason), ServerNetworkManager.Instance.Setting.GuilFunctionCoolTime / (60 * 60));
                        UIManager.Instance.ShowAlert(text, null, null);
                    }
                    else
                        UIManager.Instance.ShowAlertLocalized("message_" + reason, null, null);
                }
            );
        }   
        else
            UIManager.Instance.ShowAlertLocalized("message_raid_not_ready", null, null);
    }

    public void CreateGuildRaid(int floor)
    {
        var timeLeft = Mathf.Max(2f + TimeGuildRaidFunction - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
        {
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
            return;
        }

        ShowLoading();
        RaidManager.Instance.OpenRaid(
            floor,
            () => {

                TimeGuildRaidFunction = (int)Time.realtimeSinceStartup;

                InterSceneManager.Instance.SetDataForGuildRaid(false);
                StartCoroutine(GuildRaidStartAfter());
            },
            (reason) => {
                HideLoading();

                if(reason == "unknown")
                    UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
                else if(reason == "guild_join_time_not_enough")
                {
                    var text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_" + reason), ServerNetworkManager.Instance.Setting.GuilFunctionCoolTime / (60 * 60));
                    UIManager.Instance.ShowAlert(text, null, null);
                }
                else
                    UIManager.Instance.ShowAlertLocalized("message_" + reason, null, null);
            }
        );
    }

    private IEnumerator GuildRaidStartAfter()
    {
        uiViewFade.FadeOut();
        
        var isSyncComplete = false;
        SyncPlayerData(true, "GuildRaidStartAfter", () => {
            isSyncComplete = true;
        });

        yield return new WaitForSeconds(0.75f);

        while(!isSyncComplete)
            yield return -1;

        SceneChangeManager.Instance.LoadGuildRaidGame();
    }

    public void GuildRaidEnd(bool didRaidDodge, int score, List<string> participantList, List<int> participantScoreList, int season, int skillUseCount, Action onSuccess, Action onFailure)
    {
        ShowLoading();

        InterSceneManager.Instance.DidRaidBattle = false;
        InterSceneManager.Instance.DidGuildRaidBattle = false;
        InterSceneManager.Instance.DidRaidWin = false;
        InterSceneManager.Instance.DidRaidDodge = false;
        InterSceneManager.Instance.DidRaidDodgeBecauseOfHost = false;
        InterSceneManager.Instance.MyRaidRank = 0;
        InterSceneManager.Instance.TotalRaidPlayMemberCount = 0;

        InterSceneManager.Instance.RaidSkillUseCount = 0;

        InterSceneManager.Instance.GuildRaidScore = 0;
        InterSceneManager.Instance.GuildRaidParticipants.Clear();
        InterSceneManager.Instance.GuildRaidParticipantScores.Clear();

        if(RaidManager.Instance.CurrentHostPlayerId == ServerNetworkManager.Instance.User.PlayFabId)
        {
            RaidManager.Instance.EndRaid(
                didRaidDodge,
                score,
                participantList,
                participantScoreList,
                season,
                skillUseCount,
                () => {
                    GetGuildData(
                        true,
                        () => {
                            EnterGuildRaid(true);
                            uiViewOutgameMain.HideRightTopContent();
                            uiViewOutgameMain.Refresh();
                            
                            onSuccess?.Invoke();
                        },
                        null
                    );
                },
                (reason) => {
                    GetGuildData(
                        true,
				        () => {
                            EnterGuildRaid(true);
                            uiViewOutgameMain.HideRightTopContent();
                            uiViewOutgameMain.Refresh();

                            onFailure?.Invoke();
                        },
                        null
                    );
                }
            );
        }
        else
        {
            RaidManager.Instance.LeaveRaid(
                didRaidDodge,
                season,
                participantScoreList[0],
                skillUseCount,
                () => {
                    GetGuildData(
                        true,
                        () => {
                            EnterGuildRaid(true);
                            uiViewOutgameMain.HideRightTopContent();
                            uiViewOutgameMain.Refresh();

                            onSuccess?.Invoke();
                        },
                        null
                    );
                },
                (reason) => {
                    GetGuildData(
                        true,
				        () => {
                            EnterGuildRaid(true);
                            uiViewOutgameMain.HideRightTopContent();
                            uiViewOutgameMain.Refresh();
                            
                            onFailure?.Invoke();
                        },
                        null
                    );
                }
            );
        }
    }

#endregion

#region Logic For Server Chatting

    private void ConnectPhotonChat()
    {   
        if(ChatManager.Instance.IsConnected)
            return;
        
        ServerNetworkManager.Instance.GetPhotonAuthToken(
            () => {
                ChatManager.Instance.Connect();
            },
            null
        );
    }

    public void ChatOn()
    {
        if(!uiPopupChat.gameObject.activeSelf)
        {
            uiPopupChat.Open();
            uiViewOutgameMain.SetChatButtonActive(true);
        }
    }

    public void ChatOnOff()
    {
        uiViewOutgameMain.SetChatButtonActive(false);

        if(uiPopupChat.gameObject.activeSelf)
        {
            uiPopupChat.Close();
            ChatManager.Instance.Disconnect();
        }
        else
        {
            if(ChatManager.Instance.IsConnected)
            {
                uiPopupChat.Open();
                uiViewOutgameMain.SetChatButtonActive(true);
            }
            else
            {
                if(!ChatManager.Instance.IsConnected)
                    ConnectPhotonChat();
                else
                    ChatOn();
            }
        }
    }

    public void SendSummonResultMessage(string itemId)
    {
        ChatManager.Instance.SendChatSummonMessageToMain(itemId);
    }

#endregion

#region Logic For Server Ranking

    public void CheckRanking()
    {
        ServerNetworkManager.Instance.GetStageRankingAdvanced(
            () =>{
                // Refresh the view
                uiViewOutgameMain.RefreshTopbar();
            },
            null
        );
    }

    public void InitForRanking()
    {
        // Open arena popup
        uiPopupRanking.OpenWithValues(0);
    }

    public void RefreshRanking(int tabId, Action onSuccess, Action onFailure)
    {
        ShowLoading();
        
        if(tabId == 0)
        {
            ServerNetworkManager.Instance.GetStageRankingAdvanced(
            () => {
                HideLoading();
                onSuccess?.Invoke();
            },
            () =>{
                HideLoading();
                onFailure?.Invoke();
            });
        }
        else if(tabId == 1)
        {
            ServerNetworkManager.Instance.GetArenaRanking(
            () => {
                 ServerNetworkManager.Instance.GetArenaMyRanking(
                    () => {
                        HideLoading();
                        onSuccess?.Invoke();
                    },
                    () => {
                        HideLoading();
                        onFailure?.Invoke();
                    }
                );
            },
            () =>{
                HideLoading();
                onFailure?.Invoke();
            });
        }
        else if(tabId == 2)
        {
            ServerNetworkManager.Instance.GetTotalArenaRanking(
            () => {
                ServerNetworkManager.Instance.GetTotalArenaMyRanking(
                    () => {
                        HideLoading();
                        onSuccess?.Invoke();
                    },
                    () => {
                        HideLoading();
                        onFailure?.Invoke();
                    }
                );
            },
            () =>{
                HideLoading();
                onFailure?.Invoke();
            });
        }
    }

    public void GetGuildRankingList(Action onSuccess)
    {
        ShowLoading();

        ServerNetworkManager.Instance.GetGuildRankingLeaderboardList(
            () => {
                HideLoading();
                onSuccess.Invoke();
            },
            () => {
                HideLoading();
                
                // Need to clear the cache to prevent issues
                ServerNetworkManager.Instance.GuildRankList.Clear();
                UIManager.Instance.ShowAlertLocalized("message_get_guild_ranking_failed", null, null);
            }
        );
    }

#endregion

#region Logic For Server Moderator
    
    public void ReportChatViolenceUser(string id, Action onSucces)
    {
        // Check moderator
        if(ServerNetworkManager.Instance.IsModerator)
        {
            ShowLoading();

            // Get target id's chat log
            string chatLog = ChatManager.Instance.GetPlayerRecentChatLog(id); 

            ServerNetworkManager.Instance.RequestChatBan(id, chatLog, 
                () => {
                    HideLoading();
                    onSucces?.Invoke();
                },
                (error) => {
                    HideLoading();

                    if(error == "ban_duplicate")
                        UIManager.Instance.ShowAlertLocalized("message_ban_duplicate", null, null);
                    else
                        UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
                }
            );
        }
    }

    public void RequestReportCancel(string id, Action onSucces)
    {
        // Check moderator
        if(ServerNetworkManager.Instance.IsModerator)
        {
            ShowLoading();

            ServerNetworkManager.Instance.RequestChatBanCancel(id, 
                () => {
                    HideLoading();
                    onSucces?.Invoke();
                },
                (error) => {
                    HideLoading();
                    
                    if(error == "ban_not_exist")
                        UIManager.Instance.ShowAlertLocalized("message_ban_not_exist", null, null);
                    else
                        UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
                }
            );
        }
    }

    public void ShowChatLog(string id)
    {
        // Check moderator
        if(ServerNetworkManager.Instance.IsModerator)
        {
            ShowLoading();

            ServerNetworkManager.Instance.RequestChatLog(id, 
                (chatLog) => {
                    HideLoading();
                    ShowGenericInfo(chatLog);
                },
                () => {
                    HideLoading();
                    UIManager.Instance.ShowAlertLocalized("message_etc", () => {}, null);
                }
            );
        }
    }

#endregion

#region Logic For Server Chat Block
  
    public void AddChatBlockList(string id, string nickname, Action onSucces)
    {
        var timeLeft = Mathf.Max(2f + TimeChatFunction - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
        {
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
            return;
        }

        // Check block list limit
        var chatBlockLimitCount = ServerNetworkManager.Instance.Setting.ChatBlockLimitCount;
        var textOverLimit = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_ban_over_limit"), chatBlockLimitCount);

        if(ServerNetworkManager.Instance.ChatBlockInfo.ChatBlockList.Count >= chatBlockLimitCount)
        {
            UIManager.Instance.ShowAlert(textOverLimit, null, null);
            return;
        }

        ShowLoading();

        ServerNetworkManager.Instance.AddChatBlock(id, nickname, 
            () => {
                TimeChatFunction = (int)Time.realtimeSinceStartup;

                HideLoading();
                onSucces?.Invoke();
            },
            (error) => {
                HideLoading();

                if(error == "ban_duplicate")
                    UIManager.Instance.ShowAlertLocalized("message_ban_duplicate", null, null);
                else if(error == "ban_over_limit")
                    UIManager.Instance.ShowAlert(textOverLimit, null, null);
                else
                    UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
            }
        );
    }

    public void RemoveChatBlockList(string id, Action onSucces)
    {
        var timeLeft = Mathf.Max(2f + TimeChatFunction - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
        {
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
            return;
        }

        // Check target user is in current chat block list or not
        if(ServerNetworkManager.Instance.ChatBlockInfo.ChatBlockList.FirstOrDefault(n => n.Id == id) == null)
            return;

        ShowLoading();

        ServerNetworkManager.Instance.RemoveChatBlock(id, 
            () => {
                TimeChatFunction = (int)Time.realtimeSinceStartup;

                HideLoading();
                onSucces?.Invoke();
            },
            () => {
                HideLoading();
                UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
            }
        );
    }
#endregion

#region Logic For Server Tower

    public void RefreshTower()
    {
        ShowLoading();
        ServerNetworkManager.Instance.RefreshTower(
            (meetMinimumVersion) => {
                TimeTowerFunction = (int)Time.realtimeSinceStartup;
                
                if(uiPopupTower.isActiveAndEnabled)
                    uiPopupTower.Refresh();
                
                RefreshTowerFloors(
                    () => {
                        HideLoading();
                    }
                );

                if(!meetMinimumVersion)
                {
                    // The user needs to update the application!
                    UIManager.Instance.ShowAlert(
                        LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_below_min_version"),
                        () => {
                            PageOpenController.Instance.OpenMarketPage();
                            Application.Quit();
                        },
                        null,
                        true
                    );
                }
            },
            () => {
                UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
            }
        );
    }

    public void RefreshTowerBackground()
    {
        ServerNetworkManager.Instance.RefreshTower(
            (meetMinimumVersion) => {
                if(uiViewOutgameMain.isActiveAndEnabled)
                    uiViewOutgameMain.RefreshMenuGroup();

                if(!meetMinimumVersion)
                {
                    // The user needs to update the application!
                    UIManager.Instance.ShowAlert(
                        LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_below_min_version"),
                        () => {
                            PageOpenController.Instance.OpenMarketPage();
                            Application.Quit();
                        },
                        null,
                        true
                    );
                }
            }, 
            null
        );
    }

    public void RefreshTowerFloors(Action onSuccess)
    {
        ServerNetworkManager.Instance.RefreshTowerFloors(
            () => {
                if(uiPopupTower.isActiveAndEnabled)
                    uiPopupTower.RefreshFloors();
                onSuccess?.Invoke();
            },
            null
        );
    }

    public void DeployTower()
    {
        ShowLoading();
        ServerNetworkManager.Instance.DeployTower(
            () => {
                TimeTowerFunction = (int)Time.realtimeSinceStartup;
                
                if(uiPopupTower.isActiveAndEnabled)
                    uiPopupTower.Refresh();
               
                RefreshTowerFloors(
                    () => {
                        HideLoading();
                    }
                );
            },
            () => {
                HideLoading();
                UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
            }
        );
    }

    public void RetreatTower()
    {
        ShowLoading();
        ServerNetworkManager.Instance.RetreatTower(
            (rewardGem, rewardFeather) => {
                TimeTowerFunction = (int)Time.realtimeSinceStartup;

                if(rewardGem > 0)
                {
                    if(rewardFeather > 0)
                        this.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_thank_you_with_rewards_and_feather"), rewardGem, rewardFeather));
                    else
                        this.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_thank_you_with_rewards"), rewardGem));
                }
                    
                uiViewOutgameMain.RefreshTopbar();

                if(uiPopupTower.isActiveAndEnabled)
                    uiPopupTower.Refresh();

                RefreshTowerFloors(
                    () => {
                        HideLoading();
                    }
                );
            },
            () => {
                HideLoading();
                UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
            }
        );
    }

    public void GetTowerOpponentAndTryBattle()
    {
        int previousFloor = ServerNetworkManager.Instance.MyTowerData.Floor;
        
        ShowLoading();
        ServerNetworkManager.Instance.RefreshTower(
            (meetMinimumVersion) => {
                
                if(uiPopupTower.isActiveAndEnabled)
                    uiPopupTower.Refresh();

                // Don't progress when already finished
                // Also check for the case when the player was attacked and position change happened
                if(ServerNetworkManager.Instance.MyTowerData.IsCompleted || !ServerNetworkManager.Instance.MyTowerData.IsDeployed || previousFloor != ServerNetworkManager.Instance.MyTowerData.Floor)
                {
                    RefreshTowerFloors(
                    () => {
                            HideLoading();
                        }
                    );
                    return;
                }

                ServerNetworkManager.Instance.RefreshTowerFloors(
                    () => {
                        TimeTowerFunction = (int)Time.realtimeSinceStartup;

                        HideLoading();

                        if(uiPopupTower.isActiveAndEnabled)
                            uiPopupTower.RefreshFloors();

                        var targetFloor = ServerNetworkManager.Instance.MyTowerData.Floor + 1;

                        if(ServerNetworkManager.Instance.TowerFloorPlayers[targetFloor - 1].CurrentPlayers.Count() < ServerNetworkManager.Instance.TowerFloorPlayers[targetFloor - 1].MaxAllowedPlayersNumber)
                            AdvanceTowerWithNoBattle();
                        else
                        {
                            TowerBattleOpponentModel opponentModel = null;
                            foreach(var tmp in new List<string>(ServerNetworkManager.Instance.TowerFloorPlayers[targetFloor - 1].CurrentPlayers.Keys).Shuffle())
                            {
                                if(opponentModel == null)
                                {
                                    opponentModel = new TowerBattleOpponentModel();

                                    opponentModel.NickName = ServerNetworkManager.Instance.TowerFloorPlayers[targetFloor - 1].CurrentPlayers[tmp];
                                    opponentModel.PlayFabId = tmp;
                                    opponentModel.Floor = targetFloor;
                                }
                            }
                            TowerBattleStart(opponentModel);
                        }
                    },
                    () => {
                        HideLoading();
                        UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
                    }
                );

                if(!meetMinimumVersion)
                {
                    // The user needs to update the application!
                    UIManager.Instance.ShowAlert(
                        LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_below_min_version"),
                        () => {
                            PageOpenController.Instance.OpenMarketPage();
                            Application.Quit();
                        },
                        null,
                        true
                    );
                }
            },
            () => {
                HideLoading();
                UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
            }
        );
    }

    public void AdvanceTowerWithNoBattle()
    {
        ShowLoading();
        ServerNetworkManager.Instance.AdvanceTowerWithNoBattle(
            () => {
                HideLoading();
                UIManager.Instance.ShowAlertLocalized(
                    "message_tower_empty",
                    () => {
                        TimeTowerFunction = (int)Time.realtimeSinceStartup;

                        ShowLoading();

                        if(uiPopupTower.isActiveAndEnabled)
                            uiPopupTower.Refresh();
                        
                        RefreshTowerFloors(
                            () => {
                                HideLoading();
                            }
                        );
                    },
                    null
                );
            },
            () => {
                HideLoading();
                UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
            }
        );
    }

    public void TowerBattleStart(TowerBattleOpponentModel opponentModel)
    {
        if(opponentModel != null)
        {
            ShowLoading();

            ServerNetworkManager.Instance.ReportTowerBattleStart(
                opponentModel.PlayFabId,
                (canSkip, opponentUserData, opponentInventoryData, opponentBonusStat) => {
                    
                    TimeTowerFunction = (int)Time.realtimeSinceStartup;

                    HideLoading();

                    if(!canSkip)
                    {
                        // Prepare battle data that will be used by ingame
                        InterSceneManager.Instance.SetDataForTower(
                            opponentModel.NickName,
                            opponentUserData,
                            opponentInventoryData,
                            opponentBonusStat
                        );

                        // Start tower battle
                        StartCoroutine(TowerBattleStartAfter());
                    }
                    else
                    {
                        // No need to fight here
                        AdvanceTowerWithNoBattle();
                    }
                },
                (reasonOfError) => {
                    HideLoading();
                    OnTowerBattleStartFailure(reasonOfError);
                }
            );
        }
    }
    
    private IEnumerator TowerBattleStartAfter()
    {
        uiViewFade.FadeOut();
        yield return new WaitForSeconds(0.75f);
        SceneChangeManager.Instance.LoadTowerGame();
    }

    private void OnTowerBattleStartFailure(string reason)
    {
        var keyTouse = "message_tower_start_fail_" + reason;

        if(reason == "left_cooldown")
            UIManager.Instance.ShowAlertLocalized(keyTouse, null, null);
        else
            UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
    }

    public void TowerBattleEnd(bool didWin, bool isSuspiciousIfWin)
    {
        ShowLoading();

        InterSceneManager.Instance.DidTowerBattle = false;
        InterSceneManager.Instance.DidTowerWin = false;

        ServerNetworkManager.Instance.ReportTowerBattleEnd(
            didWin,
            isSuspiciousIfWin,
            () => {        
                // This is the limitation of Playfab
                StartCoroutine(CompleteTowerEnd());
            },
            () => {
                HideLoading();

                // Inform failure
                UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
            }
        );
    }

    private IEnumerator CompleteTowerEnd()
    {
        yield return new WaitForSeconds(1f);

        HideLoading();
        
        // Show tower again
        uiViewOutgameMain.HideRightTopContent();
        ShowTowerPopup();
    }

#endregion

#region Logic For Server Wing
    public void EquipWing(string wingId, Action onSucces)
    {
        if(ServerNetworkManager.Instance.PurchasedWingList.FirstOrDefault(n => n.WingId == wingId) != null)
        {
            var targetWing = ServerNetworkManager.Instance.PurchasedWingList.FirstOrDefault(n => n.WingId == wingId);
            
            if(targetWing != null)
            {
                foreach(var wing in ServerNetworkManager.Instance.PurchasedWingList)
                {
                    if(wing.WingId == targetWing.WingId)
                        wing.IsEquipment = true;
                    else
                        wing.IsEquipment = false;
                }
                
                var convertendWingIndex = int.Parse(wingId.Substring(wingId.Length - 2)) - 1;

                // Get winig body info
                var tmpWingBody = ServerNetworkManager.Instance.WingBodyInfo;
                var wingBodyEquipmentStatBonus = 0;

                if(tmpWingBody != null)
                    wingBodyEquipmentStatBonus = tmpWingBody.EquipmentStatBonusValue;

                ServerNetworkManager.Instance.Inventory.WingIndex = convertendWingIndex;
                ServerNetworkManager.Instance.User.SkillInfo[0].BonusLevel = targetWing.EquipmentStatBonus1Value + wingBodyEquipmentStatBonus;
                ServerNetworkManager.Instance.User.SkillInfo[1].BonusLevel = targetWing.EquipmentStatBonus2Value + wingBodyEquipmentStatBonus;
                ServerNetworkManager.Instance.User.SkillInfo[2].BonusLevel = targetWing.EquipmentStatBonus3Value + wingBodyEquipmentStatBonus;

                // Need to recalculate numbers
                NumberMainController.Instance.Refresh();

                // Refresh hero visual
                IngameMainController.Instance.RefreshHeroVisual(true);

                onSucces?.Invoke();
            }
        }
    }

    public void PurchaseWing(string wingId, Action onSucces)
    {
        var tmpWingProductModel = ServerNetworkManager.Instance.Catalog.WingProductList.FirstOrDefault(n => n.Id == wingId);

        if(tmpWingProductModel != null)
        {
            if(tmpWingProductModel.PriceType == GoodsPriceType.PACKAGE)
            {
                this.ShowShopPopup(0);
            }
            else
            {
                ShowLoading();

                string priceType = "GE";

                if(tmpWingProductModel.PriceType == GoodsPriceType.GEM)
                    priceType = "GE";
                else if(tmpWingProductModel.PriceType == GoodsPriceType.FEATHER)
                    priceType = "FE";

                ServerNetworkManager.Instance.PurchaseWing(wingId, priceType, tmpWingProductModel.Price,
                    () => {
                        HideLoading();

                        // Get wing body info
                        var tmpWingBody = ServerNetworkManager.Instance.WingBodyInfo;
                        int wingBodyOwnedStatBonus = 1;

                        if(tmpWingBody != null)
                            wingBodyOwnedStatBonus = tmpWingBody.OwnedStatBonusValue;

                        // Caculate total wing owned bonus
                        int totalWingAttackBonus = 0;
                        int totalWingGoldBonus = 0;

                        foreach(var wing in ServerNetworkManager.Instance.PurchasedWingList)
                        {
                            totalWingAttackBonus = totalWingAttackBonus + wing.OwnedStatBonus1Value;
                            totalWingGoldBonus = totalWingGoldBonus + wing.OwnedStatBonus2Value;
                        }

                        ServerNetworkManager.Instance.User.TotalWingAttackBonus = totalWingAttackBonus * wingBodyOwnedStatBonus;
                        ServerNetworkManager.Instance.User.TotalWingGoldBonus = totalWingGoldBonus * wingBodyOwnedStatBonus;

                        // Need to recalculate numbers
                        NumberMainController.Instance.Refresh();

                        uiViewOutgameMain.RefreshTopbar();
                        onSucces?.Invoke();
                    },
                    () => {
                        HideLoading();
                        UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
                    }
                );
            }
        }
    }

    public void ResetWingStat(string wingId, List<bool> statLockList, Action onSucces)
    {
        var tmpWingModel = ServerNetworkManager.Instance.PurchasedWingList.FirstOrDefault(n => n.WingId == wingId);
        
        if(tmpWingModel != null)
        {
            int lockStatCount = statLockList.FindAll(n => n == true).Count;
            
            bool isLockOwnedStat1 = statLockList[(int)WingStatInfoType.ATTACK];
            bool isLockOwnedStat2 = statLockList[(int)WingStatInfoType.GOLD];
            bool isLockEquipmentStat1 = statLockList[(int)WingStatInfoType.SKILL_1];
            bool isLockEquipmentStat2 = statLockList[(int)WingStatInfoType.SKILL_2];
            bool isLockEquipmentStat3 = statLockList[(int)WingStatInfoType.SKILL_3];

            ShowLoading();
            ServerNetworkManager.Instance.ResetWingStat(wingId, lockStatCount, isLockOwnedStat1, isLockOwnedStat2, isLockEquipmentStat1, isLockEquipmentStat2, isLockEquipmentStat3,
                () => {
                    HideLoading();

                    // Get wing body info
                    var tmpWingBody = ServerNetworkManager.Instance.WingBodyInfo;
                    int wingBodyOwnedStatBonus = 1;
                    int wingBodyEquipmentStatBonus = 0;

                    if(tmpWingBody != null)
                    {
                        wingBodyOwnedStatBonus = tmpWingBody.OwnedStatBonusValue;
                        wingBodyEquipmentStatBonus = tmpWingBody.EquipmentStatBonusValue;
                    }

                    // Caculate total wing owned bonus
                    int totalWingAttackBonus = 0;
                    int totalWingGoldBonus = 0;

                    foreach(var wing in ServerNetworkManager.Instance.PurchasedWingList)
                    {
                        totalWingAttackBonus = totalWingAttackBonus + wing.OwnedStatBonus1Value;
                        totalWingGoldBonus = totalWingGoldBonus + wing.OwnedStatBonus2Value;
                    }

                    ServerNetworkManager.Instance.User.TotalWingAttackBonus = totalWingAttackBonus * wingBodyOwnedStatBonus;
                    ServerNetworkManager.Instance.User.TotalWingGoldBonus = totalWingGoldBonus * wingBodyOwnedStatBonus;

                    // Get updated target wing model
                    tmpWingModel = ServerNetworkManager.Instance.PurchasedWingList.FirstOrDefault(n => n.WingId == wingId);

                    // Update wing equipment stat bonus
                    if(tmpWingModel.IsEquipment)
                    {
                        ServerNetworkManager.Instance.User.SkillInfo[0].BonusLevel = tmpWingModel.EquipmentStatBonus1Value + wingBodyEquipmentStatBonus;
                        ServerNetworkManager.Instance.User.SkillInfo[1].BonusLevel = tmpWingModel.EquipmentStatBonus2Value + wingBodyEquipmentStatBonus;
                        ServerNetworkManager.Instance.User.SkillInfo[2].BonusLevel = tmpWingModel.EquipmentStatBonus3Value + wingBodyEquipmentStatBonus;
                    }

                    // Need to recalculate numbers
                    NumberMainController.Instance.Refresh();

                    uiViewOutgameMain.RefreshTopbar();
                    onSucces?.Invoke();
                },
                () => {
                    HideLoading();
                    UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
                }
            );
        }
    }

    public void OpenWingBody(Action onSuccess)
    {
        // Check purchased wing count
        if(ServerNetworkManager.Instance.PurchasedWingList.Count < ServerNetworkManager.Instance.Setting.WingBodyOpenCount)
        {
            Debug.Log("Purchased wing's count is not enough!");
            return;
        }

        // Check feater
        if(ServerNetworkManager.Instance.Inventory.Feather < ServerNetworkManager.Instance.Setting.WingUpgradePrice)
        {
            UIManager.Instance.ShowAlertLocalized("message_not_enough_feather", null, null);
            return;
        }

        ShowLoading();
        ServerNetworkManager.Instance.OpenWingBody(
            () => {
                HideLoading();

                var tmpWingBody = ServerNetworkManager.Instance.WingBodyInfo;

                // Caculate total wing owned bonus
                int totalWingAttackBonus = 0;
                int totalWingGoldBonus = 0;

                foreach(var wing in ServerNetworkManager.Instance.PurchasedWingList)
                {
                    totalWingAttackBonus = totalWingAttackBonus + wing.OwnedStatBonus1Value;
                    totalWingGoldBonus = totalWingGoldBonus + wing.OwnedStatBonus2Value;
                }

                ServerNetworkManager.Instance.User.TotalWingAttackBonus = totalWingAttackBonus * tmpWingBody.OwnedStatBonusValue;
                ServerNetworkManager.Instance.User.TotalWingGoldBonus = totalWingGoldBonus * tmpWingBody.OwnedStatBonusValue;

                // Get updated target wing model
                var tmpWingModel = ServerNetworkManager.Instance.PurchasedWingList.FirstOrDefault(n => n.IsEquipment);

                // Update wing equipment stat bonus
                if(tmpWingModel != null)
                {
                    ServerNetworkManager.Instance.User.SkillInfo[0].BonusLevel = (tmpWingModel.EquipmentStatBonus1Value + tmpWingBody.EquipmentStatBonusValue);
                    ServerNetworkManager.Instance.User.SkillInfo[1].BonusLevel = (tmpWingModel.EquipmentStatBonus2Value + tmpWingBody.EquipmentStatBonusValue);
                    ServerNetworkManager.Instance.User.SkillInfo[2].BonusLevel = (tmpWingModel.EquipmentStatBonus3Value + tmpWingBody.EquipmentStatBonusValue);
                }

                // Need to recalculate numbers
                NumberMainController.Instance.Refresh();

                uiViewOutgameMain.RefreshTopbar();
                onSuccess?.Invoke();
            },
            () => {
                HideLoading();
                UIManager.Instance.ShowAlertLocalized("message_wing_body_open_fail", null, null);
            }
        );
    }

    public void UpgradeWingBody(Action<bool> onSuccess)
    {
        var tmpWingBody = ServerNetworkManager.Instance.WingBodyInfo;
        
        if(tmpWingBody != null)
        {
            // Check max level
            if(tmpWingBody.Level >= ServerNetworkManager.Instance.Setting.WingMaxLevel)
            {
                Debug.Log("Wing body is max level!");
                return;
            }

            // Check feater
            if(ServerNetworkManager.Instance.Inventory.Feather < ServerNetworkManager.Instance.Setting.WingUpgradePrice)
            {
                UIManager.Instance.ShowAlertLocalized("message_not_enough_feather", null, null);
                return;
            }

            ShowLoading();
            ServerNetworkManager.Instance.UpgradeWingBody(
                (isUpgradeSuccess) => {
                    HideLoading();

                    // Caculate total wing owned bonus
                    int totalWingAttackBonus = 0;
                    int totalWingGoldBonus = 0;

                    foreach(var wing in ServerNetworkManager.Instance.PurchasedWingList)
                    {
                        totalWingAttackBonus = totalWingAttackBonus + wing.OwnedStatBonus1Value;
                        totalWingGoldBonus = totalWingGoldBonus + wing.OwnedStatBonus2Value;
                    }

                    ServerNetworkManager.Instance.User.TotalWingAttackBonus = totalWingAttackBonus * tmpWingBody.OwnedStatBonusValue;
                    ServerNetworkManager.Instance.User.TotalWingGoldBonus = totalWingGoldBonus * tmpWingBody.OwnedStatBonusValue;

                    // Get updated target wing model
                    var tmpWingModel = ServerNetworkManager.Instance.PurchasedWingList.FirstOrDefault(n => n.IsEquipment);

                    // Update wing equipment stat bonus
                    if(tmpWingModel != null)
                    {
                        ServerNetworkManager.Instance.User.SkillInfo[0].BonusLevel = (tmpWingModel.EquipmentStatBonus1Value + tmpWingBody.EquipmentStatBonusValue);
                        ServerNetworkManager.Instance.User.SkillInfo[1].BonusLevel = (tmpWingModel.EquipmentStatBonus2Value + tmpWingBody.EquipmentStatBonusValue);
                        ServerNetworkManager.Instance.User.SkillInfo[2].BonusLevel = (tmpWingModel.EquipmentStatBonus3Value + tmpWingBody.EquipmentStatBonusValue);
                    }

                    // Need to recalculate numbers
                    NumberMainController.Instance.Refresh();

                    uiViewOutgameMain.RefreshTopbar();
                    onSuccess?.Invoke(isUpgradeSuccess);
                },
                () => {
                    HideLoading();
                    UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
                }
            );
        }
    }
#endregion

#region Logic For Server Guild
    public void CreateGuild(string guildName, int guildFlagIndex, string guildNotice, bool isPrivate, int requiredStageProgress)
    {
        var timeLeft = Mathf.Max(2f + TimeGuildFunction - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
        {
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
            return;
        }

        // Check gem
        if(ServerNetworkManager.Instance.Inventory.Gem < ServerNetworkManager.Instance.Setting.GuildCreatePrice)
        {
            UIManager.Instance.ShowAlertLocalized("message_not_enough_gem" , null , null);
            return;
        }
    
        ShowLoading();

        guildNotice = ChatManager.RemoveRichText(ChatManager.Instance.FilteredText(guildNotice));

        var userName = ServerNetworkManager.Instance.User.NickName;
        var stageProgress = ServerNetworkManager.Instance.User.StageProgress;

        ServerNetworkManager.Instance.CreateGuild(guildName, guildFlagIndex, guildNotice, isPrivate, requiredStageProgress, userName, stageProgress,
            ()=>{
                
                TimeGuildFunction = (int)Time.realtimeSinceStartup;

                HideLoading();
                uiViewOutgameMain.Refresh();
                uiPopupGuildCreate.Close();
                uiPopupGuild.RefreshMyGuildInfo();
            },
            (error)=>{
                HideLoading();

                if(error == "guild_name_duplicated")
                    UIManager.Instance.ShowAlertLocalized("message_guild_name_duplicated", null, null);
                else
                    UIManager.Instance.ShowAlertLocalized("message_guild_create_failed", null, null);

            }
        );
    }

    public void DeleteGuild()
    {
        var timeLeft = Mathf.Max(2f + TimeGuildFunction - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
        {
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
            return;
        }

        ShowLoading();

        ServerNetworkManager.Instance.GuildDelete(
            ()=>{
                HideLoading();
                uiPopupGuild.OnTabButtonClicked(0);
            },
            (error)=>{
                HideLoading();

                if(error == "guild_member_left")
                    UIManager.Instance.ShowAlertLocalized("message_guild_member_left", null, null);
                else
                    UIManager.Instance.ShowAlertLocalized("message_guild_delete_failed", null, null);
            }
        );
    }

    public void GuildMasterTransfer(string newGuildMasterId)
    {
        var timeLeft = Mathf.Max(2f + TimeGuildFunction - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
        {
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
            return;
        }

        ShowLoading();

        ServerNetworkManager.Instance.GuildMasterTransfer(newGuildMasterId,
            ()=>{
                HideLoading();
                uiPopupGuild.OnTabButtonClicked(1);
            },
            ()=>{
                HideLoading();
                UIManager.Instance.ShowAlertLocalized("message_guild_give_master_failed", null, null);
            }
        );
    }

    public void GetGuildApplicantList()
    {
        var timeLeft = Mathf.Max(2f + TimeGuildFunction - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
        {
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
            return;
        }

        ShowLoading();

        ServerNetworkManager.Instance.GetGuildApplicantList(
            ()=>{

                TimeGuildFunction = (int)Time.realtimeSinceStartup;

                HideLoading();
                uiPopupGuild.RefreshMyGuildInfo();
                uiPopupApplication.Open();
            },
            ()=>{
                HideLoading();
                UIManager.Instance.ShowAlertLocalized("message_get_guild_applicant_list_failed", null, null);
            }
        );
    }

    public void AccpectGuildApplicant(string applicantId)
    {
        var timeLeft = Mathf.Max(2f + TimeGuildFunction - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
        {
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
            return;
        }

        ShowLoading();

        ServerNetworkManager.Instance.GuildApplicantAccept(applicantId,
            ()=>{

                TimeGuildFunction = (int)Time.realtimeSinceStartup;

                HideLoading();
                uiPopupApplication.Refresh();
                uiPopupGuild.RefreshMyGuildInfo();
            },
            (error)=>{
                HideLoading();
                
                if(error == "unknown")
                    UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
                else
                    UIManager.Instance.ShowAlertLocalized("message_" + error, null, null);
            }
        );
    }

    public void RefuseGuildApplicant(string applicantId)
    {
        var timeLeft = Mathf.Max(2f + TimeGuildFunction - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
        {
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
            return;
        }

        ShowLoading();

        ServerNetworkManager.Instance.GuildApplicantRefuse(applicantId,
            ()=>{

                TimeGuildFunction = (int)Time.realtimeSinceStartup;

                HideLoading();
                uiPopupApplication.Refresh();
            },
            ()=>{
                HideLoading();
                UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
            }
        );
    }

    public void GetGuildKickList()
    {
        var timeLeft = Mathf.Max(2f + TimeGuildFunction - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
        {
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
            return;
        }

        ShowLoading();

        ServerNetworkManager.Instance.GetGuildKickList(
            ()=>{

                TimeGuildFunction = (int)Time.realtimeSinceStartup;

                HideLoading();
                uiPopupGuildKickList.Open();
            },
            ()=>{
                HideLoading();
                UIManager.Instance.ShowAlertLocalized("message_get_guild_kick_list_failed", null, null);
            }
        );
    }

    public void KickGuildMember(string memberId, string memeberName)
    {
        var timeLeft = Mathf.Max(2f + TimeGuildFunction - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
        {
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
            return;
        }

        ShowLoading();

        ServerNetworkManager.Instance.KickGuildMember(memberId, memeberName,
            ()=>{
                
                TimeGuildFunction = (int)Time.realtimeSinceStartup;

                HideLoading();
                uiPopupGuild.OnTabButtonClicked(1);
            },
            ()=>{
                HideLoading();
                UIManager.Instance.ShowAlertLocalized("message_guild_kick_failed", null, null);
            }
        );
    }

    public void KickCancelGuildMember(string memberId)
    {
        var timeLeft = Mathf.Max(2f + TimeGuildFunction - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
        {
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
            return;
        }

        ShowLoading();

        ServerNetworkManager.Instance.KickGuildMemberCancel(memberId,
            ()=>{

                TimeGuildFunction = (int)Time.realtimeSinceStartup;


                HideLoading();
                uiPopupGuildKickList.Refresh();
            },
            ()=>{
                HideLoading();
                UIManager.Instance.ShowAlertLocalized("message_guild_kick_cancel_failed", null, null);
            }
        );
    }

    public void EditGuildSetting(string guildNotice, bool isPrivate, int needStageProgress, int guildFlagIndex)
    {      
        var timeLeft = Mathf.Max(2f + TimeGuildFunction - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
        {
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
            return;
        }

        // Get guild management data
        var tmpGuildManagementData = ServerNetworkManager.Instance.GuildData.GuildManagementData;

        if(tmpGuildManagementData == null)
            return;

        guildNotice = ChatManager.RemoveRichText(ChatManager.Instance.FilteredText(guildNotice));
        
        // Check setting is changed
        if(tmpGuildManagementData.GuildNotice != guildNotice || tmpGuildManagementData.GuildFlagIndex != guildFlagIndex 
        || tmpGuildManagementData.RequiredStageProgress != needStageProgress || tmpGuildManagementData.IsPrivate != isPrivate)
        {
            ShowLoading();

            ServerNetworkManager.Instance.EditGuildSetting(guildNotice, guildFlagIndex, needStageProgress, isPrivate,
                ()=>{

                    TimeGuildFunction = (int)Time.realtimeSinceStartup;

                    HideLoading();
                    uiPopupGuild.RefreshMyGuildInfo();
                    uiPopupGuildSetting.Close();
                },
                ()=>{
                    HideLoading();
                    UIManager.Instance.ShowAlertLocalized("message_edit_guild_setting_failed", null, null);
                }
            );
        }
        else
        {
            uiPopupGuildSetting.Close();
        }
    }

    public void GetGuildWithId(string guildId)
    {
        var timeLeft = Mathf.Max(2f + TimeGuildFunction - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
        {
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
            return;
        }

        ShowLoading();

        ServerNetworkManager.Instance.GetGuildWithId(guildId.ToUpper(),
            ()=>{

                TimeGuildFunction = (int)Time.realtimeSinceStartup;

                HideLoading();
                uiPopupGuild.RefreshRecommendGuild();
            },
            ()=>{
                HideLoading();
                UIManager.Instance.ShowAlertLocalized("message_guild_not_found", null, null);
            }
        );
    }

    public void GetGuildRecommendation(Action<int> onSuccess)
    {
        var timeLeft = Mathf.Max(2f + TimeGuildFunction - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
        {
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
            return;
        }

        ShowLoading();

        ServerNetworkManager.Instance.GetGuildRecommendation(
            (leftGuildJoinTime)=>{

                TimeGuildFunction = (int)Time.realtimeSinceStartup;

                HideLoading();
                onSuccess.Invoke(leftGuildJoinTime);
            },
            ()=>{
                HideLoading();
                UIManager.Instance.ShowAlertLocalized("message_get_guild_recommendation_failed", null, null);
            }
        );
    }

    public void JoinGuild(string guildId, bool isPrivate)
    {
        var timeLeft = Mathf.Max(2f + TimeGuildFunction - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
        {
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
            return;
        }

        ShowLoading();

        // Get user name and stage progress
        string userName = ServerNetworkManager.Instance.User.NickName;
        int stageProgress = ServerNetworkManager.Instance.User.StageProgress;

        if(isPrivate)
        {
            ServerNetworkManager.Instance.PrivateGuildJoin(guildId, userName, stageProgress,
                ()=>{
                    
                    TimeGuildFunction = (int)Time.realtimeSinceStartup;

                    HideLoading();
                    uiPopupGuild.RefreshRecommendGuild();
                },
                (error)=>{
                    HideLoading();
                    
                    if(error == "unknown")
                        UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
                    else
                        UIManager.Instance.ShowAlertLocalized("message_" + error, null, null);
                }
            );
        }
        else
        {
            ServerNetworkManager.Instance.GuildJoin(guildId, userName, stageProgress,
                ()=>{
                    HideLoading();
                    uiPopupGuild.RefreshMyGuildInfo();
                },
                (error)=>{
                    HideLoading();

                    if(error == "unknown")
                        UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
                    else
                        UIManager.Instance.ShowAlertLocalized("message_" + error, null, null);
                }
            );
        }
    }

    public void CancelApplyGuild(string guildId)
    {      
        var timeLeft = Mathf.Max(2f + TimeGuildFunction - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
        {
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
            return;
        }

        if(ServerNetworkManager.Instance.GuildData.GuildApplyData.ApplyGuildList.Exists(n => n.ToUpper() == guildId))
        {
            ShowLoading();

            ServerNetworkManager.Instance.PrivateGuildJoinCancel(guildId,
                ()=>{

                    TimeGuildFunction = (int)Time.realtimeSinceStartup;

                    HideLoading();
                    uiPopupGuild.RefreshRecommendGuild();
                },
                ()=>{
                    HideLoading();
                    
                    UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
                }
            );
        }
    }

    public void LeaveGuild()
    {
        var timeLeft = Mathf.Max(2f + TimeGuildFunction - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
        {
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
            return;
        }

        // Check guild master
        if(ServerNetworkManager.Instance.User.PlayFabId == ServerNetworkManager.Instance.GuildData.GuildManagementData.GuildId)
        {
            UIManager.Instance.ShowAlertLocalized("message_guild_quit_failed_master", null, null);
            return;
        }

        ShowLoading();
        
        ServerNetworkManager.Instance.LeaveGuild(
            ()=>{
                HideLoading();
                uiPopupGuild.OnTabButtonClicked(0);
            },
            ()=>{
                HideLoading();
                UIManager.Instance.ShowAlertLocalized("message_guild_quit_failed", null, null);
            }
        );
    }

    public void GuildDonateFree()
    {
        var timeLeft = Mathf.Max(2f + TimeGuildFunction - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
        {
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
            return;
        }

        ShowLoading();

        ServerNetworkManager.Instance.GuildFreeDonate(
            ()=>{

                TimeGuildFunction = (int)Time.realtimeSinceStartup;

                HideLoading();
                uiPopupGuild.RefreshMyGuildInfo();
            },
            (error)=>{
                HideLoading();

                if(error == "unknown")
                    UIManager.Instance.ShowAlertLocalized("message_donate_failed", null, null);
                else if(error == "guild_join_time_not_enough")
                {
                    var text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_" + error), ServerNetworkManager.Instance.Setting.GuilFunctionCoolTime / (60 * 60));
                    UIManager.Instance.ShowAlert(text, null, null);
                }
                else
                    UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
            }
        );
    }

    public void GuildDonate(int donateAmount)
    {
        var timeLeft = Mathf.Max(2f + TimeGuildFunction - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
        {
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
            return;
        }
        
        // Check user's current gem amount
        if(donateAmount > ServerNetworkManager.Instance.Inventory.Gem)
        {
            UIManager.Instance.ShowAlertLocalized("message_not_enough_gem", null, null);
            return;
        }

        ShowLoading();

        ServerNetworkManager.Instance.GuildDonate(donateAmount,
            ()=>{

                TimeGuildFunction = (int)Time.realtimeSinceStartup;

                HideLoading();
                uiViewOutgameMain.Refresh();
                uiPopupGuild.RefreshMyGuildInfo();
                uiPopupGuildDonate.Close();
            },
            (error)=>{
                HideLoading();

                if(error == "unknown")
                    UIManager.Instance.ShowAlertLocalized("message_donate_failed", null, null);
                else if(error == "guild_join_time_not_enough")
                {
                    var text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_" + error), ServerNetworkManager.Instance.Setting.GuilFunctionCoolTime / (60 * 60));
                    UIManager.Instance.ShowAlert(text, null, null);
                }
                else
                    UIManager.Instance.ShowAlertLocalized("message_etc", null, null);            
            }
        );
    }

    public void GetGuildData(bool skipUpdateCache, Action onSuccess, Action onFailure)
    {
        var timeLeft = Mathf.Max(2f + TimeGuildFunction - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
        {
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
            return;
        }

        ShowLoading();

        // Get user name and stage progress
        string userName = ServerNetworkManager.Instance.User.NickName;
        int stageProgress = ServerNetworkManager.Instance.User.StageProgress;

        ServerNetworkManager.Instance.GuildRefresh(userName, stageProgress, skipUpdateCache,
            () =>{

                TimeGuildFunction = (int)Time.realtimeSinceStartup;

                HideLoading();
                onSuccess?.Invoke();
            },
            ()=>{
                HideLoading();
                UIManager.Instance.ShowAlertLocalized("message_get_guild_data_failed", null, null);
                onFailure?.Invoke();
            }
        );
    }

    public void GetGuildDataForRaid(Action onSuccess, Action onFailure)
    {
        ShowLoading();

        // Get user name and stage progress
        string userName = ServerNetworkManager.Instance.User.NickName;
        int stageProgress = ServerNetworkManager.Instance.User.StageProgress;

        ServerNetworkManager.Instance.GuildRefresh(userName, stageProgress, true,
            () =>{
                HideLoading();
                onSuccess?.Invoke();
            },
            ()=>{
                HideLoading();
               
                if(ServerNetworkManager.Instance.GuildData.GuildManagementData.GuildId == "")
                    UIManager.Instance.ShowAlertLocalized("message_guild_raid_enter_failed", null, null);
                else
                    UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
                
                onFailure?.Invoke();
            }
        );
    }
    
    public void UpgradeGuildRelic(string targetGuildRelicId, bool isUnlock, Action onSuccess)
    {
        var timeLeft = Mathf.Max(2f + TimeGuildRelicFunction - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
        {
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
            return;
        }

        ShowLoading();

        ServerNetworkManager.Instance.GuildUpgradeRelic(targetGuildRelicId, 1,
            () => {

                TimeGuildRelicFunction = (int)Time.realtimeSinceStartup;

                HideLoading();
                NumberMainController.Instance.Refresh();
                
                // Refresh the view
                uiViewOutgameMain.RefreshTopbar();
                onSuccess?.Invoke();

            },
            () => {
                HideLoading();
                
                if(isUnlock)
                    UIManager.Instance.ShowAlertLocalized("message_guild_relic_unlock_failed", null, null);
                else
                    UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
            }
        );
    }

#endregion

#region Logic For Server Liapp

    public void CheckLiappValidity()
    {
        StartCoroutine(CheckLiappValidityCoroutine());
    }

    private IEnumerator CheckLiappValidityCoroutine()
    {
        yield return new WaitForSeconds(10f);
#if !UNITY_EDITOR && !BYPASS_LIAPP
        ServerNetworkManager.Instance.GetUserKeyForLiapp(
            (userKey) => {
                var token = LiappManager.Instance.GetAuthKey(userKey);
                ServerNetworkManager.Instance.AuthLiapp(token, null, null);
            },
            null
        );
#endif
    }

#endregion

#region Utils

    public bool CheckText(string text)
	{
		if(filterLlist == null)
		{
			filterLlist = filterText.text.Split(new string[] {"\n", "\r\n"}, StringSplitOptions.RemoveEmptyEntries);
		}

        text = text.ToLower();
		
		foreach (var tmp in filterLlist)
		{
            if(HasNonASCIIChars(tmp))
			{
                if(text.Contains(tmp.ToLower()))
                    return false;
            }
			else
			{
                if(text == tmp)
                    return false;
                else if(text.Contains((" " + tmp + " ").ToLower()))
                    return false;
            }
		}

        var chkSpecialCase = !CheckSpecialCase(text); // added for check special name case
        return chkSpecialCase;
    }

    public bool CheckSpecialCase(string text) // if text contains special name, return true
    {
        string tempText = text;
        bool result = false;
        string pattern1 = "임|림|읨|윔|릠|륌";
        string pattern2 = "종|중|죵|즁|쫑|쭁|쭝|쯍";
        string pattern3 = "수|슈|쑤|쓔";
        var cond1 = Regex.Matches(text, pattern1);
        foreach (var item1 in cond1) // last name, first name
        {
            var cond1Idx = text.IndexOf(item1.ToString());
            tempText = text.Substring(cond1Idx + 1);
            var cond2 = Regex.Matches(tempText, pattern2);
            foreach (var item2 in cond2)
            {
                var cond2Idx = tempText.IndexOf(item2.ToString());
                tempText = tempText.Substring(cond2Idx + 1);
                var cond3 = Regex.Matches(tempText, pattern3);
                if (cond3.Count > 0) return true;
            }
        }

        cond1 = Regex.Matches(text, pattern2);
        foreach (var item1 in cond1) // fist name, last name
        {
            var cond1Idx = text.IndexOf(item1.ToString());
            tempText = text.Substring(cond1Idx + 1);
            var cond2 = Regex.Matches(tempText, pattern3);
            foreach (var item2 in cond2)
            {
                var cond2Idx = tempText.IndexOf(item2.ToString());
                tempText = tempText.Substring(cond2Idx + 1);
                var cond3 = Regex.Matches(tempText, pattern1);
                if (cond3.Count > 0) return true;
            }
        }
        return result;
    }
    
    public static bool HasNonASCIIChars(string str)
	{
		return (System.Text.Encoding.UTF8.GetByteCount(str) != str.Length);
	}

#endregion

}
