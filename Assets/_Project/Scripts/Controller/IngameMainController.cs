﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using System.Globalization;
using System.Linq;
using System;

using UnityEngine;
using Sirenix.OdinInspector;

using DG.Tweening;
/*
 */
/*
 */
/*
 */
/*
 */
using CodeStage.AntiCheat.ObscuredTypes;

public class IngameMainController : MonoBehaviour
{
    public static IngameMainController Instance;

    [Title("Current Values")]
    
    // Stage progress and which monster to show
    public ObscuredInt currentStageIndex = 0;

    // Boss related values
    public ObscuredBool IsShowingBoss = false;
    public ObscuredFloat bossCooldown = 30f;
    public ObscuredFloat bossCooldownPassed = 0f;
    private ObscuredFloat bossDuration = 15f;
    public ObscuredFloat BossDuration{
        get{
            // Get legndary relic effect
            var targetLegendaryRelic = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.LegendaryRelicType == LegendaryRelicType.BOSS_TIME);
            var bossDurationMutliplierByLegendaryRelic = (targetLegendaryRelic != null)? (1f + targetLegendaryRelic.BossTimeIncreasePercent / 100f) : 1f;

            return bossDuration * bossDurationMutliplierByLegendaryRelic;
        }
    }
    public ObscuredFloat bossPassed = 0f;
    private bool shouldWaitNextAttack = false;

    // Berserk related values
    [SerializeField]
    private ObscuredBool isBerserk = false;
    public bool IsBerserk{
        get{
            return isBerserk;
        }
    }

    [SerializeField]
    private ObscuredFloat berserkPassed = 0f;
    public float BerserkPassed{
        get{
            return berserkPassed;
        }
    }

    [SerializeField]
    private BigInteger goldByThisBerserk = 0;
    public BigInteger GoldByThisBerserk{
        get{
            return goldByThisBerserk;
        }
    }

    [Title("Spawn Info")]

    [SerializeField]
    private Transform actors = null;

    [SerializeField]
    private List<SpawnInfoModel> spawnMobList = new List<SpawnInfoModel>();
    public List<SpawnInfoModel> SpawnMobList{
        get{
            return spawnMobList;
        }
    } 

    [Title("Spawn References")]

    [SerializeField]
    private List<Transform> spawnPositionList = new List<Transform>();
    public List<Transform> SpawnPositionList{
        get{
            return spawnPositionList;
        }
    }

    [Title("Enitty References")]

    [SerializeField]
    private List<MobIdentity> mobIdentityList = new List<MobIdentity>();
    public List<MobIdentity> MobIdentityList{
        get{
            return mobIdentityList;
        }
    }

    [SerializeField]
    private MobIdentity bossIdentity = null;
    public MobIdentity BossIdentity
    {
        get{
            return bossIdentity;
        }
    }

    [SerializeField]
    private HeroIdentity heroIdentity;
    public HeroIdentity HeroIdentity
    {
        get{
            return heroIdentity;
        }
    }

    [SerializeField]
    private List<PetAnimation> petAnimations;

    [SerializeField]
    private MercenaryAnimation mercenaryAnimation;

    [Title("UI References")]

    [SerializeField]
    private UIViewFloatingHUD uiViewFloatingHUD;

    [SerializeField]
    private UIViewMain uiViewOutgameMain;

    [SerializeField]
    private UIViewFade uiViewFade;

    [SerializeField]
    private UIPopupUpgrade uiPopupUpgrade;

    [SerializeField]
    private UIPopupEquipment uiPopupEquipment;

    [SerializeField]
	private UIPopupToast uiPopupToast;

    [SerializeField]
    private UltimateJoystick ultimateJoystick;

    [SerializeField]
    private TargetTrackerLateFollow follower;

    [Title("Camera and Effects")]
    [SerializeField]
    private UnityEngine.Rendering.PostProcessing.PostProcessVolume postEffectVolume;

    [SerializeField]
    private FPSCounter fpsCounter;

    [Title("Others")]
    
    [SerializeField]
    private UnityEngine.Vector3 inputDirection = UnityEngine.Vector3.zero;
    public UnityEngine.Vector3 InputDirection{
        get{
            return inputDirection;
        }
    }

    private bool needToRefreshByBerserk = false;

    private void Awake()
    {
		IngameMainController.Instance = this;

        UIManager.Instance.ResetInputValues();

        // Hide actors to prevent nav issues
        actors.gameObject.SetActive(false);
	}

    public void InitBattle(bool isFromSceneLoad = true)
    {
        // Read values for the current battle situation
        currentStageIndex = Mathf.Max(1, ServerNetworkManager.Instance.User.SelectedStage);

        // Set the boss cooldown to be done
        if(isFromSceneLoad)
            bossCooldownPassed = bossCooldown;

        // Load proper stage date
        LoadStageData(
            (didLoadNewWorld) => {
                // Fade in
                if(isFromSceneLoad)
                    uiViewFade.FadeIn();

                // Enable actors
                actors.gameObject.SetActive(true);

                // Reset the hero from saved data
                heroIdentity.Reset(true, didLoadNewWorld, ServerNetworkManager.Instance.Inventory.SelectedClass.Id, ServerNetworkManager.Instance.Inventory.SelectedWeapon.Id, ServerNetworkManager.Instance.Inventory.WingIndex);

                // Reset the mob from saved data
                foreach(var tmp in mobIdentityList)
                {
                    tmp.ResetNormal();

                    if(isFromSceneLoad)
                    {
                        uiViewFloatingHUD.AddEnemyHPBar(tmp);
                        uiViewFloatingHUD.AddEnemyBossHPBar(tmp);
                        uiViewFloatingHUD.AddEnemyBossTime();
                    }
                }
                
                // Reset pet Information
                if(isFromSceneLoad)
                {
                    ResetPets(didLoadNewWorld);
                    ResetMercenary(didLoadNewWorld);
                }

                // Run battle loic
                if(isFromSceneLoad)
                    StartCoroutine(BattleLogicCoroutine());
            }
        );        
    }

    private IEnumerator BattleLogicCoroutine()
    {
        while(true)
		{
            CheckInput();
            CheckVisuals();

            CheckAttack();
            CheckMove();
            CheckAutoBehavior();

            CheckBossTiming();

            CheckBerserk();
            CheckBerserkTiming();
            
            uiViewFloatingHUD.Refresh();

            uiViewOutgameMain.RefreshBerserk();
            uiViewOutgameMain.RefreshBoss();
            uiViewOutgameMain.RefreshSkillCooldown();

            if(needToRefreshByBerserk)
            {
                if(uiViewOutgameMain != null)
                {
                    uiViewOutgameMain.RefreshTopbar();
                    uiViewOutgameMain.RefreshMenuGroup();
                }

                SoundManager.Instance.PlaySound("Coin");
            }
            needToRefreshByBerserk = false;
            
			yield return -1;
		}
    }

    private int previousBackground = 0;
    public void LoadStageData(Action<bool> onComplete)
    {
        StartCoroutine(LoadStageDataCoroutine(onComplete));
    }

    private IEnumerator LoadStageDataCoroutine(Action<bool> onComplete)
    {
        NumberMainController.Instance.MonsterHPByEnemy = BigInteger.Parse(BalanceInfoManager.Instance.StageInfoList[currentStageIndex - 1].Hp, NumberStyles.Any, CultureInfo.InvariantCulture);
        NumberMainController.Instance.BossHPByEnemy = NumberMainController.Instance.MonsterHPByEnemy * 10;
        NumberMainController.Instance.FarmingChanceByEnemy = float.Parse(BalanceInfoManager.Instance.StageInfoList[currentStageIndex - 1].FarmChance, NumberStyles.Any, CultureInfo.InvariantCulture);
        
        var rewardTier = int.Parse(BalanceInfoManager.Instance.StageInfoList[currentStageIndex - 1].RewardTier);
        NumberMainController.Instance.GoldIncomeByEnemy = BigInteger.Parse(BalanceInfoManager.Instance.RewardGoldInfoList[rewardTier - 1].Value, NumberStyles.Any, CultureInfo.InvariantCulture);
        
        var bestRewardTier = int.Parse(BalanceInfoManager.Instance.StageInfoList[ServerNetworkManager.Instance.User.StageProgress - 1].RewardTier);
        NumberMainController.Instance.GoldIncomeByBestEnemy = BigInteger.Parse(BalanceInfoManager.Instance.RewardGoldInfoList[bestRewardTier - 1].Value, NumberStyles.Any, CultureInfo.InvariantCulture);
        
        NumberMainController.Instance.Refresh();
        
        var worldIndex = (currentStageIndex - 1) / ServerNetworkManager.Instance.Setting.MaxStageInWorld;
        worldIndex %= 5;
        worldIndex += 1;
        if(previousBackground != worldIndex)
        {
            var operation = SceneChangeManager.Instance.LoadMap(worldIndex);
            previousBackground = worldIndex;

            yield return operation;

            // As a new map loaded, need to update the light probes
            LightProbes.TetrahedralizeAsync();

            // As a new map loaded, need to update the list of spawn positions
            spawnPositionList = GameObject.FindObjectOfType<SpawnPositions>().SpawnPositionList;

            onComplete?.Invoke(true);
        }
        else
        {
            onComplete?.Invoke(false);
        }    
    }

    float passed = 0f;
    private void CheckInput()
    {
        if(heroIdentity.IsAttacking)
        {
            passed = 0.15f;
            return;
        }

        if(Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S))
        {
            inputDirection = UnityEngine.Vector3.zero;
            if(Input.GetKey(KeyCode.D))
                inputDirection += UnityEngine.Vector3.right;
            if(Input.GetKey(KeyCode.A))
                inputDirection += UnityEngine.Vector3.left;
            if(Input.GetKey(KeyCode.W))
                inputDirection += UnityEngine.Vector3.forward;
            if(Input.GetKey(KeyCode.S))
                inputDirection -= UnityEngine.Vector3.forward;
            inputDirection = UnityEngine.Vector3.Normalize(inputDirection);
        }

        if(ultimateJoystick.GetVerticalAxis() != 0f || ultimateJoystick.GetHorizontalAxis() != 0f)
        {
            inputDirection = UnityEngine.Vector3.zero;
            inputDirection += UnityEngine.Vector3.forward * ultimateJoystick.GetVerticalAxis();
            inputDirection += UnityEngine.Vector3.right * ultimateJoystick.GetHorizontalAxis();
            inputDirection = UnityEngine.Vector3.Normalize(inputDirection);
        }
    
        if(passed >= 0.15f)
        {
            // Input relative things
            if(inputDirection != UnityEngine.Vector3.zero)
            {
                heroIdentity.MoveWithDirection(Camera.main.transform.TransformDirection(inputDirection));
                follower.FollowRotation = false;
                inputDirection = UnityEngine.Vector3.zero;
            }
            else
            {
                follower.FollowRotation = UIManager.Instance.IsCameraFollowRotation;
            }

            passed = 0;
        }
        passed += Time.deltaTime;
    }

    private void CheckVisuals()
    {
        // Check for visuals
        postEffectVolume.enabled = OptionManager.Instance.IsPostEffectOn;
        fpsCounter.gameObject.SetActive(OptionManager.Instance.IsPerformanceMonitorOn);
    }

    private void CheckAttack()
    {
        // Only when attack is asked to be done
        if(!UIManager.Instance.IsTouchedForAttackRecently)
            return;
        
        if(heroIdentity.IsAttacking)
            return;

        // Stop the hero
        heroIdentity.MoveStop();
        
        // Attack the mob
        heroIdentity.AttackMob(UIManager.Instance.SelectedSkillIndex);
        
        UIManager.Instance.IsTouchedForAttackRecently = false;
        UIManager.Instance.SelectedSkillIndex = 0;
    }

    private void CheckMove()
    {
        // Only move is asked to be done
        if(!UIManager.Instance.IsTouchedForMoveRecently)
            return;

        if(heroIdentity.IsAttacking)
            return;
        
        heroIdentity.MoveToDestination(UIManager.Instance.MoveDestination);

        UIManager.Instance.IsTouchedForMoveRecently = false;
    }

    private void CheckAutoBehavior()
    {
        if(UIManager.Instance.IsAutoOn)
        {
            // When the hero is doing nothing
            if(!heroIdentity.IsAttacking && !heroIdentity.IsRunning)
            {
                heroIdentity.AttackMobAfterMoveToMob();
            }
        }
    }

    private void CheckBossTiming()
    {
        if(IsShowingBoss)
        {
            if(bossPassed < BossDuration && !shouldWaitNextAttack)
            {
                bossPassed += Time.deltaTime;
                bossPassed = Mathf.Min(bossPassed, BossDuration);
            }

            // Check for boss ends
            if(bossPassed >= BossDuration && !heroIdentity.IsInSkillAttack)
            {
                EndBossMode();
                bossIdentity.ResetNormal();
                ShowToast(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_hunt_boss_failed"));
            }
        }
        else
        {
            if(bossCooldownPassed < bossCooldown)
            {
                bossCooldownPassed += Time.deltaTime;
                bossCooldownPassed = Mathf.Min(bossCooldownPassed, bossCooldown);
            }
        }
    }
    
    public void OnAttackDuringBossMode()
    {
        shouldWaitNextAttack = false;
    }

    private void CheckBerserk()
    {
        if(isBerserk)
            return;

        if(InterSceneManager.Instance.BerserkPoint >= NumberMainController.Instance.FinalBerserkTargetPoint)
        {
            goldByThisBerserk = 0;
            berserkPassed = 0f;
            isBerserk = true;

            heroIdentity.HeroAnimation.SetFeverEffect(true);
        }
    }

    private void CheckBerserkTiming()
    {
        if(isBerserk)
        {
            if(berserkPassed < NumberMainController.Instance.FinalBerserkTime)
            {
                berserkPassed += Time.deltaTime;
                berserkPassed = Mathf.Min(berserkPassed, NumberMainController.Instance.FinalBerserkTime);

                // To show the berserk point is being used up, spend berkserk point 
                InterSceneManager.Instance.BerserkPoint = (1f - berserkPassed / NumberMainController.Instance.FinalBerserkTime) * NumberMainController.Instance.FinalBerserkTargetPoint;
            }

            // Check for berserk ends
            if(berserkPassed >= NumberMainController.Instance.FinalBerserkTime)
            {
                InterSceneManager.Instance.BerserkPoint = 0f;
                isBerserk = false;

                heroIdentity.HeroAnimation.SetFeverEffect(false);
            }

        }
    }

    public void OnMonsterKill(MobIdentity target, float delayAfterKill)
    {
        if(target.IsDying)
            return;

        target.IsDying = true;

        // Update achievement related kill monster
        OutgameController.Instance.UpdateAchievement("achievement_01", 1);
 
        // Save monster hunt counts if the current stage is same as the stage progress
        if(currentStageIndex == ServerNetworkManager.Instance.User.StageProgress)
        {
            int updatedKillCountForBestStage = ServerNetworkManager.Instance.User.KillCountForBestStage + 1;
            ServerNetworkManager.Instance.User.KillCountForBestStage = Mathf.Min(99999, updatedKillCountForBestStage);
        }

        // Get bounty based on the enemy's state
        var receivedGold = ReceiveBounty(target);

        bool isTargetBoss = (IsShowingBoss && target == bossIdentity);

        // Play effect
        EffectController.Instance.ShowGoldEffect(target.transform, isTargetBoss);

        // Increase berserk
        if(!isBerserk && currentStageIndex == ServerNetworkManager.Instance.User.StageProgress)
        {
            InterSceneManager.Instance.BerserkPoint += (NumberMainController.Instance.FinalBerserkChargeSpeed * 15);
            InterSceneManager.Instance.BerserkPoint = Mathf.Min(InterSceneManager.Instance.BerserkPoint, NumberMainController.Instance.FinalBerserkTargetPoint);
        }

        // In case boss is killed
        if(isTargetBoss)
        {
            EndBossMode();
            
            currentStageIndex = Mathf.Min(ServerNetworkManager.Instance.Setting.MaxStageInWorld * ServerNetworkManager.Instance.Setting.LastWorldIndex, currentStageIndex + 1);

            // For a new stage progress
            bool isNewStageProgress = false;
            if(ServerNetworkManager.Instance.User.StageProgress < currentStageIndex)
                isNewStageProgress = true;

            if(isNewStageProgress)
            {
                // Need to reset the count
                ServerNetworkManager.Instance.User.KillCountForBestStage = 0;
            }

            ServerNetworkManager.Instance.User.StageProgress = Mathf.Max(ServerNetworkManager.Instance.User.StageProgress, currentStageIndex);
            ServerNetworkManager.Instance.User.SelectedStage = currentStageIndex;

            // Restart the battle
            InitBattle(false);

            string goldRewardInfoText = "";

            if(isNewStageProgress)
                goldRewardInfoText = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_hunt_boss_first_reward"), LanguageManager.Instance.NumberToString(receivedGold));
            else
                goldRewardInfoText = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_hunt_boss_reward"), LanguageManager.Instance.NumberToString(receivedGold));

            ShowToast($"{LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_hunt_boss_success")}\n{goldRewardInfoText}");

            // Reset boss cooldown
            bossCooldownPassed = bossCooldown;

            if(isNewStageProgress)
            {
                OutgameController.Instance.SyncPlayerData(false, "OnMonsterKill");
            }

            // Check for enjoying popup
            if(isNewStageProgress && currentStageIndex == 21)
            {
                OutgameController.Instance.CheckEnjoyingPopup();
            }

            SoundManager.Instance.PlaySound("New");
            SoundManager.Instance.Vibrate();
        }
        else
        {
            // For no boss fights, try to check whether to find a item
            var didFindItem = (UnityEngine.Random.Range(0f, 1f) < NumberMainController.Instance.FinalFarmingChance);

            var targetNameId = "";
            if(didFindItem)
            {
                if(UnityEngine.Random.Range(0f,1f) < 0.5f)
                {
                    // Give weapon
                    var targetId = "weapon_" + int.Parse(BalanceInfoManager.Instance.StageInfoList[currentStageIndex - 1].FarmItemIndex).ToString("D2");
                    targetNameId = "weapon_name_" + int.Parse(BalanceInfoManager.Instance.StageInfoList[currentStageIndex - 1].FarmItemIndex).ToString("D2");

                    WeaponModel itemToChangeCount = ServerNetworkManager.Instance.Inventory.WeaponList.FirstOrDefault(n => n.Id == targetId);
                    if(itemToChangeCount == null)
                    {
                        itemToChangeCount = new WeaponModel();
                        itemToChangeCount.Id = targetId;
                        itemToChangeCount.Count = 0;
                        itemToChangeCount.Level = 1;
                        itemToChangeCount.IsNew = true;
                        ServerNetworkManager.Instance.Inventory.WeaponList.Add(itemToChangeCount);
                    }
                    itemToChangeCount.Count ++;
                }
                else
                {
                    // Give class
                    var targetId = "class_" + int.Parse(BalanceInfoManager.Instance.StageInfoList[currentStageIndex - 1].FarmItemIndex).ToString("D2");
                    targetNameId = "class_name_" + int.Parse(BalanceInfoManager.Instance.StageInfoList[currentStageIndex - 1].FarmItemIndex).ToString("D2");

                    ClassModel itemToChangeCount = ServerNetworkManager.Instance.Inventory.ClassList.FirstOrDefault(n => n.Id == targetId);
                    if(itemToChangeCount == null)
                    {
                        itemToChangeCount = new ClassModel();
                        itemToChangeCount.Id = targetId;
                        itemToChangeCount.Count = 0;
                        itemToChangeCount.Level = 1;
                        itemToChangeCount.IsNew = true;
                        ServerNetworkManager.Instance.Inventory.ClassList.Add(itemToChangeCount);
                    }
                    itemToChangeCount.Count ++;
                }
            
                ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_hunt_got_item"), LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, targetNameId)));
            }
        }

        // Refresh ui that would be affected by killing the enemy
        if(uiViewOutgameMain != null)
            uiViewOutgameMain.Refresh();

        if(uiPopupUpgrade != null && uiPopupUpgrade.gameObject.activeSelf)
            uiPopupUpgrade.Refresh();

        if(uiPopupEquipment != null && uiPopupEquipment.gameObject.activeSelf)
            uiPopupEquipment.RefreshWithOutDestroy();

        // Show die animation of the mob
        target.MobAnimation.PlayDieAnimation();

        target.ResetNormal(3f);
    }

    public void OnMonsterHit()
    {
        if(isBerserk)
        {
            ReceiveBerserkBounty();

            // Refresh ui that would be affected by hiting the enemy during berserk
            needToRefreshByBerserk = true;
        }
    }

    private BigInteger ReceiveBounty(MobIdentity target)
    {
        BigInteger rewardGold = 0;
    
        if(isBerserk)
        {
            rewardGold = 10 * NumberMainController.Instance.FinalBerserkGoldIncome;
        }
        else
        {
            if(IsShowingBoss && target == bossIdentity && ServerNetworkManager.Instance.User.StageProgress == ServerNetworkManager.Instance.User.SelectedStage)
                rewardGold = target.Bounty * 10;
            else
                rewardGold = target.Bounty;
        }

        ServerNetworkManager.Instance.Inventory.Gold += rewardGold;

        SoundManager.Instance.PlaySound("Coin");

        return rewardGold;
    }

    private void ReceiveBerserkBounty()
    {
        goldByThisBerserk += NumberMainController.Instance.FinalBerserkGoldIncome;
        ServerNetworkManager.Instance.Inventory.Gold += NumberMainController.Instance.FinalBerserkGoldIncome;
    }

    public void ResetPets(bool resetPosition)
    {
        var mainPet = ServerNetworkManager.Instance.Inventory.MainPet;
        if(mainPet != null)
        {
            petAnimations[0].RefreshVisual(mainPet.Id);
            petAnimations[0].Reset(resetPosition);
        }
        else
        {
            petAnimations[0].RefreshVisual("");
            petAnimations[0].Reset(resetPosition);
        }
    }

    public void ResetMercenary(bool resetPosition)
    {
        var mainMercenary = ServerNetworkManager.Instance.Inventory.SelectedMercenary;
        if(mainMercenary != null)
        {
            mercenaryAnimation.RefreshVisual(mainMercenary.Id);
            mercenaryAnimation.Reset(resetPosition);
        }
        else
        {
            mercenaryAnimation.RefreshVisual("");
            mercenaryAnimation.Reset(resetPosition);
        }
    }
    
    public void StartBossMode()
    {
        if(!IsShowingBoss && bossCooldownPassed >= bossCooldown)
        {
            // Stop the hero
            heroIdentity.MoveStop();

            // Choose a nearest mob and make it the boss 
            float nearestDistance = float.MaxValue;
            foreach(var tmp in mobIdentityList)
            {
                if(tmp.HP <= 0)
                    continue;

                var distance = UnityEngine.Vector3.Distance(heroIdentity.transform.position, tmp.transform.position);
                if(nearestDistance > distance)
                {
                    bossIdentity = tmp;
                    nearestDistance = distance;
                }
            }

            if(bossIdentity == null)
                bossIdentity = mobIdentityList[0];

            bossIdentity.ResetBoss();

            IsShowingBoss = true;
            UIManager.Instance.NeedUIForBoss = true;
            bossPassed = 0f;

            if(heroIdentity.IsAttacking)
                shouldWaitNextAttack = true;
                
        }
    }

    public void EndBossMode()
    {
        Debug.Log("End Boss Mode");
        IsShowingBoss = false;
        UIManager.Instance.NeedUIForBoss = false;
        bossCooldownPassed = 0f;
    }

    public void ReadyForAttack()
    {
        UIManager.Instance.IsTouchedForAttackRecently = true;
    }

    public void ReadyForSkill(int skillIndex)
    {
        UIManager.Instance.IsTouchedForAttackRecently = true;
        UIManager.Instance.SelectedSkillIndex = skillIndex;
    }

    public void ReadyForMove(UnityEngine.Vector3 targetPosition)
    {
        UIManager.Instance.IsTouchedForMoveRecently = true;
        UIManager.Instance.MoveDestination = targetPosition;
    }

    public void StopMove()
    {
        UIManager.Instance.IsTouchedForMoveRecently = true;
        UIManager.Instance.MoveDestination = heroIdentity.transform.position;
    }

    public void RefreshHeroVisual(bool forceChange = false)
    {
        heroIdentity.Refresh(ServerNetworkManager.Instance.Inventory.SelectedClass.Id, ServerNetworkManager.Instance.Inventory.SelectedWeapon.Id, ServerNetworkManager.Instance.Inventory.WingIndex, forceChange);
    }

#region Util
    public void ShowToast(string text, bool isImportant = true)
    {
        if(uiPopupToast != null)
            uiPopupToast.ShowToast(text, isImportant);
    }
#endregion

}
