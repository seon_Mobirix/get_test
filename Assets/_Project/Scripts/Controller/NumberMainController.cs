﻿using System.Numerics;
using System.Linq;
using UnityEngine;
using Sirenix.OdinInspector;

using CodeStage.AntiCheat.ObscuredTypes;

/**/
/**/
/**/
/**/
public class NumberMainController : MonoBehaviour
{
    public static NumberMainController Instance;

    public bool showLogs = false;

    private void Awake(){
		NumberMainController.Instance = this;
        showLogs = false;
	}

#region Intermidiate Values For Display

    [Title("Intermidiate Values")]

    public BigInteger BaseAttackByUpgrade = 0;
    public ObscuredFloat BaseAttackMultiplierByWeapon = 0;
    public BigInteger BaseAttackMultiplierByRelic = 0;
    public ObscuredFloat BaseAttackMultiplierByPet = 0;
    public BigInteger BaseAttackMultiplierByBuff = 0;
    public ObscuredFloat BaseAttackMultiplierByMercenary = 0;
    public ObscuredInt BaseAttackMultiplierByWing = 0;
    public ObscuredInt BaseAttackMultiplierByWingBody = 0;

    public BigInteger CriticalAttackPercentByUpgrade = 0;
    public BigInteger CriticalMultiplierByRelic = 0;

    public BigInteger SuperAttackPercentByUpgrade = 0;
    public BigInteger SuperAttackByRelic = 0;

    public BigInteger UltimateAttackPercentByUpgrade = 0;
    public ObscuredFloat UltimateAttackMultiplierByLegendaryRelic = 0;

    public BigInteger HyperAttackPercentByUpgrade = 0;

    public ObscuredFloat BerserkTargetPointReduceByRelic = 0;
    public ObscuredFloat BerserkChargeSpeedMultiplierByClass = 0;
    public ObscuredFloat BerserkTimeIncreaseByRelic = 0f;

    public ObscuredFloat AttackSpeedMultiplierByClass = 0;

    public BigInteger GoldIncomeByEnemy = 0;
    public ObscuredFloat GoldIncomeMultiplierByClass = 0;
    public BigInteger GoldIncomeMultiplierByRelic = 0;
    public ObscuredFloat GoldIncomeMuliplierByPet = 0;
    public BigInteger GoldIncomeMultiplierByBuff = 0;
    public ObscuredInt GoldIncomeMuliplierByWing = 0;
    public ObscuredInt GoldIncomeMuliplierByWingBody = 0;

    public BigInteger GoldIncomeByBestEnemy = 0;
    public ObscuredFloat GoldIncomeMultiplierByBestClass = 0;
    public ObscuredFloat GoldIncomeMuliplierByBestPet = 0;

    public BigInteger SupplyBoxIncomeIncreasePercentByRelic = 0;
    
    public BigInteger BerserkGoldIncomeIncreasePercentByRelic = 0;
    public BigInteger BerserkGoldIncomeMultiplierByBuff = 0;

    public BigInteger ReturnGoldIncomeIncreasePercentByRelic = 0;

    public ObscuredFloat FarmingChanceByEnemy = 0f;
    public ObscuredFloat FarmingChanceIncreasePercentByRelic = 0f;

    public BigInteger MonsterHPByEnemy = 0;
    public ObscuredInt MonsterHPReducePercentByRelic = 0;

    public BigInteger BossHPByEnemy = 0;
    public ObscuredInt BossHPReducePercentByRelic = 0;

#endregion

#region Final Values That Should Be Actually Used

    [Title("Final Values")]

    public BigInteger FinalBaseAttackDamage = 0;
    public BigInteger FinalCriticalAttackDamage = 0;
    public ObscuredFloat FinalCriticalChance = 0f;
    public BigInteger FinalSuperAttackDamage = 0;
    public ObscuredFloat FinalSuperChance = 0f;
    public BigInteger FinalUltimateAttackDamage = 0;
    public ObscuredFloat FinalUltimateChance = 0f;
    public BigInteger FinalHyperAttackDamage = 0;
    public ObscuredFloat FinalHyperChance = 0f;

    public ObscuredFloat FinalAttackSpeed = 1f;

    public ObscuredFloat FinalBerserkTargetPoint = 1000f;
    public ObscuredFloat FinalBerserkChargeSpeed = 1f;
    public ObscuredFloat FinalBerserkTime = 5f;

    public BigInteger FinalGoldIncome = 0;
    public BigInteger FinalBerserkGoldIncome = 0;
    public BigInteger FinalBestGoldIncome = 0;

    public BigInteger FinalSupplyBoxIncome = 0;
    
    public ObscuredFloat FinalFarmingChance = 0;

    public BigInteger FinalMonsterHP = 1;
    public BigInteger FinalBossHP = 10;

#endregion

    public BigInteger DPA = 0;
    public BigInteger DPS = 0;

    public void Refresh()
    {
        float petPower = 0;
        foreach(var tmp in ServerNetworkManager.Instance.Inventory.SelectedPetList)
        {
            petPower += tmp.EffectPower;
        }
        petPower = Mathf.Max(petPower, 1f);

        float bestPetPower = 0;
        if(ServerNetworkManager.Instance.Inventory.BestPet != null)
            bestPetPower = ServerNetworkManager.Instance.Inventory.BestPet.EffectPower;
        bestPetPower = Mathf.Max(bestPetPower, 1f);

        BaseAttackByUpgrade = ServerNetworkManager.Instance.User.Upgrade.BaseAttack;
        BaseAttackMultiplierByWeapon = ServerNetworkManager.Instance.Inventory.SelectedWeapon.BaseAttackMultiplier;
        var targetRelic = ServerNetworkManager.Instance.Inventory.ExtendedRelicList.FirstOrDefault(n => n.RelicType == RelicType.BASE_ATTACK);
        BaseAttackMultiplierByRelic = (targetRelic != null)? targetRelic.BaseAttackMultiplier : 1;
        BaseAttackMultiplierByPet = petPower;
        BaseAttackMultiplierByBuff = BuffManager.Instance.GetAttackBuffValue(ServerNetworkManager.Instance.User.AttackBuffTier);
        if(BaseAttackMultiplierByBuff > 1000)
            AntiCheatManager.Instance.IsMemorySuspicious = true;

        if(ServerNetworkManager.Instance.Inventory.SelectedMercenary != null)
            BaseAttackMultiplierByMercenary = ServerNetworkManager.Instance.Inventory.SelectedMercenary.BaseAttackMultiplier;
        else
            BaseAttackMultiplierByMercenary = 1f;

        if(ServerNetworkManager.Instance.PurchasedWingList != null)
        {
            BaseAttackMultiplierByWing = 0;
            foreach(var wing in ServerNetworkManager.Instance.PurchasedWingList)
            {
                BaseAttackMultiplierByWing = BaseAttackMultiplierByWing + wing.OwnedStatBonus1Value;
            }
        }
        else
            BaseAttackMultiplierByWing = 0;

        if(ServerNetworkManager.Instance.WingBodyInfo != null)
            BaseAttackMultiplierByWingBody = ServerNetworkManager.Instance.WingBodyInfo.OwnedStatBonusValue;
        else
            BaseAttackMultiplierByWingBody = 1;

        BaseAttackMultiplierByWing = Mathf.Max(1, BaseAttackMultiplierByWing) * BaseAttackMultiplierByWingBody;

        FinalBaseAttackDamage = BaseAttackByUpgrade 
            * (BigInteger)(BaseAttackMultiplierByWeapon * 100f) / 100 
            * BaseAttackMultiplierByRelic 
            * (BigInteger)(BaseAttackMultiplierByPet * 100f) / 100 
            * BaseAttackMultiplierByBuff 
            * (BigInteger)(BaseAttackMultiplierByMercenary * 100f) / 100
            * (int)BaseAttackMultiplierByWing;
        
        if(showLogs)
        {
            Debug.Log("BaseAttackByUpgrade: " + BaseAttackByUpgrade);
            Debug.Log("BaseAttackMultiplierByWeapon: " + BaseAttackMultiplierByWeapon);
            Debug.Log("BaseAttackMultiplierByRelic: " + BaseAttackMultiplierByRelic);
            Debug.Log("BaseAttackMultiplierByPet: " + BaseAttackMultiplierByPet);
            Debug.Log("BaseAttackMultiplierByBuff: " + BaseAttackMultiplierByBuff);
            Debug.Log("BaseAttackMultiplierByMercenary: " + BaseAttackMultiplierByMercenary);
            Debug.Log("BaseAttackMultiplierByWing: " + BaseAttackMultiplierByWing);
            Debug.Log("FinalBaseAttackDamage: " + FinalBaseAttackDamage);
            Debug.Log("***************************************************");
        }

        CriticalAttackPercentByUpgrade = ServerNetworkManager.Instance.User.Upgrade.CriticalAttackPercent;
        targetRelic = ServerNetworkManager.Instance.Inventory.ExtendedRelicList.FirstOrDefault(n => n.RelicType == RelicType.CRITICAL_ATTACK);
        CriticalMultiplierByRelic = (targetRelic != null)? targetRelic.CriticalAttackMultiplier : 1;
        FinalCriticalAttackDamage = FinalBaseAttackDamage * CriticalAttackPercentByUpgrade / 100 * CriticalMultiplierByRelic;

        if(showLogs)
        {   
            Debug.Log("CriticalAttackPercentByUpgrade: " + CriticalAttackPercentByUpgrade);
            Debug.Log("CriticalMultiplierByRelic: " + CriticalMultiplierByRelic);
            Debug.Log("FinalCriticalAttackDamage: " + FinalCriticalAttackDamage);
            Debug.Log("***************************************************");
        }

        FinalCriticalChance = ServerNetworkManager.Instance.User.Upgrade.CriticalChance;

        if(showLogs)
        {
            Debug.Log("FinalCriticalChance: " + FinalCriticalChance);
            Debug.Log("***************************************************");
        }

        SuperAttackPercentByUpgrade = ServerNetworkManager.Instance.User.Upgrade.SuperAttackPercent;
        targetRelic = ServerNetworkManager.Instance.Inventory.ExtendedRelicList.FirstOrDefault(n => n.RelicType == RelicType.SUPER_ATTACK);
        SuperAttackByRelic = (targetRelic != null)? targetRelic.SuperAttackMultiplier : 1;
        FinalSuperAttackDamage = FinalCriticalAttackDamage * SuperAttackPercentByUpgrade / 100 * SuperAttackByRelic;

        if(showLogs)
        {
            Debug.Log("SuperAttackPercentByUpgrade: " + SuperAttackPercentByUpgrade);
            Debug.Log("SuperAttackByRelic: " + SuperAttackByRelic);
            Debug.Log("FinalSuperAttackDamage: " + FinalSuperAttackDamage);
            Debug.Log("***************************************************");
        }

        FinalSuperChance = ServerNetworkManager.Instance.User.Upgrade.SuperChance;

        if(showLogs)
        {
            Debug.Log("FinalSuperChance: " + FinalSuperChance);
            Debug.Log("***************************************************");
        }

        UltimateAttackPercentByUpgrade = ServerNetworkManager.Instance.User.Upgrade.UltimateAttackPercent;
        var targetLegendaryRelic = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.LegendaryRelicType == LegendaryRelicType.ULTIMATE_ATTACK);
        UltimateAttackMultiplierByLegendaryRelic = (targetLegendaryRelic != null)? targetLegendaryRelic.UltimateAttackMultiplier : 1;
        FinalUltimateAttackDamage = FinalSuperAttackDamage * UltimateAttackPercentByUpgrade / 100 * (BigInteger)(UltimateAttackMultiplierByLegendaryRelic * 100f) / 100;

        if(showLogs)
        {
            Debug.Log("UltimateAttackPercentByUpgrade: " + UltimateAttackPercentByUpgrade);
            Debug.Log("UltimateAttackMultiplierByLegendaryRelic: " + UltimateAttackMultiplierByLegendaryRelic);
            Debug.Log("FinalUltimateAttackDamage: " + FinalUltimateAttackDamage);
            Debug.Log("***************************************************");
        }

        FinalUltimateChance = ServerNetworkManager.Instance.User.Upgrade.UltimateChance;

        if(showLogs)
        {
            Debug.Log("FinalUltimateChance: " + FinalUltimateChance);
            Debug.Log("***************************************************");
        }

        HyperAttackPercentByUpgrade = ServerNetworkManager.Instance.User.Upgrade.HyperAttackPercent;
        FinalHyperAttackDamage = FinalUltimateAttackDamage * HyperAttackPercentByUpgrade / 100;

        if(showLogs)
        {
            Debug.Log("HyperAttackPercentByUpgrade: " + HyperAttackPercentByUpgrade);
            Debug.Log("FinalHyperAttackDamage: " + FinalHyperAttackDamage);
            Debug.Log("***************************************************");
        }

        FinalHyperChance = ServerNetworkManager.Instance.User.Upgrade.HyperChance;

        if(showLogs)
        {
            Debug.Log("FinalHyperChance: " + FinalHyperChance);
            Debug.Log("***************************************************");
        }

        targetRelic = ServerNetworkManager.Instance.Inventory.ExtendedRelicList.FirstOrDefault(n => n.RelicType == RelicType.BERSERK_POINT);
        BerserkTargetPointReduceByRelic = (targetRelic != null)? targetRelic.BerserkPointReduce : 0f;
        FinalBerserkTargetPoint = Mathf.Max(1000f + BerserkTargetPointReduceByRelic, 1f);

        if(showLogs)
        {
            Debug.Log("BerserkTargetPointReduceByRelic: " + BerserkTargetPointReduceByRelic);
            Debug.Log("FinalBerserkTargetPoint: " + FinalBerserkTargetPoint);
            Debug.Log("***************************************************");
        }

        BerserkChargeSpeedMultiplierByClass = ServerNetworkManager.Instance.Inventory.SelectedClass.BerserkChargeSpeedMultiplier;
        FinalBerserkChargeSpeed = 1f * BerserkChargeSpeedMultiplierByClass;
        
        if(showLogs)
        {
            Debug.Log("BerserkChargeSpeedMultiplierByClass: " + BerserkChargeSpeedMultiplierByClass);
            Debug.Log("FinalBerserkChargeSpeed: " + FinalBerserkChargeSpeed);
            Debug.Log("***************************************************");
        }

        targetRelic = ServerNetworkManager.Instance.Inventory.ExtendedRelicList.FirstOrDefault(n => n.RelicType == RelicType.BERSERK_TIME);
        BerserkTimeIncreaseByRelic = (targetRelic != null)? targetRelic.BerserkTimeIncrease : 0f;
        FinalBerserkTime = 10f + BerserkTimeIncreaseByRelic;
        
        if(showLogs)
        {
            Debug.Log("BerserkTimeIncreaseByRelic: " + BerserkTimeIncreaseByRelic);
            Debug.Log("FinalBerserkTime: " + FinalBerserkTime);
            Debug.Log("***************************************************");
        }

        AttackSpeedMultiplierByClass = ServerNetworkManager.Instance.Inventory.SelectedClass.AttackSpeedMultiplier;
        FinalAttackSpeed = 1f * AttackSpeedMultiplierByClass;
        
        if(showLogs)
        {
            Debug.Log("AttackSpeedMultiplierByClass: " + AttackSpeedMultiplierByClass);
            Debug.Log("FinalAttackSpeed: " + FinalAttackSpeed);
            Debug.Log("***************************************************");
        }

        GoldIncomeMultiplierByClass = ServerNetworkManager.Instance.Inventory.SelectedClass.GoldIncomeMultiplier;
        targetRelic = ServerNetworkManager.Instance.Inventory.ExtendedRelicList.FirstOrDefault(n => n.RelicType == RelicType.GOLD_INCOME);
        GoldIncomeMultiplierByRelic = (targetRelic != null)? targetRelic.GoldIncomeMultiplier : 1;
        GoldIncomeMuliplierByPet = petPower;
        GoldIncomeMultiplierByBuff = BuffManager.Instance.GetGoldBuffValue(ServerNetworkManager.Instance.User.GoldBuffTier);
        if(GoldIncomeMultiplierByBuff > 1000)
            AntiCheatManager.Instance.IsMemorySuspicious = true;
        
        if(ServerNetworkManager.Instance.PurchasedWingList != null)
        {
            GoldIncomeMuliplierByWing = 0;
            foreach(var wing in ServerNetworkManager.Instance.PurchasedWingList)
            {
                GoldIncomeMuliplierByWing = GoldIncomeMuliplierByWing + wing.OwnedStatBonus2Value;
            }
        }
        else
            GoldIncomeMuliplierByWing = 0;

        if(ServerNetworkManager.Instance.WingBodyInfo != null)
            GoldIncomeMuliplierByWingBody = ServerNetworkManager.Instance.WingBodyInfo.OwnedStatBonusValue;
        else
            GoldIncomeMuliplierByWingBody = 1;

        GoldIncomeMuliplierByWing = Mathf.Max(1, GoldIncomeMuliplierByWing) * GoldIncomeMuliplierByWingBody;

        FinalGoldIncome = GoldIncomeByEnemy 
            * (BigInteger)(GoldIncomeMultiplierByClass * 100f) / 100 
            * GoldIncomeMultiplierByRelic 
            * (BigInteger)(GoldIncomeMuliplierByPet * 100f) / 100 
            * GoldIncomeMultiplierByBuff
            * (int)GoldIncomeMuliplierByWing;
        
        if(showLogs)
        {
            Debug.Log("GoldIncomeByEnemy: " + GoldIncomeByEnemy);
            Debug.Log("GoldIncomeMultiplierByClass: " + GoldIncomeMultiplierByClass);
            Debug.Log("GoldIncomeMultiplierByRelic: " + GoldIncomeMultiplierByRelic);
            Debug.Log("GoldIncomeMuliplierByPet: " + GoldIncomeMuliplierByPet);
            Debug.Log("GoldIncomeMultiplierByBuff: " + GoldIncomeMultiplierByBuff);
            Debug.Log("GoldIncomeMuliplierByWing: " + GoldIncomeMuliplierByWing);
            Debug.Log("FinalGoldIncome: " + FinalGoldIncome);
            Debug.Log("***************************************************");
        }
        
        targetRelic = ServerNetworkManager.Instance.Inventory.ExtendedRelicList.FirstOrDefault(n => n.RelicType == RelicType.BERSERK_GOLD);
        BerserkGoldIncomeIncreasePercentByRelic = (targetRelic != null)? targetRelic.BerserkGoldIncreasePercent : 0;
        BerserkGoldIncomeMultiplierByBuff = BuffManager.Instance.GetGoldBuffValue(ServerNetworkManager.Instance.User.GoldBuffTier);
        if(BerserkGoldIncomeMultiplierByBuff > 1000)
            AntiCheatManager.Instance.IsMemorySuspicious = true;
        FinalBerserkGoldIncome = FinalGoldIncome * (100 + BerserkGoldIncomeIncreasePercentByRelic) / 100 * BerserkGoldIncomeMultiplierByBuff;
        
        if(showLogs)
        {
            Debug.Log("BerserkGoldIncomeIncreasePercentByRelic: " + BerserkGoldIncomeIncreasePercentByRelic);
            Debug.Log("BerserkGoldIncomeMultiplierByBuff: " + BerserkGoldIncomeMultiplierByBuff);
            Debug.Log("FinalBerserkGoldIncome: " + FinalBerserkGoldIncome);
            Debug.Log("***************************************************");
        }

        GoldIncomeMultiplierByBestClass = ServerNetworkManager.Instance.Inventory.BestClass.GoldIncomeMultiplier;
        GoldIncomeMuliplierByBestPet = bestPetPower;
        GoldIncomeMultiplierByBuff = BuffManager.Instance.GetGoldBuffValue(ServerNetworkManager.Instance.User.GoldBuffTier);
        if(GoldIncomeMultiplierByBuff > 1000)
            AntiCheatManager.Instance.IsMemorySuspicious = true;
        FinalBestGoldIncome = GoldIncomeByBestEnemy 
            * (BigInteger)(GoldIncomeMultiplierByBestClass * 100f) / 100 * GoldIncomeMultiplierByRelic * (BigInteger)(GoldIncomeMuliplierByBestPet * 100f) / 100 * GoldIncomeMultiplierByBuff
            * (100 + BerserkGoldIncomeIncreasePercentByRelic) / 100 * BerserkGoldIncomeMultiplierByBuff;
        
        if(showLogs)
        {
            Debug.Log("GoldIncomeByBestEnemy: " + GoldIncomeByBestEnemy);
            Debug.Log("GoldIncomeMultiplierByBestClass: " + GoldIncomeMultiplierByBestClass);
            Debug.Log("GoldIncomeMuliplierByBestPet: " + GoldIncomeMuliplierByBestPet);
            Debug.Log("FinalBestGoldIncome: " + FinalBestGoldIncome);
            Debug.Log("***************************************************");
        }

        targetRelic = ServerNetworkManager.Instance.Inventory.ExtendedRelicList.FirstOrDefault(n => n.RelicType == RelicType.RETURN_GOLD);
        SupplyBoxIncomeIncreasePercentByRelic = (targetRelic != null)? targetRelic.ReturnGoldIncreasePercent : 0;
        FinalSupplyBoxIncome = FinalBestGoldIncome * (100 + SupplyBoxIncomeIncreasePercentByRelic) / 100 * 10;
        
        if(showLogs)
        {
            Debug.Log("SupplyBoxIncomeIncreasePercentByRelic: " + SupplyBoxIncomeIncreasePercentByRelic);
            Debug.Log("FinalSupplyBoxIncome: " + FinalSupplyBoxIncome);
            Debug.Log("***************************************************");
        }

        targetRelic = ServerNetworkManager.Instance.Inventory.ExtendedRelicList.FirstOrDefault(n => n.RelicType == RelicType.FARMING);
        FarmingChanceIncreasePercentByRelic = (targetRelic != null)? targetRelic.FarmingIncreasePercent : 0f;
        FinalFarmingChance = FarmingChanceByEnemy + FarmingChanceIncreasePercentByRelic / 100f;
        
        if(showLogs)
        {
            Debug.Log("FarmingChanceByEnemy: " + FarmingChanceByEnemy);
            Debug.Log("FarmingChanceIncreasePercentByRelic: " + FarmingChanceIncreasePercentByRelic);
            Debug.Log("FinalFarmingChance: " + FinalFarmingChance);
            Debug.Log("***************************************************");
        }

        targetRelic = ServerNetworkManager.Instance.Inventory.ExtendedRelicList.FirstOrDefault(n => n.RelicType == RelicType.MONSTER_HP);
        MonsterHPReducePercentByRelic = (targetRelic != null)? targetRelic.MonsterHPReducePercent : 0;
        FinalMonsterHP = BigInteger.Max(MonsterHPByEnemy * Mathf.Max(100 + MonsterHPReducePercentByRelic, 1) / 100, 1);
        
        if(showLogs)
        {
            Debug.Log("MonsterHPByEnemy: " + MonsterHPByEnemy);
            Debug.Log("MonsterHPReducePercentByRelic: " + MonsterHPReducePercentByRelic);
            Debug.Log("FinalMonsterHP: " + FinalMonsterHP);
            Debug.Log("***************************************************");
        }

        targetRelic = ServerNetworkManager.Instance.Inventory.ExtendedRelicList.FirstOrDefault(n => n.RelicType == RelicType.BOSS_HP);
        BossHPReducePercentByRelic = (targetRelic != null)? targetRelic.BossHPReducePercent : 0;
        FinalBossHP = BigInteger.Max(BossHPByEnemy * Mathf.Max(100 + BossHPReducePercentByRelic, 1) / 100, 1);
        
        if(showLogs)
        {
            Debug.Log("BossHPByEnemy: " + BossHPByEnemy);
            Debug.Log("BossHPReducePercentByRelic: " + BossHPReducePercentByRelic);
            Debug.Log("FinalBossHP: " + FinalBossHP);
            Debug.Log("***************************************************");
        }

        // Do additional informative calculations

        DPA = 0;
        DPA += FinalBaseAttackDamage * (BigInteger)((1f - FinalCriticalChance) * 100) / 100;
        DPA += FinalCriticalAttackDamage * (BigInteger)(FinalCriticalChance * (1f - FinalSuperChance) * 100) / 100;
        DPA += FinalSuperAttackDamage * (BigInteger)(FinalCriticalChance * FinalSuperChance * (1f - FinalUltimateChance) * 100) / 100;
        DPA += FinalUltimateAttackDamage * (BigInteger)(FinalCriticalChance * FinalSuperChance * FinalUltimateChance * (1f - FinalHyperChance) * 100) / 100;
        DPA += FinalHyperAttackDamage * (BigInteger)(FinalCriticalChance * FinalSuperChance * FinalUltimateChance * FinalHyperChance * 100) / 100;
        if(DPA < 1)
            DPA = 1;

        DPS = DPA * 2 * (BigInteger)(FinalAttackSpeed * BuffManager.Instance.GetSpeedBuffValue(ServerNetworkManager.Instance.User.SpeedBuffTier) * 100) / 100;

        // Adjust hero speed
        if(IngameMainController.Instance.HeroIdentity != null)
            IngameMainController.Instance.HeroIdentity.HeroAnimation.AdjustMoveSpeed();

        
    }

}