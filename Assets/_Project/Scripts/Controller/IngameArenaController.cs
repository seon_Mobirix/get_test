﻿using System.Collections;
using System.Numerics;
using System;
using UnityEngine;
using Sirenix.OdinInspector;

using CodeStage.AntiCheat.ObscuredTypes;

public class IngameArenaController : MonoBehaviour
{
    public class ConstantBattleData
    {
        public int RemindCount;         //남은진입횟수
        public int CurrentCount;        //현제 진행 횟수
        public bool IsConstantBattle = false;            //연속 전투 여부
    }
    public static IngameArenaController Instance;

    public static ConstantBattleData ArenaConstantBattleData = new ConstantBattleData();            //연속 전투 여부

    [Title("Current Values")]
    public BigInteger HeroDamage = 0;
    public BigInteger EnemyDamage = 0;

    public int AttackCount = 0;

    public ObscuredFloat PassedTime = 0f;
    const float ArenaBattleTotalTime = 22.0f;
    public int LeftTime{
        get{
            return (int)(ArenaBattleTotalTime - PassedTime);
        }
    }
    const float BattleSkipTimeRate = 5.0f;

    private bool isPlaying = false;

    [Title("Enitty References")]

    [SerializeField]
    private HeroIdentity heroIdentity;
    public HeroIdentity HeroIdentity{
        get{
            return heroIdentity;
        }
    }

    [SerializeField]
    private HeroIdentity opponentIdentity;

    [Title("UI References")]

    [SerializeField]
    private UIViewFloatingHUD uiViewFloatingHUD;

    [SerializeField]
    private UIViewArena uiViewArena;

    [SerializeField]
    private UIViewFade uiViewFade;

    [SerializeField]
    private UltimateJoystick ultimateJoystick;

    [Title("Camera and Effects")]
    [SerializeField]
    private UnityEngine.Rendering.PostProcessing.PostProcessVolume postEffectVolume;
    
    [Title("Others")]
    
    [SerializeField]
    private UnityEngine.Vector3 inputDirection = UnityEngine.Vector3.zero;
    public UnityEngine.Vector3 InputDirection{
        get{
            return inputDirection;
        }
    }

    private void Awake()
    {
		IngameArenaController.Instance = this;
        UIManager.Instance.ResetInputValues();
	}

    private void Start()
    {
        InitBattle();

        // Refresh arena UI to show data as battle started
        uiViewArena.Refresh();

        UIManager.Instance.StartCoroutine(ArenaBattleSpeedCoroutine(false));
    }

    IEnumerator ArenaBattleSpeedCoroutine(bool isEnd)
    {
        if(!PlayerPrefs.HasKey("arenaSpeedRate") || PlayerPrefs.GetInt("arenaSpeedRate") == 0)
        {
            SetArenaSpeedRate(isUp: false);
            yield break;
        }

        if (isEnd)
            SetArenaSpeedRate(isUp: false);
        else
        {
            yield return new WaitForSeconds(2.2f);
            SetArenaSpeedRate(isUp: true);
        }
    }

    public void SetArenaSpeedRate(bool isUp)
    {
        if(isUp)
            Time.timeScale = BattleSkipTimeRate;
        else
            Time.timeScale = 1.0f;
    }

    float passed = 0f;
    private void Update()
    {
        if(PassedTime >= 17f)
            return;

        if(heroIdentity.IsAttacking)
        {
            passed = 0.15f;
            return;
        }

        if(Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S))
        {
            inputDirection = UnityEngine.Vector3.zero;
            if(Input.GetKey(KeyCode.D))
                inputDirection += UnityEngine.Vector3.right;
            if(Input.GetKey(KeyCode.A))
                inputDirection += UnityEngine.Vector3.left;
            if(Input.GetKey(KeyCode.W))
                inputDirection += UnityEngine.Vector3.forward;
            if(Input.GetKey(KeyCode.S))
                inputDirection -= UnityEngine.Vector3.forward;
            inputDirection = UnityEngine.Vector3.Normalize(inputDirection);
        }

        if(ultimateJoystick.GetVerticalAxis() != 0f || ultimateJoystick.GetHorizontalAxis() != 0f)
        {
            inputDirection = UnityEngine.Vector3.zero;
            inputDirection += UnityEngine.Vector3.forward * ultimateJoystick.GetVerticalAxis();
            inputDirection += UnityEngine.Vector3.right * ultimateJoystick.GetHorizontalAxis();
            inputDirection = UnityEngine.Vector3.Normalize(inputDirection);
        }
    
        if(passed >= 0.15f)
        {
            // Input related
            if(inputDirection != UnityEngine.Vector3.zero)
            {
                heroIdentity.MoveWithDirection(Camera.main.transform.TransformDirection(inputDirection));
                inputDirection = UnityEngine.Vector3.zero;
            }
            passed = 0;

            // Check for visuals
            postEffectVolume.enabled = OptionManager.Instance.IsPostEffectOn;
        }
        passed += Time.deltaTime;
    }

    private void InitBattle()
    {
        uiViewFade.FadeIn();

        NumberArenaController.Instance.RefreshHero();
        NumberArenaController.Instance.RefreshEnemy();

        heroIdentity.Reset(true, false, ServerNetworkManager.Instance.Inventory.SelectedClass.Id, ServerNetworkManager.Instance.Inventory.SelectedWeapon.Id, ServerNetworkManager.Instance.Inventory.WingIndex, true, false);
        opponentIdentity.Reset(false, false, InterSceneManager.Instance.OpponentInventoryData.SelectedClass.Id, InterSceneManager.Instance.OpponentInventoryData.SelectedWeapon.Id, InterSceneManager.Instance.OpponentInventoryData.WingIndex, true, false);

        // Run battle loic
		StartCoroutine(BattleLogicCoroutine());
    }

    private IEnumerator BattleLogicCoroutine()
    {
        yield return new WaitForSeconds(1f);

        isPlaying = true;

        while(isPlaying)
		{
			CheckTime();

            CheckAttack();

            if(PassedTime < 17f)
                CheckMove();
            CheckAutoBehavior();
            CheckEnemyAttack();

            CheckValueChanges();

            uiViewArena.RefreshSkillCooldown();
            
			yield return -1;
		}

        yield return new WaitForSeconds(1f);

        uiViewFade.FadeOut();

        yield return new WaitForSeconds(0.75f);

        SceneChangeManager.Instance.LoadMainGame();
    }

    private void CheckTime()
    {
        if(!isPlaying)
            return;

        if(PassedTime > ArenaBattleTotalTime)
        {
            FinishGame();
        }
        else
        {
            PassedTime += Time.deltaTime;
        }
    }

    private void FinishGame()
    {
        if(!isPlaying)
            return;

        isPlaying = false;

        if(HeroDamage != 0 && HeroDamage >= EnemyDamage)
        {
            if(InterSceneManager.Instance.DidArenaBattle)
                InterSceneManager.Instance.DidArenaWin = true;
            else if(InterSceneManager.Instance.DidTowerBattle)
                InterSceneManager.Instance.DidTowerWin = true;
            uiViewArena.RefreshBattleText(true, true);
        }
        else
        {
            if(InterSceneManager.Instance.DidArenaBattle)
                InterSceneManager.Instance.DidArenaWin = false;
            else if(InterSceneManager.Instance.DidTowerBattle)
                InterSceneManager.Instance.DidTowerWin = false;
            uiViewArena.RefreshBattleText(true, false);
        }
        UIManager.Instance.StartCoroutine(ArenaBattleSpeedCoroutine(true));
    }

    private void CheckAttack()
    {
        // Only when attack is asked to be done
        if(!UIManager.Instance.IsTouchedForAttackRecently)
            return;
        
        if(heroIdentity.IsAttacking)
            return;

        // Stop the hero
        heroIdentity.MoveStop();
        
        // Attack the hero
        heroIdentity.AttackHero(UIManager.Instance.SelectedSkillIndex);
        
        UIManager.Instance.IsTouchedForAttackRecently = false;
        UIManager.Instance.SelectedSkillIndex = 0;
    }

    private void CheckMove()
    {
        // Only move is asked to be done
        if(!UIManager.Instance.IsTouchedForMoveRecently)
            return;

        if(heroIdentity.IsAttacking)
            return;
            
        heroIdentity.MoveToDestination(UIManager.Instance.MoveDestination);

        UIManager.Instance.IsTouchedForMoveRecently = false;
    }

    private void CheckAutoBehavior()
    {
        // In arena, always auto on
        // When the hero is doing nothing
        if(!heroIdentity.IsAttacking && !heroIdentity.IsRunning)
        {
            heroIdentity.AttackHeroAfterMoveToHero();
        }
    }

    private void CheckEnemyAttack()
    {
        if(!opponentIdentity.IsAttacking && !opponentIdentity.IsRunning)
        {
            opponentIdentity.AttackHeroAfterMoveToHero();
        }
    }

    private BigInteger previousHeroDamage = -1;
    private BigInteger previousEnemyDamage = -1;
    private int previousLeftTime = -1;
    private void CheckValueChanges()
    {
        if(previousHeroDamage != HeroDamage || previousEnemyDamage != EnemyDamage)
        {
            AttackCount ++;
            CheckColdGame((previousHeroDamage != HeroDamage), (previousEnemyDamage != EnemyDamage));
            uiViewArena.RefreshGauge();
        }
        
        previousHeroDamage = HeroDamage;
        previousEnemyDamage = EnemyDamage;

        if(previousLeftTime != LeftTime)
        {
            uiViewArena.RefreshTime();
        }
        previousLeftTime = LeftTime;
    }

    private void CheckColdGame(bool isHeroChanged, bool isEnemyChanged)
    {
        if(!isPlaying || PassedTime < 9f)
            return;

        if(IngameArenaController.Instance.HeroDamage == 0 || IngameArenaController.Instance.EnemyDamage == 0)
            return;

        var valueToCheck = (float)Math.Exp(BigInteger.Log(IngameArenaController.Instance.HeroDamage + 1) - BigInteger.Log(IngameArenaController.Instance.HeroDamage + IngameArenaController.Instance.EnemyDamage + 2));
        
        if(valueToCheck < 0.1f && isEnemyChanged)
        {
            heroIdentity.Die();
            FinishGame();
        }
        else if(valueToCheck > 0.9f && isHeroChanged)
        {
            opponentIdentity.Die();
            FinishGame();
        }  
    }


    public void ReadyForAttack()
    {
        UIManager.Instance.IsTouchedForAttackRecently = true;
    }

    public void ReadyForSkill(int skillIndex)
    {
        UIManager.Instance.IsTouchedForAttackRecently = true;
        UIManager.Instance.SelectedSkillIndex = skillIndex;
    }

    public void ReadyForMove(UnityEngine.Vector3 targetPosition)
    {
        UIManager.Instance.IsTouchedForMoveRecently = true;
        UIManager.Instance.MoveDestination = targetPosition;
    }

    public void StopMove()
    {
        UIManager.Instance.IsTouchedForMoveRecently = true;
        UIManager.Instance.MoveDestination = heroIdentity.transform.position;
    }


}
