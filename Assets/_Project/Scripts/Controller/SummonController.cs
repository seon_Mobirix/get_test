﻿using System.Collections.Generic;
using UnityEngine;

using System.Linq;
using System.Globalization;

using CodeStage.AntiCheat.ObscuredTypes;

public enum SummonType {WEAPON, CLASS, RELIC, PET, LEGEMDARY_RELIC, MERCENARY};

public class SummonController : MonoBehaviour
{
    public static SummonController Instance;
    
    public int MinimumIndexOfWeaponSummon{
        get{
            return 
                Mathf.Min(ServerNetworkManager.Instance.Setting.TotalWeaponCount - 1, int.Parse(BalanceInfoManager.Instance.SummonInfoList[ServerNetworkManager.Instance.User.WeaponGachaLevel - 1].SummonMinIndex));
        }
    }

    public int MinimumIndexOfClassSummon{
        get{
            return 
                Mathf.Min(ServerNetworkManager.Instance.Setting.TotalClassCount - 1, int.Parse(BalanceInfoManager.Instance.SummonInfoList[ServerNetworkManager.Instance.User.ClassGachaLevel - 1].SummonMinIndex));
        }
    }

    public int MinimumIndexOfMercenarySummon{
        get{
            return 
                Mathf.Min(ServerNetworkManager.Instance.Setting.TotalMercenaryCount - 1, int.Parse(BalanceInfoManager.Instance.SummonInfoList[ServerNetworkManager.Instance.User.MercenaryGachaLevel - 1].SummonMinIndex));
        }
    }

    public int MinimumIndexOfPetSummon{
        get{
            return 0; 
        }
    }

    private void Awake(){
		SummonController.Instance = this;
	}

    public List<float> WeaponSummonTable{
        get{
            float totalPower = 0f;
            for(int i = MinimumIndexOfWeaponSummon; i < ServerNetworkManager.Instance.Setting.TotalWeaponCount; i ++)
                totalPower += float.Parse(BalanceInfoManager.Instance.WeaponInfoList[i].SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture);
            
            var toReturn = new List<float>();
            for(int i = MinimumIndexOfWeaponSummon; i < ServerNetworkManager.Instance.Setting.TotalWeaponCount; i ++)
                toReturn.Add(float.Parse(BalanceInfoManager.Instance.WeaponInfoList[i].SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture) / (float)totalPower);

            return toReturn;
        }
    }

    public List<float> ClassSummonTable{
        get{
            float totalPower = 0f;
            for(int i = MinimumIndexOfClassSummon; i < ServerNetworkManager.Instance.Setting.TotalClassCount; i ++)
                totalPower += float.Parse(BalanceInfoManager.Instance.ClassInfoList[i].SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture);
            
            var toReturn = new List<float>();
            for(int i = MinimumIndexOfClassSummon; i < ServerNetworkManager.Instance.Setting.TotalClassCount; i ++)
                toReturn.Add(float.Parse(BalanceInfoManager.Instance.ClassInfoList[i].SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture) / (float)totalPower);

            return toReturn;
        }
    }

    public List<float> MercenarySummonTable{
        get{
            float totalPower = 0f;
            for(int i = MinimumIndexOfMercenarySummon; i < ServerNetworkManager.Instance.Setting.TotalMercenaryCount; i ++)
                totalPower += float.Parse(BalanceInfoManager.Instance.MercenaryInfoList[i].SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture);
            
            var toReturn = new List<float>();
            for(int i = MinimumIndexOfMercenarySummon; i < ServerNetworkManager.Instance.Setting.TotalMercenaryCount; i ++)
                toReturn.Add(float.Parse(BalanceInfoManager.Instance.MercenaryInfoList[i].SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture) / (float)totalPower);

            return toReturn;
        }
    }

    public List<float> RelicSummonTable{
        get{
            float totalPower = 0f;
            for(int i = 0; i < BalanceInfoManager.Instance.RelicInfoList.Count; i ++)
                totalPower += float.Parse(BalanceInfoManager.Instance.RelicInfoList[i].SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture);
            
            var toReturn = new List<float>();
            for(int i = 0; i < BalanceInfoManager.Instance.RelicInfoList.Count; i ++)
                toReturn.Add(float.Parse(BalanceInfoManager.Instance.RelicInfoList[i].SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture) / (float)totalPower);

            return toReturn;
        }
    }

    public List<float> LegendaryRelicSummonTable{
        get{
            
            var convertedIndexLegendaryRelicInfoList = new List<RelicInfoModel>();

            // Remove max level legnedary relic
            foreach (var tmp in BalanceInfoManager.Instance.LegendaryRelicInfoList)
            {
                var targetLegendaryRelic = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.Id == tmp.Id);
                
                if(targetLegendaryRelic != null)
                {
                    // Check level + count vs target legendary relic max level
                    if((targetLegendaryRelic.Level + targetLegendaryRelic.Count - 1) < int.Parse(tmp.MaxLevel))
                        convertedIndexLegendaryRelicInfoList.Add(tmp);
                }
                else
                    convertedIndexLegendaryRelicInfoList.Add(tmp);
            }

            float totalPower = 0f;
            for(int i = 0; i < convertedIndexLegendaryRelicInfoList.Count; i ++)
                totalPower += float.Parse(convertedIndexLegendaryRelicInfoList[i].SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture);
            
            var toReturn = new List<float>();
            for(int i = 0; i < convertedIndexLegendaryRelicInfoList.Count; i ++)
                toReturn.Add(float.Parse(convertedIndexLegendaryRelicInfoList[i].SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture) / (float)totalPower);

            return toReturn;
        }
    }

    public List<string> PossibleLegendaryRelicIds{
        get{
            var toReturn = new List<string>();

            // Remove max level legnedary relic
            foreach (var tmp in BalanceInfoManager.Instance.LegendaryRelicInfoList)
            {
                var targetLegendaryRelic = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.Id == tmp.Id);
                
                if(targetLegendaryRelic != null)
                {
                    // Check level + count vs target legendary relic max level
                    if((targetLegendaryRelic.Level + targetLegendaryRelic.Count - 1) < int.Parse(tmp.MaxLevel))
                        toReturn.Add(tmp.Id);
                }
                else
                    toReturn.Add(tmp.Id);
            }

            return toReturn;
        }
    }

    public List<float> PetSummonTable{
        get{
            float totalPower = 0f;
            for(int i = MinimumIndexOfPetSummon; i < ServerNetworkManager.Instance.Setting.TotalPetCount; i ++)
                totalPower += float.Parse(BalanceInfoManager.Instance.PetInfoList[i].SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture);
            
            var toReturn = new List<float>();
            for(int i = MinimumIndexOfPetSummon; i < ServerNetworkManager.Instance.Setting.TotalPetCount; i ++)
                toReturn.Add(float.Parse(BalanceInfoManager.Instance.PetInfoList[i].SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture) / (float)totalPower);

            return toReturn;
        }
    }

    public (bool isEnoughGem, List<string> summonItemIdList, List<bool> newInfoList, List<string> goodSummonIdList) SummonWeapons(int amount)
    {
        var requiredGem = (amount == 1)? 10 : amount / 11 * 100;
        var isEnoughGem = (ServerNetworkManager.Instance.TotalGem >= requiredGem);
    
        // Check and use gems
        if(!isEnoughGem)
            return (false, new List<string>(), new List<bool>(), new List<string>());
        ServerNetworkManager.Instance.UnsyncedGem -= requiredGem;
        ServerNetworkManager.Instance.User.UnsyncedGemChangeCount ++;

        var summonTable = this.WeaponSummonTable;

        // Generate list of items summoned
        var summonedItems = new List<string>();
        for(int i = 0; i < amount; i ++)
        {
            var summonResult = Random.Range(0f, 1f);
            
            int j = 0;
            for (; j < summonTable.Count; j ++)
            {
                summonResult = summonResult - summonTable[j];
                if(summonResult <= 0f)
                    break;
            }
            summonedItems.Add(BalanceInfoManager.Instance.WeaponInfoList[j + MinimumIndexOfWeaponSummon].Id);
        }

        // Generate list of new or not infomation
        var newList = new List<bool>();

        // New good summon list
        var goodSummonIdList = new List<string>();

        // Add those items to the inventory
        foreach(var tmp in summonedItems)
        {
            WeaponModel itemToChangeCount = ServerNetworkManager.Instance.Inventory.WeaponList.FirstOrDefault(n => n.Id == tmp);
            if(itemToChangeCount == null)
            {
                itemToChangeCount = new WeaponModel();
                itemToChangeCount.Id = tmp;
                itemToChangeCount.Count = 0;
                itemToChangeCount.Level = 1;
                itemToChangeCount.IsNew = true;
                ServerNetworkManager.Instance.Inventory.WeaponList.Add(itemToChangeCount);
                newList.Add(true);

                // Check item id for announce
                int convertedIndex = int.Parse(itemToChangeCount.Id.Substring(itemToChangeCount.Id.Length - 2));
                
                if(convertedIndex > 16)
                {
                    goodSummonIdList.Add(itemToChangeCount.Id);
                    InterSceneManager.Instance.GoodSummonCount ++;
                }
            }
            else
            {
                newList.Add(false);
            }
            itemToChangeCount.Count ++;
        }

        // Add exp to user's weapon gacha level
        ServerNetworkManager.Instance.User.WeaponGachaExp += amount;
        if(ServerNetworkManager.Instance.User.WeaponGachaLevel < ServerNetworkManager.Instance.Setting.MaxSummonLevel && int.Parse(BalanceInfoManager.Instance.SummonInfoList[ServerNetworkManager.Instance.User.WeaponGachaLevel].SummonExp) <= ServerNetworkManager.Instance.User.WeaponGachaExp)
        {
            ServerNetworkManager.Instance.User.WeaponGachaExp -= int.Parse(BalanceInfoManager.Instance.SummonInfoList[ServerNetworkManager.Instance.User.WeaponGachaLevel].SummonExp);
            ServerNetworkManager.Instance.User.WeaponGachaLevel ++;
        }

        return (true, summonedItems, newList, goodSummonIdList);
    }

    public (bool isEnoughGem, List<string> summonItemIdList, List<bool> newInfoList, List<string> goodSummonIdList) SummonClass(int amount)
    {
        var requiredGem = (amount == 1)? 10 : amount / 11 * 100;
        var isEnoughGem = (ServerNetworkManager.Instance.TotalGem >= requiredGem);
    
        // Check and use gems
        if(!isEnoughGem)
            return (false, new List<string>(), new List<bool>(), new List<string>());
        ServerNetworkManager.Instance.UnsyncedGem -= requiredGem;
        ServerNetworkManager.Instance.User.UnsyncedGemChangeCount ++;

        var summonTable = this.ClassSummonTable;

        // Generate list of items summoned
        var summonedItems = new List<string>();
        for(int i = 0; i < amount; i ++)
        {
            var summonResult = Random.Range(0f, 1f);
            
            int j = 0;
            for (; j < summonTable.Count; j ++)
            {
                summonResult = summonResult - summonTable[j];
                if(summonResult <= 0f)
                    break;
            }
            summonedItems.Add(BalanceInfoManager.Instance.ClassInfoList[j + MinimumIndexOfClassSummon].Id);
        }

        // Generate list of new or not infomation
        var newList = new List<bool>();
        
        // New good summon list
        var goodSummonIdList = new List<string>();

        // Add those items to the inventory
        foreach(var tmp in summonedItems)
        {
            ClassModel itemToChangeCount = ServerNetworkManager.Instance.Inventory.ClassList.FirstOrDefault(n => n.Id == tmp);
            if(itemToChangeCount == null)
            {
                itemToChangeCount = new ClassModel();
                itemToChangeCount.Id = tmp;
                itemToChangeCount.Count = 0;
                itemToChangeCount.Level = 1;
                itemToChangeCount.IsNew = true;
                ServerNetworkManager.Instance.Inventory.ClassList.Add(itemToChangeCount);
                newList.Add(true);

                // Check item id for announce
                int convertedIndex = int.Parse(itemToChangeCount.Id.Substring(itemToChangeCount.Id.Length - 2));
                
                if(convertedIndex > 16)
                {
                    goodSummonIdList.Add(itemToChangeCount.Id);
                    InterSceneManager.Instance.GoodSummonCount ++;
                }
            }
            else
            {
                newList.Add(false);
            }
            itemToChangeCount.Count ++;
        }

        // Add exp to user's class gacha level
        ServerNetworkManager.Instance.User.ClassGachaExp += amount;
        if(ServerNetworkManager.Instance.User.ClassGachaLevel < ServerNetworkManager.Instance.Setting.MaxSummonLevel && int.Parse(BalanceInfoManager.Instance.SummonInfoList[ServerNetworkManager.Instance.User.ClassGachaLevel].SummonExp) <= ServerNetworkManager.Instance.User.ClassGachaExp)
        {
            ServerNetworkManager.Instance.User.ClassGachaExp -= int.Parse(BalanceInfoManager.Instance.SummonInfoList[ServerNetworkManager.Instance.User.ClassGachaLevel].SummonExp);
            ServerNetworkManager.Instance.User.ClassGachaLevel ++;
        }

        return (true, summonedItems, newList, goodSummonIdList);
    }

    public (bool isEnoughGem, List<string> summonItemIdList, List<bool> newInfoList, List<string> goodSummonIdList) SummonMercenary(int amount)
    {
        var requiredGem = (amount == 1)? ServerNetworkManager.Instance.Setting.MercenarySummonPrice : amount / 11 * (ServerNetworkManager.Instance.Setting.MercenarySummonPrice * 10);
        var isEnoughGem = (ServerNetworkManager.Instance.TotalGem >= requiredGem);
    
        // Check and use gems
        if(!isEnoughGem)
            return (false, new List<string>(), new List<bool>(), new List<string>());
        ServerNetworkManager.Instance.UnsyncedGem -= requiredGem;
        ServerNetworkManager.Instance.User.UnsyncedGemChangeCount ++;

        var summonTable = this.MercenarySummonTable;

        // Generate list of items summoned
        var summonedItems = new List<string>();
        for(int i = 0; i < amount; i ++)
        {
            var summonResult = Random.Range(0f, 1f);
            
            int j = 0;
            for (; j < summonTable.Count; j ++)
            {
                summonResult = summonResult - summonTable[j];
                if(summonResult <= 0f)
                    break;
            }
            summonedItems.Add(BalanceInfoManager.Instance.MercenaryInfoList[j + MinimumIndexOfMercenarySummon].Id);
        }

        // Generate list of new or not infomation
        var newList = new List<bool>();
        
        // New good summon list
        var goodSummonIdList = new List<string>();

        // Add those items to the inventory
        foreach(var tmp in summonedItems)
        {
            MercenaryModel itemToChangeCount = ServerNetworkManager.Instance.Inventory.MercenaryList.FirstOrDefault(n => n.Id == tmp);
            if(itemToChangeCount == null)
            {
                itemToChangeCount = new MercenaryModel();
                itemToChangeCount.Id = tmp;
                itemToChangeCount.Count = 0;
                itemToChangeCount.Level = 1;
                itemToChangeCount.IsNew = true;
                ServerNetworkManager.Instance.Inventory.MercenaryList.Add(itemToChangeCount);
                newList.Add(true);

                // Check item id for announce
                int convertedIndex = int.Parse(itemToChangeCount.Id.Substring(itemToChangeCount.Id.Length - 2));
                
                if(convertedIndex > 11)
                {
                    goodSummonIdList.Add(itemToChangeCount.Id);
                    InterSceneManager.Instance.GoodSummonCount ++;
                }
            }
            else
            {
                newList.Add(false);
            }
            itemToChangeCount.Count ++;
        }

        // Add exp to user's mercenary gacha level
        ServerNetworkManager.Instance.User.MercenaryGachaExp += amount;
        if(ServerNetworkManager.Instance.User.MercenaryGachaLevel < ServerNetworkManager.Instance.Setting.MaxMercenarySummonLevel && int.Parse(BalanceInfoManager.Instance.SummonInfoList[ServerNetworkManager.Instance.User.MercenaryGachaLevel].SummonExp) <= ServerNetworkManager.Instance.User.MercenaryGachaExp)
        {
            ServerNetworkManager.Instance.User.MercenaryGachaExp -= int.Parse(BalanceInfoManager.Instance.SummonInfoList[ServerNetworkManager.Instance.User.MercenaryGachaLevel].SummonExp);
            ServerNetworkManager.Instance.User.MercenaryGachaLevel ++;
        }

        return (true, summonedItems, newList, goodSummonIdList);
    }

    public (bool isEnoughGem, List<string> summonItemIdList, List<bool> newInfoList, List<string> goodSummonIdList) SummonRelic(int amount)
    {
        var requiredGem = (amount == 1)? 100 : amount / 11 * 1000;
        var isEnoughGem = (ServerNetworkManager.Instance.TotalGem >= requiredGem);
    
        // Check and use gems
        if(!isEnoughGem)
            return (false, new List<string>(), new List<bool>(), new List<string>());
        ServerNetworkManager.Instance.UnsyncedGem -= requiredGem;
        ServerNetworkManager.Instance.User.UnsyncedGemChangeCount ++;

        var summonTable = this.RelicSummonTable;

        // Generate list of items summoned
        var summonedItems = new List<string>();
        for(int i = 0; i < amount; i ++)
        {
            var summonResult = Random.Range(0f, 1f);
            
            int j = 0;
            for (; j < summonTable.Count; j ++)
            {
                summonResult = summonResult - summonTable[j];
                if(summonResult <= 0f)
                    break;
            }
            summonedItems.Add(BalanceInfoManager.Instance.RelicInfoList[j].Id);
        }

        // Generate list of new or not infomation
        var newList = new List<bool>();

        // Add those items to the inventory
        foreach(var tmp in summonedItems)
        {
            RelicModel itemToChangeCount = ServerNetworkManager.Instance.Inventory.RelicList.FirstOrDefault(n => n.Id == tmp);
            if(itemToChangeCount == null)
            {
                itemToChangeCount = new RelicModel();
                itemToChangeCount.Id = tmp;
                itemToChangeCount.Count = 0;
                itemToChangeCount.Level = 1;
                itemToChangeCount.IsNew = true;
                itemToChangeCount.RelicType = (RelicType)(int.Parse(tmp.Substring(tmp.Length - 2)) - 1);
                ServerNetworkManager.Instance.Inventory.RelicList.Add(itemToChangeCount);
                newList.Add(true);
            }
            else
            {
                newList.Add(false);
            }
            itemToChangeCount.Count ++;
        }

        return (true, summonedItems, newList, new List<string>());
    }

    public (bool isEnoughGem, List<string> summonItemIdList, List<bool> newInfoList, List<string> goodSummonIdList) SummonLegendaryRelic(int amount)
    {
        var requiredGem = (amount == 1)? 200 : amount / 11 * 2000;
        var isEnoughGem = (ServerNetworkManager.Instance.TotalGem >= requiredGem);
    
        // Check and use gems
        if(!isEnoughGem)
            return (false, new List<string>(), new List<bool>(), new List<string>());
        ServerNetworkManager.Instance.UnsyncedGem -= requiredGem;
        ServerNetworkManager.Instance.User.UnsyncedGemChangeCount ++;

        var summonTable = this.LegendaryRelicSummonTable;

        // Generate list of items summoned
        var summonedItems = new List<string>();
        for(int i = 0; i < amount; i ++)
        {
            var summonResult = Random.Range(0f, 1f);
            
            int j = 0;
            for (; j < summonTable.Count; j ++)
            {
                summonResult = summonResult - summonTable[j];
                if(summonResult <= 0f)
                    break;
            }
            summonedItems.Add(this.PossibleLegendaryRelicIds[j]);
        }

        // Generate list of new or not infomation
        var newList = new List<bool>();

        // Add those items to the inventory
        foreach(var tmp in summonedItems)
        {
            LegendaryRelicModel itemToChangeCount = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.Id == tmp);
            if(itemToChangeCount == null)
            {
                itemToChangeCount = new LegendaryRelicModel();
                itemToChangeCount.Id = tmp;
                itemToChangeCount.Count = 0;
                itemToChangeCount.Level = 1;
                itemToChangeCount.IsNew = true;
                itemToChangeCount.LegendaryRelicType = (LegendaryRelicType)(int.Parse(tmp.Substring(tmp.Length - 2)) - 1);
                ServerNetworkManager.Instance.Inventory.LegendaryRelicList.Add(itemToChangeCount);
                newList.Add(true);
            }
            else
            {
                newList.Add(false);
            }
            itemToChangeCount.Count ++;
        }

        return (true, summonedItems, newList, new List<string>());
    }

    public (bool isEnoughGem, List<string> summonItemIdList, List<bool> newInfoList, List<string> goodSummonIdList) SummonPet(int amount)
    {
        var requiredGem = (amount == 1)? 30 : amount / 11 * 300;
        var isEnoughGem = (ServerNetworkManager.Instance.TotalGem >= requiredGem);
    
        // Check and use gems
        if(!isEnoughGem)
            return (false, new List<string>(), new List<bool>(), new List<string>());
        ServerNetworkManager.Instance.UnsyncedGem -= requiredGem;
        ServerNetworkManager.Instance.User.UnsyncedGemChangeCount ++;

        var summonTable = this.PetSummonTable;

        // Generate list of items summoned
        var summonedItems = new List<string>();
        for(int i = 0; i < amount; i ++)
        {
            var summonResult = Random.Range(0f, 1f);
            
            int j = 0;
            for (; j < summonTable.Count; j ++)
            {
                summonResult = summonResult - summonTable[j];
                if(summonResult <= 0f)
                    break;
            }
            summonedItems.Add(BalanceInfoManager.Instance.PetInfoList[j + MinimumIndexOfPetSummon].Id);
        }

        // Generate list of new or not infomation
        var newList = new List<bool>();

        // New good summon list
        var goodSummonIdList = new List<string>();

        // Add those items to the inventory
        foreach(var tmp in summonedItems)
        {
            PetModel itemToChangeCount = ServerNetworkManager.Instance.Inventory.PetList.FirstOrDefault(n => n.Id == tmp);
            if(itemToChangeCount == null)
            {
                itemToChangeCount = new PetModel();
                itemToChangeCount.Id = tmp;
                itemToChangeCount.Count = 0;
                itemToChangeCount.Level = 1;
                itemToChangeCount.IsNew = true;
                itemToChangeCount.GoldLevelUpCount = 0;
                itemToChangeCount.HeartLevelUpCount = 0;
                itemToChangeCount.PetLevelUpCount = 0;
                ServerNetworkManager.Instance.Inventory.PetList.Add(itemToChangeCount);
                newList.Add(true);

                // Check item id for announce
                int convertedIndex = int.Parse(itemToChangeCount.Id.Substring(itemToChangeCount.Id.Length - 2));
                
                if(convertedIndex > 8)
                {
                    goodSummonIdList.Add(itemToChangeCount.Id);
                    InterSceneManager.Instance.GoodSummonCount ++;
                }
            }
            else
            {
                newList.Add(false);
            }
            itemToChangeCount.Count ++;
        }

        // Add exp to user's pet gacha level
        ServerNetworkManager.Instance.User.PetGachaExp += amount;
        if(ServerNetworkManager.Instance.User.PetGachaLevel < 10 && int.Parse(BalanceInfoManager.Instance.SummonInfoList[ServerNetworkManager.Instance.User.PetGachaLevel].SummonExp) <= ServerNetworkManager.Instance.User.PetGachaExp)
        {
            ServerNetworkManager.Instance.User.PetGachaExp -= int.Parse(BalanceInfoManager.Instance.SummonInfoList[ServerNetworkManager.Instance.User.PetGachaLevel].SummonExp);
            ServerNetworkManager.Instance.User.PetGachaLevel ++;
        }

        return (true, summonedItems, newList, goodSummonIdList);
    }

}