﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using System;
using System.Globalization;
using UnityEngine;
using Sirenix.OdinInspector;

using DG.Tweening;

using CodeStage.AntiCheat.ObscuredTypes;

/*
 */
/*
 */
/*
 */
public class IngameRaidController : MonoBehaviour
{
    public static IngameRaidController Instance;

    [Title("Current Values")]
    public BigInteger HeroDamage = 0;
    public BigInteger TeamDamage = 0;
    public BigInteger BossMaxHp{
        get{
            if(RaidManager.Instance.CurrentFloor < 10)
                return (BigInteger)Math.Pow(10d, 43d) * (BigInteger)Math.Pow(5d, RaidManager.Instance.CurrentFloor);
            else
                return (BigInteger)Math.Pow(10d, 43d) * (BigInteger)Math.Pow(5d, 10) * (BigInteger)Math.Pow(11d, RaidManager.Instance.CurrentFloor - 10);
        }
    }

    public ObscuredFloat PassedTime = 0f;
    public int LeftTime{
        get{
            return (int)(30f - PassedTime);
        }
    }

    private bool isPlaying = false;
    public bool IsPlaying{ 
        get{
            return isPlaying;
        }
    }
    private bool isPlayingBefore = false;

    public Dictionary<string, BigInteger> ScoreBoard = new Dictionary<string, BigInteger>();

    public Dictionary<string, UnityEngine.Vector3> Positions = new Dictionary<string, UnityEngine.Vector3>();

    private Stack<UIPopup> opendPopupStack = new Stack<UIPopup>();

    [Title("Spawn References")]

    [SerializeField]
    private List<Transform> spawnPositionList = new List<Transform>();
    public List<Transform> SpawnPositionList{
        get{
            return spawnPositionList;
        }
    }

    [Title("Enitity References")]

    [SerializeField]
    private HeroIdentity heroIdentity;

    public HeroIdentity HeroIdentity{
        get{
            return heroIdentity;
        }
    }

    [SerializeField]
    private List<HeroIdentity> allyHeroList = new List<HeroIdentity>();
    public List<HeroIdentity> AllyHeroList{
        get{
            return allyHeroList;
        }
    }

    [SerializeField]
    private List<string> allyHeroIdList = new List<string>();
    public List<string> AllyHeroIdList{
        get{
            return allyHeroIdList;
        }
    }

    [SerializeField]
    private RaidBossAnimation raidBossAnimation;

    [Title("UI References")]

    [SerializeField]
    private UIViewFloatingHUD uiViewFloatingHUD;

    [SerializeField]
    private UIViewRaid uiViewRaid;

    [SerializeField]
    private UIViewFade uiViewFade;

    [SerializeField]
    private UIPopupChat uiPopupChat;

    [SerializeField]
    private UIPopupToast uiPopupToast;

    [SerializeField]
    private UltimateJoystick ultimateJoystick;

    [Title("Camera and Effects")]
    [SerializeField]
    private UnityEngine.Rendering.PostProcessing.PostProcessVolume postEffectVolume;

    [Title("Others")]
    
    [SerializeField]
    private UnityEngine.Vector3 inputDirection = UnityEngine.Vector3.zero;
    public UnityEngine.Vector3 InputDirection{
        get{
            return inputDirection;
        }
    }

    private void Awake()
    {
		IngameRaidController.Instance = this;
        UIManager.Instance.ResetInputValues();
	}

    float passed = 0f;
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape) && !isPlaying && !isPlayingBefore)
		{
            if(!UIManager.Instance.IsCanvasShowing("Loading Canvas") && !UIManager.Instance.IsCanvasShowing("Fade Canvas"))
            {
                if(GameObject.FindObjectsOfType<UIPopupAlert>().Count() == 0)
                {
                    TryQuitIngame();
                }
                else
                {
                    GameObject.FindObjectOfType<UIPopupAlert>().Close();
                }
            }
        }

        if(heroIdentity.IsAttacking)
        {
            passed = 0.15f;
            return;
        }

        if(Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.S))
        {
            inputDirection = UnityEngine.Vector3.zero;
            if(Input.GetKey(KeyCode.D))
                inputDirection += UnityEngine.Vector3.right;
            if(Input.GetKey(KeyCode.A))
                inputDirection += UnityEngine.Vector3.left;
            if(Input.GetKey(KeyCode.W))
                inputDirection += UnityEngine.Vector3.forward;
            if(Input.GetKey(KeyCode.S))
                inputDirection -= UnityEngine.Vector3.forward;
            inputDirection = UnityEngine.Vector3.Normalize(inputDirection);
        }

        if(ultimateJoystick.GetVerticalAxis() != 0f || ultimateJoystick.GetHorizontalAxis() != 0f)
        {
            inputDirection = UnityEngine.Vector3.zero;
            inputDirection += UnityEngine.Vector3.forward * ultimateJoystick.GetVerticalAxis();
            inputDirection += UnityEngine.Vector3.right * ultimateJoystick.GetHorizontalAxis();
            inputDirection = UnityEngine.Vector3.Normalize(inputDirection);
        }
    
        if(passed >= 0.15f)
        {
            // Input related
            if(inputDirection != UnityEngine.Vector3.zero)
            {
                heroIdentity.MoveWithDirection(Camera.main.transform.TransformDirection(inputDirection));
                inputDirection = UnityEngine.Vector3.zero;
            }
            passed = 0;

            // Check for visuals
            postEffectVolume.enabled = OptionManager.Instance.IsPostEffectOn;
        }
        passed += Time.deltaTime;

    }

    private void Start()
    {
        uiViewFade.FadeIn();
        
        // Build name bars
        uiViewFloatingHUD.AddNameBar(heroIdentity);
        uiViewFloatingHUD.ResetNameBar(heroIdentity, ServerNetworkManager.Instance.User.NickName);
        foreach(var tmp in allyHeroList)
        {
            uiViewFloatingHUD.AddNameBar(tmp);
        }

        // Check allies and build them if needed
        RefreshAllyBeforeRaidStart();

        NumberRaidController.Instance.RefreshHero();
        heroIdentity.Reset(true, true, ServerNetworkManager.Instance.Inventory.SelectedClass.Id, ServerNetworkManager.Instance.Inventory.SelectedWeapon.Id, ServerNetworkManager.Instance.Inventory.WingIndex, false, true);

        // Refresh raid UI to show data as battle started
        uiViewRaid.Refresh();

        // Show idle animation of boss
        raidBossAnimation.PlayIdleAnimation();
        
        // Just open chat
        uiPopupChat.Open();

        // Need to check raid start
        StartCoroutine(CheckRaidStartCoroutine());
    }

    private IEnumerator CheckRaidStartCoroutine()
    {   
        while(!isPlaying)
        {
            // Always need to check floors
            CheckFloor();

            // Allow move of my hero move
            CheckMove();
            CheckPositionSync();

            if(ChatManager.Instance.ChatLogScore.Length > 0)
            {
                // If any score log exists, this means battle already started somehow
                StartRaid();
            }
            else
            {
                RefreshAllyBeforeRaidStart();

                // Check whether the host left or not
                if(ChatManager.Instance.PlayersInPrivateRoom.Count > 0 && !ChatManager.Instance.PlayersInPrivateRoom.ContainsKey(RaidManager.Instance.CurrentHostPlayerId))
                {
                    if(!isQuiting)
                        StartCoroutine(QuitIngameCoroutine(true));
                }
            }

            yield return -1;
        }
    }

    public void RefreshAllyBeforeRaidStart()
    {
        bool changed = false;

        // Need to add new players
        foreach(var tmp in ChatManager.Instance.PlayersInPrivateRoom.Take(Mathf.Min(ChatManager.Instance.PlayersInPrivateRoom.Count, 12)))
        {
            if(!ScoreBoard.ContainsKey(tmp.Key))
            {
                ScoreBoard.Add(tmp.Key, 0);
                changed = true;
            }
        }

        // Need to remove left players
        var toRemove = new List<string>();
        foreach(var tmp in ScoreBoard)
        {
            if(!ChatManager.Instance.PlayersInPrivateRoom.ContainsKey(tmp.Key))
            {
                toRemove.Add(tmp.Key);
                changed = true;
            }
        }
        foreach(var tmp in toRemove)
            ScoreBoard.Remove(tmp);

        // Logics should be done when changes made to the scoreboard
        if(changed)
        {
            uiViewRaid.RefreshScoreBoard();
            BuildAllyHeroes();
        }

        // Get position of ally heroes
        CheckRaidPositions();

        // Try to move ally heroes
        if(changed)
        {
            for(int i = 0; i < allyHeroIdList.Count; i++)
            {
                if(i < allyHeroList.Count)
                {
                    if(Positions.ContainsKey(allyHeroIdList[i]) && Positions[allyHeroIdList[i]] != UnityEngine.Vector3.zero)
                        allyHeroList[i].MoveImmediately(Positions[allyHeroIdList[i]]);
                }
            }
        }
        else
        {
            // Just move others
            for(int i = 0; i < allyHeroIdList.Count; i++)
            {
                if(i < allyHeroList.Count)
                {
                    if(Positions.ContainsKey(allyHeroIdList[i]) && Positions[allyHeroIdList[i]] != UnityEngine.Vector3.zero)
                        allyHeroList[i].MoveToDestination(Positions[allyHeroIdList[i]]);
                }
            }
        }
        
    }

    public void StartRaid()
    {
        if(!isPlayingBefore)
        {
            // If local player is the host, then need to call start raid
            if(RaidManager.Instance.CurrentHostPlayerId == ServerNetworkManager.Instance.User.PlayFabId)
                RaidManager.Instance.StartRaid();

            // Run battle logic
		    StartCoroutine(BattleLogicCoroutine());

            isPlayingBefore = true;
        }
    }

    private IEnumerator BattleLogicCoroutine()
    {
        isPlaying = true;

        // Refresh raid UI to show data as battle started
        uiViewRaid.Refresh();

        // Show attack animation of boss
        raidBossAnimation.PlayAttackAnimation();

        // Final update of ally heroes
        BuildAllyHeroes();

        // Save the count of players
        InterSceneManager.Instance.TotalRaidPlayMemberCount = ChatManager.Instance.PlayersInPrivateRoom.Count;

        uiPopupToast.ShowToast(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "arena_start") + "... 3", false);
        yield return new WaitForSeconds(1f);

        uiPopupToast.ShowToast(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "arena_start") + "... 2", false);
        yield return new WaitForSeconds(1f);

        uiPopupToast.ShowToast(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "arena_start") + "... 1", false);
        yield return new WaitForSeconds(1f);

        while(isPlaying)
		{
            // Always need to check floors
            CheckFloor();

            CheckTime();
            
            CheckMove();
            CheckAutoBehavior();

            CheckPositionSync();
            CheckRaidPositions();

            CheckDamageSync();
            CheckRaidScores();
            
            CheckValueChanges();

            uiViewRaid.RefreshSkillCooldown();

            // Try to play animation if damage is not zero
            for(int i = 0; i < allyHeroIdList.Count; i++)
            {
                if(Positions.ContainsKey(allyHeroIdList[i]) && UnityEngine.Vector3.Distance(Positions[allyHeroIdList[i]], allyHeroList[i].transform.position) > 3f)
                    allyHeroList[i].MoveToDestination(Positions[allyHeroIdList[i]]);
                else if(ScoreBoard.ContainsKey(allyHeroIdList[i]) && ScoreBoard[allyHeroIdList[i]] != 0)
                    allyHeroList[i].FakeAttack();
            } 

			yield return new WaitForFixedUpdate();
		}

        // Need to wait for score sending until my final attack is completed
        while(heroIdentity.IsAttacking)
            yield return -1;

        // Last sync of damage
        RaidManager.Instance.SyncScore(HeroDamage);

        // Wait for 3.5 seconds in case of time difference exists
        yield return new WaitForSeconds(3.5f);

        // Final check of ally heroes' changes
        CheckRaidPositions();

        CheckRaidScores();
            
        CheckValueChanges();

        if(IngameRaidController.Instance.HeroDamage + IngameRaidController.Instance.TeamDamage >= BossMaxHp)
        {
            InterSceneManager.Instance.DidRaidWin = true;

            // Save my raid rank
            var sortedScoreBoard = ScoreBoard.OrderByDescending(n => n.Value).ToList();
            int myRank = 0;
            
            for(int i = 0; i < sortedScoreBoard.Count; i ++)
            {
                myRank = myRank + 1;
                
                if(sortedScoreBoard[i].Key == ServerNetworkManager.Instance.User.PlayFabId)
                    break;
            }

            InterSceneManager.Instance.MyRaidRank = myRank;

            // Show die animation of boss
            raidBossAnimation.PlayDieAnimation();
        }
        else
        {
            InterSceneManager.Instance.DidRaidWin = false;

            // Show die animation of boss
            raidBossAnimation.PlayIdleAnimation();
        }

        // Show boss alive or die animation
        yield return new WaitForSeconds(1.5f);

        uiViewFade.FadeOut();
        yield return new WaitForSeconds(0.75f);

        SceneChangeManager.Instance.LoadMainGame();
    }

    private int previousFloor = -1;
    private void CheckFloor()
    {
        if(previousFloor != RaidManager.Instance.CurrentFloor)
        {
            uiViewRaid.RefreshFloor();
            uiViewRaid.RefreshGauge();
        }
        previousFloor = RaidManager.Instance.CurrentFloor;
    }

    private void CheckTime()
    {
        if(!isPlaying)
            return;

        if(PassedTime > 30f)
        {
            FinishGame();
        }
        else
        {
            PassedTime += Time.fixedDeltaTime;
        }
    }

    private void CheckBossKill()
    {
        if(!isPlaying)
            return;

        if(IngameRaidController.Instance.HeroDamage + IngameRaidController.Instance.TeamDamage >= BossMaxHp)
            FinishGame(); 
    }

    private void FinishGame()
    {
        if(!isPlaying)
            return;

        isPlaying = false;
    }

    private void CheckMove()
    {
        // Only move is asked to be done
        if(!UIManager.Instance.IsTouchedForMoveRecently)
            return;

        if(heroIdentity.IsAttacking)
            return;
        
        heroIdentity.MoveToDestination(UIManager.Instance.MoveDestination);

        UIManager.Instance.IsTouchedForMoveRecently = false;
    }

    private void CheckAutoBehavior()
    {
        // In raid, always auto on
        // When the hero is doing nothing
        if(!heroIdentity.IsAttacking && !heroIdentity.IsRunning)
        {
            heroIdentity.AttackRaidBossAfterMoveToRaidBoss();
        }
    }

    private float damageSyncPassed = 0f;
    private void CheckDamageSync()
    {
        damageSyncPassed += Time.fixedDeltaTime;
        if(IngameRaidController.Instance.HeroDamage != 0)
        {
            if(damageSyncPassed >= 2.5f)
            {
                RaidManager.Instance.SyncScore(HeroDamage);
                damageSyncPassed = 0f;
            }
        }
    }

    private float positionSyncPassed = 0f;
    private UnityEngine.Vector3 previousPosition = UnityEngine.Vector3.zero;
    private void CheckPositionSync()
    {
        positionSyncPassed += Time.fixedDeltaTime;
        if(positionSyncPassed >= 2.5f)
        {
            if(previousPosition != heroIdentity.transform.position)
            {
                RaidManager.Instance.SyncPosition(heroIdentity.transform.position);
            }
            previousPosition = heroIdentity.transform.position;
            positionSyncPassed = 0f;
        }
    }

    private BigInteger previousHeroDamage = -1;
    private BigInteger previousTeamDamage = -1;
    private int previousLeftTime = -1;
    private void CheckValueChanges()
    {
        if(previousHeroDamage != HeroDamage)
        {
            ScoreBoard[ServerNetworkManager.Instance.User.PlayFabId] = HeroDamage;
            uiViewRaid.RefreshScoreBoard();
        }

        if(previousHeroDamage != HeroDamage || previousTeamDamage != TeamDamage)
        {
            CheckBossKill();
            uiViewRaid.RefreshGauge();
        }
        
        previousHeroDamage = HeroDamage;
        previousTeamDamage = TeamDamage;

        if(previousLeftTime != LeftTime)
        {
            uiViewRaid.RefreshTime();
        }
        previousLeftTime = LeftTime;
    }

    private string previousScoreString = "";
    private void CheckRaidScores()
    {
        if(!previousScoreString.Equals(ChatManager.Instance.ChatLogScore))
        {   
            ScoreBoard.Clear();
            TeamDamage = 0;

            // Basically the scoreboard should be from the score chat room
            var list = new List<string>(ChatManager.Instance.ChatLogScore.Split('\n'));
            list.Reverse();
            foreach(var tmp in list)
            {
                if(tmp.Split('@').Length < 2)
                    continue;

                var id = tmp.Split('@')[0];
                var score = BigInteger.Parse(tmp.Split('@')[1], NumberStyles.Any, CultureInfo.InvariantCulture);
                
                if(id != ServerNetworkManager.Instance.User.PlayFabId)
                {
                    if(!ScoreBoard.ContainsKey(id))
                    {
                        TeamDamage += score;
                        ScoreBoard.Add(id, score);
                    }
                }
            }

            // Use the latest value of score for local player
            ScoreBoard[ServerNetworkManager.Instance.User.PlayFabId] = HeroDamage;

            foreach(var tmp in ChatManager.Instance.PlayersInPrivateRoom)
            {
                // In case for the players with no scores synced yet, just use 0 damage
                if(!ScoreBoard.ContainsKey(tmp.Key))
                    ScoreBoard.Add(tmp.Key, 0);
            }

            uiViewRaid.RefreshScoreBoard();

            previousScoreString = ChatManager.Instance.ChatLogScore;
        }        
    }

    private string previousPositionString = "";
    private void CheckRaidPositions()
    {
        if(!previousPositionString.Equals(ChatManager.Instance.ChatLogPosition))
        {   
            Positions.Clear();

            // Basically the Positions should be from the position chat room
            var list = new List<string>(ChatManager.Instance.ChatLogPosition.Split('\n'));
            list.Reverse();
            foreach(var tmp in list)
            {
                if(tmp.Split('@').Length < 2)
                    continue;

                var id = tmp.Split('@')[0];
                UnityEngine.Vector3 position = StringToVector3(tmp.Split('@')[1]);
                
                if(id != ServerNetworkManager.Instance.User.PlayFabId)
                {
                    if(!Positions.ContainsKey(id))
                    {
                        Positions.Add(id, position);
                    }
                }
            }

            previousPositionString = ChatManager.Instance.ChatLogPosition;
        }        
    }

    public void ToPreviousFloor()
    {
        if(RaidManager.Instance.CurrentFloor > 1)
            RaidManager.Instance.ChangeRaidFloor(RaidManager.Instance.CurrentFloor - 1);
    }

    public void ToNextFloor()
    {
        if(RaidManager.Instance.CurrentFloor < 199)
            RaidManager.Instance.ChangeRaidFloor(RaidManager.Instance.CurrentFloor + 1);
    }

    private void BuildAllyHeroes()
    {
        
        allyHeroIdList.Clear();

        // Build ally hero list
        var i = 0;
        foreach(var tmp in ChatManager.Instance.PlayersInPrivateRoom.Take(Mathf.Min(ChatManager.Instance.PlayersInPrivateRoom.Count, 12)))
        {
            if(tmp.Key != ServerNetworkManager.Instance.User.PlayFabId)
            {
                if(i < allyHeroList.Count)
                {
                    allyHeroIdList.Add(tmp.Key);

                    allyHeroList[i].gameObject.SetActive(true);
                    allyHeroList[i].Reset(
                        false, 
                        false, 
                        ChatManager.Instance.ClassesInPrivateRoom[tmp.Key], 
                        ChatManager.Instance.WeaponsInPrivateRoom[tmp.Key],
                        (ChatManager.Instance.WingIndicesInPrivateRoom.ContainsKey(tmp.Key))? ChatManager.Instance.WingIndicesInPrivateRoom[tmp.Key]: -1,
                        false,
                        true
                    );
                    uiViewFloatingHUD.ResetNameBar(allyHeroList[i], tmp.Value);
                    
                    i ++;
                }
            }
        }

        // Hide non-used heroes
        for(; i < allyHeroList.Count; i++)
        {
            allyHeroList[i].gameObject.SetActive(false);
            uiViewFloatingHUD.ResetNameBar(allyHeroList[i], "");
        }
    }

    public void TryQuitIngame()
    {
        if(!isPlaying && !isPlayingBefore)
        {
            UIManager.Instance.ShowAlertLocalized(
                "message_ingame_quit_check", 
                () =>{
                    if(!isPlaying && !isQuiting)
                    {
                        StartCoroutine(QuitIngameCoroutine(false));
                    }
                }, 
                () => {}
            );
        }
    }

    private bool isQuiting = false;
    private IEnumerator QuitIngameCoroutine(bool byHost = false)
    {
        isQuiting = true;
        uiViewFade.FadeOut();
        yield return new WaitForSeconds(0.75f);

        InterSceneManager.Instance.DidRaidDodge = true;
        InterSceneManager.Instance.DidRaidDodgeBecauseOfHost = byHost;
        SceneChangeManager.Instance.LoadMainGame();
    }

    public void ReadyForAttack()
    {
        UIManager.Instance.IsTouchedForAttackRecently = true;
    }

    public void ReadyForSkill(int skillIndex)
    {
        UIManager.Instance.IsTouchedForAttackRecently = true;
        UIManager.Instance.SelectedSkillIndex = skillIndex;
    }

    public void ReadyForMove(UnityEngine.Vector3 targetPosition)
    {
        UIManager.Instance.IsTouchedForMoveRecently = true;
        UIManager.Instance.MoveDestination = targetPosition;
    }

    public void StopMove()
    {
        UIManager.Instance.IsTouchedForMoveRecently = true;
        UIManager.Instance.MoveDestination = heroIdentity.transform.position;
    }

#region Utils

    public static UnityEngine.Vector3 StringToVector3(string sVector)
    {
        // Remove the parentheses
        if (sVector.StartsWith ("(") && sVector.EndsWith (")")) {
            sVector = sVector.Substring(1, sVector.Length-2);
        }

        // split the items
        string[] sArray = sVector.Split(',');

        // store as a Vector3
        var result = new UnityEngine.Vector3(
            float.Parse(sArray[0], NumberStyles.Any, CultureInfo.InvariantCulture),
            float.Parse(sArray[1], NumberStyles.Any, CultureInfo.InvariantCulture),
            float.Parse(sArray[2], NumberStyles.Any, CultureInfo.InvariantCulture));

        return result;
    }

#endregion

}