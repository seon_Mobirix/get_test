﻿using System.Numerics;
using System.Linq;
using UnityEngine;
using Sirenix.OdinInspector;

using CodeStage.AntiCheat.ObscuredTypes;

public class NumberRaidController : MonoBehaviour
{
    public static NumberRaidController Instance;

    public bool showLogs = false;

    private void Awake(){
		NumberRaidController.Instance = this;
        showLogs = false;
	}

#region Intermidiate Values For Display

    [Title("Intermidiate Values")]

    private BigInteger BaseAttackByUpgrade = 0;
    private ObscuredFloat BaseAttackMultiplierByWeapon = 0;
    private BigInteger BaseAttackMultiplierByRelic = 0;
    private BigInteger BaseAttackMultiplierByBuff = 0;
    private ObscuredFloat BaseAttackMultiplierByMercenary = 0;
    public ObscuredInt BaseAttackMultiplierByWing = 0;
    public ObscuredInt BaseAttackMultiplierByWingBody = 0;
    private BigInteger CriticalAttackPercentByUpgrade = 0;
    private BigInteger CriticalMultiplierByRelic = 0;
    private BigInteger SuperAttackPercentByUpgrade = 0;
    private BigInteger SuperAttackByRelic = 0;
    private BigInteger UltimateAttackPercentByUpgrade = 0;
    public ObscuredFloat UltimateAttackMultiplierByLegendaryRelic = 0;
    public BigInteger HyperAttackPercentByUpgrade = 0;
    private ObscuredFloat AttackSpeedMultiplierByClass = 1f;

#endregion

#region Final Values That Should Be Actually Used

    [Title("Final Values For Player")]

    public BigInteger FinalBaseAttackDamage = 0;
    public BigInteger FinalCriticalAttackDamage = 0;
    public ObscuredFloat FinalCriticalChance = 0f;
    public BigInteger FinalSuperAttackDamage = 0;
    public ObscuredFloat FinalSuperChance = 0f;
    public BigInteger FinalUltimateAttackDamage = 0;
    public ObscuredFloat FinalUltimateChance = 0f;
    public BigInteger FinalHyperAttackDamage = 0;
    public ObscuredFloat FinalHyperChance = 0f;

    public ObscuredFloat FinalAttackSpeed = 1f;

    public BigInteger DPA = 0;
    public BigInteger DPS = 0;

#endregion

    public void RefreshHero()
    {
        BaseAttackByUpgrade = ServerNetworkManager.Instance.User.Upgrade.BaseAttack;
        BaseAttackMultiplierByWeapon = ServerNetworkManager.Instance.Inventory.SelectedWeapon.BaseAttackMultiplier;
        var targetRelic = ServerNetworkManager.Instance.Inventory.ExtendedRelicList.FirstOrDefault(n => n.RelicType == RelicType.BASE_ATTACK);
        BaseAttackMultiplierByRelic = (targetRelic != null)? targetRelic.BaseAttackMultiplier : 1;
        BaseAttackMultiplierByBuff = BuffManager.Instance.GetAttackBuffValue(ServerNetworkManager.Instance.User.AttackBuffTier);
        
        if(ServerNetworkManager.Instance.Inventory.SelectedMercenary != null)
            BaseAttackMultiplierByMercenary = ServerNetworkManager.Instance.Inventory.SelectedMercenary.BaseAttackMultiplier;
        else
            BaseAttackMultiplierByMercenary = 1f;

        if(ServerNetworkManager.Instance.PurchasedWingList != null)
        {
            BaseAttackMultiplierByWing = 0;
            foreach(var wing in ServerNetworkManager.Instance.PurchasedWingList)
            {
                BaseAttackMultiplierByWing = BaseAttackMultiplierByWing + wing.OwnedStatBonus1Value;
            }
        }
        else
            BaseAttackMultiplierByWing = 0;

        if(ServerNetworkManager.Instance.WingBodyInfo != null)
            BaseAttackMultiplierByWingBody = ServerNetworkManager.Instance.WingBodyInfo.OwnedStatBonusValue;
        else
            BaseAttackMultiplierByWingBody = 1;

        BaseAttackMultiplierByWing = Mathf.Max(1, BaseAttackMultiplierByWing) * BaseAttackMultiplierByWingBody;

        FinalBaseAttackDamage = BaseAttackByUpgrade 
            * (BigInteger)(BaseAttackMultiplierByWeapon * 100f) / 100 
            * BaseAttackMultiplierByRelic * BaseAttackMultiplierByBuff 
            * (BigInteger)(BaseAttackMultiplierByMercenary * 100f) / 100
            * (int)BaseAttackMultiplierByWing;
        
        if(showLogs)
        {
            Debug.Log("BaseAttackByUpgrade: " + BaseAttackByUpgrade);
            Debug.Log("BaseAttackMultiplierByWeapon: " + BaseAttackMultiplierByWeapon);
            Debug.Log("BaseAttackMultiplierByRelic: " + BaseAttackMultiplierByRelic);
            Debug.Log("BaseAttackMultiplierByBuff: " + BaseAttackMultiplierByBuff);
            Debug.Log("BaseAttackMultiplierByMercenary: " + BaseAttackMultiplierByMercenary);
            Debug.Log("BaseAttackMultiplierByWing: " + BaseAttackMultiplierByWing);
            Debug.Log("FinalBaseAttackDamage: " + FinalBaseAttackDamage);
            Debug.Log("***************************************************");
        }

        CriticalAttackPercentByUpgrade = ServerNetworkManager.Instance.User.Upgrade.CriticalAttackPercent;
        targetRelic = ServerNetworkManager.Instance.Inventory.ExtendedRelicList.FirstOrDefault(n => n.RelicType == RelicType.CRITICAL_ATTACK);
        CriticalMultiplierByRelic = (targetRelic != null)? targetRelic.CriticalAttackMultiplier : 1;
        FinalCriticalAttackDamage = FinalBaseAttackDamage * CriticalAttackPercentByUpgrade / 100 * CriticalMultiplierByRelic;

        if(showLogs)
        {
            Debug.Log("CriticalAttackPercentByUpgrade: " + CriticalAttackPercentByUpgrade);
            Debug.Log("CriticalMultiplierByRelic: " + CriticalMultiplierByRelic);
            Debug.Log("FinalCriticalAttackDamage: " + FinalCriticalAttackDamage);
            Debug.Log("***************************************************");
        }

        FinalCriticalChance = ServerNetworkManager.Instance.User.Upgrade.CriticalChance;

        if(showLogs)
        {
            Debug.Log("FinalCriticalChance: " + FinalCriticalChance);
            Debug.Log("***************************************************");
        }

        SuperAttackPercentByUpgrade = ServerNetworkManager.Instance.User.Upgrade.SuperAttackPercent;
        targetRelic = ServerNetworkManager.Instance.Inventory.ExtendedRelicList.FirstOrDefault(n => n.RelicType == RelicType.SUPER_ATTACK);
        SuperAttackByRelic = (targetRelic != null)? targetRelic.SuperAttackMultiplier : 1;
        FinalSuperAttackDamage = FinalCriticalAttackDamage * SuperAttackPercentByUpgrade / 100 * SuperAttackByRelic;

        if(showLogs)
        {
            Debug.Log("SuperAttackPercentByUpgrade: " + SuperAttackPercentByUpgrade);
            Debug.Log("SuperAttackByRelic: " + SuperAttackByRelic);
            Debug.Log("FinalSuperAttackDamage: " + FinalSuperAttackDamage);
            Debug.Log("***************************************************");
        }

        FinalSuperChance = ServerNetworkManager.Instance.User.Upgrade.SuperChance;

        if(showLogs)
        {
            Debug.Log("FinalSuperChance: " + FinalSuperChance);
            Debug.Log("***************************************************");
        }

        UltimateAttackPercentByUpgrade = ServerNetworkManager.Instance.User.Upgrade.UltimateAttackPercent;
        var targetLegendaryRelic = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.LegendaryRelicType == LegendaryRelicType.ULTIMATE_ATTACK);
        UltimateAttackMultiplierByLegendaryRelic = (targetLegendaryRelic != null)? targetLegendaryRelic.UltimateAttackMultiplier : 1;
        FinalUltimateAttackDamage = FinalSuperAttackDamage * UltimateAttackPercentByUpgrade / 100 * (BigInteger)(UltimateAttackMultiplierByLegendaryRelic * 100f) / 100;

        if(showLogs)
        {
            Debug.Log("UltimateAttackPercentByUpgrade: " + UltimateAttackPercentByUpgrade);
            Debug.Log("UltimateAttackPercentByLegendaryRelic: " + UltimateAttackMultiplierByLegendaryRelic);
            Debug.Log("FinalUltimateAttackDamage: " + FinalUltimateAttackDamage);
            Debug.Log("***************************************************");
        }

        FinalUltimateChance = ServerNetworkManager.Instance.User.Upgrade.UltimateChance;

        if(showLogs)
        {
            Debug.Log("FinalUltimateChance: " + FinalUltimateChance);
            Debug.Log("***************************************************");
        }

        HyperAttackPercentByUpgrade = ServerNetworkManager.Instance.User.Upgrade.HyperAttackPercent;
        FinalHyperAttackDamage = FinalUltimateAttackDamage * HyperAttackPercentByUpgrade / 100;

        if(showLogs)
        {
            Debug.Log("HyperAttackPercentByUpgrade: " + HyperAttackPercentByUpgrade);
            Debug.Log("FinalHyperAttackDamage: " + FinalHyperAttackDamage);
            Debug.Log("***************************************************");
        }

        FinalHyperChance = ServerNetworkManager.Instance.User.Upgrade.HyperChance;

        if(showLogs)
        {
            Debug.Log("FinalHyperChance: " + FinalHyperChance);
            Debug.Log("***************************************************");
        }

        AttackSpeedMultiplierByClass = ServerNetworkManager.Instance.Inventory.SelectedClass.AttackSpeedMultiplier;
        FinalAttackSpeed = 1f * AttackSpeedMultiplierByClass;
        
        if(showLogs)
        {
            Debug.Log("AttackSpeedMultiplierByClass: " + AttackSpeedMultiplierByClass);
            Debug.Log("FinalAttackSpeed: " + FinalAttackSpeed);
            Debug.Log("***************************************************");
        }

        DPA = 0;
        DPA += FinalBaseAttackDamage * (BigInteger)((1f - FinalCriticalChance) * 100) / 100;
        DPA += FinalCriticalAttackDamage * (BigInteger)(FinalCriticalChance * (1f - FinalSuperChance) * 100) / 100;
        DPA += FinalSuperAttackDamage * (BigInteger)(FinalCriticalChance * FinalSuperChance * (1f - FinalUltimateChance) * 100) / 100;
        DPA += FinalUltimateAttackDamage * (BigInteger)(FinalCriticalChance * FinalSuperChance * FinalUltimateChance * (1f - FinalHyperChance) * 100) / 100;
        DPA += FinalHyperAttackDamage * (BigInteger)(FinalCriticalChance * FinalSuperChance * FinalUltimateChance * FinalHyperChance * 100) / 100;
        if(DPA < 1)
            DPA = 1;

        DPS = DPA * 2 * (BigInteger)(FinalAttackSpeed * BuffManager.Instance.GetSpeedBuffValue(ServerNetworkManager.Instance.User.SpeedBuffTier) * 100) / 100;
    }
}