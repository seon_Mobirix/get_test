﻿using UnityEngine;
using UnityEngine.Networking;

public class PageOpenController : MonoBehaviour {

	public static PageOpenController Instance;
	
	[SerializeField]
	private string cafeURL = "";

	[SerializeField]
	private string marketiOS = "";

	[SerializeField]
	private string marketAndroid = "https://play.google.com/store/apps/details?id=com.fireshrike.dk";

	[SerializeField]
	private string mobirixKorHelpURL = "https://mobirix.zendesk.com/hc/ko";

	[SerializeField]
	private string mobirixGlobalHelpURL = "https://mobirix.zendesk.com/hc/en-us";

	[SerializeField]
	private string helpEmailAddress = "support-play@fireshrike.com";

	[SerializeField]
	private string privacyPage = "http://gdprinfo.mobirix.net:33364/GdprServer/aosdesc.html";

	[SerializeField]
	private string privacyiOSPage = "http://gdprinfo.mobirix.net:33364/GdprServer/iosdesc.html";

	[SerializeField]
	private string mobirixRefundPolicyPage = "http://www.mobirix.com/refundkr.html";

	[SerializeField]
	private string mobirixFanPage = "https://www.facebook.com/mobirixplayen";


	private void Awake()
	{
		PageOpenController.Instance = this;
	}

	public void OpenCafe()
	{
		Debug.Log("PageOpen.OpenCafe");
		Application.OpenURL(cafeURL);
	}

	public void OpenMarketPage()
	{
		Debug.Log("PageOpen.OpenMarketPage");
		if(Application.platform == RuntimePlatform.IPhonePlayer)
		{
			Application.OpenURL(marketiOS);
		}
		else if(Application.platform == RuntimePlatform.Android)
		{
			Application.OpenURL(marketAndroid);
		}
	}

	public void OpenReview()
	{
		Debug.Log("PageOpen.OpenReviewPage");
		if(Application.platform == RuntimePlatform.IPhonePlayer)
		{
			iOSReviewRequest.Request();
		}
		else if(Application.platform == RuntimePlatform.Android)
		{
			Application.OpenURL(marketAndroid);
		}
	}

	public void OpenPrivacy()
	{
		if(Application.platform == RuntimePlatform.IPhonePlayer)
			Application.OpenURL(privacyiOSPage);
		else
			Application.OpenURL(privacyPage);
	}

	public void OpenHelp()
	{
		if(OptionManager.Instance.Language == OptionManager.LanguageModeType.ko)
			Application.OpenURL(mobirixKorHelpURL);
		else
			Application.OpenURL(mobirixGlobalHelpURL);
	}

	public void OpenRefundPolicyPage()
	{
		Debug.Log("PageOpen.OpenRefundPolicyPage");
		Application.OpenURL(mobirixRefundPolicyPage);
	}

	public void OpenFanPage()
	{
		Debug.Log("PageOpen.OpenFanPage");
		Application.OpenURL(mobirixFanPage);
	}
}