﻿using System.Numerics;
using System.Linq;
using UnityEngine;
using Sirenix.OdinInspector;

using CodeStage.AntiCheat.ObscuredTypes;

/**/
/**//**//**/
/**/
/**/
/**/
public class NumberArenaController : MonoBehaviour
{
    public static NumberArenaController Instance;

    public bool showLogs = false;

    private void Awake(){
		NumberArenaController.Instance = this;
        showLogs = false;
	}

#region Intermidiate Values For Display

    [Title("Intermidiate Values")]

    private BigInteger BaseAttackByUpgrade = 0;
    private ObscuredFloat BaseAttackMultiplierByWeapon = 0;
    private BigInteger BaseAttackMultiplierByRelic = 0;
    private BigInteger BaseAttackMultiplierByBuff = 0;
    private ObscuredFloat BaseAttackMultiplierByMercenary = 0;
    public ObscuredInt BaseAttackMultiplierByWing = 0;
    public ObscuredInt BaseAttackMultiplierByWingBody = 0;
    private BigInteger CriticalAttackPercentByUpgrade = 0;
    private BigInteger CriticalMultiplierByRelic = 0;
    private BigInteger SuperAttackPercentByUpgrade = 0;
    private BigInteger SuperAttackByRelic = 0;
    private BigInteger UltimateAttackPercentByUpgrade = 0;
    public ObscuredFloat UltimateAttackMultiplierByLegendaryRelic = 0;
    public BigInteger HyperAttackPercentByUpgrade = 0;
    private ObscuredFloat AttackSpeedMultiplierByClass = 1f;

    private BigInteger BaseAttackByUpgradeEnemy = 0;
    private ObscuredFloat BaseAttackMultiplierByWeaponEnemy = 0;
    private BigInteger BaseAttackMultiplierByRelicEnemy = 0;
    private BigInteger BaseAttackMultiplierByBuffEnemy = 0;
    private ObscuredFloat BaseAttackMultiplierByMercenaryEnemy = 0;
    private ObscuredInt BaseAttackMultiplierByWingEnemy = 0;
    private BigInteger CriticalAttackPercentByUpgradeEnemy = 0;
    private BigInteger CriticalMultiplierByRelicEnemy = 0;
    private BigInteger SuperAttackPercentByUpgradeEnemy = 0;
    private BigInteger SuperAttackByRelicEnemy = 0;
    private BigInteger UltimateAttackPercentByUpgradeEnemy = 0;
    public ObscuredFloat UltimateAttackMultiplierByLegendaryRelicEnemy = 0;
    public BigInteger HyperAttackPercentByUpgradeEnemy = 0;
    private ObscuredFloat AttackSpeedMultiplierByClassEnemy = 1f;

#endregion

#region Final Values That Should Be Actually Used

    [Title("Final Values For Player")]

    public BigInteger FinalBaseAttackDamage = 0;
    public BigInteger FinalCriticalAttackDamage = 0;
    public ObscuredFloat FinalCriticalChance = 0f;
    public BigInteger FinalSuperAttackDamage = 0;
    public ObscuredFloat FinalSuperChance = 0f;
    public BigInteger FinalUltimateAttackDamage = 0;
    public ObscuredFloat FinalUltimateChance = 0f;
    public BigInteger FinalHyperAttackDamage = 0;
    public ObscuredFloat FinalHyperChance = 0f;

    public ObscuredFloat FinalAttackSpeed = 1f;

    public BigInteger DPA = 0;
    public BigInteger DPS = 0;

    [Title("Final Values For Opponent")]

    public BigInteger FinalBaseAttackDamageEnemy = 0;
    public BigInteger FinalCriticalAttackDamageEnemy = 0;
    public ObscuredFloat FinalCriticalChanceEnemy = 0f;
    public BigInteger FinalSuperAttackDamageEnemy = 0;
    public ObscuredFloat FinalSuperChanceEnemy = 0f;
    public BigInteger FinalUltimateAttackDamageEnemy = 0;
    public ObscuredFloat FinalUltimateChanceEnemy = 0f;
    public BigInteger FinalHyperAttackDamageEnemy = 0;
    public ObscuredFloat FinalHyperChanceEnemy = 0f;

    public ObscuredFloat FinalAttackSpeedEnemy = 1f;

    public BigInteger DPAEnemy = 0;
    public BigInteger DPSEnemy = 0;

#endregion

    public void RefreshHero()
    {
        BaseAttackByUpgrade = ServerNetworkManager.Instance.User.Upgrade.BaseAttack;
        BaseAttackMultiplierByWeapon = ServerNetworkManager.Instance.Inventory.SelectedWeapon.BaseAttackMultiplier;
        var targetRelic = ServerNetworkManager.Instance.Inventory.ExtendedRelicList.FirstOrDefault(n => n.RelicType == RelicType.BASE_ATTACK);
        BaseAttackMultiplierByRelic = (targetRelic != null)? targetRelic.BaseAttackMultiplier : 1;
        BaseAttackMultiplierByBuff = BuffManager.Instance.GetAttackBuffValue(ServerNetworkManager.Instance.User.AttackBuffTier);

        if(ServerNetworkManager.Instance.Inventory.SelectedMercenary != null)
            BaseAttackMultiplierByMercenary = ServerNetworkManager.Instance.Inventory.SelectedMercenary.BaseAttackMultiplier;
        else
            BaseAttackMultiplierByMercenary = 1f;

        if(ServerNetworkManager.Instance.PurchasedWingList != null)
        {
            BaseAttackMultiplierByWing = 0;
            foreach(var wing in ServerNetworkManager.Instance.PurchasedWingList)
            {
                BaseAttackMultiplierByWing = BaseAttackMultiplierByWing + wing.OwnedStatBonus1Value;
            }
        }
        else
            BaseAttackMultiplierByWing = 0;

        if(ServerNetworkManager.Instance.WingBodyInfo != null)
            BaseAttackMultiplierByWingBody = ServerNetworkManager.Instance.WingBodyInfo.OwnedStatBonusValue;
        else
            BaseAttackMultiplierByWingBody = 1;

        BaseAttackMultiplierByWing = Mathf.Max(1, BaseAttackMultiplierByWing) * BaseAttackMultiplierByWingBody;

        FinalBaseAttackDamage = BaseAttackByUpgrade 
            * (BigInteger)(BaseAttackMultiplierByWeapon * 100f) / 100 
            * BaseAttackMultiplierByRelic * BaseAttackMultiplierByBuff 
            * (BigInteger)(BaseAttackMultiplierByMercenary * 100f) / 100
            * (int)BaseAttackMultiplierByWing;

        if(showLogs)
        {
            Debug.Log("BaseAttackByUpgrade: " + BaseAttackByUpgrade);
            Debug.Log("BaseAttackMultiplierByWeapon: " + BaseAttackMultiplierByWeapon);
            Debug.Log("BaseAttackMultiplierByRelic: " + BaseAttackMultiplierByRelic);
            Debug.Log("BaseAttackMultiplierByBuff: " + BaseAttackMultiplierByBuff);
            Debug.Log("BaseAttackMultiplierByMercenary: " + BaseAttackMultiplierByMercenary);
            Debug.Log("BaseAttackMultiplierByWing: " + BaseAttackMultiplierByWing);
            Debug.Log("FinalBaseAttackDamage: " + FinalBaseAttackDamage);
            Debug.Log("***************************************************");
        }

        CriticalAttackPercentByUpgrade = ServerNetworkManager.Instance.User.Upgrade.CriticalAttackPercent;
        targetRelic = ServerNetworkManager.Instance.Inventory.ExtendedRelicList.FirstOrDefault(n => n.RelicType == RelicType.CRITICAL_ATTACK);
        CriticalMultiplierByRelic = (targetRelic != null)? targetRelic.CriticalAttackMultiplier : 1;
        FinalCriticalAttackDamage = FinalBaseAttackDamage * CriticalAttackPercentByUpgrade / 100 * CriticalMultiplierByRelic;

        if(showLogs)
        {
            Debug.Log("CriticalAttackPercentByUpgrade: " + CriticalAttackPercentByUpgrade);
            Debug.Log("CriticalMultiplierByRelic: " + CriticalMultiplierByRelic);
            Debug.Log("FinalCriticalAttackDamage: " + FinalCriticalAttackDamage);
            Debug.Log("***************************************************");
        }

        FinalCriticalChance = ServerNetworkManager.Instance.User.Upgrade.CriticalChance;

        if(showLogs)
        {
            Debug.Log("FinalCriticalChance: " + FinalCriticalChance);
            Debug.Log("***************************************************");
        }

        SuperAttackPercentByUpgrade = ServerNetworkManager.Instance.User.Upgrade.SuperAttackPercent;
        targetRelic = ServerNetworkManager.Instance.Inventory.ExtendedRelicList.FirstOrDefault(n => n.RelicType == RelicType.SUPER_ATTACK);
        SuperAttackByRelic = (targetRelic != null)? targetRelic.SuperAttackMultiplier : 1;
        FinalSuperAttackDamage = FinalCriticalAttackDamage * SuperAttackPercentByUpgrade / 100 * SuperAttackByRelic;

        if(showLogs)
        {
            Debug.Log("SuperAttackPercentByUpgrade: " + SuperAttackPercentByUpgrade);
            Debug.Log("SuperAttackByRelic: " + SuperAttackByRelic);
            Debug.Log("FinalSuperAttackDamage: " + FinalSuperAttackDamage);
            Debug.Log("***************************************************");
        }

        FinalSuperChance = ServerNetworkManager.Instance.User.Upgrade.SuperChance;

        if(showLogs)
        {
            Debug.Log("FinalSuperChance: " + FinalSuperChance);
            Debug.Log("***************************************************");
        }

        UltimateAttackPercentByUpgrade = ServerNetworkManager.Instance.User.Upgrade.UltimateAttackPercent;
        var targetLegendaryRelic = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.LegendaryRelicType == LegendaryRelicType.ULTIMATE_ATTACK);
        UltimateAttackMultiplierByLegendaryRelic = (targetLegendaryRelic != null)? targetLegendaryRelic.UltimateAttackMultiplier : 1;
        FinalUltimateAttackDamage = FinalSuperAttackDamage * UltimateAttackPercentByUpgrade / 100 * (BigInteger)(UltimateAttackMultiplierByLegendaryRelic * 100f) / 100;

        if(showLogs)
        {
            Debug.Log("UltimateAttackPercentByUpgrade: " + UltimateAttackPercentByUpgrade);
            Debug.Log("UltimateAttackPercentByLegendaryRelic: " + UltimateAttackMultiplierByLegendaryRelic);
            Debug.Log("FinalUltimateAttackDamage: " + FinalUltimateAttackDamage);
            Debug.Log("***************************************************");
        }

        FinalUltimateChance = ServerNetworkManager.Instance.User.Upgrade.UltimateChance;

        if(showLogs)
        {
            Debug.Log("FinalUltimateChance: " + FinalUltimateChance);
            Debug.Log("***************************************************");
        }

        HyperAttackPercentByUpgrade = ServerNetworkManager.Instance.User.Upgrade.HyperAttackPercent;
        FinalHyperAttackDamage = FinalUltimateAttackDamage * HyperAttackPercentByUpgrade / 100;

        if(showLogs)
        {
            Debug.Log("HyperAttackPercentByUpgrade: " + HyperAttackPercentByUpgrade);
            Debug.Log("FinalHyperAttackDamage: " + FinalHyperAttackDamage);
            Debug.Log("***************************************************");
        }

        FinalHyperChance = ServerNetworkManager.Instance.User.Upgrade.HyperChance;

        if(showLogs)
        {
            Debug.Log("FinalHyperChance: " + FinalHyperChance);
            Debug.Log("***************************************************");
        }

        AttackSpeedMultiplierByClass = ServerNetworkManager.Instance.Inventory.SelectedClass.AttackSpeedMultiplier;
        FinalAttackSpeed = 1f * AttackSpeedMultiplierByClass;
        
        if(showLogs)
        {
            Debug.Log("AttackSpeedMultiplierByClass: " + AttackSpeedMultiplierByClass);
            Debug.Log("FinalAttackSpeed: " + FinalAttackSpeed);
            Debug.Log("***************************************************");
        }

        DPA = 0;
        DPA += FinalBaseAttackDamage * (BigInteger)((1f - FinalCriticalChance) * 100) / 100;
        DPA += FinalCriticalAttackDamage * (BigInteger)(FinalCriticalChance * (1f - FinalSuperChance) * 100) / 100;
        DPA += FinalSuperAttackDamage * (BigInteger)(FinalCriticalChance * FinalSuperChance * (1f - FinalUltimateChance) * 100) / 100;
        DPA += FinalUltimateAttackDamage * (BigInteger)(FinalCriticalChance * FinalSuperChance * FinalUltimateChance * (1f - FinalHyperChance) * 100) / 100;
        DPA += FinalHyperAttackDamage * (BigInteger)(FinalCriticalChance * FinalSuperChance * FinalUltimateChance * FinalHyperChance * 100) / 100;
        if(DPA < 1)
            DPA = 1;

        DPS = DPA * 2 * (BigInteger)(FinalAttackSpeed * BuffManager.Instance.GetSpeedBuffValue(ServerNetworkManager.Instance.User.SpeedBuffTier) * 100) / 100;
    }

    public void RefreshEnemy()
    {
        BaseAttackByUpgradeEnemy = InterSceneManager.Instance.OpponentUserData.Upgrade.BaseAttack;
        BaseAttackMultiplierByWeaponEnemy = InterSceneManager.Instance.OpponentInventoryData.SelectedWeapon.BaseAttackMultiplier;
        var targetRelic = InterSceneManager.Instance.OpponentInventoryData.ExtendedRelicList.FirstOrDefault(n => n.RelicType == RelicType.BASE_ATTACK);
        BaseAttackMultiplierByRelicEnemy = (targetRelic != null)? targetRelic.BaseAttackMultiplier : 1;
        BaseAttackMultiplierByBuffEnemy = BuffManager.Instance.GetAttackBuffValue(InterSceneManager.Instance.OpponentUserData.AttackBuffTier, false);

        if(InterSceneManager.Instance.OpponentInventoryData.SelectedMercenary != null)
            BaseAttackMultiplierByMercenaryEnemy = InterSceneManager.Instance.OpponentInventoryData.SelectedMercenary.BaseAttackMultiplier;
        else
            BaseAttackMultiplierByMercenaryEnemy = 1f;

        BaseAttackMultiplierByWingEnemy = Mathf.Max(1, InterSceneManager.Instance.OpponentUserData.TotalWingAttackBonus);    
        
        FinalBaseAttackDamageEnemy = BaseAttackByUpgradeEnemy 
            * (BigInteger)(BaseAttackMultiplierByWeaponEnemy * 100f) / 100 
            * BaseAttackMultiplierByRelicEnemy * BaseAttackMultiplierByBuffEnemy 
            * (BigInteger)(BaseAttackMultiplierByMercenaryEnemy * 100f) / 100
            * (int)BaseAttackMultiplierByWingEnemy;
        
        if(showLogs)
        {
            Debug.Log("BaseAttackByUpgradeEnemy: " + BaseAttackByUpgradeEnemy);
            Debug.Log("BaseAttackMultiplierByWeaponEnemy: " + BaseAttackMultiplierByWeaponEnemy);
            Debug.Log("BaseAttackMultiplierByRelicEnemy: " + BaseAttackMultiplierByRelicEnemy);
            Debug.Log("BaseAttackMultiplierByBuffEnemy: " + BaseAttackMultiplierByBuffEnemy);
            Debug.Log("BaseAttackMultiplierByMercenaryEnemy: " + BaseAttackMultiplierByMercenaryEnemy);
            Debug.Log("BaseAttackMultiplierByMercenaryEnemy: " + BaseAttackMultiplierByWingEnemy);
            Debug.Log("FinalBaseAttackDamageEnemy: " + FinalBaseAttackDamageEnemy);
            Debug.Log("***************************************************");
        }

        CriticalAttackPercentByUpgradeEnemy = InterSceneManager.Instance.OpponentUserData.Upgrade.CriticalAttackPercent;
        targetRelic = InterSceneManager.Instance.OpponentInventoryData.ExtendedRelicList.FirstOrDefault(n => n.RelicType == RelicType.CRITICAL_ATTACK);
        CriticalMultiplierByRelicEnemy = (targetRelic != null)? targetRelic.CriticalAttackMultiplier : 1;
        FinalCriticalAttackDamageEnemy = FinalBaseAttackDamageEnemy * CriticalAttackPercentByUpgradeEnemy / 100 * CriticalMultiplierByRelicEnemy;

        if(showLogs)
        {
            Debug.Log("CriticalAttackByUpgradeEnemy: " + CriticalAttackPercentByUpgradeEnemy);
            Debug.Log("CriticalMultiplierByRelicEnemy: " + CriticalMultiplierByRelicEnemy);
            Debug.Log("FinalCriticalAttackDamageEnemy: " + FinalCriticalAttackDamageEnemy);
            Debug.Log("***************************************************");
        }

        FinalCriticalChanceEnemy = InterSceneManager.Instance.OpponentUserData.Upgrade.CriticalChance;

        if(showLogs)
        {
            Debug.Log("FinalCriticalChanceEnemy: " + FinalCriticalChanceEnemy);
            Debug.Log("***************************************************");
        }

        SuperAttackPercentByUpgradeEnemy = InterSceneManager.Instance.OpponentUserData.Upgrade.SuperAttackPercent;
        targetRelic = InterSceneManager.Instance.OpponentInventoryData.ExtendedRelicList.FirstOrDefault(n => n.RelicType == RelicType.SUPER_ATTACK);
        SuperAttackByRelicEnemy = (targetRelic != null)? targetRelic.SuperAttackMultiplier : 1;
        FinalSuperAttackDamageEnemy = FinalCriticalAttackDamageEnemy * SuperAttackPercentByUpgradeEnemy / 100 * SuperAttackByRelicEnemy;

        if(showLogs)
        {
            Debug.Log("SuperAttackPercentByUpgradeEnemy: " + SuperAttackPercentByUpgradeEnemy);
            Debug.Log("SuperAttackByRelicEnemy: " + SuperAttackByRelicEnemy);
            Debug.Log("FinalSuperAttackDamageEnemy: " + FinalSuperAttackDamageEnemy);
            Debug.Log("***************************************************");
        }

        FinalSuperChanceEnemy = InterSceneManager.Instance.OpponentUserData.Upgrade.SuperChance;

        if(showLogs)
        {
            Debug.Log("FinalSuperChanceEnemy: " + FinalSuperChanceEnemy);
            Debug.Log("***************************************************");
        }

        UltimateAttackPercentByUpgradeEnemy = InterSceneManager.Instance.OpponentUserData.Upgrade.UltimateAttackPercent;
        var targetLegendaryRelic = InterSceneManager.Instance.OpponentInventoryData.LegendaryRelicList.FirstOrDefault(n => n.LegendaryRelicType == LegendaryRelicType.ULTIMATE_ATTACK);
        UltimateAttackMultiplierByLegendaryRelicEnemy = (targetLegendaryRelic != null)? targetLegendaryRelic.UltimateAttackMultiplier : 1;
        FinalUltimateAttackDamageEnemy = FinalSuperAttackDamageEnemy * UltimateAttackPercentByUpgradeEnemy / 100 * (BigInteger)(UltimateAttackMultiplierByLegendaryRelicEnemy * 100f) / 100;;

        if(showLogs)
        {
            Debug.Log("UltimateAttackPercentByUpgradeEnemy: " + UltimateAttackPercentByUpgradeEnemy);
            Debug.Log("UltimateAttackMultiplierByLegendaryRelicEnemy: " + UltimateAttackMultiplierByLegendaryRelicEnemy);
            Debug.Log("FinalUltimateAttackDamageEnemy: " + FinalUltimateAttackDamageEnemy);
            Debug.Log("***************************************************");
        }

        FinalUltimateChanceEnemy = InterSceneManager.Instance.OpponentUserData.Upgrade.UltimateChance;

        if(showLogs)
        {
            Debug.Log("FinalUltimateChanceEnemy: " + FinalUltimateChanceEnemy);
            Debug.Log("***************************************************");
        }

        HyperAttackPercentByUpgradeEnemy = InterSceneManager.Instance.OpponentUserData.Upgrade.HyperAttackPercent;
        FinalHyperAttackDamageEnemy = FinalUltimateAttackDamageEnemy * HyperAttackPercentByUpgradeEnemy / 100;

        if(showLogs)
        {
            Debug.Log("HyperAttackPercentByUpgradeEnemy: " + HyperAttackPercentByUpgradeEnemy);
            Debug.Log("FinalHyperAttackDamageEnemy: " + FinalHyperAttackDamageEnemy);
            Debug.Log("***************************************************");
        }

        FinalHyperChanceEnemy = InterSceneManager.Instance.OpponentUserData.Upgrade.HyperChance;

        if(showLogs)
        {
            Debug.Log("FinalHyperChanceEnemy: " + FinalHyperChanceEnemy);
            Debug.Log("***************************************************");
        }

        AttackSpeedMultiplierByClassEnemy = InterSceneManager.Instance.OpponentInventoryData.SelectedClass.AttackSpeedMultiplier;
        FinalAttackSpeedEnemy = 1f * AttackSpeedMultiplierByClassEnemy;
        
        if(showLogs)
        {
            Debug.Log("AttackSpeedMultiplierByClassEnemy: " + AttackSpeedMultiplierByClassEnemy);
            Debug.Log("FinalAttackSpeedEnemy: " + FinalAttackSpeedEnemy);
            Debug.Log("***************************************************");
        }

        DPAEnemy = 0;
        DPAEnemy += FinalBaseAttackDamageEnemy * (BigInteger)((1f - FinalCriticalChanceEnemy) * 100) / 100;
        DPAEnemy += FinalCriticalAttackDamageEnemy * (BigInteger)(FinalCriticalChanceEnemy * (1f - FinalSuperChanceEnemy) * 100) / 100;
        DPAEnemy += FinalSuperAttackDamageEnemy * (BigInteger)(FinalCriticalChanceEnemy * FinalSuperChanceEnemy * (1f - FinalUltimateChanceEnemy) * 100) / 100;
        DPAEnemy += FinalUltimateAttackDamageEnemy * (BigInteger)(FinalCriticalChanceEnemy * FinalSuperChanceEnemy * FinalUltimateChanceEnemy * (1f - FinalHyperChanceEnemy) * 100) / 100;
        DPAEnemy += FinalHyperAttackDamageEnemy * (BigInteger)(FinalCriticalChanceEnemy * FinalSuperChanceEnemy * FinalUltimateChanceEnemy * FinalHyperChanceEnemy * 100) / 100;
        if(DPAEnemy < 1)
            DPAEnemy = 1;

        DPSEnemy = DPAEnemy * 2 * (BigInteger)(FinalAttackSpeedEnemy * BuffManager.Instance.GetSpeedBuffValue(InterSceneManager.Instance.OpponentUserData.SpeedBuffTier, false) * 100) / 100;
    }

}