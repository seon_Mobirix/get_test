﻿using UnityEngine;
using Firebase.Analytics;

public class AnalyticsController : MonoBehaviour
{
    public static AnalyticsController Instance;

    private void Awake()
	{
		AnalyticsController.Instance = this;
	}

    public void SummonEvent(int count)
    {
        // Check max values
        count = Mathf.Min(count, 300);

        if(ServerNetworkManager.Instance.User.LastSummonLogCount < count)
        {
            if(count >= 10)
                SummonEventLog(10);
            if(count >= 30)
                SummonEventLog(30);
            if(count >= 50)
                SummonEventLog(50);
            if(count >= 70)
                SummonEventLog(70);
            if(count >= 100)
                SummonEventLog(100);
            if(count >= 150)
                SummonEventLog(150);
            if(count >= 200)
                SummonEventLog(200);
            if(count >= 250)
                SummonEventLog(250);
            if(count >= 300)
                SummonEventLog(300);
        }
    }

    public void AdRewardEvent(int count)
    {
        // Check owned basic package user
        if(ServerNetworkManager.Instance.PurchasedBasicPackageList.Count > 0)
            return;

        // Check max values
        count = Mathf.Min(count, 100);

        if(ServerNetworkManager.Instance.User.LastFreeGemRewardLogCount < count)
        {
            if(count >= 5)
                AdRewardEventLog(5);
            if(count >= 10)
                AdRewardEventLog(10);
            if(count >= 15)
                AdRewardEventLog(15);
            if(count >= 20)
                AdRewardEventLog(20);
            if(count >= 30)
                AdRewardEventLog(30);
            if(count >= 50)
                AdRewardEventLog(50);    
            if(count >= 70)
                AdRewardEventLog(70);
            if(count >= 100)
                AdRewardEventLog(100);

            // Need to log for facebook
            FacebookManager.Instance.LogEvent(FacebookLogType.REWARD_ADS);
        }
    }

    private void SummonEventLog(int value)
    {
        if(ServerNetworkManager.Instance.User.LastSummonLogCount < value)
        {
            FirebaseAnalytics.LogEvent($"Gacha_{value}");

            // Need to log for facebook
            if(value == 10)
                FacebookManager.Instance.LogEvent(FacebookLogType.SUMMON_10);
            else if(value == 50)
                FacebookManager.Instance.LogEvent(FacebookLogType.SUMMON_50);
            else if(value == 100)
                FacebookManager.Instance.LogEvent(FacebookLogType.SUMMON_100);

            ServerNetworkManager.Instance.User.LastSummonLogCount = value;
        }
    }

    private void AdRewardEventLog(int value)
    {
        if(ServerNetworkManager.Instance.User.LastFreeGemRewardLogCount < value)
        {
            FirebaseAnalytics.LogEvent($"Reward_{value}");
            
            ServerNetworkManager.Instance.User.LastFreeGemRewardLogCount = value;
        }
    }

    public void AdRewardValueLog(GoogleMobileAds.Api.AdValue adValue)
    {
        FirebaseAnalytics.LogEvent("paid_ad_impression", "valuemicros", adValue.Value);
    }
}
