﻿using UnityEngine;
using System.Collections;

using Sirenix.OdinInspector;

public class PerformanceManager : MonoBehaviour {

	private static PerformanceManager instance = null;
	public static PerformanceManager Instance
	{
		get{
			if(instance == null)
				instance = GameObject.FindObjectOfType<PerformanceManager>();
			return instance;
		}
	}

	private void Awake()
	{
		// Never sleep
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
	}

	public void AdjustFramerate()
	{
		if(OptionManager.Instance.TargetFrameRate != -1)
		{
			Application.targetFrameRate = OptionManager.Instance.TargetFrameRate;
		}
		else
		{
			// Do not have auto frame rate system anymore
			// Let players just use 60 frames per second
			Application.targetFrameRate = 60;
		}
	}

}
