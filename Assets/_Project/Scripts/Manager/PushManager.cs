﻿using Firebase;
using UnityEngine;

public class PushManager : MonoBehaviour
{
    private static PushManager instance = null;
	public static PushManager Instance
	{
		get{
			if(instance == null)
				instance = GameObject.FindObjectOfType<PushManager>();
			return instance;
		}
	}

    public FirebaseApp app = null;
    private bool isIOSTokenSent = false;

    public void Start()
    {

#if UNITY_ANDROID
        Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
        
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
        var dependencyStatus = task.Result;
        if (dependencyStatus == Firebase.DependencyStatus.Available) {
            // Create and hold a reference to your FirebaseApp,
            // where app is a Firebase.FirebaseApp property of your application class.
            app = Firebase.FirebaseApp.DefaultInstance;
            Debug.Log("PushManager: Firebase App initialization is successful.");
            // Set a flag here to indicate whether Firebase is ready to use by your app.
        } else {
            UnityEngine.Debug.LogError(System.String.Format(
            "PushManager: Could not resolve all Firebase dependencies: {0}", dependencyStatus));
            // Firebase Unity SDK is not safe to use here.
        }
        });
#elif UNITY_IPHONE
        Debug.Log("PushManager: Trying to register iOS notification.");
        UnityEngine.iOS.NotificationServices.RegisterForNotifications(UnityEngine.iOS.NotificationType.Alert | UnityEngine.iOS.NotificationType.Badge | UnityEngine.iOS.NotificationType.Sound, true);
#endif
    }

#if UNITY_IPHONE
    private void Update()
    {
        if(!isIOSTokenSent)
        {
            byte[] token = UnityEngine.iOS.NotificationServices.deviceToken;
            if(token != null)
            {
                isIOSTokenSent = true;
                
                ServerNetworkManager.Instance.PushToken = System.BitConverter.ToString(token).Replace("-", "").ToLower();
                Debug.Log("PushManager.OnTokenReceived: Received Registration Token: " + ServerNetworkManager.Instance.PushToken);
                
                if(!PlayerPrefs.HasKey("push_registered") || PlayerPrefs.GetInt("push_registered") == 0)
                {
                    ServerNetworkManager.Instance.RegisterForPush();
                }
            }
        }
    }
#endif

#if UNITY_ANDROID
    private void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
    {
        Debug.Log("PushManager.OnTokenReceived: Received Registration Token: " + token.Token);
        ServerNetworkManager.Instance.PushToken = token.Token;

        if(!PlayerPrefs.HasKey("push_registered") || PlayerPrefs.GetInt("push_registered") == 0)
        {
            ServerNetworkManager.Instance.RegisterForPush();
        }
    }
#endif

}