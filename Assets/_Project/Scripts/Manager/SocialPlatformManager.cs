﻿using System;
using UnityEngine;

using GooglePlayGames;
using GooglePlayGames.BasicApi;

using UnityEngine.SocialPlatforms;

#if UNITY_IPHONE
using UnityEngine.SocialPlatforms.GameCenter;
#endif

public class SocialPlatformManager : MonoBehaviour {
	private static SocialPlatformManager instance = null;
	public static SocialPlatformManager Instance
	{
		get{
			if(instance == null)
				instance = GameObject.FindObjectOfType<SocialPlatformManager>();
			return instance;
		}
	}

	public enum AchievemenType {LOG_IN, FIRST_BOSS_KILL, SUMMON, FIRST_PVP_WIN, UNLOCK_SKILL}
	public enum LeaderBoardType {LAST_STAGE};

	public bool IsSocialAuthenticatedProperly{
		get{
			if(Social.localUser.id.Contains("UnknownID") || Social.localUser.id.Contains("Unavailable") || Social.localUser.id.Length <= 5)
				return false;
			else
				return Social.localUser.authenticated;
		}
	}

	private void Awake()
	{
#if UNITY_IPHONE
		// Do nothing.
#elif UNITY_ANDROID
		PlayGamesPlatform.Activate();
#endif
	}

	public void LoginSocialService(Action onSuccess, Action onFailure)
	{
		if(IsSocialAuthenticatedProperly)
		{
			onSuccess?.Invoke();
		}
		else
		{
			Social.localUser.Authenticate((bool success) => {
				Debug.Log("SocialPlatformManager.LoginSocialService");
				Debug.Log("sucesss => " + success);
				Debug.Log("authenticated => " + Social.localUser.authenticated);
				Debug.Log("id => " + Social.localUser.id);

				if(success && IsSocialAuthenticatedProperly)
					onSuccess?.Invoke();
				else
					onFailure?.Invoke();
			});
		}
	}

	public void LogoutSocialService()
	{
#if UNITY_ANDROID
		if(Social.localUser.authenticated)
			PlayGamesPlatform.Instance.SignOut();
#endif
	}

}