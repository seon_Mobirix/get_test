﻿using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;


public enum FacebookLogType {SUMMON_10, SUMMON_50, SUMMON_100, REWARD_ADS}

public class FacebookManager : MonoBehaviour {

	private static FacebookManager instance = null;
	public static FacebookManager Instance
	{
		get{
			if(instance == null)
				instance = GameObject.FindObjectOfType<FacebookManager>();
			return instance;
		}
	}

	// Awake function from Unity's MonoBehavior
	void Awake ()
	{
		if (FB.IsInitialized) {
			FB.ActivateApp();
		} 
		else {
			//Handle FB.Init
			FB.Init( () => {
				FB.ActivateApp();
			});
		}
#if UNITY_IPHONE && !UNITY_EDITOR
		FB.Mobile.SetAdvertiserTrackingEnabled(true);
#endif
	}

	void OnApplicationPause (bool pauseStatus)
	{
		// Check the pauseStatus to see if we are in the foreground
		// or background
		if (!pauseStatus) {
			//app resume
			if (FB.IsInitialized) {
				FB.ActivateApp();
			} 
			else {
				//Handle FB.Init
				FB.Init( () => {
					FB.ActivateApp();
				});
			}
		}
	}

	public void LogEvent (FacebookLogType logType)
	{
		var parameters = new Dictionary<string, object>();
    
		switch (logType) {
			case FacebookLogType.SUMMON_10:
				parameters[AppEventParameterName.Description] = "summoned more than 10";
				parameters[AppEventParameterName.ContentID] = "summon_10";
				parameters[AppEventParameterName.Success] = 1;
				FB.LogAppEvent(
					AppEventName.CompletedTutorial,
					1f,
					parameters
				);
				break;
			case FacebookLogType.SUMMON_50:
				parameters[AppEventParameterName.Level] = 1;
				FB.LogAppEvent(
					AppEventName.AchievedLevel,
					1f,
					parameters
				);
				break;
			case FacebookLogType.SUMMON_100:
				parameters[AppEventParameterName.Description] = "summoned more than 100";
				FB.LogAppEvent(
					AppEventName.UnlockedAchievement,
					1f,
					parameters
				);
				break;
			case FacebookLogType.REWARD_ADS:
				parameters["viewCount"] = 1;
				FB.LogAppEvent(
					"Reward",
					1f,
					parameters
				);
				break;
		}
	}

	public void LogPurchaseForAndroid(decimal amount, string currency)
	{
		FB.LogPurchase(amount, currency);
	}
}
