﻿#define TEST_SEON

using UnityEngine;

using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using System.Numerics;
using System.Globalization;

using PlayFab;
using PlayFab.ClientModels;

using Sirenix.OdinInspector;

using CodeStage.AntiCheat.Storage;
using CodeStage.AntiCheat.ObscuredTypes;

using Photon.Chat;

public class ServerNetworkManager: MonoBehaviour {

	protected internal ChatSettings chatAppSettings;

	public ObscuredBool IsCacheAltered = false;
	
	private static ServerNetworkManager instance = null;
	public static ServerNetworkManager Instance
	{
		get{
			if(instance == null)
				instance = GameObject.FindObjectOfType<ServerNetworkManager>();
			return instance;
		}
	}

	private void Awake()
	{
#if UNITY_IPHONE && !UNITY_EDITOR
		PlayFabSettings.TitleId = "85B34";
#else
		PlayFabSettings.TitleId = "5CC39";
#endif
		// To detect local cache altered
        ObscuredPrefs.OnAlterationDetected += () => {IsCacheAltered = true;};
	}

#region Editor Test Options
	
	[Title("Editor Test Options")]
	[SerializeField]
	private int testingCloudScriptRevision = -1;

#endregion

#region For Auth
	
	[Title("For Auth")]
	public string AuthKey;
	private bool isLoggedIn = false;
	
	public bool IsLoggedIn{
		get{
			return isLoggedIn;
		}
	}

#endregion

#region For Push
	public string PushToken;
	private bool isFailedBefore = false;
#endregion

#region For Server Settings

	[Title("For Server Settings")]
	[SerializeField]
	private SettingModel setting;
	public SettingModel Setting{
		get{
			return setting;
		}
	}

	private bool isInReview;
	public bool IsInReview{
		get{
			return isInReview;
		}
	}

#endregion

#region Catalog Information
	
	[Title("Catalog Information")]
	[SerializeField]
	private CatalogModel catalog;
	public CatalogModel Catalog{
		get{
			return catalog;
		}
	}
#endregion

#region Account Information
	
	[Title("User Data")]
	[SerializeField]
	private UserModel user;
	public UserModel User{
		get{
			return user;
		}
	}

	private bool isDefaultSettingsAppplied = false;

	[SerializeField]
	private int dailyRewardCount = 0;
	public int DailyRewardCount
	{
		get{ 
			return dailyRewardCount;
		}
	}
    
	[SerializeField]
	private int freeGemDailyLeftLimit = 0;
	public int FreeGemDailyLeftLimit
	{ 
		get{ 
			return freeGemDailyLeftLimit;
		}
	}

	[SerializeField]
	private int nextFreeGemAdsTimeStamp = 0;
	public int NextFreeGemAdsTimeStamp
	{ 
		get{ 
			return nextFreeGemAdsTimeStamp;
		}
	}

	[SerializeField]
	private int rankingPosition = 0;
	public int RankingPosition{
		get{
			return rankingPosition;
		}
	}

	[SerializeField]
	private ChatBlockInfoListModel chatBlockInfo;
	public ChatBlockInfoListModel ChatBlockInfo{
		get{
			return chatBlockInfo;
		}
	}

	[SerializeField]
	private ChatBanInfoListModel chatBanInfo;
	public ChatBanInfoListModel ChatBanInfo{
		get{
			return chatBanInfo;
		}
	}

	private ObscuredBool isModerator = false;
	public ObscuredBool IsModerator{
		get{
			return isModerator;
		}
	}

	private ObscuredBool isChatBanned = false;
	public ObscuredBool IsChatBanned{
		get{
			return isChatBanned;
		}
	}
	
#endregion

#region Inventory Information
	
	[Title("Inventory Data")]
	[SerializeField]
	private InventoryModel inventory;
	public InventoryModel Inventory{
		get{
			return inventory;
		}
	}

	[SerializeField]
	private bool isUsedYT10Coupon = false;
	public bool IsUsedYT10Coupon{
		get{
			return isUsedYT10Coupon;
		}
	}

	[SerializeField]
	private List<string> purchasedLimitedPackageList = new List<string>();
	public List<string> PurchasedLimitedPackageList{
		get{
			return purchasedLimitedPackageList;
		}
	}

	[SerializeField]
	private List<string> purchasedBasicPackageList = new List<string>();
	public List<string> PurchasedBasicPackageList{
		get{
			return purchasedBasicPackageList;
		}
	}

	[SerializeField]
	private List<string> purchasedGrowthPackageList = new List<string>();
	public List<string> PurchasedGrowthPackageList{
		get{
			return purchasedGrowthPackageList;
		}
	}

	[SerializeField]
	private List<string> purchasedGemBundleList = new List<string>();
	public List<string> PurchasedGemBundleList{
		get{
			return purchasedGemBundleList;
		}
	}
	
	[SerializeField]
	private WingBodyInstanceModel wingBodyInfo = new WingBodyInstanceModel(false, null);
	public WingBodyInstanceModel WingBodyInfo{
		get{
			return wingBodyInfo;
		}
	}

	[SerializeField]
	private List<WingInstanceModel> purchasedWingList = new List<WingInstanceModel>();
	public List<WingInstanceModel> PurchasedWingList{
		get{
			return purchasedWingList;
		}
	}

	[SerializeField]
	private List<String> receivedGrowthSupportFreeRewardList = new List<string>();
	public List<string> ReceivedGrowthSupportFreeRewardList{
		get{
			return receivedGrowthSupportFreeRewardList;
		}
	}

	[SerializeField]
	private List<String> receivedGrowthSupportFirstRewardList = new List<string>();
	public List<string> ReceivedGrowthSupportFirstRewardList{
		get{
			return receivedGrowthSupportFirstRewardList;
		}
	}

	[SerializeField]
	private List<String> receivedGrowthSupportSecondRewardList = new List<string>();
	public List<string> ReceivedGrowthSupportSecondRewardList{
		get{
			return receivedGrowthSupportSecondRewardList;
		}
	}

	// TODO: remove below after is_stack_package becomes true
	// nextAvailableDailyPackageTimeSeconds, nextAvailableDailyPackage2TimeSeconds, nextAvailableWeeklyPackageTimeSeconds, nextAvailableWeeklyPackage2TimeSeconds

	[SerializeField]
	private DateTime nextAvailableDailyPackageTimeSeconds = new DateTime();
	public double LeftAvailableDailyPackageTimeSeconds{
		get{
			return nextAvailableDailyPackageTimeSeconds.Subtract(DateTime.UtcNow).TotalSeconds;
		}
	}

	[SerializeField]
	private DateTime nextAvailableDailyPackage2TimeSeconds = new DateTime();
	public double LeftAvailableDailyPackage2TimeSeconds{
		get{
			return nextAvailableDailyPackage2TimeSeconds.Subtract(DateTime.UtcNow).TotalSeconds;
		}
	}

	[SerializeField]
	private DateTime nextAvailableWeeklyPackageTimeSeconds = new DateTime();
	public double LeftAvailableWeeklyPackageTimeSeconds{
		get{
			return nextAvailableWeeklyPackageTimeSeconds.Subtract(DateTime.UtcNow).TotalSeconds;
		}
	}

	[SerializeField]
	private DateTime nextAvailableWeeklyPackage2TimeSeconds = new DateTime();
	public double LeftAvailableWeeklyPackage2TimeSeconds{
		get{
			return nextAvailableWeeklyPackage2TimeSeconds.Subtract(DateTime.UtcNow).TotalSeconds;
		}
	}

	public ObscuredInt UnsyncedGem = 0;
	public ObscuredInt TotalGem{
		get{ 
			return inventory.Gem + UnsyncedGem;
		}
	}
    
#endregion

#region Daily Reward Information
	[SerializeField]
    public DailyRewardListModel dailyRewardTable = new DailyRewardListModel();
	public DailyRewardListModel DailyRewardTable{
		get{
			return dailyRewardTable;
		}
	}
#endregion

#region Post Data
	[Title("Post Data")]
	[SerializeField]
	private PostListModel post = new PostListModel();
	public PostListModel Post{
		get{
			return post;
		}
	}
	[Title("System Post Data")]
	[SerializeField]
	private SystemPostListModel systemPost = new SystemPostListModel();
	public SystemPostListModel SystemPost{
		get{
			return systemPost;
		}
	}
#endregion

#region Notice Data
	
	[Title("Notice Data")]
	[SerializeField]
	private List<NoticeModel> noticeList = new List<NoticeModel>();
	public List<NoticeModel> NoticeList{
		get{
			return noticeList;
		}
	}

#endregion

#region Ranking Data

	[Title("Ranking Data")]
	[SerializeField]
	private RankingModel stageRank = new RankingModel();
	public RankingModel StageRank{
		get{
			return stageRank;
		}
	}

	[SerializeField]
	private RankingModel arenaRank = new RankingModel();
	public RankingModel ArenaRank{
		get{
			return arenaRank;
		}
	}

	[SerializeField]
	private RankingModel totalArenaRank = new RankingModel();
	public RankingModel TotalArenaRank{
		get{
			return totalArenaRank;
		}
	}

	[SerializeField]
	private List<GuildRankModel> guildRankList = new List<GuildRankModel>();
	public List<GuildRankModel> GuildRankList{
		get{
			return guildRankList;
		}
	}

#endregion

#region Arena Data
	
	[Title("Arena Data")]
	[SerializeField]
	private ArenaModel arena = new ArenaModel();
	public ArenaModel Arena{
		get{
			return arena;
		}
	}

	[SerializeField]
	private List<string> recentArenaMatchList = new List<string>();

#endregion

#region Raid Data

	[Title("Raid Data")]
	[SerializeField]
	private RaidModel raid = new RaidModel();
	public RaidModel Raid{
		get{
			return raid;
		}
	}

#endregion

#region Chat Data

	public string PhotonAuthKey = "";

#endregion 

#region Attendent Event Data
	
	[Title("Attendent Event Data")]
	[SerializeField]
	private double lastAttendanceTimestampday = 0;
	public double LastAttendanceTimeStampDay
	{
		get{
			return lastAttendanceTimestampday;
		}	
	}

	[SerializeField]
	private AttendanceEventInfoModel attendanceEventInfo;
	public AttendanceEventInfoModel AttendanceEventInfo
	{
		get{ 
			return attendanceEventInfo;
		}
	}

#endregion

#region Free Growth Support Data
	[SerializeField]
	private FreeGrowthRewardListModel freeGrowthRewardTable;
	public FreeGrowthRewardListModel FreeGrowthRewardTable
	{
		get{ 
			return freeGrowthRewardTable;
		}
	}
#endregion

#region Tower Data

	[Title("Tower Data")]
	[SerializeField]
	private TowerModel myTowerData = new TowerModel();
	public TowerModel MyTowerData{
		get{
			return myTowerData;
		}
	}

	public int TowerCooldownLeft = 0;

	public int TowerCooldownDeployLeft = 0;

	public int TowerDailyLimitLeft = 0;

	public List<TowerFloorModel> TowerFloorPlayers = new List<TowerFloorModel>();

#endregion

#region Guild Data

	[Title("Guild Data")]
	[SerializeField]
	private string myGuildId = "";
	public String MyGuildId{
		get{
			return myGuildId;
		}
	}

	[SerializeField]
	private GuildModel guildData = new GuildModel();
	public GuildModel GuildData{
		get{
			return guildData;
		}
	}

	[SerializeField]
	private GuildRaidModel guildRaidData = new GuildRaidModel();
	public GuildRaidModel GuildRaidData{
		get{
			return guildRaidData;
		}
	}

	[SerializeField]
	private GuildRelicListModel guildRelicData = new GuildRelicListModel();
	public GuildRelicListModel GuildRelicData{
		get{
			return guildRelicData;
		}
	}

#endregion

#region Login Chain
	
	[Title("Login Chain")]
	public string loginChainPhaseText = "";
	public void Login(Action onSuccess, Action<string> onFailure)
	{
		// Check for log in status
		if(this.isLoggedIn)
		{
			onSuccess?.Invoke();
			return;
		}

		loginChainPhaseText = "Signing In";

		// Values to retrieve
		var paramsToGet = new GetPlayerCombinedInfoRequestParams();
		paramsToGet.GetUserAccountInfo = true;
		paramsToGet.GetUserInventory = true;
		paramsToGet.GetUserData = true;
		paramsToGet.GetUserVirtualCurrency = true;
		paramsToGet.GetUserReadOnlyData = true;
		paramsToGet.GetTitleData = true;
		paramsToGet.GetPlayerProfile = true;
		
		var playerProfileViewConstraints= new PlayerProfileViewConstraints();
		playerProfileViewConstraints.ShowTags = true;
		paramsToGet.ProfileConstraints = playerProfileViewConstraints;

		if(OptionManager.Instance.LoginMode == OptionManager.LoginModeType.PLATFORM_DEFAULT && SocialPlatformManager.Instance.IsSocialAuthenticatedProperly)
		{
#if UNITY_IPHONE && !UNITY_EDITOR
			Debug.Log("ServerNetworkManager.Login: Game Center");
			LoginWithGameCenterRequest request = new LoginWithGameCenterRequest()
			{
				TitleId = PlayFabSettings.TitleId,
				CreateAccount = true,
				PlayerId = this.AuthKey,
				InfoRequestParameters = paramsToGet
			};

			PlayFabClientAPI.LoginWithGameCenter(request, 
				(result) => {OnLogin(onSuccess, onFailure, result);},
				(error) => {OnFailedLogin(onFailure, error);}
			);
#else
			Debug.Log("ServerNetworkManager.Login: Custom for Generic Social Platform");
			LoginWithCustomIDRequest request = new LoginWithCustomIDRequest()
			{
				TitleId = PlayFabSettings.TitleId,
				CreateAccount = true,
				CustomId = this.AuthKey,
				InfoRequestParameters = paramsToGet
			};

			PlayFabClientAPI.LoginWithCustomID(request, 
				(result) => {OnLogin(onSuccess, onFailure, result);},
				(error) => {OnFailedLogin(onFailure, error);}
			);
#endif
		}
		else
		{
#if UNITY_IPHONE && !UNITY_EDITOR
			Debug.Log("ServerNetworkManager.Login: iOS Deveice");
			LoginWithIOSDeviceIDRequest request = new LoginWithIOSDeviceIDRequest()
			{
				TitleId = PlayFabSettings.TitleId,
				CreateAccount = true,
				DeviceId = this.AuthKey,
				DeviceModel = SystemInfo.deviceModel,
				OS = SystemInfo.operatingSystem,
				InfoRequestParameters = paramsToGet
			};

			PlayFabClientAPI.LoginWithIOSDeviceID(request, 
				(result) => {OnLogin(onSuccess, onFailure, result);},
				(error) => {OnFailedLogin(onFailure, error);}
			);
#elif UNITY_ANDROID && !UNITY_EDITOR
			Debug.Log("ServerNetworkManager.Login: Android Device");
			LoginWithAndroidDeviceIDRequest request = new LoginWithAndroidDeviceIDRequest()
			{
				TitleId = PlayFabSettings.TitleId,
				CreateAccount = true,
				AndroidDeviceId = this.AuthKey,
				AndroidDevice = SystemInfo.deviceModel,
				OS = SystemInfo.operatingSystem,
				InfoRequestParameters = paramsToGet
			};

			PlayFabClientAPI.LoginWithAndroidDeviceID(request, 
				(result) => {OnLogin(onSuccess, onFailure, result);},
				(error) => {OnFailedLogin(onFailure, error);}
			);
#else
			Debug.Log("ServerNetworkManager.Login: Custom for editor");
			LoginWithCustomIDRequest request = new LoginWithCustomIDRequest()
			{
				TitleId = PlayFabSettings.TitleId,
				CreateAccount = true,
				CustomId = this.AuthKey,
				InfoRequestParameters = paramsToGet
			};

			PlayFabClientAPI.LoginWithCustomID(request, 
				(result) => {OnLogin(onSuccess, onFailure, result);},
				(error) => {OnFailedLogin(onFailure, error);}
			);
#endif
		}		
	}

	private void OnLogin(Action onSuccess, Action<string> onFailure, LoginResult result)
	{
		Debug.Log("ServerNetworkManager.OnLogin");

		if(PlayFabClientAPI.IsClientLoggedIn())
			this.isLoggedIn = true;

		loginChainPhaseText = "Retrieving Settings and Player Data";

		// Get setting data
		if(result.InfoResultPayload.TitleData.ContainsKey("setting_json"))
		{
			setting = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<SettingModel>(result.InfoResultPayload.TitleData["setting_json"]);
		}

		// Get balance growth data
		if(result.InfoResultPayload.TitleData.ContainsKey("balance_inventory_growth") 
			&& result.InfoResultPayload.TitleData.ContainsKey("balance_main_growth")
			&& result.InfoResultPayload.TitleData.ContainsKey("balance_monster"))
		{
			BalanceInfoManager.Instance.LoadDataFromServer(
				result.InfoResultPayload.TitleData["balance_main_growth"], 
				result.InfoResultPayload.TitleData["balance_inventory_growth"], 
				result.InfoResultPayload.TitleData["balance_monster"],
				result.InfoResultPayload.TitleData["balance_achievement"]);
		}

		// Get attendance event data
		if(result.InfoResultPayload.TitleData.ContainsKey("attendance_event_data"))
			attendanceEventInfo = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<AttendanceEventInfoModel>(result.InfoResultPayload.TitleData["attendance_event_data"]);
		else
			attendanceEventInfo = null;

		// Get growth support free reward table
		if(result.InfoResultPayload.TitleData.ContainsKey("growth_support_free_reward_table"))
			freeGrowthRewardTable = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<FreeGrowthRewardListModel>(result.InfoResultPayload.TitleData["growth_support_free_reward_table"]);
		else
			freeGrowthRewardTable = null;

		this.user = new UserModel();
		var serverUserData = new ServerUserModel();

		user.PlayFabId = result.PlayFabId;
		Debug.Log("   >>>>>> id: " + user.PlayFabId);

		if(result.InfoResultPayload.AccountInfo.TitleInfo.DisplayName != null)
			user.NickName = result.InfoResultPayload.AccountInfo.TitleInfo.DisplayName;
		else
			user.NickName = "";
		
		Debug.Log("   >>>>>> nickname: " + user.NickName);

		// Check whether local cache removed or not
		var localCacheExist = ObscuredPrefs.HasKey("dt_us_1" + user.PlayFabId) && ObscuredPrefs.HasKey("dt_iv_1" + user.PlayFabId) && ObscuredPrefs.HasKey("dt_iv_2" + user.PlayFabId);

		// Check whether device change detected recently and local cache removed
		var recentlyResetLocalCacheByDeviceChange = false;
		if(ObscuredPrefs.HasKey("is_recently_reset_by_device_change"))
		{
			recentlyResetLocalCacheByDeviceChange = true;
			ObscuredPrefs.DeleteKey("is_recently_reset_by_device_change");
		}

		// For checking the last device was not this device
		var wasLastDeviceThis = true;
		if(result.InfoResultPayload.UserReadOnlyData.ContainsKey("last_device_tag"))
		{
			var deviceTag = SystemInfo.deviceModel + "_" + SystemInfo.deviceUniqueIdentifier;
			if(deviceTag != result.InfoResultPayload.UserReadOnlyData["last_device_tag"].Value)
				wasLastDeviceThis = false;
		}

		// If this is not the last device used, reset data cache
		if(localCacheExist && !wasLastDeviceThis)
		{
			ObscuredPrefs.DeleteKey("dt_us_1" + user.PlayFabId);
			ObscuredPrefs.DeleteKey("dt_iv_1" + user.PlayFabId);
			ObscuredPrefs.DeleteKey("dt_iv_2" + user.PlayFabId);
			ObscuredPrefs.SetBool("is_recently_reset_by_device_change", true);
		}

		if(result.InfoResultPayload.UserReadOnlyData.ContainsKey("user_data"))
			serverUserData = JsonUtility.FromJson<ServerUserModel>(result.InfoResultPayload.UserReadOnlyData["user_data"].Value);
		
		var localCacheUsed = ApplyUserDataFromServer(user.PlayFabId, this.user, serverUserData);

		this.inventory = new InventoryModel();
		var serverInventoryData = new ServerInventoryModel();
		if(result.InfoResultPayload.UserReadOnlyData.ContainsKey("inventory_data"))
			serverInventoryData = JsonUtility.FromJson<ServerInventoryModel>(result.InfoResultPayload.UserReadOnlyData["inventory_data"].Value);
		localCacheUsed = ApplyInventoryDataFromServer(user.PlayFabId, this.inventory, serverInventoryData) && localCacheUsed;

		// Receive push setting and server id
		if(result.InfoResultPayload.PlayerProfile != null && result.InfoResultPayload.PlayerProfile.Tags != null)
		{
			if(result.InfoResultPayload.PlayerProfile.Tags.FirstOrDefault(n => n.TagValue.Contains("PushOff")) != null)
				OptionManager.Instance.IsPushOff = true;
			else
				OptionManager.Instance.IsPushOff = false;

			var serverId = result.InfoResultPayload.PlayerProfile.Tags.FirstOrDefault(n => n.TagValue.Contains("Server"));
			if(serverId != null)
			{
				user.ServerId = serverId.TagValue.Substring(serverId.TagValue.Length - 2);
				if(user.ServerId == "er")
					user.ServerId = "";
			}
			else
			{
				user.ServerId = "";
			}
		}
		else
			OptionManager.Instance.IsPushOff = false;

		// Get purchased gem bundle list for gem bonus
		if(result.InfoResultPayload.UserReadOnlyData.ContainsKey("purchased_gem_bunlde_list"))
			this.purchasedGemBundleList = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<List<string>>(result.InfoResultPayload.UserReadOnlyData["purchased_gem_bunlde_list"].Value);
		else
			this.purchasedGemBundleList = new List<string>();

		// Get growth support free received reward list
		if(result.InfoResultPayload.UserReadOnlyData.ContainsKey("growth_support_free_received_reward_list"))
			this.receivedGrowthSupportFreeRewardList = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<List<string>>(result.InfoResultPayload.UserReadOnlyData["growth_support_free_received_reward_list"].Value);
		else
			this.receivedGrowthSupportFreeRewardList = new List<string>();

		// Get growth support 1 received reward list
		if(result.InfoResultPayload.UserReadOnlyData.ContainsKey("growth_support_1_received_reward_list"))
			this.receivedGrowthSupportFirstRewardList = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<List<string>>(result.InfoResultPayload.UserReadOnlyData["growth_support_1_received_reward_list"].Value);	
		else
			this.receivedGrowthSupportFirstRewardList = new List<string>();

		// Get growth support 2 received reward list
		if(result.InfoResultPayload.UserReadOnlyData.ContainsKey("growth_support_2_received_reward_list"))
			this.receivedGrowthSupportSecondRewardList = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<List<string>>(result.InfoResultPayload.UserReadOnlyData["growth_support_2_received_reward_list"].Value);
		else
			this.receivedGrowthSupportSecondRewardList = new List<string>();

		// Get post data
		if(result.InfoResultPayload.UserReadOnlyData.ContainsKey("inbox"))
			this.post = JsonUtility.FromJson<PostListModel>(result.InfoResultPayload.UserReadOnlyData["inbox"].Value);
		else
			this.post = new PostListModel();

		// Get system post data
		if(result.InfoResultPayload.UserReadOnlyData.ContainsKey("inbox_system"))
			this.systemPost = JsonUtility.FromJson<SystemPostListModel>(result.InfoResultPayload.UserReadOnlyData["inbox_system"].Value);
		else
			this.systemPost = new SystemPostListModel();

		// Get next free gem AD time stamp
		if(result.InfoResultPayload.UserReadOnlyData.ContainsKey("nextFreeGemAdsTimeStamp"))
			this.nextFreeGemAdsTimeStamp = int.Parse(result.InfoResultPayload.UserReadOnlyData["nextFreeGemAdsTimeStamp"].Value);
		else
			this.nextFreeGemAdsTimeStamp = 0;

		// Get moderator info
		if(result.InfoResultPayload.UserReadOnlyData.ContainsKey("is_moderator"))
			this.isModerator = Boolean.Parse(result.InfoResultPayload.UserReadOnlyData["is_moderator"].Value);
		else
			this.isModerator = false;

		// If user is moderator then get chat ban data
		if(this.isModerator)
		{
			if(result.InfoResultPayload.UserReadOnlyData.ContainsKey("chat_ban_data"))
				this.chatBanInfo = JsonUtility.FromJson<ChatBanInfoListModel>(result.InfoResultPayload.UserReadOnlyData["chat_ban_data"].Value);
			else
				this.chatBanInfo = new ChatBanInfoListModel();
		}
		else
			this.chatBanInfo = new ChatBanInfoListModel();

		// Get chat banned status
		if(result.InfoResultPayload.UserReadOnlyData.ContainsKey("is_chat_banned"))
			this.isChatBanned = Boolean.Parse(result.InfoResultPayload.UserReadOnlyData["is_chat_banned"].Value);
		else
			this.isChatBanned = false;

		// Get chat block data
		if(result.InfoResultPayload.UserReadOnlyData.ContainsKey("chat_block_data"))
			this.chatBlockInfo = JsonUtility.FromJson<ChatBlockInfoListModel>(result.InfoResultPayload.UserReadOnlyData["chat_block_data"].Value);
		else
			this.chatBlockInfo = new ChatBlockInfoListModel();

		// Get last attendance event timestampday
		if(result.InfoResultPayload.UserReadOnlyData.ContainsKey("last_attendance_event_timestampday"))
			this.lastAttendanceTimestampday = int.Parse(result.InfoResultPayload.UserReadOnlyData["last_attendance_event_timestampday"].Value);
		else
			this.lastAttendanceTimestampday = 0;

		// Get my tower data
		if(result.InfoResultPayload.UserReadOnlyData.ContainsKey("tower_data_1"))
		{
			this.myTowerData = JsonUtility.FromJson<TowerModel>(result.InfoResultPayload.UserReadOnlyData["tower_data_1"].Value);

			// Check my tower deployed
			if(myTowerData.Floor != 0f)
				myTowerData.IsDeployed = true;

			// Cehck minutes left
        	var currentTimestampSecond = DateTimeOffset.Now.ToUnixTimeSeconds();
			myTowerData.SecondLeft = Math.Max(myTowerData.DeployedTimestamp + 14400 - currentTimestampSecond, 0d);

			// Check my tower completed
			if(myTowerData.SecondLeft <= 0 && myTowerData.IsDeployed)
				myTowerData.IsCompleted = true;
		}			
		else
			this.myTowerData = new TowerModel();

		// Get my guild id
		if(result.InfoResultPayload.UserReadOnlyData.ContainsKey("guild_id"))
			this.myGuildId = result.InfoResultPayload.UserReadOnlyData["guild_id"].Value;
		else
			this.myGuildId = "";

		// Get my guild relic data
		if(result.InfoResultPayload.UserReadOnlyData.ContainsKey("guild_relic_data"))
			this.guildRelicData = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<GuildRelicListModel>(result.InfoResultPayload.UserReadOnlyData["guild_relic_data"].Value);
		else
			this.guildRelicData = new GuildRelicListModel();
				
		// Check minimum version
		bool meetMinimumVersion = true;
		
		if(Application.platform == RuntimePlatform.IPhonePlayer)
		{
			var minVersion = new System.Version(setting.MiniOSVersion);
			Debug.Log("   >>>>>> Minimum version to play: " + minVersion);
			var currentVersion = new System.Version(Application.version);
			Debug.Log("   >>>>>> Current version to play: " + currentVersion);

			if(currentVersion < minVersion)
				meetMinimumVersion = false;
		}
		else
		{
			var minVersion = new System.Version(setting.MinAndroidVersion);
			Debug.Log("   >>>>>> Minimum version to play: " + minVersion);
			var currentVersion = new System.Version(Application.version);
			Debug.Log("   >>>>>> Current version to play: " + currentVersion);

#if !TEST_SEON
			if(currentVersion < minVersion)
				meetMinimumVersion = false;
#endif
		}

		// Check server status
		if (meetMinimumVersion && !this.setting.IsMaintenance)
		{
			// Check default settings have been applied or not
			if(result.InfoResultPayload.UserReadOnlyData.ContainsKey("is_default_settings_applied"))
				isDefaultSettingsAppplied = bool.Parse(result.InfoResultPayload.UserReadOnlyData["is_default_settings_applied"].Value);
			else
				isDefaultSettingsAppplied = false;

			if(isDefaultSettingsAppplied)
			{
				if(wasLastDeviceThis) 
				{
					// We can override server data (no matter how it was from server or local cache)
					GetCatalog(
						() => {
							SyncPlayerData(
								!localCacheExist && !recentlyResetLocalCacheByDeviceChange,
								"OnLogin",
								onSuccess,
								(error) => {
									if(error == "ban")
									{
										Debug.Log("ServerNetworkManager.OnLogin: Account is banned");
										Application.Quit();
									}
									else
									{
										onFailure?.Invoke("network");
										return;
									}
								}
							);
						}, 
						() => {
							onFailure?.Invoke("network");
							return;
						}
					);
				}
				else
				{
					// If the device was no the last device, then we shouldn't override data
					GetCatalog(
						onSuccess, 
						() => {
							onFailure?.Invoke("network");
							return;
						}
					);
				}
			}
			else
			{
				ApplyDefaultSettings(
					onSuccess, 
					() => {
						onFailure?.Invoke("network");
						return;
					}
				);
			}
		}
		else if(this.setting.IsMaintenance)
		{
			onFailure?.Invoke("in_maintenance");
			return;
		}
		else if(!meetMinimumVersion)
		{
			onFailure?.Invoke("below_min_version");
			return;
		}
		else
		{
			onFailure?.Invoke("network");
			return;
		}
	}

	private void OnFailedLogin(Action<string> onFailure, PlayFabError error)
	{
		Debug.Log(error.ErrorMessage);
		
		if(error.Error == PlayFabErrorCode.AccountBanned)
			onFailure?.Invoke("banned");
		else
			onFailure?.Invoke("network");
	}

	private void ApplyDefaultSettings(Action onSuccess, Action onFailure)
	{
		loginChainPhaseText = "Applying Default Settings";
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "defaultPlayerSetting",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "defaultPlayerSetting",
				GeneratePlayStreamEvent = true,
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.ApplyDefaultSettings");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);
				
				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;

				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}

					if(jsonResult.ContainsKey("userData"))
					{
						object valueToUse;
						jsonResult.TryGetValue("userData", out valueToUse);

						var serverUserData = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<ServerUserModel>(Convert.ToString(valueToUse));
						ApplyUserDataFromServer(user.PlayFabId, this.user, serverUserData);
					}

					if(jsonResult.ContainsKey("inventoryData"))
					{
						object valueToUse;
						jsonResult.TryGetValue("inventoryData", out valueToUse);

						var serverInventoryData = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<ServerInventoryModel>(Convert.ToString(valueToUse));
						ApplyInventoryDataFromServer(user.PlayFabId, this.inventory, serverInventoryData);
					}

					onSuccess?.Invoke();
					{
						GetCatalog(
							onSuccess, 
							() => {
								onFailure?.Invoke();
								return;
							}
						);
					}
				}
				else
				{
					Debug.Log("ServerNetworkManager.DefaultPlayerSetting: No json result exists");
					onFailure?.Invoke();
					return;
				}
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);
				onFailure?.Invoke();
				return;
			}
		);
	}

	public void GetPhotonAuthToken(Action onSuccess,  Action<string> onFailure)
	{
		#if PHOTON_UNITY_NETWORKING
        this.chatAppSettings = PhotonNetwork.PhotonServerSettings.AppSettings;
		#else
		if (this.chatAppSettings == null)
		{
			this.chatAppSettings = ChatSettings.Instance;
		}
        #endif

		loginChainPhaseText = "Requesting Realtime Network Token";
		GetPhotonAuthenticationTokenRequest request = new GetPhotonAuthenticationTokenRequest()
		{
#if UNITY_IPHONE && !UNITY_EDITOR
			PhotonApplicationId = "42735267-163d-4b32-8ff3-757be8321abd"
#else
			PhotonApplicationId = "715a6ea2-7ac7-4c5a-aed7-189217929db4"
#endif
		};

		PlayFabClientAPI.GetPhotonAuthenticationToken(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.GetPhotonAuthToken: " + result.PhotonCustomAuthenticationToken);

				PhotonAuthKey = result.PhotonCustomAuthenticationToken;

				onSuccess?.Invoke();

			}, 
			(error) =>
			{
				Debug.Log(error.ErrorMessage);
				onFailure?.Invoke("network");
			}
		);
	}

	private void GetCatalog(Action onSuccess, Action onFailure)
	{
		loginChainPhaseText = "Retrieving Catalog Data";
		GetCatalogItemsRequest request = new GetCatalogItemsRequest();

		this.catalog = new CatalogModel();

        PlayFabClientAPI.GetCatalogItems(
			request, 
			(Action<GetCatalogItemsResult>)((result) =>
			{
                Debug.Log("ServerNetworkManager.GetCatalog");

				foreach(var tmp in result.Catalog)
				{
					if(tmp.ItemId.Contains("bundle_gold_"))
					{
						var tmpGoldBundleModel = new GoldBundleModel();
						tmpGoldBundleModel.ItemId = tmp.ItemId;

						foreach(var tmp2 in PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<Dictionary<string, string>>(tmp.CustomData))
						{
							if(tmp2.Key == "gold_weight")
								tmpGoldBundleModel.GivingAmount = Convert.ToInt32(tmp2.Value);
						}
						
						tmpGoldBundleModel.PriceType = GoodsPriceType.GEM;
						tmpGoldBundleModel.Price = (int)tmp.VirtualCurrencyPrices["GE"];

						this.catalog.GoldBundleList.Add(tmpGoldBundleModel);
					}
					else if(tmp.ItemId.Contains("bundle_gem_"))
					{
						var tmpGemPackageModel = new GemBundleModel();
						tmpGemPackageModel.ItemId = tmp.ItemId;
						tmpGemPackageModel.GivingAmount = (int)tmp.Bundle.BundledVirtualCurrencies["GE"];
						tmpGemPackageModel.VipReward = (int)tmp.Bundle.BundledVirtualCurrencies["VP"];
						tmpGemPackageModel.PriceType = GoodsPriceType.CASH;

						#if UNITY_EDITOR
							tmpGemPackageModel.RealMoneyPrice = string.Format("${0}", (float)tmp.VirtualCurrencyPrices["RM"] / 100.0f);
						#endif

						this.catalog.GemBundleList.Add(tmpGemPackageModel);
					}
					else if(tmp.ItemId.Contains("bundle_package_") && !tmp.ItemId.Contains("bundle_package_growth_"))
					{
						var tmpNormalPackageModel = new NormalPackageModel();
						tmpNormalPackageModel.Id = tmp.ItemId;
						tmpNormalPackageModel.Name = tmp.DisplayName;

						foreach(var tmp2 in PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<Dictionary<string, string>>(tmp.CustomData))
						{
							if(tmp2.Key == "package_value")
								tmpNormalPackageModel.PackageBuffValue = tmp2.Value;
							else if(tmp2.Key == "background_idx")
								tmpNormalPackageModel.PackageBackgroundIndex = Convert.ToInt32(tmp2.Value);
							else if(tmp2.Key == "is_new")
								tmpNormalPackageModel.IsNew = Convert.ToBoolean(tmp2.Value);
							else if(tmp2.Key == "is_hot")
								tmpNormalPackageModel.IsHot = Convert.ToBoolean(tmp2.Value);
							else if(tmp2.Key == "is_limited")
								tmpNormalPackageModel.IsLimited = Convert.ToBoolean(tmp2.Value);
							else if(tmp2.Key == "is_available")
								tmpNormalPackageModel.IsAvailable = Convert.ToBoolean(tmp2.Value);
							else if(tmp2.Key == "total_purchasable_per_period")
								tmpNormalPackageModel.TotalPurchasablePerPeriod = Convert.ToInt32(tmp2.Value);
							else if(tmp2.Key == "avail_info_in_period_idx")
								tmpNormalPackageModel.AvailInfoInPeriodIndex = Convert.ToInt32(tmp2.Value);
						}

						if(tmp.Bundle.BundledVirtualCurrencies.ContainsKey("GE"))
							tmpNormalPackageModel.GemReward = (int)tmp.Bundle.BundledVirtualCurrencies["GE"];

						if(tmp.Bundle.BundledVirtualCurrencies.ContainsKey("FE"))
							tmpNormalPackageModel.FeatherReward = (int)tmp.Bundle.BundledVirtualCurrencies["FE"];
						
						if(tmp.Bundle.BundledVirtualCurrencies.ContainsKey("VP"))
							tmpNormalPackageModel.VIPReward = (int)tmp.Bundle.BundledVirtualCurrencies["VP"];
						
						if(tmp.Bundle.BundledVirtualCurrencies.ContainsKey("BS"))
							tmpNormalPackageModel.BonusStatReward = (int)tmp.Bundle.BundledVirtualCurrencies["BS"];

						tmpNormalPackageModel.PriceType = GoodsPriceType.CASH;

						#if UNITY_EDITOR
							tmpNormalPackageModel.RealMoneyPrice = string.Format("${0}", (float)tmp.VirtualCurrencyPrices["RM"] / 100.0f);
						#endif

						this.catalog.NormalPackageList.Add(tmpNormalPackageModel);
					}
					else if(tmp.ItemId.Contains("bundle_package_growth_"))
					{
						var tmpGrowthPackageModel = new GrowthPackageModel();
						tmpGrowthPackageModel.Id = tmp.ItemId;
						tmpGrowthPackageModel.Name = tmp.DisplayName;

						foreach(var tmp2 in PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<Dictionary<string, string>>(tmp.CustomData))
						{
							if(tmp2.Key == "package_value")
								tmpGrowthPackageModel.PackageValue = tmp2.Value;
							else if(tmp2.Key == "is_new")
								tmpGrowthPackageModel.IsNew = Convert.ToBoolean(tmp2.Value);
							else if(tmp2.Key == "is_hot")
								tmpGrowthPackageModel.IsHot = Convert.ToBoolean(tmp2.Value);
							else if(tmp2.Key == "is_available")
								tmpGrowthPackageModel.IsAvailable = Convert.ToBoolean(tmp2.Value);
							else if(tmp2.Key == "growth_reward_list")
							{
								tmpGrowthPackageModel.GrowthRewardList = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<List<GrowthRewardModel>>(tmp2.Value);
							}
						}

						if(tmp.Bundle.BundledVirtualCurrencies.ContainsKey("GE"))
							tmpGrowthPackageModel.GemReward = (int)tmp.Bundle.BundledVirtualCurrencies["GE"];

						if(tmp.Bundle.BundledVirtualCurrencies.ContainsKey("VP"))
							tmpGrowthPackageModel.VIPReward = (int)tmp.Bundle.BundledVirtualCurrencies["VP"];
					
						tmpGrowthPackageModel.PriceType = GoodsPriceType.CASH;

						#if UNITY_EDITOR
							tmpGrowthPackageModel.RealMoneyPrice = string.Format("${0}", (float)tmp.VirtualCurrencyPrices["RM"] / 100.0f);
						#endif

						this.catalog.GrowthPackageList.Add(tmpGrowthPackageModel);
					}
					else if(tmp.ItemId != "wing_body" && tmp.ItemId.Contains("wing_"))
					{
						var tmpWingProductModel = new WingProductModel();
						tmpWingProductModel.Id = tmp.ItemId;

						// If price type is cash. it means that wing only can get from package.
						if(tmp.VirtualCurrencyPrices.ContainsKey("GE"))
						{
							tmpWingProductModel.PriceType = GoodsPriceType.GEM;
							tmpWingProductModel.Price = (int)tmp.VirtualCurrencyPrices["GE"];
						}
						else if(tmp.VirtualCurrencyPrices.ContainsKey("FE"))
						{
							tmpWingProductModel.PriceType = GoodsPriceType.FEATHER;
							tmpWingProductModel.Price = (int)tmp.VirtualCurrencyPrices["FE"];
						}
						else
						{
							tmpWingProductModel.PriceType = GoodsPriceType.PACKAGE;
							tmpWingProductModel.Price = 0;
						}

						foreach(var tmp2 in PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<Dictionary<string, string>>(tmp.CustomData))
						{
							if(tmp2.Key == "is_available")
								tmpWingProductModel.IsAvailable = Convert.ToBoolean(tmp2.Value);
						}

						this.catalog.WingProductList.Add(tmpWingProductModel);
					}
				}

                // Init PurchaseManager
                PurchaseManager.Instance.Initialize(this.catalog.GemBundleList, this.catalog.NormalPackageList, this.catalog.GrowthPackageList);

                // Need to get inventory
				GetInventory(onSuccess, onFailure);
			}),
			(error) =>
			{
                Debug.Log(error.ErrorMessage);
				onFailure?.Invoke();
				return;
			}
		);
	}

#endregion

#region Bind Guest Account to Social Platforms

	public void LinkAccountWithCustomAuthkey(string newAuthkey, Action onSuccess, Action onFailure)
	{
		this.AuthKey = newAuthkey;
		LinkCustomIDRequest request = new LinkCustomIDRequest()
		{
			CustomId = this.AuthKey,
			ForceLink = false
		};

		PlayFabClientAPI.LinkCustomID(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.LinkAccountWithCurrentAuthkey: " + AuthKey);

				onSuccess?.Invoke();

			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);
				onFailure?.Invoke();
				return;
			}
		);
	}

	public void LinkAccountWithGameCenterAuthkey(string newAuthkey, Action onSuccess, Action onFailure)
	{
		this.AuthKey = newAuthkey;
		LinkGameCenterAccountRequest request = new LinkGameCenterAccountRequest()
		{
			GameCenterId = this.AuthKey,
			ForceLink = false
		};

		PlayFabClientAPI.LinkGameCenterAccount(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.LinkAccountWithCurrentAuthkey: " + AuthKey);

				onSuccess?.Invoke();

			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);
				onFailure?.Invoke();
				return;
			}
		);
	}

	public void Logout()
	{
		this.isLoggedIn = false;
	}

#endregion

#region For Check Last Device

	public void RefreshLastDevice(bool isForLogin, Action<bool, bool, bool> onSuccess, Action<bool> onFailure)
	{
		var deviceTag = SystemInfo.deviceModel + "_" + SystemInfo.deviceUniqueIdentifier;

		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "refreshLastDevice",
				GeneratePlayStreamEvent = false,
				FunctionParameter = new {isForLogin = isForLogin, deviceTag = deviceTag},
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "refreshLastDevice",
				FunctionParameter = new {isForLogin = isForLogin, deviceTag = deviceTag},
				GeneratePlayStreamEvent = false
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.RefreshLastDevice");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Refresh Last Device Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke(false);
							return;
						}
					}

					setting.MinAndroidVersion = "0.0.0";
					if(jsonResult.ContainsKey("minAndroidVersionCode"))
					{
						object valueToUse;
						jsonResult.TryGetValue("minAndroidVersionCode", out valueToUse);
						setting.MinAndroidVersion = Convert.ToString(valueToUse);
					}
					
					setting.MiniOSVersion = "0.0.0";
					if(jsonResult.ContainsKey("miniOSVersionCode"))
					{
						object valueToUse;
						jsonResult.TryGetValue("miniOSVersionCode", out valueToUse);
						setting.MiniOSVersion = Convert.ToString(valueToUse);
					}

					setting.MinLoginCode = 0;
					if(jsonResult.ContainsKey("minLoginCode"))
					{
						object valueToUse;
						jsonResult.TryGetValue("minLoginCode", out valueToUse);
						setting.MinLoginCode = Convert.ToInt32(valueToUse);
					}
					
					if(jsonResult.ContainsKey("willOtherClose"))
					{
						object valueToUse;
						jsonResult.TryGetValue("willOtherClose", out valueToUse);
						
						if(Convert.ToBoolean(valueToUse))
						{
							
							if(jsonResult.ContainsKey("otherWasPlayingRecently"))
							{
								jsonResult.TryGetValue("otherWasPlayingRecently", out valueToUse);
								if(Convert.ToBoolean(valueToUse))
								{
									onSuccess?.Invoke(true, true, false);
									return;
								}
								else
								{
									onSuccess?.Invoke(true, false, false);
									return;
								}
							}
							else
							{
								onSuccess?.Invoke(true, false, false);
								return;
							}
						}
					}
					
					if(jsonResult.ContainsKey("needClose"))
					{
						object valueToUse;
						jsonResult.TryGetValue("needClose", out valueToUse);
						
						if(Convert.ToBoolean(valueToUse))
						{
							onSuccess?.Invoke(false, false, true);
							return;
						}
					}

					if(jsonResult.ContainsKey("isChatBanned"))
					{
						object valueToUse;
						jsonResult.TryGetValue("isChatBanned", out valueToUse);
						
						this.isChatBanned = Convert.ToBoolean(valueToUse);
					}

					onSuccess?.Invoke(false, false, false);
				}
				else
				{
					Debug.Log("ServerNetworkManager.RefreshLastDevice: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				if(error.Error == PlayFabErrorCode.AccountBanned)
				{
					onFailure?.Invoke(true);
					return;
				}
				else
				{
					onFailure?.Invoke(false);
					return;
				}
			}
		);
	}

#endregion

#region For Inventory	
	public void GetInventory(Action onSuccess, Action onFailure)
	{
		loginChainPhaseText = "Retrieving Inventory Data";
		GetUserInventoryRequest request = new GetUserInventoryRequest();

		PlayFabClientAPI.GetUserInventory(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.GetInventory");

				this.isUsedYT10Coupon = false;
				this.purchasedGrowthPackageList.Clear();
				this.purchasedBasicPackageList.Clear();
				this.purchasedLimitedPackageList.Clear();
				this.purchasedWingList.Clear();

				// Check owend wing in purchased list
				bool isOwnedWingIndexAvailable = false;

				// Wing owned Stat
				int totalWingAttackBonus = 0;
				int totalWingGoldBonus = 0;

				// Wing equipment stat
				int equipmentStatBonus1 = 0;
				int equipmentStatBonus2 = 0;
				int equipmentStatBonus3 = 0;

				// Reset Wing body
				this.wingBodyInfo = new WingBodyInstanceModel(false, null);

				foreach(var tmp in result.Inventory)
				{
					if(tmp.ItemId.Contains("package"))
					{
						// Save purchase package id depand on category
						if(tmp.ItemId.Contains("bundle_package_growth"))
							this.purchasedGrowthPackageList.Add(tmp.ItemId);
						else
						{
							var targetPackageInfo = this.catalog.NormalPackageList.FirstOrDefault(n => n.Id == tmp.ItemId);

							if(targetPackageInfo != null && targetPackageInfo.IsLimited)
							{
								if(tmp.ItemId.Contains("bundle_package_basic_"))
									this.purchasedBasicPackageList.Add(tmp.ItemId);
								else
									this.purchasedLimitedPackageList.Add(tmp.ItemId);
							}
						}
						
						// TODO: Once is_stack_package is true, remove below
						// Get recent daily and weekly package purchase date time
						if(tmp.ItemId == "bundle_package_daily")
							this.nextAvailableDailyPackageTimeSeconds = (tmp.Expiration == null) ? new DateTime() : tmp.Expiration.Value;
						else if(tmp.ItemId == "bundle_package_daily_2")
							this.nextAvailableDailyPackage2TimeSeconds = (tmp.Expiration == null) ? new DateTime() : tmp.Expiration.Value;
						else if(tmp.ItemId == "bundle_package_weekly")
							this.nextAvailableWeeklyPackageTimeSeconds = (tmp.Expiration == null) ? new DateTime() : tmp.Expiration.Value;
						else if(tmp.ItemId == "bundle_package_weekly_2")
							this.nextAvailableWeeklyPackage2TimeSeconds = (tmp.Expiration == null) ? new DateTime() : tmp.Expiration.Value;
					}
					else if(tmp.ItemId == "bundle_coupon_yt10")
						this.isUsedYT10Coupon = true;
					else if(tmp.ItemId == "wing_body")
						this.wingBodyInfo = new WingBodyInstanceModel(true, tmp.CustomData);
					else if(tmp.ItemId != "wing_body" && tmp.ItemId.Contains("wing"))
					{
						WingInstanceModel tmpWingInstance = null;
						
						if(tmp.CustomData == null)
							tmpWingInstance = GetDefaultWingInstance(tmp.ItemId);
						else
							tmpWingInstance = new WingInstanceModel(tmp.ItemId, tmp.CustomData);

						this.purchasedWingList.Add(tmpWingInstance);

						// Check equipment wing
						if(tmpWingInstance.IsEquipment)
						{
							isOwnedWingIndexAvailable = true;
							equipmentStatBonus1 = tmpWingInstance.EquipmentStatBonus1Value;
							equipmentStatBonus2 = tmpWingInstance.EquipmentStatBonus2Value;
							equipmentStatBonus3 = tmpWingInstance.EquipmentStatBonus3Value;	
						}

						// Caluate total wing owned bonus
						totalWingAttackBonus = totalWingAttackBonus + tmpWingInstance.OwnedStatBonus1Value;
						totalWingGoldBonus = totalWingGoldBonus + tmpWingInstance.OwnedStatBonus2Value;
					}
				}

				if(result.VirtualCurrency.ContainsKey("GE"))
					this.inventory.Gem = result.VirtualCurrency["GE"];
				if(result.VirtualCurrency.ContainsKey("VP"))
					this.inventory.VipPoint = result.VirtualCurrency["VP"];
				if(result.VirtualCurrency.ContainsKey("BS"))
					this.inventory.BonusStat = result.VirtualCurrency["BS"];
				if(result.VirtualCurrency.ContainsKey("DR"))
					this.inventory.DailyRewardTicket = result.VirtualCurrency["DR"];
				if(result.VirtualCurrency.ContainsKey("FE"))
					this.inventory.Feather = result.VirtualCurrency["FE"];
				if(result.VirtualCurrency.ContainsKey("GT"))
					this.inventory.GuildToken = result.VirtualCurrency["GT"];

				// Refresh total wing attack and gold bonus
				this.user.TotalWingAttackBonus = totalWingAttackBonus * this.wingBodyInfo.OwnedStatBonusValue;
				this.user.TotalWingGoldBonus = totalWingGoldBonus * this.wingBodyInfo.OwnedStatBonusValue;

				// Refresh equipment bonus
				ServerNetworkManager.Instance.User.SkillInfo[0].BonusLevel = equipmentStatBonus1 + this.wingBodyInfo.EquipmentStatBonusValue;
				ServerNetworkManager.Instance.User.SkillInfo[1].BonusLevel = equipmentStatBonus2 + this.wingBodyInfo.EquipmentStatBonusValue;
				ServerNetworkManager.Instance.User.SkillInfo[2].BonusLevel = equipmentStatBonus3 + this.wingBodyInfo.EquipmentStatBonusValue;

				// Check owend wing index available
				if(!isOwnedWingIndexAvailable)
					this.inventory.WingIndex = -1;

				onSuccess?.Invoke();
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);
				onFailure?.Invoke();
				return;
			}
		);
	}
#endregion

#region For Player Info

	public void JoinVirtualServer(string serverId, Action<string> onSuccess, Action onFailure)
	{
		user.ServerId = serverId;

		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "updateVirtualServer",
				FunctionParameter = new {serverId = serverId},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "updateVirtualServer",
				FunctionParameter = new {serverId = serverId},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.JoinVirtualServer.Server Id: " + serverId);

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Request Attached Item Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							serverId = "";
							onFailure?.Invoke();
							return;
						}
					}

					var resultServerId = "";
					if(jsonResult.ContainsKey("serverId"))
					{
						object valueToUse;
						jsonResult.TryGetValue("serverId", out valueToUse);

						resultServerId = Convert.ToString(valueToUse);
					}

					onSuccess?.Invoke(resultServerId);
				}
				else
				{
					Debug.Log("ServerNetworkManager.JoinVirtualServer: No json result exists");
					serverId = "";
					onFailure?.Invoke();
					return;
				}
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);
				serverId = "";
				onFailure?.Invoke();
				return;
			}
		);
	}

	public void SetPlayerName(string nickNameToUse, bool isFree, Action onSuccess, Action<bool> onFailure)
	{	
		UpdateUserTitleDisplayNameRequest request = new UpdateUserTitleDisplayNameRequest()
		{
			DisplayName = nickNameToUse
		};

		PlayFabClientAPI.UpdateUserTitleDisplayName(
			request,
			(result) =>
			{
				Debug.Log("ServerNetworkManager.SetName");
				Debug.Log("   >>>>>> nickname: " + result.DisplayName);
			
				this.user.NickName = result.DisplayName;

				if(!isFree)
				{
					this.UnsyncedGem -= 500;
					this.user.UnsyncedGemChangeCount ++;
				}

				onSuccess?.Invoke();
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);
				
				bool isDuplicated= false;
				if(error.Error == PlayFabErrorCode.NameNotAvailable)
					isDuplicated = true;
				
				onFailure?.Invoke(isDuplicated);
				return;
			}
		);
	}

	public void SetProfileLanguage(string language, Action onSuccess, Action onFailure)
	{
		var request = new PlayFab.ProfilesModels.SetProfileLanguageRequest
		{
			Language = language
		};
		PlayFabProfilesAPI.SetProfileLanguage(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.SetProfileLanguage");
				
				onSuccess?.Invoke();
			}, 
			(error) =>
			{
				Debug.Log(error.ErrorMessage);
				
				onFailure?.Invoke();
			}
		);
	}

	public void SyncPlayerData(bool localCacheLost, string syncOrigin, Action onSuccess, Action<string> onFailure)
	{
		var serverInventoryDataModel = ServerInventoryModel.FromLocalModel(this.inventory);
		var inventoryData = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).SerializeObject(serverInventoryDataModel);

		var serverUserDataModel = ServerUserModel.FromLocalModel(this.user);
		var userData = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).SerializeObject(serverUserDataModel);

		var progressValue = serverInventoryDataModel.ProgressPoint + serverUserDataModel.ProgressPoint;

		var dps = (int)Math.Min(int.MaxValue, BigInteger.Log(NumberMainController.Instance.DPS) * 10000d);

		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "syncPlayerData",
				FunctionParameter = new {
					localCacheLost = localCacheLost,
					gemsChange = (int)UnsyncedGem,
					unsyncedGemChangeVersion = ServerNetworkManager.Instance.User.UnsyncedGemChangeCount,
					inventoryData = inventoryData,
					userData = userData,
					progressValue = progressValue,
					stageProgress = (int)this.user.StageProgress,
					dps = dps,
					killedMobsForCurrentStage = (int)ServerNetworkManager.Instance.User.KillCountForBestStage,
					isSuspicious = AntiCheatManager.Instance.IsMemorySuspicious,
					syncOrigin = syncOrigin
				},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "syncPlayerData",
				FunctionParameter = new {
					localCacheLost = localCacheLost,
					gemsChange = (int)UnsyncedGem,
					unsyncedGemChangeVersion = ServerNetworkManager.Instance.User.UnsyncedGemChangeCount,
					inventoryData = inventoryData,
					userData = userData,
					progressValue = progressValue,
					stageProgress = (int)this.user.StageProgress,
					dps = dps,
					killedMobsForCurrentStage = (int)ServerNetworkManager.Instance.User.KillCountForBestStage,
					isSuspicious = AntiCheatManager.Instance.IsMemorySuspicious,
					syncOrigin = syncOrigin
				},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.SyncPlayerData:");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Request Sync Player Data Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							Debug.Log("ServerNetworkManager.SyncPlayerData: Failed to sync");

							if(jsonResult.ContainsKey("reasonOfError"))
							{
								jsonResult.TryGetValue("reasonOfError", out valueToUse);
								
								onFailure?.Invoke(Convert.ToString(valueToUse));
								return;
							}
							else
							{
								onFailure?.Invoke("unknown");
								return;
							}
						}
					}

					// As sync of gem has been successfully done, we need to reset these values
					this.inventory.Gem += UnsyncedGem;
					this.UnsyncedGem = 0;
					
					// Normal case
					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.SyncPlayerData: Failed to sync");

					onFailure?.Invoke("unknown");
					return;
				}
			},
			(error) =>
			{
				Debug.Log("ServerNetworkManager.SyncPlayerData: Failed to sync");
				
				if(error.Error == PlayFabErrorCode.AccountBanned)
					onFailure?.Invoke("ban");
				else
					onFailure?.Invoke("unknown");

				return;
			}
		);
	}

#endregion

#region For Moderator

	public void RequestChatBan(string targetPlayfabId, string chatLog, Action onSuccess, Action<string> onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestChatBan",
				FunctionParameter = new {targetPlayfabId = targetPlayfabId, chatLog = chatLog},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestChatBan",
				FunctionParameter = new {targetPlayfabId = targetPlayfabId, chatLog = chatLog},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.RequestChatBan");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Request Chat Ban Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);
						
						if(Convert.ToBoolean(valueToUse))
						{
							if(jsonResult.ContainsKey("reasonOfError"))
							{
								jsonResult.TryGetValue("reasonOfError", out valueToUse);
								
								onFailure?.Invoke(Convert.ToString(valueToUse));
								return;
							}
							else
							{
								onFailure?.Invoke("unknown");
								return;
							}
						}
					}

					if(jsonResult.ContainsKey("updatedBanData"))
					{
						object valueToUse;
						jsonResult.TryGetValue("updatedBanData", out valueToUse);
						var json = Convert.ToString(valueToUse);

						this.chatBanInfo = JsonUtility.FromJson<ChatBanInfoListModel>(json) as ChatBanInfoListModel;
					}

					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.RequestChatBan: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke("unknown");
				return;
			}
		);
	}

	public void RequestChatBanCancel(string targetPlayfabId, Action onSuccess, Action<string> onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestChatBanCancel",
				FunctionParameter = new {targetPlayfabId = targetPlayfabId},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestChatBanCancel",
				FunctionParameter = new {targetPlayfabId = targetPlayfabId},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.RequestChatBanCancel");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Request Chat Ban Cancel Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(jsonResult.ContainsKey("reasonOfError"))
							{
								jsonResult.TryGetValue("reasonOfError", out valueToUse);
								
								onFailure?.Invoke(Convert.ToString(valueToUse));
								return;
							}
							else
							{
								onFailure?.Invoke("unknown");
								return;
							}
						}
					}

					if(jsonResult.ContainsKey("updatedBanData"))
					{
						object valueToUse;
						jsonResult.TryGetValue("updatedBanData", out valueToUse);
						var json = Convert.ToString(valueToUse);

						this.chatBanInfo = JsonUtility.FromJson<ChatBanInfoListModel>(json) as ChatBanInfoListModel;
					}

					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.RequestChatBanCancel: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke("unknown");
				return;
			}
		);
	}

	public void RequestChatLog(string targetPlayfabId, Action<string> onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestChatLog",
				FunctionParameter = new {targetPlayfabId = targetPlayfabId},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestChatLog",
				FunctionParameter = new {targetPlayfabId = targetPlayfabId},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.RequestChatLog");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var chatBanLog = "";
				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Request Chat Ban Log Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}

					if(jsonResult.ContainsKey("chatLog"))
					{
						object valueToUse;
						jsonResult.TryGetValue("chatLog", out valueToUse);
						chatBanLog = Convert.ToString(valueToUse);
					}

					onSuccess?.Invoke(chatBanLog);
				}
				else
				{
					Debug.Log("ServerNetworkManager.RequestChatLog: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke();
				return;
			}
		);
	}

#endregion

#region For Chat Block

	public void AddChatBlock(string targetPlayfabId, string targetNickname, Action onSuccess, Action<string> onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "addChatBlockList",
				FunctionParameter = new {targetId = targetPlayfabId, targetNickname = targetNickname},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "addChatBlockList",
				FunctionParameter = new {targetId = targetPlayfabId, targetNickname = targetNickname},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.AddChatBlock");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Request Add Chat Block Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);
						
						if(Convert.ToBoolean(valueToUse))
						{
							if(jsonResult.ContainsKey("reasonOfError"))
							{
								jsonResult.TryGetValue("reasonOfError", out valueToUse);
								
								onFailure?.Invoke(Convert.ToString(valueToUse));
								return;
							}
							else
							{
								onFailure?.Invoke("unknown");
								return;
							}
						}
					}

					if(jsonResult.ContainsKey("updatedChatBlockData"))
					{
						object valueToUse;
						jsonResult.TryGetValue("updatedChatBlockData", out valueToUse);
						var json = Convert.ToString(valueToUse);

						this.chatBlockInfo = JsonUtility.FromJson<ChatBlockInfoListModel>(json) as ChatBlockInfoListModel;
					}

					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.AddChatBlock: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);
				onFailure?.Invoke("unknown");
			}
		);
	}

	public void RemoveChatBlock(string targetPlayfabId, Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "removeChatBlockList",
				FunctionParameter = new {targetId = targetPlayfabId},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "removeChatBlockList",
				FunctionParameter = new {targetId = targetPlayfabId},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.RemoveChatBlock");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Remove Chat Block Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}

					if(jsonResult.ContainsKey("updatedChatBlockData"))
					{
						object valueToUse;
						jsonResult.TryGetValue("updatedChatBlockData", out valueToUse);
						var json = Convert.ToString(valueToUse);

						this.chatBlockInfo = JsonUtility.FromJson<ChatBlockInfoListModel>(json) as ChatBlockInfoListModel;
					}

					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.RemoveChatBlock: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);
				onFailure?.Invoke();
			}
		);
	}
	
#endregion

#region For Rankings

	public void GetStageMyRanking(Action onSuccess, Action onFailure)
	{
		var statName = (ServerNetworkManager.Instance.user.ServerId == "") ? "ranking" : "ranking_" + ServerNetworkManager.Instance.user.ServerId;
		var request = new GetLeaderboardAroundPlayerRequest()
		{
			MaxResultsCount = 1,
			StatisticName = statName
		};

		PlayFabClientAPI.GetLeaderboardAroundPlayer(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.GetStageMyRanking");
				
				stageRank.MyRankingModel = new ArenaRankModel();
				var myData = result.Leaderboard.FirstOrDefault(n => n.PlayFabId == user.PlayFabId);

				if(myData != null)
				{
					stageRank.MyRankingModel.name = myData.DisplayName;
					stageRank.MyRankingModel.position = myData.Position + 1;
					stageRank.MyRankingModel.score = myData.StatValue;
					this.rankingPosition = myData.Position + 1;
				}
				else
					stageRank.MyRankingModel = null;

				onSuccess?.Invoke();
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);
				onFailure?.Invoke();
			}
		);
	}

	public void GetStageRankingAdvanced(Action onSuccess, Action onFailure)
	{
		var statName = (ServerNetworkManager.Instance.user.ServerId == "") ? "ranking" : "ranking_" + ServerNetworkManager.Instance.user.ServerId;
		var request = new GetLeaderboardRequest()
		{
			MaxResultsCount = 100,
        	StartPosition = 0,
			StatisticName = statName
		};

		var statName2 = (ServerNetworkManager.Instance.user.ServerId == "") ? "dps" : "dps_" + ServerNetworkManager.Instance.user.ServerId;
		var request2 = new GetLeaderboardRequest()
		{
			MaxResultsCount = 100,
        	StartPosition = 0,
			StatisticName = statName2
		};

		PlayFabClientAPI.GetLeaderboard(
			request, 
			(result) =>
			{
				PlayFabClientAPI.GetLeaderboard(
					request2, 
					(result2) =>
					{
						Debug.Log("ServerNetworkManager.GetStageRankingAdvanced");
						
						this.stageRank = new RankingModel();
						this.stageRank.RankingList = new List<ArenaRankModel>();

						foreach(var tmp in result.Leaderboard)
						{
							var tmpRankingModel = new ArenaRankModel();

							tmpRankingModel.name = tmp.DisplayName;
							tmpRankingModel.score = tmp.StatValue;

							stageRank.RankingList.Add(tmpRankingModel);
						}

						foreach(var tmp in stageRank.RankingList)
						{
							var target = result2.Leaderboard.FirstOrDefault(n => n.DisplayName == tmp.name);
							if(target != null)
							{
								tmp.subScore = target.StatValue;
							}
						}

						// Sort based on ranking
						stageRank.RankingList = stageRank.RankingList.OrderByDescending(n => n.subScore).OrderByDescending(n => n.score).ToList();
						
						int i = 1;
						foreach(var tmp in stageRank.RankingList)
						{
							tmp.position = i;
							if(tmp.name == this.User.NickName)
							{
								stageRank.MyRankingModel = new ArenaRankModel();
								stageRank.MyRankingModel.name = tmp.name;
								stageRank.MyRankingModel.position = tmp.position;
								stageRank.MyRankingModel.score = tmp.score;
								this.rankingPosition = tmp.position;
							}
							i++;
						}

						if(stageRank.MyRankingModel == null)
						{
							Debug.Log("ServerNetworkManager.GetStageRankingAdvanced: Not in top 100. Need to get my ranking again.");
							GetStageMyRanking(onSuccess, onFailure);
						}
						else
						{
							onSuccess?.Invoke();
						}
					},
					(error) =>
					{
						Debug.Log(error.ErrorMessage);
						onFailure?.Invoke();
					}
				);
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);
				onFailure?.Invoke();
			}
		);
	}

	public void GetArenaMyRanking(Action onSuccess, Action onFailure)
	{
		var statName = (ServerNetworkManager.Instance.user.ServerId == "") ? "arena" : "arena_" + ServerNetworkManager.Instance.user.ServerId;
		var request = new GetLeaderboardAroundPlayerRequest()
		{
			MaxResultsCount = 1,
			StatisticName = statName
		};

		PlayFabClientAPI.GetLeaderboardAroundPlayer(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.GetArenaMyRanking");
				
				this.arenaRank.MyRankingModel = new ArenaRankModel();
				var myData = result.Leaderboard.FirstOrDefault(n => n.PlayFabId == user.PlayFabId);

				if(myData != null)
				{
					this.arenaRank.MyRankingModel = new ArenaRankModel();
					this.arenaRank.MyRankingModel.name = myData.DisplayName;
					this.arenaRank.MyRankingModel.position = myData.Position + 1;
					this.arenaRank.MyRankingModel.score = myData.StatValue;
				}
				else
					this.arenaRank.MyRankingModel = null;

				onSuccess?.Invoke();
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);
				onFailure?.Invoke();
			}
		);
	}

	public void GetArenaRanking(Action onSuccess, Action onFailure)
	{
		var statName = (ServerNetworkManager.Instance.user.ServerId == "") ? "arena" : "arena_" + ServerNetworkManager.Instance.user.ServerId;
		var request = new GetLeaderboardRequest()
		{
			MaxResultsCount = 100,
        	StartPosition = 0,
			StatisticName = statName
		};

		PlayFabClientAPI.GetLeaderboard(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.GetArenaRanking");
				
				this.arenaRank = new RankingModel();
				this.arenaRank.RankingList = new List<ArenaRankModel>();
				
				foreach(var tmp in result.Leaderboard)
				{
					var tmpRankingModel = new ArenaRankModel();

					tmpRankingModel.name = tmp.DisplayName;
					tmpRankingModel.position = tmp.Position + 1;
					tmpRankingModel.score = tmp.StatValue;

					arenaRank.RankingList.Add(tmpRankingModel);
				}

				onSuccess?.Invoke();
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);
				onFailure?.Invoke();
			}
		);
	}

	public void GetTotalArenaMyRanking(Action onSuccess, Action onFailure)
	{
		var statName = (ServerNetworkManager.Instance.user.ServerId == "") ? "arena_all_season" : "arena_all_season_" + ServerNetworkManager.Instance.user.ServerId;
		var request = new GetLeaderboardAroundPlayerRequest()
		{
			MaxResultsCount = 1,
			StatisticName = statName
		};

		PlayFabClientAPI.GetLeaderboardAroundPlayer(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.GetTotalArenaMyRanking");
				
				this.totalArenaRank.MyRankingModel = new ArenaRankModel();
				var myData = result.Leaderboard.FirstOrDefault(n => n.PlayFabId == user.PlayFabId);

				if(myData != null)
				{
					this.totalArenaRank.MyRankingModel = new ArenaRankModel();
					this.totalArenaRank.MyRankingModel.name = myData.DisplayName;
					this.totalArenaRank.MyRankingModel.position = myData.Position + 1;
					this.totalArenaRank.MyRankingModel.score = myData.StatValue;
				}
				else
					this.totalArenaRank.MyRankingModel = null;

				onSuccess?.Invoke();
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);
				onFailure?.Invoke();
			}
		);
	}

	public void GetTotalArenaRanking(Action onSuccess, Action onFailure)
	{
		var statName = (ServerNetworkManager.Instance.user.ServerId == "") ? "arena_all_season" : "arena_all_season_" + ServerNetworkManager.Instance.user.ServerId;
		var request = new GetLeaderboardRequest()
		{
			MaxResultsCount = 100,
        	StartPosition = 0,
			StatisticName = statName
		};

		PlayFabClientAPI.GetLeaderboard(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.GetTotalArenaRanking");
			
				this.totalArenaRank = new RankingModel();
				this.totalArenaRank.RankingList = new List<ArenaRankModel>();
				
				foreach(var tmp in result.Leaderboard)
				{
					var tmpRankingModel = new ArenaRankModel();

					tmpRankingModel.name = tmp.DisplayName;
					tmpRankingModel.position = tmp.Position + 1;
					tmpRankingModel.score = tmp.StatValue;

					this.totalArenaRank.RankingList.Add(tmpRankingModel);
				}

				onSuccess?.Invoke();
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);
				onFailure?.Invoke();
			}
		);
	}

#endregion

#region For Shop
	public void ValidateiOSPurchase(string receipt, string currencyCode, int purchasePrice, Action onSuccess, Action onFailure)
	{
		ValidateIOSReceiptRequest request = new ValidateIOSReceiptRequest()
		{
			ReceiptData = receipt,
			CurrencyCode = currencyCode,
			PurchasePrice = (currencyCode == "USD")? purchasePrice : purchasePrice * 100
		};

		PlayFabClientAPI.ValidateIOSReceipt(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.ValidateiOSPurchase");

				onSuccess?.Invoke();
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke();
				return;
			}
		);
	}

	public void ValidateAndroidPurchase(string receipt, string signature, Action onSuccess, Action onFailure)
	{
		ValidateGooglePlayPurchaseRequest request = new ValidateGooglePlayPurchaseRequest()
		{
			ReceiptJson = receipt,
			Signature = signature
		};

		PlayFabClientAPI.ValidateGooglePlayPurchase(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.ValidateGooglePlayPurchase");

				onSuccess?.Invoke();
			},
			(error) =>
			{
				Debug.Log (error.ErrorMessage);

				onFailure?.Invoke();
				return;
			}
		);
	}

	public void ValidateOnestorePurchase(string purchasedItemId, Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "validateOnestorePurchase",
				FunctionParameter = new {purchasedItemId = purchasedItemId},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "validateOnestorePurchase",
				FunctionParameter = new {purchasedItemId = purchasedItemId},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.ValidateOnestorePurchase." + purchasedItemId);

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Validate Onestore Purchase Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}

					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.ValidateOnestorePurchase: No json result exists");
					
					onFailure?.Invoke();
					return;
				}
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);
				
				onFailure?.Invoke();
				return;
			}
		);
	}

	public void RequestGemBonus(string purchasedGemBundleId, Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestGemBonus",
				FunctionParameter = new {purchasedGemBundleId = purchasedGemBundleId},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestGemBonus",
				FunctionParameter = new {purchasedGemBundleId = purchasedGemBundleId},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.RequestGemBonus." + purchasedGemBundleId);

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Request Gem Bonus Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}

					if(jsonResult.ContainsKey("updatedPurchasedGemBundleList"))
					{
						object valueToUse;
						jsonResult.TryGetValue("updatedPurchasedGemBundleList", out valueToUse);

						var json = Convert.ToString(valueToUse);

						this.purchasedGemBundleList = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<List<string>>(json);
					}

					if(jsonResult.ContainsKey("bonusGemAmount"))
					{
						object valueToUse;
						jsonResult.TryGetValue("bonusGemAmount", out valueToUse);

						this.inventory.Gem = this.inventory.Gem + (Convert.ToInt32(valueToUse) * 2);
					}

					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.RequestGemBonus: No json result exists");
					
					onFailure?.Invoke();
					return;
				}
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);
				
				onFailure?.Invoke();
				return;
			}
		);
	}

	public void CheckLimitedPackageAvailable(string targetPackageId, Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "checkLimitedPackageAvailable",
				FunctionParameter = new {targetPackageId = targetPackageId},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "checkLimitedPackageAvailable",
				FunctionParameter = new {targetPackageId = targetPackageId},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.CheckLimitedPackageAvailable." + targetPackageId);

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Request Check Limited Package Available Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}

					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.CheckLimitedPackageAvailable: No json result exists");
					
					onFailure?.Invoke();
					return;
				}
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);
				
				onFailure?.Invoke();
				return;
			}
		);
	}

	public void CheckDailyWeeklyPackageAvailable(Action<List<int>, List<int>, List<int>, List<int>> onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "checkDailyWeeklyPackageAvailableV2",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "checkDailyWeeklyPackageAvailableV2",
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.CheckDailyWeeklyPackageAvailable");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Request Check Daily Weekly Package Available Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}

					var dailyPackage1AvailList = new List<int>();
					if(jsonResult.ContainsKey("dailyPackage1Avail"))
					{
						object valueToUse;
						jsonResult.TryGetValue("dailyPackage1Avail", out valueToUse);
						dailyPackage1AvailList = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<List<int>>(Convert.ToString(valueToUse));
					}

					var dailyPackage3AvailList = new List<int>();
					if(jsonResult.ContainsKey("dailyPackage3Avail"))
					{
						object valueToUse;
						jsonResult.TryGetValue("dailyPackage3Avail", out valueToUse);
						dailyPackage3AvailList = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<List<int>>(Convert.ToString(valueToUse));
					}

					var weeklyPackage1AvailList = new List<int>();
					if(jsonResult.ContainsKey("weeklyPackage1Avail"))
					{
						object valueToUse;
						jsonResult.TryGetValue("weeklyPackage1Avail", out valueToUse);
						weeklyPackage1AvailList = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<List<int>>(Convert.ToString(valueToUse));
					}

					var weeklyPackage3AvailList = new List<int>();
					if(jsonResult.ContainsKey("weeklyPackage3Avail"))
					{
						object valueToUse;
						jsonResult.TryGetValue("weeklyPackage3Avail", out valueToUse);
						weeklyPackage3AvailList = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<List<int>>(Convert.ToString(valueToUse));
					}

					onSuccess?.Invoke(dailyPackage1AvailList, dailyPackage3AvailList, weeklyPackage1AvailList, weeklyPackage3AvailList);
				}
				else
				{
					Debug.Log("ServerNetworkManager.CheckDailyWeeklyPackageAvailable: No json result exists");
					
					onFailure?.Invoke();
					return;
				}
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);
				
				onFailure?.Invoke();
				return;
			}
		);
	}

	public void UseDailyWeeklyPackageIfAvailAndAsked(string targetPackageId, bool askForUsing, Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "useDailyWeeklyPackageIfAvailV2",
				FunctionParameter = new {targetPackageId = targetPackageId, askForUsing = askForUsing},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "useDailyWeeklyPackageIfAvailV2",
				FunctionParameter = new {targetPackageId = targetPackageId, askForUsing = askForUsing},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.UseDailyWeeklyPackageIfAvail." + targetPackageId);

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Request Use Daily Weekly Package If Avail Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}

					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.UseDailyWeeklyPackageIfAvail: No json result exists");
					
					onFailure?.Invoke();
					return;
				}
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);
				
				onFailure?.Invoke();
				return;
			}
		);
	}

#endregion

#region For Push
	public void RegisterForPush(byte[] token = null)
    {
        if(string.IsNullOrEmpty(PushToken) || string.IsNullOrEmpty(PlayFabSettings.TitleId))
            return;
		
		if(!isLoggedIn)
		{
			StartCoroutine(RetryRegisterForPush());
			return;
		}			

#if UNITY_ANDROID
        var request = new AndroidDevicePushNotificationRegistrationRequest {
            DeviceToken = PushToken,
            SendPushNotificationConfirmation = true,
            ConfirmationMessage = "Push notifications registered successfully"
        };
        PlayFabClientAPI.AndroidDevicePushNotificationRegistration(request, 
		(result)=>{
			Debug.Log("ServerNetworkManager.RegisterForPush: RegisterForPush is successfull.");
			PlayerPrefs.SetInt("push_registered", 1);
		}, 
		(error) => {
			Debug.Log("ServerNetworkManager.RegisterForPush: RegisterForPush is failed. " + error.ErrorMessage);

			if(!isFailedBefore)
				StartCoroutine(RetryRegisterForPush());
			
			isFailedBefore = true;
		});
#elif UNITY_IPHONE
		var request = new RegisterForIOSPushNotificationRequest {
			DeviceToken = PushToken,
			SendPushNotificationConfirmation = true,
			ConfirmationMessage = "Push notifications registered successfully"
		};
        PlayFabClientAPI.RegisterForIOSPushNotification(request, 
		(result) => {
            Debug.Log("ServerNetworkManager.RegisterForPush: RegisterForPush is successfull.");
			PlayerPrefs.SetInt("push_registered", 1);
        },
		(error) => {
			Debug.Log("ServerNetworkManager.RegisterForPush: RegisterForPush is failed. " + error.ErrorMessage);

			if(!isFailedBefore)
				StartCoroutine(RetryRegisterForPush());
			
			isFailedBefore = true;
		});
#endif
    }

	private IEnumerator RetryRegisterForPush()
	{
		yield return new WaitForSeconds(30f);
		RegisterForPush();
	}

	public void SetPushNotificationOff(bool isPushOff, Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "pushNotificationSetting",
				FunctionParameter = new {isPushOff = isPushOff},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "pushNotificationSetting",
				FunctionParameter = new {isPushOff = isPushOff},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.SetPushNotificationOff: " + isPushOff);

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Request Set PushNotification Off Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}
					
					OptionManager.Instance.IsPushOff = isPushOff;

					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.SetPushNotification: No json result exists");
					
					onFailure?.Invoke();
					return;
				}
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);
				
				onFailure?.Invoke();
				return;
			}
		);
	}
#endregion

#region Daily Reward
	public void GetDailyRewardInfo(Action<int> onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestDailyRewardInfo",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestDailyRewardInfo",
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.RequestDailyRewardInfo");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Request Daily Reward Info Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}		

					if(jsonResult.ContainsKey("dailyRewardTable"))
					{
						object valueToUse;
						jsonResult.TryGetValue("dailyRewardTable", out valueToUse);
						var json = Convert.ToString(valueToUse);

						this.dailyRewardTable = JsonUtility.FromJson<DailyRewardListModel>(json) as DailyRewardListModel;
					}
					else
					{
						onFailure?.Invoke();
						return;
					}

					if(jsonResult.ContainsKey("userDailyRewardCount"))
					{
						object valueToUse;
						jsonResult.TryGetValue("userDailyRewardCount", out valueToUse);
						this.dailyRewardCount = Convert.ToInt32(valueToUse);
					}

					if(jsonResult.ContainsKey("dailyRewardTicket"))
					{
						object valueToUse;
						jsonResult.TryGetValue("dailyRewardTicket", out valueToUse);
						this.inventory.DailyRewardTicket = Convert.ToInt32(valueToUse);
					}

					int dailyRewardTicketRechargeTimes = 0;
					if(jsonResult.ContainsKey("dailyRewardTicketRechargeTimes"))
					{
						object valueToUse;
						jsonResult.TryGetValue("dailyRewardTicketRechargeTimes", out valueToUse);
						dailyRewardTicketRechargeTimes = Convert.ToInt32(valueToUse);
					}

					onSuccess?.Invoke(dailyRewardTicketRechargeTimes);
				}
				else
				{
					Debug.Log("ServerNetworkManager.RequestDailyRewardInfo: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke();
				return;
			}
		);
	}

	public void RequestDailyReward(Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestDailyReward",
				FunctionParameter = new {},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestDailyReward",
				FunctionParameter = new {},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.RequestDailyReward");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Request Daily Reward Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}		

					var receivedItem = new DailyRewardModel();

					if(jsonResult.ContainsKey("itemId"))
					{
						object valueToUse;
						jsonResult.TryGetValue("itemId", out valueToUse);
						receivedItem.ItemId = Convert.ToString(valueToUse);
					}

					if(jsonResult.ContainsKey("count"))
					{
						object valueToUse;
						jsonResult.TryGetValue("count", out valueToUse);
						receivedItem.ItemCount = Convert.ToInt32(valueToUse);
					}

					if(jsonResult.ContainsKey("updatedDailyRewardCount"))
					{
						object valueToUse;
						jsonResult.TryGetValue("updatedDailyRewardCount", out valueToUse);
						this.dailyRewardCount = Convert.ToInt32(valueToUse);
					}

					if(receivedItem.ItemId == "gem")
						this.inventory.Gem = this.inventory.Gem + receivedItem.ItemCount;

					this.inventory.DailyRewardTicket = this.inventory.DailyRewardTicket - 1;

					onSuccess?.Invoke();	
				}
				else
				{
					Debug.Log("ServerNetworkManager.RequestDailyReward: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke();
				return;
			}
		);
	}
#endregion

#region For Coupon

	public void RedeemCoupon(string code, Action onSuccess, Action onFailure)
	{
		RedeemCouponRequest request = new RedeemCouponRequest()
		{
			CouponCode = code
		};

		PlayFabClientAPI.RedeemCoupon(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.RedeemCoupon");
				onSuccess?.Invoke();
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);
				
				onFailure?.Invoke();
				return;
			}
		);
	}

	public void OneTimeReward(string id, Action onSuccess, Action<String> onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "oneTimeReward",
				GeneratePlayStreamEvent = true,
				FunctionParameter = new {id = id},
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "oneTimeReward",
				FunctionParameter = new {id = id},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.OneTimeReward");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> One Time Reward: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);
						if(Convert.ToBoolean(valueToUse))
						{
							if(jsonResult.ContainsKey("reasonOfError"))
							{
								jsonResult.TryGetValue("reasonOfError", out valueToUse);
								
								onFailure?.Invoke(Convert.ToString(valueToUse));
								return;
							}
							else
							{
								
								onFailure?.Invoke("unknown");
								return;
							}
						}
					}

					if(jsonResult.ContainsKey("rewardGem"))
					{
						object valueToUse;
						jsonResult.TryGetValue("rewardGem", out valueToUse);
						this.inventory.Gem += Convert.ToInt32(valueToUse);
					}

					if(jsonResult.ContainsKey("rewardGold"))
					{
						object valueToUse;
						jsonResult.TryGetValue("rewardGold", out valueToUse);
						this.inventory.Gold += Convert.ToInt32(valueToUse);
					}

					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.OneTimeReward: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke("unknown");
				return;
			}
		);
	}

#endregion

#region For Notice
	public void GetTitleNews(Action onSuccess, Action onFailure)
	{
		noticeList = new List<NoticeModel>();
		GetTitleNewsRequest request = new GetTitleNewsRequest();
		request.Count = setting.MaxNoticeListCount;

		PlayFabClientAPI.GetTitleNews(
			request, 
			(result) => 
			{
				Debug.Log("SeverNetworkManager.GetTitleNews");

				noticeList.Clear();
				
				foreach(var tmp in result.News)
				{					
					var tmpNoticeModel = new NoticeModel();
					tmpNoticeModel.Id = tmp.NewsId;
					tmpNoticeModel.TimeText = tmp.Timestamp.ToShortDateString();
					tmpNoticeModel.TimeSeconds = Convert.ToInt32((tmp.Timestamp - new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime()).TotalSeconds);
					tmpNoticeModel.TitleText = tmp.Title;
					tmpNoticeModel.ContentText = tmp.Body;

					noticeList.Add(tmpNoticeModel);
				}

				noticeList = noticeList.OrderByDescending(n => n.TimeSeconds).ToList();

				onSuccess?.Invoke();
			}, 
			(error) => 
			{
				Debug.LogError(error.GenerateErrorReport());
			
				onFailure?.Invoke();
				return;
			}
		);
	}
#endregion

#region For Inbox
	public void GetUserMailList(Action onSuccess, Action onFailure)
	{
		GetUserDataRequest getUserDataRequest = new GetUserDataRequest(){};

		PlayFabClientAPI.GetUserReadOnlyData(
			getUserDataRequest,
			(getUserReadOnlyDataResult) =>
			{			
				// Get user mail list
				if(getUserReadOnlyDataResult.Data.ContainsKey("inbox"))
					this.post = JsonUtility.FromJson<PostListModel>(getUserReadOnlyDataResult.Data["inbox"].Value);

				// Get user system mail list
				if(getUserReadOnlyDataResult.Data.ContainsKey("inbox_system"))
					this.systemPost = JsonUtility.FromJson<SystemPostListModel>(getUserReadOnlyDataResult.Data["inbox_system"].Value);
			
				onSuccess?.Invoke();					
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke();
				return;
			}		
		);
	}

	public void RequestAttachedItem(int mailId, Action<string, int> onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestAttachedItem",
				FunctionParameter = new {mailId = mailId},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestAttachedItem",
				FunctionParameter = new {mailId = mailId},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.RequestAttachedItem.Mail ID: " + mailId);

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Request Attached Item Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}

					string itemId = "";
					int count = 0;

					if(jsonResult.ContainsKey("itemId"))
					{
						object valueToUse;
						jsonResult.TryGetValue("itemId", out valueToUse);

						itemId = Convert.ToString(valueToUse);
					}

					if(jsonResult.ContainsKey("count"))
					{
						object valueToUse;
						jsonResult.TryGetValue("count", out valueToUse);

						count = Convert.ToInt32(valueToUse);
					}

					if(itemId == "gem")
						inventory.Gem += count;
					else if(itemId == "guild_token")
						inventory.GuildToken += count;

					GetUserMailList(
						() => {
							onSuccess?.Invoke(itemId, count);
						},
						onFailure
					);
				}
				else
				{
					Debug.Log("ServerNetworkManager.RequestAttachedItem: No json result exists");
					
					onFailure?.Invoke();
					return;
				}
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);
				
				onFailure?.Invoke();
				return;
			}
		);
	}

	public void RequestAttachedItemAll(Action<PostRewardListModel> onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestAttachedItemAll",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestAttachedItemAll",
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.RequestAttachedItemAll");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Request Attached Item All Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					PostRewardListModel receivedInfo = null;

					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}

					if(jsonResult.ContainsKey("receiveItemList"))
					{
						object valueToUse;
						jsonResult.TryGetValue("receiveItemList", out valueToUse);
						var json = Convert.ToString(valueToUse);
						receivedInfo = JsonUtility.FromJson<PostRewardListModel>(json) as PostRewardListModel;
					}

					foreach(var tmp in receivedInfo.ItemList)
					{
						if(tmp.ItemId == "gem")
							inventory.Gem += (int)tmp.Count;
						else if(tmp.ItemId == "guild_token")
							inventory.GuildToken += (int)tmp.Count;
					}
					
					GetUserMailList(
						() => {
							onSuccess?.Invoke(receivedInfo);
						},
						onFailure
					);
				}
				else
				{
					Debug.Log("ServerNetworkManager.requestAttachedItemAll: No json result exists");
					
					onFailure?.Invoke();
					return;
				}
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);
				
				onFailure?.Invoke();
				return;
			}
		);
	}

	public void RequestDeleteMail(int mailId, Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestDeleteMail",
				FunctionParameter = new {mailId = mailId},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestDeleteMail",
				FunctionParameter = new {mailId = mailId},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.RequestDeleteMail.Mail ID: " + mailId);

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Request Delete Mail Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}

					// Refresh user mail list and inventory of the user
					GetUserMailList(
						() => {
							onSuccess?.Invoke();
						},
						onFailure
					);
				}
				else
				{
					Debug.Log("ServerNetworkManager.RequestDeleteMail: No json result exists");
					
					onFailure?.Invoke();
					return;
				}
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);
				
				onFailure?.Invoke();
				return;
			}
		);
	}

	public void RequestAttachedItemSystemMailItemAll(Action<SystemPostRewardListModel> onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestAttachedItemSystemMailItemAll",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestAttachedItemSystemMailItemAll",
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.RequestAttachedItemSystemMailItemAll");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Request Attached Item System Mail ItemAll Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					SystemPostRewardListModel receivedInfo = null;

					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}

					if(jsonResult.ContainsKey("receiveItemList"))
					{
						object valueToUse;
						jsonResult.TryGetValue("receiveItemList", out valueToUse);
						var json = Convert.ToString(valueToUse);
						receivedInfo = JsonUtility.FromJson<SystemPostRewardListModel>(json) as SystemPostRewardListModel;
					}

					if(jsonResult.ContainsKey("updatedSystemPost"))
					{
						object valueToUse;
						jsonResult.TryGetValue("updatedSystemPost", out valueToUse);
						var json = Convert.ToString(valueToUse);

						var serializer = PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer);
						this.systemPost = serializer.DeserializeObject<SystemPostListModel>(json);
					}

					onSuccess?.Invoke(receivedInfo);
				}
				else
				{
					Debug.Log("ServerNetworkManager.requestAttachedItemSystemMailItemAll: No json result exists");
					
					onFailure?.Invoke();
					return;
				}
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);
				
				onFailure?.Invoke();
				return;
			}
		);
	}

	public void RequestDeleteSystemMail(int mailId, Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestDeleteSystemMail",
				FunctionParameter = new {mailId = mailId},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestDeleteSystemMail",
				FunctionParameter = new {mailId = mailId},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.RequestDeleteSystemMail.Mail ID: " + mailId);

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Request Delete System Mail Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{	
							onFailure?.Invoke();
							return;
						}
					}

					if(jsonResult.ContainsKey("updatedSystemPost"))
					{
						object valueToUse;
						jsonResult.TryGetValue("updatedSystemPost", out valueToUse);
						var json = Convert.ToString(valueToUse);

						var serializer = PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer);
						this.systemPost = serializer.DeserializeObject<SystemPostListModel>(json);
					}

					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.RequestDeleteSystemMail: No json result exists");
					
					onFailure?.Invoke();
					return;
				}
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);
				
				onFailure?.Invoke();
				return;
			}
		);
	}
#endregion

#region For Free Gem
	public void GetFreeGemDailyLeftLimit(Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestFreeGemDailyLeftLimit",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestFreeGemDailyLeftLimit",
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.GetFreeGemDailyLeftLimit");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Refresh Get Free Gem Daily Left Limit Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}

					if(jsonResult.ContainsKey("leftLimit"))
					{
						object valueToUse;
						jsonResult.TryGetValue("leftLimit", out valueToUse);
						freeGemDailyLeftLimit = Convert.ToInt32(valueToUse);
					}
					
					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.GetFreeGemDailyLeftLimit: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke();
				return;
			}
		);
	}

	public void GetFreeGemAds(Action<int> onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "getFreeGemAds",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "getFreeGemAds",
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.GetFreeGemAds");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Refresh Get Free Gem Ads Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							
							onFailure?.Invoke();
							return;
						}
					}

					int gemCount = 0;

					if(jsonResult.ContainsKey("gemCount"))
					{
						object valueToUse;
						jsonResult.TryGetValue("gemCount", out valueToUse);
						gemCount = Convert.ToInt32(valueToUse);
					}

					if(jsonResult.ContainsKey("nextFreeGemAdsTimeStamp"))
					{
						object valueToUse;
						jsonResult.TryGetValue("nextFreeGemAdsTimeStamp", out valueToUse);
						nextFreeGemAdsTimeStamp = Convert.ToInt32(valueToUse);
					}

					// Update daily limit
					if(jsonResult.ContainsKey("leftLimit"))
					{
						object valueToUse;
						jsonResult.TryGetValue("leftLimit", out valueToUse);
						freeGemDailyLeftLimit = Convert.ToInt32(valueToUse);
					}
					
					// Update gem
					inventory.Gem = inventory.Gem + gemCount;

					onSuccess?.Invoke(gemCount);
				}
				else
				{
					Debug.Log("ServerNetworkManager.GetFreeGemAds: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				
					onFailure?.Invoke();
				return;
			}
		);
	}
#endregion

#region Growth Support Package Reward

	public void RequestGrowthSupportFreeReward(string targetRewardId, Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestGrowthSupportFree",
				FunctionParameter = new {growthSupportRewardId = targetRewardId},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestGrowthSupportFree",
				FunctionParameter = new {growthSupportRewardId = targetRewardId},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.RequestGrowthSupportFreeReward");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Request Growth Support Free Reward Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							
							onFailure?.Invoke();
							return;
						}
					}		

					if(jsonResult.ContainsKey("rewardGemAmount"))
					{
						object valueToUse;
						jsonResult.TryGetValue("rewardGemAmount", out valueToUse);
						this.inventory.Gem = this.inventory.Gem + Convert.ToInt32(valueToUse);
					}

					if(jsonResult.ContainsKey("updatedReceivedRewardList"))
					{
						object valueToUse;
						jsonResult.TryGetValue("updatedReceivedRewardList", out valueToUse);
						var json = Convert.ToString(valueToUse);

						var serializer = PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer);
						this.receivedGrowthSupportFreeRewardList = serializer.DeserializeObject<List<string>>(json);
					}

					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.RequestGrowthSupportFreeReward: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				
				onFailure?.Invoke();
				return;
			}
		);
	}

	public void RequestGrowthSupportFirstReward(string targetRewardId, Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestGrowthSupportFirst",
				FunctionParameter = new {growthSupportRewardId = targetRewardId},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestGrowthSupportFirst",
				FunctionParameter = new {growthSupportRewardId = targetRewardId},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.RequestGrowthSupportFirstReward");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Request Growth Support First Reward Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							
							onFailure?.Invoke();
							return;
						}
					}		

					if(jsonResult.ContainsKey("rewardGemAmount"))
					{
						object valueToUse;
						jsonResult.TryGetValue("rewardGemAmount", out valueToUse);
						this.inventory.Gem = this.inventory.Gem + Convert.ToInt32(valueToUse);
					}

					if(jsonResult.ContainsKey("updatedReceivedRewardList"))
					{
						object valueToUse;
						jsonResult.TryGetValue("updatedReceivedRewardList", out valueToUse);
						var json = Convert.ToString(valueToUse);

						var serializer = PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer);
						this.receivedGrowthSupportFirstRewardList = serializer.DeserializeObject<List<string>>(json);
					}

					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.RequestGrowthSupportFirstReward: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				
				onFailure?.Invoke();
				return;
			}
		);
	}

	public void RequestGrowthSupportSecondReward(string targetRewardId, Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestGrowthSupportSecond",
				FunctionParameter = new {growthSupportRewardId = targetRewardId},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestGrowthSupportSecond",
				FunctionParameter = new {growthSupportRewardId = targetRewardId},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.RequestGrowthSupportSecondReward");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Request Growth Support Second Reward Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							
							onFailure?.Invoke();
							return;
						}
					}

					if(jsonResult.ContainsKey("rewardGemAmount"))
					{
						object valueToUse;
						jsonResult.TryGetValue("rewardGemAmount", out valueToUse);
						this.inventory.Gem = this.inventory.Gem + Convert.ToInt32(valueToUse);
					}

					if(jsonResult.ContainsKey("updatedReceivedRewardList"))
					{
						object valueToUse;
						jsonResult.TryGetValue("updatedReceivedRewardList", out valueToUse);
						var json = Convert.ToString(valueToUse);

						var serializer = PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer);
						this.receivedGrowthSupportSecondRewardList = serializer.DeserializeObject<List<string>>(json);
					}

					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.RequestGrowthSupportSecondReward: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke();
				return;
			}
		);
	}
#endregion

#region VIP Reward
	public void RequestVIPReward(Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestVIPReward",
				FunctionParameter = new {},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestVIPReward",
				FunctionParameter = new {},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.RequestVIPReward");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Request VIP Reward Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{	
							onFailure?.Invoke();
							return;
						}
					}

					if(jsonResult.ContainsKey("usedVIPPoint"))
					{
						object valueToUse;
						jsonResult.TryGetValue("usedVIPPoint", out valueToUse);
						this.inventory.VipPoint = this.inventory.VipPoint - Convert.ToInt32(valueToUse);
					}

					if(jsonResult.ContainsKey("rewardGemAmount"))
					{
						object valueToUse;
						jsonResult.TryGetValue("rewardGemAmount", out valueToUse);
						this.inventory.Gem = this.inventory.Gem + Convert.ToInt32(valueToUse);
					}

					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.RequestVIPReward: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);
				onFailure?.Invoke();
				return;
			}
		);
	}
#endregion

#region For Arena

	public void RefreshArena(Action<bool, int, int> onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "arenaRefresh2",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "arenaRefresh2",
				GeneratePlayStreamEvent = true
			};
		}
		
		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.RefreshArena");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Refresh Arena Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}

					if(jsonResult.ContainsKey("dailyLimitLeft"))
					{
						object valueToUse;
						jsonResult.TryGetValue("dailyLimitLeft", out valueToUse);
						Arena.DailyLimitLeft = Convert.ToInt32(valueToUse);
					}

					if(jsonResult.ContainsKey("currentSeason"))
					{
						object valueToUse;
						jsonResult.TryGetValue("currentSeason", out valueToUse);
						Arena.Season = Convert.ToInt32(valueToUse);
					}

					if(jsonResult.ContainsKey("position"))
					{
						object valueToUse;
						jsonResult.TryGetValue("position", out valueToUse);
						Arena.Position = Convert.ToInt32(valueToUse) + 1;
					}

					if(jsonResult.ContainsKey("score"))
					{
						object valueToUse;
						jsonResult.TryGetValue("score", out valueToUse);
						Arena.Score = Convert.ToInt32(valueToUse);
					}

					if(jsonResult.ContainsKey("lastSeasonPosition"))
					{
						object valueToUse;
						jsonResult.TryGetValue("lastSeasonPosition", out valueToUse);
						Arena.LastSeasonPosition = Convert.ToInt32(valueToUse);
					}

					if(jsonResult.ContainsKey("lastSeasonScore"))
					{
						object valueToUse;
						jsonResult.TryGetValue("lastSeasonScore", out valueToUse);
						Arena.LastSeasonScore = Convert.ToInt32(valueToUse);
					}

					var recievedGems = 0;
					if(jsonResult.ContainsKey("lastSeasonRewardGem"))
					{
						object valueToUse;
						jsonResult.TryGetValue("lastSeasonRewardGem", out valueToUse);
						recievedGems = Convert.ToInt32(valueToUse);

						inventory.Gem += recievedGems;
					}

					if(jsonResult.ContainsKey("rankerList"))
					{
						object valueToUse;
						jsonResult.TryGetValue("rankerList", out valueToUse);
						Arena.rankList = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<List<ArenaRankModel>>(Convert.ToString(valueToUse));
					}

					if(jsonResult.ContainsKey("currentSeasonWeekly"))
					{
						object valueToUse;
						jsonResult.TryGetValue("currentSeasonWeekly", out valueToUse);
						Arena.SeasonWeekly = Convert.ToInt32(valueToUse);
					}

					if(jsonResult.ContainsKey("star"))
					{
						object valueToUse;
						jsonResult.TryGetValue("star", out valueToUse);
						Arena.Star = Convert.ToInt32(valueToUse);
					}

					if(jsonResult.ContainsKey("lastSeasonStar"))
					{
						object valueToUse;
						jsonResult.TryGetValue("lastSeasonStar", out valueToUse);
						Arena.LastSeasonStar = Convert.ToInt32(valueToUse);
					}

					var recievedGemsWeekly = 0;
					if(jsonResult.ContainsKey("lastSeasonRewardGemWeekly"))
					{
						object valueToUse;
						jsonResult.TryGetValue("lastSeasonRewardGemWeekly", out valueToUse);
						recievedGemsWeekly = Convert.ToInt32(valueToUse);

						inventory.Gem += recievedGemsWeekly;
					}

					// Check min version
					bool meetMinimumVersion = true;
					if(Application.platform == RuntimePlatform.IPhonePlayer)
					{
						string miniOSVersionString = "0.0.0";
						
						if(jsonResult.ContainsKey("miniOSVersion"))
						{
							object valueToUse;
							jsonResult.TryGetValue("miniOSVersion", out valueToUse);
							miniOSVersionString = valueToUse.ToString();
						}

						var minVersion = new System.Version(miniOSVersionString);
						Debug.Log("   >>>>>> Minimum version to play: " + minVersion);
						var currentVersion = new System.Version(Application.version);
						Debug.Log("   >>>>>> Current version to play: " + currentVersion);

						if(currentVersion < minVersion)
							meetMinimumVersion = false;
					}
					else
					{
						string minAndroidVersionString = "0.0.0";
						
						if(jsonResult.ContainsKey("minAndroidVersion"))
						{
							object valueToUse;
							jsonResult.TryGetValue("minAndroidVersion", out valueToUse);
							minAndroidVersionString = valueToUse.ToString();
						}

						var minVersion = new System.Version(minAndroidVersionString);
						Debug.Log("   >>>>>> Minimum version to play: " + minVersion);
						var currentVersion = new System.Version(Application.version);
						Debug.Log("   >>>>>> Current version to play: " + currentVersion);
#if !TEST_SEON
						if(currentVersion < minVersion)
							meetMinimumVersion = false;
#endif
					}
					
					onSuccess?.Invoke(meetMinimumVersion, recievedGems, recievedGemsWeekly);
				}
				else
				{
					Debug.Log("ServerNetworkManager.RefreshArena: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				
				onFailure?.Invoke();
				return;
			}
		);
	}

	public void GetArenaOppponent(Action onSuccess, Action onFailure)
	{
		var currentTimeStamp = DateTimeOffset.Now.ToUnixTimeSeconds();
		var currentTimeStampDay = currentTimeStamp / (60 * 60 * 24); 

		var sizeToUse = 40;

		Arena.OpponentId = "";
		Arena.OpponentName = "";
		var statName = (ServerNetworkManager.Instance.user.ServerId == "") ? "ranking" : "ranking_" + ServerNetworkManager.Instance.user.ServerId;

		if(ServerNetworkManager.Instance.Setting.UseDPSForArena)
			statName = (ServerNetworkManager.Instance.user.ServerId == "") ? "dps" : "dps_" + ServerNetworkManager.Instance.user.ServerId;

		var request = new GetLeaderboardAroundPlayerRequest()
		{
			MaxResultsCount = sizeToUse,
			StatisticName = statName
		};

		PlayFabClientAPI.GetLeaderboardAroundPlayer(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.GetArenaOppponent");
				var myData = result.Leaderboard.FirstOrDefault(n => n.PlayFabId == user.PlayFabId);

				if(result.Leaderboard.Count >= ServerNetworkManager.Instance.Setting.ArenaSkipSize + 2)
				{
					var skipSizeToUse = Mathf.Min(ServerNetworkManager.Instance.Setting.ArenaSkipSize, Mathf.Max(myData.Position - 10));
					foreach(var tmp in result.Leaderboard.Skip(skipSizeToUse).ToList().Shuffle().OrderBy(n => recentArenaMatchList.Where(k => k == n.PlayFabId).Count()))
					{
						if(tmp.PlayFabId == user.PlayFabId)
							continue;
						
						if(tmp.StatValue == 0)
							continue;

						Arena.OpponentId = tmp.PlayFabId;
						Arena.OpponentName = tmp.DisplayName;
						recentArenaMatchList.Add(tmp.PlayFabId);

						onSuccess?.Invoke();

						break;
					}
				}
				else
				{
					foreach(var tmp in result.Leaderboard.Shuffle())
					{
						if(tmp.PlayFabId == user.PlayFabId)
							continue;
						
						if(tmp.StatValue == 0)
							continue;

						Arena.OpponentId = tmp.PlayFabId;
						Arena.OpponentName = tmp.DisplayName;

						onSuccess?.Invoke();

						break;
					}
				}
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke();
				return;
			}
		);
	}

	public void ReportArenaStart(string opponentId, Action<string, string, int> onSuccess, Action<string> onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "arenaStart",
				FunctionParameter = new {opponentId = opponentId},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "arenaStart",
				FunctionParameter = new {opponentId = opponentId},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request,
			(result) => {
				Debug.Log("ServerNetworkManager.ReportArenaStart");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Report Arena Start Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);
						if(Convert.ToBoolean(valueToUse))
						{
							if(jsonResult.ContainsKey("reasonOfError"))
							{
								jsonResult.TryGetValue("reasonOfError", out valueToUse);
								
								onFailure?.Invoke(Convert.ToString(valueToUse));
								return;
							}
							else
							{
								
								onFailure?.Invoke("unknown");
								return;
							}
						}
					}

					string opponentUserData = "";
					if(jsonResult.ContainsKey("opponentUserData"))
					{
						object valueToUse;
						jsonResult.TryGetValue("opponentUserData", out valueToUse);
						opponentUserData = Convert.ToString(valueToUse);
					}

					string opponentInventoryData = "";
					if(jsonResult.ContainsKey("opponentInventoryData"))
					{
						object valueToUse;
						jsonResult.TryGetValue("opponentInventoryData", out valueToUse);
						opponentInventoryData = Convert.ToString(valueToUse);
					}

					int opponentBonusStat = 0;
					if(jsonResult.ContainsKey("opponentBonusStat"))
					{
						object valueToUse;
						jsonResult.TryGetValue("opponentBonusStat", out valueToUse);
						opponentBonusStat = int.Parse(Convert.ToString(valueToUse));
					}

					onSuccess?.Invoke(opponentUserData, opponentInventoryData, opponentBonusStat);
				}
				else
				{
					Debug.Log("ServerNetworkManager.ReportArenaStart: No json result exists");
					
					onFailure?.Invoke("unknown");
					return;
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke("unknown");
				return;
			}
		);
	}

	public void ReportArenaEnd(bool didWin, int season, int seasonWeekly, bool isSuspiciousIfWin, Action<int> onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "arenaEnd2",
				FunctionParameter = new {didWin = didWin, season = season, seasonWeekly = seasonWeekly, isSuspiciousIfWin = isSuspiciousIfWin},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "arenaEnd2",
				FunctionParameter = new {didWin = didWin, season = season, seasonWeekly = seasonWeekly, isSuspiciousIfWin = isSuspiciousIfWin},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request,
			(result) => {
				Debug.Log("ServerNetworkManager.ReportArenaEnd");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Report Arena End Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}

					int score = 0;
					if(jsonResult.ContainsKey("myNewScore"))
					{
						object valueToUse;
						jsonResult.TryGetValue("myNewScore", out valueToUse);
						score = Convert.ToInt32(valueToUse);
					}

					onSuccess?.Invoke(score);
				}
				else
				{
					Debug.Log("ServerNetworkManager.ReportArenaEnd: No json result exists");
					
					onFailure?.Invoke();
					return;
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke();
				return;
			}
		);
	}

#endregion

#region For Raid

	public void RefreshRaid(BigInteger goldRewardWeight, BigInteger heartRewardWeight, int gemRewardWeight, int raidTicketRewardWeight, Action<bool, int, int, int, int> onSuccess, Action onFailure)
	{		
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "raidRefresh",
				FunctionParameter = new {goldRewardWeight = goldRewardWeight.ToString(), heartRewardWeight = heartRewardWeight.ToString(), gemRewardWeight = gemRewardWeight, raidTicketRewardWeight = raidTicketRewardWeight},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "raidRefresh",
				FunctionParameter = new {goldRewardWeight = goldRewardWeight.ToString(), heartRewardWeight = heartRewardWeight.ToString(), gemRewardWeight = gemRewardWeight, raidTicketRewardWeight = raidTicketRewardWeight},
				GeneratePlayStreamEvent = true
			};
		}
		
		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.RefreshRaid");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Refresh Raid Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}

					if(jsonResult.ContainsKey("dailyLimitLeft"))
					{
						object valueToUse;
						jsonResult.TryGetValue("dailyLimitLeft", out valueToUse);
						Raid.DailyLimitLeft = Convert.ToInt32(valueToUse);
					}
					
					var passedHours = 0;
					if(jsonResult.ContainsKey("hourPassed"))
					{
						object valueToUse;
						jsonResult.TryGetValue("hourPassed", out valueToUse);
						passedHours = Convert.ToInt32(valueToUse);
					}

					var passedDays = 0;
					if(jsonResult.ContainsKey("dayPassed"))
					{
						object valueToUse;
						jsonResult.TryGetValue("dayPassed", out valueToUse);
						passedDays = Convert.ToInt32(valueToUse);
					}

					var currentTimestampHour = 0;
					if(jsonResult.ContainsKey("currentTimestampHour"))
					{
						object valueToUse;
						jsonResult.TryGetValue("currentTimestampHour", out valueToUse);
						currentTimestampHour = Convert.ToInt32(valueToUse);
					}

					var currentTimestampDay = 0;
					if(jsonResult.ContainsKey("currentTimestampDay"))
					{
						object valueToUse;
						jsonResult.TryGetValue("currentTimestampDay", out valueToUse);
						currentTimestampDay = Convert.ToInt32(valueToUse);
					}

					// Check min version
					bool meetMinimumVersion = true;
					if(Application.platform == RuntimePlatform.IPhonePlayer)
					{
						string miniOSVersionString = "0.0.0";
						if(jsonResult.ContainsKey("miniOSVersion"))
						{
							object valueToUse;
							jsonResult.TryGetValue("miniOSVersion", out valueToUse);
							miniOSVersionString = valueToUse.ToString();
						}

						var minVersion = new System.Version(miniOSVersionString);
						Debug.Log("   >>>>>> Minimum version to play: " + minVersion);
						var currentVersion = new System.Version(Application.version);
						Debug.Log("   >>>>>> Current version to play: " + currentVersion);

						if(currentVersion < minVersion)
							meetMinimumVersion = false;
					}
					else
					{
						string minAndroidVersionString = "0.0.0";
						if(jsonResult.ContainsKey("minAndroidVersion"))
						{
							object valueToUse;
							jsonResult.TryGetValue("minAndroidVersion", out valueToUse);
							minAndroidVersionString = valueToUse.ToString();
						}

						var minVersion = new System.Version(minAndroidVersionString);
						Debug.Log("   >>>>>> Minimum version to play: " + minVersion);
						var currentVersion = new System.Version(Application.version);
						Debug.Log("   >>>>>> Current version to play: " + currentVersion);
#if !TEST_SEON
						if(currentVersion < minVersion)
							meetMinimumVersion = false;
#endif
					}

					onSuccess?.Invoke(meetMinimumVersion, passedDays, passedHours, currentTimestampDay, currentTimestampHour);
				}
				else
				{
					Debug.Log("ServerNetworkManager.RefreshRaid: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke();
				return;
			}
		);
	}

	public void OpenRaid(Action<int> onSuccess, Action<string> onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "raidOpen",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "raidOpen",
				GeneratePlayStreamEvent = true
			};
		}
		
		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.OpenRaid");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Open Raid Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(jsonResult.ContainsKey("reasonOfError"))
							{
								jsonResult.TryGetValue("reasonOfError", out valueToUse);
								
								onFailure?.Invoke(Convert.ToString(valueToUse));
								return;
							}
							else
							{
								onFailure?.Invoke("unknown");
								return;
							}
						}
					}

					if(jsonResult.ContainsKey("raidIndex"))
					{
						object valueToUse;
						jsonResult.TryGetValue("raidIndex", out valueToUse);
						var raidIndex = Convert.ToInt32(valueToUse);

						onSuccess?.Invoke(raidIndex);
					}
					else
					{
						onFailure?.Invoke("unknown");
					}

				}
				else
				{
					Debug.Log("ServerNetworkManager.OpenRaid: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke("unknown");
				return;
			}
		);
	}

	public void StartRaid(Action onSuccess, Action<string> onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "raidStart",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "raidStart",
				GeneratePlayStreamEvent = true
			};
		}
		
		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.StartRaid");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Start Raid Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(jsonResult.ContainsKey("reasonOfError"))
							{
								jsonResult.TryGetValue("reasonOfError", out valueToUse);
								
								onFailure?.Invoke(Convert.ToString(valueToUse));
								return;
							}
							else
							{
								onFailure?.Invoke("unknown");
								return;
							}
						}
					}

					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.StartRaid: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);
				
				onFailure?.Invoke("unknown");
				return;
			}
		);
	}

	public void EndRaid(bool willGetReward, int skillUseCount, Action onSuccess, Action<string> onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "raidEnd",
				FunctionParameter = new {willGetReward = willGetReward, skillUseCount = skillUseCount},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "raidEnd",
				FunctionParameter = new {willGetReward = willGetReward, skillUseCount = skillUseCount},
				GeneratePlayStreamEvent = true
			};
		}
		
		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.EndRaid");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> End Raid Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(jsonResult.ContainsKey("reasonOfError"))
							{
								jsonResult.TryGetValue("reasonOfError", out valueToUse);
								
								onFailure?.Invoke(Convert.ToString(valueToUse));
								return;
							}
							else
							{
								onFailure?.Invoke("unknown");
								return;
							}
						}
					}

					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.EndRaid: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke("unknown");
				return;
			}
		);
	}

	public void JoinRaid(string hostPlayerId, int hostRaidIndex, Action onSuccess, Action<string> onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "raidJoin",
				GeneratePlayStreamEvent = true,
				FunctionParameter = new {hostPlayerId = hostPlayerId, hostRaidIndex = hostRaidIndex},
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "raidJoin",
				GeneratePlayStreamEvent = true,
				FunctionParameter = new {hostPlayerId = hostPlayerId, hostRaidIndex = hostRaidIndex}
			};
		}
		
		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.JoinRaid");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Join Raid Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(jsonResult.ContainsKey("reasonOfError"))
							{
								jsonResult.TryGetValue("reasonOfError", out valueToUse);
								
								onFailure?.Invoke(Convert.ToString(valueToUse));
								return;
							}
							else
							{
								onFailure?.Invoke("unknown");
								return;
							}
						}
					}

					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.JoinRaid: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke("unknown");
				return;
			}
		);
	}

	public void LeaveRaid(bool willGetReward, int skillUseCount, Action onSuccess, Action<string> onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "raidLeave",
				FunctionParameter = new {willGetReward = willGetReward, skillUseCount = skillUseCount},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "raidLeave",
				FunctionParameter = new {willGetReward = willGetReward, skillUseCount = skillUseCount},
				GeneratePlayStreamEvent = true
			};
		}
		
		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.LeaveRaid");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Leave Raid Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(jsonResult.ContainsKey("reasonOfError"))
							{
								jsonResult.TryGetValue("reasonOfError", out valueToUse);
								
								onFailure?.Invoke(Convert.ToString(valueToUse));
								return;
							}
							else
							{
								onFailure?.Invoke("unknown");
								return;
							}
						}
					}

					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.LeaveRaid: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke("unknown");
				return;
			}
		);
	}

#endregion

#region For Guild Raid

	public void RefreshRaidGuildRaid(Action<bool, int, int, int, int> onSuccess, Action onFailure)
	{

		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildRaidRefresh",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildRaidRefresh",
				GeneratePlayStreamEvent = true
			};
		}
		
		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.GuildRefreshRaid");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Refresh Raid Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}

					if(jsonResult.ContainsKey("dailyLimitLeft"))
					{
						object valueToUse;
						jsonResult.TryGetValue("dailyLimitLeft", out valueToUse);
						GuildRaidData.DailyLimitLeft = Convert.ToInt32(valueToUse);
					}
					
					if(jsonResult.ContainsKey("currentSeason"))
					{
						object valueToUse;
						jsonResult.TryGetValue("currentSeason", out valueToUse);
						GuildRaidData.Season = Convert.ToInt32(valueToUse);
					}
					
					if(jsonResult.ContainsKey("position"))
					{
						object valueToUse;
						jsonResult.TryGetValue("position", out valueToUse);
						GuildRaidData.Position = Convert.ToInt32(valueToUse) + 1;
					}

					if(jsonResult.ContainsKey("score"))
					{
						object valueToUse;
						jsonResult.TryGetValue("score", out valueToUse);
						GuildRaidData.Score = Convert.ToInt32(valueToUse);
					}

					if(jsonResult.ContainsKey("lastSeasonPosition"))
					{
						object valueToUse;
						jsonResult.TryGetValue("lastSeasonPosition", out valueToUse);
						GuildRaidData.LastSeasonPosition = Convert.ToInt32(valueToUse);
					}

					if(jsonResult.ContainsKey("lastSeasonScore"))
					{
						object valueToUse;
						jsonResult.TryGetValue("lastSeasonScore", out valueToUse);
						GuildRaidData.LastSeasonScore = Convert.ToInt32(valueToUse);
					}

					var lastSeasonRewardGem = 0;
					if(jsonResult.ContainsKey("lastSeasonRewardGem"))
					{
						object valueToUse;
						jsonResult.TryGetValue("lastSeasonRewardGem", out valueToUse);
						lastSeasonRewardGem = Convert.ToInt32(valueToUse);

						inventory.Gem += lastSeasonRewardGem;
					}

					var lastSeasonRewardExp = 0;
					if(jsonResult.ContainsKey("lastSeasonRewardExp"))
					{
						object valueToUse;
						jsonResult.TryGetValue("lastSeasonRewardExp", out valueToUse);
						lastSeasonRewardExp = Convert.ToInt32(valueToUse);

						guildData.GuildExp += lastSeasonRewardExp;
					}

					var lastSeasonRewardGuildToken = 0;
					if(jsonResult.ContainsKey("lastSeasonRewardGuildToken"))
					{
						object valueToUse;
						jsonResult.TryGetValue("lastSeasonRewardGuildToken", out valueToUse);
						lastSeasonRewardGuildToken = Convert.ToInt32(valueToUse);

						this.inventory.GuildToken += lastSeasonRewardGuildToken;
					}

					if(jsonResult.ContainsKey("guildRankList"))
					{
						object valueToUse;
						jsonResult.TryGetValue("guildRankList", out valueToUse);
						GuildRaidData.rankList = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<List<ArenaRankModel>>(Convert.ToString(valueToUse));
					}

					// Check min version
					bool meetMinimumVersion = true;
					if(Application.platform == RuntimePlatform.IPhonePlayer)
					{
						string miniOSVersionString = "0.0.0";
						if(jsonResult.ContainsKey("miniOSVersion"))
						{
							object valueToUse;
							jsonResult.TryGetValue("miniOSVersion", out valueToUse);
							miniOSVersionString = valueToUse.ToString();
						}

						var minVersion = new System.Version(miniOSVersionString);
						Debug.Log("   >>>>>> Minimum version to play: " + minVersion);
						var currentVersion = new System.Version(Application.version);
						Debug.Log("   >>>>>> Current version to play: " + currentVersion);

						if(currentVersion < minVersion)
							meetMinimumVersion = false;
					}
					else
					{
						string minAndroidVersionString = "0.0.0";
						if(jsonResult.ContainsKey("minAndroidVersion"))
						{
							object valueToUse;
							jsonResult.TryGetValue("minAndroidVersion", out valueToUse);
							minAndroidVersionString = valueToUse.ToString();
						}

						var minVersion = new System.Version(minAndroidVersionString);
						Debug.Log("   >>>>>> Minimum version to play: " + minVersion);
						var currentVersion = new System.Version(Application.version);
						Debug.Log("   >>>>>> Current version to play: " + currentVersion);
#if !TEST_SEON
						if(currentVersion < minVersion)
							meetMinimumVersion = false;
#endif
					}

					onSuccess?.Invoke(meetMinimumVersion, lastSeasonRewardGem, 0, lastSeasonRewardExp, lastSeasonRewardGuildToken);
				}
				else
				{
					Debug.Log("ServerNetworkManager.RefreshRaid: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke();
				return;
			}
		);
	}

	public void OpenGuildRaid(Action<int> onSuccess, Action<string> onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildRaidOpen",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildRaidOpen",
				GeneratePlayStreamEvent = true
			};
		}
		
		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.OpenGuildRaid");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Open Raid Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(jsonResult.ContainsKey("reasonOfError"))
							{
								jsonResult.TryGetValue("reasonOfError", out valueToUse);
								
								onFailure?.Invoke(Convert.ToString(valueToUse));
								return;
							}
							else
							{
								onFailure?.Invoke("unknown");
								return;
							}
						}
					}

					if(jsonResult.ContainsKey("raidIndex"))
					{
						object valueToUse;
						jsonResult.TryGetValue("raidIndex", out valueToUse);
						var raidIndex = Convert.ToInt32(valueToUse);

						onSuccess?.Invoke(raidIndex);
					}
					else
					{
						onFailure?.Invoke("unknown");
					}

				}
				else
				{
					Debug.Log("ServerNetworkManager.OpenRaid: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke("unknown");
				return;
			}
		);
	}

	public void StartGuildRaid(Action onSuccess, Action<string> onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildRaidStart",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildRaidStart",
				GeneratePlayStreamEvent = true
			};
		}
		
		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.StartGuildRaid");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Start Guild Raid Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(jsonResult.ContainsKey("reasonOfError"))
							{
								jsonResult.TryGetValue("reasonOfError", out valueToUse);
								
								onFailure?.Invoke(Convert.ToString(valueToUse));
								return;
							}
							else
							{
								onFailure?.Invoke("unknown");
								return;
							}
						}
					}

					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.StartGuildRaid: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);
				
				onFailure?.Invoke("unknown");
				return;
			}
		);
	}

	public void EndGuildRaid(bool willGetReward, int score, List<string> participantList, List<int> participantScoreList, int season, int skillUseCount, Action onSuccess, Action<string> onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildRaidEnd",
				FunctionParameter = new {willGetReward = willGetReward, score = score, participantList = participantList, participantScoreList = participantScoreList, season = season, skillUseCount = skillUseCount, checkTimeStampForChanges = true},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildRaidEnd",
				FunctionParameter = new {willGetReward = willGetReward, score = score, participantList = participantList, participantScoreList = participantScoreList, season = season, skillUseCount = skillUseCount, checkTimeStampForChanges = true},
				GeneratePlayStreamEvent = true
			};
		}
		
		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.EndGuildRaid");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> End Guild Raid Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(jsonResult.ContainsKey("reasonOfError"))
							{
								jsonResult.TryGetValue("reasonOfError", out valueToUse);
								
								onFailure?.Invoke(Convert.ToString(valueToUse));
								return;
							}
							else
							{
								onFailure?.Invoke("unknown");
								return;
							}
						}
					}

					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.EndGuildRaid: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke("unknown");
				return;
			}
		);
	}

	public void JoinGuildRaid(string hostPlayerId, int hostGuildRaidIndex, Action onSuccess, Action<string> onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildRaidJoin",
				GeneratePlayStreamEvent = true,
				FunctionParameter = new {hostPlayerId = hostPlayerId, hostGuildRaidIndex = hostGuildRaidIndex},
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildRaidJoin",
				GeneratePlayStreamEvent = true,
				FunctionParameter = new {hostPlayerId = hostPlayerId, hostGuildRaidIndex = hostGuildRaidIndex}
			};
		}
		
		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.JoinGuildRaid");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Join Guild Raid Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(jsonResult.ContainsKey("reasonOfError"))
							{
								jsonResult.TryGetValue("reasonOfError", out valueToUse);
								
								onFailure?.Invoke(Convert.ToString(valueToUse));
								return;
							}
							else
							{
								onFailure?.Invoke("unknown");
								return;
							}
						}
					}

					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.JoinGuildRaid: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke("unknown");
				return;
			}
		);
	}

	public void LeaveGuildRaid(bool willGetReward, int season, int personalScore, int skillUseCount, Action onSuccess, Action<string> onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildRaidLeave",
				GeneratePlayStreamEvent = true,
				FunctionParameter = new {willGetReward = willGetReward, season = season, personalScore = personalScore, skillUseCount = skillUseCount},
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildRaidLeave",
				FunctionParameter = new {willGetReward = willGetReward, season = season, personalScore = personalScore, skillUseCount = skillUseCount},
				GeneratePlayStreamEvent = true
			};
		}
		
		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.LeaveGuildRaid");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Leave Guild Raid Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(jsonResult.ContainsKey("reasonOfError"))
							{
								jsonResult.TryGetValue("reasonOfError", out valueToUse);
								
								onFailure?.Invoke(Convert.ToString(valueToUse));
								return;
							}
							else
							{
								onFailure?.Invoke("unknown");
								return;
							}
						}
					}

					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.LeaveGuildRaid: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke("unknown");
				return;
			}
		);
	}

#endregion

#region For Tower

	public void RefreshTower(Action<bool> onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "towerRefresh",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "towerRefresh",
				GeneratePlayStreamEvent = true
			};
		}
		
		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.RefreshTower");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Refresh Tower Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}

					myTowerData = new TowerModel();

					if(jsonResult.ContainsKey("collectedGems"))
					{
						object valueToUse;
						jsonResult.TryGetValue("collectedGems", out valueToUse);
						myTowerData.CollectedGems = float.Parse(Convert.ToString(valueToUse), NumberStyles.Any, CultureInfo.InvariantCulture);
					}

					if(jsonResult.ContainsKey("collectedFeathers"))
					{
						object valueToUse;
						jsonResult.TryGetValue("collectedFeathers", out valueToUse);
						myTowerData.CollectedFeathers = float.Parse(Convert.ToString(valueToUse), NumberStyles.Any, CultureInfo.InvariantCulture);
					}

					if(jsonResult.ContainsKey("currentFloor"))
					{
						object valueToUse;
						jsonResult.TryGetValue("currentFloor", out valueToUse);
						var data = Convert.ToString(valueToUse);
						myTowerData.Floor = Convert.ToInt32(data);
					}

					if(jsonResult.ContainsKey("secondLeft"))
					{
						object valueToUse;
						jsonResult.TryGetValue("secondLeft", out valueToUse);
						var data = Convert.ToString(valueToUse);
						myTowerData.SecondLeft = Convert.ToInt32(data);
					}

					if(jsonResult.ContainsKey("rewardRate"))
					{
						object valueToUse;
						jsonResult.TryGetValue("rewardRate", out valueToUse);
						var data = Convert.ToString(valueToUse);
						myTowerData.RewardRate = Convert.ToInt32(data);
					}

					if(jsonResult.ContainsKey("rewardFeatherRate"))
					{
						object valueToUse;
						jsonResult.TryGetValue("rewardFeatherRate", out valueToUse);
						var data = Convert.ToString(valueToUse);
						myTowerData.RewardFeatherRate = Convert.ToInt32(data);
					}

					if(jsonResult.ContainsKey("defenceRecords"))
					{
						object valueToUse;
						jsonResult.TryGetValue("defenceRecords", out valueToUse);
						var data = Convert.ToString(valueToUse);
						if(data != null && data != "")
						{
							var listToUse = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<List<TowerBattleLogModel>>(data);
							myTowerData.BattleLog = listToUse;
						}
					}

					if(jsonResult.ContainsKey("cooldownLeft"))
					{
						object valueToUse;
						jsonResult.TryGetValue("cooldownLeft", out valueToUse);
						TowerCooldownLeft = Convert.ToInt32(valueToUse);
					}

					if(jsonResult.ContainsKey("cooldownDeployLeft"))
					{
						object valueToUse;
						jsonResult.TryGetValue("cooldownDeployLeft", out valueToUse);
						TowerCooldownDeployLeft = Convert.ToInt32(valueToUse);
					}

					if(jsonResult.ContainsKey("dailyLimitLeft"))
					{
						object valueToUse;
						jsonResult.TryGetValue("dailyLimitLeft", out valueToUse);
						TowerDailyLimitLeft = Convert.ToInt32(valueToUse);
					}

					// Check min version
					bool meetMinimumVersion = true;
					if(Application.platform == RuntimePlatform.IPhonePlayer)
					{
						string miniOSVersionString = "0.0.0";
						
						if(jsonResult.ContainsKey("miniOSVersion"))
						{
							object valueToUse;
							jsonResult.TryGetValue("miniOSVersion", out valueToUse);
							miniOSVersionString = valueToUse.ToString();
						}

						var minVersion = new System.Version(miniOSVersionString);
						Debug.Log("   >>>>>> Minimum version to play: " + minVersion);
						var currentVersion = new System.Version(Application.version);
						Debug.Log("   >>>>>> Current version to play: " + currentVersion);

						if(currentVersion < minVersion)
							meetMinimumVersion = false;
					}
					else
					{
						string minAndroidVersionString = "0.0.0";
						
						if(jsonResult.ContainsKey("minAndroidVersion"))
						{
							object valueToUse;
							jsonResult.TryGetValue("minAndroidVersion", out valueToUse);
							minAndroidVersionString = valueToUse.ToString();
						}

						var minVersion = new System.Version(minAndroidVersionString);
						Debug.Log("   >>>>>> Minimum version to play: " + minVersion);
						var currentVersion = new System.Version(Application.version);
						Debug.Log("   >>>>>> Current version to play: " + currentVersion);
#if !TEST_SEON
						if(currentVersion < minVersion)
							meetMinimumVersion = false;
#endif
					}

					// Check my tower deployed
					if(myTowerData.Floor != 0d)
						myTowerData.IsDeployed = true;

					// Check my tower completed
					if(myTowerData.SecondLeft <= 0 && myTowerData.IsDeployed)
						myTowerData.IsCompleted = true;
					
					onSuccess?.Invoke(meetMinimumVersion);
				}
				else
				{
					Debug.Log("ServerNetworkManager.RefreshTower: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke();
				return;
			}
		);
	}

	public void RefreshTowerFloors(Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "towerRefreshFloors",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "towerRefreshFloors",
				GeneratePlayStreamEvent = true
			};
		}
		
		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.RefreshTowerFloors");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Refresh Tower Floors Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}
					
					if(jsonResult.ContainsKey("floorPlayers"))
					{
						object valueToUse;
						jsonResult.TryGetValue("floorPlayers", out valueToUse);
						
						TowerFloorPlayers.Clear();
						int floor = 1;
						int index = 0;
				
						foreach(var tmp in PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<List<Dictionary<string,string>>>(Convert.ToString(valueToUse)))
						{
							var tmpFloor = new TowerFloorModel();
							tmpFloor.Floor = floor;
							tmpFloor.MaxAllowedPlayersNumber = setting.TowerSizes[index];
							tmpFloor.CurrentPlayers = tmp;
							TowerFloorPlayers.Add(tmpFloor);

							floor = floor + 1;
							index = index + 1;
						}
					}

					onSuccess?.Invoke();
				}
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke();
				return;
			}
		);

	}

	public void DeployTower(Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "towerDeploy",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "towerDeploy",
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request,
			(result) => {
				Debug.Log("ServerNetworkManager.TowerDeploy");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Tower Deploy Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}

					if(jsonResult.ContainsKey("collectedGems"))
					{
						object valueToUse;
						jsonResult.TryGetValue("collectedGems", out valueToUse);
						myTowerData.CollectedGems = float.Parse(Convert.ToString(valueToUse), NumberStyles.Any, CultureInfo.InvariantCulture);
					}

					if(jsonResult.ContainsKey("collectedFeathers"))
					{
						object valueToUse;
						jsonResult.TryGetValue("collectedFeathers", out valueToUse);
						myTowerData.CollectedFeathers = float.Parse(Convert.ToString(valueToUse), NumberStyles.Any, CultureInfo.InvariantCulture);
					}

					if(jsonResult.ContainsKey("currentFloor"))
					{
						object valueToUse;
						jsonResult.TryGetValue("currentFloor", out valueToUse);
						var data = Convert.ToString(valueToUse);
						myTowerData.Floor = Convert.ToInt32(data);
					}

					if(jsonResult.ContainsKey("secondLeft"))
					{
						object valueToUse;
						jsonResult.TryGetValue("secondLeft", out valueToUse);
						var data = Convert.ToString(valueToUse);
						myTowerData.SecondLeft = Convert.ToInt32(data);
					}

					if(jsonResult.ContainsKey("rewardRate"))
					{
						object valueToUse;
						jsonResult.TryGetValue("rewardRate", out valueToUse);
						var data = Convert.ToString(valueToUse);
						myTowerData.RewardRate = Convert.ToInt32(data);
					}

					if(jsonResult.ContainsKey("rewardFeatherRate"))
					{
						object valueToUse;
						jsonResult.TryGetValue("rewardFeatherRate", out valueToUse);
						var data = Convert.ToString(valueToUse);
						myTowerData.RewardFeatherRate = Convert.ToInt32(data);
					}

					if(jsonResult.ContainsKey("defenceRecords"))
					{
						object valueToUse;
						jsonResult.TryGetValue("defenceRecords", out valueToUse);
						var data = Convert.ToString(valueToUse);
						if(data != null && data != "")
						{
							var listToUse = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<List<TowerBattleLogModel>>(data);
							myTowerData.BattleLog = listToUse;
						}
					}

					// Set deployed and completed
					myTowerData.IsDeployed = true;
					myTowerData.IsCompleted = false;

					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.TowerDeploy: No json result exists");
					onFailure?.Invoke();
					return;
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke();
				return;
			}
		);
	}

	public void RetreatTower(Action<int, int> onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "towerRetreat",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "towerRetreat",
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request,
			(result) => {
				Debug.Log("ServerNetworkManager.RetreatTowerTeam");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Retreat Tower Team Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					int rewardGem = 0;
					int rewardFeather = 0;

					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}

					if(jsonResult.ContainsKey("rewardGem"))
					{
						object valueToUse;
						jsonResult.TryGetValue("rewardGem", out valueToUse);
						rewardGem = Convert.ToInt32(valueToUse);

						this.inventory.Gem += rewardGem;
					}

					if(jsonResult.ContainsKey("rewardFeather"))
					{
						object valueToUse;
						jsonResult.TryGetValue("rewardFeather", out valueToUse);
						rewardFeather = Convert.ToInt32(valueToUse);

						this.inventory.Feather += rewardFeather;
					}

					if(jsonResult.ContainsKey("collectedGems"))
					{
						object valueToUse;
						jsonResult.TryGetValue("collectedGems", out valueToUse);
						myTowerData.CollectedGems = float.Parse(Convert.ToString(valueToUse), NumberStyles.Any, CultureInfo.InvariantCulture);
					}

					if(jsonResult.ContainsKey("collectedFeathers"))
					{
						object valueToUse;
						jsonResult.TryGetValue("collectedFeathers", out valueToUse);
						myTowerData.CollectedFeathers = float.Parse(Convert.ToString(valueToUse), NumberStyles.Any, CultureInfo.InvariantCulture);
					}

					if(jsonResult.ContainsKey("currentFloor"))
					{
						object valueToUse;
						jsonResult.TryGetValue("currentFloor", out valueToUse);
						var data = Convert.ToString(valueToUse);
						myTowerData.Floor = Convert.ToInt32(data);
					}

					if(jsonResult.ContainsKey("secondLeft"))
					{
						object valueToUse;
						jsonResult.TryGetValue("secondLeft", out valueToUse);
						var data = Convert.ToString(valueToUse);
						myTowerData.SecondLeft = Convert.ToInt32(data);
					}

					if(jsonResult.ContainsKey("rewardRate"))
					{
						object valueToUse;
						jsonResult.TryGetValue("rewardRate", out valueToUse);
						var data = Convert.ToString(valueToUse);
						myTowerData.RewardRate = Convert.ToInt32(data);
					}

					if(jsonResult.ContainsKey("rewardFeatherRate"))
					{
						object valueToUse;
						jsonResult.TryGetValue("rewardFeatherRate", out valueToUse);
						var data = Convert.ToString(valueToUse);
						myTowerData.RewardFeatherRate = Convert.ToInt32(data);
					}

					if(jsonResult.ContainsKey("defenceRecords"))
					{
						object valueToUse;
						jsonResult.TryGetValue("defenceRecords", out valueToUse);
						var data = Convert.ToString(valueToUse);
						if(data != null && data != "")
						{
							var listToUse = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<List<TowerBattleLogModel>>(data);
							myTowerData.BattleLog = listToUse;
						}
					}

					if(jsonResult.ContainsKey("cooldownDeployLeft"))
					{
						object valueToUse;
						jsonResult.TryGetValue("cooldownDeployLeft", out valueToUse);
						TowerCooldownDeployLeft = Convert.ToInt32(valueToUse);
					}

					if(jsonResult.ContainsKey("dailyLimitLeft"))
					{
						object valueToUse;
						jsonResult.TryGetValue("dailyLimitLeft", out valueToUse);
						TowerDailyLimitLeft = Convert.ToInt32(valueToUse);
					}

					// Set deployed and completed
					myTowerData.IsDeployed = false;
					myTowerData.IsCompleted = false;

					onSuccess?.Invoke(rewardGem, rewardFeather);
				}
				else
				{
					Debug.Log("ServerNetworkManager.RetreatTowerTeam: No json result exists");
					onFailure?.Invoke();
					return;
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke();
				return;
			}
		);
	}

	public void AdvanceTowerWithNoBattle(Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "towerAdvanceWithNoBattle",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "towerAdvanceWithNoBattle",
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request,
			(result) => {
				Debug.Log("ServerNetworkManager.AdvanceTowerWithNoBattle");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Advance Tower With No Battle Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}

					if(jsonResult.ContainsKey("collectedGems"))
					{
						object valueToUse;
						jsonResult.TryGetValue("collectedGems", out valueToUse);
						myTowerData.CollectedGems = float.Parse(Convert.ToString(valueToUse), NumberStyles.Any, CultureInfo.InvariantCulture);
					}

					if(jsonResult.ContainsKey("collectedFeathers"))
					{
						object valueToUse;
						jsonResult.TryGetValue("collectedFeathers", out valueToUse);
						myTowerData.CollectedFeathers = float.Parse(Convert.ToString(valueToUse), NumberStyles.Any, CultureInfo.InvariantCulture);
					}

					if(jsonResult.ContainsKey("currentFloor"))
					{
						object valueToUse;
						jsonResult.TryGetValue("currentFloor", out valueToUse);
						var data = Convert.ToString(valueToUse);
						myTowerData.Floor = Convert.ToInt32(data);
					}

					if(jsonResult.ContainsKey("secondLeft"))
					{
						object valueToUse;
						jsonResult.TryGetValue("secondLeft", out valueToUse);
						var data = Convert.ToString(valueToUse);
						myTowerData.SecondLeft = Convert.ToInt32(data);
					}

					if(jsonResult.ContainsKey("rewardRate"))
					{
						object valueToUse;
						jsonResult.TryGetValue("rewardRate", out valueToUse);
						var data = Convert.ToString(valueToUse);
						myTowerData.RewardRate = Convert.ToInt32(data);
					}

					if(jsonResult.ContainsKey("rewardFeatherRate"))
					{
						object valueToUse;
						jsonResult.TryGetValue("rewardFeatherRate", out valueToUse);
						var data = Convert.ToString(valueToUse);
						myTowerData.RewardFeatherRate = Convert.ToInt32(data);
					}

					if(jsonResult.ContainsKey("defenceRecords"))
					{
						object valueToUse;
						jsonResult.TryGetValue("defenceRecords", out valueToUse);
						var data = Convert.ToString(valueToUse);
						if(data != null && data != "")
						{
							var listToUse = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<List<TowerBattleLogModel>>(data);
							myTowerData.BattleLog = listToUse;
						}
					}
					
					// Set my tower deloyed
					myTowerData.IsDeployed = true;

					// Check my tower completed
					if(myTowerData.SecondLeft <= 0 && myTowerData.IsDeployed)
						myTowerData.IsCompleted = true;
					else
						myTowerData.IsCompleted = false;

					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.AdvanceTowerWithNoBattle: No json result exists");
					onFailure?.Invoke();
					return;
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke();
				return;
			}
		);
	}

	public void ReportTowerBattleStart(string opponentId, Action<bool, string, string, int> onSuccess, Action<string> onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "towerBattleStart",
				FunctionParameter = new {opponentId = opponentId},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "towerBattleStart",
				FunctionParameter = new {opponentId = opponentId},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request,
			(result) => {
				Debug.Log("ServerNetworkManager.ReportTowerBattleStart");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Report Tower Battle Start Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);
						if(Convert.ToBoolean(valueToUse))
						{
							if(jsonResult.ContainsKey("reasonOfError"))
							{
								jsonResult.TryGetValue("reasonOfError", out valueToUse);
								
								onFailure?.Invoke(Convert.ToString(valueToUse));
								return;
							}
							else
							{
								
								onFailure?.Invoke("unknown");
								return;
							}
						}
					}

					string opponentUserData = "";
					if(jsonResult.ContainsKey("opponentUserData"))
					{
						object valueToUse;
						jsonResult.TryGetValue("opponentUserData", out valueToUse);
						opponentUserData = Convert.ToString(valueToUse);
					}

					string opponentInventoryData = "";
					if(jsonResult.ContainsKey("opponentInventoryData"))
					{
						object valueToUse;
						jsonResult.TryGetValue("opponentInventoryData", out valueToUse);
						opponentInventoryData = Convert.ToString(valueToUse);
					}

					int opponentBonusStat = 0;
					if(jsonResult.ContainsKey("opponentBonusStat"))
					{
						object valueToUse;
						jsonResult.TryGetValue("opponentBonusStat", out valueToUse);
						opponentBonusStat = int.Parse(Convert.ToString(valueToUse));
					}

					bool canSkip = false;
					if(jsonResult.ContainsKey("canSkipBecauseOpponentFinished"))
					{
						object valueToUse;
						jsonResult.TryGetValue("canSkipBecauseOpponentFinished", out valueToUse);
						canSkip = bool.Parse(Convert.ToString(valueToUse));
					}

					onSuccess?.Invoke(canSkip, opponentUserData, opponentInventoryData, opponentBonusStat);
				}
				else
				{
					Debug.Log("ServerNetworkManager.ReportTowerBattleStart: No json result exists");
					onFailure?.Invoke("unknown");
					return;
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke("unknown");
				return;
			}
		);
	}

	public void ReportTowerBattleEnd(bool isWin, bool isSuspiciousIfWin, Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "towerBattleEnd",
				FunctionParameter = new {isWin = isWin, isSuspiciousIfWin = isSuspiciousIfWin},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "towerBattleEnd",
				FunctionParameter = new {isWin = isWin, isSuspiciousIfWin = isSuspiciousIfWin},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request,
			(result) => {
				Debug.Log("ServerNetworkManager.ReportTowerBattleEnd");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Report Tower Battle End Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}

					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.ReportTowerBattleEnd: No json result exists");
					onFailure?.Invoke();
					return;
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke();
				return;
			}
		);
	}

#endregion
 
#region For Wing
	public void PurchaseWing(string itemId, string currencyType, int price, Action onSuccess, Action onFailure)
	{
		PurchaseItemRequest purchaseItemRequest = new PurchaseItemRequest()
		{
			ItemId = itemId,
			VirtualCurrency = currencyType,
			Price = price
		};

		PlayFabClientAPI.PurchaseItem(
			purchaseItemRequest, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.PurchaseWing: " + itemId);

				GetInventory(
					()=>{
						onSuccess?.Invoke();
					}, 
					onFailure
				);
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);

				if(onFailure != null)
					onFailure.Invoke();
				return;
			}
		);
	}

	public void ResetWingStat(string targetWingId, int lockStatCount, bool isLockOwnedStat1, bool isLockOwnedStat2, bool isLockEquipmentStat1, bool isLockEquipmentStat2, bool isLockEquipmentStat3, 
	Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "resetWingStat",
				FunctionParameter = new {targetWingId = targetWingId, lockStatCount = lockStatCount, isLockOwnedStat1 = isLockOwnedStat1, isLockOwnedStat2 = isLockOwnedStat2, 
				isLockEquipmentStat1 = isLockEquipmentStat1, isLockEquipmentStat2 = isLockEquipmentStat2, isLockEquipmentStat3 = isLockEquipmentStat3},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "resetWingStat",
				FunctionParameter = new {targetWingId = targetWingId, lockStatCount = lockStatCount, isLockOwnedStat1 = isLockOwnedStat1, isLockOwnedStat2 = isLockOwnedStat2, 
				isLockEquipmentStat1 = isLockEquipmentStat1, isLockEquipmentStat2 = isLockEquipmentStat2, isLockEquipmentStat3 = isLockEquipmentStat3},
				GeneratePlayStreamEvent = true
			};
		}
		
		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.ResetStat");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Reset Stat Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}

					GetInventory(
						() => {
							onSuccess?.Invoke();
						},
						onFailure
					);
				}
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke();
				return;
			}
		);
	}

	public void OpenWingBody(Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "openWingBody",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "openWingBody",
				GeneratePlayStreamEvent = true
			};
		}
		
		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.OpenWingBody");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Open Wing Body Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}

					int requiredFeather = 0;

					if(jsonResult.ContainsKey("requiredFeather"))
					{
						object valueToUse;
						jsonResult.TryGetValue("requiredFeather", out valueToUse);
						requiredFeather = Convert.ToInt32(valueToUse);
					}

					// Update feather amount
					this.inventory.Feather = this.inventory.Feather - requiredFeather;

					int updatedLevel = 1;
					if(jsonResult.ContainsKey("updatedLevel"))
					{
						object valueToUse;
						jsonResult.TryGetValue("updatedLevel", out valueToUse);
						updatedLevel = Convert.ToInt32(valueToUse);
					}

					// Set wing body info
					this.wingBodyInfo = new WingBodyInstanceModel(true, updatedLevel);

					onSuccess?.Invoke();
				}
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke();
				return;
			}
		);
	}

	public void UpgradeWingBody(Action<bool> onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "upgradeWingBody",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "upgradeWingBody",
				GeneratePlayStreamEvent = true
			};
		}
		
		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.UpgradeWingBody");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Upgrade Wing Body Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}

					int requiredFeather = 0;

					if(jsonResult.ContainsKey("requiredFeather"))
					{
						object valueToUse;
						jsonResult.TryGetValue("requiredFeather", out valueToUse);
						requiredFeather = Convert.ToInt32(valueToUse);
					}

					// Update feather amount
					this.inventory.Feather = this.inventory.Feather - requiredFeather;

					bool isUpgradeSuccess = false;

					if(jsonResult.ContainsKey("isUpgradeSuccess"))
					{
						object valueToUse;
						jsonResult.TryGetValue("isUpgradeSuccess", out valueToUse);
						isUpgradeSuccess = Convert.ToBoolean(valueToUse);
					}
					
					// Update target wing body's level
					if(isUpgradeSuccess)
					{
						int updatedLevel = 0;
						if(jsonResult.ContainsKey("updatedLevel"))
						{
							object valueToUse;
							jsonResult.TryGetValue("updatedLevel", out valueToUse);
							updatedLevel = Convert.ToInt32(valueToUse);
						}

						if(this.wingBodyInfo != null)
						{
							this.wingBodyInfo.IsOpen = true;
							this.wingBodyInfo.UpdateWingBodyLevel(updatedLevel);
						}
					}

					onSuccess?.Invoke(isUpgradeSuccess);
				}
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke();
				return;
			}
		);
	}

#endregion

#region Supply Box

	public void RefreshSupplyBox(Action<int> onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestSupplyBox",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestSupplyBox",
				GeneratePlayStreamEvent = true
			};
		}
		
		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.RefreshSupplyBox");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Refresh SupplyBox Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}
					
					var passedMins = 0;
					if(jsonResult.ContainsKey("minPassed"))
					{
						object valueToUse;
						jsonResult.TryGetValue("minPassed", out valueToUse);
						passedMins = Convert.ToInt32(valueToUse);
					}

					onSuccess?.Invoke(passedMins);
				}
				else
				{
					Debug.Log("ServerNetworkManager.RefreshSupplyBox: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);
				
				onFailure?.Invoke();
				return;
			}
		);
	}

#endregion

#region Attendance Event
	public void RefreshAttendanceEventInfo(Action onSuccess, Action<string> onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestAttendanceEventInfo",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestAttendanceEventInfo",
				GeneratePlayStreamEvent = true
			};
		}
		
		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.RefreshAttendanceEventInfo");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Refresh Attendance Event Info Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("lastAttendanceTimeStamp"))
					{
						object valueToUse;
						jsonResult.TryGetValue("lastAttendanceTimeStamp", out valueToUse);
						this.lastAttendanceTimestampday = Convert.ToInt32(valueToUse);
					}

					if(jsonResult.ContainsKey("attendanceEventInfo"))
					{
						object valueToUse;
						jsonResult.TryGetValue("attendanceEventInfo", out valueToUse);
						var json = Convert.ToString(valueToUse);
						this.attendanceEventInfo = JsonUtility.FromJson<AttendanceEventInfoModel>(json) as AttendanceEventInfoModel;
					}

					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(jsonResult.ContainsKey("reasonOfError"))
							{
								jsonResult.TryGetValue("reasonOfError", out valueToUse);
								
								onFailure?.Invoke(Convert.ToString(valueToUse));
								return;
							}
							else
							{
								onFailure?.Invoke("unknown");
								return;
							}
						}
					}

					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.RefreshAttendanceEventInfo: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);
				
				onFailure?.Invoke("unknown");
				return;
			}
		);
	}

	public void RequestAttendanceEventReward(int selectedRewardIndex, Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestAttendanceEventReward",
				FunctionParameter = new {selectedRewardIndex = selectedRewardIndex},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "requestAttendanceEventReward",
				FunctionParameter = new {selectedRewardIndex = selectedRewardIndex},
				GeneratePlayStreamEvent = true
			};
		}
		
		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.RequestAttendanceEventReward");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Request Attendance Event Reward Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}

					var tmpEventReward = new AttendanceEventRewardModel();

					if(jsonResult.ContainsKey("itemId"))
					{
						object valueToUse;
						jsonResult.TryGetValue("itemId", out valueToUse);
						tmpEventReward.ItemId = Convert.ToString(valueToUse);
					}

					if(jsonResult.ContainsKey("itemCode"))
					{
						object valueToUse;
						jsonResult.TryGetValue("itemCode", out valueToUse);
						tmpEventReward.ItemCode = Convert.ToString(valueToUse);
					}

					if(jsonResult.ContainsKey("count"))
					{
						object valueToUse;
						jsonResult.TryGetValue("count", out valueToUse);
						tmpEventReward.ItemCount = Convert.ToInt32(valueToUse);
					}

					if(jsonResult.ContainsKey("lastAttendanceTimeStamp"))
					{
						object valueToUse;
						jsonResult.TryGetValue("lastAttendanceTimeStamp", out valueToUse);
						this.lastAttendanceTimestampday = Convert.ToInt32(valueToUse);
					}
					
					// Give target reward
					if(tmpEventReward.ItemCode == "GE")
					{
						this.inventory.Gem = this.inventory.Gem + tmpEventReward.ItemCount;
						onSuccess?.Invoke();
					}
					else if(tmpEventReward.ItemCode == "relic")
					{
						RelicModel tmpRelicModel = ServerNetworkManager.Instance.Inventory.RelicList.FirstOrDefault(n => n.Id == tmpEventReward.ItemId);
						
						if(tmpRelicModel == null)
						{
							tmpRelicModel = new RelicModel();
							tmpRelicModel.Id = tmpEventReward.ItemId;
							tmpRelicModel.Count = tmpEventReward.ItemCount;
							tmpRelicModel.Level = 1;
							tmpRelicModel.IsNew = true;
							tmpRelicModel.RelicType = (RelicType)(int.Parse(tmpEventReward.ItemId.Substring(tmpEventReward.ItemId.Length - 2)) - 1);
							ServerNetworkManager.Instance.Inventory.RelicList.Add(tmpRelicModel);
						}
						else
							tmpRelicModel.Count = tmpRelicModel.Count + tmpEventReward.ItemCount;

						SyncPlayerData(
							false,
							"RequestAttendanceEventReward",
							onSuccess,
							(error) => {
								if(error == "ban")
								{
									Debug.Log("ServerNetworkManager.OnLogin: Account is banned");
									Application.Quit();
								}
								else
								{
									onFailure?.Invoke();
									return;
								}
							}
						);	
					}
					else
						onFailure?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.RequestAttendanceEventReward: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);
				
				onFailure?.Invoke();
				return;
			}
		);
	}
#endregion

#region Utilty

	public static bool ApplyUserDataFromServer(string playerId, UserModel target, ServerUserModel serverUserData)
	{

		bool localCacheUsed = false;

		// Before using the data from server, we need to check whether there is local cache exists.
		// If local cache exists and its timestamp is bigger than server's data, we need to use local cache.
		try
		{
			if(!ServerNetworkManager.Instance.IsCacheAltered && ObscuredPrefs.HasKey("dt_us_1" + playerId))
			{
				var localCacheJson = ObscuredPrefs.GetString("dt_us_1" + playerId);

				var localCache = new ServerUserModel();
				localCache.FromJson(localCacheJson);

				if(localCache.TimeStamp > serverUserData.TimeStamp)
				{
					serverUserData = localCache;
					localCacheUsed = true;
				}
			}
		}
		catch(Exception e)
		{
			Debug.Log(e);
		}

		target.TimeStamp = serverUserData.TimeStamp;

		// Restore buff passed information for local user
		if(ServerNetworkManager.Instance.User.PlayFabId == playerId)
		{
			BuffManager.Instance.AutoEnablePassed = serverUserData.AutoEnableFreeBuffPassed;
			BuffManager.Instance.AttackBuffPassed = serverUserData.AttackFreeBuffPassed;
			BuffManager.Instance.GoldBuffPassed = serverUserData.GoldFreeBuffPassed;
			BuffManager.Instance.SpeedBuffPassed = serverUserData.SpeedFreeBuffPassed;
		}
		
		target.Upgrade.BaseAttackLevel = serverUserData.BaseAttackLevel;
		target.Upgrade.CriticalAttackLevel = serverUserData.CriticalAttackLevel;
		target.Upgrade.CriticalChanceLevel = serverUserData.CriticalChanceLevel;
		target.Upgrade.SuperAttackLevel = serverUserData.SuperAttackLevel;
		target.Upgrade.SuperChanceLevel = serverUserData.SuperChanceLevel;
		target.Upgrade.UltimateAttackLevel = serverUserData.UltimateAttackLevel;
		target.Upgrade.UltimateChanceLevel = serverUserData.UltimateChanceLevel;
		target.Upgrade.HyperAttackLevel = serverUserData.HyperAttackLevel;
		target.Upgrade.HyperChanceLevel = serverUserData.HyperChanceLevel;

		if(target.Upgrade.HyperAttackLevel == 0)
			target.Upgrade.HyperAttackLevel = 1;
		if(target.Upgrade.HyperChanceLevel == 0)
			target.Upgrade.HyperChanceLevel = 1;

		target.AutoEnabledTier = serverUserData.AutoAttackTier;
        target.AttackBuffTier = serverUserData.AttackBuffTier;
        target.SpeedBuffTier = serverUserData.SpeedBuffTier;
        target.GoldBuffTier = serverUserData.GoldBuffTier;

		target.KillCountForBestStage = serverUserData.KillCountForBestStage;

		target.StageProgress = serverUserData.StageProgress;
		target.SelectedStage = serverUserData.SelectedStage;

		target.WeaponGachaLevel = serverUserData.WeaponGachaLevel;
        target.WeaponGachaExp = serverUserData.WeaponGachaExp;
        target.ClassGachaLevel = serverUserData.ClassGachaLevel;
        target.ClassGachaExp = serverUserData.ClassGachaExp;
		target.MercenaryGachaLevel = Mathf.Max(1, serverUserData.MercenaryGachaLevel);
		target.MercenaryGachaExp = serverUserData.MercenaryGachaExp;
		target.PetGachaLevel = serverUserData.PetGachaLevel;
        target.PetGachaExp = serverUserData.PetGachaExp;
        target.VIPLevel = serverUserData.VIPLevel;
        target.VIPExp = serverUserData.VIPExp;

		target.TotalWingAttackBonus = serverUserData.TotalWingAttackBonus;
		target.TotalWingGoldBonus = serverUserData.TotalWingGoldBonus;

		target.SkillInfo.Clear();
		foreach(var tmp in serverUserData.SkillInfo)
        {
            var tmpSkillModel = new SkillModel();

            tmpSkillModel.Id = tmp.Id;
			tmpSkillModel.Level = tmp.Level;
			tmpSkillModel.GoldLevelUpCount = tmp.GoldLevelUpCount;
			tmpSkillModel.BonusLevel = tmp.BonusLevel;

            target.SkillInfo.Add(tmpSkillModel);
        }

		target.AchievementInfo.Clear();
        foreach(var tmp in serverUserData.AchievementInfo)
        {
            var tmpAchievementModel = new AchievementModel();

            tmpAchievementModel.Id = tmp.Id;
            tmpAchievementModel.Count = tmp.Count;
            tmpAchievementModel.Goal = tmp.Goal;
			tmpAchievementModel.Level = tmp.Level;
            tmpAchievementModel.IsMaxLevel = tmp.IsMaxLevel;

            target.AchievementInfo.Add(tmpAchievementModel);
        }

		target.UnsyncedGemChangeCount = serverUserData.UnsyncedGemChangeCount;

		return localCacheUsed;
	}

	public static bool ApplyInventoryDataFromServer(string playerId, InventoryModel target, ServerInventoryModel serverInventoryData)
	{
		
		bool localCacheUsed = false;

		// Before using the data from server, we need to check whether there is local cache exists.
		// If local cache exists and its timestamp is bigger than server's data, we need to use local cache.
		try
		{
			if(!ServerNetworkManager.Instance.IsCacheAltered && ObscuredPrefs.HasKey("dt_iv_1" + playerId) && ObscuredPrefs.HasKey("dt_iv_2" + playerId))
			{
				var localCacheJson = ObscuredPrefs.GetString("dt_iv_1" + playerId) + ObscuredPrefs.GetString("dt_iv_2" + playerId);

				var localCache = new ServerInventoryModel();
				localCache.FromJson(localCacheJson);

				if(localCache.TimeStamp > serverInventoryData.TimeStamp)
				{
					serverInventoryData = localCache;
					localCacheUsed = true;
				}
			}
		}
		catch(Exception e)
		{
			Debug.Log(e);
		}

		// Restore unsynced gem for local user
		if(ServerNetworkManager.Instance.User.PlayFabId == playerId)
			ServerNetworkManager.Instance.UnsyncedGem = serverInventoryData.UnsyncedGem;
		
		if(serverInventoryData.Gold != null)
			target.Gold = BigInteger.Parse(serverInventoryData.Gold, NumberStyles.Any, CultureInfo.InvariantCulture);
		if(serverInventoryData.Heart != null)
			target.Heart = BigInteger.Parse(serverInventoryData.Heart, NumberStyles.Any, CultureInfo.InvariantCulture);
		if(serverInventoryData.TicketRaid != null)
			target.TicketRaid = int.Parse(serverInventoryData.TicketRaid, NumberStyles.Any, CultureInfo.InvariantCulture);

		foreach(var tmp in serverInventoryData.OwnedClassList)
		{
			var toAdd = new ClassModel();
			toAdd.Id = tmp.id;
			toAdd.Level = tmp.level;
			toAdd.Count = tmp.count;
			toAdd.IsNew = tmp.isNew;
			target.ClassList.Add(toAdd);
		}
		target.SelectedClass = target.ClassList.FirstOrDefault(n => n.Id == serverInventoryData.ClassId);
		
		foreach(var tmp in serverInventoryData.OwnedWeaponList)
		{
			var toAdd = new WeaponModel();
			toAdd.Id = tmp.id;
			toAdd.Level = tmp.level;
			toAdd.Count = tmp.count;
			toAdd.IsNew = tmp.isNew;
			target.WeaponList.Add(toAdd);
		}
		target.SelectedWeapon = target.WeaponList.FirstOrDefault(n => n.Id == serverInventoryData.WeaponId);

		foreach(var tmp in serverInventoryData.OwnedMercenaryList)
		{
			var toAdd = new MercenaryModel();
			toAdd.Id = tmp.id;
			toAdd.Level = tmp.level;
			toAdd.Count = tmp.count;
			toAdd.IsNew = tmp.isNew;
			target.MercenaryList.Add(toAdd);
		}
		target.SelectedMercenary = target.MercenaryList.FirstOrDefault(n => n.Id == serverInventoryData.MercenaryId);
		
		foreach(var tmp in serverInventoryData.OwnedRelicList)
		{
			var toAdd = new RelicModel();
			toAdd.Id = tmp.id;
			toAdd.Level = tmp.level;
			toAdd.Count = tmp.count;
			toAdd.IsNew = tmp.isNew;
			toAdd.RelicType = (RelicType)(int.Parse(tmp.id.Substring(6)) - 1);
			target.RelicList.Add(toAdd);
		}

		foreach(var tmp in serverInventoryData.OwnedLegendaryRelicList)
		{
			var toAdd = new LegendaryRelicModel();
			toAdd.Id = tmp.id;
			toAdd.Level = tmp.level;
			toAdd.Count = tmp.count;
			toAdd.IsNew = tmp.isNew;
			toAdd.LegendaryRelicType = (LegendaryRelicType)(int.Parse(tmp.id.Substring(16)) - 1);
			target.LegendaryRelicList.Add(toAdd);
		}

		foreach(var tmp in serverInventoryData.OwnedPetList)
		{
			var toAdd = new PetModel();
			toAdd.Id = tmp.id;
			toAdd.Level = tmp.level;
			toAdd.Count = tmp.count;
			toAdd.IsNew = tmp.isNew;
			toAdd.GoldLevelUpCount = tmp.goldLevelUpCount;
            toAdd.HeartLevelUpCount = tmp.heartLevelUpCount;
            toAdd.PetLevelUpCount = tmp.mergeLevelUpCount;
			target.PetList.Add(toAdd);
		}

		if(serverInventoryData.PetIdList == null)
			serverInventoryData.PetIdList = new List<string>();
		foreach(var tmp in serverInventoryData.PetIdList)
		{
			var toAdd = target.PetList.FirstOrDefault(n => n.Id == tmp);
			if(toAdd != null)
				target.SelectedPetList.Add(toAdd);
		}

		target.MainPet = target.PetList.FirstOrDefault(n => n.Id == serverInventoryData.MainPetId);
		
		foreach(var tmp in serverInventoryData.OwnedTrophyList)
		{
			var toAdd = new TrophyModel();
			toAdd.Id = tmp.id;
			toAdd.Level = tmp.level;
			toAdd.Count = tmp.count;
            toAdd.HeartLevelUpCount = tmp.heartLevelUpCount;
            toAdd.TrophyLevelUpCount = tmp.mergeLevelUpCount;
			target.TrophyList.Add(toAdd);
		}

		if(serverInventoryData.TrophyIdList == null)
			serverInventoryData.TrophyIdList = new List<string>();
		foreach(var tmp in serverInventoryData.TrophyIdList)
		{
			var toAdd = target.TrophyList.FirstOrDefault(n => n.Id == tmp);
			if(toAdd != null)
				target.SelectedTrophyList.Add(toAdd);
		}

		target.WingIndex = serverInventoryData.WingIndex;
		
		return localCacheUsed;

	}

	public bool CheckNeedRelogIn()
	{
		if(Application.platform == RuntimePlatform.IPhonePlayer)
		{
			var minVersion = new System.Version(setting.MiniOSVersion);
			var currentVersion = new System.Version(Application.version);
			if(currentVersion < minVersion)
				return true;
		}
		else
		{
			var minVersion = new System.Version(setting.MinAndroidVersion);
			var currentVersion = new System.Version(Application.version);
#if !TEST_SEON
			if(currentVersion < minVersion)
				return true;
#else
			return false;
#endif
		}

		var minLoginCode = setting.MinLoginCode;
		var currentLoginCode = setting.LoginCode;
		if(currentLoginCode < minLoginCode)
			return true;

		return false;
	}

	private WingInstanceModel GetDefaultWingInstance(string wingId)
	{		
		var tmpWingOwnedStatBonusModel = new WingOwnedStatBonusModel();
		tmpWingOwnedStatBonusModel.Stat1Rank = "E";
		tmpWingOwnedStatBonusModel.Stat1SubRank = 0;
		tmpWingOwnedStatBonusModel.Stat2Rank = "E";
		tmpWingOwnedStatBonusModel.Stat2SubRank = 0;

		var tmpWingEquipmentStatBonusModel = new WingEquipmentStatBonusModel();
		tmpWingEquipmentStatBonusModel.Stat1Rank = "E";
		tmpWingEquipmentStatBonusModel.Stat2Rank = "E";
		tmpWingEquipmentStatBonusModel.Stat3Rank = "E";

		return new WingInstanceModel(wingId, tmpWingOwnedStatBonusModel, tmpWingEquipmentStatBonusModel);
	} 

#endregion

#region Security

	public void GetUserKeyForLiapp(Action<string> onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "liappUserKey",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "liappUserKey",
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.RefreshLastDevice");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Refresh Last Device Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							onFailure?.Invoke();
							return;
						}
					}

					var userKey = "";
					if(jsonResult.ContainsKey("userKey"))
					{
						object valueToUse;
						jsonResult.TryGetValue("userKey", out valueToUse);
						userKey = Convert.ToString(valueToUse);
					}
					onSuccess?.Invoke(userKey);
				}
				else
				{
					Debug.Log("ServerNetworkManager.RefreshLastDevice: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);
				onFailure?.Invoke();
			}
		);
	}

	public void AuthLiapp(string token, Action onSuccess, Action<string> onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "liappAuth",
				FunctionParameter = new {token = token},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "liappAuth",
				FunctionParameter = new {token = token},
				GeneratePlayStreamEvent = true
			};
		}
		
		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.AuthLiapp");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Call Liapp Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(jsonResult.ContainsKey("reasonOfError"))
							{
								jsonResult.TryGetValue("reasonOfError", out valueToUse);
								
								onFailure?.Invoke(Convert.ToString(valueToUse));
								return;
							}
							else
							{
								onFailure?.Invoke("unknown");
								return;
							}
						}
					}

					onSuccess?.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.LeaveRaid: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke("unknown");
				return;
			}
		);
	}

#endregion

#region Guild

#region  Guild Creat / Delete / Guild Master Transfer
	public void CreateGuild(string guildName, int guildFlagIndex, string guildNotice, bool isPrivate, int requiredStageProgress, string guildMasterName, int guildMasterStageProgress,
	Action onSuccess, Action<string> onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildCreate",
				FunctionParameter = new {guildName = guildName, guildFlagIndex = guildFlagIndex, guildNotice = guildNotice, isPrivate = isPrivate, 
				requiredStageProgress = requiredStageProgress, guildMasterName = guildMasterName, guildMasterStageProgress = guildMasterStageProgress},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildCreate",
				FunctionParameter = new {guildName = guildName, guildFlagIndex = guildFlagIndex, guildNotice = guildNotice, isPrivate = isPrivate, 
				requiredStageProgress = requiredStageProgress, guildMasterName = guildMasterName, guildMasterStageProgress = guildMasterStageProgress},
				GeneratePlayStreamEvent = true
			};
		}
		
		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.CreateGuild");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Create Guild Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(jsonResult.ContainsKey("reasonOfError"))
							{
								jsonResult.TryGetValue("reasonOfError", out valueToUse);
								if(onFailure != null)
									onFailure.Invoke(Convert.ToString(valueToUse));
								return;
							}
							else
							{
								if(onFailure != null)
									onFailure.Invoke("unknown");
								return;
							}
						}
					}

					if(jsonResult.ContainsKey("guildManagementData"))
					{
						object valueToUse;
						jsonResult.TryGetValue("guildManagementData", out valueToUse);
						var json = Convert.ToString(valueToUse);

						this.guildData.GuildManagementData = JsonUtility.FromJson<GuildManagementModel>(json) as GuildManagementModel;
						this.myGuildId = guildData.GuildManagementData.GuildId;
					}
					else
					{
						if(onFailure != null)
							onFailure.Invoke("unknown");
						return;
					}

					// Default guild level and exp info
					guildData.GuildLevel = 1;
					guildData.GuildExp = 0;
					guildData.GuildNextExp = 100;

					if(jsonResult.ContainsKey("guildMemberData"))
					{
						object valueToUse;
						jsonResult.TryGetValue("guildMemberData", out valueToUse);
						var json = Convert.ToString(valueToUse);

						this.guildData.GuildMemberData = JsonUtility.FromJson<GuildMemberListModel>(json) as GuildMemberListModel;
					}
					else
					{
						if(onFailure != null)
							onFailure.Invoke("unknown");
						return;
					}

					// Reset other guild data
					this.guildData.ApplicantData = new GuildApplicantListModel();
					this.guildData.GuildApplyData = new GuildApplyListModel();
					this.guildData.RecommendedGuildData = new GuildRecommendationListModel();
					this.guildData.DailyLimitFreeDonateLeft = 1;
					this.guildData.DailyLimitDonateLeft = 1;

					if(jsonResult.ContainsKey("requiredGem"))
					{
						object valueToUse;
						jsonResult.TryGetValue("requiredGem", out valueToUse);
						this.inventory.Gem = this.inventory.Gem - Convert.ToInt32(valueToUse);
					}

					onSuccess?.Invoke();
				}
			},
			(error) =>
			{
				Debug.Log(error.ErrorMessage);

				onFailure?.Invoke("unknown");
				return;
			}
		);
	}

	public void GuildDelete(Action onSuccess, Action<string> onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildDelete",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildDelete",
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.GuildDelete");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Guild Delete Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(jsonResult.ContainsKey("reasonOfError"))
							{
								jsonResult.TryGetValue("reasonOfError", out valueToUse);
								if(onFailure != null)
									onFailure.Invoke(Convert.ToString(valueToUse));
								return;
							}
							else
							{
								if(onFailure != null)
									onFailure.Invoke("unknown");
								return;
							}
						}
					}

					// Reset guild data
					this.myGuildId = "";
					this.guildData.GuildManagementData = new GuildManagementModel();
					this.guildData.GuildLevel = 0;
					this.guildData.GuildExp = 0;
					this.guildData.GuildNextExp = 0;
					this.guildData.GuildMemberData = new GuildMemberListModel();
					this.guildData.ApplicantData = new GuildApplicantListModel();

					if(onSuccess != null)
						onSuccess.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.GuildDelete: No json result exists");
					if(onFailure != null)
						onFailure.Invoke("unknown");
					return;
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				if(onFailure != null)
					onFailure.Invoke("unknown");
				return;
			}
		);
	}

	public void GuildMasterTransfer(string newGuildMasterId, Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildMasterTransfer",
				FunctionParameter = new {newGuildMasterId = newGuildMasterId},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildMasterTransfer",
				FunctionParameter = new {newGuildMasterId = newGuildMasterId},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.GuildMasterTransfer");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Guild Master Transfer Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(onFailure != null)
								onFailure.Invoke();
							return;
						}
					}

					if(jsonResult.ContainsKey("guildId"))
					{
						object valueToUse;
						jsonResult.TryGetValue("guildId", out valueToUse);
						this.myGuildId = Convert.ToString(valueToUse);
						this.guildData.GuildManagementData.GuildId = Convert.ToString(valueToUse);
					}
					else
					{
						if(onFailure != null)
							onFailure.Invoke();
						return;
					}

					if(onSuccess != null)
						onSuccess.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.GuildMasterTransfer: No json result exists");
					if(onFailure != null)
						onFailure.Invoke();
					return;
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				if(onFailure != null)
					onFailure.Invoke();
				return;
			}
		);
	}

#endregion

#region Related With Guild Applicant
	public void GetGuildApplicantList(Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildGetApplicantList",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildGetApplicantList",
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.GetGuildApplicantList");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Get Guild ApplicantList Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(onFailure != null)
								onFailure.Invoke();
							return;
						}
					}

					if(jsonResult.ContainsKey("applicantList"))
					{
						object valueToUse;
						jsonResult.TryGetValue("applicantList", out valueToUse);
						var json = Convert.ToString(valueToUse);

						this.guildData.ApplicantData = JsonUtility.FromJson<GuildApplicantListModel>(json) as GuildApplicantListModel;
						PlayerPrefs.SetInt("applicant_list_count" , this.guildData.ApplicantData.ApplicantList.Count);
					}
					else
					{
						if(onFailure != null)
							onFailure.Invoke();
						return;
					}
					
					if(onSuccess != null)
						onSuccess.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.GetGuildApplicantList: No json result exists");
					
					if(onFailure != null)
						onFailure.Invoke();
					return;
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				if(onFailure != null)
					onFailure.Invoke();
				return;
			}
		);
	}

	public void GuildApplicantAccept(string applicantId, Action onSuccess, Action<string> onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildApplicantAccept",
				FunctionParameter = new {applicantId = applicantId},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildApplicantAccept",
				FunctionParameter = new {applicantId = applicantId},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.GuildApplicantAccept");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Guild Applicant Accept Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(jsonResult.ContainsKey("reasonOfError"))
							{
								jsonResult.TryGetValue("reasonOfError", out valueToUse);
								if(onFailure != null)
									onFailure.Invoke(Convert.ToString(valueToUse));
								return;
							}
							else
							{
								if(onFailure != null)
									onFailure.Invoke("unknown");
								return;
							}
						}
					}

					if(jsonResult.ContainsKey("applicantList"))
					{
						object valueToUse;
						jsonResult.TryGetValue("applicantList", out valueToUse);
						var json = Convert.ToString(valueToUse);

						this.guildData.ApplicantData = JsonUtility.FromJson<GuildApplicantListModel>(json) as GuildApplicantListModel;

						// For red dot
						PlayerPrefs.SetInt("applicant_list_count" , this.guildData.ApplicantData.ApplicantList.Count);
					}
					else
					{
						if(onFailure != null)
							onFailure.Invoke("unknown");
						return;
					}

					if(jsonResult.ContainsKey("memberCacheDataList"))
					{
						object valueToUse;
						jsonResult.TryGetValue("memberCacheDataList", out valueToUse);
						var json = Convert.ToString(valueToUse);

						this.guildData.GuildMemberData = JsonUtility.FromJson<GuildMemberListModel>(json) as GuildMemberListModel;
					}
					else
					{
						if(onFailure != null)
							onFailure.Invoke("unknown");
						return;
					}
					
					if(onSuccess != null)
						onSuccess.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.GuildApplicantAccept: No json result exists");
					if(onFailure != null)
							onFailure.Invoke("unknown");
						return;
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				if(onFailure != null)
					onFailure.Invoke("unknown");
				return;
			}
		);
	}

	public void GuildApplicantRefuse(string applicantId, Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildApplicantRefuse",
				FunctionParameter = new {applicantId = applicantId},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildApplicantRefuse",
				FunctionParameter = new {applicantId = applicantId},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.GuildApplicantRefuse");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Guild Applicant Refuse Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(onFailure != null)
								onFailure.Invoke();
							return;
						}
					}

					if(jsonResult.ContainsKey("applicantList"))
					{
						object valueToUse;
						jsonResult.TryGetValue("applicantList", out valueToUse);
						var json = Convert.ToString(valueToUse);

						this.guildData.ApplicantData = JsonUtility.FromJson<GuildApplicantListModel>(json) as GuildApplicantListModel;
						PlayerPrefs.SetInt("applicant_list_count" , this.guildData.ApplicantData.ApplicantList.Count);
					}
					else
					{
						if(onFailure != null)
							onFailure.Invoke();
						return;
					}
					
					if(onSuccess != null)
						onSuccess.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.GuildApplicantRefuse: No json result exists");
					if(onFailure != null)
						onFailure.Invoke();
					return;
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				if(onFailure != null)
					onFailure.Invoke();
				return;
			}
		);
	}
#endregion

#region Related With Guild Ban
	public void GetGuildKickList(Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildGetKickList",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildGetKickList",
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.GetGuildKickList");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Get Guild Kick List Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(onFailure != null)
								onFailure.Invoke();
							return;
						}
					}

					if(jsonResult.ContainsKey("kickList"))
					{
						object valueToUse;
						jsonResult.TryGetValue("kickList", out valueToUse);
						var json = Convert.ToString(valueToUse);

						this.guildData.GuildManagementData.GuildKickMemberData = JsonUtility.FromJson<GuildKickMemberListModel>(json) as GuildKickMemberListModel;
					}
					else
					{
						if(onFailure != null)
							onFailure.Invoke();
						return;
					}
					
					if(onSuccess != null)
						onSuccess.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.GetGuildApplicantList: No json result exists");
					
					if(onFailure != null)
						onFailure.Invoke();
					return;
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				if(onFailure != null)
					onFailure.Invoke();
				return;
			}
		);
	}

	public void KickGuildMember(string memberId, string memberName, Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildKickMember",
				FunctionParameter = new {memberId = memberId, memberName = memberName},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildKickMember",
				FunctionParameter = new {memberId = memberId, memberName = memberName},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.KickGuildMember");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Kick Guild Member Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(onFailure != null)
								onFailure.Invoke();
							return;
						}
					}

					if(jsonResult.ContainsKey("memberCacheDataList"))
					{
						object valueToUse;
						jsonResult.TryGetValue("memberCacheDataList", out valueToUse);
						var json = Convert.ToString(valueToUse);

						this.guildData.GuildMemberData = JsonUtility.FromJson<GuildMemberListModel>(json) as GuildMemberListModel;
					}
					else
					{
						if(onFailure != null)
							onFailure.Invoke();
						return;
					}

					if(onSuccess != null)
						onSuccess.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.KickGuildMember: No json result exists");
					if(onFailure != null)
						onFailure.Invoke();
					return;
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				if(onFailure != null)
					onFailure.Invoke();
				return;
			}
		);
	}
	
	public void KickGuildMemberCancel(string targetId, Action onSuccess, Action onFailure){
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildKickCancel",
				FunctionParameter = new {memberId = targetId},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildKickCancel",
				FunctionParameter = new {memberId = targetId},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.KickGuildMemberCancel");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Kick Guild Member Cancel Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(onFailure != null)
								onFailure.Invoke();
							return;
						}
					}

					// Remove from guild kick list
					var targetMemberInfo = this.guildData.GuildManagementData.GuildKickMemberData.KickMemberList.Find(n => n.Id == targetId);
					this.guildData.GuildManagementData.GuildKickMemberData.KickMemberList.Remove(targetMemberInfo);

					if(onSuccess != null)
						onSuccess.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.KickGuildMember: No json result exists");
					if(onFailure != null)
						onFailure.Invoke();
					return;
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				if(onFailure != null)
					onFailure.Invoke();
				return;
			}
		);
	}
#endregion

#region Related With Guild Setting
	public void EditGuildSetting(string guildNotice, int guildFlagIndex, int requiredStageProgress, bool isPrivate, Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildSettingEdit",
				FunctionParameter = new {guildNotice = guildNotice, guildFlagIndex = guildFlagIndex, requiredStageProgress = requiredStageProgress, isPrivate = isPrivate},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildSettingEdit",
				FunctionParameter = new {guildNotice = guildNotice, guildFlagIndex = guildFlagIndex, requiredStageProgress = requiredStageProgress, isPrivate = isPrivate},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.EditGuildSetting");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Edit Guild Setting Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(onFailure != null)
								onFailure.Invoke();
							return;
						}
					}
					
					// Update guild setting
					this.guildData.GuildManagementData.GuildNotice = guildNotice;
					this.guildData.GuildManagementData.GuildFlagIndex = guildFlagIndex;
					this.guildData.GuildManagementData.RequiredStageProgress = requiredStageProgress;
					this.guildData.GuildManagementData.IsPrivate = isPrivate;

					if(onSuccess != null)
						onSuccess.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.EditGuildSetting: No json result exists");
					if(onFailure != null)
							onFailure.Invoke();
						return;
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				if(onFailure != null)
					onFailure.Invoke();
				return;
			}
		);
	}
#endregion

#region Related With Guild Join / Leave
	public void	GuildJoin(string guildId, string userName, int userStageProgress, Action onSuccess, Action<string> onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildJoin",
				FunctionParameter = new {guildId = guildId, userName = userName, userStageProgress = userStageProgress},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildJoin",
				FunctionParameter = new {guildId = guildId, userName = userName, userStageProgress = userStageProgress},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.GuildJoin");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Guild Join Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(jsonResult.ContainsKey("reasonOfError"))
							{
								jsonResult.TryGetValue("reasonOfError", out valueToUse);
								if(onFailure != null)
									onFailure.Invoke(Convert.ToString(valueToUse));
								return;
							}
							else
							{
								if(onFailure != null)
									onFailure.Invoke("unknown");
								return;
							}
						}
					}

					if(jsonResult.ContainsKey("guildManagementData"))
					{
						object valueToUse;
						jsonResult.TryGetValue("guildManagementData", out valueToUse);
						var json = Convert.ToString(valueToUse);

						this.guildData.GuildManagementData = JsonUtility.FromJson<GuildManagementModel>(json) as GuildManagementModel;
						this.myGuildId = this.guildData.GuildManagementData.GuildId;
					}
					else
					{
						if(onFailure != null)
							onFailure.Invoke("unknown");
						return;
					}

					if(jsonResult.ContainsKey("memberCacheDataList"))
					{
						object valueToUse;
						jsonResult.TryGetValue("memberCacheDataList", out valueToUse);
						var json = Convert.ToString(valueToUse);

						this.guildData.GuildMemberData = JsonUtility.FromJson<GuildMemberListModel>(json) as GuildMemberListModel;
					}
					else
					{
						if(onFailure != null)
							onFailure.Invoke("unknown");
						return;
					}

					if(jsonResult.ContainsKey("guildLevel"))
					{
						object valueToUse;
						jsonResult.TryGetValue("guildLevel", out valueToUse);
						this.guildData.GuildLevel = Convert.ToInt32(valueToUse);
					}
					else
						this.guildData.GuildLevel = 1;

					if(jsonResult.ContainsKey("guildExp"))
					{
						object valueToUse;
						jsonResult.TryGetValue("guildExp", out valueToUse);
						this.guildData.GuildExp = Convert.ToInt32(valueToUse);
					}
					else
						this.guildData.GuildExp = 0;

					if(jsonResult.ContainsKey("guildNextExp"))
					{
						object valueToUse;
						jsonResult.TryGetValue("guildNextExp", out valueToUse);
						this.guildData.GuildNextExp = Convert.ToInt32(valueToUse);
					}
					else
						this.guildData.GuildNextExp = 100;

					if(jsonResult.ContainsKey("dailyLimitFreeDonateLeft"))
					{
						object valueToUse;
						jsonResult.TryGetValue("dailyLimitFreeDonateLeft", out valueToUse);
						this.guildData.DailyLimitFreeDonateLeft = Convert.ToInt32(valueToUse);
					}

					if(jsonResult.ContainsKey("dailyLimitDonateLeft"))
					{
						object valueToUse;
						jsonResult.TryGetValue("dailyLimitDonateLeft", out valueToUse);
						this.guildData.DailyLimitDonateLeft = Convert.ToInt32(valueToUse);
					}

					if(onSuccess != null)
						onSuccess.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.GuildJoin: No json result exists");
					if(onFailure != null)
						onFailure.Invoke("unknown");
					return;
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				if(onFailure != null)
					onFailure.Invoke("unknown");
				return;
			}
		);
	}

	public void	PrivateGuildJoin(string guildId, string userName, int userStageProgress, Action onSuccess, Action<string> onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildPrivateJoin",
				FunctionParameter = new {guildId = guildId, userName = userName, userStageProgress = userStageProgress},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildPrivateJoin",
				FunctionParameter = new {guildId = guildId, userName = userName, userStageProgress = userStageProgress},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.PrivateGuildJoin");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Private Guild Join Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(jsonResult.ContainsKey("reasonOfError"))
							{
								jsonResult.TryGetValue("reasonOfError", out valueToUse);
								if(onFailure != null)
									onFailure.Invoke(Convert.ToString(valueToUse));
								return;
							}
							else
							{
								if(onFailure != null)
									onFailure.Invoke("unknown");
								return;
							}
						}
					}

					if(jsonResult.ContainsKey("applyGuildList"))
					{
						object valueToUse;
						jsonResult.TryGetValue("applyGuildList", out valueToUse);
						var json = Convert.ToString(valueToUse);

						this.guildData.GuildApplyData = JsonUtility.FromJson<GuildApplyListModel>(json) as GuildApplyListModel;
					}
					else
					{
						if(onFailure != null)
							onFailure.Invoke("unknown");
						return;
					}

					if(onSuccess != null)
						onSuccess.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.PrivateGuildJoin: No json result exists");
					if(onFailure != null)
						onFailure.Invoke("unknown");
					return;
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				if(onFailure != null)
					onFailure.Invoke("unknown");
				return;
			}
		);
	}

	public void	PrivateGuildJoinCancel(string guildId, Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildPrivateJoinCancel",
				FunctionParameter = new {guildId = guildId},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildPrivateJoinCancel",
				FunctionParameter = new {guildId = guildId},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.PrivateGuildJoinCancel");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Private Guild Join Cancel Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(onFailure != null)
								onFailure.Invoke();
							return;
						}
					}

					if(jsonResult.ContainsKey("applyGuildList"))
					{
						object valueToUse;
						jsonResult.TryGetValue("applyGuildList", out valueToUse);
						var json = Convert.ToString(valueToUse);

						this.guildData.GuildApplyData = JsonUtility.FromJson<GuildApplyListModel>(json) as GuildApplyListModel;
					}
					else
					{
						if(onFailure != null)
							onFailure.Invoke();
						return;
					}

					if(onSuccess != null)
						onSuccess.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.PrivateGuildJoinCancel: No json result exists");
					if(onFailure != null)
						onFailure.Invoke();
					return;
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				if(onFailure != null)
					onFailure.Invoke();
				return;
			}
		);
	}

	public void LeaveGuild(Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildLeave",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildLeave",
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.LeaveGuild");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Leave Guild Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					object valueToUse;
					
					if(jsonResult.ContainsKey("error"))
					{
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(onFailure != null)
								onFailure.Invoke();
							return;
						}
					}

					// Reset guild data
					this.myGuildId = "";
					this.guildData = new GuildModel();

					if(onSuccess != null)
						onSuccess.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.LeaveGuild: No json result exists");
					if(onFailure != null)
						onFailure.Invoke();
					return;
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				if(onFailure != null)
					onFailure.Invoke();
				return;
			}
		);
	}
#endregion

#region Related With Serach Guild / Get Recommendation Guild
	public void GetGuildWithId(string guildId, Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildSearchWithId",
				FunctionParameter = new {guildId = guildId},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildSearchWithId",
				FunctionParameter = new {guildId = guildId},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.GetGuildWithId");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Get Guild With Id Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(onFailure != null)
								onFailure.Invoke();
							return;
						}
					}

					if(jsonResult.ContainsKey("targetGuildData"))
					{
						object valueToUse;
						jsonResult.TryGetValue("targetGuildData", out valueToUse);
						var json = Convert.ToString(valueToUse);

						this.guildData.RecommendedGuildData = JsonUtility.FromJson<GuildRecommendationListModel>(json) as GuildRecommendationListModel;
					}
					else
					{
						if(onFailure != null)
							onFailure.Invoke();
						return;
					}

					if(jsonResult.ContainsKey("applyGuildList"))
					{
						object valueToUse;
						jsonResult.TryGetValue("applyGuildList", out valueToUse);
						var json = Convert.ToString(valueToUse);

						this.guildData.GuildApplyData = JsonUtility.FromJson<GuildApplyListModel>(json) as GuildApplyListModel;
					}

					if(onSuccess != null)
						onSuccess.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.GetGuildWithId: No json result exists");

					if(onFailure != null)
						onFailure.Invoke();
					return;
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				if(onFailure != null)
					onFailure.Invoke();
				return;
			}
		);
	}

	public void GetGuildRecommendation(Action<int> onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildGetRecommendation",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildGetRecommendation",
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.GetGuildRecommendation");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Get Guild Recommendation Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(onFailure != null)
								onFailure.Invoke();
							return;
						}
					}

					int leftGuildJoinTime = 0;

					if(jsonResult.ContainsKey("leftGuildJoinTime"))
					{
						object valueToUse;
						jsonResult.TryGetValue("leftGuildJoinTime", out valueToUse);
						leftGuildJoinTime = Convert.ToInt32(valueToUse);
					}

					if(jsonResult.ContainsKey("myGuildId"))
					{
						object valueToUse;
						jsonResult.TryGetValue("myGuildId", out valueToUse);
						this.myGuildId = Convert.ToString(valueToUse);
						this.guildData.GuildManagementData.GuildId = Convert.ToString(valueToUse);
					}

					if(jsonResult.ContainsKey("guildList"))
					{
						object valueToUse;
						jsonResult.TryGetValue("guildList", out valueToUse);
						var json = Convert.ToString(valueToUse);

						this.guildData.RecommendedGuildData = JsonUtility.FromJson<GuildRecommendationListModel>(json) as GuildRecommendationListModel;
					}

					if(jsonResult.ContainsKey("applyGuildList"))
					{
						object valueToUse;
						jsonResult.TryGetValue("applyGuildList", out valueToUse);
						var json = Convert.ToString(valueToUse);

						this.guildData.GuildApplyData = JsonUtility.FromJson<GuildApplyListModel>(json) as GuildApplyListModel;
					}

					if(onSuccess != null)
						onSuccess.Invoke(leftGuildJoinTime);
				}
				else
				{
					Debug.Log("ServerNetworkManager.GetGuildRecommendation: No json result exists");
					if(onFailure != null)
						onFailure.Invoke();
					return;
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				if(onFailure != null)
					onFailure.Invoke();
				return;
			}
		);
	}
#endregion

#region Related With Guild Refresh / Guild Donate
	public void GuildRefresh(string userName, int userStageProgress, bool skipUpdateCache, Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildRefresh",
				FunctionParameter = new {userName = userName, userStageProgress = userStageProgress, skipUpdateCache = skipUpdateCache},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildRefresh",
				FunctionParameter = new {userName = userName, userStageProgress = userStageProgress, skipUpdateCache = skipUpdateCache},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.GuildRefresh");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Get Guild Data: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(onFailure != null)
								onFailure.Invoke();
							return;
						}
					}

					// Get my guild data
					if(jsonResult.ContainsKey("guildManagementData"))
					{
						object valueToUse;
						jsonResult.TryGetValue("guildManagementData", out valueToUse);

						if(valueToUse != null)
						{
							var json = Convert.ToString(valueToUse);
							this.guildData.GuildManagementData = JsonUtility.FromJson<GuildManagementModel>(json) as GuildManagementModel;
							this.myGuildId = this.guildData.GuildManagementData.GuildId;
						}
						else
						{
							// Reset guild management data
							this.guildData.GuildManagementData = new GuildManagementModel();
							this.myGuildId = "";
						}
					}
					else
					{
						if(onFailure != null)
							onFailure.Invoke();
						return;
					}

					if(jsonResult.ContainsKey("memberList"))
					{
						object valueToUse;
						jsonResult.TryGetValue("memberList", out valueToUse);
						var json = Convert.ToString(valueToUse);

						this.guildData.GuildMemberData = JsonUtility.FromJson<GuildMemberListModel>(json) as GuildMemberListModel;
					}
					else
					{
						if(onFailure != null)
							onFailure.Invoke();
						return;
					}

					if(jsonResult.ContainsKey("applicantList"))
					{
						object valueToUse;
						jsonResult.TryGetValue("applicantList", out valueToUse);
						var json = Convert.ToString(valueToUse);

						this.guildData.ApplicantData = JsonUtility.FromJson<GuildApplicantListModel>(json) as GuildApplicantListModel;
					}
					else
					{
						if(onFailure != null)
							onFailure.Invoke();
						return;
					}

					// Get guild level and exp info
					if(jsonResult.ContainsKey("guildLevel"))
					{
						object valueToUse;
						jsonResult.TryGetValue("guildLevel", out valueToUse);
						this.guildData.GuildLevel = Convert.ToInt32(valueToUse);
					}
					else
						this.guildData.GuildLevel = 1;

					if(jsonResult.ContainsKey("guildExp"))
					{
						object valueToUse;
						jsonResult.TryGetValue("guildExp", out valueToUse);
						this.guildData.GuildExp = Convert.ToInt32(valueToUse);
					}
					else
						this.guildData.GuildExp = 0;

					if(jsonResult.ContainsKey("guildNextExp"))
					{
						object valueToUse;
						jsonResult.TryGetValue("guildNextExp", out valueToUse);
						this.guildData.GuildNextExp = Convert.ToInt32(valueToUse);
					}
					else
						this.guildData.GuildNextExp = 100;

					// Get daily donate left
					if(jsonResult.ContainsKey("dailyLimitFreeDonateLeft"))
					{
						object valueToUse;
						jsonResult.TryGetValue("dailyLimitFreeDonateLeft", out valueToUse);
						this.guildData.DailyLimitFreeDonateLeft = Convert.ToInt32(valueToUse);
					}

					if(jsonResult.ContainsKey("dailyLimitDonateLeft"))
					{
						object valueToUse;
						jsonResult.TryGetValue("dailyLimitDonateLeft", out valueToUse);
						this.guildData.DailyLimitDonateLeft = Convert.ToInt32(valueToUse);
					}
					
					// Get server time stamp and last guild leave time stamp
					if(jsonResult.ContainsKey("serverTimeStamp"))
					{
						object valueToUse;
						jsonResult.TryGetValue("serverTimeStamp", out valueToUse);
						this.guildData.CurrentServerTimeStamp = Convert.ToInt32(valueToUse);
					}
					
					if(this.myGuildId != "")
					{
						if(onSuccess != null)
							onSuccess.Invoke();
					}
					else// This is the case when kicked from the guild, but the guild data still remains on client side
					{
						if(onFailure != null)
							onFailure.Invoke();
						return;
					}
					
				}
				else
				{
					Debug.Log("ServerNetworkManager.GetGuildDataAndReward: No json result exists");
					if(onFailure != null)
						onFailure.Invoke();
					return;
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				if(onFailure != null)
					onFailure.Invoke();
				return;
			}
		);
	}

	public void GuildFreeDonate(Action onSuccess, Action<string> onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildFreeDonate",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildFreeDonate",
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.guildFreeDonate");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Guild Free Donate Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(jsonResult.ContainsKey("reasonOfError"))
							{
								jsonResult.TryGetValue("reasonOfError", out valueToUse);
								if(onFailure != null)
									onFailure.Invoke(Convert.ToString(valueToUse));
								return;
							}
							else
							{
								if(onFailure != null)
									onFailure.Invoke("unknown");
								return;
							}
						}
					}

					if(jsonResult.ContainsKey("guildLevel"))
					{
						object valueToUse;
						jsonResult.TryGetValue("guildLevel", out valueToUse);
						this.guildData.GuildLevel = Convert.ToInt32(valueToUse);
					}
					else
					{
						if(onFailure != null)
							onFailure.Invoke("unknown");
						return;
					}

					if(jsonResult.ContainsKey("guildExp"))
					{
						object valueToUse;
						jsonResult.TryGetValue("guildExp", out valueToUse);
						this.guildData.GuildExp = Convert.ToInt32(valueToUse);
					}
					else
					{
						if(onFailure != null)
							onFailure.Invoke("unknown");
						return;
					}

					if(jsonResult.ContainsKey("guildNextExp"))
					{
						object valueToUse;
						jsonResult.TryGetValue("guildNextExp", out valueToUse);
						this.guildData.GuildNextExp = Convert.ToInt32(valueToUse);
					}
					else
					{
						if(onFailure != null)
							onFailure.Invoke("unknown");
						return;
					}

					var addedExp = 0;
					if(jsonResult.ContainsKey("addedExp"))
					{
						object valueToUse;
						jsonResult.TryGetValue("addedExp", out valueToUse);
						addedExp = Convert.ToInt32(valueToUse);
					}
					else
					{
						if(onFailure != null)
							onFailure.Invoke("unknown");
						return;
					}

					if(onSuccess != null)
					{
						// Refresh client data
						this.guildData.DailyLimitFreeDonateLeft = this.guildData.DailyLimitFreeDonateLeft - 1;
						
						// Add to my total contribution
						var myGuildMemberData = this.guildData.GuildMemberData.MemberCacheDataList.FirstOrDefault(n => n.Id == this.user.PlayFabId);

						if(myGuildMemberData != null)
							myGuildMemberData.TotalContribution = myGuildMemberData.TotalContribution + addedExp;

						onSuccess.Invoke();
					}
				}
				else
				{
					Debug.Log("ServerNetworkManager.GuildFreeDonate: No json result exists");
					if(onFailure != null)
						onFailure.Invoke("unknown");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				if(onFailure != null)
					onFailure.Invoke("unknown");
			}
		);
	}

	public void GuildDonate(int donateAmount, Action onSuccess, Action<string> onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildDonate",
				FunctionParameter = new {donateAmount = donateAmount},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildDonate",
				FunctionParameter = new {donateAmount = donateAmount},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.GuildDonate");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Guild Donate Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(jsonResult.ContainsKey("reasonOfError"))
							{
								jsonResult.TryGetValue("reasonOfError", out valueToUse);
								if(onFailure != null)
									onFailure.Invoke(Convert.ToString(valueToUse));
								return;
							}
							else
							{
								if(onFailure != null)
									onFailure.Invoke("unknown");
								return;
							}
						}
					}

					if(jsonResult.ContainsKey("guildLevel"))
					{
						object valueToUse;
						jsonResult.TryGetValue("guildLevel", out valueToUse);
						this.guildData.GuildLevel = Convert.ToInt32(valueToUse);
					}
					else
					{
						if(onFailure != null)
							onFailure.Invoke("unknown");
						return;
					}

					if(jsonResult.ContainsKey("guildExp"))
					{
						object valueToUse;
						jsonResult.TryGetValue("guildExp", out valueToUse);
						this.guildData.GuildExp = Convert.ToInt32(valueToUse);
					}
					else
					{
						if(onFailure != null)
							onFailure.Invoke("unknown");
						return;
					}

					if(jsonResult.ContainsKey("guildNextExp"))
					{
						object valueToUse;
						jsonResult.TryGetValue("guildNextExp", out valueToUse);
						this.guildData.GuildNextExp = Convert.ToInt32(valueToUse);
					}
					else
					{
						if(onFailure != null)
							onFailure.Invoke("unknown");
						return;
					}

					var addedExp = 0;
					if(jsonResult.ContainsKey("addedExp"))
					{
						object valueToUse;
						jsonResult.TryGetValue("addedExp", out valueToUse);
						addedExp = Convert.ToInt32(valueToUse);
					}
					else
					{
						if(onFailure != null)
							onFailure.Invoke("unknown");
						return;
					}

					if(onSuccess != null)
					{
						// Refresh client data
						this.inventory.Gem = this.inventory.Gem - donateAmount;
						this.guildData.DailyLimitDonateLeft = this.guildData.DailyLimitDonateLeft - 1;
						
						// Add to my total contribution
						var myGuildMemberData = this.guildData.GuildMemberData.MemberCacheDataList.FirstOrDefault(n => n.Id == this.user.PlayFabId);

						if(myGuildMemberData != null)
							myGuildMemberData.TotalContribution = myGuildMemberData.TotalContribution + addedExp;

						onSuccess.Invoke();
					}
				}
				else
				{
					Debug.Log("ServerNetworkManager.GuildDonate: No json result exists");
					if(onFailure != null)
						onFailure.Invoke("unknown");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				if(onFailure != null)
					onFailure.Invoke("unknown");
			}
		);
	}
#endregion

#region Related With Guild Shop
	public void GuildUpgradeRelic(string guildRelicId, int upgradeCount, Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildUpgradeRelic",
				FunctionParameter = new {targetGuildRelicId = guildRelicId, upgradeCount = upgradeCount},
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildUpgradeRelic",
				FunctionParameter = new {targetGuildRelicId = guildRelicId, upgradeCount = upgradeCount},
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.GuildUpgradeRelic");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Guild Upgrade Relic Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(onFailure != null)
								onFailure.Invoke();
							return;
						}
					}

					if(jsonResult.ContainsKey("guildRelicList"))
					{
						object valueToUse;
						jsonResult.TryGetValue("guildRelicList", out valueToUse);
						var json = Convert.ToString(valueToUse);

						this.guildRelicData = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<GuildRelicListModel>(json) as GuildRelicListModel;
					}

					if(jsonResult.ContainsKey("addedBonusStat"))
					{
						object valueToUse;
						jsonResult.TryGetValue("addedBonusStat", out valueToUse);
						this.inventory.BonusStat = this.inventory.BonusStat + Convert.ToInt32(valueToUse);
					}

					if(jsonResult.ContainsKey("requiredGuildToken"))
					{
						object valueToUse;
						jsonResult.TryGetValue("requiredGuildToken", out valueToUse);
						this.inventory.GuildToken = this.inventory.GuildToken - Convert.ToInt32(valueToUse);
					}

					if(onSuccess != null)
						onSuccess.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.GuildUpgradeRelic: No json result exists");

					if(onFailure != null)
						onFailure.Invoke();
					return;
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				if(onFailure != null)
					onFailure.Invoke();
				return;
			}
		);
	}
#endregion

	public void GetGuildRankingLeaderboardList(Action onSuccess, Action onFailure)
	{
		ExecuteCloudScriptRequest request = null;
		if(testingCloudScriptRevision != -1 && Application.platform == RuntimePlatform.OSXEditor)
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildGetRank",
				GeneratePlayStreamEvent = true,
				SpecificRevision = testingCloudScriptRevision
			};
		}
		else
		{
			request = new ExecuteCloudScriptRequest()
			{
				FunctionName = "guildGetRank",
				GeneratePlayStreamEvent = true
			};
		}

		PlayFabClientAPI.ExecuteCloudScript(
			request, 
			(result) =>
			{
				Debug.Log("ServerNetworkManager.GetGuildRankingLeaderboardList");

				foreach(var tmp in result.Logs)
					Debug.Log("   >>>>>> " + tmp.Message);

				var jsonResult = (PlayFab.Json.JsonObject)result.FunctionResult;
				Debug.Log("   >>>>>> Request Get Guild Ranking Info Result: " + jsonResult);
				
				if(jsonResult != null)
				{
					if(jsonResult.ContainsKey("error"))
					{
						object valueToUse;
						jsonResult.TryGetValue("error", out valueToUse);

						if(Convert.ToBoolean(valueToUse))
						{
							if(onFailure != null)
								onFailure.Invoke();
							return;
						}
					}

					this.guildRankList.Clear();

					List<PlayerLeaderboardEntry> rankList = null;
					List<string> nameList = null;
					if(jsonResult.ContainsKey("guildRankList"))
					{
						object valueToUse;
						jsonResult.TryGetValue("guildRankList", out valueToUse);
						var json = Convert.ToString(valueToUse);

						var serializer = PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer);
						rankList = serializer.DeserializeObject<List<PlayerLeaderboardEntry>>(json);
					}

					if(jsonResult.ContainsKey("guildNameList"))
					{
						object valueToUse;
						jsonResult.TryGetValue("guildNameList", out valueToUse);
						var json = Convert.ToString(valueToUse);

						var serializer = PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer);
						nameList = serializer.DeserializeObject<List<string>>(json);
					}

					if(jsonResult.ContainsKey("guildManagementData"))
					{
						object valueToUse;
						jsonResult.TryGetValue("guildManagementData", out valueToUse);

						if(valueToUse == null)
						{
							this.guildData.GuildManagementData = new GuildManagementModel();
							this.myGuildId = "";
						}
						else
						{
							var json = Convert.ToString(valueToUse);
							this.guildData.GuildManagementData = JsonUtility.FromJson<GuildManagementModel>(json) as GuildManagementModel;
						}
					}

					if(jsonResult.ContainsKey("myGuildMasterName"))
					{
						object valueToUse;
						jsonResult.TryGetValue("myGuildMasterName", out valueToUse);
						this.guildData.GuildMasterName = valueToUse.ToString();
					}

					if(jsonResult.ContainsKey("myGuildRank"))
					{
						object valueToUse;
						jsonResult.TryGetValue("myGuildRank", out valueToUse);
						
						if(Convert.ToInt32(valueToUse) != -1)
							this.guildData.Ranking = Convert.ToInt32(valueToUse) + 1;
						else
							this.guildData.Ranking = -1;
					}

					if(jsonResult.ContainsKey("myGuildScore"))
					{
						object valueToUse;
						jsonResult.TryGetValue("myGuildScore", out valueToUse);
						this.guildData.RankingScore = Convert.ToInt32(valueToUse);
					}

					for(int i = 0; i < rankList.Count; i ++)
					{
						var tmp = new GuildRankModel();
						tmp.GuildName = nameList[i];
						tmp.GuildMasterName = rankList[i].DisplayName;
						tmp.Position = rankList[i].Position + 1;
						tmp.Score = rankList[i].StatValue;
						this.guildRankList.Add(tmp);
					}

					if(onSuccess != null)
						onSuccess.Invoke();
				}
				else
				{
					Debug.Log("ServerNetworkManager.GetGuildRankingLeaderboardList: No json result exists");
				}
			},
			(error) => {
				Debug.Log(error.ErrorMessage);

				if(onFailure != null)
					onFailure.Invoke();
				return;
			}
		);
	}
#endregion

}