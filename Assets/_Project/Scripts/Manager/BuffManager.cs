﻿using System.Numerics;
using UnityEngine;
using Sirenix.OdinInspector;

using CodeStage.AntiCheat.ObscuredTypes;
/**/
/**/
/**/
/**//**/
/**/
/**/
/**/
public enum BuffType{
    ATK,
    GOLD,
    SPEED
}

public enum BuffTier{
    NONE,
    BUFF_TIER_FREE,
    BUFF_TIER_1,
    BUFF_TIER_2,
    BUFF_TIER_3,
    BUFF_TIER_4,
    BUFF_TIER_5
}

public class BuffManager : MonoBehaviour
{
    public static BuffManager Instance;

    [Title("Auto Enable")]

    public int AutoEnableTimeLeftSecond
    {
        get
        {
            if(ServerNetworkManager.Instance.User.AutoEnabledTier == (int)BuffTier.BUFF_TIER_FREE)
                return (int)(autoEnableDuration - AutoEnablePassed);
            else
                return 0;
        }
    }

    public ObscuredFloat AutoEnablePassed = 0f;

    [SerializeField]
    private ObscuredFloat autoEnableDuration = 900f;

    public bool GetAutoEnabled(int buffTier, bool isLocal = true)
    {
        if(buffTier != (int)BuffTier.NONE)
            return true;
        else
            return false;
    }

    [Title("Attack Buff")]

    public int AttackBuffTimeLeftSecond
    {
        get
        {
            if(ServerNetworkManager.Instance.User.AttackBuffTier == (int)BuffTier.BUFF_TIER_FREE)
                return (int)(attackBuffDuration - AttackBuffPassed);
            else
                return 0;
        }
    }

    public ObscuredFloat AttackBuffPassed = 0f;

    [SerializeField]
    private ObscuredFloat attackBuffDuration = 900f;

    public int GetAttackBuffValue(int buffTier, bool isLocal = true)
    {
        if(buffTier == (int)BuffTier.BUFF_TIER_FREE || buffTier == (int)BuffTier.BUFF_TIER_1)
            return 10;
        else if(buffTier == (int)BuffTier.BUFF_TIER_2)
            return 25;
        else if(buffTier == (int)BuffTier.BUFF_TIER_3)
            return 75;
        else if(buffTier == (int)BuffTier.BUFF_TIER_4)
            return 250;
        else if(buffTier == (int)BuffTier.BUFF_TIER_5)
            return 1000;
        else
            return 1;
    }

    [Title("Speed Buff")]

    public int SpeedBuffTimeLeftSecond
    {
        get
        {
            if(ServerNetworkManager.Instance.User.SpeedBuffTier == (int)BuffTier.BUFF_TIER_FREE)
                return (int)(speedBuffDuration - SpeedBuffPassed);
            else
                return 0;
        }
    }

    public ObscuredFloat SpeedBuffPassed = 0f;

    [SerializeField]
    private ObscuredFloat speedBuffDuration = 900f;

    public float GetSpeedBuffValue(int buffTier, bool isLocal = true)
    {
        if(buffTier != (int)BuffTier.NONE)
            return 2f;
        else
            return 1f;
    }

    [Title("Gold Buff")]

    public int GoldBuffTimeLeftSecond
    {
        get
        {
            if(ServerNetworkManager.Instance.User.GoldBuffTier == (int)BuffTier.BUFF_TIER_FREE)
                return (int)(goldBuffDuration - GoldBuffPassed);
            else
                return 0;
        }
    }

    public ObscuredFloat GoldBuffPassed = 0f;

    [SerializeField]
    private ObscuredFloat goldBuffDuration = 900f;

    public int GetGoldBuffValue(int buffTier, bool isLocal = true)
    {
        if(buffTier == (int)BuffTier.BUFF_TIER_FREE || buffTier == (int)BuffTier.BUFF_TIER_1)
            return 10;
        else if(buffTier == (int)BuffTier.BUFF_TIER_2)
            return 25;
        else if(buffTier == (int)BuffTier.BUFF_TIER_3)
            return 75;
        else if(buffTier == (int)BuffTier.BUFF_TIER_4)
            return 250;
        else if(buffTier == (int)BuffTier.BUFF_TIER_5)
            return 1000;
        else
            return 1;
    }
    
    private void Awake()
    {
		BuffManager.Instance = this;
	}

    private void Update()
    {
        if(ServerNetworkManager.Instance.User.AttackBuffTier == (int)BuffTier.BUFF_TIER_FREE)
        {
            AttackBuffPassed += Time.deltaTime;
            if(AttackBuffPassed >= attackBuffDuration)
            {
                ServerNetworkManager.Instance.User.AttackBuffTier = (int)BuffTier.NONE;
                AttackBuffPassed = 0f;
                Refresh();
            }
        }
        
        if(ServerNetworkManager.Instance.User.SpeedBuffTier == (int)BuffTier.BUFF_TIER_FREE)
        {
            SpeedBuffPassed += Time.deltaTime;
            if(SpeedBuffPassed >= speedBuffDuration)
            {
                ServerNetworkManager.Instance.User.SpeedBuffTier = (int)BuffTier.NONE;
                SpeedBuffPassed = 0f;
                Refresh();
            }
        }

        if(ServerNetworkManager.Instance.User.GoldBuffTier == (int)BuffTier.BUFF_TIER_FREE)
        {
            GoldBuffPassed += Time.deltaTime;
            if(GoldBuffPassed >= goldBuffDuration)
            {
                ServerNetworkManager.Instance.User.GoldBuffTier = (int)BuffTier.NONE;
                GoldBuffPassed = 0f;
                Refresh();
            }
        }
    }

    public void StartTemporrayBuff(BuffType buffType)
    {
        switch(buffType)
        {
            case BuffType.ATK:
                if(ServerNetworkManager.Instance.User.AttackBuffTier == (int)BuffTier.NONE)
                    ServerNetworkManager.Instance.User.AttackBuffTier = (int)BuffTier.BUFF_TIER_FREE;
            break;
            case BuffType.GOLD:
                if(ServerNetworkManager.Instance.User.GoldBuffTier == (int)BuffTier.NONE)
                    ServerNetworkManager.Instance.User.GoldBuffTier = (int)BuffTier.BUFF_TIER_FREE;
            break;
            case BuffType.SPEED:
                if(ServerNetworkManager.Instance.User.SpeedBuffTier == (int)BuffTier.NONE)
                    ServerNetworkManager.Instance.User.SpeedBuffTier = (int)BuffTier.BUFF_TIER_FREE;
            break;
        }

        Refresh();
    }

    public void Refresh()
    {
        // Need to recalculate numbers
        NumberMainController.Instance.Refresh();

        var viewToRefresh = GameObject.FindObjectOfType<UIViewMain>();
        if(viewToRefresh != null)
            viewToRefresh.Refresh();
    }

}
