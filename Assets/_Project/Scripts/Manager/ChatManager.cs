﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Chat;

public class ChatManager : MonoBehaviour, IChatClientListener {

	private static ChatManager instance = null;
	public static ChatManager Instance
	{
		get{
			if(instance == null)
				instance = GameObject.FindObjectOfType<ChatManager>();
			return instance;
		}
	}

    public ChatClient chatClient;

	[SerializeField]
	private TextAsset filterText = null;
	private string[] filterLlist = null;

    [SerializeField]
    private List<string> channelsSubscribed = new List<string>();

	private Dictionary<string, string> playersInPrivateRoom = new Dictionary<string, string>();
	public Dictionary<string, string> PlayersInPrivateRoom{
		get{
			return playersInPrivateRoom;
		}
	}

	public List<string> PlayerInPrivateRoomAfterThisPlayer = new List<string>();

	private Dictionary<string, string> classesInPrivateRoom = new Dictionary<string, string>();
	public Dictionary<string, string> ClassesInPrivateRoom{
		get{
			return classesInPrivateRoom;
		}
	}

	private Dictionary<string, string> weaponsInPrivateRoom = new Dictionary<string, string>();
	public Dictionary<string, string> WeaponsInPrivateRoom{
		get{
			return weaponsInPrivateRoom;
		}
	}

	private Dictionary<string, int> wingsInPrivateRoom = new Dictionary<string, int>();
	public Dictionary<string, int> WingIndicesInPrivateRoom{
		get{
			return wingsInPrivateRoom;
		}
	}

	public string ChatLogMain = "";
	public string ChatLogSub = "";
	public string ChatLogGuild = "";
	public string ChatLogIngame = "";

	public string ChatLogList = "";

	public string ChatLogInfo = "";
	public string ChatLogScore = "";
	public string ChatLogPosition = "";

    private bool isConnected = false;
    public bool IsConnected{
        get{
            return isConnected;
        }
    }

	private Dictionary<string, float> ChnnelsNeedToBeUpdated = new Dictionary<string, float>();
	private float lastUpdateTime = 0f;

	public Dictionary<string, string> RecentPlayers = new Dictionary<string, string>();

    private void Awake()
    {
        this.chatClient = new ChatClient(this);
        this.chatClient.UseBackgroundWorkerForSending = true;
    }

    private void Update()
    {
		if (this.chatClient != null)
		{
			this.chatClient.Service();
		}

		foreach(var tmp in ChnnelsNeedToBeUpdated.Keys)
		{
			if(ChnnelsNeedToBeUpdated[tmp] > lastUpdateTime)
			{
				UpdateChatLog(tmp);
			}
		}
		lastUpdateTime = Time.time;
    }
    
    public void OnApplicationQuit()
	{
		Disconnect();
	}
    
    public void Connect()
	{
        Debug.Log("ChatManager.Connect");

		var authValues = new AuthenticationValues();
		authValues.AuthType = CustomAuthenticationType.Custom;
		authValues.AddAuthParameter("username", ServerNetworkManager.Instance.User.PlayFabId);
		authValues.AddAuthParameter("token", ServerNetworkManager.Instance.PhotonAuthKey);

		this.chatClient.ChatRegion = ServerNetworkManager.Instance.Setting.ChatServerRegion;
#if UNITY_IPHONE && !UNITY_EDITOR
		this.chatClient.Connect("42735267-163d-4b32-8ff3-757be8321abd", "1.0", authValues);
#else
		this.chatClient.Connect("715a6ea2-7ac7-4c5a-aed7-189217929db4", "1.0", authValues);
#endif
	}

	public void Disconnect()
	{
		if (this.chatClient != null)
		{
			this.chatClient.Disconnect();
		}
	}

    public void Subscribe(string channel)
    {
        if(channel.Contains("main"))
        {
			// Only one main channel at a time
			var toRemove = channelsSubscribed.Where(n => n.Contains("main")).ToArray();
			var toUnsubscribe = channelsSubscribed.Where(n => n != channel && n.Contains("main")).ToArray();
            this.chatClient.Unsubscribe(toUnsubscribe);

			foreach (var tmp in toRemove)
	            channelsSubscribed.Remove(tmp);

			// Remove previous log
			this.ChatLogMain = "";
        }

		if(channel.Contains("sub"))
        {
			// Only one sub channel at a time
			var toRemove = channelsSubscribed.Where(n => n.Contains("sub")).ToArray();
			var toUnsubscribe = channelsSubscribed.Where(n => n != channel && n.Contains("sub")).ToArray();
            this.chatClient.Unsubscribe(toUnsubscribe);
            
			foreach (var tmp in toRemove)
	            channelsSubscribed.Remove(tmp);

			// Remove previous log
			this.ChatLogSub = "";
        }

		if(channel.Contains("guild"))
        {
			// Only one guild channel at a time
			var toRemove = channelsSubscribed.Where(n => n.Contains("guild")).ToArray();
			var toUnsubscribe = channelsSubscribed.Where(n => n != channel && n.Contains("guild")).ToArray();
            this.chatClient.Unsubscribe(toUnsubscribe);
            
			foreach (var tmp in toRemove)
	            channelsSubscribed.Remove(tmp);

			// Remove previous log
			this.ChatLogGuild = "";
        }

		if(channel.Contains("ingame"))
        {
			// Only one sub channel at a time
			var toRemove = channelsSubscribed.Where(n => n.Contains("ingame")).ToArray();
			var toUnsubscribe = channelsSubscribed.Where(n => n != channel && n.Contains("ingame")).ToArray();
            this.chatClient.Unsubscribe(toUnsubscribe);
            
			foreach (var tmp in toRemove)
	            channelsSubscribed.Remove(tmp);

			// Remove previous log
			this.ChatLogIngame = "";
        }

		if(channel.Contains("info"))
        {
			// Only one info channel at a time
			var toRemove = channelsSubscribed.Where(n => n.Contains("info")).ToArray();
			var toUnsubscribe = channelsSubscribed.Where(n => n != channel && n.Contains("info")).ToArray();
            this.chatClient.Unsubscribe(toUnsubscribe);
            
			foreach (var tmp in toRemove)
	            channelsSubscribed.Remove(tmp);

			// Remove previous log
			this.ChatLogInfo = "";
        }

		if(channel.Contains("score"))
        {
			// Only one score channel at a time
			var toRemove = channelsSubscribed.Where(n => n.Contains("score")).ToArray();
			var toUnsubscribe = channelsSubscribed.Where(n => n != channel && n.Contains("score")).ToArray();
            this.chatClient.Unsubscribe(toUnsubscribe);
            
			foreach (var tmp in toRemove)
	            channelsSubscribed.Remove(tmp);

			// Remove previous log
			this.ChatLogScore = "";
        }

		if(channel.Contains("position"))
        {
			// Only one position channel at a time
			var toRemove = channelsSubscribed.Where(n => n.Contains("position")).ToArray();
			var toUnsubscribe = channelsSubscribed.Where(n => n != channel && n.Contains("position")).ToArray();
            this.chatClient.Unsubscribe(toUnsubscribe);
            
			foreach (var tmp in toRemove)
	            channelsSubscribed.Remove(tmp);

			// Remove previous log
			this.ChatLogPosition = "";
        }
		
		if(channel.Contains("list"))
        {
			// Only one list channel at a time
			var toRemove = channelsSubscribed.Where(n => n.Contains("list")).ToArray();
			var toUnsubscribe = channelsSubscribed.Where(n => n != channel && n.Contains("list")).ToArray();
            this.chatClient.Unsubscribe(toUnsubscribe);
            
			foreach (var tmp in toRemove)
	            channelsSubscribed.Remove(tmp);

			// Remove previous log
			this.ChatLogList = "";
        }
        
        Debug.Log("ChatManager.Subscribe: " + channel);
        
		if(channel.Contains("info"))
		{
			var options = new ChannelCreationOptions();
			options.PublishSubscribers = true;

			this.chatClient.Subscribe(channel, 0, -1, options);
		}
		else
		{
			this.chatClient.Subscribe(channel);
		}

		channelsSubscribed.Add(channel);
        
    }

	public void Unsubscribe(string channel)
	{
		this.chatClient.Unsubscribe(channelsSubscribed.Where(n => n == channel).ToArray());
		channelsSubscribed.Remove(channel);
	}

    public void UpdateChatLog(string channelName)
	{
		ChatChannel channel = null;
		bool found = this.chatClient.TryGetChannel(channelName, out channel);
		if (!found)
		{
			Debug.Log("ShowChannel failed to find channel: " + channelName);
			return;
		}

		if(channelName.Contains("main"))
			this.ChatLogMain = "";
		else if(channelName.Contains("sub"))
			this.ChatLogSub = "";
		else if(channelName.Contains("guild"))
			this.ChatLogGuild = "";
		else if(channelName.Contains("ingame"))
			this.ChatLogIngame = "";
		else if(channelName.Contains("list"))
			this.ChatLogList = "";
		else if(channelName.Contains("info"))
			this.ChatLogInfo = "";
		else if(channelName.Contains("score"))
			this.ChatLogScore = "";
		else if(channelName.Contains("position"))
			this.ChatLogPosition = "";

		var existBefore = false;
		foreach(var tmp in channel.Messages)
		{
			var message = tmp.ToString();
			bool isMessageBlock = false;

			// Need to filter meta information
			if(!channelName.Contains("system") && message.Contains("@"))
			{
				var tmpSplited = message.Split(new char[]{'@', ':', '：'});
				if(tmpSplited.Length >= 3)
				{
					if(tmpSplited[0].Substring(0, 6) == "summon")
					{
						var tmpSplited2 = tmpSplited[0].Split('_');
						if(tmpSplited2.Length == 4)
						{
							message = BuildSummonMessage(tmpSplited2[1], tmpSplited2[2] + "_" + tmpSplited2[3]);
						}
						else
						{
							message = tmpSplited[1] + ":";
							for(int i = 2; i < tmpSplited.Length; i++)
							{
								message += tmpSplited[i];
							}
						}
					}
					else if(tmpSplited[0].Substring(0, 4) == "raid")
					{
						var tmpSplited2 = tmpSplited[0].Split('_');
						if(tmpSplited2.Length == 4)
						{
							message = BuildRaidMessage(tmpSplited2[1], tmpSplited2[2], tmpSplited2[3]);
						}
						else
						{
							message = tmpSplited[1] + ":";
							for(int i = 2; i < tmpSplited.Length; i++)
							{
								message += tmpSplited[i];
							}
						}
					}
					else if(tmpSplited[0].Substring(0, 6) == "normal")
					{
						var tmpSplited2 = tmpSplited[0].Split('_');
						if(tmpSplited2.Length > 1)
							RecentPlayers[tmpSplited2[1]] = tmpSplited[1];

						message = tmpSplited[1] + ":";
						for(int i = 2; i < tmpSplited.Length; i++)
						{
							message += tmpSplited[i];
						}

						// Only for guild chat, we add time information
						if(channelName.Contains("guild"))
						{
							var currentTimeStampMinute = DateTimeOffset.Now.ToUnixTimeSeconds() / 60;
							var hourDifference = Mathf.RoundToInt(Mathf.Abs(currentTimeStampMinute - int.Parse(tmpSplited2[3]) / 60) / 60);

							if(hourDifference < 1)
							{
								message = "(" + LanguageManager.Instance.GetTextData(LanguageDataType.UI, "tower_time_recent") + ") " + message;
							}
							else if(hourDifference < 24)
							{
								message = "(" + string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "tower_time_hours"), hourDifference) + ") " + message;
							}
							else
							{
								var dayDifference = hourDifference / 24;
								message = "(" + string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "tower_time_days"), dayDifference) + ") " + message;
							}	
						}

						// Check chat block
						if(ServerNetworkManager.Instance.ChatBlockInfo.ChatBlockList.Exists(n => n.Id == tmpSplited2[1]))
							isMessageBlock = true;
					}
					else
					{
						message = tmpSplited[1] + ":";
						for(int i = 2; i < tmpSplited.Length; i++)
						{
							message += tmpSplited[i];
						}
					}
				}
			}

			if(existBefore)
			{
				if(channelName.Contains("main"))
				{
					if(!isMessageBlock)
						this.ChatLogMain += "\n";
				}
				else if(channelName.Contains("sub"))
				{
					if(!isMessageBlock)
						this.ChatLogSub += "\n";
				}
				else if(channelName.Contains("guild"))
				{
					if(!isMessageBlock)
						this.ChatLogGuild += "\n";
				}
				else if(channelName.Contains("ingame"))
				{
					if(!isMessageBlock)
						this.ChatLogIngame += "\n";
				}
				else if(channelName.Contains("list"))
					this.ChatLogList += "\n";
				else if(channelName.Contains("info"))
					this.ChatLogInfo += "\n";
				else if(channelName.Contains("score"))
					this.ChatLogScore += "\n";
				else if(channelName.Contains("position"))
					this.ChatLogPosition += "\n";
			}

			if(channelName.Contains("main"))
			{
				if(!isMessageBlock)
					this.ChatLogMain += message;
			}
			else if(channelName.Contains("sub"))
			{
				if(!isMessageBlock)
					this.ChatLogSub += message;
			}
			else if(channelName.Contains("guild"))
			{
				if(!isMessageBlock)
					this.ChatLogGuild += message;
			}
			else if(channelName.Contains("ingame"))
			{
				if(!isMessageBlock)
					this.ChatLogIngame += message;	
			}
			else if(channelName.Contains("list"))
				this.ChatLogList += message;
			else if(channelName.Contains("info"))
				this.ChatLogInfo += message;
			else if(channelName.Contains("score"))
				this.ChatLogScore += message;
			else if(channelName.Contains("position"))
				this.ChatLogPosition += message;

			existBefore = true;
		}
	}

    public void SendChatTextMessageToMain(string message)
    {
		var channelName = channelsSubscribed.FirstOrDefault(n => n.Contains("main"));
		if(channelName != null && message.Length > 0 && !ServerNetworkManager.Instance.IsChatBanned)
	        this.chatClient.PublishMessage(channelName, AddMetaInformation(message));
    }

	public void SendChatTextMessageToSub(string message)
	{
		var channelName = channelsSubscribed.FirstOrDefault(n => n.Contains("sub"));
		if(channelName != null && message.Length > 0 && !ServerNetworkManager.Instance.IsChatBanned)
	        this.chatClient.PublishMessage(channelName, AddMetaInformation(message));
	}

	public void SendChatTextMessageToGuild(string message)
	{
		var channelName = channelsSubscribed.FirstOrDefault(n => n.Contains("guild"));
		if(channelName != null && message.Length > 0 && !ServerNetworkManager.Instance.IsChatBanned)
	        this.chatClient.PublishMessage(channelName, AddMetaInformation(message));
	}

	public void SendChatTextMessageToIngame(string message)
	{
		var channelName = channelsSubscribed.FirstOrDefault(n => n.Contains("ingame"));
		if(channelName != null && message.Length > 0 && !ServerNetworkManager.Instance.IsChatBanned)
	        this.chatClient.PublishMessage(channelName, AddMetaInformation(message));
	}

	public void SendChatSystemMessage(string channelName, string message)
	{
		if(channelName != null && channelName.Contains("system"))
			this.chatClient.PublishMessage(channelName, message);
	}

	private void SendChatWelcomeMessage(string channelName)
	{
		if(channelName != null)
			this.chatClient.PublishMessage(
				channelName, 
				"welcome_from_" 
				+ ServerNetworkManager.Instance.User.PlayFabId 
				+ "@" + ServerNetworkManager.Instance.User.NickName 
				+ "@" + ServerNetworkManager.Instance.Inventory.SelectedClass.Id
				+ "@" + ServerNetworkManager.Instance.Inventory.SelectedWeapon.Id
				+ "@" + ServerNetworkManager.Instance.Inventory.WingIndex
			);
	}

	public void SendChatFloorChangeMessage(string channelName, int floor)
	{
		if(channelName != null)
			this.chatClient.PublishMessage(channelName, "floor_change_to@" + floor);
	}

	public void SendChatSummonMessageToMain(string id)
	{
		var channelName = channelsSubscribed.FirstOrDefault(n => n.Contains("main"));
		if(channelName != null)
	        this.chatClient.PublishMessage(channelName, AddSummonMetaInformation(id, "t:message"));
	}
 
	public void SendChatRaidRoomMessageToMain(int rank, int roomId)
	{
		var channelName = channelsSubscribed.FirstOrDefault(n => n.Contains("main"));
		if(channelName != null)
	        this.chatClient.PublishMessage(channelName, AddRaidMetaInformation(rank, roomId, "t:message"));
	}

	public void SendChatRaidRoomMessageToSub(int rank, int roomId)
	{
		var channelName = channelsSubscribed.FirstOrDefault(n => n.Contains("sub"));
		if(channelName != null)
	        this.chatClient.PublishMessage(channelName, AddRaidMetaInformation(rank, roomId, "t:message"));
	}

	public void SendChatRaidRoomMessageToGuild(int rank, int roomId)
	{
		var channelName = channelsSubscribed.FirstOrDefault(n => n.Contains("guild"));
		if(channelName != null)
	        this.chatClient.PublishMessage(channelName, AddRaidMetaInformation(rank, roomId, "t:message"));
	}

#region For Moderation

	public string GetPlayerRecentChatLog(string playerId)
	{
		return GetPlayerRecentChatLog("main", playerId) + "\n" + GetPlayerRecentChatLog("raid", playerId);
	}

	public string GetPlayerRecentChatLog(string channelType, string playerId)
	{
		var toReturn = "";

		var channelName = channelsSubscribed.FirstOrDefault(n => n.Contains(channelType));

		ChatChannel channel = null;
		bool found = this.chatClient.TryGetChannel(channelName, out channel);
		if (!found)
		{
			Debug.Log("ShowChannel failed to find channel: " + channelName);
			return toReturn;
		}

		var existBefore = false;
		foreach(var tmp in channel.Messages)
		{
			var message = tmp.ToString();

			// Need to filter meta information
			if(message.Contains("@"))
			{
				var tmpSplited = message.Split(new char[]{'@', ':', '：'});
				if(tmpSplited.Length >= 3)
				{
					if(tmpSplited[0].Substring(0, 6) == "normal")
					{
						var tmpSplited2 = tmpSplited[0].Split('_');
						if(tmpSplited2.Length > 1)
						{
							if(tmpSplited2[1] == playerId)
							{
								message = tmpSplited[1] + ":";
								for(int i = 2; i < tmpSplited.Length; i++)
								{
									message += tmpSplited[i];
								}

								if(existBefore)
								{
									toReturn += "\n";
								}
								toReturn += message;

								existBefore = true;
							}
						}
					}
				}
			}
		}

		return toReturn;
	}

#endregion

#region Interface implementation

    public void DebugReturn(ExitGames.Client.Photon.DebugLevel level, string message)
	{
		if (level == ExitGames.Client.Photon.DebugLevel.ERROR)
		{
			Debug.LogError(message);
		}
		else if (level == ExitGames.Client.Photon.DebugLevel.WARNING)
		{
			Debug.LogWarning(message);
		}
		else
		{
			Debug.Log(message);
		}
	}

    public void OnConnected()
	{
        isConnected = true;

        Debug.Log("ChatManager.OnConnected");

        // Subscribe main channel
		if(ServerNetworkManager.Instance.User.ServerId == "03" || ServerNetworkManager.Instance.User.ServerId == "04")
	    {
			Subscribe("main_" + ServerNetworkManager.Instance.User.ServerId);
		}
		else
		{
			if(ServerNetworkManager.Instance.Setting.LocalChatList.Contains(OptionManager.Instance.Language.ToString())
				&& !OptionManager.Instance.ForceGlobalChat)
				Subscribe("main_" + ServerNetworkManager.Instance.User.ServerId + "_" + OptionManager.Instance.Language);
			else
				Subscribe("main_" + ServerNetworkManager.Instance.User.ServerId);
		}

		// Subscribe sub channels
		ChatManager.Instance.Subscribe("sub_raid_2_" + ServerNetworkManager.Instance.User.ServerId);
		
        // Open chat popup if needed
        var target = OutgameController.Instance;
        if(target != null)
            OutgameController.Instance.ChatOn();
	}

	public void OnDisconnected()
	{
	    isConnected = false;

        Debug.Log("ChatManager.OnDisconnected");
	}

	public void OnSubscribed(string[] channels, bool[] results)
	{
		Debug.Log("ChatManager.OnSubscribed: " + string.Join(", ", channels));

		var infoChannel = new List<string>(channels).FirstOrDefault(n => n.Contains("info"));
		if(infoChannel != null)
		{
			// Need to remove all previous friends
			playersInPrivateRoom.Clear();
			PlayerInPrivateRoomAfterThisPlayer.Clear();
			classesInPrivateRoom.Clear();
			weaponsInPrivateRoom.Clear();
			wingsInPrivateRoom.Clear();

			// Save self as the list
			playersInPrivateRoom[ServerNetworkManager.Instance.User.PlayFabId] = ServerNetworkManager.Instance.User.NickName;

			// Say welcome to self
			SendChatWelcomeMessage(infoChannel);
		}

		// Also update the chat log string
		foreach(var tmp in channelsSubscribed)
		{
			UpdateChatLog(tmp);
		}
	}

	public void OnUnsubscribed(string[] channels)
	{
		Debug.Log("ChatManager.OnUnsubscribed: " + string.Join(", ", channels));

		if(new List<string>(channels).Exists(n => n.Contains("info")))
		{
			// Need to remove all friends
			playersInPrivateRoom.Clear();
			PlayerInPrivateRoomAfterThisPlayer.Clear();
			classesInPrivateRoom.Clear();
			weaponsInPrivateRoom.Clear();
			wingsInPrivateRoom.Clear();
		}
	}

	public void OnGetMessages(string channelName, string[] senders, object[] messages)
	{	
		var friendsToAdd = new Dictionary<string, string>();
		var classesToAdd = new Dictionary<string, string>();
		var weaponsToAdd = new Dictionary<string, string>();
		var wingsToAdd = new Dictionary<string, int>();

		for(int i = 0; i < messages.Length; i++)
		{
			var message = messages[i].ToString();
			var splitMessage = message.Split('@');
			
			if(message.Contains("welcome_from_"))
			{
				if(channelName.Contains("info"))
				{
					friendsToAdd[senders[i]] = splitMessage[1];
					classesToAdd[senders[i]] = splitMessage[2];
					weaponsToAdd[senders[i]] = splitMessage[3];

					if(splitMessage.Length > 4)
						wingsToAdd[senders[i]] = int.Parse(splitMessage[4]);
				}
			}
			else if(message.Contains("floor_change_to"))
			{
				if(channelName.Contains("info"))
				{
					RaidManager.Instance.OnChangeRaidFloor(int.Parse(splitMessage[1]));
				}
			}
		}

		if(friendsToAdd.Count != 0)
		{
			foreach(var tmp in friendsToAdd)
			{
				playersInPrivateRoom[tmp.Key] = tmp.Value;
			}
			foreach(var tmp in classesToAdd)
			{
				classesInPrivateRoom[tmp.Key] = tmp.Value;
			}
			foreach(var tmp in weaponsToAdd)
			{
				weaponsInPrivateRoom[tmp.Key] = tmp.Value;
			}
			foreach(var tmp in wingsToAdd)
			{
				wingsInPrivateRoom[tmp.Key] = tmp.Value;
			}
		}
		
		ChnnelsNeedToBeUpdated[channelName] = Time.time;
	}

	public void OnUserSubscribed(string channel, string user)
    {
		if(channel.Contains("info"))
		{
			// Need to send hello message for the newcomer.
			SendChatWelcomeMessage(channel);

			if(!PlayerInPrivateRoomAfterThisPlayer.Contains(user))
				PlayerInPrivateRoomAfterThisPlayer.Add(user);
		}
    }

	public void OnUserUnsubscribed(string channel, string user)
    {
		if(channel.Contains("info"))
		{
			if(playersInPrivateRoom.ContainsKey(user))
				playersInPrivateRoom.Remove(user);
			if(PlayerInPrivateRoomAfterThisPlayer.Contains(user))
				PlayerInPrivateRoomAfterThisPlayer.Remove(user);
			if(classesInPrivateRoom.ContainsKey(user))
				classesInPrivateRoom.Remove(user);
			if(weaponsInPrivateRoom.ContainsKey(user))
				weaponsInPrivateRoom.Remove(user);
			if(wingsInPrivateRoom.ContainsKey(user))
				wingsInPrivateRoom.Remove(user);
		}
    }

#endregion

#region Not Used

	public void OnStatusUpdate(string user, int status, bool gotMessage, object message)
	{
		// Do nothing
	}

	public void OnChatStateChange(ChatState state)
	{
		// Do nothing
	}

	public void OnPrivateMessage(string sender, object message, string channelName)
	{
		// Do nothing
	}

#endregion

#region Utils

	public string FilteredText(string text)
	{
        var textToReturn = text;

		if(filterLlist == null)
		{
			filterLlist = filterText.text.Split(new string[] {"\n", "\r\n"}, StringSplitOptions.RemoveEmptyEntries);
		}
		
		foreach (var tmp in filterLlist)
		{
			if(HasNonASCIIChars(tmp))
				textToReturn = textToReturn.Replace(tmp, "*");
			else
			{
				if(textToReturn == tmp)
					textToReturn = "*";
				else
				{
					textToReturn = textToReturn.Replace(tmp + " ", "*");
					textToReturn = textToReturn.Replace(" " + tmp, "*");
				}
			}
		}

		return textToReturn;
	}

	public string BuildSummonMessage(string nickname, string itemId)
    {
		string itemType = "class";
		string colorCode = "#227766";
		if(itemId.Contains("weapon"))
		{
			itemType = "weapon";
			colorCode = "#B21E43";
		}
		else if(itemId.Contains("pet"))
		{
			itemType = "pet";
			colorCode = "#63338C";
		}
		else if(itemId.Contains("mercenary"))
		{
			itemType = "mercenary";
			colorCode = "#8B4BCA";
		}

        string itemName = LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, $"{itemType}_name_{int.Parse(itemId.Substring(itemId.Length - 2)).ToString("D2")}");
        string itemRank = "";

        if(itemType == "pet")
        {
            var currentPetBalanceInfo = BalanceInfoManager.Instance.PetInfoList.FirstOrDefault(n => n.Id == itemId);
            
            // Prevent null error
            if(currentPetBalanceInfo != null)
                itemRank = currentPetBalanceInfo.Rank;
            else
                itemRank = LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, $"rank_name_{int.Parse(itemId.Substring(itemId.Length - 2)).ToString("D2")}");
        }
        else
            itemRank = LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, $"rank_name_{int.Parse(itemId.Substring(itemId.Length - 2)).ToString("D2")}");
        
        string message = string.Format(
            LanguageManager.Instance.GetTextData(LanguageDataType.UI, "chat_message_summon_format"), 
            nickname, 
            itemRank, 
            itemName, 
            colorCode
        );

		return message;
	}

	public string BuildRaidMessage(string nickname, string rankString, string idString)
	{	
		var rank = 0;
		int.TryParse(rankString, out rank);
		
		var rankColor = InterSceneManager.Instance.GetColorForRank(rank);

		rankString = "?";
		if(rank != 0)
		{
			rankString = ChatManager.AddOrdinal(rank);
		}

		string message = string.Format(
			LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_raid_created"), 
			rankString,
			nickname,
			idString,
			rankColor
		);
		return message;
	}

	public static bool HasNonASCIIChars(string str)
	{
		return (System.Text.Encoding.UTF8.GetByteCount(str) != str.Length);
	}

	public static string AddMetaInformation(string message)
	{
		var currentTimeStampSecond = DateTimeOffset.Now.ToUnixTimeSeconds();
		return "normal_" + ServerNetworkManager.Instance.User.PlayFabId + "_" + ServerNetworkManager.Instance.RankingPosition + "_" + ((int)currentTimeStampSecond).ToString() + "@" + message;
	}

	public static string AddSummonMetaInformation(string id, string message)
	{
		return "summon_" + ServerNetworkManager.Instance.User.NickName + "_" + id + "@" + message;
	}

	public static string AddRaidMetaInformation(int rank, int roomId, string message)
	{
		return "raid_" + ServerNetworkManager.Instance.User.NickName + "_" + rank + "_" + roomId + "@" + message;
	}

	public static string AddOrdinal(int number)
    {
        if( number <= 0 ) return number.ToString();

        switch(number % 100)
        {
            case 11:
            case 12:
            case 13:
                return number + "th";
        }
        
        switch(number % 10)
        {
            case 1:
                return number + "st";
            case 2:
                return number + "nd";
            case 3:
                return number + "rd";
            default:
                return number + "th";
        }
    }

	private static string[] RichText { get; } = new string[]
	{
		"b",
		"i",
		// TMP
		"u",
		"s",
		"sup",
		"sub",
		"allcaps",
		"smallcaps",
		"uppercase",
	};

	private static string[] RichTextDynamic { get; } = new string[]
	{
		"color",
		"a",
		"a href",
		// TMP
		"align",
		"size",
		"cspace",
		"font",
		"indent",
		"line-height",
		"line-indent",
		"link",
		"margin",
		"margin-left",
		"margin-right",
		"mark",
		"mspace",
		"noparse",
		"nobr",
		"page",
		"pos",
		"space",
		"sprite index",
		"sprite name",
		"sprite",
		"style",
		"voffset",
		"width",
	};

	public static string RemoveRichText(string input)
	{

		foreach(string tag in RichTextDynamic)
			input = RemoveRichTextDynamicTag(input, tag);

		foreach (string tag in RichText)
			input = RemoveRichTextTag(input, tag);

		return input;

	}

	private static string RemoveRichTextDynamicTag (string input, string tag)
	{
		int index = -1;
		while (true)
		{
			index = input.IndexOf($"<{tag}=");
			//Debug.Log($"{{{index}}} - <noparse>{input}");
			if (index != -1)
			{
				int endIndex = input.Substring(index, input.Length - index).IndexOf('>');
				if (endIndex > 0)
					input = input.Remove(index, endIndex + 1);
				continue;
			}
			input = RemoveRichTextTag(input, tag, false);
			return input;
		}
	}
	private static string RemoveRichTextTag (string input, string tag, bool isStart = true)
	{
		while (true)
		{
			int index = input.IndexOf(isStart ? $"<{tag}>" : $"</{tag}>");
			if (index != -1)
			{
				input = input.Remove(index, 2 + tag.Length + (!isStart).GetHashCode());
				continue;
			}
			if (isStart)
				input = RemoveRichTextTag(input, tag, false);
			return input;
		}
	}

#endregion

}