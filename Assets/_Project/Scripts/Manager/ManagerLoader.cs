﻿using UnityEngine;
using System;
using System.Collections;

public class ManagerLoader : MonoBehaviour {

	[SerializeField]
	private Transform managersPrefabe;

	private void Awake()
	{
		var manager = GameObject.Find("Managers");

		if(manager == null)
		{
			var newManager = Instantiate(managersPrefabe);
			newManager.name = "Managers";
		}
	}
}
