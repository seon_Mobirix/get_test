﻿using System;
using System.Numerics;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaidManager : MonoBehaviour {

	private static RaidManager instance = null;
	public static RaidManager Instance
	{
		get{
			if(instance == null)
				instance = GameObject.FindObjectOfType<RaidManager>();
			return instance;
		}
	}
	
	private int raidType = 0;
	public int RaidType{
		get{
			return raidType;
		}
	}

	[SerializeField]
	private string currentHostPlayerId = "";
	public string CurrentHostPlayerId{
		get{
			return currentHostPlayerId;
		}
	}
	
	[SerializeField]
	private int currentRaidIndex = 0;

	[SerializeField]
	private int currentRoomNumber = 0;
	public int CurrentRoomNumber{
		get{
			return currentRoomNumber;
		}
	}

	[SerializeField]
	private int currentFloor = 1;
	public int CurrentFloor{ 
		get { 
			return currentFloor;
		}
	}

	private int currentRaidFloorCache = 1;
	public int CurrentRaidFloorCache{ 
		get{
			return currentRaidFloorCache;
		}
	}
	private int currentGuildRaidFloorCache = 1;
	public int CurrentGuildRaidFloorCache{
		get{
			return currentGuildRaidFloorCache;
		}
	}

	public void EnterRaidPrepareRoom(int raidType)
	{
		this.raidType = raidType;
		if(raidType == 0)
			ChatManager.Instance.Subscribe("system_raid_list_" + ServerNetworkManager.Instance.User.ServerId);
		else
			ChatManager.Instance.Subscribe("system_raid_list_" + raidType + "_" + ServerNetworkManager.Instance.GuildData.GuildManagementData.GroupId);
	}

	public void LeaveRaidPrepareRoom(int raidType)
	{
		if(raidType == 0)
			ChatManager.Instance.Unsubscribe("system_raid_list_" + ServerNetworkManager.Instance.User.ServerId);
		else
			ChatManager.Instance.Unsubscribe("system_raid_list_" + raidType + "_" + ServerNetworkManager.Instance.GuildData.GuildManagementData.GroupId);
	}

	// Called by the host player
	public void OpenRaid(int floor, Action onSuccess, Action<string> onFailure)
	{
		if(raidType == 0)
		{
			ServerNetworkManager.Instance.OpenRaid(
				(index) => {
					OnOpenRaid(ServerNetworkManager.Instance.User.PlayFabId, index, floor);
					onSuccess?.Invoke();
				},
				(reason) => {
					onFailure?.Invoke(reason);
				}
			);
		}
		else
		{
			ServerNetworkManager.Instance.OpenGuildRaid(
				(index) => {
					OnOpenRaid(ServerNetworkManager.Instance.User.PlayFabId, index, floor);
					onSuccess?.Invoke();
				},
				(reason) => {
					onFailure?.Invoke(reason);
				}
			);
		}
		
	}

	private void OnOpenRaid(string hostPlayerId, int raidIndex, int floor)
	{
		currentHostPlayerId = hostPlayerId;
		currentRaidIndex = raidIndex;
		
		currentFloor = floor;
		if(raidType == 0)
			currentRaidFloorCache = currentFloor;
		else
			currentGuildRaidFloorCache = currentFloor;

		currentRoomNumber = UnityEngine.Random.Range(1000, 10000);

		if(raidType == 0)
		{
			if(OptionManager.Instance.NotifyNewRaidsToggle)
			{
				ChatManager.Instance.SendChatRaidRoomMessageToSub(ServerNetworkManager.Instance.RankingPosition, currentRoomNumber);

				// Only raid shows for top players
				if(ServerNetworkManager.Instance.RankingPosition < 25)
					ChatManager.Instance.SendChatRaidRoomMessageToMain(ServerNetworkManager.Instance.RankingPosition, currentRoomNumber);	
			}

			ChatManager.Instance.SendChatSystemMessage(
				"system_raid_list_" + ServerNetworkManager.Instance.User.ServerId, 
				currentRoomNumber + "@" + currentHostPlayerId + "@" + raidIndex + "@" + floor
			);
		}
		else
		{
			ChatManager.Instance.SendChatRaidRoomMessageToGuild(ServerNetworkManager.Instance.RankingPosition, currentRoomNumber);

			ChatManager.Instance.SendChatSystemMessage(
				"system_raid_list_" + raidType + "_" + ServerNetworkManager.Instance.GuildData.GuildManagementData.GroupId, 
				currentRoomNumber + "@" + currentHostPlayerId + "@" + raidIndex + "@" + floor
			);
		}
#if UNITY_IPHONE
		if(raidType == 0)
		{
			ChatManager.Instance.Subscribe("ingame_raid_" + 2 + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
			ChatManager.Instance.Subscribe("system_raid_info_" + 2 + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
			ChatManager.Instance.Subscribe("system_raid_score_" + 2 + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
			ChatManager.Instance.Subscribe("system_raid_position_" + 2 + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
		}
		else
		{
			ChatManager.Instance.Subscribe("ingame_raid_" + raidType + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
			ChatManager.Instance.Subscribe("system_raid_info_" + raidType + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
			ChatManager.Instance.Subscribe("system_raid_score_" + raidType + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
			ChatManager.Instance.Subscribe("system_raid_position_" + raidType + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
		}
#else
		ChatManager.Instance.Subscribe("ingame_raid_" + raidType + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
		ChatManager.Instance.Subscribe("system_raid_info_" + raidType + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
		ChatManager.Instance.Subscribe("system_raid_score_" + raidType + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
		ChatManager.Instance.Subscribe("system_raid_position_" + raidType + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
#endif
	}

	// Called by the non-host player
	public void JoinRaid(string roomId, Action onSuccess, Action<string> onFailure)
	{
		var list = ChatManager.Instance.ChatLogList.Split('\n');
		var targetRaidInfo = list.Reverse().FirstOrDefault(n => n.Split('@')[0].Contains(roomId));

		if(targetRaidInfo != null)
		{
			var raidInfoSplited = targetRaidInfo.Split('@');
			var hostPlayerId = raidInfoSplited[1];
			var raidIndex = int.Parse(raidInfoSplited[2]);
			var floor = int.Parse(raidInfoSplited[3]);

			if(hostPlayerId == ServerNetworkManager.Instance.User.PlayFabId)
			{
				onFailure?.Invoke("raid_not_open");
			}
			else
			{
				if(raidType == 0)
				{
					ServerNetworkManager.Instance.JoinRaid(
						hostPlayerId, 
						raidIndex, 
						() => {
							OnJoinRaid(roomId, hostPlayerId, raidIndex, floor, onSuccess, onFailure);
						},
						(reason) => {
							onFailure?.Invoke(reason);
						}
					);
				}
				else
				{
					ServerNetworkManager.Instance.JoinGuildRaid(
						hostPlayerId, 
						raidIndex, 
						() => {
							OnJoinRaid(roomId, hostPlayerId, raidIndex, floor, onSuccess, onFailure);
						},
						(reason) => {
							onFailure?.Invoke(reason);
						}
					);
				}
			}
		}
		else
		{
			onFailure?.Invoke("raid_not_open");
		}
	}
	
	private void OnJoinRaid(string roomId, string hostPlayerId, int raidIndex, int floor, Action onSuccess, Action<string> onFailure)
	{
		currentRoomNumber = int.Parse(roomId);
		currentHostPlayerId = hostPlayerId;
		currentRaidIndex = raidIndex;
		currentFloor = floor;
		if(raidType == 0)
			currentRaidFloorCache = currentFloor;
		else
			currentGuildRaidFloorCache = currentFloor;

#if UNITY_IPHONE
		if(raidType == 0)
		{
			ChatManager.Instance.Subscribe("ingame_raid_" + 2 + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
			ChatManager.Instance.Subscribe("system_raid_info_" + 2 + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
			ChatManager.Instance.Subscribe("system_raid_score_" + 2 + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
			ChatManager.Instance.Subscribe("system_raid_position_" + 2 + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
		}
		else
		{
			ChatManager.Instance.Subscribe("ingame_raid_" + raidType + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
			ChatManager.Instance.Subscribe("system_raid_info_" + raidType + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
			ChatManager.Instance.Subscribe("system_raid_score_" + raidType + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
			ChatManager.Instance.Subscribe("system_raid_position_" + raidType + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
		}
#else
		ChatManager.Instance.Subscribe("ingame_raid_" + raidType + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
		ChatManager.Instance.Subscribe("system_raid_info_" + raidType + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
		ChatManager.Instance.Subscribe("system_raid_score_" + raidType + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
		ChatManager.Instance.Subscribe("system_raid_position_" + raidType + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
#endif

		StartCoroutine(CheckPlayersCount(onSuccess, onFailure));
	}

	private IEnumerator CheckPlayersCount(Action onSuccess, Action<string> onFailure)
	{
		yield return new WaitForSeconds(2.5f);
		
		var raidAllowedCount = 12;
		if(raidType != 0)
			raidAllowedCount = ServerNetworkManager.Instance.GuildData.MaxMemeberCount;
 
		if(ChatManager.Instance.PlayersInPrivateRoom.Count > raidAllowedCount && ChatManager.Instance.PlayerInPrivateRoomAfterThisPlayer.Count >= raidAllowedCount)
		{
			// Cancel raid, because too many players now
			LeaveRaid(true, false, 0, null, null);

			onFailure?.Invoke("raid_full");
		}
		else
		{
			onSuccess?.Invoke();
		}
	}

	// Called by the host player
	public void StartRaid()
	{
		// This server API call will actually block the players
		if(raidType == 0)
		{
			ServerNetworkManager.Instance.StartRaid(
				() =>{
					OnStartRaid();
				},
				null
			);
		}
		else
		{
			ServerNetworkManager.Instance.StartGuildRaid(
				() =>{
					OnStartRaid();
				},
				null
			);
		}
	}

	private void OnStartRaid()
	{
		// Sync the floor
		ChangeRaidFloor(currentFloor);
		
		// Start of raid is done by the host player, firstly sending '0' damage
		SyncScore(0);
	}

	// Called by the host player
	public void EndRaid(bool didDodge, bool didWin, int skillUseCount, Action onSuccess, Action<string> onFailure)
	{
		// This server API call will actually block the players
		ServerNetworkManager.Instance.EndRaid(
			(!didDodge && didWin),
			skillUseCount,
			() =>{
				OnEndRaid();
				onSuccess?.Invoke();
			},
			(reason) =>{
				OnEndRaid();
				onFailure?.Invoke(reason);
			}
		);
	}

	// Called by the host player (and for guild raid)
	public void EndRaid(bool didDodge, int score, List<string> participantList, List<int> participantScoreList, int season, int skillUseCount, Action onSuccess, Action<string> onFailure)
	{
		// This server API call will actually block the players
		ServerNetworkManager.Instance.EndGuildRaid(
			!didDodge,
			score,
			participantList,
			participantScoreList,
			season,
			skillUseCount,
			() =>{
				OnEndRaid();
				onSuccess?.Invoke();
			},
			(reason) =>{
				OnEndRaid();
				onFailure?.Invoke(reason);
			}
		);
	}
	
	// Called by the non-host player
	public void LeaveRaid(bool didDodge, bool didWin, int skillUseCount, Action onSuccess, Action<string> onFailure)
	{
		ServerNetworkManager.Instance.LeaveRaid(
			(!didDodge && didWin),
			skillUseCount,
			() =>{
				OnEndRaid();
				onSuccess?.Invoke();
			},
			(reason) =>{
				OnEndRaid();
				onFailure?.Invoke(reason);
			}
		);
	}

	// Called by the non-host player (and for guild raid)
	public void LeaveRaid(bool didDodge, int season, int personalScore, int skillUseCount, Action onSuccess, Action<string> onFailure)
	{
		ServerNetworkManager.Instance.LeaveGuildRaid(
			!didDodge, 
			season,
			personalScore,
			skillUseCount,
			() =>{
				OnEndRaid();
				onSuccess?.Invoke();
			},
			(reason) =>{
				OnEndRaid();
				onFailure?.Invoke(reason);
			}
		);
	}

	private void OnEndRaid()
	{
#if UNITY_IPHONE
		if(raidType == 0)
		{
			ChatManager.Instance.Unsubscribe("ingame_raid_" + 2 + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
			ChatManager.Instance.Unsubscribe("system_raid_info_" + 2 + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
			ChatManager.Instance.Unsubscribe("system_raid_score_" + 2 + "_"  + currentHostPlayerId + "_" + currentRaidIndex.ToString());
			ChatManager.Instance.Unsubscribe("system_raid_position_" + 2 + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
		}
		else
		{
			ChatManager.Instance.Unsubscribe("ingame_raid_" + raidType + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
			ChatManager.Instance.Unsubscribe("system_raid_info_" + raidType + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
			ChatManager.Instance.Unsubscribe("system_raid_score_" + raidType + "_"  + currentHostPlayerId + "_" + currentRaidIndex.ToString());
			ChatManager.Instance.Unsubscribe("system_raid_position_" + raidType + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
		}
#else
		ChatManager.Instance.Unsubscribe("ingame_raid_" + raidType + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
		ChatManager.Instance.Unsubscribe("system_raid_info_" + raidType + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
		ChatManager.Instance.Unsubscribe("system_raid_score_" + raidType + "_"  + currentHostPlayerId + "_" + currentRaidIndex.ToString());
		ChatManager.Instance.Unsubscribe("system_raid_position_" + raidType + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString());
#endif
	}

	public void ChangeRaidFloor(int floor)
	{
		this.currentFloor = floor;
#if UNITY_IPHONE
		if(raidType == 0)
		{
			ChatManager.Instance.SendChatFloorChangeMessage(
				"system_raid_info_" + 2 + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString(),
				floor
			);
		}
		else
		{
			ChatManager.Instance.SendChatFloorChangeMessage(
				"system_raid_info_" + raidType + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString(),
				floor
			);
		}
#else
		ChatManager.Instance.SendChatFloorChangeMessage(
			"system_raid_info_" + raidType + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString(),
			floor
		);
#endif

		if(raidType == 0)
			currentRaidFloorCache = currentFloor;
		else
			currentGuildRaidFloorCache = currentFloor;
	}

	public void OnChangeRaidFloor(int floor)
	{
		if(currentHostPlayerId != ServerNetworkManager.Instance.User.PlayFabId)
			currentFloor = floor;

		if(raidType == 0)
			currentRaidFloorCache = currentFloor;
		else
			currentGuildRaidFloorCache = currentFloor;
	}

	public void SyncScore(BigInteger number)
	{
#if UNITY_IPHONE
		if(raidType == 0)
		{
			ChatManager.Instance.SendChatSystemMessage(
				"system_raid_score_" + 2 + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString(), 
				ServerNetworkManager.Instance.User.PlayFabId + "@" + number.ToString()
			);
		}
		else
		{
			ChatManager.Instance.SendChatSystemMessage(
				"system_raid_score_" + raidType + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString(), 
				ServerNetworkManager.Instance.User.PlayFabId + "@" + number.ToString()
			);	
		}
#else
		ChatManager.Instance.SendChatSystemMessage(
			"system_raid_score_" + raidType + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString(), 
			ServerNetworkManager.Instance.User.PlayFabId + "@" + number.ToString()
		);
#endif
	}

	public void SyncPosition(UnityEngine.Vector3 position)
	{
#if UNITY_IPHONE
		if(raidType == 0)
		{
			ChatManager.Instance.SendChatSystemMessage(
				"system_raid_position_" + 2 + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString(), 
				ServerNetworkManager.Instance.User.PlayFabId + "@" + position.ToString()
			);
		}
		else
		{
			ChatManager.Instance.SendChatSystemMessage(
				"system_raid_position_" + raidType + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString(), 
				ServerNetworkManager.Instance.User.PlayFabId + "@" + position.ToString()
			);
		}
#else
		ChatManager.Instance.SendChatSystemMessage(
			"system_raid_position_" + raidType + "_" + currentHostPlayerId + "_" + currentRaidIndex.ToString(), 
			ServerNetworkManager.Instance.User.PlayFabId + "@" + position.ToString()
		);
#endif
	}

}