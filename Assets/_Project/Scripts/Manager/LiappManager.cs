﻿using UnityEngine;
using PlayFab;
using System;
using System.Runtime.InteropServices;

public class LiappManager : MonoBehaviour {

	private static LiappManager instance = null;
	public static LiappManager Instance
	{
		get{
			if(instance == null)
				instance = GameObject.FindObjectOfType<LiappManager>();
			return instance;
		}
	}

	private bool bIsStarted = false;
	private int nRet = Liapp.LiappforUnityImpl.LIAPP_EXCEPTION;

	private string strLiappMessage;

	Liapp.LiappforUnityImpl _LiappAgent;
	Liapp.LiappForUnityImplAndroid _LiappAgentAndroid;

	public void StartLiapp(Action onSuccess)
	{
#if UNITY_IOS
		_LiappAgent = Liapp.LiappforUnityImpl.Instance;

		if (bIsStarted == false)
		{
			nRet = _LiappAgent.LA1();
		}
		else
		{
			nRet = _LiappAgent.LA2();
		}

		Debug.Log("[StartLiapp] nRet : " + nRet);

		if(Liapp.LiappforUnityImpl.LIAPP_SUCCESS == nRet)
		{
			//Success
			bIsStarted = true;
			
			onSuccess?.Invoke();
		}
		else if(Liapp.LiappforUnityImpl.LIAPP_EXCEPTION == nRet)
		{
			//Exception. Bypass or Network Connected Error.
			Application.Quit();
		}
		else
		{
			strLiappMessage = _LiappAgent.GetMessage();

			Debug.Log("[StartLiapp] strLiappMessage : " + strLiappMessage );

			if (Liapp.LiappforUnityImpl.LIAPP_DETECTED == nRet)
			{
				//DETECTED USER BLOCK or ANTI DEBUGGING or ANTI TAMPER
			}
			else if (Liapp.LiappforUnityImpl.LIAPP_DETECTED_ROOTING == nRet)
			{
				//Rooting Detection!
			}
			else if (Liapp.LiappforUnityImpl.LIAPP_DETECTED_VM == nRet)
			{
				//Virtual Machine Detection!
			}
			else if (Liapp.LiappforUnityImpl.LIAPP_DETECTED_HACKING_TOOL == nRet)
			{
				//Hacking Tool Detection!
			}
			else
			{
				//unknown Error
			}

			//Process Terminate
			Application.Quit();
		}
#elif UNITY_ANDROID
		_LiappAgentAndroid = Liapp.LiappForUnityImplAndroid.Instance;
		_LiappAgentAndroid.Init();

		if(bIsStarted == false)
		{
			nRet = _LiappAgentAndroid.LA1();
		}
		else
		{
			nRet = _LiappAgentAndroid.LA2();
		}

		Debug.Log("[StartLiapp] nRet : " + nRet);

		if(Liapp.LiappForUnityImplAndroid.LIAPP_SUCCESS == nRet)
		{
			//Success
			bIsStarted = true;
			
			onSuccess?.Invoke();
		}
		else if(Liapp.LiappForUnityImplAndroid.LIAPP_EXCEPTION == nRet)
		{
			//Exception. Bypass or Network Connected Error.
			Application.Quit();
		}
		else
		{
			strLiappMessage = _LiappAgentAndroid.GetMessage();

			Debug.Log("[StartLiapp] strLiappMessage : " + strLiappMessage );

			if (Liapp.LiappForUnityImplAndroid.LIAPP_DETECTED == nRet)
			{
				//DETECTED USER BLOCK or ANTI DEBUGGING or ANTI TAMPER
			}
			else if (Liapp.LiappForUnityImplAndroid.LIAPP_DETECTED_ROOTING == nRet)
			{
				//Rooting Detection!
			}
			else if(Liapp.LiappForUnityImplAndroid.LIAPP_DETECTED_VM == nRet)
			{
				//Virtual Machine Detection!
			}
			else if (Liapp.LiappForUnityImplAndroid.LIAPP_DETECTED_HACKING_TOOL == nRet)
			{
				//Hacking Tool Detection!
			}
			else
			{
				//unknown Error
			}

			//Process Terminate
			Application.Quit();
		}
#endif
	}

	public string GetAuthKey(string serverKey)
	{
#if UNITY_IOS
		return _LiappAgent.GA(serverKey);
#elif UNITY_ANDROID
		return _LiappAgentAndroid.GA(serverKey);
#else
		return string.Empty;
#endif
	}
}