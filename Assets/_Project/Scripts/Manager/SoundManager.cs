﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using System;

public class SoundManager : MonoBehaviour {

	private static SoundManager instance = null;
	public static SoundManager Instance
	{
		get{
			if(instance == null)
				instance = GameObject.FindObjectOfType<SoundManager>();
			return instance;
		}
	}

	[SerializeField]
	private AudioMixer mixer;

	[SerializeField]
	private List<AudioSource> sounds;

	private float timePassed = 0f;
	private Dictionary<AudioClip, float> audioClipPlayrecords = new Dictionary<AudioClip, float>();

	private Dictionary<string, float> mixerParamDict = new Dictionary<string, float>();
	void Awake()
	{
		AudioSettings.OnAudioConfigurationChanged += ReallocateVolume;
		mixerParamDict.Add("volume_effect", 0f);
		mixerParamDict.Add("volume_bgm", 0f);
		mixerParamDict.Add("volume_voice", 0f);
	}
	private void Update()
	{	
		// Need to increment this.
		timePassed += Time.deltaTime;
	}

	public void PlayBGM()
	{
		StartCoroutine(PlayBGMAfter());
	}

	private IEnumerator PlayBGMAfter()
	{
		// Wait for mixer to set up.
		yield return -1f;
		if(GameObject.Find("BGM") != null)
		{
			var bgm = GameObject.Find("BGM").GetComponent<AudioSource>();
			bgm.Play();
		}
	}

	public void SetEffectVolumeDelayed(float volume)
	{
		StartCoroutine(SetEffectVolumeAfter(volume));
	}

	private IEnumerator SetEffectVolumeAfter(float volume)
	{
		yield return new WaitForEndOfFrame();
		SetEffectVolume(volume);
	}

	public void SetVoiceVolumeDelayed(float volume)
	{
		StartCoroutine(SetVoiceVolumeAfter(volume));
	}

	private IEnumerator SetVoiceVolumeAfter(float volume)
	{
		yield return new WaitForEndOfFrame();
		SetVoiceVolume(volume);
	}

	public void SetEffectVolume(float volume)
	{
		var volumeToUse = Mathf.Lerp(-80f, 0f, Mathf.Pow(volume, 0.25f));
		mixer.SetFloat("volume_effect", volumeToUse);
		mixerParamDict["volume_effect"] = volumeToUse; 
	}

	public void SetVoiceVolume(float volume)
	{
		var volumeToUse = Mathf.Lerp(-80f, 0f, Mathf.Pow(volume, 0.25f));
		mixer.SetFloat("volume_voice", volumeToUse);
		mixerParamDict["volume_voice"] = volumeToUse; 

	}
	
	public void SetBGMVolumeDelayed(float volume)
	{
		StartCoroutine(SetBGMVolumeAfter(volume));
	}

	private IEnumerator SetBGMVolumeAfter(float volume)
	{
		yield return new WaitForEndOfFrame();
		SetBGMVolume(volume);
	}

	public void SetBGMVolume(float volume)
	{
		var volumeToUse = Mathf.Lerp(-80f, 0f, Mathf.Pow(volume, 0.25f));
		mixer.SetFloat("volume_bgm", volumeToUse);
		mixerParamDict["volume_bgm"] = volumeToUse; 
	}

	public void MuteBGM()
	{
		SetBGMVolume(0f);
	}

	public void MuteEffectVolume()
	{
		SetEffectVolume(0f);
	}

	public bool CountAudioClipPlay(AudioClip clipToPlay)
	{
		if(audioClipPlayrecords.ContainsKey(clipToPlay))
		{
			if(timePassed - audioClipPlayrecords[clipToPlay] >= 0.05f)
			{
				audioClipPlayrecords[clipToPlay] = timePassed;
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			audioClipPlayrecords.Add(clipToPlay, timePassed);
			return true;
		}

	}

	public void PlaySound(string soundName)
	{
		if(soundName == "Button")
			sounds[0].Play();
		else if(soundName == "Upgrade")
			sounds[1].Play();
		else if(soundName == "UpgradeFail")
			sounds[2].Play();
		else if(soundName == "New")
			sounds[3].Play();
		else if(soundName == "Coin")
			sounds[4].Play();
	}

	public void Vibrate()
	{
		if(OptionManager.Instance.IsVibrationOn)
			Handheld.Vibrate();
	}
	public void ReallocateVolume(bool value)
	{
		foreach (var item in mixerParamDict)
		{
			mixer.SetFloat(item.Key, item.Value);
		}
	}
}