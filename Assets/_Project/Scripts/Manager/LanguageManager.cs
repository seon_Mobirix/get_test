﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

using UnityEngine;
using Sirenix.OdinInspector;

using PlayFab;

public class LanguageManager : MonoBehaviour
{
    private static LanguageManager instance = null;
	public static LanguageManager Instance
	{
		get{
			if(instance == null)
				instance = GameObject.FindObjectOfType<LanguageManager>();
			return instance;
		}
	}

    [Title("Json Info")]

    [ReadOnly]
	[SerializeField]
	private List<LocalizationModel> entity;

    [ReadOnly]
	[SerializeField]
	private List<LocalizationModel> ui;

    [ReadOnly]
	[SerializeField]
	private List<LocalizationModel> narrative;
    
    private void Awake()
	{
		LoadData();
	}

    private void LoadData()
	{
        var json = Resources.Load<TextAsset>("Text Sheets/text_language");
        var tmpResource = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<LanguageDataModel>(json.text);
        entity = tmpResource.Entity;
        ui = tmpResource.UI;
        narrative = tmpResource.Narrative;
    }

    public string GetTextData(LanguageDataType dataTpye, string key, bool askTag = false)
    {
		LocalizationModel tmpLocalizationModel = null;
		
		if(dataTpye == LanguageDataType.ENTITY)
			tmpLocalizationModel = entity.FirstOrDefault(n => n.Id == key);
		else if(dataTpye == LanguageDataType.UI)
			tmpLocalizationModel = ui.FirstOrDefault(n => n.Id == key);
		else if(dataTpye == LanguageDataType.NARRATIVE)
			tmpLocalizationModel = narrative.FirstOrDefault(n => n.Id == key);

		string toReturn = "";
		if(tmpLocalizationModel != null)
		{
			if(askTag)
				toReturn = tmpLocalizationModel.Tag;
			else if(OptionManager.Instance.Language == OptionManager.LanguageModeType.ko)
				toReturn = tmpLocalizationModel.Korean;
			else if(OptionManager.Instance.Language == OptionManager.LanguageModeType.en)
				toReturn = tmpLocalizationModel.English;
			else if(OptionManager.Instance.Language == OptionManager.LanguageModeType.ja)
				toReturn = (tmpLocalizationModel.Japanese != null)? tmpLocalizationModel.Japanese : tmpLocalizationModel.English;
			else if(OptionManager.Instance.Language == OptionManager.LanguageModeType.zh_chs)
				toReturn = (tmpLocalizationModel.ChineseSimplified != null)? tmpLocalizationModel.ChineseSimplified : tmpLocalizationModel.English;
			else if(OptionManager.Instance.Language == OptionManager.LanguageModeType.zh_cht)
				toReturn = (tmpLocalizationModel.ChineseTraditional != null)? tmpLocalizationModel.ChineseTraditional : tmpLocalizationModel.English;
			else if(OptionManager.Instance.Language == OptionManager.LanguageModeType.de)
				toReturn = (tmpLocalizationModel.German != null)? tmpLocalizationModel.German : tmpLocalizationModel.English;
			else if(OptionManager.Instance.Language == OptionManager.LanguageModeType.es)
				toReturn = (tmpLocalizationModel.Spanish != null)? tmpLocalizationModel.Spanish : tmpLocalizationModel.English;
			else if(OptionManager.Instance.Language == OptionManager.LanguageModeType.fr)
				toReturn = (tmpLocalizationModel.French != null)? tmpLocalizationModel.French : tmpLocalizationModel.English;
			else if(OptionManager.Instance.Language == OptionManager.LanguageModeType.vi)
				toReturn = (tmpLocalizationModel.Vietnamese != null)? tmpLocalizationModel.Vietnamese : tmpLocalizationModel.English;
			else if(OptionManager.Instance.Language == OptionManager.LanguageModeType.th)
				toReturn = (tmpLocalizationModel.Thai != null)? tmpLocalizationModel.Thai : tmpLocalizationModel.English;
			else if(OptionManager.Instance.Language == OptionManager.LanguageModeType.ru)
				toReturn = (tmpLocalizationModel.Russian != null)? tmpLocalizationModel.Russian : tmpLocalizationModel.English;
			else if(OptionManager.Instance.Language == OptionManager.LanguageModeType.it)
				toReturn = (tmpLocalizationModel.Italian != null)? tmpLocalizationModel.Italian : tmpLocalizationModel.English;
			else if(OptionManager.Instance.Language == OptionManager.LanguageModeType.nl)
				toReturn = (tmpLocalizationModel.Dutch != null)? tmpLocalizationModel.Dutch : tmpLocalizationModel.English;
			else
				toReturn = tmpLocalizationModel.English;
		}

		if(key.Contains("button_") || key.Contains("title_"))
			return toReturn.ToUpper();
		else
			return toReturn;
	}

	public string NumberToString(BigInteger number)
	{
		if(OptionManager.Instance.Language == OptionManager.LanguageModeType.ko || OptionManager.Instance.Language == OptionManager.LanguageModeType.ja || OptionManager.Instance.Language == OptionManager.LanguageModeType.zh_chs || OptionManager.Instance.Language == OptionManager.LanguageModeType.zh_cht)
			return NumberToStringBy4(number);
		else
			return NumberToStringBy3(number);
	}

	public string NumberToStringBy4(BigInteger number)
	{
		if(number < 10000)
		{
			return number.ToString();
		}
		else
		{
			var logValue = BigInteger.Log10(number);
			var place = (int)logValue / 4;
			
			var numberToShow = number;
			for(int i = 0; i < place - 1; i++)
			{
				numberToShow /= 10000;
			}
			var numberToShow2 = numberToShow - (numberToShow / 10000) * 10000;

			var placeName = GetTextData(LanguageDataType.ENTITY, "number_place_" + place.ToString("D3"));
			var placeName2 = GetTextData(LanguageDataType.ENTITY, "number_place_" + (place - 1).ToString("D3"));
			
			if(numberToShow2 != 0)
				return (numberToShow - numberToShow2) / 10000 + placeName + numberToShow2 + placeName2;
			else
				return (numberToShow - numberToShow2) / 10000 + placeName;
		}
	}

	public string NumberToStringBy3(BigInteger number)
	{
		if(number < 1000)
		{
			return number.ToString();
		}
		else
		{
			var logValue = BigInteger.Log10(number);
			var place = (int)logValue / 3;

			var numberToShow = number;
			for(int i = 0; i < place - 1; i++)
			{
				numberToShow /= 1000;
			}

			var placeName = GetPlaceName(place);
			
			return string.Format("{0:f2}", ((double)numberToShow / 1000d)) + placeName;
		}	
	}

	public static string GetPlaceName(int column)
	{
		string columnString = "";
		decimal columnNumber = column;
		while (columnNumber > 0)
		{
			decimal currentLetterNumber = (columnNumber - 1) % 26;
			char currentLetter = (char)(currentLetterNumber + 65);
			columnString = currentLetter + columnString;
			columnNumber = (columnNumber - (currentLetterNumber + 1)) / 26;
		}
		return columnString;
	}
}
