﻿using System.Collections.Generic;

using UnityEngine;
using PlayFab;
/**/
/**/
/**/
/**/
public class AntiCheatManager : MonoBehaviour {

    public bool IsMemorySuspicious = false;

	private static AntiCheatManager instance = null;
	public static AntiCheatManager Instance
	{
		get{
			if(instance == null)
				instance = GameObject.FindObjectOfType<AntiCheatManager>();
			return instance;
		}
	}

    public void OnCheatDetected()
    {
        IsMemorySuspicious = true;
    }
}
