﻿using System;
using UnityEngine;
using Sirenix.OdinInspector;

public class OptionManager : MonoBehaviour {

	private static OptionManager instance = null;
	public static OptionManager Instance
	{
		get{
			if(instance == null)
				instance = GameObject.FindObjectOfType<OptionManager>();
			return instance;
		}
	}

	public enum LoginModeType {UNSET, GUEST, PLATFORM_DEFAULT}

	public enum LanguageModeType {en, ko, zh_chs, zh_cht, ja, de, es, fr, vi, th, ru, it, nl}

	public class UnityCloudBuildManifestData
	{
		public string scmCommitId;
		public string scmBranch;
		public string buildNumber;
		public string buildStartTime;
		public string projectId;
		public string bundleId;
		public string unityVersion;
		public string xcodeVersion;
		public string cloudBuildTargetName;
	}

	[ReadOnly]
	[SerializeField]
	private UnityCloudBuildManifestData manifestData;
	public UnityCloudBuildManifestData ManifestData{
		get{
			if(!isValueLoaded)
				LoadData();
			return manifestData;
		}
	}

	[ReadOnly]
	[SerializeField]
	private LoginModeType loginMode = LoginModeType.UNSET;
	public LoginModeType LoginMode{
		get{
			if(!isValueLoaded)
				LoadData();
			return loginMode;
		}
		set{
			if(value != loginMode)
				SaveLoginMode(value);
		}
	}

	[ReadOnly]
	[SerializeField]
	private bool forceGlobalChat = false;
	public bool ForceGlobalChat{
		get{
			if(!isValueLoaded)
				LoadData();
			return forceGlobalChat;
		}
		set{
			if(value != forceGlobalChat)
				SaveForceGlobalChat(value);
		}
	}

	[ReadOnly]
	[SerializeField]
	private bool isPushOff = false;
	public bool IsPushOff{
		get{
			return isPushOff;
		}
		set{
			isPushOff = value;
		}
	}

	[ReadOnly]
	[SerializeField]
	private float bgmVolume = 0.75f;
	public float BGMVolume{
		get{
			if(!isValueLoaded)
				LoadData();
			return bgmVolume;
		}
		set{
			if(value != bgmVolume)
				SaveBGM(value);
		}
	}

	[ReadOnly]
	[SerializeField]
	private float soundVolume = 1f;
	public float SoundVolume{
		get{
			if(!isValueLoaded)
				LoadData();
			return soundVolume;
		}
		set{
			if(value != soundVolume)
				SaveSound(value);
		}
	}

	[ReadOnly]
	[SerializeField]
	private bool isVibrationOn = true;
	public bool IsVibrationOn{
		get{
			if(!isValueLoaded)
				LoadData();
			return isVibrationOn;
		}
		set{
			if(value != isVibrationOn)
				SaveVibration(value);
		}
	}

	[ReadOnly]
	[SerializeField]
	private LanguageModeType language = LanguageModeType.en;
	public LanguageModeType Language{
		get{
			if(!isValueLoaded)
				LoadData();
			return language;
		}
		set{
			if(value != language)
			{
				SaveLanguage(value);
				
				// Need to ensure the mimimum font size.
				FontSetter.didSetMinimumFontSize = false;
			}
		}
	}
	public bool IsLanguageSelected{
		get{
			return PlayerPrefs.HasKey("option_language_type_v2");
		}
	}

	[ReadOnly]
	[SerializeField]
	private bool isDamageInfoOff = false;
	public bool IsDamageInfoOff{
		get{
			if(!isValueLoaded)
				LoadData();
			return isDamageInfoOff;
		}
		set{
			if(value != isDamageInfoOff)
				SaveDamageInfoOff(value);
		}
	}

	[ReadOnly]
	[SerializeField]
	private bool isPerformanceMonitorOn = false;
	public bool IsPerformanceMonitorOn{
		get{
			if(!isValueLoaded)
				LoadData();
			return isPerformanceMonitorOn;
		}
		set{
			if(value != isPerformanceMonitorOn)
				SavePerformanceMonitor(value);
		}
	}

	[ReadOnly]
	[SerializeField]
	private bool isPostEffectOn = true;
	public bool IsPostEffectOn{
		get{
			if(!isValueLoaded)
				LoadData();
			return isPostEffectOn;
		}
		set{
			if(value != isPostEffectOn)
				SavePostEffect(value);
		}
	}

	[ReadOnly]
	[SerializeField]
	private int targetFrameRate = -1;
	public int TargetFrameRate{
		get{
			if(!isValueLoaded)
				LoadData();
			return targetFrameRate;
		}
		set{
			if(value != targetFrameRate)
				SaveTargetFrameRate(value);
		}
	}

	[ReadOnly]
	[SerializeField]
	private bool isTouchMoveOn = true;
	public bool IsTouchMoveOn{
		get{
			if(!isValueLoaded)
				LoadData();
			return isTouchMoveOn;
		}
		set{
			if(value != isTouchMoveOn)
				SaveTouchMove(value);
		}
	}

	[ReadOnly]
	[SerializeField]
	private bool isAutoSkillOn = true;
	public bool IsAutoSkillOn{
		get{
			if(!isValueLoaded)
				LoadData();
			return isAutoSkillOn;
		}
		set{
			if(value != isAutoSkillOn)
				SaveAutoSkill(value);
		}
	}

	[ReadOnly]
	[SerializeField]
	private bool notifyNewRaidsToggle = true;
	public bool NotifyNewRaidsToggle{
		get{
			if(!isValueLoaded)
				LoadData();
			return notifyNewRaidsToggle;
		}
		set{
			if(value != notifyNewRaidsToggle)
				SaveNotifyNewRaids(value);
		}
	}


	[ReadOnly]
	[SerializeField]
	private bool isValueLoaded = false;

#region None-persistent options

	[ReadOnly]
	[SerializeField]
	private bool isAutoRetry = false;
	public bool IsAutoRetry{
		get{
			return isAutoRetry;
		}
		set{
			isAutoRetry = value;
		}
	}

	[ReadOnly]
	[SerializeField]
	private bool isFoldLeftHorizontalButtons = false;
	public bool IsFoldLeftHorizontalButtons{
		get{
			return isFoldLeftHorizontalButtons;
		}
		set{
			isFoldLeftHorizontalButtons = value;
		}
	}

#endregion

	private void Awake()
	{
		LoadData();
	}

	private void LoadData()
	{
		isValueLoaded = true;
		
		if(PlayerPrefs.HasKey("option_login_mode"))
		{
			var valueToUse = PlayerPrefs.GetInt("option_login_mode");
			loginMode = (LoginModeType)valueToUse;
		}

		if(PlayerPrefs.HasKey("option_language_type_v2"))
		{
			var valueToUse = PlayerPrefs.GetString("option_language_type_v2");
			language = (OptionManager.LanguageModeType)Enum.Parse(typeof(OptionManager.LanguageModeType), valueToUse);
		}
		else
		{
			switch(Application.systemLanguage)
			{
				case SystemLanguage.English:
					language = LanguageModeType.en;
					break;
				case SystemLanguage.Korean:
					language = LanguageModeType.ko;
					break;
				case SystemLanguage.Japanese:
					language = LanguageModeType.ja;
					break;
				case SystemLanguage.ChineseSimplified:
					language = LanguageModeType.zh_chs;
					break;
				case SystemLanguage.ChineseTraditional:
					language = LanguageModeType.zh_cht;
					break;
				case SystemLanguage.German:
					language = LanguageModeType.de;
					break;
				case SystemLanguage.Spanish:
					language = LanguageModeType.es;
					break;
				case SystemLanguage.French:
					language = LanguageModeType.fr;
					break;
				case SystemLanguage.Vietnamese:
					language = LanguageModeType.vi;
					break;
				case SystemLanguage.Thai:
					language = LanguageModeType.th;
					break;
				case SystemLanguage.Russian:
					language = LanguageModeType.ru;
					break;
				case SystemLanguage.Italian:
					language = LanguageModeType.it;
					break;
				case SystemLanguage.Dutch:
					language = LanguageModeType.nl;
					break;
			}

			// Save the initial preferences
			PlayerPrefs.SetString("option_language_type_v2", language.ToString());
		}
		
		if(PlayerPrefs.HasKey("option_chat_force_global"))
		{
			var valueToUse = PlayerPrefs.GetInt("option_chat_force_global");
			if (valueToUse == 1)
				forceGlobalChat = true;
			else
				forceGlobalChat = false;
		}

		if(PlayerPrefs.HasKey("option_sound_voulume"))
		{
			var valueToUse = PlayerPrefs.GetFloat("option_sound_voulume");
			soundVolume = valueToUse;
			SoundManager.Instance.SetEffectVolumeDelayed(soundVolume);
		}

		if(PlayerPrefs.HasKey("option_bgm_volume"))
		{
			var valueToUse = PlayerPrefs.GetFloat("option_bgm_volume");
			bgmVolume = valueToUse;
			SoundManager.Instance.SetBGMVolumeDelayed(bgmVolume);
		}
		else
		{
			bgmVolume = 0.75f;
		}

		if(PlayerPrefs.HasKey("option_is_vibration_on"))
		{
			var valueToUse = PlayerPrefs.GetInt("option_is_vibration_on");
			if (valueToUse == 1)
				isVibrationOn = true;
			else
				isVibrationOn = false;
		}

		var manifestAsset = (TextAsset)Resources.Load("UnityCloudBuildManifest.json");
		if (manifestAsset != null)
		{
			manifestData = JsonUtility.FromJson<UnityCloudBuildManifestData>(manifestAsset.text);
		}
		else
		{
			Debug.Log("OptionManager.LoadData: No cloud manifest");
		}

		if(PlayerPrefs.HasKey("option_is_post_effect_on"))
		{
			var valueToUse = PlayerPrefs.GetInt("option_is_post_effect_on");
			if (valueToUse == 0)
				isPostEffectOn = false;
			else
				isPostEffectOn = true;
		}

		if(PlayerPrefs.HasKey("option_is_performance_monitor_on"))
		{
			var valueToUse = PlayerPrefs.GetInt("option_is_performance_monitor_on");
			if (valueToUse == 1)
				isPerformanceMonitorOn = true;
			else
				isPerformanceMonitorOn = false;
		}

		if(PlayerPrefs.HasKey("option_is_damage_info_off"))
		{
			var valueToUse = PlayerPrefs.GetInt("option_is_damage_info_off");
			if (valueToUse == 1)
				isDamageInfoOff = true;
			else
				isDamageInfoOff = false;
		}

		if(PlayerPrefs.HasKey("option_target_framerate"))
		{
			targetFrameRate = PlayerPrefs.GetInt("option_target_framerate");
		}
		else
		{
#if UNITY_ANDROID
			targetFrameRate = -1;
#elif UNITY_IPHONE
			targetFrameRate = -1;
#else
			targetFrameRate = -1;
#endif
		}

		PerformanceManager.Instance.AdjustFramerate();

		if(PlayerPrefs.HasKey("option_is_touch_move_on"))
		{
			var valueToUse = PlayerPrefs.GetInt("option_is_touch_move_on");
			if (valueToUse == 1)
				isTouchMoveOn = true;
			else
				isTouchMoveOn = false;
		}

		if(PlayerPrefs.HasKey("option_auto_skill_on"))
		{
			var valueToUse = PlayerPrefs.GetInt("option_auto_skill_on");
			if (valueToUse == 1)
				isAutoSkillOn = true;
			else
				isAutoSkillOn = false;
		}

		if(PlayerPrefs.HasKey("option_notify_new_raids"))
		{
			var valueToUse = PlayerPrefs.GetInt("option_notify_new_raids");
			if (valueToUse == 1)
				notifyNewRaidsToggle = true;
			else
				notifyNewRaidsToggle = false;
		}
	}

	private void SaveLoginMode(LoginModeType mode)
	{
		this.loginMode = mode;
		Debug.Log("OptionManager.SaveLoginMode: " + (int)mode);
		PlayerPrefs.SetInt("option_login_mode", (int)mode);
	}

	private void SaveForceGlobalChat(bool isOn)
	{
		this.forceGlobalChat = isOn;
		Debug.Log("OptionManager.SaveForceGlobalChat: " + isOn);
		if(isOn)
			PlayerPrefs.SetInt("option_chat_force_global", 1);
		else
			PlayerPrefs.SetInt("option_chat_force_global", 0);
	}

	private void SaveBGM(float value)
	{
		this.bgmVolume = value;
		Debug.Log("OptionManager.SaveBGM.Volume: " + bgmVolume);
		PlayerPrefs.SetFloat("option_bgm_volume", bgmVolume);
		SoundManager.Instance.SetBGMVolume(this.bgmVolume);
	}

	private void SaveSound(float value)
	{
		this.soundVolume = value;
		Debug.Log("OptionManager.SaveSound.Voulume: " + soundVolume);
		PlayerPrefs.SetFloat("option_sound_voulume", soundVolume);
		SoundManager.Instance.SetEffectVolume(this.soundVolume);
	}

	private void SaveVibration(bool isOn)
	{
		this.isVibrationOn = isOn;
		Debug.Log("OptionManager.SaveVibration: " + isOn);
		if(isOn)
			PlayerPrefs.SetInt("option_is_vibration_on", 1);
		else
			PlayerPrefs.SetInt("option_is_vibration_on", 0);
	}

	private void SaveLanguage(LanguageModeType mode)
	{
		this.language = mode;
		Debug.Log("OptionManager.SaveLanguage: " + mode);
		PlayerPrefs.SetString("option_language_type_v2", mode.ToString());
	}

	private void SavePostEffect(bool isOn)
	{
		this.isPostEffectOn = isOn;
		Debug.Log("OptionManager.SavePostEffect: " + isOn);
		if(isOn)
			PlayerPrefs.SetInt("option_is_post_effect_on", 1);
		else
			PlayerPrefs.SetInt("option_is_post_effect_on", 0);
	}

	private void SavePerformanceMonitor(bool isOn)
	{
		this.isPerformanceMonitorOn = isOn;
		Debug.Log("OptionManager.SavePerformanceMonitor: " + isOn);
		if(isOn)
			PlayerPrefs.SetInt("option_is_performance_monitor_on", 1);
		else
			PlayerPrefs.SetInt("option_is_performance_monitor_on", 0);
	}

	private void SaveDamageInfoOff(bool isOn)
	{
		this.isDamageInfoOff = isOn;
		Debug.Log("OptionManager.SaveDamageInfoOff: " + isOn);
		if(isOn)
			PlayerPrefs.SetInt("option_is_damage_info_off", 1);
		else
			PlayerPrefs.SetInt("option_is_damage_info_off", 0);
	}

	private void SaveTargetFrameRate(int frame)
	{
		if(this.targetFrameRate == frame)
			return;
		this.targetFrameRate = frame;
		Debug.Log("OptionManager.SaveTargetFrameRate: " + frame);
		PlayerPrefs.SetInt("option_target_framerate", frame);
		
		PerformanceManager.Instance.AdjustFramerate();

		PlayerPrefs.DeleteKey("frame_rate_cap");
	}

	private void SaveTouchMove(bool isOn)
	{
		this.isTouchMoveOn = isOn;
		Debug.Log("OptionManager.SaveTouchMove: " + isOn);
		if(isOn)
			PlayerPrefs.SetInt("option_is_touch_move_on", 1);
		else
			PlayerPrefs.SetInt("option_is_touch_move_on", 0);
	}

	private void SaveAutoSkill(bool isOn)
	{
		this.isAutoSkillOn = isOn;
		Debug.Log("OptionManager.SaveAutoSkill: " + isOn);
		if(isOn)
			PlayerPrefs.SetInt("option_auto_skill_on", 1);
		else
			PlayerPrefs.SetInt("option_auto_skill_on", 0);
	}

	private void SaveNotifyNewRaids(bool isOn)
	{
		this.notifyNewRaidsToggle = isOn;
		Debug.Log("OptionManager.SaveNotifyNewRaids: " + isOn);
		if(isOn)
			PlayerPrefs.SetInt("option_notify_new_raids", 1);
		else
			PlayerPrefs.SetInt("option_notify_new_raids", 0);
	}
}
