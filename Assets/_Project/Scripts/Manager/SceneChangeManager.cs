﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SceneChangeManager : MonoBehaviour {

	private static SceneChangeManager instance = null;
	public static SceneChangeManager Instance
	{
		get{
			if(instance == null)
				instance = GameObject.FindObjectOfType<SceneChangeManager>();
			return instance;
		}
	}

	private string previousSceneName = "";
	public string PreviousSceneName{
		get{
			return previousSceneName;
		}
	}

	public string SceneName{
		get{
			return SceneManager.GetActiveScene().name;
		}
	}

	private int numberOfSceneChanges = 0;
	public int NumberOfSceneChanges{
		get{
			return numberOfSceneChanges;
		}
	}

	public bool ForceShowPromotion = false;

	public void LoadMainGame(bool forceRestart = false, bool forceIncrementNumberOfChanges = false)
	{
		if(SceneManager.GetActiveScene().name != "MainGame" || forceRestart)
		{
			if(!forceRestart || forceIncrementNumberOfChanges)
				numberOfSceneChanges ++;
			previousSceneName = SceneManager.GetActiveScene().name;
			SceneManager.LoadScene("MainGame");
		}

		// Need to remove this
		previousMapName = null;
	}

	public void LoadArenaGame(bool forceRestart = false, bool forceIncrementNumberOfChanges = false)
	{
		if(SceneManager.GetActiveScene().name != "ArenaGame" || forceRestart)
		{
			if(!forceRestart || forceIncrementNumberOfChanges)
				numberOfSceneChanges ++;
			previousSceneName = SceneManager.GetActiveScene().name;
			SceneManager.LoadScene("ArenaGame");
		}
	}

	public void LoadRaidGame(bool forceRestart = false, bool forceIncrementNumberOfChanges = false)
	{
		if(SceneManager.GetActiveScene().name != "RaidGame" || forceRestart)
		{
			if(!forceRestart || forceIncrementNumberOfChanges)
				numberOfSceneChanges ++;
			previousSceneName = SceneManager.GetActiveScene().name;
			SceneManager.LoadScene("RaidGame");
		}
	}

	public void LoadGuildRaidGame(bool forceRestart = false, bool forceIncrementNumberOfChanges = false)
	{
		if(SceneManager.GetActiveScene().name != "GuildRaidGame" || forceRestart)
		{
			if(!forceRestart || forceIncrementNumberOfChanges)
				numberOfSceneChanges ++;
			previousSceneName = SceneManager.GetActiveScene().name;
			SceneManager.LoadScene("GuildRaidGame");
		}
	}

	public void LoadTowerGame(bool forceRestart = false, bool forceIncrementNumberOfChanges = false)
	{
		if(SceneManager.GetActiveScene().name != "TowerGame" || forceRestart)
		{
			if(!forceRestart || forceIncrementNumberOfChanges)
				numberOfSceneChanges ++;
			previousSceneName = SceneManager.GetActiveScene().name;
			SceneManager.LoadScene("TowerGame");
		}
	}

	string previousMapName = null;
	public AsyncOperation LoadMap(int worldIndex) 
	{
		var mapName = "Map_" + worldIndex.ToString("D2");
		if(previousMapName != null && mapName != previousMapName)
			SceneManager.UnloadSceneAsync(previousMapName);
		var operation = SceneManager.LoadSceneAsync(mapName, LoadSceneMode.Additive);
		previousMapName = mapName;

		return operation;
	}

}