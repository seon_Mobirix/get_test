﻿using System.Collections.Generic;

using UnityEngine;
using PlayFab;

public class BalanceInfoManager : MonoBehaviour {

	private static BalanceInfoManager instance = null;
	public static BalanceInfoManager Instance
	{
		get{
			if(instance == null)
				instance = GameObject.FindObjectOfType<BalanceInfoManager>();
			return instance;
		}
	}

	[SerializeField]
    public List<StageInfoModel> StageInfoList;
	[SerializeField]
    public List<GoldInfoModel> RewardGoldInfoList;
	[SerializeField]
    public List<GoldInfoModel> RequiredGoldInfoList;
	[SerializeField]
    public List<UpgradeInfoModel> UpgradeInfoList;
	[SerializeField]
    public List<AdditionalUpgradeInfoModel> AdditionalUpgradeInfoList;

	[SerializeField]
    public List<ClassInfoModel> ClassInfoList;
	[SerializeField]
    public List<WeaponInfoModel> WeaponInfoList;
	[SerializeField]
    public List<MercenaryInfoModel> MercenaryInfoList;
	[SerializeField]
    public List<RelicInfoModel> RelicInfoList;
	[SerializeField]
    public List<RelicInfoModel> LegendaryRelicInfoList;
	[SerializeField]
    public List<GuildRelicInfoModel> GuildRelicInfoList;
	[SerializeField]
	public List<WingInfoModel> WingInfoList;
	[SerializeField]
	public List<SkillInfoModel> SkillInfoList;
	[SerializeField]
    public List<PetInfoModel> PetInfoList;
	[SerializeField]
    public List<TrophyInfoModel> TrophyInfoList;
	[SerializeField]
    public List<SummonInfoModel> SummonInfoList;
	[SerializeField]
    public List<AchievementInfoModel> AchievementInfoList;

	[SerializeField]
    public List<MonsterInfoModel> MonsterInfoList;
    
	public void LoadDataFromServer(string balanceMainGrowth, string balanceInventoryGrowth, string balanceMonster, string balanceAchievement)
	{
		var tmpModels = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<BalanceInfoModel>(balanceMainGrowth);
		StageInfoList = tmpModels.StageInfo;
		RewardGoldInfoList = tmpModels.RewardGoldInfo;
		RequiredGoldInfoList = tmpModels.RequiredGoldInfo;
		UpgradeInfoList = tmpModels.UpgradeInfo;
		AdditionalUpgradeInfoList = tmpModels.AdditionalUpgradeInfo;

		tmpModels = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<BalanceInfoModel>(balanceInventoryGrowth);
		ClassInfoList = tmpModels.ClassInfo;
		WeaponInfoList = tmpModels.WeaponInfo;
		MercenaryInfoList = tmpModels.MercenaryInfo;
        RelicInfoList = tmpModels.RelicInfo;
		LegendaryRelicInfoList = tmpModels.LegendaryRelicInfo;
		GuildRelicInfoList = tmpModels.GuildRelicInfo;
		WingInfoList = tmpModels.WingInfo;
		SkillInfoList = tmpModels.SkillInfo;
		PetInfoList = tmpModels.PetInfo;
		TrophyInfoList = tmpModels.TrophyInfo;
		SummonInfoList = tmpModels.SummonInfo;

		tmpModels = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<BalanceInfoModel>(balanceMonster);
		MonsterInfoList = tmpModels.MonsterInfo;

		tmpModels = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<BalanceInfoModel>(balanceAchievement);
		AchievementInfoList = tmpModels.AchievementInfo;
	}

	public void LoadDummy()
	{
		var balanceMainGrowth = Resources.Load<TextAsset>("Balance Sheets/balance_main_growth").text;
		var balanceUpgrade = Resources.Load<TextAsset>("Balance Sheets/balance_inventory_growth").text;
		var balanceMonster = Resources.Load<TextAsset>("Balance Sheets/balance_monster").text;
		var balanceAchievement = Resources.Load<TextAsset>("Balance Sheets/balance_achievement").text;

		LoadDataFromServer(balanceMainGrowth, balanceUpgrade, balanceMonster, balanceAchievement);
    }
}