﻿using System;
using UnityEngine;

public class UIManager : MonoBehaviour {

	[SerializeField]
	private Transform alertPopup1;
	[SerializeField]
	private Transform alertPopup2;

	public bool IsArenaSpeedRate = false;
	public bool IsAutoOn = false;
	public bool IsCameraFollowRotation = false;
	
    public bool IsTouchedForAttackRecently = false;
	public int SelectedSkillIndex = 0;
	public bool IsTouchedForMoveRecently = false;
	public UnityEngine.Vector3 MoveDestination = Vector3.zero;
	public bool NeedUIForBoss = false;

	private static UIManager instance = null;
	public static UIManager Instance
	{
		get{
			if(instance == null)
				instance = GameObject.FindObjectOfType<UIManager>();
			return instance;
		}
	}

	public void ShowCanvas(string name)
	{
		var target = GameObject.Find(name);
		if(target != null)
		{
			target.GetComponent<Canvas>().enabled = true;
		}
		else
		{
			Debug.LogWarning("The canvas with name of " + name + "isn't exist.");
		}
	}

	public void HideCanvas(string name)
	{
		var target = GameObject.Find(name);
		if(target != null)
		{
			target.GetComponent<Canvas>().enabled = false;
		}
		else
		{
			Debug.LogWarning("The canvas with name of " + name + "isn't exist.");
		}
	}

	public bool IsCanvasShowing(string name)
	{
		var target = GameObject.Find(name);
		if(target != null && target.GetComponent<Canvas>().enabled)
			return true;
		else
			return false;
	}

	public void ShowAlert(string text, Action OnSucces, Action OnCancel, bool isOverFade = false, bool showYesNo = false)
	{
		GameObject target = GameObject.Find("Popup Canvas");

		if(isOverFade)
		{
			target = GameObject.Find("Popup Canvas Over Fade");
		}

		if(target != null)
		{
			if(OnCancel == null)
			{
				var tmp = GameObject.Instantiate<Transform>(alertPopup1);
				tmp.SetParent(target.transform, false);
				tmp.GetComponent<UIPopupAlert>().OpenWithValues(text, showYesNo, OnSucces, OnCancel);
			}
			else
			{
				var tmp = GameObject.Instantiate<Transform>(alertPopup2);
				tmp.SetParent(target.transform, false);
				tmp.GetComponent<UIPopupAlert>().OpenWithValues(text, showYesNo, OnSucces, OnCancel);
			}
		}
	}

	public void ShowAlertLocalized(string key, Action OnSucces, Action OnCancel, bool isOverFade = false, bool showYesNo = false)
	{
		ShowAlert(LanguageManager.Instance.GetTextData(LanguageDataType.UI , key), OnSucces, OnCancel, isOverFade, showYesNo);
	}

	public void ResetInputValues()
	{
		IsTouchedForAttackRecently = false;
		SelectedSkillIndex = 0;
		IsTouchedForMoveRecently = false;
		UnityEngine.Vector3 MoveDestination = Vector3.zero;
		NeedUIForBoss = false;
	}

}