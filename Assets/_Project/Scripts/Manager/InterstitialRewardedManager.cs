﻿using System;
using System.Collections.Generic;
using GoogleMobileAds.Api;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Analytics;

public class InterstitialRewardedManager : MonoBehaviour, IUnityAdsListener{

	private static InterstitialRewardedManager instance = null;
	public static InterstitialRewardedManager Instance
	{
		get{
			if(instance == null)
				instance = GameObject.FindObjectOfType<InterstitialRewardedManager>();
			return instance;
		}
	}
	
#region Google Ads Variables
	private RewardedAd rewardedAd;
#endregion

	private Action<ShowResult> onAdvertisementComplete;

	private int googleAdsFailCounts = 0;

	bool isInited = false;
	public void Init()
	{
		if(!isInited)
		{
			MobileAds.Initialize((initInfo) => 
			{
				Debug.Log("InterstitialRewardedManager.Init().MobileAds.Initialize");
				
				Dictionary<string, AdapterStatus> dic = initInfo.getAdapterStatusMap();

                foreach (KeyValuePair<string, AdapterStatus> pair in dic)
                {
                    Debug.Log($"ADMOB initialize adapter : {pair.Key}, desc : {pair.Value.Description} latency : {pair.Value.Latency} statue : {pair.Value.InitializationState}");
                }

				RequestAndLoadRewardedAd(null, null);
			});

			Advertisement.AddListener(this);
#if UNITY_ANDROID
			Advertisement.Initialize("3953995", false);
#elif UNITY_IPHONE
			Advertisement.Initialize("3953994", false);
#endif

#if UNITY_IPHONE
			// Init setting for tracking for facebook
			AudienceNetwork.AdSettings.SetAdvertiserTrackingEnabled(true);
#endif

			// Anyway this is inited.
			isInited = true;
		}
	}

	bool isAdmbAdLoading = false;
	private void RequestAndLoadRewardedAd(Action onWaitThenLoaded, Action onWaitThenFailed)
	{
		Debug.Log("InterstitialRewardedManager.RequestAndLoadRewardedAd");

		if(Application.platform == RuntimePlatform.OSXEditor)
			return;

		if(isAdmbAdLoading)
			return;

		isAdmbAdLoading = true;

		// Initialize an InterstitialAd.

		string adUnitId = "unexpected_platform";
		if(Application.platform == RuntimePlatform.Android)
		{
			adUnitId = ServerNetworkManager.Instance.Setting.AndroidAdmobMobirixRewardedKey;
		}
		else if(Application.platform == RuntimePlatform.IPhonePlayer)
		{
			adUnitId = ServerNetworkManager.Instance.Setting.iOSAdmobRewardedKey;
		}

		rewardedAd = new RewardedAd(adUnitId);

		// Called when an ad request has successfully loaded.
		if(onWaitThenLoaded != null)
			this.rewardedAd.OnAdLoaded += (sender, args) => {
				Debug.Log("InterstitialRewardedManager.RequestAndLoadRewardedAd: Load successful and on success function");
				isAdmbAdLoading = false;
				onWaitThenLoaded?.Invoke();
			};
		else
	        this.rewardedAd.OnAdLoaded += OnLoaded;
        // Called when an ad request failed to load.
		if(onWaitThenFailed != null)
			this.rewardedAd.OnAdFailedToLoad += (sender, args) => {
				Debug.Log("InterstitialRewardedManager.RequestAndLoadRewardedAd: Load failed and on failure function");
				googleAdsFailCounts ++;
				isAdmbAdLoading = false;
				onWaitThenFailed?.Invoke();
			};
		else
	        this.rewardedAd.OnAdFailedToLoad += OnFailedToLoaded;

        // Called when an ad is shown.
        this.rewardedAd.OnAdOpening += OnOpening;
        // Called when an ad request failed to show.
        this.rewardedAd.OnAdFailedToShow += OnFailed;
        // Called when the user should be rewarded for interacting with the ad.
        this.rewardedAd.OnUserEarnedReward += OnRewarded;
        // Called when the ad is closed.
        this.rewardedAd.OnAdClosed += OnClosed;

		// For collecting LTV
		this.rewardedAd.OnPaidEvent += OnPaidEvent;

		// Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();

        // Load the rewarded ad with the request.
        this.rewardedAd.LoadAd(request);

		Debug.Log("InterstitialRewardedManager.RequestAndLoadRewardedAd: Load a new advertisement requested");
	}

	public void ShowAds(string eventType, Action<ShowResult> onComplete)
	{
		if(onAdvertisementComplete != null)
		{
			Debug.Log("InterstitialRewardedManager: Already trying to show ads!");
			return;
		}

		// Save callback.
		onAdvertisementComplete = onComplete;

		Analytics.CustomEvent(
			"ads_rewarded",
			new Dictionary<string,object> {
					{ "type", eventType }
			}
		);

		var randomValue = UnityEngine.Random.Range(0, 100);
		if(Application.platform == RuntimePlatform.OSXEditor)
			ShowUnityAds();
		else
		{
			// 0: Google
			// 1: Unity

			Debug.Log("InterstitialRewardedManager.ShowAds: " + randomValue);

#if UNITY_EDITOR
			if(Advertisement.IsReady())
				ShowUnityAds();
	
#elif UNITY_ANDROID

			if(this.rewardedAd.IsLoaded())
				ShowAdmobAds();
			else
			{
				if(!isAdmbAdLoading)
				{
					// Request and wait until the rewarded ad is ready and then finally show admob ads
					RequestAndLoadRewardedAd(
						() => {
							ShowAdmobAds();
						},
						() => {
							if(ServerNetworkManager.Instance.Setting.UnityAdsRatio != 0 && googleAdsFailCounts > 2)
							{
								Debug.Log("InterstitialRewardedManager.ShowAds: Show unity ads instead.");
								ShowUnityAds();
							}
							else
							{
								if(onAdvertisementComplete != null)
									onAdvertisementComplete.Invoke(ShowResult.Failed);
								onAdvertisementComplete = null;
							}
						}
					);
				}
				else
				{
					Debug.Log("InterstitialRewardedManager.ShowAds: Admob ad is being loaded but not loaded yet.");
		
					if(ServerNetworkManager.Instance.Setting.UnityAdsRatio != 0 && googleAdsFailCounts > 2)
					{
						Debug.Log("InterstitialRewardedManager.ShowAds: Show unity ads instead.");
						ShowUnityAds();
					}
					else
					{
						if(onAdvertisementComplete != null)
							onAdvertisementComplete.Invoke(ShowResult.Failed);
						onAdvertisementComplete = null;
					}
				}
			}

#elif UNITY_IPHONE

			if(this.rewardedAd.IsLoaded())
				ShowAdmobAds();
			else
			{
				if(!isAdmbAdLoading)
				{
					// Request and wait until the rewarded ad is ready and then finally show admob ads
					RequestAndLoadRewardedAd(
						() => {
							ShowAdmobAds();
						},
						() => {
							if(ServerNetworkManager.Instance.Setting.UnityAdsRatio != 0 && googleAdsFailCounts > 2)
							{
								Debug.Log("InterstitialRewardedManager.ShowAds: Show unity ads instead.");
								ShowUnityAds();
							}
							else
							{
								if(onAdvertisementComplete != null)
									onAdvertisementComplete.Invoke(ShowResult.Failed);
								onAdvertisementComplete = null;
							}
						}
					);
				}
				else
				{
					Debug.Log("InterstitialRewardedManager.ShowAds: Admob ad is being loaded but not loaded yet");
					
					if(ServerNetworkManager.Instance.Setting.UnityAdsRatio != 0 && googleAdsFailCounts > 2)
					{
						Debug.Log("InterstitialRewardedManager.ShowAds: Show unity ads instead.");
						ShowUnityAds();
					}
					else
					{
						if(onAdvertisementComplete != null)
							onAdvertisementComplete.Invoke(ShowResult.Failed);
						onAdvertisementComplete = null;
					}
				}
			}
#endif
		}
	}

	private void ShowAdmobAds()
	{
		Debug.Log("InterstitialRewardedManager.ShowAdmobAds");
		SoundManager.Instance.MuteBGM();
		SoundManager.Instance.MuteEffectVolume();
		this.rewardedAd.Show();
	} 

	private void ShowUnityAds()
	{
		Debug.Log("InterstitialRewardedManager.ShowUnityAds");
		Advertisement.Show ("rewardedVideo");
	}

#region Unity Ads Callbacks

    public void OnUnityAdsDidFinish (string placementId, ShowResult showResult)
	{
		Debug.Log("InterstitialRewardedManager.OnUnityAdsDidFinish: " + showResult.ToString());

		if(onAdvertisementComplete != null)
			onAdvertisementComplete.Invoke(showResult);
		onAdvertisementComplete = null;
    }

    public void OnUnityAdsReady (string placementId)
	{    
		Debug.Log("InterstitialRewardedManager.OnUnityAdsReady: " + placementId);
    }

    public void OnUnityAdsDidError (string message)
	{
		Debug.Log("InterstitialRewardedManager.OnUnityAdsDidError: " + message);
		if(onAdvertisementComplete != null)
			onAdvertisementComplete.Invoke(ShowResult.Failed);
		onAdvertisementComplete = null;
    }

    public void OnUnityAdsDidStart (string placementId)
	{
		Debug.Log("InterstitialRewardedManager.OnUnityAdsDidStart: " + placementId);
    }

#endregion

#region Admob Callbacks

	private void OnLoaded(object sender, System.EventArgs args)
	{
		isAdmbAdLoading = false;
		Debug.Log("InterstitialRewardedManager.OnLoaded");
	}

	private void OnFailedToLoaded(object sender, System.EventArgs args)
	{
		isAdmbAdLoading = false;
		googleAdsFailCounts ++;
		Debug.Log("InterstitialRewardedManager.OnFailedToLoaded");
	}

	private void OnOpening(object sender, System.EventArgs args)
	{
		Debug.Log("InterstitialRewardedManager.OnOpening");
	}

	private void OnFailed(object sender, AdErrorEventArgs args)
	{
		Debug.Log("InterstitialRewardedManager.OnFailed: " + args.Message);
		SoundManager.Instance.SetBGMVolume(OptionManager.Instance.BGMVolume);
		SoundManager.Instance.SetEffectVolume(OptionManager.Instance.SoundVolume);
		if(onAdvertisementComplete != null)
			onAdvertisementComplete.Invoke(ShowResult.Failed);
		onAdvertisementComplete = null;
	}

	private void OnRewarded(object sender, Reward args)
	{
		Debug.Log("InterstitialRewardedManager.OnRewarded: " + args.Type);
		if(onAdvertisementComplete != null)
			onAdvertisementComplete.Invoke(ShowResult.Finished);
		onAdvertisementComplete = null;
	}

	private void OnClosed(object sender, EventArgs args)
	{
		Debug.Log("InterstitialRewardedManager.OnClosed");
		SoundManager.Instance.SetBGMVolume(OptionManager.Instance.BGMVolume);
		SoundManager.Instance.SetEffectVolume(OptionManager.Instance.SoundVolume);
		if(onAdvertisementComplete != null)
			onAdvertisementComplete.Invoke(ShowResult.Skipped);
		onAdvertisementComplete = null;

		RequestAndLoadRewardedAd(null, null);
	}

	private void OnPaidEvent(object sender, AdValueEventArgs args)
	{
		AdValue adValue = args.AdValue;
		Debug.Log("InterstitialRewardedManager.OnPaidEvent: " + adValue.Value + ", " + adValue.CurrencyCode);
		AnalyticsController.Instance.AdRewardValueLog(adValue);
	}

#endregion
}