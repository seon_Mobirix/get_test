﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Analytics;

public class AndroidPurchaseReceipt
{
	public string Store;
	public string TransactionID;
	public string Payload;
}

public class AndroidPurchasePayload
{
	public string json;
	public string signature;
}

public class PurchaseManager : MonoBehaviour, IStoreListener {

	private static PurchaseManager instance = null;
	public static PurchaseManager Instance
	{
		get{
			if(instance == null)
				instance = GameObject.FindObjectOfType<PurchaseManager>();
			return instance;
		}
	}

	private IStoreController storeController;
	private IExtensionProvider storeExtensionProvider;
	private IAppleExtensions appleExtensions;

	[SerializeField]
	private bool isInited = false;
	public bool IsInited{
		get{
			return isInited;
		}
	}

	public void Initialize(List<GemBundleModel> gemBundleList, List<NormalPackageModel> normalPackageList, List<GrowthPackageModel> growthPackageList)
	{
		if(isInited)
		{
			OnInitialized(storeController, storeExtensionProvider);
			return;
		}

		ConfigurationBuilder builder;

		if(Application.platform == RuntimePlatform.Android)
		{
			builder = ConfigurationBuilder.Instance(Google.Play.Billing.GooglePlayStoreModule.Instance());
		}
		else
		{
			var module = StandardPurchasingModule.Instance();
			module.useFakeStoreUIMode = FakeStoreUIMode.StandardUser;
			
			builder = ConfigurationBuilder.Instance(module);
		}

		foreach(var tmp in gemBundleList)
		{
			builder.AddProduct(tmp.ItemId, ProductType.Consumable);
		}

		foreach(var tmp in normalPackageList)
		{
			builder.AddProduct(tmp.Id, ProductType.Consumable);
		}

		foreach(var tmp in growthPackageList)
		{
			builder.AddProduct(tmp.Id, ProductType.Consumable);
		}

		UnityPurchasing.Initialize(this, builder);
	}

	public void Restore(Action onComplete)
	{
		Debug.Log("PurchaseManger.Restore");

		if(isInited && appleExtensions != null)
		{
			UIManager.Instance.ShowCanvas("Loading Canvas");

			appleExtensions.RestoreTransactions (
				result => {
					if (result) {
						UIManager.Instance.HideCanvas("Loading Canvas");
						if(onComplete != null)
							onComplete.Invoke();
					} else {
						UIManager.Instance.HideCanvas("Loading Canvas");
						if(onComplete != null)
							onComplete.Invoke();
					}
				}
			);
		}
	}

	public void Purchase(string id)
	{		
		if(ServerNetworkManager.Instance.Setting.IsBeta)
		{
			UIManager.Instance.ShowAlertLocalized("message_purchase_fail_beta", null, null);
			return;
		}
		
		if(isInited)
		{
			UIManager.Instance.ShowCanvas("Loading Canvas");
			
			Product product = storeController.products.WithID(id);
			if (product != null && product.availableToPurchase)
			{
				Debug.Log(string.Format("PurchaseManager.Purchase: '{0}'", product.definition.id));
				storeController.InitiatePurchase(product);
			}
			else
			{
				Debug.LogWarning("PurchaseManager.Purchase: FAIL. Not purchasing product, either is not found or is not available for purchase");
			}
		}
	}
   
	public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
	{
		Debug.Log("PurchaseManager.OnInitialized");
		isInited = true;
		storeController = controller;
		storeExtensionProvider = extensions;

		if(Application.platform == RuntimePlatform.IPhonePlayer)
		{
			appleExtensions = storeExtensionProvider.GetExtension<IAppleExtensions> ();
			appleExtensions.RegisterPurchaseDeferredListener(OnDeferred);
		}

		foreach(var tmp in controller.products.all)
		{
			if(ServerNetworkManager.Instance.Catalog.NormalPackageList.Exists(n => n.Id == tmp.definition.id))
			{
				var tmpItem = ServerNetworkManager.Instance.Catalog.NormalPackageList.First(n => n.Id == tmp.definition.id);

				#if UNITY_EDITOR
					// Nothing to do
				#else
					tmpItem.RealMoneyPrice = tmp.metadata.localizedPriceString;
				#endif
			}
			else if(ServerNetworkManager.Instance.Catalog.GrowthPackageList.Exists(n => n.Id == tmp.definition.id))
			{
				var tmpItem = ServerNetworkManager.Instance.Catalog.GrowthPackageList.First(n => n.Id == tmp.definition.id);
				
				#if UNITY_EDITOR
					// Nothing to do
				#else
					tmpItem.RealMoneyPrice = tmp.metadata.localizedPriceString;
				#endif
			}
			else if(ServerNetworkManager.Instance.Catalog.GemBundleList.Exists(n => n.ItemId == tmp.definition.id))
			{
				var tmpItem = ServerNetworkManager.Instance.Catalog.GemBundleList.First(n => n.ItemId == tmp.definition.id);
				
				#if UNITY_EDITOR
					// Nothing to do
				#else
					tmpItem.RealMoneyPrice = tmp.metadata.localizedPriceString;
				#endif
			}
		}
	}

	public void OnInitializeFailed(InitializationFailureReason error)
	{
		UIManager.Instance.ShowAlert("Error. In app purchase initialization failed!", null, null);
		Debug.Log("PurchaseManager.OnInitializeFailed: " + error);
	}

	private void OnDeferred(Product item)
	{
		Debug.Log("Purchase deferred: " + item.definition.id);
	}

	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
	{ 
		if(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name != "MainGame")
		{
			Debug.Log("PurchaseManager.PurchaseProcessingResult: Not MainGame");
			return PurchaseProcessingResult.Pending;
		}

		Debug.Log("PurchaseManager.ProcessPurchase: id: " + args.purchasedProduct.definition.id);
		Debug.Log("receipt => " + args.purchasedProduct.receipt);

		if(Application.platform == RuntimePlatform.IPhonePlayer)
		{
			// Saftey code.
			if(args.purchasedProduct.metadata.isoCurrencyCode == null)
			{
				storeController.ConfirmPendingPurchase(args.purchasedProduct);
				return PurchaseProcessingResult.Pending;
			}

			var wrapper = JsonUtility.FromJson<AndroidPurchaseReceipt>(args.purchasedProduct.receipt);
			var payload = wrapper.Payload;
			Debug.Log(payload);

			ServerNetworkManager.Instance.ValidateiOSPurchase(
				payload,
				args.purchasedProduct.metadata.isoCurrencyCode, 
				(int)args.purchasedProduct.metadata.localizedPrice,
				() => {
					UIManager.Instance.HideCanvas("Loading Canvas");
					storeController.ConfirmPendingPurchase(args.purchasedProduct);

					Analytics.CustomEvent(
						"purchase",
						new Dictionary<string,object> {
							{ "type", "cash" },
							{ "id", args.purchasedProduct.definition.id }
						}
					);

					OutgameController.Instance.OnPurchseShopItem(args.purchasedProduct.definition.id);
				},
				() => {
					UIManager.Instance.HideCanvas("Loading Canvas");
					storeController.ConfirmPendingPurchase(args.purchasedProduct);
				}
			);
		}
		else if(Application.platform == RuntimePlatform.Android)
		{
			var wrapper = JsonUtility.FromJson<AndroidPurchaseReceipt>(args.purchasedProduct.receipt);
			var payload = wrapper.Payload;

			var details = JsonUtility.FromJson<AndroidPurchasePayload>(payload);
			var receiptJson = details.json;
			var signature = details.signature;

			ServerNetworkManager.Instance.ValidateAndroidPurchase(
				receiptJson,
				signature,
				() => {
					UIManager.Instance.HideCanvas("Loading Canvas");
					storeController.ConfirmPendingPurchase(args.purchasedProduct);
					
					Analytics.CustomEvent(
						"purchase",
						new Dictionary<string,object> {
							{ "type", "cash" },
							{ "id", args.purchasedProduct.definition.id }
						}
					);
					
					FacebookManager.Instance.LogPurchaseForAndroid(args.purchasedProduct.metadata.localizedPrice, args.purchasedProduct.metadata.isoCurrencyCode);

					OutgameController.Instance.OnPurchseShopItem(args.purchasedProduct.definition.id);
				},
				() => {
					UIManager.Instance.HideCanvas("Loading Canvas");
					storeController.ConfirmPendingPurchase(args.purchasedProduct);
				}
			);
		}

		return PurchaseProcessingResult.Pending;
	}

	public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
	{
		if(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name != "MainGame")
		{
			Debug.Log("PurchaseManager.PurchaseProcessingResult: Not MainGame");
			return;
		}

		UIManager.Instance.HideCanvas("Loading Canvas");
		UIManager.Instance.ShowAlert(
			string.Format(
				LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_purchase_fail"),
				product.definition.storeSpecificId, 
				failureReason
			), 
			null, 
			null
		);
  	}
}