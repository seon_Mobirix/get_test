﻿using UnityEngine;

using PlayFab;

using CodeStage.AntiCheat.ObscuredTypes;

using System.Collections.Generic;

public class InterSceneManager : MonoBehaviour {

	private static InterSceneManager instance = null;
	public static InterSceneManager Instance
	{
		get{
			if(instance == null)
				instance = GameObject.FindObjectOfType<InterSceneManager>();
			return instance;
		}
	}

#region More Game
    public bool IsMoreGameShown = false;
#endregion

#region Arena
    public string OpponentName = "";
    public UserModel OpponentUserData = null;
    public InventoryModel OpponentInventoryData = null;

    public bool DidArenaBattle = false;
    public bool DidArenaWin = false;
#endregion

#region Raid

    public bool DidRaidBattle = false;
    public bool DidRaidWin = false;
    public bool DidRaidDodge = false;
    public bool DidRaidDodgeBecauseOfHost = false;
    public int MyRaidRank = 0;
    public int TotalRaidPlayMemberCount = 0;

    public int RaidSkillUseCount = 0;

    public bool DidGuildRaidBattle = false;
    public int GuildRaidScore = 0;
    public List<string> GuildRaidParticipants = new List<string>();
    public List<int> GuildRaidParticipantScores = new List<int>();

#endregion

#region Tower

    public bool DidTowerBattle = false;
    public bool DidTowerWin = false;

#endregion

#region Rank Color

    public Color To1RankColor = new Color();
    public Color To2RankColor = new Color();
    public Color To3RankColor = new Color();
    public Color To10RankColor = new Color();
    public Color To100RankColor = new Color();
    public Color To1000RankColor = new Color();
    public Color To5000RankColor = new Color();
    public Color To10000RankColor = new Color();
    public Color OtherRankColor = new Color();

#endregion

#region 

    public ObscuredFloat BerserkPoint = 0f;

    public int GoodSummonCount = 0;

#endregion

    public void SetDataForArena(string name, string userData, string inventoryData, int bonusStat)
    {
        this.OpponentName = name;
        this.OpponentUserData = new UserModel();
        this.OpponentInventoryData = new InventoryModel();

        this.DidArenaBattle = true;
        this.DidArenaWin = false;

        var serverUserModel = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<ServerUserModel>(userData);
        var serverInventoryModel = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<ServerInventoryModel>(inventoryData);
        
        this.OpponentInventoryData.BonusStat = bonusStat;

        ServerNetworkManager.ApplyUserDataFromServer("none", this.OpponentUserData, serverUserModel);
        ServerNetworkManager.ApplyInventoryDataFromServer("none", this.OpponentInventoryData, serverInventoryModel);
    }

    public void SetDataForRaid(bool isFullRaid)
    {
        this.DidRaidBattle = true;
        this.DidGuildRaidBattle = false;
        this.DidRaidWin = false;
        this.MyRaidRank = 0;
        this.TotalRaidPlayMemberCount = 0;
        this.GuildRaidScore = 0;
        this.RaidSkillUseCount = 0;
    }

    public void SetDataForGuildRaid(bool isFullRaid)
    {
        this.DidRaidBattle = false;
        this.DidGuildRaidBattle = true;
        this.DidRaidWin = false;
        this.MyRaidRank = 0;
        this.TotalRaidPlayMemberCount = 0;
        this.GuildRaidScore = 0;
        this.RaidSkillUseCount = 0;
    }

    public void SetDataForTower(string name, string userData, string inventoryData, int bonusStat)
    {
        this.OpponentName = name;
        this.OpponentUserData = new UserModel();
        this.OpponentInventoryData = new InventoryModel();

        this.DidTowerBattle = true;
        this.DidTowerWin = false;

        var serverUserModel = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<ServerUserModel>(userData);
        var serverInventoryModel = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<ServerInventoryModel>(inventoryData);
        
        this.OpponentInventoryData.BonusStat = bonusStat;

        ServerNetworkManager.ApplyUserDataFromServer("none", this.OpponentUserData, serverUserModel);
        ServerNetworkManager.ApplyInventoryDataFromServer("none", this.OpponentInventoryData, serverInventoryModel);
    }


    public string GetColorForRank(int rank)
    {
        if(rank == 0)
            return "#" + ColorUtility.ToHtmlStringRGB(OtherRankColor);
        else if(rank <= 1)
            return "#" + ColorUtility.ToHtmlStringRGB(To1RankColor);
        else if(rank <= 2)
            return "#" + ColorUtility.ToHtmlStringRGB(To2RankColor);
        else if(rank <= 3)
            return "#" + ColorUtility.ToHtmlStringRGB(To3RankColor);
        else if(rank <= 10)
            return "#" + ColorUtility.ToHtmlStringRGB(To10RankColor);
        else if(rank <= 100)
            return "#" + ColorUtility.ToHtmlStringRGB(To100RankColor);
        else if(rank <= 1000)
            return "#" + ColorUtility.ToHtmlStringRGB(To1000RankColor);
        else if(rank <= 5000)
            return "#" + ColorUtility.ToHtmlStringRGB(To5000RankColor);
        else if(rank <= 10000)
            return "#" + ColorUtility.ToHtmlStringRGB(To10000RankColor);
        else
            return "#" + ColorUtility.ToHtmlStringRGB(OtherRankColor);
    }

}