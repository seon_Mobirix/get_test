﻿using System;
using System.Globalization;
using System.Numerics;
using UnityEngine;
using CodeStage.AntiCheat.ObscuredTypes;

public enum RelicType{
    GOLD_INCOME,
    BASE_ATTACK,
    CRITICAL_ATTACK,
    SUPER_ATTACK,
    BERSERK_POINT,
    BERSERK_TIME,
    BERSERK_GOLD,
    RETURN_GOLD,
    FARMING,
    MONSTER_HP,
    BOSS_HP
}

[Serializable]
public class RelicModel : ModelBase
{
    public string Id;
    public ObscuredInt Count;
    public ObscuredInt Level;

    public bool IsNew;
    
    public RelicType RelicType;
    private RelicInfoModel infoModel = null;

    private int IntValueWithLevel{
        get{
            if(infoModel == null)
            {
                infoModel = BalanceInfoManager.Instance.RelicInfoList.Find(n => n.Id == Id);
            }
            return int.Parse(infoModel.DefaultEffect) + int.Parse(infoModel.EffectPower) * Level;
        }
    }

    private float FloatValueWithLevel{
        get{
            if(infoModel == null)
            {
                infoModel = BalanceInfoManager.Instance.RelicInfoList.Find(n => n.Id == Id);
            }
            return float.Parse(infoModel.DefaultEffect, NumberStyles.Any, CultureInfo.InvariantCulture) + float.Parse(infoModel.EffectPower, NumberStyles.Any, CultureInfo.InvariantCulture) * Level;
        }
    }

    public int MaxLevel{
        get{
            if(infoModel == null)
            {
                infoModel = BalanceInfoManager.Instance.RelicInfoList.Find(n => n.Id == Id);
            }
            return int.Parse(infoModel.MaxLevel);
        }
    }

    public float LevelUpSuccessChancePercent{
        get{
            if(infoModel == null)
            {
                infoModel = BalanceInfoManager.Instance.RelicInfoList.Find(n => n.Id == Id);
            }
            return int.Parse(infoModel.MaxLevel);
        }
    }

    public BigInteger GoldIncomeMultiplier{
        get{
            if(RelicType == RelicType.GOLD_INCOME)
                return this.IntValueWithLevel;
            else
                return 0;
        }
    }

    public BigInteger BaseAttackMultiplier{
        get{
            if(RelicType == RelicType.BASE_ATTACK)
                return this.IntValueWithLevel;
            else
                return 0;
        }
    }

    public BigInteger CriticalAttackMultiplier{
        get{
            if(RelicType == RelicType.CRITICAL_ATTACK)
                return this.IntValueWithLevel;
            else
                return 0;
        }
    }

    public BigInteger SuperAttackMultiplier{
        get{
            if(RelicType == RelicType.SUPER_ATTACK)
                return this.IntValueWithLevel;
            else
                return 0;
        }
    }

    public int BerserkPointReduce{
        get{
            if(RelicType == RelicType.BERSERK_POINT)
                return this.IntValueWithLevel;
            else
                return 0;
        }
    }

    public float BerserkTimeIncrease{
        get{
            if(RelicType == RelicType.BERSERK_TIME)
                return this.FloatValueWithLevel;
            else
                return 0;
        }
    }

    public BigInteger BerserkGoldIncreasePercent{
        get{
            if(RelicType == RelicType.BERSERK_GOLD)
                return this.IntValueWithLevel;
            else
                return 0;
        }
    }

    public BigInteger ReturnGoldIncreasePercent{
        get{
            if(RelicType == RelicType.RETURN_GOLD)
                return this.IntValueWithLevel;
            else
                return 0;
        }
    }

    public float FarmingIncreasePercent{
        get{
            if(RelicType == RelicType.FARMING)
                return this.FloatValueWithLevel;
            else
                return 0;
        }
    }

    public int MonsterHPReducePercent{
        get{
            if(RelicType == RelicType.MONSTER_HP)
                return this.IntValueWithLevel;
            else
                return 0;
        }
    }

    public int BossHPReducePercent{
        get{
            if(RelicType == RelicType.BOSS_HP)
                return this.IntValueWithLevel;
            else
                return 0;
        }
    }

    public int LevelUpChancePercent{
        get{
            return Mathf.Max(100 - (Level - 1), 20);
        }
    }

    public bool IsMaxLevel{ 
        get{
            if(infoModel == null)
                infoModel = BalanceInfoManager.Instance.RelicInfoList.Find(n => n.Id == Id);

            int maxLevel= int.Parse(infoModel.MaxLevel);
            
            if(maxLevel== -1)
                return false;
            else
            {
                if(Level < maxLevel)
                    return false;
                else
                    return true;
            }
        }
    }
}