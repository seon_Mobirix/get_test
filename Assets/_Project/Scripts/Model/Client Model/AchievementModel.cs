﻿using System;
using CodeStage.AntiCheat.ObscuredTypes;

[Serializable]
public class AchievementModel : ModelBase
{
    public string Id;
    public ObscuredInt Count;
    public ObscuredInt Goal;
    public ObscuredInt Level;
    public bool IsMaxLevel;
}