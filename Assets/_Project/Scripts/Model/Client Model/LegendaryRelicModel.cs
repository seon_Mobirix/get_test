﻿using System;
using System.Globalization;
using CodeStage.AntiCheat.ObscuredTypes;

public enum LegendaryRelicType{
    UPGRADE_DISCOUNT,
    ULTIMATE_ATTACK,
    MOVEMENT_SPEED,
    SKILL1_ATTACK,
    SKILL2_ATTACK,
    SKILL3_ATTACK,
    BOSS_TIME
}

[Serializable]
public class LegendaryRelicModel : ModelBase
{
    public string Id;
    public ObscuredInt Count;
    public ObscuredInt Level;

    public bool IsNew;
    
    public LegendaryRelicType LegendaryRelicType;
    private RelicInfoModel infoModel = null;

    private int IntValueWithLevel{
        get{
            if(infoModel == null)
            {
                infoModel = BalanceInfoManager.Instance.LegendaryRelicInfoList.Find(n => n.Id == Id);
            }
            return int.Parse(infoModel.DefaultEffect) + int.Parse(infoModel.EffectPower) * Level;
        }
    }

    private float FloatValueWithLevel{
        get{
            if(infoModel == null)
            {
                infoModel = BalanceInfoManager.Instance.LegendaryRelicInfoList.Find(n => n.Id == Id);
            }
            return float.Parse(infoModel.DefaultEffect, NumberStyles.Any, CultureInfo.InvariantCulture) + float.Parse(infoModel.EffectPower, NumberStyles.Any, CultureInfo.InvariantCulture) * Level;
        }
    }

    public int MaxLevel{
        get{
            if(infoModel == null)
            {
                infoModel = BalanceInfoManager.Instance.LegendaryRelicInfoList.Find(n => n.Id == Id);
            }
            return int.Parse(infoModel.MaxLevel, NumberStyles.Any, CultureInfo.InvariantCulture);
        }
    }

    public float UpgradeCostReducePercent{
        get{
            if(LegendaryRelicType == LegendaryRelicType.UPGRADE_DISCOUNT)
                return this.IntValueWithLevel;
            else
                return 0;
        }
    }

    public float UltimateAttackMultiplier{
        get{
            if(LegendaryRelicType == LegendaryRelicType.ULTIMATE_ATTACK)
                return this.FloatValueWithLevel;
            else
                return 0;
        }
    }

    public float MovementSpeedIncreasePercent{
        get{
            if(LegendaryRelicType == LegendaryRelicType.MOVEMENT_SPEED)
                return this.IntValueWithLevel;
            else
                return 0;
        }
    }

    public float SKill1AttackMultiplier{
        get{
            if(LegendaryRelicType == LegendaryRelicType.SKILL1_ATTACK)
                return this.FloatValueWithLevel;
            else
                return 0;
        }
    }

    public float SKill2AttackMultiplier{
        get{
            if(LegendaryRelicType == LegendaryRelicType.SKILL2_ATTACK)
                return this.FloatValueWithLevel;
            else
                return 0;
        }
    }

    public float SKill3AttackMultiplier{
        get{
            if(LegendaryRelicType == LegendaryRelicType.SKILL3_ATTACK)
                return this.FloatValueWithLevel;
            else
                return 0;
        }
    }

    public float BossTimeIncreasePercent{
        get{
            if(LegendaryRelicType == LegendaryRelicType.BOSS_TIME)
                return this.FloatValueWithLevel;
            else
                return 0;
        }
    }

    public int LevelUpChancePercent{
        get{
            return 100;
        }
    }

    public bool IsMaxLevel{ 
        get{
            if(infoModel == null)
                infoModel = BalanceInfoManager.Instance.LegendaryRelicInfoList.Find(n => n.Id == Id);

            int maxLevel= int.Parse(infoModel.MaxLevel);
            
            if(maxLevel== -1)
                return false;
            else
            {
                if(Level < maxLevel)
                    return false;
                else
                    return true;
            }
        }
    }
}
