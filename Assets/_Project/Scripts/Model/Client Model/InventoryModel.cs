﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Numerics;
using CodeStage.AntiCheat.ObscuredTypes;

[Serializable]
public class InventoryModel : ModelBase
{
    public ObscuredInt Gem;
	public ObscuredInt Feather;
	public ObscuredInt GuildToken;
	public ObscuredInt VipPoint;
	public ObscuredInt BonusStat = 0;
	public ObscuredInt DailyRewardTicket;
    public BigInteger Gold
	{
		get;
		set;
	}
    public BigInteger Heart;
	public ObscuredInt TicketRaid;

    public List<ClassModel> ClassList = new List<ClassModel>();
    public ClassModel SelectedClass = null;

    public List<WeaponModel> WeaponList = new List<WeaponModel>();
    public WeaponModel SelectedWeapon = null;

	public List<MercenaryModel> MercenaryList = new List<MercenaryModel>();
    private MercenaryModel selectedMercenary = null;
    public MercenaryModel SelectedMercenary{
        get{
            return selectedMercenary;
        }
        set{
            selectedMercenary = value;
        }
    }

    public List<RelicModel> RelicList = new List<RelicModel>();
	public List<RelicModel> ExtendedRelicList
	{
		get{
			var relicList = new List<RelicModel>();

			foreach(var tmp in RelicList)
			{
				if(tmp.Id != "relic_02" && tmp.Id != "relic_03" && tmp.Id != "relic_04")
					relicList.Add(tmp);	
			}

			// base attack relic (relic_02)
			var baseAttackRelic = RelicList.FirstOrDefault(n => n.Id == "relic_02");

			if(baseAttackRelic == null)
			{
				if(this.BonusStat > 0)
				{
					var tmpRelicModel = new RelicModel();
					tmpRelicModel.Id = "relic_02";
					tmpRelicModel.RelicType = RelicType.BASE_ATTACK;
					tmpRelicModel.Count = 1;
					tmpRelicModel.Level = this.BonusStat;
					tmpRelicModel.IsNew = true;

					relicList.Add(tmpRelicModel);
				}
			}
			else
			{
				var tmpRelicModel = new RelicModel();
				tmpRelicModel.Id = "relic_02";
				tmpRelicModel.RelicType = RelicType.BASE_ATTACK;
				tmpRelicModel.Count = baseAttackRelic.Count;
				tmpRelicModel.Level = baseAttackRelic.Level + this.BonusStat;
				tmpRelicModel.IsNew = false;

				relicList.Add(tmpRelicModel);
			}
			
			// critical attack relic (relic_03)
			var criticalAttackRelic = RelicList.FirstOrDefault(n => n.Id == "relic_03");

			if(criticalAttackRelic == null)
			{
				if(this.BonusStat > 0)
				{
					var tmpRelicModel = new RelicModel();
					tmpRelicModel.Id = "relic_03";
					tmpRelicModel.RelicType = RelicType.CRITICAL_ATTACK;
					tmpRelicModel.Count = 1;
					tmpRelicModel.Level = this.BonusStat;
					tmpRelicModel.IsNew = true;

					relicList.Add(tmpRelicModel);
				}
			}
			else
			{
				var tmpRelicModel = new RelicModel();
				tmpRelicModel.Id = "relic_03";
				tmpRelicModel.RelicType = RelicType.CRITICAL_ATTACK;
				tmpRelicModel.Count = criticalAttackRelic.Count;
				tmpRelicModel.Level = criticalAttackRelic.Level + this.BonusStat;
				tmpRelicModel.IsNew = false;

				relicList.Add(tmpRelicModel);
			}

			// super attack relic (relic_04)
			var superAttackRelic = RelicList.FirstOrDefault(n => n.Id == "relic_04");

			if(superAttackRelic == null)
			{
				if(this.BonusStat > 0)
				{
					var tmpRelicModel = new RelicModel();
					tmpRelicModel.Id = "relic_04";
					tmpRelicModel.RelicType = RelicType.SUPER_ATTACK;
					tmpRelicModel.Count = 1;
					tmpRelicModel.Level = this.BonusStat;
					tmpRelicModel.IsNew = true;

					relicList.Add(tmpRelicModel);
				}
			}
			else
			{
				var tmpRelicModel = new RelicModel();
				tmpRelicModel.Id = "relic_04";
				tmpRelicModel.RelicType = RelicType.SUPER_ATTACK;
				tmpRelicModel.Count = superAttackRelic.Count;
				tmpRelicModel.Level = superAttackRelic.Level + this.BonusStat;
				tmpRelicModel.IsNew = false;

				relicList.Add(tmpRelicModel);
			}

			return relicList;
		}
	}
	
    public List<PetModel> PetList = new List<PetModel>();
    public List<PetModel> SelectedPetList = new List<PetModel>();

	public List<LegendaryRelicModel> LegendaryRelicList = new List<LegendaryRelicModel>();
	
	private PetModel mainPet = null;
	public PetModel MainPet{
		get{
			return mainPet;
		}
		set{
			mainPet = value;
		}
	}

	public List<TrophyModel> TrophyList = new List<TrophyModel>();
    public List<TrophyModel> SelectedTrophyList = new List<TrophyModel>();

	public ObscuredInt WingIndex = -1;

	public ClassModel BestClass{
		get{
			return ClassList.OrderByDescending(n => n.Id).FirstOrDefault();
		}
	}

	public int PetCountByBestClass{
		get{
			if(int.Parse(this.BestClass.Id.Substring(6)) >= 25)
				return 8;
			else if(int.Parse(this.BestClass.Id.Substring(6)) >= 19)
				return 7;
			else if(int.Parse(this.BestClass.Id.Substring(6)) >= 16)
				return 6;
			else if(int.Parse(this.BestClass.Id.Substring(6)) >= 13)
				return 5;
			else if(int.Parse(this.BestClass.Id.Substring(6)) >= 10)
				return 4;
			else
				return 3;
		}
	}

	public int TrophyCountByBestClass{
		get{
			if(int.Parse(this.BestClass.Id.Substring(6)) >= 25)
				return 8;
			else if(int.Parse(this.BestClass.Id.Substring(6)) >= 19)
				return 7;
			else if(int.Parse(this.BestClass.Id.Substring(6)) >= 16)
				return 6;
			else if(int.Parse(this.BestClass.Id.Substring(6)) >= 13)
				return 5;
			else if(int.Parse(this.BestClass.Id.Substring(6)) >= 10)
				return 4;
			else
				return 3;
		}
	}

	public WeaponModel BestWeapon{
		get{
			return WeaponList.OrderByDescending(n => n.Id).FirstOrDefault();
		}
	}

	public PetModel BestPet{
		get{
			return PetList.OrderByDescending(n => n.Id).FirstOrDefault();
		}
	}

	public BigInteger TotalHeartCost{
		get{
			BigInteger toReturn = 0;

			foreach(var tmp in PetList)
			{
				toReturn += tmp.HeartLevelupCost;
			}

			foreach(var tmp in TrophyList)
			{
				toReturn += tmp.HeartLevelupCost;
			}

			return toReturn;
		}
	}

    public bool IsEquipedWeapon(string weaponId)
	{
		if(SelectedWeapon != null && SelectedWeapon.Id == weaponId)
			return true;
		else
			return false;
	}

	public bool IsEquipedClass(string classId)
	{
		if(SelectedClass != null && SelectedClass.Id == classId)
			return true;
		else
			return false;
	}

	public bool IsEquipedMercenary(string mercenaryId)
	{
		if(SelectedMercenary != null && SelectedMercenary.Id == mercenaryId)
			return true;
		else
			return false;
	}

	public bool IsEquipPet(string petId)
	{
		if(SelectedPetList != null && SelectedPetList.Exists(n => n.Id == petId))
			return true;
		else
			return false;
	}

	public bool IsEquipTrophy(string trophyId)
	{
		if(SelectedTrophyList != null && SelectedTrophyList.Exists(n => n.Id == trophyId))
			return true;
		else
			return false;
	}
}