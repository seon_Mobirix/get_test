﻿using System;
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;

[Serializable]

public class UserModel : ModelBase
{
    public string ServerId = "";
    
    public string PlayFabId;
    public string NickName;

    public double TimeStamp = -1d;

    public UpgradeModel Upgrade = new UpgradeModel();

    public ObscuredInt AutoEnabledTier = 0;
    public ObscuredInt AttackBuffTier = 0;
    public ObscuredInt SpeedBuffTier = 0;
    public ObscuredInt GoldBuffTier = 0;
   
    public ObscuredInt StageProgress = 1;
    public ObscuredInt SelectedStage = 1;

    public ObscuredInt KillCountForBestStage = 0;

    public ObscuredInt VIPLevel = 1;
    public ObscuredInt VIPExp = 0;
    public ObscuredInt WeaponGachaLevel = 1;
    public ObscuredInt WeaponGachaExp = 0;
    public ObscuredInt ClassGachaLevel = 1;
    public ObscuredInt ClassGachaExp = 0;
    public ObscuredInt MercenaryGachaLevel = 1;
    public ObscuredInt MercenaryGachaExp = 0;
    public ObscuredInt PetGachaLevel = 1;
    public ObscuredInt PetGachaExp = 0;

    public ObscuredInt TotalWingAttackBonus = 0;
	public ObscuredInt TotalWingGoldBonus = 0;
    
    public List<SkillModel> SkillInfo = new List<SkillModel>();

    // Achievement Info
    public List<AchievementModel> AchievementInfo = new List<AchievementModel>();

    // Firebase Analytics Log
    public int LastSummonLogCount = 0;
    public int LastFreeGemRewardLogCount = 0;
    public int BuffAndRestRewardCount = 0;

    // For fixing the UnsyncedGem issue
    public int UnsyncedGemChangeCount = -1;
}
