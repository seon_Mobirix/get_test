﻿using System;
using System.Numerics;
using System.Globalization;
using CodeStage.AntiCheat.ObscuredTypes;

[Serializable]
public class MercenaryModel : ModelBase
{
    public string Id;
    public ObscuredInt Count;
    public ObscuredInt Level;

    public bool IsNew;

    private MercenaryInfoModel infoModel = null;

    public bool IsAvail{
        get{
            if(Count > 0)
                return true;
            else
                return false;
        }
    }

    public float BaseAttackMultiplier{
        get{
            if(infoModel == null)
            {
                infoModel = BalanceInfoManager.Instance.MercenaryInfoList.Find(n => n.Id == Id);
            }
            var originalValue = float.Parse(infoModel.AttackPower, NumberStyles.Any, CultureInfo.InvariantCulture);
            return originalValue + originalValue / 100f * (Level - 1);
        }
    }

    public float MercenaryAttackMultiplier{
        get{
            if(infoModel == null)
            {
                infoModel = BalanceInfoManager.Instance.MercenaryInfoList.Find(n => n.Id == Id);
            }
            
            return float.Parse(infoModel.MercenaryAttackPower, NumberStyles.Any, CultureInfo.InvariantCulture);
        }
    }

    public BigInteger LevelupCost{
        get{
            if(infoModel == null)
                infoModel = BalanceInfoManager.Instance.MercenaryInfoList.Find(n => n.Id == Id);
            
            var originalValue = BigInteger.Parse(infoModel.LevelupCost, NumberStyles.Any, CultureInfo.InvariantCulture);
            return originalValue * (BigInteger)Math.Min(double.MaxValue, Math.Pow(1.5d, Level));
        }
    }
    
    public int LevelUpChancePercent{
        get{
            return 100;
        }
    }
}