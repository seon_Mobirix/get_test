﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using System.Globalization;
using UnityEngine;
using CodeStage.AntiCheat.ObscuredTypes;

[Serializable]
public class PetModel : ModelBase
{
    public string Id;
    public ObscuredInt Count;
    public ObscuredInt Level;

    public bool IsNew;

    public ObscuredInt GoldLevelUpCount;
    public ObscuredInt HeartLevelUpCount;
    public ObscuredInt PetLevelUpCount;

    private PetInfoModel infoModel = null;

    private PetInfoModel firstPetInfoModel = null;

    public bool IsAvail{
        get{
            if(Count > 0)
                return true;
            else
                return false;
        }
    }

    public float EffectPower{
        get{
            if(infoModel == null)
            {
                infoModel = BalanceInfoManager.Instance.PetInfoList.Find(n => n.Id == Id);
            }
            var originalValue = float.Parse(infoModel.EffectPower, NumberStyles.Any, CultureInfo.InvariantCulture);
            return originalValue + originalValue / 100 * (Level - 1);
        }
    }

    public BigInteger GoldLevelupCost{
        get{
            if(infoModel == null)
            {
                infoModel = BalanceInfoManager.Instance.PetInfoList.Find(n => n.Id == Id);
            }
            var originalValue = BigInteger.Parse(infoModel.GoldLevelupCost, NumberStyles.Any, CultureInfo.InvariantCulture);
            return originalValue + originalValue * (BigInteger)Math.Min(double.MaxValue, Math.Pow(1.75d, (double)GoldLevelUpCount));
        }
    }

    public BigInteger HeartLevelupCost{
        get{
            if(infoModel == null)
            {
                infoModel = BalanceInfoManager.Instance.PetInfoList.Find(n => n.Id == Id);
            }
            var originalValue = BigInteger.Parse(infoModel.HeartLevelupCost, NumberStyles.Any, CultureInfo.InvariantCulture);
            return originalValue * (BigInteger)Math.Min(double.MaxValue, Math.Pow(1.5d, HeartLevelUpCount));
        }
    }

    public int PetLevelupCost{
        get{
            return 2 * (PetLevelUpCount + 1);
        }
    }

    public int PetSellReward{
        get{ 
            if(infoModel == null)
            {
                infoModel = BalanceInfoManager.Instance.PetInfoList.Find(n => n.Id == Id);
            }
            var originalValue = BigInteger.Parse(infoModel.HeartLevelupCost, NumberStyles.Any, CultureInfo.InvariantCulture);
            return (int)originalValue;
        }
    }
}