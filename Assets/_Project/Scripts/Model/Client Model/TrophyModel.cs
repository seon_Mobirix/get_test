using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using System.Globalization;
using UnityEngine;
using CodeStage.AntiCheat.ObscuredTypes;

[Serializable]
public class TrophyModel : ModelBase
{
    public string Id;
    public ObscuredInt Count;
    public ObscuredInt Level;

    public ObscuredInt HeartLevelUpCount;
    public ObscuredInt TrophyLevelUpCount;

    private TrophyInfoModel infoModel = null;

    private TrophyInfoModel firstTrophyInfoModel = null;

    public bool IsAvail{
        get{
            if(Count > 0)
                return true;
            else
                return false;
        }
    }

    public float EffectPowerWithMultiply{
        get{
            if(infoModel == null)
            {
                infoModel = BalanceInfoManager.Instance.TrophyInfoList.Find(n => n.Id == Id);
            }
            var originalValue = float.Parse(infoModel.EffectPower, NumberStyles.Any, CultureInfo.InvariantCulture);
            return originalValue + originalValue / 10 * (Level - 1);
        }
    }

    public float EffectPowerWithAddition{
        get{
            if(infoModel == null)
            {
                infoModel = BalanceInfoManager.Instance.TrophyInfoList.Find(n => n.Id == Id);
            }
            var originalValue = float.Parse(infoModel.EffectPower, NumberStyles.Any, CultureInfo.InvariantCulture);
            return originalValue + (Level - 1);
        }
    }

    public BigInteger HeartLevelupCost{
        get{
            if(infoModel == null)
            {
                infoModel = BalanceInfoManager.Instance.TrophyInfoList.Find(n => n.Id == Id);
            }
            var originalValue = BigInteger.Parse(infoModel.HeartLevelupCost, NumberStyles.Any, CultureInfo.InvariantCulture);
            return originalValue * (BigInteger)Math.Min(double.MaxValue, Math.Pow(1.5d, HeartLevelUpCount));
        }
    }

    public int TrophyLevelupCost{
        get{
            return 2 * (TrophyLevelUpCount + 1);
        }
    }

    public int TrophySellReward{
        get{ 
            if(infoModel == null)
            {
                infoModel = BalanceInfoManager.Instance.TrophyInfoList.Find(n => n.Id == Id);
            }
            var originalValue = BigInteger.Parse(infoModel.HeartLevelupCost, NumberStyles.Any, CultureInfo.InvariantCulture);
            return (int)originalValue;
        }
    }
}