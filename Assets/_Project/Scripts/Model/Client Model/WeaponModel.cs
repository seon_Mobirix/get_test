﻿using System;
using System.Numerics;
using System.Globalization;
using UnityEngine;
using CodeStage.AntiCheat.ObscuredTypes;

[Serializable]
public class WeaponModel : ModelBase
{
    public string Id;
    public ObscuredInt Count;
    public ObscuredInt Level;

    public bool IsNew;

    private WeaponInfoModel infoModel = null;

    public bool IsAvail{
        get{
            if(Count > 0)
                return true;
            else
                return false;
        }
    }

    public float BaseAttackMultiplier{
        get{
            if(infoModel == null)
            {
                infoModel = BalanceInfoManager.Instance.WeaponInfoList.Find(n => n.Id == Id);
            }
            var originalValue = float.Parse(infoModel.AttackPower, NumberStyles.Any, CultureInfo.InvariantCulture);
            return originalValue + originalValue / 100f * (Level - 1);
        }
    }

    public BigInteger LevelupCost{
        get{
            if(infoModel == null)
                infoModel = BalanceInfoManager.Instance.WeaponInfoList.Find(n => n.Id == Id);
            
            if(LevelUpChancePercent == 0) 
                return 100;
            else
            {
                var originalValue = BigInteger.Parse(infoModel.LevelupCost, NumberStyles.Any, CultureInfo.InvariantCulture);
                return originalValue + originalValue * (Level - 1);
            }
        }
    }

    public int LevelUpChancePercent{
        get{
            return Mathf.Max(100 - (Level - 1), 0);
        }
    }
}