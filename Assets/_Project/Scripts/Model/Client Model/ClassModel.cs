﻿using System;
using System.Numerics;
using System.Globalization;
using UnityEngine;
using CodeStage.AntiCheat.ObscuredTypes;

[Serializable]
public class ClassModel : ModelBase
{
    public string Id;
    public ObscuredInt Count;
    public ObscuredInt Level;

    public bool IsNew;

    private ClassInfoModel infoModel = null;

    public bool IsAvail{
        get{
            if(Count > 0)
                return true;
            else
                return false;
        }
    }

    public string ClassRank{
        get{
            return infoModel.Rank;
        }
    }

    public float GoldIncomeMultiplier{
        get{
            if(infoModel == null)
            {
                infoModel = BalanceInfoManager.Instance.ClassInfoList.Find(n => n.Id == Id);
            }
            var originalValue = float.Parse(infoModel.GoldPower, NumberStyles.Any, CultureInfo.InvariantCulture);
            return originalValue + originalValue / 100f * (Level - 1);
        }
    }

    public float BerserkChargeSpeedMultiplier{
        get{
            if(infoModel == null)
            {
                infoModel = BalanceInfoManager.Instance.ClassInfoList.Find(n => n.Id == Id);
            }
            var originalValue = float.Parse(infoModel.BerserkPower, NumberStyles.Any, CultureInfo.InvariantCulture);
            return originalValue;
        }
    }

    public float AttackSpeedMultiplier{
        get{
            if(infoModel == null)
            {
                infoModel = BalanceInfoManager.Instance.ClassInfoList.Find(n => n.Id == Id);
            }
            var originalValue = float.Parse(infoModel.AttackSpeedPower, NumberStyles.Any, CultureInfo.InvariantCulture);
            return originalValue;
        }
    }

    public BigInteger LevelupCost{
        get{
            if(infoModel == null)
                infoModel = BalanceInfoManager.Instance.ClassInfoList.Find(n => n.Id == Id);
            
            if(LevelUpChancePercent == 0) 
                return 100;
            else
            {
                var originalValue = BigInteger.Parse(infoModel.LevelupCost, NumberStyles.Any, CultureInfo.InvariantCulture);
                return originalValue + originalValue * (Level - 1);
            }
        }
    }

    public int LevelUpChancePercent{
        get{
            return Mathf.Max(100 - (Level - 1), 0);
        }
    }
}