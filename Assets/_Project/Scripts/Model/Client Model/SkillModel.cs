﻿using System;
using System.Linq;
using System.Numerics;
using System.Globalization;
using CodeStage.AntiCheat.ObscuredTypes;

[Serializable]
public class SkillModel : ModelBase
{
    public string Id;
    public ObscuredInt Level;
    public ObscuredInt GoldLevelUpCount;
    public ObscuredInt BonusLevel;

    private SkillInfoModel infoModel = null;

    public float NextEffectPower{
        get{
            if(infoModel == null)
            {
                infoModel = BalanceInfoManager.Instance.SkillInfoList.Find(n => n.Id == Id);
            }
            var originalValue = float.Parse(infoModel.EffectPower, NumberStyles.Any, CultureInfo.InvariantCulture);
            return originalValue + originalValue / 100 * (Level + BonusLevel);
        }
    }

    public float EffectPower{
        get{
            if(infoModel == null)
            {
                infoModel = BalanceInfoManager.Instance.SkillInfoList.Find(n => n.Id == Id);
            }
            var originalValue = float.Parse(infoModel.EffectPower, NumberStyles.Any, CultureInfo.InvariantCulture);
            return originalValue + originalValue / 100 * (Level + BonusLevel - 1);
        }
    }

    public BigInteger GoldLevelupCost{
        get{
            if(infoModel == null)
            {
                infoModel = BalanceInfoManager.Instance.SkillInfoList.Find(n => n.Id == Id);
            }
            var originalValue = BigInteger.Parse(infoModel.GoldLevelupCost, NumberStyles.Any, CultureInfo.InvariantCulture);

            // Get legndary relic effect
            var targetLegendaryRelic = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.LegendaryRelicType == LegendaryRelicType.UPGRADE_DISCOUNT);
            var priceMutliplierPercentByLegendaryRelic = (targetLegendaryRelic != null)? targetLegendaryRelic.UpgradeCostReducePercent : 0;
            
            return (originalValue + originalValue * (BigInteger)Math.Min(double.MaxValue, Math.Pow(1.75d, (double)GoldLevelUpCount))) * (BigInteger)Math.Max(1f, 100f + priceMutliplierPercentByLegendaryRelic) / 100;
        }
    }
}