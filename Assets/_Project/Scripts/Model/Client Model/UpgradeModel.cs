﻿using System;
using System.Linq;
using System.Numerics;
using System.Globalization;
using UnityEngine;
using CodeStage.AntiCheat.ObscuredTypes;

public enum StatType{
    ATTACK_POWER,
    CRITICAL_POWER,
    CRITICAL_ATTACK_CHANCE,
    SUPER_POWER,
    SUPER_ATTACK_CHANCE,
    ULTIMATE_POWER,
    ULTIMATE_ATTACK_CHANCE,
    HYPER_POWER,
    HYPER_ATTACK_CHANCE
}

[Serializable]
public class UpgradeModel : ModelBase
{
    public ObscuredInt BaseAttackLevel = 1;
    public ObscuredInt CriticalAttackLevel = 1;
    public ObscuredInt CriticalChanceLevel = 1;
    public ObscuredInt SuperAttackLevel = 1;
    public ObscuredInt SuperChanceLevel = 1;
    public ObscuredInt UltimateAttackLevel = 1;
    public ObscuredInt UltimateChanceLevel = 1;
    public ObscuredInt HyperAttackLevel = 1;
    public ObscuredInt HyperChanceLevel = 1;

    public BigInteger BaseAttack{
        get{
            if(BaseAttackLevel <= 10)
                return BigInteger.Parse(BalanceInfoManager.Instance.UpgradeInfoList[BaseAttackLevel - 1].Attack, NumberStyles.Any, CultureInfo.InvariantCulture);
            else
                return GetMutlipliedValue(
                    BigInteger.Parse(BalanceInfoManager.Instance.UpgradeInfoList[9].Attack, NumberStyles.Any, CultureInfo.InvariantCulture), 
                    Double.Parse(BalanceInfoManager.Instance.AdditionalUpgradeInfoList[1].AttackMultiplier, NumberStyles.Any, CultureInfo.InvariantCulture), 
                    BaseAttackLevel
                );
        }
    }

    public BigInteger NextBaseAttack{
        get{
            if((BaseAttackLevel + 1) <= 10)
                return BigInteger.Parse(BalanceInfoManager.Instance.UpgradeInfoList[(BaseAttackLevel + 1) - 1].Attack, NumberStyles.Any, CultureInfo.InvariantCulture);
            else
                return GetMutlipliedValue(
                    BigInteger.Parse(BalanceInfoManager.Instance.UpgradeInfoList[9].Attack, NumberStyles.Any, CultureInfo.InvariantCulture), 
                    Double.Parse(BalanceInfoManager.Instance.AdditionalUpgradeInfoList[1].AttackMultiplier, NumberStyles.Any, CultureInfo.InvariantCulture), 
                    (BaseAttackLevel + 1)
                );
        }
    }

    public BigInteger BaseAttackLevelupPrice{
        get{
            return GetGoldTableReferencedValue(
                int.Parse(BalanceInfoManager.Instance.AdditionalUpgradeInfoList[1].AttackPriceStartTier), 
                BaseAttackLevel
            );
        }
    }

    public BigInteger CriticalAttackPercent{
        get{
            if(CriticalAttackLevel <= 10)
                return BigInteger.Parse(BalanceInfoManager.Instance.UpgradeInfoList[CriticalAttackLevel - 1].CriticalAttack, NumberStyles.Any, CultureInfo.InvariantCulture);
            else
                return GetMutlipliedValue(
                    BigInteger.Parse(BalanceInfoManager.Instance.UpgradeInfoList[9].CriticalAttack, NumberStyles.Any, CultureInfo.InvariantCulture), 
                    Double.Parse(BalanceInfoManager.Instance.AdditionalUpgradeInfoList[1].CriticalAttackMultiplier, NumberStyles.Any, CultureInfo.InvariantCulture), 
                    CriticalAttackLevel
                );
        }
    }

    public BigInteger NextCriticalAttackPercent{
        get{
            if((CriticalAttackLevel + 1) <= 10)
                return BigInteger.Parse(BalanceInfoManager.Instance.UpgradeInfoList[(CriticalAttackLevel + 1) - 1].CriticalAttack, NumberStyles.Any, CultureInfo.InvariantCulture);
            else
                return GetMutlipliedValue(
                    BigInteger.Parse(BalanceInfoManager.Instance.UpgradeInfoList[9].CriticalAttack, NumberStyles.Any, CultureInfo.InvariantCulture), 
                    Double.Parse(BalanceInfoManager.Instance.AdditionalUpgradeInfoList[1].CriticalAttackMultiplier, NumberStyles.Any, CultureInfo.InvariantCulture), 
                    (CriticalAttackLevel + 1)
                );
        }
    }

    public BigInteger CriticalAttackLevelupPrice{
        get{
            return GetGoldTableReferencedValue(
                int.Parse(BalanceInfoManager.Instance.AdditionalUpgradeInfoList[1].CriticalAttackPriceStartTier), 
                CriticalAttackLevel
            );    
        }
    }

    public float CriticalChance{
        get{
            if(CriticalChanceLevel <= 10)
                return float.Parse(BalanceInfoManager.Instance.UpgradeInfoList[CriticalChanceLevel - 1].CriticalChance, NumberStyles.Any, CultureInfo.InvariantCulture);
            else
                return GetCriticalChanceAdded(float.Parse(BalanceInfoManager.Instance.UpgradeInfoList[9].CriticalChance, NumberStyles.Any, CultureInfo.InvariantCulture), CriticalChanceLevel);
        }
    }

    public float NextCriticalChance{
        get{
            if((CriticalChanceLevel + 1) <= 10)
                return float.Parse(BalanceInfoManager.Instance.UpgradeInfoList[(CriticalChanceLevel + 1) - 1].CriticalChance, NumberStyles.Any, CultureInfo.InvariantCulture);
            else
                return GetCriticalChanceAdded(float.Parse(BalanceInfoManager.Instance.UpgradeInfoList[9].CriticalChance, NumberStyles.Any, CultureInfo.InvariantCulture), (CriticalChanceLevel + 1));
        }
    }

    public BigInteger CriticalChanceLevelupPrice{
        get{
            return GetGoldTableReferencedValue(
                int.Parse(BalanceInfoManager.Instance.AdditionalUpgradeInfoList[1].CriticalChancePriceStartTier), 
                CriticalChanceLevel
            );    
        }
    }

    public BigInteger SuperAttackPercent{
        get{
            if(SuperAttackLevel <= 1)
                return 100;
            
            if(SuperAttackLevel <= 10)
                return BigInteger.Parse(BalanceInfoManager.Instance.UpgradeInfoList[SuperAttackLevel - 1].SuperAttack, NumberStyles.Any, CultureInfo.InvariantCulture);
            else
                return GetMutlipliedValue(
                    BigInteger.Parse(BalanceInfoManager.Instance.UpgradeInfoList[9].SuperAttack, NumberStyles.Any, CultureInfo.InvariantCulture), 
                    Double.Parse(BalanceInfoManager.Instance.AdditionalUpgradeInfoList[1].SuperAttackMultiplier, NumberStyles.Any, CultureInfo.InvariantCulture), 
                    SuperAttackLevel
                );
        }
    }

    public BigInteger NextSuperAttackPercent{
        get{
            if((SuperAttackLevel + 1) <= 1)
                return 100;
            
            if((SuperAttackLevel + 1) <= 10)
                return BigInteger.Parse(BalanceInfoManager.Instance.UpgradeInfoList[(SuperAttackLevel + 1) - 1].SuperAttack, NumberStyles.Any, CultureInfo.InvariantCulture);
            else
                return GetMutlipliedValue(
                    BigInteger.Parse(BalanceInfoManager.Instance.UpgradeInfoList[9].SuperAttack, NumberStyles.Any, CultureInfo.InvariantCulture), 
                    Double.Parse(BalanceInfoManager.Instance.AdditionalUpgradeInfoList[1].SuperAttackMultiplier, NumberStyles.Any, CultureInfo.InvariantCulture), 
                    (SuperAttackLevel + 1)
                );
        }
    }

    public BigInteger SuperAttackLevelupPrice{
        get{
            return GetGoldTableReferencedValue(
                int.Parse(BalanceInfoManager.Instance.AdditionalUpgradeInfoList[1].SuperAttackPriceStartTier), 
                SuperAttackLevel
            );
        }
    }

    public float SuperChance{
        get{
            if(SuperChanceLevel <= 1)
                return 0f;

            if(SuperChanceLevel <= 10)
                return float.Parse(BalanceInfoManager.Instance.UpgradeInfoList[SuperChanceLevel - 1].SuperChance, NumberStyles.Any, CultureInfo.InvariantCulture);
            else
                return GetSuperChanceAdded(float.Parse(BalanceInfoManager.Instance.UpgradeInfoList[9].SuperChance, NumberStyles.Any, CultureInfo.InvariantCulture), SuperChanceLevel);
        }
    }

    public float NextSuperChance{
        get{
            if((SuperChanceLevel + 1) <= 1)
                return 0f;

            if((SuperChanceLevel + 1) <= 10)
                return float.Parse(BalanceInfoManager.Instance.UpgradeInfoList[(SuperChanceLevel + 1) - 1].SuperChance, NumberStyles.Any, CultureInfo.InvariantCulture);
            else
                return GetSuperChanceAdded(float.Parse(BalanceInfoManager.Instance.UpgradeInfoList[9].SuperChance, NumberStyles.Any, CultureInfo.InvariantCulture), (SuperChanceLevel + 1));
        }
    }
    
    public BigInteger SuperChanceLevelupPrice{
        get{
            return GetGoldTableReferencedValue(
                int.Parse(BalanceInfoManager.Instance.AdditionalUpgradeInfoList[1].SuperChancePriceStartTier), 
                SuperChanceLevel
            );
        }
    }

    public BigInteger UltimateAttackPercent{
        get{
            if(UltimateAttackLevel <= 1)
                return 100;
            
            if(UltimateAttackLevel <= 10)
                return BigInteger.Parse(BalanceInfoManager.Instance.UpgradeInfoList[UltimateAttackLevel - 1].UltimateAttack, NumberStyles.Any, CultureInfo.InvariantCulture);
            else
                return GetMutlipliedValue(
                    BigInteger.Parse(BalanceInfoManager.Instance.UpgradeInfoList[9].UltimateAttack, NumberStyles.Any, CultureInfo.InvariantCulture), 
                    Double.Parse(BalanceInfoManager.Instance.AdditionalUpgradeInfoList[1].UltimateAttackMultiplier, NumberStyles.Any, CultureInfo.InvariantCulture), 
                    UltimateAttackLevel
                );
        }
    }

    public BigInteger NextUltimateAttackPercent{
        get{
            if((UltimateAttackLevel + 1) <= 1)
                return 100;
            
            if((UltimateAttackLevel + 1) <= 10)
                return BigInteger.Parse(BalanceInfoManager.Instance.UpgradeInfoList[(UltimateAttackLevel + 1) - 1].UltimateAttack, NumberStyles.Any, CultureInfo.InvariantCulture);
            else
                return GetMutlipliedValue(
                    BigInteger.Parse(BalanceInfoManager.Instance.UpgradeInfoList[9].UltimateAttack, NumberStyles.Any, CultureInfo.InvariantCulture), 
                    Double.Parse(BalanceInfoManager.Instance.AdditionalUpgradeInfoList[1].UltimateAttackMultiplier, NumberStyles.Any, CultureInfo.InvariantCulture), 
                    (UltimateAttackLevel + 1)
                );
        }
    }

    public BigInteger UltimateAttackLevelupPrice{
        get{
            return GetGoldTableReferencedValue(
                int.Parse(BalanceInfoManager.Instance.AdditionalUpgradeInfoList[1].UltimateAttackPriceStartTier), 
                UltimateAttackLevel
            );
        }
    }

    public float UltimateChance{
        get{
            if(UltimateChanceLevel <= 1)
                return 0f;
            
            if(UltimateChanceLevel <= 10)
                return float.Parse(BalanceInfoManager.Instance.UpgradeInfoList[UltimateChanceLevel - 1].UltimateChance, NumberStyles.Any, CultureInfo.InvariantCulture);
            else
                return GetUltimateChanceAdded(float.Parse(BalanceInfoManager.Instance.UpgradeInfoList[9].UltimateChance, NumberStyles.Any, CultureInfo.InvariantCulture), UltimateChanceLevel);
        }
    }

    public float NextUltimateChance{
        get{
            if((UltimateChanceLevel + 1) <= 1)
                return 0f;
            
            if((UltimateChanceLevel + 1) <= 10)
                return float.Parse(BalanceInfoManager.Instance.UpgradeInfoList[(UltimateChanceLevel + 1) - 1].UltimateChance, NumberStyles.Any, CultureInfo.InvariantCulture);
            else
                return GetUltimateChanceAdded(float.Parse(BalanceInfoManager.Instance.UpgradeInfoList[9].UltimateChance, NumberStyles.Any, CultureInfo.InvariantCulture), (UltimateChanceLevel)+ 1) ;
        }
    }

    public BigInteger UltimateChanceLevelupPrice{
        get{
            return GetGoldTableReferencedValue(
                int.Parse(BalanceInfoManager.Instance.AdditionalUpgradeInfoList[1].UltimateChancePriceStartTier), 
                UltimateChanceLevel
            );
        }
    }

    public BigInteger HyperAttackPercent{
        get{
            if(HyperAttackLevel <= 1)
                return 100;
            
            if(HyperAttackLevel <= 10)
                return BigInteger.Parse(BalanceInfoManager.Instance.UpgradeInfoList[HyperAttackLevel - 1].HyperAttack, NumberStyles.Any, CultureInfo.InvariantCulture);
            else
                return GetMutlipliedValue(
                    BigInteger.Parse(BalanceInfoManager.Instance.UpgradeInfoList[9].HyperAttack, NumberStyles.Any, CultureInfo.InvariantCulture), 
                    Double.Parse(BalanceInfoManager.Instance.AdditionalUpgradeInfoList[1].HyperAttackMultiplier, NumberStyles.Any, CultureInfo.InvariantCulture), 
                    HyperAttackLevel
                );
        }
    }

    public BigInteger NextHyperAttackPercent{
        get{
            if((HyperAttackLevel + 1) <= 1)
                return 100;
            
            if((HyperAttackLevel + 1) <= 10)
                return BigInteger.Parse(BalanceInfoManager.Instance.UpgradeInfoList[(HyperAttackLevel + 1) - 1].HyperAttack, NumberStyles.Any, CultureInfo.InvariantCulture);
            else
                return GetMutlipliedValue(
                    BigInteger.Parse(BalanceInfoManager.Instance.UpgradeInfoList[9].HyperAttack, NumberStyles.Any, CultureInfo.InvariantCulture), 
                    Double.Parse(BalanceInfoManager.Instance.AdditionalUpgradeInfoList[1].HyperAttackMultiplier, NumberStyles.Any, CultureInfo.InvariantCulture), 
                    (HyperAttackLevel + 1)
                );
        }
    }

    public BigInteger HyperAttackLevelupPrice{
        get{
            return GetGoldTableReferencedValue(
                int.Parse(BalanceInfoManager.Instance.AdditionalUpgradeInfoList[1].HyperAttackPriceStartTier), 
                HyperAttackLevel
            );
        }
    }

    public float HyperChance{
        get{
            if(HyperChanceLevel <= 1)
                return 0f;
            
            if(HyperChanceLevel <= 10)
                return float.Parse(BalanceInfoManager.Instance.UpgradeInfoList[HyperChanceLevel - 1].HyperChance, NumberStyles.Any, CultureInfo.InvariantCulture);
            else
                return GetHyperChanceAdded(float.Parse(BalanceInfoManager.Instance.UpgradeInfoList[9].HyperChance, NumberStyles.Any, CultureInfo.InvariantCulture), HyperChanceLevel);
        }
    }

    public float NextHyperChance{
        get{
            if((HyperChanceLevel + 1) <= 1)
                return 0f;
            
            if((HyperChanceLevel + 1) <= 10)
                return float.Parse(BalanceInfoManager.Instance.UpgradeInfoList[(HyperChanceLevel + 1) - 1].HyperChance, NumberStyles.Any, CultureInfo.InvariantCulture);
            else
                return GetHyperChanceAdded(float.Parse(BalanceInfoManager.Instance.UpgradeInfoList[9].HyperChance, NumberStyles.Any, CultureInfo.InvariantCulture), (HyperChanceLevel)+ 1) ;
        }
    }

    public BigInteger HyperChanceLevelupPrice{
        get{
            return GetGoldTableReferencedValue(
                int.Parse(BalanceInfoManager.Instance.AdditionalUpgradeInfoList[1].HyperChancePriceStartTier), 
                HyperChanceLevel
            );
        }
    }

    public static BigInteger Lerp(BigInteger a, BigInteger b, int t)
    {
        return a + (b - a) * (t * 100000) / 700000;
    }
    
    public BigInteger GetMutlipliedValue(BigInteger original, double multiplier, int level)
    {
        var excessiveLevel = level - 10;
        var halfExcessiveLevel = excessiveLevel / 2;

        return original 
            * (BigInteger)(Math.Min(double.MaxValue, Math.Pow(multiplier, excessiveLevel - halfExcessiveLevel) * 10000)) / 10000 
            * (BigInteger)(Math.Min(double.MaxValue, Math.Pow(multiplier, halfExcessiveLevel) * 10000)) / 10000;
    }

    public BigInteger GetGoldTableReferencedValue(int startGoldTier, int level)
    {
        var goldTier = (level - 1) / 7 + startGoldTier;

        // Safety check: if goldTier is greater than the size of BalanceInfoManager.Instance.RewardGoldInfoList, return -1
        // This will block upgrade.
        if(BalanceInfoManager.Instance.RequiredGoldInfoList.Count <= goldTier)
            return -1;

        var subGoldTier = (level - 1) % 7;
        var priceForCurrentGoldTier = BigInteger.Parse(BalanceInfoManager.Instance.RequiredGoldInfoList[goldTier - 1].Value, NumberStyles.Any, CultureInfo.InvariantCulture);
        var priceForNextGoldTier = BigInteger.Parse(BalanceInfoManager.Instance.RequiredGoldInfoList[goldTier].Value, NumberStyles.Any, CultureInfo.InvariantCulture);

        // Get legndary relic effect
        var targetLegendaryRelic = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.LegendaryRelicType == LegendaryRelicType.UPGRADE_DISCOUNT);
        var priceMutliplierPercentByLegendaryRelic = (targetLegendaryRelic != null)? targetLegendaryRelic.UpgradeCostReducePercent : 0;
            
        return (BigInteger)(priceForCurrentGoldTier + Lerp(priceForCurrentGoldTier, priceForNextGoldTier, subGoldTier)) * (BigInteger)Math.Max(1f, 100f + priceMutliplierPercentByLegendaryRelic) / 100;
    }

    public float GetCriticalChanceAdded(float original, float level)
    {
        {
            var excessiveLevel = level - 10;
            var toReturn = original + excessiveLevel * float.Parse(BalanceInfoManager.Instance.AdditionalUpgradeInfoList[1].CriticalChanceAdd, NumberStyles.Any, CultureInfo.InvariantCulture);
            return UnityEngine.Mathf.Min(toReturn, 1f);
        }
    }

    public float GetSuperChanceAdded(float original, float level)
    {
        {
            var excessiveLevel = level - 10;
            var toReturn = original + excessiveLevel * float.Parse(BalanceInfoManager.Instance.AdditionalUpgradeInfoList[1].SuperChanceAdd, NumberStyles.Any, CultureInfo.InvariantCulture);
            return UnityEngine.Mathf.Min(toReturn, 1f);
        }
    }

    public float GetUltimateChanceAdded(float original, float level)
    {
        {
            var excessiveLevel = level - 10;
            var toReturn = original + excessiveLevel * float.Parse(BalanceInfoManager.Instance.AdditionalUpgradeInfoList[1].UltimateChanceAdd, NumberStyles.Any, CultureInfo.InvariantCulture);
            return UnityEngine.Mathf.Min(toReturn, 1f);
        }
    }

    public float GetHyperChanceAdded(float original, float level)
    {
        {
            var excessiveLevel = level - 10;
            var toReturn = original + excessiveLevel * float.Parse(BalanceInfoManager.Instance.AdditionalUpgradeInfoList[1].HyperChanceAdd, NumberStyles.Any, CultureInfo.InvariantCulture);
            return UnityEngine.Mathf.Min(toReturn, 1f);
        }
    }
}
