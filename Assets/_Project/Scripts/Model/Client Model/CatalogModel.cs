﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using CodeStage.AntiCheat.ObscuredTypes;

public enum GoodsPriceType {FREE, CASH, GEM, FEATHER, PACKAGE};

[Serializable]
public class CatalogModel : ModelBase
{
    [Title("Shop")]
    public List<NormalPackageModel> NormalPackageList = new List<NormalPackageModel>();
    public List<GrowthPackageModel> GrowthPackageList = new List<GrowthPackageModel>();
    public List<GemBundleModel> GemBundleList = new List<GemBundleModel>();
    public List<GoldBundleModel> GoldBundleList = new List<GoldBundleModel>();
    public List<WingProductModel> WingProductList = new List<WingProductModel>();

    public ObscuredInt WeaponGachaPrice = 0;
    public ObscuredInt WeaponGacha11Price = 0;
    public ObscuredInt WeaponGacha55Price = 0;
    public ObscuredInt ClassGachaPrice = 0;
    public ObscuredInt ClassGacha11Price = 0;
    public ObscuredInt ClassGacha55Price = 0;
    public ObscuredInt RelicGachaPrice = 0;
    public ObscuredInt RelicGacha11Price = 0;
    public ObscuredInt RelicGacha55Price = 0;
}
