﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class GoldInfoModel : ModelBase {
    public string Tier;
    public string Value;
}
