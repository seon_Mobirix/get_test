using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TrophyInfoModel : ModelBase {
    public string Id;
    public string EffectPower;
    public string HeartLevelupCost;
    public string FirstAppearFloor;
    public string LastAppearFloor;
    public string SummonPower;
}
