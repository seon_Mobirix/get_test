﻿using System;

[Serializable]
public class UpgradeInfoModel : ModelBase {
    public string Level;
    public string Attack;
    public string CriticalAttack;
    public string CriticalChance;
    public string SuperAttack;
    public string SuperChance;
    public string UltimateAttack;
    public string UltimateChance;
    public string HyperAttack;
    public string HyperChance;
}
