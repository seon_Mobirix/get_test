﻿using System;

[Serializable]
public class WeaponInfoModel : ModelBase {
    public string Id;
    public string Rank;
    public string AttackPower;
    public string LevelupCost;
    public string SummonPower;
}
