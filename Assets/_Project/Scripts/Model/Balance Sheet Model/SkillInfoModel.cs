﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SkillInfoModel : ModelBase {
    public string Id;
    public string EffectPower;
    public string GoldLevelupCost;
}
