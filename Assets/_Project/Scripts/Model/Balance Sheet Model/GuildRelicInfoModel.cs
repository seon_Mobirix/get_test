﻿using System;

[Serializable]
public class GuildRelicInfoModel : ModelBase {
    public string Id;
    public GuildRelicType RelicType;
    public string DefaultEffect;
    public string EffectPower;
    public int MaxLevel;
    public int Price;
}
