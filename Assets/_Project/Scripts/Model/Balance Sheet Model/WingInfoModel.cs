﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WingInfoModel : ModelBase
{
    public string Rank;

    public int OwnedStatBonus1Min;
    public int OwnedStatBonus1Max;
    public int OwnedStatBonus2Min;
    public int OwnedStatBonus2Max;

    public int EquipmentStatBonus1;
    public int EquipmentStatBonus2;
    public int EquipmentStatBonus3;

    public string SummonPower;

    public int GetOwnedStatBonus1Value(int subRank)
    {
        // Limit sub rank value (0 ~ 3)
        subRank = Math.Min(subRank, 3);

        var statBonusValueList = new List<int>();
        var step = (OwnedStatBonus1Max - OwnedStatBonus1Min) / 3;
        
        for(int i = OwnedStatBonus1Min; i <= OwnedStatBonus1Max; i += step)
        {
            statBonusValueList.Add(i);
        }
        
        return statBonusValueList[subRank];
    }

    public int GetOwnedStatBonus2Value(int subRank)
    {
        // Limit sub rank value (0 ~ 3)
        subRank = Math.Min(subRank, 3);

        var statBonusValueList = new List<int>();
        var step = (OwnedStatBonus2Max - OwnedStatBonus2Min) / 3;

        for(int i = OwnedStatBonus2Min; i <= OwnedStatBonus2Max; i += step)
        {
            statBonusValueList.Add(i);
        }

        return statBonusValueList[subRank];
    }
}
