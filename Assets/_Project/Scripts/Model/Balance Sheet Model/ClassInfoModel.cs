﻿using System;

[Serializable]
public class ClassInfoModel : ModelBase {
    public string Id;
    public string Rank;
    public string GoldPower;
    public string BerserkPower;
    public string AttackSpeedPower;
    public string LevelupCost;
    public string SummonPower;
}
