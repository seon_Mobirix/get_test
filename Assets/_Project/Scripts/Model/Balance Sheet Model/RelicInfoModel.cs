﻿using System;

[Serializable]
public class RelicInfoModel : ModelBase {
    public string Id;
    public string DefaultEffect;
    public string EffectPower;
    public string MaxLevel;
    public string SummonPower;
}
