﻿using System;

[Serializable]
public class MercenaryInfoModel : ModelBase
{
    public string Id;
    public string Rank;
    public string AttackPower;
    public string MercenaryAttackPower;
    public string LevelupCost;
    public string SummonPower;
}