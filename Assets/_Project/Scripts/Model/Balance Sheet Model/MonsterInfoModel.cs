﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class MonsterInfoModel : ModelBase {
    public string Id;
    public string PetRank;
    public string Region1;
    public string Region2;
    public string Region3;
    public string Region4;
    public string Region5;
    public string Region6;
    public string Region7;
    public string Region8;
}
