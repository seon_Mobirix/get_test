﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PetInfoModel : ModelBase {
    public string Id;
    public string Rank;
    public string EffectPower;
    public string GoldLevelupCost;
    public string HeartLevelupCost;
    public string SummonPower;
}
