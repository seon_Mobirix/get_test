﻿using System;
using System.Collections.Generic;

[Serializable]
public class BalanceInfoModel : ModelBase {

    // balance_main_growth.json

    public List<StageInfoModel> StageInfo;
    public List<GoldInfoModel> RewardGoldInfo;
    public List<GoldInfoModel> RequiredGoldInfo;
    public List<UpgradeInfoModel> UpgradeInfo;
    public List<AdditionalUpgradeInfoModel> AdditionalUpgradeInfo;

    // balance_upgrade.json

    public List<ClassInfoModel> ClassInfo;
    public List<WeaponInfoModel> WeaponInfo;
    public List<MercenaryInfoModel> MercenaryInfo;
    public List<RelicInfoModel> RelicInfo;
    public List<RelicInfoModel> LegendaryRelicInfo;
    public List<GuildRelicInfoModel> GuildRelicInfo;
    public List<WingInfoModel> WingInfo;
    public List<SkillInfoModel> SkillInfo;
    public List<PetInfoModel> PetInfo;
    public List<TrophyInfoModel> TrophyInfo;
    public List<SummonInfoModel> SummonInfo;

    // balance_monster.json

    public List<MonsterInfoModel> MonsterInfo;

    // balnce_achievement.json

    public List<AchievementInfoModel> AchievementInfo;

}
