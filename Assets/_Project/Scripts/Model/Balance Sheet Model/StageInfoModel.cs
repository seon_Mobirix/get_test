﻿using System;

[Serializable]
public class StageInfoModel: ModelBase{
    public string Id;
    public string RegionId;
    public string Size;
    public string Hp;
    public string RewardTier;
    public string FarmItem;
    public string FarmItemIndex;
    public string FarmChance;
}
