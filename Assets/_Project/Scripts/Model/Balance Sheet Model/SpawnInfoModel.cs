﻿using System;
using System.Collections.Generic;

[Serializable]
public class SpawnInfoModel : ModelBase {
    public string Id;
    public List<string> MobList = new List<string>();
}
