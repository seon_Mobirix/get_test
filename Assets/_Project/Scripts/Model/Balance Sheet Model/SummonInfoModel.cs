﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SummonInfoModel : ModelBase {
    public string Level;
    public string SummonExp;
    public string SummonMinIndex;
}
