using System;

[Serializable]
public class AdditionalUpgradeInfoModel : ModelBase {
    public string Level;
    public string AttackMultiplier;
    public string AttackPriceStartTier;
    public string CriticalAttackMultiplier;
    public string CriticalAttackPriceStartTier;
    public string CriticalChanceAdd;
    public string CriticalChancePriceStartTier;
    public string SuperAttackMultiplier;
    public string SuperAttackPriceStartTier;
    public string SuperChanceAdd;
    public string SuperChancePriceStartTier;
    public string UltimateAttackMultiplier;
    public string UltimateAttackPriceStartTier;
    public string UltimateChanceAdd;
    public string UltimateChancePriceStartTier;
    public string HyperAttackMultiplier;
    public string HyperAttackPriceStartTier;
    public string HyperChanceAdd;
    public string HyperChancePriceStartTier;
}
