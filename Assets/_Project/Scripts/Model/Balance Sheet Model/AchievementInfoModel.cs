﻿using System;

[Serializable]
public class AchievementInfoModel : ModelBase
{
    public string Id;
    public int BasicGoal;
    public bool IsIncreaseGoal;
    public int MaxIncreaseGoal;
    public int MaxLevel;
    public int GemReward;
}
