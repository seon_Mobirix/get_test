﻿using System;

[Serializable]

public class SystemPostModel : ModelBase
{
   public int Id;
   public string Message;
   public SystemPostRewardModel AttachedItem;
   public double ExpiredTime;
}
