﻿using System;
using System.Numerics;

[Serializable]
public class RewardModel : ModelBase
{
   public string ItemId;
   public BigInteger Count;
}
