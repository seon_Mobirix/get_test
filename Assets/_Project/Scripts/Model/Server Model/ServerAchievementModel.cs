﻿
using System;

[Serializable]
public class ServerAchievementModel : ModelBase
{
    public string Id;
    public int Count;
    public int Goal;
    public int Level;
    public bool IsMaxLevel;
}