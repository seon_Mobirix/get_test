﻿using System;

[Serializable]
public class ArenaRankModel : ModelBase {
    public string name;
    public int position;
    public int score;
    public int subScore;
}
