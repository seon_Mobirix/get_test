﻿using System;

[Serializable]
public class GrowthRewardModel : ModelBase
{
    public string ItemId;
    public int Amount;
}