﻿using System;
using System.Collections.Generic;

[Serializable]
public class DailyRewardListModel : ModelBase
{
    public List<DailyRewardModel> DailyRewardList = new List<DailyRewardModel>();
}