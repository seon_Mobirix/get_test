﻿using System;
using System.Collections.Generic;

[Serializable]
public class PostRewardListModel : ModelBase
{
    public List<PostRewardModel> ItemList = new List<PostRewardModel>();
}
