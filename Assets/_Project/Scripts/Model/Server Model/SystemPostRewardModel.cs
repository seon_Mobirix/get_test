﻿using System;

[Serializable]
public class SystemPostRewardModel : ModelBase
{
   public string ItemId;
   public string ItemCode;
   public string Count;
}