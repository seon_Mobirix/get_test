
using System;

[Serializable]
public class ServerSkillModel : ModelBase
{
    public string Id;
    public int Level;
    public int GoldLevelUpCount;
    public int BonusLevel;
}