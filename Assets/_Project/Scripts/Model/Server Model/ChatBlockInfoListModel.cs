﻿using System;
using System.Collections.Generic;

[Serializable]
public class ChatBlockInfoListModel : ModelBase
{
    public List<ChatBlockInfoModel> ChatBlockList = new List<ChatBlockInfoModel>();
}
