using System;
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;

[Serializable]
public class CountAndLevel{
    public string id;
    public int level;
    public int count;
    public bool isNew;
}

[Serializable]
public class CountAndLevelExtended{
    public string id;
    public int level;
    public int count;
    public bool isNew;
    public int goldLevelUpCount;
    public int heartLevelUpCount;
    public int mergeLevelUpCount;
}

public class ServerInventoryModel : ModelBase
{

    public double TimeStamp;

    public int UnsyncedGem;

#region Currency

    public string Gold;
    public string Heart;
    public string TicketRaid;

#endregion

#region Items

    public List<CountAndLevel> OwnedClassList = new List<CountAndLevel>();
    public List<CountAndLevel> OwnedWeaponList = new List<CountAndLevel>();
    public List<CountAndLevel> OwnedMercenaryList = new List<CountAndLevel>();
    public List<CountAndLevel> OwnedRelicList = new List<CountAndLevel>();
    public List<CountAndLevel> OwnedLegendaryRelicList = new List<CountAndLevel>();
    public List<CountAndLevelExtended> OwnedPetList = new List<CountAndLevelExtended>();
    public List<CountAndLevelExtended> OwnedTrophyList = new List<CountAndLevelExtended>();

#endregion

#region Slot Info
    public string ClassId;
    public string WeaponId;
    public string MercenaryId;
    public List<string> PetIdList;
    public string MainPetId;
    public List<string> TrophyIdList;
    public int WingIndex = -1;
#endregion

    public int ProgressPoint{
        get{
            var pointToReturn = 0;

            pointToReturn += OwnedClassList.Count;
            pointToReturn += OwnedWeaponList.Count;
            pointToReturn += OwnedMercenaryList.Count;
            pointToReturn += OwnedRelicList.Count;
            pointToReturn += OwnedLegendaryRelicList.Count;
            pointToReturn += OwnedPetList.Count;
            pointToReturn += OwnedTrophyList.Count;

            foreach(var tmp in OwnedClassList)
                pointToReturn += tmp.level;
            
            foreach(var tmp in OwnedWeaponList)
                pointToReturn += tmp.level;
            
            foreach(var tmp in OwnedMercenaryList)
                pointToReturn += tmp.level;
            
            foreach(var tmp in OwnedRelicList)
                pointToReturn += tmp.level;

            foreach(var tmp in OwnedLegendaryRelicList)
                pointToReturn += tmp.level;

            foreach(var tmp in OwnedPetList)
                pointToReturn += tmp.level;

            foreach(var tmp in OwnedTrophyList)
                pointToReturn += tmp.level;

            return pointToReturn;
        }
    }

    public static ServerInventoryModel FromLocalModel(InventoryModel model)
    {
        ServerInventoryModel toReturn = new ServerInventoryModel();

        toReturn.TimeStamp = DateTimeOffset.Now.ToUnixTimeSeconds();

        toReturn.UnsyncedGem = ServerNetworkManager.Instance.UnsyncedGem;

        toReturn.Gold = model.Gold.ToString();
        toReturn.Heart = model.Heart.ToString();
        toReturn.TicketRaid = model.TicketRaid.ToString();

        toReturn.OwnedClassList = new List<CountAndLevel>();
        foreach(var tmp in model.ClassList)
        {
            var toAdd = new CountAndLevel();
            toAdd.id = tmp.Id;
            toAdd.count = tmp.Count;
            toAdd.isNew = tmp.IsNew;
            toAdd.level = tmp.Level;
            toReturn.OwnedClassList.Add(toAdd);
        }

        toReturn.OwnedWeaponList = new List<CountAndLevel>();
        foreach(var tmp in model.WeaponList)
        {
            var toAdd = new CountAndLevel();
            toAdd.id = tmp.Id;
            toAdd.count = tmp.Count;
            toAdd.isNew = tmp.IsNew;
            toAdd.level = tmp.Level;
            toReturn.OwnedWeaponList.Add(toAdd);
        }

        toReturn.OwnedMercenaryList = new List<CountAndLevel>();
        foreach(var tmp in model.MercenaryList)
        {
            var toAdd = new CountAndLevel();
            toAdd.id = tmp.Id;
            toAdd.count = tmp.Count;
            toAdd.isNew = tmp.IsNew;
            toAdd.level = tmp.Level;
            toReturn.OwnedMercenaryList.Add(toAdd);
        }

        toReturn.OwnedRelicList = new List<CountAndLevel>();
        foreach(var tmp in model.RelicList)
        {
            var toAdd = new CountAndLevel();
            toAdd.id = tmp.Id;
            toAdd.count = tmp.Count;
            toAdd.isNew = tmp.IsNew;
            toAdd.level = tmp.Level;
            toReturn.OwnedRelicList.Add(toAdd);
        }

        toReturn.OwnedLegendaryRelicList = new List<CountAndLevel>();
        foreach(var tmp in model.LegendaryRelicList)
        {
            var toAdd = new CountAndLevel();
            toAdd.id = tmp.Id;
            toAdd.count = tmp.Count;
            toAdd.isNew = tmp.IsNew;
            toAdd.level = tmp.Level;
            toReturn.OwnedLegendaryRelicList.Add(toAdd);
        }

        toReturn.OwnedPetList = new List<CountAndLevelExtended>();
        foreach(var tmp in model.PetList)
        {
            var toAdd = new CountAndLevelExtended();
            toAdd.id = tmp.Id;
            toAdd.count = tmp.Count;
            toAdd.isNew = tmp.IsNew;
            toAdd.level = tmp.Level;
            toAdd.goldLevelUpCount = tmp.GoldLevelUpCount;
            toAdd.heartLevelUpCount = tmp.HeartLevelUpCount;
            toAdd.mergeLevelUpCount = tmp.PetLevelUpCount;
            toReturn.OwnedPetList.Add(toAdd);
        }

        toReturn.OwnedTrophyList = new List<CountAndLevelExtended>();
        foreach(var tmp in model.TrophyList)
        {
            var toAdd = new CountAndLevelExtended();
            toAdd.id = tmp.Id;
            toAdd.count = tmp.Count;
            toAdd.level = tmp.Level;
            toAdd.heartLevelUpCount = tmp.HeartLevelUpCount;
            toAdd.mergeLevelUpCount = tmp.TrophyLevelUpCount;
            toReturn.OwnedTrophyList.Add(toAdd);
        }

        toReturn.ClassId = model.SelectedClass.Id;

        toReturn.WeaponId = model.SelectedWeapon.Id;

        // Check selected mercenary null
        if(model.SelectedMercenary != null)
            toReturn.MercenaryId = model.SelectedMercenary.Id;
        else
            toReturn.MercenaryId = "";

        toReturn.PetIdList = new List<string>();
        foreach(var tmp in model.SelectedPetList)
        {
            toReturn.PetIdList.Add(tmp.Id);
        }

        if(model.MainPet != null)
            toReturn.MainPetId = model.MainPet.Id;
        else
            toReturn.MainPetId = "";

        toReturn.TrophyIdList = new List<string>();
        foreach(var tmp in model.SelectedTrophyList)
        {
            toReturn.TrophyIdList.Add(tmp.Id);
        }
        
        toReturn.WingIndex = model.WingIndex;

        return toReturn;
    }
}
