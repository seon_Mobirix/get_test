﻿using System;

[Serializable]
public class WingOwnedStatBonusModel : ModelBase
{
    public string Stat1Rank;
    public int Stat1SubRank;
    public string Stat2Rank;
    public int Stat2SubRank;
}
