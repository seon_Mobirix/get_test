﻿using System;

[Serializable]
public class WingEquipmentStatBonusModel : ModelBase
{
    public string Stat1Rank;
    public string Stat2Rank;
    public string Stat3Rank;
}