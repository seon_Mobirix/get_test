﻿using System;
using System.Collections.Generic;

[Serializable]
public class SystemPostRewardListModel : ModelBase
{
    public List<SystemPostRewardModel> ItemList = new List<SystemPostRewardModel>();
}