﻿using System;
using System.Collections.Generic;

[Serializable]
public class AttendanceEventInfoModel : ModelBase
{
    public int StartTimeStampDay = 0;
    public int EndTimeStampDay = 0;
    public int AttendanceDayLimit = 0;
    public List<AttendanceEventRewardModel> AttendanceEventRewardList = new List<AttendanceEventRewardModel>();
}