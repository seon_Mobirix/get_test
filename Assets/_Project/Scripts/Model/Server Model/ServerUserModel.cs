﻿using System;
using System.Collections.Generic;

public class ServerUserModel : ModelBase
{
    public double TimeStamp;

#region Stat Info
    public int BaseAttackLevel;
    public int CriticalAttackLevel;
    public int CriticalChanceLevel;
    public int SuperAttackLevel;
    public int SuperChanceLevel;
    public int UltimateAttackLevel;
    public int UltimateChanceLevel;
    public int HyperAttackLevel;
    public int HyperChanceLevel;
#endregion

    public int AutoAttackTier = 0;
    public int AttackBuffTier = 0;
    public int SpeedBuffTier = 0;
    public int GoldBuffTier = 0;

    public float AutoEnableFreeBuffPassed = 0f;
    public float AttackFreeBuffPassed = 0f;
    public float SpeedFreeBuffPassed = 0f;
    public float GoldFreeBuffPassed = 0f;

    public int StageProgress;
    public int SelectedStage;

    public int KillCountForBestStage;

    public int WeaponGachaLevel;
    public int WeaponGachaExp;
    public int ClassGachaLevel;
    public int ClassGachaExp;
    public int MercenaryGachaLevel;
    public int MercenaryGachaExp;
    public int PetGachaLevel;
    public int PetGachaExp;
    public int VIPLevel;
    public int VIPExp;

    public int TotalWingAttackBonus = 0;
	public int TotalWingGoldBonus = 0;

    public List<ServerSkillModel> SkillInfo = new List<ServerSkillModel>();

    public List<ServerAchievementModel> AchievementInfo = new List<ServerAchievementModel>();

    // Firebase Analytics Log
    public int LastSummonLogCount = 0;
    public int LastFreeGemRewardLogCount = 0;
    public int BuffAndRestRewardCount = 0;

    // For fixing the UnsyncedGem issue
    public int UnsyncedGemChangeCount = -1;

    public int ProgressPoint {
        get{
            var pointToReturn = 0;

            foreach(var tmp in SkillInfo)
                pointToReturn += tmp.Level;
            
            foreach(var tmp in AchievementInfo)
                pointToReturn += tmp.Level;

            pointToReturn += BaseAttackLevel;
            pointToReturn += CriticalAttackLevel;
            pointToReturn += CriticalChanceLevel;
            pointToReturn += SuperAttackLevel;
            pointToReturn += SuperChanceLevel;
            pointToReturn += UltimateAttackLevel;
            pointToReturn += UltimateChanceLevel;
            pointToReturn += HyperAttackLevel;
            pointToReturn += HyperChanceLevel;

            pointToReturn += StageProgress;

            pointToReturn += WeaponGachaLevel;
            pointToReturn += ClassGachaLevel;
            pointToReturn += MercenaryGachaLevel;
            pointToReturn += PetGachaLevel;

            return pointToReturn;
        }
    }

    public static ServerUserModel FromLocalModel(UserModel model)
    {
        var toReturn = new ServerUserModel();

        toReturn.TimeStamp = DateTimeOffset.Now.ToUnixTimeSeconds();

        toReturn.BaseAttackLevel = model.Upgrade.BaseAttackLevel;
		toReturn.CriticalAttackLevel = model.Upgrade.CriticalAttackLevel;
		toReturn.CriticalChanceLevel = model.Upgrade.CriticalChanceLevel;
		toReturn.SuperAttackLevel = model.Upgrade.SuperAttackLevel;
		toReturn.SuperChanceLevel = model.Upgrade.SuperChanceLevel;
		toReturn.UltimateAttackLevel = model.Upgrade.UltimateAttackLevel;
		toReturn.UltimateChanceLevel = model.Upgrade.UltimateChanceLevel;
        toReturn.HyperAttackLevel = model.Upgrade.HyperAttackLevel;
		toReturn.HyperChanceLevel = model.Upgrade.HyperChanceLevel;

        toReturn.AutoAttackTier = model.AutoEnabledTier;
        toReturn.AttackBuffTier = model.AttackBuffTier;
        toReturn.SpeedBuffTier = model.SpeedBuffTier;
        toReturn.GoldBuffTier = model.GoldBuffTier;

        toReturn.AutoEnableFreeBuffPassed = BuffManager.Instance.AutoEnablePassed;
        toReturn.AttackFreeBuffPassed = BuffManager.Instance.AttackBuffPassed;
        toReturn.SpeedFreeBuffPassed = BuffManager.Instance.SpeedBuffPassed;
        toReturn.GoldFreeBuffPassed = BuffManager.Instance.GoldBuffPassed;

        toReturn.KillCountForBestStage = model.KillCountForBestStage;

        toReturn.StageProgress = model.StageProgress;
        toReturn.SelectedStage = model.SelectedStage;

        toReturn.WeaponGachaLevel = model.WeaponGachaLevel;
        toReturn.WeaponGachaExp = model.WeaponGachaExp;
        toReturn.ClassGachaLevel = model.ClassGachaLevel;
        toReturn.ClassGachaExp = model.ClassGachaExp;
        toReturn.MercenaryGachaLevel = model.MercenaryGachaLevel;
        toReturn.MercenaryGachaExp = model.MercenaryGachaExp;
        toReturn.PetGachaLevel = model.PetGachaLevel;
        toReturn.PetGachaExp = model.PetGachaExp;
        toReturn.VIPLevel = model.VIPLevel;
        toReturn.VIPExp = model.VIPExp;

        toReturn.TotalWingAttackBonus = model.TotalWingAttackBonus;
        toReturn.TotalWingGoldBonus = model.TotalWingGoldBonus;

        toReturn.SkillInfo.Clear();
        foreach(var tmp in model.SkillInfo)
        {
            var tmpServerSkillModel = new ServerSkillModel();
            
            tmpServerSkillModel.Id = tmp.Id;
            tmpServerSkillModel.Level = tmp.Level;
            tmpServerSkillModel.GoldLevelUpCount = tmp.GoldLevelUpCount;
            tmpServerSkillModel.BonusLevel = tmp.BonusLevel;

            toReturn.SkillInfo.Add(tmpServerSkillModel);
        }

        toReturn.AchievementInfo.Clear();
        foreach(var tmp in model.AchievementInfo)
        {
            var tmpServerAchievementModel = new ServerAchievementModel();

            tmpServerAchievementModel.Id = tmp.Id;
            tmpServerAchievementModel.Count = tmp.Count;
            tmpServerAchievementModel.Goal = tmp.Goal;
            tmpServerAchievementModel.Level = tmp.Level;
            tmpServerAchievementModel.IsMaxLevel = tmp.IsMaxLevel;

            toReturn.AchievementInfo.Add(tmpServerAchievementModel);
        }

        toReturn.LastSummonLogCount = model.LastSummonLogCount;
        toReturn.LastFreeGemRewardLogCount = model.LastFreeGemRewardLogCount;
        toReturn.BuffAndRestRewardCount = model.BuffAndRestRewardCount;

        toReturn.UnsyncedGemChangeCount = model.UnsyncedGemChangeCount;

        return toReturn;
    }
}
