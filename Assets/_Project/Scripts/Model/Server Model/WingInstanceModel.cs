﻿using System;
using System.Linq;
using System.Collections.Generic;

[Serializable]
public class WingInstanceModel : ModelBase
{
    public string WingId;

	public bool IsEquipment;
    
    public WingOwnedStatBonusModel WingOwnedStatBonusInfo;
    public string OwnedStatBonus1Rank{
        get{
            return WingOwnedStatBonusInfo.Stat1Rank;
        }
    }
    public int OwnedStatBonus1SubRank{
        get{
            return WingOwnedStatBonusInfo.Stat1SubRank;
        }
    }
    public string OwnedStatBonus2Rank{
        get{
            return WingOwnedStatBonusInfo.Stat2Rank;
        }
    }
    public int OwnedStatBonus2SubRank{
        get{
            return WingOwnedStatBonusInfo.Stat2SubRank;
        }
    }

    public WingEquipmentStatBonusModel WingEquipmentStatBonusInfo;
    public string EquipmentStatBonus1Rank{
		get{
			return WingEquipmentStatBonusInfo.Stat1Rank;
		}
	}
	public string EquipmentStatBonus2Rank{
		get{
			return WingEquipmentStatBonusInfo.Stat2Rank;
		}
	}
	public string EquipmentStatBonus3Rank{
		get{
			return WingEquipmentStatBonusInfo.Stat3Rank;
		}
	}

    // Generated values
	public int OwnedStatBonus1Value{
		get{
			var ownedStatBonus1Info = BalanceInfoManager.Instance.WingInfoList.FirstOrDefault(n => n.Rank == WingOwnedStatBonusInfo.Stat1Rank);
			
            if(ownedStatBonus1Info != null)
                return ownedStatBonus1Info.GetOwnedStatBonus1Value(OwnedStatBonus1SubRank);
            else
                return 0;
		}
	}

	public int OwnedStatBonus2Value{
		get{
			var ownedStatBonus2Info = BalanceInfoManager.Instance.WingInfoList.FirstOrDefault(n => n.Rank == WingOwnedStatBonusInfo.Stat2Rank);
			
            if(ownedStatBonus2Info != null)
                return ownedStatBonus2Info.GetOwnedStatBonus2Value(OwnedStatBonus2SubRank);
            else
                return 0;
		}
	}

	public int EquipmentStatBonus1Value{
		get{
            var wingInfo = BalanceInfoManager.Instance.WingInfoList.FirstOrDefault(n => n.Rank == WingEquipmentStatBonusInfo.Stat1Rank);
			
			if(wingInfo != null)
				return wingInfo.EquipmentStatBonus1;
            else
				return 0;
		}
	}
	public int EquipmentStatBonus2Value{
		get{
            var wingInfo = BalanceInfoManager.Instance.WingInfoList.FirstOrDefault(n => n.Rank == WingEquipmentStatBonusInfo.Stat2Rank);

			if(wingInfo != null)
                return wingInfo.EquipmentStatBonus2;
            else
                return 0;
		}
	}
	public int EquipmentStatBonus3Value{
		get{
            var wingInfo = BalanceInfoManager.Instance.WingInfoList.FirstOrDefault(n => n.Rank == WingEquipmentStatBonusInfo.Stat3Rank);

			if(wingInfo != null)
                return wingInfo.EquipmentStatBonus3;
            else
                return 0;
		}
	}

	public WingInstanceModel(string wingId, WingOwnedStatBonusModel wingOwnedStatBonusInfo, WingEquipmentStatBonusModel wingEquipmentStatBonusInfo)
	{
		this.WingId = wingId;
		this.WingOwnedStatBonusInfo = wingOwnedStatBonusInfo;
		this.WingEquipmentStatBonusInfo = wingEquipmentStatBonusInfo;
		this.IsEquipment = GetEquipmentValue(WingId);
	}

	public WingInstanceModel(string wingId, Dictionary<string, string> customData)
	{
		this.WingId = wingId;

		var tmpWingOwnedStatBonusModel = new WingOwnedStatBonusModel();
		tmpWingOwnedStatBonusModel.Stat1Rank = "E";
		tmpWingOwnedStatBonusModel.Stat1SubRank = 0;
		tmpWingOwnedStatBonusModel.Stat2Rank = "E";
		tmpWingOwnedStatBonusModel.Stat2SubRank = 0;

		var tmpWingEquipmentStatBonusModel = new WingEquipmentStatBonusModel();
		tmpWingEquipmentStatBonusModel.Stat1Rank = "E";
		tmpWingEquipmentStatBonusModel.Stat2Rank = "E";
		tmpWingEquipmentStatBonusModel.Stat3Rank = "E";

		this.WingOwnedStatBonusInfo = tmpWingOwnedStatBonusModel;
		this.WingEquipmentStatBonusInfo = tmpWingEquipmentStatBonusModel;

		foreach(var tmp2 in customData)
		{
			if(tmp2.Key == "OwnedStatInfo")
			{
				this.WingOwnedStatBonusInfo = new WingOwnedStatBonusModel().FromJson(Convert.ToString(tmp2.Value)) as WingOwnedStatBonusModel;
			}
			else if(tmp2.Key == "EquipmentStatInfo")
			{
				this.WingEquipmentStatBonusInfo = new WingEquipmentStatBonusModel().FromJson(Convert.ToString(tmp2.Value)) as WingEquipmentStatBonusModel;
			}
		}

		this.IsEquipment = GetEquipmentValue(WingId);
	}

	private bool GetEquipmentValue(string wingId)
	{
		var basicIdFormat = "00";
		var convertedWingIndex = ServerNetworkManager.Instance.Inventory.WingIndex + 1;

		if(convertedWingIndex < 1)
			basicIdFormat = "00";
		else if(convertedWingIndex < 10)
			basicIdFormat = $"0{convertedWingIndex}";
		else
			basicIdFormat = $"{convertedWingIndex}";

		var covertedWingId = $"wing_{basicIdFormat}";

		if(covertedWingId == wingId)
			return true;
		else
			return false;
	}
}