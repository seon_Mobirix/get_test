﻿using System;

[Serializable]
public class DailyRewardModel : ModelBase
{
    public string ItemId;
    public string ItemCode;
    public int ItemCount;
}