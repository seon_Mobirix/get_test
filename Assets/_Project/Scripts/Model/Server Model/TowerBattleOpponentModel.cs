﻿using System;

[Serializable]
public class TowerBattleOpponentModel : ModelBase
{
    public string PlayFabId;
    public string NickName;
    public int Floor;
}