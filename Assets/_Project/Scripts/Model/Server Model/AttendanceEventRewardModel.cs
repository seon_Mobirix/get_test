﻿using System;

[Serializable]
public class AttendanceEventRewardModel : ModelBase
{
    public string ItemId;
    public string ItemCode;
    public int ItemCount;
}