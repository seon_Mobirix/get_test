﻿using System;
using UnityEngine;
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;

[Serializable]
public class WingBodyInstanceModel : ModelBase
{
    public ObscuredBool IsOpen = false;
    public ObscuredInt Level = 0;

    public WingBodyInstanceModel(bool isOpen, int level)
	{
        this.IsOpen = isOpen;
		this.Level = level;
	}

    public WingBodyInstanceModel(bool isOpen, Dictionary<string, string> customData)
	{
        this.IsOpen = isOpen;
		this.Level = 0; 

		if(customData != null)
        {
            foreach(var tmp in customData)
            {
                if(tmp.Key == "Level")
                {
                    this.Level = Mathf.Min(ServerNetworkManager.Instance.Setting.WingMaxLevel, Convert.ToInt32(tmp.Value));
                }
            }
        }
	}

    public int OwnedStatBonusValue{
		get{			
            return Mathf.Min(ServerNetworkManager.Instance.Setting.WingMaxLevel + 1, this.Level + 1);
		}
	}

    public int EquipmentStatBonusValue{
		get{			
            return Mathf.Min(ServerNetworkManager.Instance.Setting.WingMaxLevel, this.Level);
		}
	}

    public int UpgradeSuccessPercentage{
        get{
            return Mathf.Max(100 - this.Level, 20);
        }
    }

    public void UpdateWingBodyLevel(int updatedLevel)
	{
		this.Level = Mathf.Min(ServerNetworkManager.Instance.Setting.WingMaxLevel, updatedLevel);
	}
}