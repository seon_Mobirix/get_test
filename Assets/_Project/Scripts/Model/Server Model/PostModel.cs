﻿using System;

[Serializable]

public class PostModel : ModelBase
{
   public int Id;
   public string Message;
   public PostRewardModel AttachedItem;
   public double ExpiredTime;
}
