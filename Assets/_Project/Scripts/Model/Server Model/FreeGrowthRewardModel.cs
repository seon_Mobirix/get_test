﻿using System;

[Serializable]
public class FreeGrowthRewardModel : ModelBase
{
    public int RequireStage;
    public string ItemCode;
    public int ItemCount;
}
