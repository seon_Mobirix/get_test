﻿using System;

[Serializable]

public class NoticeModel : ModelBase
{
    public string Id;
    public string TimeText;
    public int TimeSeconds;
    public string TitleText;
    public string ContentText;
}
