﻿using System;
using System.Collections.Generic;

[Serializable]
public class FreeGrowthRewardListModel : ModelBase
{
    public List<FreeGrowthRewardModel> GrowthSupportFreeRewardList = new List<FreeGrowthRewardModel>();
}
