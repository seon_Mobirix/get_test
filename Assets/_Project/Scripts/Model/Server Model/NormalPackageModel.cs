﻿using System;

[Serializable]
public class NormalPackageModel : ModelBase
{
    public string Id;
	public string Name;
	public string PackageBuffValue;
	public int PackageBackgroundIndex = 0;
	public bool IsHot;
	public bool IsNew;
	public bool IsLimited;
	public bool IsAvailable;
	public int TotalPurchasablePerPeriod = 0;
	public int AvailInfoInPeriodIndex = -1;
	public int GemReward;
	public int FeatherReward;
	public int VIPReward;
	public int BonusStatReward;
	public string RealMoneyPrice;
	public GoodsPriceType PriceType;
}
