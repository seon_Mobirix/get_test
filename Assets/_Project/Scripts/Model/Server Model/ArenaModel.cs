﻿using System;
using System.Collections.Generic;
using System.Linq;

[Serializable]
public class ArenaModel : ModelBase {
    public string OpponentId = "";
    public string OpponentName = "";
    
    public int DailyLimitMaxCount = 10; // This value is hard-coded for performance
	public int DailyLimitLeft = 0;

    public int Season = 0;
	public int Position = -1;
	public int Score = 0;
    public int LastSeasonPosition = -1;
    public int LastSeasonScore = 0;
    public List<ArenaRankModel> rankList = new List<ArenaRankModel>();

    public int SeasonWeekly = 0;
    public int Star = -1;
    public int LastSeasonStar = -1;
    public int LastSeasonRewardGemWeekly = 0;

}