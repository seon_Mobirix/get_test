﻿using System;

[Serializable]
public class GemBundleModel: ModelBase
{
    public string ItemId;
    public int GivingAmount;
    public int VipReward;
    public GoodsPriceType PriceType;
    public int Price = 0;
    public string RealMoneyPrice = "";
}
