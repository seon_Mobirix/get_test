﻿using System;
using System.Collections.Generic;

[Serializable]
public class TowerFloorModel : ModelBase
{
    public int Floor = 0;
    public int MaxAllowedPlayersNumber = -1;
    public Dictionary<string, string> CurrentPlayers = new Dictionary<string, string>();
    public bool IsOccupy = false;
    public bool IsComplete = false;
}
