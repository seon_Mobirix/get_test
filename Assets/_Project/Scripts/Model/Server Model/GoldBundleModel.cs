﻿using System;

[Serializable]
public class GoldBundleModel : ModelBase
{
    public string ItemId;
    public int GivingAmount;
    public GoodsPriceType PriceType;
    public int Price = 0;
}