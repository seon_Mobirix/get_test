﻿using System;

[Serializable]
public class WingProductModel : ModelBase
{
    public string Id;
    public GoodsPriceType PriceType;
    public int Price = 0;
    public bool IsAvailable = false;
}