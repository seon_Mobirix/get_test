﻿using System;
using System.Collections.Generic;

[Serializable]
public class SettingModel : ModelBase
{
    public string AndroidAdmobMobirixRewardedKey = "";
	public string iOSAdmobRewardedKey = "";

    public bool IsMaintenance = false;
    public bool IsBeta = false;

    public int MultiDeviceCheckPeriod = 15;

    public int GetRankingPeriod = 600;

    public int AutoSyncPeriod = 15;

    public int IdleAutoSyncPeriod = 600;
  
    public int LoginCode = 0;
    public int MinLoginCode = 0;

    public string MinAndroidVersion = "0.0.0";
    public string MiniOSVersion = "0.0.0";
    public string ReviewingiOSVersion = "0.0.0";
    public int UnityAdsRatio = 0;
    public int MaxNoticeListCount = 0;

    public int MaxStageInWorld = 20;
    public int LastWorldIndex = 6;

    public int MaxStatLevel = 0;
    public int MaxSkillLevel = 0;
    public int MaxItemLevel = 0;
    public int MaxPetLevel = 0;
    public int MaxTrophyLevel = 0;
    public int MaxSummonLevel = 10;
    
    public int TotalClassCount = 21;
    public int TotalWeaponCount = 21;
    public int TotalPetCount = 11;

    public int RefundRelicPrice = 0;

    public int FreeGemMinAmount = 0;
    public int FreeGemMaxAmount = 0;
    public int FreeGemDailyLimit = 0;
    public int FreeGemCoolTime = 0;

    public int RequireVIPTicket = 0;
    public int VIPRewardGem = 0;
    
    public int MaxGrowthSupportStage = 0;

    // Arena
    public List<int> ArenaRewardList = new List<int>();
    public int ArenaSkipSize = 10;
    public List<int> ArenaWeeklyRewardList = new List<int>();
    public List<int> ArenaWeeklyRequiredStarsList = new List<int>();

    // Raid
    public int RaidGemReward = 0;
    public int RaidHeartReward = 0;
    public int BigRoomSizeForRaid = 8;
    public int RaidOpenStageProgress = 0;
    
    // Gem, Heart, Trophy
    public List<int> RaidRewardChances = new List<int>();

    // Legendary Relic
    public bool IsLegendaryRelicOpen = false;
    public int LegendaryRelicOpenStageProgress = 0;
    public int RefundLegendaryRelicPrice = 0;

    // Languages with Local Chat
    public List<string> LocalChatList = new List<string>(new string[]{"en", "zh_chs", "zh_cht", "ja", "vi", "th", "ru"});

    // Chat related settings
    public string ChatServerRegion = "EU";
    public int ChatBlockLimitCount = 100;
    
    // Mercenary
    public bool IsMercenaryAndroidOpen = true;
    public bool IsMercenaryiOSOpen = false;
    public int MercenaryOpenStageProgress = 0;
    public int TotalMercenaryCount = 15;
    public int MaxMercenaryLevel = 999;
    public int MaxMercenarySummonLevel = 7;
    public int MercenarySummonPrice = 50;

    // Tower
    public List<int> TowerSizes = new List<int>(new int[]{-1, 100, 90, 80, 70, 60, 50, 40, 30, 20, 10});
    public bool IsTowerAndroidOpen = true;
    public bool IsToweriOSOpen = true;
    public int TowerOpenStageProgress = 11;
    public int TowerDataRefreshPeriod = 600;

    public bool IsInBeta = true;

    // Wing
    public int WingOpenStageProgress = 11;
    public bool IsWingAndroidOpen = true;
    public bool IsWingiOSOpen = true;
    public List<int> WingResetStatPriceList = new List<int>(new int[]{50, 100, 200, 400, 800});
    public int WingUpgradePrice = 400;
    public int WingMaxLevel = 100;
    public int WingBodyOpenCount = 3;

     // Hyper
    public bool IsHyperOpen = true;

    // DPS For Arena
    public bool UseDPSForArena = false;

    // Guild
    public bool IsGuildBeta = true;
    public bool IsGuildAndroidOpen = false;
    public bool IsGuildiOSOpen = false;
    public int GuildOpenStageProgress = 61;
    public int GuildCreatePrice = 3000;
    public int GuildReJoinCoolTime = 172800;
    public int GuilFunctionCoolTime = 86400;
    public int FreeDonateEXPReward = 10;
    public int DonateEXPReward = 10;
    public int LimitGuildDonate = 1;
    public int MaxGuildDonateValue = 1000;
    public int GuildMaxLevel = 20;
    public List<int> GuildRaidInfoList = new List<int>(new int[]{2500, 2000, 1500, 1000, 750, 250, 50});
    public List<int> GuildRaidExpInfoList = new List<int>(new int[]{2500, 2000, 1500, 1000, 500});
    public List<int> GuildRaidTokenInfoList = new List<int>(new int[]{2500, 2000, 1500, 1000, 500});
    public List<int> GuildMemberCountList = new List<int>(new int[]{8, 9, 10, 11, 12, 13, 14, 15});

    public bool Use180Package = true;

    // Daily Reward
    public int DailyRewardHighlightValue;
}
