﻿using System;
using System.Collections.Generic;

[Serializable]
public class GrowthPackageModel : ModelBase
{
    public string Id;
	public string Name;
	public string PackageValue;
	public bool IsHot;
	public bool IsNew;
	public bool IsAvailable;
	public int GemReward;
	public int VIPReward;
	public List<GrowthRewardModel> GrowthRewardList;
	public string RealMoneyPrice;
	public GoodsPriceType PriceType;
}
