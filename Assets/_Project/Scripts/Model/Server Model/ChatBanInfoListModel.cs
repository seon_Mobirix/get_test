﻿using System;
using System.Collections.Generic;

[Serializable]
public class ChatBanInfoListModel : ModelBase
{
    public List<ChatBanInfoModel> BanList = new List<ChatBanInfoModel>();
}