﻿using System;

[Serializable]
public class ChatBanInfoModel : ModelBase
{
    public string Id = "";
    public double BanTime = 0;
}