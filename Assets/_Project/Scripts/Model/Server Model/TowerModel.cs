﻿using System;
using System.Collections.Generic;

[Serializable]
public class TowerModel : ModelBase
{   
    public bool IsDeployed = false;
    public bool IsCompleted = false;
    
    public int Floor = 0;
    public double DeployedTimestamp = 0;
    public double SecondLeft = 0;
    public float CollectedGems = 0;
    public float RewardRate = 0;
    public float CollectedFeathers = 0;
    public float RewardFeatherRate = 0;
    
    public List<TowerBattleLogModel> BattleLog = new List<TowerBattleLogModel>();
    
    public TowerBattleOpponentModel TowerBattleOpponent = new TowerBattleOpponentModel();
}