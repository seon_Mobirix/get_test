﻿using System;
using System.Collections.Generic;

[Serializable]
public class RankingModel : ModelBase
{
    public ArenaRankModel MyRankingModel;
    public List<ArenaRankModel> RankingList;
}