﻿using System;
using System.Collections.Generic;

[Serializable]
public class GuildApplicantListModel : ModelBase
{
    public List<GuildApplicantModel> ApplicantList = new List<GuildApplicantModel>();
}