using System;
using System.Collections.Generic;
using System.Linq;

using CodeStage.AntiCheat.ObscuredTypes;

[Serializable]
public class GuildRaidModel : ModelBase {
    public int DailyLimitMaxCount = 5; // This value is hard-coded for performance
	public ObscuredInt DailyLimitLeft = 0;

    public int Season = 0;
	public int Position = -1;
	public int Score = 0;
    public int LastSeasonPosition = -1;
    public int LastSeasonScore = 0;
    public List<ArenaRankModel> rankList = new List<ArenaRankModel>();
}
