﻿using System;
using System.Collections.Generic;

[Serializable]
public class GuildApplyListModel : ModelBase
{
    public List<string> ApplyGuildList = new List<string>();
}