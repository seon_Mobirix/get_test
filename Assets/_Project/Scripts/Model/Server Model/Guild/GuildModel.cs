﻿using System;

[Serializable]
public class GuildModel : ModelBase
{
    public GuildManagementModel GuildManagementData = new GuildManagementModel();
    public string GuildMasterName;

    public int Ranking;
    public int RankingScore;

    public int GuildLevel;
    public int GuildExp;
    public int GuildNextExp;

    public GuildMemberListModel GuildMemberData = new GuildMemberListModel();
    public GuildApplicantListModel ApplicantData = new GuildApplicantListModel();
    public GuildApplyListModel GuildApplyData = new GuildApplyListModel();

    public GuildRecommendationListModel RecommendedGuildData = new GuildRecommendationListModel();

    public int DailyLimitFreeDonateLeft = 0;
    public int DailyLimitDonateLeft = 0;

    public int CurrentServerTimeStamp;

    public int MaxMemeberCount
	{
        get{
            // Default value is 8
            int guildMemberLimit = ServerNetworkManager.Instance.Setting.GuildMemberCountList[0];

            // guild member limit depend on guild level (max 15)
            if(GuildLevel >= 15)
                guildMemberLimit = ServerNetworkManager.Instance.Setting.GuildMemberCountList[7];
            else if(GuildLevel >= 14)
                guildMemberLimit = ServerNetworkManager.Instance.Setting.GuildMemberCountList[6];
            else if(GuildLevel >= 13)
                guildMemberLimit = ServerNetworkManager.Instance.Setting.GuildMemberCountList[5];
            else if(GuildLevel >= 12)
                guildMemberLimit = ServerNetworkManager.Instance.Setting.GuildMemberCountList[4];
            else if(GuildLevel >= 9)
                guildMemberLimit = ServerNetworkManager.Instance.Setting.GuildMemberCountList[3];
            else if(GuildLevel >= 6)
                guildMemberLimit = ServerNetworkManager.Instance.Setting.GuildMemberCountList[2];
            else if(GuildLevel >= 3)
                guildMemberLimit = ServerNetworkManager.Instance.Setting.GuildMemberCountList[1];

            return guildMemberLimit;
        }
	}
}