﻿using System;
using System.Collections.Generic;

[Serializable]
public class GuildRelicListModel : ModelBase
{
    public List<GuildRelicModel> GuildRelicList = new List<GuildRelicModel>();
}