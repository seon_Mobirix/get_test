﻿using System;
using System.Collections.Generic;

[Serializable]
public class GuildKickMemberListModel : ModelBase
{
    public List<GuildKickMemberModel> KickMemberList = new List<GuildKickMemberModel>();
}