﻿using System;

[Serializable]
public class GuildRecommendationModel : ModelBase
{
    public string GuildId;
    public string GuildName;
    public string GuildNotice;
    public int GuildRanking;
    public int GuildLevel;
    public int GuildRequiredStageProgress;
    public int GuildFlagIndex;
    public int GuildMemberCount;
    public bool IsPrivate;
    public int MaxMemeberCount
	{
        get{
            // Default value is 8
            int guildMemberLimit = ServerNetworkManager.Instance.Setting.GuildMemberCountList[0];

            // guild member limit depend on guild level (max 15)
            if(GuildLevel >= 15)
                guildMemberLimit = ServerNetworkManager.Instance.Setting.GuildMemberCountList[7];
            else if(GuildLevel >= 14)
                guildMemberLimit = ServerNetworkManager.Instance.Setting.GuildMemberCountList[6];
            else if(GuildLevel >= 13)
                guildMemberLimit = ServerNetworkManager.Instance.Setting.GuildMemberCountList[5];
            else if(GuildLevel >= 12)
                guildMemberLimit = ServerNetworkManager.Instance.Setting.GuildMemberCountList[4];
            else if(GuildLevel >= 9)
                guildMemberLimit = ServerNetworkManager.Instance.Setting.GuildMemberCountList[3];
            else if(GuildLevel >= 6)
                guildMemberLimit = ServerNetworkManager.Instance.Setting.GuildMemberCountList[2];
            else if(GuildLevel >= 3)
                guildMemberLimit = ServerNetworkManager.Instance.Setting.GuildMemberCountList[1];

            return guildMemberLimit;
        }
	}
}
