﻿using System;

[Serializable]
public class GuildRankModel : ModelBase {
    public string GuildName;
    public string GuildMasterName;
    public int Position;
    public int Score;
}