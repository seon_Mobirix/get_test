﻿using System;
using System.Collections.Generic;

[Serializable]
public class GuildMemberListModel : ModelBase
{
    public List<GuildMemberModel> MemberCacheDataList = new List<GuildMemberModel>();
}
