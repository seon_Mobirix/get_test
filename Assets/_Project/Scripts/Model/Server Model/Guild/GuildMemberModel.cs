﻿using System;

[Serializable]
public class GuildMemberModel : ModelBase
{
    public string Id;
    public string Name;
    public int StageProgress;
    public int LastTimeStamp;
    public int TotalContribution;
    public int RaidScore;
}