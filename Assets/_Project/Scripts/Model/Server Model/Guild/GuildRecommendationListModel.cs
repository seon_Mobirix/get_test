﻿using System;
using System.Collections.Generic;

[Serializable]
public class GuildRecommendationListModel : ModelBase
{
    public List<GuildRecommendationModel> GuildDataList = new List<GuildRecommendationModel>();
}