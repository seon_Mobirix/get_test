﻿using System;

[Serializable]
public class GuildApplicantModel : ModelBase
{
    public string Id;
    public string Name;
    public int StageProgress;
}
