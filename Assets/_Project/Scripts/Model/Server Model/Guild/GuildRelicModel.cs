﻿using System;
using System.Globalization;
using CodeStage.AntiCheat.ObscuredTypes;

public enum GuildRelicType{
    GUILD_RAID_ATTACK,
    MERCENARY_ATTACK,
    RELIC_BONUS
}

[Serializable]
public class GuildRelicModel : ModelBase
{
    public string Id;
    public bool IsMaxLevel;
    public int Level;
    
    public GuildRelicType RelicType;
    private GuildRelicInfoModel infoModel = null;

    private int IntValueWithLevel{
        get{
            if(infoModel == null)
            {
                infoModel = BalanceInfoManager.Instance.GuildRelicInfoList.Find(n => n.Id == Id);
            }
            return int.Parse(infoModel.DefaultEffect) + int.Parse(infoModel.EffectPower) * Level;
        }
    }

    private float FloatValueWithLevel{
        get{
            if(infoModel == null)
            {
                infoModel = BalanceInfoManager.Instance.GuildRelicInfoList.Find(n => n.Id == Id);
            }
            return float.Parse(infoModel.DefaultEffect, NumberStyles.Any, CultureInfo.InvariantCulture) + float.Parse(infoModel.EffectPower, NumberStyles.Any, CultureInfo.InvariantCulture) * Level;
        }
    }

    public int MaxLevel{
        get{
            if(infoModel == null)
            {
                infoModel = BalanceInfoManager.Instance.GuildRelicInfoList.Find(n => n.Id == Id);
            }
            return infoModel.MaxLevel;
        }
    }

    public int GuildRaidAttackMultiplier{
        get{
            if(RelicType == GuildRelicType.GUILD_RAID_ATTACK)
                return this.IntValueWithLevel;
            else
                return 0;
        }
    }

    public float MercenaryAttackMultiplier{
        get{
            if(RelicType == GuildRelicType.MERCENARY_ATTACK)
                return this.FloatValueWithLevel;
            else
                return 0;
        }
    }

    public int NormalRelicBonus{
        get{
            if(RelicType == GuildRelicType.RELIC_BONUS)
                return this.Level;
            else
                return 0;
        }
    }

}