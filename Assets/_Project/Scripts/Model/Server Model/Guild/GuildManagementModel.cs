﻿using System;

[Serializable]
public class GuildManagementModel : ModelBase
{
    public string GroupId = "";
    public string GuildId = "";
    public string GuildName = "";
    public string GuildNotice = "";
    public int GuildFlagIndex = 0;
    public bool IsPrivate = false;
    public int RequiredStageProgress = 1;
    public GuildKickMemberListModel GuildKickMemberData = new GuildKickMemberListModel();    
}
