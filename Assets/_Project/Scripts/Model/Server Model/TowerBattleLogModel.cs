﻿using System;

[Serializable]
public class TowerBattleLogModel : ModelBase
{
   
    public string Name;

    public bool Win;

    public bool IsDefense;

    public int Floor;

    public int TimeStamp;

}