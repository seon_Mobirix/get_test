﻿using System;
using System.Collections.Generic;

[Serializable]
public class RewardListModel : ModelBase
{
     public List<RewardModel> ItemList = new List<RewardModel>();
}
