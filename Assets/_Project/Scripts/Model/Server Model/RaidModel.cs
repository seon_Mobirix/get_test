using System;
using System.Collections.Generic;
using System.Linq;

using CodeStage.AntiCheat.ObscuredTypes;

[Serializable]
public class RaidModel : ModelBase {
    public int DailyLimitMaxCount = 5; // This value is hard-coded for performance
	public ObscuredInt DailyLimitLeft = 0;
}
