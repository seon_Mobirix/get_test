﻿using System;
using UnityEngine;

[Serializable]
public class ModelBase{
    public string ToJson()
    {
        return JsonUtility.ToJson(this);
    }

    public ModelBase FromJson(string json)
    {
        JsonUtility.FromJsonOverwrite(json, this);
        return this;
    }
}
