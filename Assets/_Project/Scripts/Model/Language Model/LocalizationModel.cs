﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]

public class LocalizationModel : ModelBase
{
   public string Id;
   public string Tag;
   public string Korean;
   public string English;
   public string Japanese;
   public string ChineseSimplified;
   public string ChineseTraditional;
   public string German;
   public string Spanish;
   public string French;
   public string Vietnamese;
   public string Thai;
   public string Russian;
   public string Italian;
   public string Dutch;
}
