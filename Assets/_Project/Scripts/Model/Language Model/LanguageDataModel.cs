﻿using System;
using System.Collections.Generic;

public enum LanguageDataType {ENTITY, UI, NARRATIVE};

[Serializable]

public class LanguageDataModel : ModelBase
{
    public List<LocalizationModel> Entity;

    public List<LocalizationModel> UI;

    public List<LocalizationModel> Narrative;
}
