﻿using UnityEngine;
using System.Collections;

namespace QuickPool
{
    public class DespawnIn : MonoBehaviour
    {
        private Coroutine despawnCoroutine = null;        
        public float time = 0f;
        private float passed = 0f;

        public void OnSpawn()
        {
            despawnCoroutine = StartCoroutine(DespawnCoroutine());
        }

        private IEnumerator DespawnCoroutine()
        {
            while(true)
            {
                if(passed >= time)
                    break;
                passed += Time.deltaTime;
                yield return -1;
            }
            passed = 0f;
            
            this.transform.SetParent(null);
            gameObject.Despawn();
        }

        public void DespawnImmediate()
        {
            if(despawnCoroutine != null)
                StopCoroutine(despawnCoroutine);
            despawnCoroutine = null;
            gameObject.Despawn();
        }
    }
}