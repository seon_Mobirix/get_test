﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class HeroAnimation : MonoBehaviour
{

    private string currentWeaponId = "";

    private string currentClassId = "";

    [SerializeField]
    private Animator animator;

    [SerializeField]
    private NavMeshAgent navMeshAgent;

    [SerializeField]
    private List<ParticleSystem> defaultAttackEffectList;
    private int defaultAttackEffectIndex = 0;

    [SerializeField]
    private List<AudioClip> defaultAttackSoundList = new List<AudioClip>();

    [SerializeField]
    private List<AudioClip> impactSoundList = new List<AudioClip>();

    [SerializeField]
    private List<ParticleSystem> skillAttackEffectList;

    [SerializeField]
    private List<AudioClip> skillAttackSoundList = new List<AudioClip>();

    [SerializeField]
    private HeroIdentity heroIdentity;

    [SerializeField]
    private Transform characterVisual = null;

    [SerializeField]
    private Transform feverVisual = null;

    [SerializeField]
    private MeshRenderer weaponVisual = null;
    
    [SerializeField]
    private AudioSource audioSourceAttack = null;

    [SerializeField]
    private AudioSource audioSourceImpact = null;

    [SerializeField]
    private List<Transform> wingList = new List<Transform>();

    public float MoveSpeed{
        get{
            return navMeshAgent.speed;
        }
    }

    [SerializeField]
    private bool isApproaching = false;
    public bool IsApproaching
    {
        get{
            return isApproaching;
        }
    }

    private bool isDying = false;

    public void Reset(Vector3 spawnPosition)
    {
        navMeshAgent.SetDestination(spawnPosition);
        navMeshAgent.Warp(spawnPosition);

        PlayIdleAnimation();
    }

    public void AdjustMoveSpeed()
    {
        // Get legndary relic effect
        var targetLegendaryRelic = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.LegendaryRelicType == LegendaryRelicType.MOVEMENT_SPEED);
        var movmentSpeedMutliplierByLegendaryRelic = (targetLegendaryRelic != null)? (1f + targetLegendaryRelic.MovementSpeedIncreasePercent / 100f) : 1f;

        navMeshAgent.speed = 8 * movmentSpeedMutliplierByLegendaryRelic;
    }

    public void RefreshVisual(string classId, string weaponId, int wingIndex, bool forceChange = false)
    {
        if(classId != "" && (currentClassId != classId || forceChange))
        {
            if(characterVisual != null)
            {
                Destroy(characterVisual.gameObject);
                characterVisual = null;
                weaponVisual = null;
            }

            characterVisual = Instantiate(Resources.Load<Transform>("Heroes/Visuals/" + "C" + classId.Substring(1)));
            characterVisual.parent = this.transform;
            characterVisual.localRotation = Quaternion.identity;
            characterVisual.localPosition = Vector3.zero;
            animator = characterVisual.GetComponent<Animator>();

            // Also need to import weapon
            if(weaponVisual == null)
                weaponVisual = characterVisual.GetComponentInChildren<WeaponVisual>().GetComponent<MeshRenderer>();

            var newWeaponSprite = Resources.Load<Texture>("Weapons/Visuals/W" + weaponId.Substring(1));
            weaponVisual.material.SetTexture("_MainTex", newWeaponSprite);

            // Change wings
            for(int i = 0; i < wingList.Count; i++)
            {
                wingList[i].parent = this.transform;
                wingList[i].gameObject.SetActive(false);
            }
            
            if(wingIndex != -1 && wingIndex < wingList.Count)
            {
                foreach(var tmp in characterVisual.GetComponentsInChildren<Transform>())
                if(tmp != null && tmp.name == "fx_center")
                {
                    wingList[wingIndex].parent = tmp;
                    wingList[wingIndex].gameObject.SetActive(true);
                    wingList[wingIndex].localPosition = new Vector3(-0.4f, -0.75f, 0f);
                    wingList[wingIndex].localRotation = Quaternion.Euler(0f, -90f, -180f);
                }
            }
        }
        currentClassId = classId;

        if(weaponId != "" && (currentWeaponId != weaponId || forceChange))
        {
            if(weaponVisual == null)
                weaponVisual = characterVisual.GetComponentInChildren<WeaponVisual>().GetComponent<MeshRenderer>();

            var newWeaponSprite = Resources.Load<Texture>("Weapons/Visuals/W" + weaponId.Substring(1));
            weaponVisual.material.SetTexture("_MainTex", newWeaponSprite);

            defaultAttackEffectIndex = Mathf.Min(int.Parse(weaponId.Substring(7)) - 1, defaultAttackEffectList.Count - 1);
        }
        currentWeaponId = weaponId;
    }

    Coroutine approchTargetCoroutine = null;
    public void ApproachTargetMob()
    {
        if(approchTargetCoroutine != null)
            StopCoroutine(approchTargetCoroutine);

        approchTargetCoroutine = StartCoroutine(ApproachTargetMobCoroutine());
    }

    private IEnumerator ApproachTargetMobCoroutine()
    {
        isApproaching = true;

        this.navMeshAgent.stoppingDistance = 5f;

        while(Vector3.Distance(this.transform.position, heroIdentity.TargetMobIdentity.transform.position) > 5f)
        {
            this.navMeshAgent.SetDestination(heroIdentity.TargetMobIdentity.transform.position);
            PlayRunAnimation();
            yield return new WaitForSeconds(0.25f);
        }

        PlayIdleAnimation();

        isApproaching = false;
    }

    public void ApproachTargetHero()
    {
        if(approchTargetCoroutine != null)
            StopCoroutine(approchTargetCoroutine);

        approchTargetCoroutine = StartCoroutine(ApproachTargetHeroCoroutine());
    }

    private IEnumerator ApproachTargetHeroCoroutine()
    {
        isApproaching = true;

        this.navMeshAgent.stoppingDistance = 5f;

        while(Vector3.Distance(this.transform.position, heroIdentity.TargetHeroIdentity.transform.position) > 5f)
        {
            this.navMeshAgent.SetDestination(heroIdentity.TargetHeroIdentity.transform.position);
            PlayRunAnimation();
            yield return new WaitForSeconds(0.25f);
        }

        PlayIdleAnimation();

        isApproaching = false;
    }

    public void ApproachTarget(Vector3 destination)
    {
        if(approchTargetCoroutine != null)
            StopCoroutine(approchTargetCoroutine);
            
        approchTargetCoroutine = StartCoroutine(ApproachTargetCoroutine(destination));
    }

    private IEnumerator ApproachTargetCoroutine(Vector3 destination)
    {
        isApproaching = true;
        
        this.navMeshAgent.stoppingDistance = 0f;

        bool isInitialLoop = true;
        while(Vector3.Distance(this.transform.position, destination) > 1f && (isInitialLoop || this.navMeshAgent.velocity.magnitude >= 0.1f))
        {
            this.navMeshAgent.SetDestination(destination);
            PlayRunAnimation();
            isInitialLoop = false;

            yield return new WaitForSeconds(0.25f);
        }

        PlayIdleAnimation();

        isApproaching = false;
    }

    public void ApproachStop()
    {
        if(approchTargetCoroutine != null)
            StopCoroutine(approchTargetCoroutine);

        this.navMeshAgent.SetDestination(this.transform.position);

        PlayIdleAnimation();

        isApproaching = false;
    }

    public void PlayIdleAnimation()
	{
        if(animator == null || isDying)
            return;

        animator.speed = 1f;

        animator.ResetTrigger("Die");
        animator.SetTrigger("Idle");
        animator.ResetTrigger("Run");
        animator.ResetTrigger("Attack");
        animator.ResetTrigger("Skill 1");
        animator.ResetTrigger("Skill 2");
        animator.ResetTrigger("Skill 3");
	}

    public void PlayDieAnimation()
	{
        if(animator == null || isDying)
            return;

        isDying = true;

        animator.speed = 1f;

        animator.SetTrigger("Die");
        animator.ResetTrigger("Idle");
        animator.ResetTrigger("Run");
        animator.ResetTrigger("Attack");
        animator.ResetTrigger("Skill 1");
        animator.ResetTrigger("Skill 2");
        animator.ResetTrigger("Skill 3");
	}

    public void PlayRunAnimation()
    {
        if(animator == null || isDying)
            return;

        animator.speed = 1f;
        animator.SetTrigger("Run");
    }

    public void PlayDefaultAttackAnimation(float speed)
	{
        if(animator == null || isDying)
            return;

        animator.speed = speed;
        animator.SetTrigger("Attack");

        StartCoroutine(PlayAttackEffect(speed));
	}

    private IEnumerator PlayAttackEffect(float speed)
    {
        yield return new WaitForSeconds(0.18f / speed);
        defaultAttackEffectList[defaultAttackEffectIndex].Play();
    }

    public void PlaySkill1AttackAnimation(float speed)
	{
        if(animator == null || isDying)
            return;

        animator.speed = speed;
        animator.SetTrigger("Skill 1");

        StartCoroutine(PlaySkill1Effect(speed));
	}

    private IEnumerator PlaySkill1Effect(float speed)
    {
        var duration = 0.25f / speed;

        yield return new WaitForSeconds(duration);

        var passed = 0f;
        while(passed < duration)
        {
            characterVisual.transform.localPosition = Vector3.Lerp(characterVisual.transform.localPosition, Vector3.up * 4.5f, Mathf.Pow(passed / duration, 1.5f));
            feverVisual.transform.localPosition = Vector3.Lerp(feverVisual.transform.localPosition, Vector3.up * 4.5f, Mathf.Pow(passed / duration, 1.5f));
            passed += Time.deltaTime;
            yield return -1;
        }
        characterVisual.transform.localPosition = Vector3.up * 4.5f;
        feverVisual.transform.localPosition = Vector3.up * 4.5f;

        skillAttackEffectList[0].Play();

        passed = 0f;
        while(passed < duration)
        {
            characterVisual.transform.localPosition = Vector3.Lerp(characterVisual.transform.localPosition, Vector3.zero, Mathf.Pow(passed / duration, 2f));
            feverVisual.transform.localPosition = Vector3.Lerp(feverVisual.transform.localPosition, Vector3.zero, Mathf.Pow(passed / duration, 2f));
            passed += Time.deltaTime;
            yield return -1;
        }
        characterVisual.transform.localPosition = Vector3.zero;
        feverVisual.transform.localPosition = Vector3.zero;
        
        audioSourceAttack.PlayOneShot(skillAttackSoundList[0]);
    }

    public void PlaySkill2AttackAnimation(float speed)
	{
        if(animator == null || isDying)
            return;

        animator.speed = speed;
        animator.SetTrigger("Skill 2");

        StartCoroutine(PlaySkill2Effect(speed));
	}

    private IEnumerator PlaySkill2Effect(float speed)
    {
        audioSourceAttack.PlayOneShot(skillAttackSoundList[1]);
        yield return new WaitForSeconds(0.18f / speed);
        skillAttackEffectList[1].Play();
    }

    public void PlaySkill3AttackAnimation(float speed)
	{
        if(animator == null || isDying)
            return;

        animator.speed = speed;
        animator.SetTrigger("Skill 3");

        StartCoroutine(PlaySkill3Effect(speed));
	}

    private IEnumerator PlaySkill3Effect(float speed)
    {
        audioSourceAttack.PlayOneShot(skillAttackSoundList[2]);
        yield return new WaitForSeconds(1f / speed);
        skillAttackEffectList[2].Play();
    }

    public void PlayImpactSound()
    {
        audioSourceImpact.clip = impactSoundList[Random.Range(0, impactSoundList.Count)];
        audioSourceImpact.Play();
    }

    public void PlayNoImpactSound()
    {
        audioSourceAttack.clip = defaultAttackSoundList[Random.Range(0, defaultAttackSoundList.Count)];
        audioSourceAttack.Play();
    }

    public void SetFeverEffect(bool value)
    {
        feverVisual.gameObject.SetActive(value);
    }
}
