﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RaidBossAnimation : MonoBehaviour
{
    [SerializeField]
    private Animator animator;

    [SerializeField]
    private AudioSource attackAudioSource = null;

    [SerializeField]
    private AudioSource dieAudioSource = null;

    public void PlayIdleAnimation()
    {
        animator.ResetTrigger("Die");
        animator.SetTrigger("Idle");
        animator.ResetTrigger("Run");
        animator.ResetTrigger("Attack");
    }

    public void PlayAttackAnimation()
    {
        animator.SetTrigger("Attack");
        attackAudioSource.Play();
    }

    public void PlayDieAnimation()
    {
        animator.SetTrigger("Die");
        dieAudioSource.Play();
    }
}
