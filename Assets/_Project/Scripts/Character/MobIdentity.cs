﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using System.Linq;
using UnityEngine;

public class MobIdentity : MonoBehaviour
{
    public BigInteger MaxHP;
    public BigInteger HP;
    public BigInteger Bounty;

    [SerializeField]
    private HeroIdentity heroIdentity;

    [SerializeField]
    private MobAnimation mobAnimation;
    public MobAnimation MobAnimation { 
        get { 
            return mobAnimation; 
        }
    }

    public bool IsDying = false;

    public float LeftHPRatio
    {
        get{
            if(HP > 0)
                return (float)Math.Exp(BigInteger.Log(HP) - BigInteger.Log(MaxHP));
            else
                return 0f;
        }
    }

    public bool IsFullHP
    {
        get{
            return (HP == MaxHP);
        }
    }

    private Coroutine resetCoroutine = null;
    public void ResetNormal(float delay = 0f)
    {
        if(resetCoroutine != null)
            StopCoroutine(resetCoroutine);

        resetCoroutine = StartCoroutine(ResetNormalCoroutine(delay));
    }

    private IEnumerator ResetNormalCoroutine(float delay)
    {
        if(delay != 0f)
            yield return new WaitForSeconds(delay);

        IsDying = false;

        MaxHP = NumberMainController.Instance.FinalMonsterHP;
        HP = MaxHP;
        Bounty = NumberMainController.Instance.FinalGoldIncome;

        var worldIndex = (ServerNetworkManager.Instance.User.SelectedStage - 1) / ServerNetworkManager.Instance.Setting.MaxStageInWorld;
        worldIndex %= 5;

        this.transform.localScale = new UnityEngine.Vector3(1f, 1f, 1f);

        var mobList = IngameMainController.Instance.SpawnMobList[worldIndex].MobList;
        mobAnimation.RefreshVisual(mobList[UnityEngine.Random.Range(0, mobList.Count)]);

        mobAnimation.Reset(heroIdentity.transform, GetSpawnPosition(), false);
    }

    public void ResetBoss()
    {
        if(resetCoroutine != null)
            StopCoroutine(resetCoroutine);
        
        IsDying = false;
        
        MaxHP = NumberMainController.Instance.FinalBossHP;
        HP = MaxHP;
        Bounty = NumberMainController.Instance.FinalGoldIncome * 10;

        this.transform.localScale = new UnityEngine.Vector3(1.75f, 1.75f, 1.75f);
        
        if(UnityEngine.Vector3.Distance(mobAnimation.transform.position, heroIdentity.transform.position) > 5f)
            mobAnimation.Reset(heroIdentity.transform, heroIdentity.transform.position + heroIdentity.transform.forward * 5f, true);
        else
            mobAnimation.Reset(heroIdentity.transform, mobAnimation.transform.position, true);
    }

    public bool ReceiveAttack(BigInteger damage)
    {
        HP -= damage;

        var didKill = (HP <= 0);

        if(HP > 0)
            mobAnimation.PlayHitAnimation();
        
        IngameMainController.Instance.OnMonsterHit();

        return didKill;
    }

    private UnityEngine.Vector3 GetSpawnPosition()
    {
        var distantDictionary = new Dictionary<Transform, float>();
        foreach(var tmp in IngameMainController.Instance.SpawnPositionList)
        {
            // Avoid other mobs
            var distantTotal = 0f;
            foreach(var tmp2 in IngameMainController.Instance.MobIdentityList)
            {
                distantTotal += 1f / Mathf.Pow(UnityEngine.Vector3.Distance(tmp.transform.position, tmp2.transform.position), 2f);   
            }

            // Also above the hero
            distantTotal += 1f / Mathf.Pow(UnityEngine.Vector3.Distance(tmp.transform.position, heroIdentity.transform.position), 2f);

            distantDictionary[tmp] = distantTotal;
        }
        
        // Randomly choose from some secluded places
        var orderedList = distantDictionary.OrderBy(n => n.Value).ToList();
        return orderedList[UnityEngine.Random.Range(0, orderedList.Count / 4)].Key.position;
    }

}
