﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MercenaryAnimation : MonoBehaviour
{
    private string currentMercenaryId = "";

    [SerializeField]
    private Transform mercenaryVisual = null;

    [SerializeField]
    private Animator animator;

    [SerializeField]
    private NavMeshAgent navMeshAgent;

    [SerializeField]
    private HeroIdentity targetHero = null;

    [SerializeField]
    private List<Projectile> defaultAttackEffectList;
    private int defaultAttackEffectIndex = 0;

    [SerializeField]
    private List<AudioClip> defaultAttackSoundList = new List<AudioClip>();

    [SerializeField]
    private List<AudioClip> impactSoundList = new List<AudioClip>();
    
    [SerializeField]
    private AudioSource audioSourceAttack = null;

    [SerializeField]
    private AudioSource audioSourceImpact = null;

    private void Update()
    {
        if(targetHero != null && targetHero.TargetMobIdentity != null && this.navMeshAgent.velocity.magnitude < 0.1f)
            this.transform.localRotation = Quaternion.Slerp(this.transform.localRotation, Quaternion.LookRotation(targetHero.TargetMobIdentity.transform.position - this.transform.position, Vector3.up), 1f * Time.deltaTime);
    }

    public void Reset(bool resetPosition)
    {
        if(resetPosition)
            navMeshAgent.Warp(targetHero.transform.position - Vector3.forward * 1f);
        
        PlayIdleAnimation();
        
        StopAllCoroutines();
        ApproachTarget();
    }

    public void RefreshVisual(string mercenaryId)
    {
        if(mercenaryId != "" && currentMercenaryId != mercenaryId)
        {
            if(mercenaryVisual != null)
            {
                Destroy(mercenaryVisual.gameObject);
                mercenaryVisual = null;
            }

            mercenaryVisual = Instantiate(Resources.Load<Transform>("Mercenaries/Visuals/" + "M" + mercenaryId.Substring(1)));
            mercenaryVisual.parent = this.transform;
            mercenaryVisual.localRotation = Quaternion.identity;
            mercenaryVisual.localPosition = Vector3.zero;
            animator = mercenaryVisual.GetComponent<Animator>();

            defaultAttackEffectIndex = Mathf.Min(int.Parse(mercenaryId.Substring(10)) - 1, defaultAttackEffectList.Count - 1);
        }
        
        currentMercenaryId = mercenaryId;
    }

    public void ApproachTarget()
    {
        StartCoroutine(ApproachTargetCoroutine());
    }

    private IEnumerator ApproachTargetCoroutine()
    {
        while(true)
        {
            if(targetHero.TargetMobIdentity != null)
            {
                var distance = Vector3.Distance(this.transform.position, targetHero.TargetMobIdentity.transform.position);

                if(distance > 15f)
                {
                    this.navMeshAgent.SetDestination(targetHero.TargetMobIdentity.transform.position);
                    PlayRunAnimation();
                    yield return new WaitForSeconds(0.25f);
                }
                else
                {
                    PlayIdleAnimation();
                    yield return new WaitForSeconds(2f);
                }
            }
        }   
    }

    public void PlayIdleAnimation()
	{
        if(animator == null)
            return;

        animator.speed = 1f;

        animator.ResetTrigger("Die");
        animator.SetTrigger("Idle");
        animator.ResetTrigger("Run");
        animator.ResetTrigger("Attack");
	}

    public void PlayRunAnimation()
    {
        if(animator == null)
            return;

        animator.speed = 1f;
        animator.SetTrigger("Run");
    }

    public void PlayDefaultAttackAnimation(Transform target, float speed)
	{
        if(animator == null)
            return;

        animator.speed = speed;
        animator.SetTrigger("Attack");

        StartCoroutine(PlayAttackEffect(target, speed));
	}

    private IEnumerator PlayAttackEffect(Transform target, float speed)
    {
        yield return new WaitForSeconds(0.18f / speed);
        var instance = QuickPool.PoolsManager.Spawn(defaultAttackEffectList[defaultAttackEffectIndex].gameObject, this.gameObject.transform.position, Quaternion.identity);
        instance.GetComponent<Projectile>().SetProjectile(this.transform.position + Vector3.up + this.transform.forward, target);
    }
}
