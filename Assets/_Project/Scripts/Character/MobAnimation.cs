﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

using DG.Tweening;

public class MobAnimation : MonoBehaviour
{   
    private string currentMobId = "";

    [SerializeField]
    private Transform visual;

    [SerializeField]
    private Animator animator;

    [SerializeField]
    private List<AudioClip> defaultAttackSoundList = new List<AudioClip>();

    [SerializeField]
    private List<AudioClip> bossSoundList = new List<AudioClip>();

    [SerializeField]
    private NavMeshAgent navMeshAgent;

    [SerializeField]
    private MobIdentity mobIdentity;

    private Transform targetHero = null;

    [SerializeField]
    private AudioSource audioSource = null;

    [SerializeField]
    private ParticleSystem bossParticle = null;

    [SerializeField]
    private bool isApproaching = false;
    public bool IsApproaching
    {
        get{
            return isApproaching;
        }
    }

    private void Update()
    {
        if(targetHero != null && mobIdentity.HP > 0 && this.navMeshAgent.velocity.magnitude < 0.1f && (targetHero.position - this.transform.position) != UnityEngine.Vector3.zero)
            this.transform.localRotation = Quaternion.Slerp(this.transform.localRotation, Quaternion.LookRotation(targetHero.position - this.transform.position, Vector3.up), 1f * Time.deltaTime);
    }

    public void Reset(Transform target, Vector3 spawnPosition, bool isBoss)
    {
        this.targetHero = target;

        if (navMeshAgent_radius != 0.0f)
            navMeshAgent.radius = navMeshAgent_radius;

        navMeshAgent.SetDestination(this.transform.position);
        navMeshAgent.Warp(spawnPosition);
        navMeshAgent.isStopped = false;
        
        PlayIdleAnimation();

        StopAllCoroutines();
        ApproachTarget();

        if(isBoss)
        {
            audioSource.PlayOneShot(bossSoundList[0]);
            bossParticle.Play();
        }
        else
        {
            if(bossParticle.isPlaying)
                bossParticle.Stop();
        }
    }

    public void RefreshVisual(string id)
    {
        if(visual != null)
        {
            Destroy(visual.gameObject);
            visual = null;
        }
        
        var resourceToLoad = Resources.Load<Transform>("Mobs/Visuals/" + id);
		visual = Instantiate(resourceToLoad);
        visual.parent = this.transform;
        visual.localRotation = Quaternion.identity;
        visual.localPosition = Vector3.zero;
        visual.localScale = resourceToLoad.localScale;
        animator = visual.GetComponent<Animator>();
    }

    public void ApproachTarget()
    {
        StartCoroutine(ApproachTargetCoroutine());
    }

    private IEnumerator ApproachTargetCoroutine()
    {
        while(mobIdentity.HP > 0)
        {
            var distance = Vector3.Distance(this.transform.position, targetHero.position);

            if(distance <= 5f)
            {
                PlayAttackAnimation();
                yield return new WaitForSeconds(3f);
            }
            else if(distance < 37.5f)
            {
                this.navMeshAgent.SetDestination(targetHero.position);
                PlayRunAnimation();
                yield return new WaitForSeconds(0.5f);
            }
            else
            {
                PlayIdleAnimation();
                yield return new WaitForSeconds(0.5f);
            }
        }
    }

    public void PlayIdleAnimation()
	{
        animator.ResetTrigger("Run");
        animator.ResetTrigger("Attack");
        animator.ResetTrigger("Hit");
        animator.ResetTrigger("Die");

        animator.SetTrigger("Idle");
	}

    public void PlayAttackAnimation()
    {
        animator.SetTrigger("Attack");
        StartCoroutine(PlayAttackEffect());       
    }

    private IEnumerator PlayAttackEffect()
    {
        yield return new WaitForSeconds(0.3f);

        if(mobIdentity.HP > 0)
        {
            if(targetHero != null)
                EffectController.Instance.ShowHitEffect(targetHero.transform, false);

            audioSource.clip = defaultAttackSoundList[Random.Range(0, defaultAttackSoundList.Count)];
            audioSource.Play();
        }
    }

    public void PlayHitAnimation()
    {
        /*
        animator.SetTrigger("Hit");
        */
    }
    
    public void PlayRunAnimation()
    {
        animator.SetTrigger("Run");
    }
    float navMeshAgent_radius = 0.0f;
    public void PlayDieAnimation()
    {
        animator.ResetTrigger("Run");
        animator.ResetTrigger("Attack");
        animator.ResetTrigger("Hit");
        animator.ResetTrigger("Idle");

        animator.SetTrigger("Die");

        navMeshAgent.isStopped = true;

        navMeshAgent_radius = navMeshAgent.radius;
        navMeshAgent.radius = 0;
    }

}
