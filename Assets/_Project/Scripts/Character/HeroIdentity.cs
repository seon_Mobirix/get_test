﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using System.Linq;
using System.Globalization;
using UnityEngine;

using CodeStage.AntiCheat.ObscuredTypes;

public enum EMaxCriticalType
{
    None = 0,
    Critical,
    Super,
    Ultimate,
    Hyper,
}

public class HeroIdentity : MonoBehaviour
{

    private bool isAlive = true;

    private bool isAttacking = false;
    public bool IsAttacking{
        get{
            return isAttacking;
        }
    }

    private bool isInSkillAttack = false;
    public bool IsInSkillAttack{
        get{
            return isInSkillAttack;
        }
    }

    public bool IsRunning{
        get{
            return heroAnimation.IsApproaching;
        }
    }

    public float SpeedToUse{
        get{
            if(isForArena)
            {
                if(isMine)
                    return NumberArenaController.Instance.FinalAttackSpeed * BuffManager.Instance.GetSpeedBuffValue(ServerNetworkManager.Instance.User.SpeedBuffTier);
                else
                    return NumberArenaController.Instance.FinalAttackSpeedEnemy * BuffManager.Instance.GetSpeedBuffValue(InterSceneManager.Instance.OpponentUserData.SpeedBuffTier);
            }
            else if(isForRaid || isForGuildRaid)
            {
                return NumberRaidController.Instance.FinalAttackSpeed * BuffManager.Instance.GetSpeedBuffValue(ServerNetworkManager.Instance.User.SpeedBuffTier);
            }
            else
            {
                return NumberMainController.Instance.FinalAttackSpeed * BuffManager.Instance.GetSpeedBuffValue(ServerNetworkManager.Instance.User.SpeedBuffTier);
            }
        }
    }

    [SerializeField]
    private List<ObscuredFloat> skillCooldownList = new List<ObscuredFloat>();
    [SerializeField]
    private List<ObscuredFloat> skillCooldownPassed = new List<ObscuredFloat>();

    public float skill1Cooldown{
        get{
            return Mathf.Min(skillCooldownPassed[0] / skillCooldownList[0], 1f);
        }
    }
    public float skill2Cooldown{
        get{
            return Mathf.Min(skillCooldownPassed[1] / skillCooldownList[1], 1f);
        }
    }
    public float skill3Cooldown{
        get{
            return Mathf.Min(skillCooldownPassed[2] / skillCooldownList[2], 1f);
        }
    }

    private bool isMine = true;
    private bool isForArena = false;
    private bool isForRaid = false;
    private bool isForGuildRaid = false;

    [SerializeField]
    private MobIdentity targetMobIdentity;
    public MobIdentity TargetMobIdentity{ 
        get { 
            return targetMobIdentity; 
        }
    }

    [SerializeField]
    private HeroIdentity targetHeroIdentity;
    public HeroIdentity TargetHeroIdentity{
        get{
            return targetHeroIdentity;
        }
    }

    [SerializeField]
    private HeroAnimation heroAnimation;
    public HeroAnimation HeroAnimation{ 
        get { 
            return heroAnimation; 
        }
    }

    [SerializeField]
    private MercenaryAnimation mercenaryAnimation;

    [SerializeField]
    private UIViewFloatingHUD uiViewFloatingHUD;

    private void Update()
    {
        for(int i = 0; i < 3; i++)
        {
            if(skillCooldownList[i] > skillCooldownPassed[i])
                skillCooldownPassed[i] = Mathf.Min(skillCooldownPassed[i] + Time.deltaTime, skillCooldownList[i]);
        }
    }

    public void Reset(bool isMine, bool resetPosition, string classId, string weaponId, int wingIndex, bool isForArena = false, bool isForRaid = false, bool isForGuildRaid = false)
    {
        for(int i = 0; i < 3; i++)
        {
            if(skillCooldownList[i] > skillCooldownPassed[i])
                skillCooldownPassed[i] = skillCooldownList[i];
        }

        this.isMine = isMine;
        this.isForArena = isForArena;
        this.isForRaid = isForRaid;
        this.isForGuildRaid = isForGuildRaid;

        heroAnimation.RefreshVisual(classId, weaponId, wingIndex);

        if(resetPosition)
        {
            if(isForGuildRaid)
                heroAnimation.Reset(GetSpawnPositionForGuildRaid());
            else if(isForRaid)
                heroAnimation.Reset(GetSpawnPositionForRaid());
            else if(isForArena)
                heroAnimation.Reset(this.transform.position);
            else
                heroAnimation.Reset(GetSpawnPositionForMain());
        }
        else
        {
            heroAnimation.Reset(this.transform.position);
        }
    }

    public void Refresh(string classId, string weaponId, int wingIndex, bool forceChange = false)
    {
        heroAnimation.RefreshVisual(classId, weaponId, wingIndex, forceChange);
    }

    public void MoveWithDirection(UnityEngine.Vector3 direction)
    {
        if(direction != UnityEngine.Vector3.zero)
        {
            MoveToDestination(this.transform.position + direction * heroAnimation.MoveSpeed * 0.15f);
        }
    }

    private Coroutine moveCoroutine = null;

    public void MoveImmediately(UnityEngine.Vector3 destination)
    {
        if(moveCoroutine != null)
            StopCoroutine(moveCoroutine);

        heroAnimation.Reset(destination);
    }

    public void MoveStop()
    {
        if(moveCoroutine != null)
            StopCoroutine(moveCoroutine);

        heroAnimation.ApproachStop();
    }

    public void MoveToDestination(UnityEngine.Vector3 destination)
    {
        if(isAttacking || !isAlive)
            return;

        if(moveCoroutine != null)
            StopCoroutine(moveCoroutine);

        heroAnimation.ApproachTarget(destination);
    }
    
    public void AttackMobAfterMoveToMob()
    {
        if(isAttacking || IsRunning || !isAlive)
            return;

        if(moveCoroutine != null)
            StopCoroutine(moveCoroutine);

        moveCoroutine = StartCoroutine(AttackMobAfterMoveCoroutine());
    }

    private IEnumerator AttackMobAfterMoveCoroutine()
    {
        // Find the target monster to attack
        FindNextTarget();
        
        if(targetMobIdentity != null)
        {
            if(UnityEngine.Vector3.Distance(this.transform.position, targetMobIdentity.transform.position) > 5f)
                heroAnimation.ApproachTargetMob();
            else if(targetMobIdentity.transform.position - this.transform.position != UnityEngine.Vector3.zero)
                this.transform.localRotation = UnityEngine.Quaternion.LookRotation(targetMobIdentity.transform.position - this.transform.position);

            // Keep finding the nearst mob while approaching
            var approachingPassed = 0f;
            while(heroAnimation.IsApproaching)
            {
                if(approachingPassed >= 2.5f)
                {
                    FindNextTarget();
                    approachingPassed = 0f;
                }

                approachingPassed += Time.deltaTime;
                yield return -1f;
            }

            if(OptionManager.Instance.IsAutoSkillOn)
            {
                // Choose proper skill or default attack
                for(var i = 3; i >= 0; i--)
                {
                    // Check cooldown
                    if(i != 0 && skillCooldownPassed[i - 1] < skillCooldownList[i - 1])
                        continue;
                    
                    // Check skill's level
                    if(i != 0 && ServerNetworkManager.Instance.User.SkillInfo[i - 1].Level < 1)
                        continue;
                    
                    AttackMob(i);
                    break;
                }
            }
            else
            {
                AttackMob(0);
            }
        }
    }

    public void AttackMob(int skillIndex)
    {
        if(isAttacking || !isAlive)
            return;

        if(skillIndex != 0)
        {
            if(skillCooldownPassed[skillIndex - 1] < skillCooldownList[skillIndex - 1])
                return;

            skillCooldownPassed[skillIndex - 1] = 0f;
        }

        StartCoroutine(AttackMobCoroutine(skillIndex));
    }

    private IEnumerator AttackMobCoroutine(int skillIndex)
    {
        isAttacking = true;

        var didHitMob = false;

        var beforeAttackDuration = 0.3f;
        var afterAttackDuration = 0.9f;

        // When attack is with skill, it takes longer
        if(skillIndex == 1 || skillIndex == 2)
        {
            beforeAttackDuration = 0.6f;
            afterAttackDuration = 1.4f;
        }
        else if(skillIndex == 3)
        {
            beforeAttackDuration = 1f;
            afterAttackDuration = 1f;
        }

        // Find the target monster to attack
        targetMobIdentity = null;
        FindNextTarget();

        if(targetMobIdentity != null && (targetMobIdentity.transform.position - this.transform.position) != UnityEngine.Vector3.zero)
           this.transform.localRotation = UnityEngine.Quaternion.LookRotation(targetMobIdentity.transform.position - this.transform.position);

        // Show attacking animations
        if(skillIndex == 0)
            heroAnimation.PlayDefaultAttackAnimation(this.SpeedToUse);
        else if(skillIndex == 1)
            heroAnimation.PlaySkill1AttackAnimation(this.SpeedToUse);
        else if(skillIndex == 2)
            heroAnimation.PlaySkill2AttackAnimation(this.SpeedToUse);
        else if(skillIndex == 3)
            heroAnimation.PlaySkill3AttackAnimation(this.SpeedToUse);
        else
            heroAnimation.PlayDefaultAttackAnimation(this.SpeedToUse);

        if(ServerNetworkManager.Instance.Inventory.SelectedMercenary != null && targetMobIdentity != null)
            mercenaryAnimation.PlayDefaultAttackAnimation(targetMobIdentity.transform, this.SpeedToUse);

        if(skillIndex != 0)
            isInSkillAttack = true;

        yield return new WaitForSeconds(beforeAttackDuration / this.SpeedToUse);

        // Attack all the enemies nearby the hero
        foreach(var target in IngameMainController.Instance.MobIdentityList)
        {
            var distanceToUse = 6;
            if(skillIndex == 1)
                distanceToUse = 10;
            else if(skillIndex == 2 || skillIndex == 3)
                distanceToUse = 20;

            var useForwardAttack = false;
            if(skillIndex == 1 || skillIndex == 2)
                useForwardAttack = true;

            if(UnityEngine.Vector3.Distance(target.transform.position, this.transform.position) <= distanceToUse && target.HP > 0)
            {
                // For forward attacks, only consider the mobs in front of the hero.
                // However, if the distance is very close, then also give the damage
                if(
                    useForwardAttack
                    && UnityEngine.Vector3.Dot(this.transform.forward, (target.transform.position - this.transform.position).normalized) < 0.5f
                    && UnityEngine.Vector3.Distance(target.transform.position, this.transform.position) > distanceToUse / 4f
                )
                    continue;

                // Now this attack hit at least one mob
                didHitMob = true;

                // Based on the chances, choose current attack's damage
                bool isSkill = (skillIndex != 0f);
                var damageCalculateResult = CalculateDamageMain(isSkill);

                if(skillIndex != 0)
                {
                    var mutliplierByLegendaryRelic = 1f;

                    if(skillIndex == 1)
                    {
                        var targetLegendaryRelic = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.LegendaryRelicType == LegendaryRelicType.SKILL1_ATTACK);
                        mutliplierByLegendaryRelic = (targetLegendaryRelic != null)? targetLegendaryRelic.SKill1AttackMultiplier : 1;
                    }
                    else if(skillIndex == 2)
                    {
                        var targetLegendaryRelic = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.LegendaryRelicType == LegendaryRelicType.SKILL2_ATTACK);
                        mutliplierByLegendaryRelic = (targetLegendaryRelic != null)? targetLegendaryRelic.SKill2AttackMultiplier : 1;
                    }
                    else if(skillIndex == 3)
                    {
                        var targetLegendaryRelic = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.LegendaryRelicType == LegendaryRelicType.SKILL3_ATTACK);
                        mutliplierByLegendaryRelic = (targetLegendaryRelic != null)? targetLegendaryRelic.SKill3AttackMultiplier : 1;
                    }

                    var multiplier = ServerNetworkManager.Instance.User.SkillInfo[skillIndex - 1].EffectPower * mutliplierByLegendaryRelic;
                    damageCalculateResult.damageToGive = damageCalculateResult.damageToGive * (int)(Mathf.Round(multiplier * 100f)) / 100;
                }
                
                // Give damage to the mob
                var didKillByAttack = target.ReceiveAttack(damageCalculateResult.damageToGive);

                if(!OptionManager.Instance.IsDamageInfoOff)
                {
                    // Display give damage
                    uiViewFloatingHUD.AddDamage(
                        LanguageManager.Instance.NumberToString(damageCalculateResult.damageToGive), 
                        damageCalculateResult.isCritical, 
                        damageCalculateResult.isSuper, 
                        damageCalculateResult.isUltimate,
                        damageCalculateResult.isHyper,
                        damageCalculateResult.maxCriticalType,
                        target.gameObject
                    );
                }

                // Show hit effects and animations
                EffectController.Instance.ShowHitEffect(target.transform, true);
                
                if(didKillByAttack)
                {
                    IngameMainController.Instance.OnMonsterKill(target, 0.9f / this.SpeedToUse);
                };
            }
        }
        
        // When hit any monster...
        if(didHitMob)
        {
            // Play impact sound
            if(skillIndex == 0)
                heroAnimation.PlayImpactSound();
        }
        else
        {
            if(skillIndex == 0)
                heroAnimation.PlayNoImpactSound();
        }

        // Attacks through mercenaries
        if(ServerNetworkManager.Instance.Inventory.SelectedMercenary != null && targetMobIdentity != null)
            StartCoroutine(AttackViaMercenary(targetMobIdentity, UnityEngine.Vector3.Distance(targetMobIdentity.transform.position, mercenaryAnimation.transform.position)));
        
        yield return new WaitForSeconds(afterAttackDuration / this.SpeedToUse);

        // Notify the hero attacked while boss fight
        if(IngameMainController.Instance.IsShowingBoss)
            IngameMainController.Instance.OnAttackDuringBossMode();

        isAttacking = false;
        isInSkillAttack = false;
    }

    private IEnumerator AttackViaMercenary(MobIdentity target, float distance)
    {
        yield return new WaitForSeconds(distance / 10f);

        if(target.HP > 0)
        {
            // Based on the chances, choose current attack's damage
            var damageCalculateResult = CalculateMecenaryDamageMain();
            
            // Give damage to the mob
            var didKillByAttack = target.ReceiveAttack(damageCalculateResult.damageToGive);

            if(!OptionManager.Instance.IsDamageInfoOff)
            {
                // Display give damage
                uiViewFloatingHUD.AddDamage(
                    LanguageManager.Instance.NumberToString(damageCalculateResult.damageToGive), 
                    damageCalculateResult.isCritical, 
                    damageCalculateResult.isSuper, 
                    damageCalculateResult.isUltimate,
                    damageCalculateResult.isHyper,
                    damageCalculateResult.maxCriticalType,
                    target.gameObject
                );
            }
            
            if(didKillByAttack)
            {
                IngameMainController.Instance.OnMonsterKill(target, 0.9f / this.SpeedToUse);
            };
        }
    }

    public void AttackHeroAfterMoveToHero()
    {
        if(isAttacking || IsRunning || !isAlive)
            return;

        if(moveCoroutine != null)
            StopCoroutine(moveCoroutine);

        moveCoroutine = StartCoroutine(AttackHeroAfterMoveCoroutine());
    }

    private IEnumerator AttackHeroAfterMoveCoroutine()
    {
        if(UnityEngine.Vector3.Distance(this.transform.position, targetHeroIdentity.transform.position) > 5f)
            heroAnimation.ApproachTargetHero();
        else
            this.transform.localRotation = UnityEngine.Quaternion.LookRotation(targetHeroIdentity.transform.position - this.transform.position);

        // Approach the target hero
        while(heroAnimation.IsApproaching)
        {
            yield return -1f;
        }

        // Choose proper skill or default attack
        for(var i = 3; i >= 0; i--)
        {
            // Check cooldown
            if(i != 0 && skillCooldownPassed[i - 1] < skillCooldownList[i - 1])
                continue;
            
            // Check skill's level
            if(i != 0)
            {
                var levelToUse = (isMine)? ServerNetworkManager.Instance.User.SkillInfo[i - 1].Level : InterSceneManager.Instance.OpponentUserData.SkillInfo[i - 1].Level;
                if(levelToUse < 1)
                    continue;
            }
            
            AttackHero(i);
            break;
        }
    }

    public void AttackHero(int skillIndex)
    {
        if(isAttacking || !isAlive)
            return;

        if(skillIndex != 0)
        {
            if(skillCooldownPassed[skillIndex - 1] < skillCooldownList[skillIndex - 1])
                return;

            skillCooldownPassed[skillIndex - 1] = 0f;
        }

        StartCoroutine(AttackHeroCoroutine(skillIndex));
    }

    private IEnumerator AttackHeroCoroutine(int skillIndex)
    {
        isAttacking = true;

        var didHitHero = false;
        
        var beforeAttackDuration = 0.3f;
        var afterAttackDuration = 0.9f;

        // When attack is with skill, it takes longer
        if(skillIndex == 1 || skillIndex == 2)
        {
            beforeAttackDuration = 0.6f;
            afterAttackDuration = 1.4f;
        }
        else if(skillIndex == 3)
        {
            beforeAttackDuration = 1f;
            afterAttackDuration = 1f;
        }

        this.transform.localRotation = UnityEngine.Quaternion.LookRotation(targetHeroIdentity.transform.position - this.transform.position);

        // Show attacking animations
        if(skillIndex == 0)
            heroAnimation.PlayDefaultAttackAnimation(this.SpeedToUse);
        else if(skillIndex == 1)
            heroAnimation.PlaySkill1AttackAnimation(this.SpeedToUse);
        else if(skillIndex == 2)
            heroAnimation.PlaySkill2AttackAnimation(this.SpeedToUse);
        else if(skillIndex == 3)
            heroAnimation.PlaySkill3AttackAnimation(this.SpeedToUse);
        else
            heroAnimation.PlayDefaultAttackAnimation(this.SpeedToUse);

        yield return new WaitForSeconds(beforeAttackDuration / this.SpeedToUse);

        var distanceToUse = 6;
        if(skillIndex == 1)
            distanceToUse = 10;
        else if(skillIndex == 2 || skillIndex == 3)
            distanceToUse = 20;

        var useForwardAttack = false;
        if(skillIndex == 1 || skillIndex == 2)
            useForwardAttack = true;

        if(UnityEngine.Vector3.Distance(targetHeroIdentity.transform.position, this.transform.position) <= distanceToUse)
        {
            // For forward attacks, only consider the mobs in front of the hero.
            // However, if the distance is very close, then also give the damage
            if(
                useForwardAttack
                && UnityEngine.Vector3.Dot(this.transform.forward, (targetHeroIdentity.transform.position - this.transform.position).normalized) < 0.5f
                && UnityEngine.Vector3.Distance(targetHeroIdentity.transform.position, this.transform.position) > distanceToUse / 4f
            )
            {
                // Do nothing
            }
            else
            {
                // Now this attack hit the hero
                didHitHero = true;

                // Based on the chances, choose current attack's damage
                bool isSkill = (skillIndex != 0f);
                var damageCalculateResult = CalculateDamageArena(isMine, isSkill);

                if(skillIndex != 0)
                {
                    var mutliplierByLegendaryRelic = 1f;
                    var mutliplierByLegendaryRelicEnemy = 1f;

                    if(skillIndex == 1)
                    {
                        if(isMine)
                        {
                            var targetLegendaryRelic = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.LegendaryRelicType == LegendaryRelicType.SKILL1_ATTACK);
                            mutliplierByLegendaryRelic = (targetLegendaryRelic != null)? targetLegendaryRelic.SKill1AttackMultiplier : 1;
                        }
                        else
                        {
                            var targetLegendaryRelicEnemy = InterSceneManager.Instance.OpponentInventoryData.LegendaryRelicList.FirstOrDefault(n => n.LegendaryRelicType == LegendaryRelicType.SKILL1_ATTACK);
                            mutliplierByLegendaryRelicEnemy = (targetLegendaryRelicEnemy != null)? targetLegendaryRelicEnemy.SKill1AttackMultiplier : 1;
                        }
                    }
                    else if(skillIndex == 2)
                    {
                        if(isMine)
                        {
                            var targetLegendaryRelic = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.LegendaryRelicType == LegendaryRelicType.SKILL2_ATTACK);
                            mutliplierByLegendaryRelic = (targetLegendaryRelic != null)? targetLegendaryRelic.SKill2AttackMultiplier : 1;
                        }
                        else
                        {
                            var targetLegendaryRelicEnemy = InterSceneManager.Instance.OpponentInventoryData.LegendaryRelicList.FirstOrDefault(n => n.LegendaryRelicType == LegendaryRelicType.SKILL2_ATTACK);
                            mutliplierByLegendaryRelicEnemy = (targetLegendaryRelicEnemy != null)? targetLegendaryRelicEnemy.SKill2AttackMultiplier : 1;
                        }
                    }
                    else if(skillIndex == 3)
                    {
                        if(isMine)
                        {
                            var targetLegendaryRelic = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.LegendaryRelicType == LegendaryRelicType.SKILL3_ATTACK);
                            mutliplierByLegendaryRelic = (targetLegendaryRelic != null)? targetLegendaryRelic.SKill3AttackMultiplier : 1;
                        }
                        else
                        {
                            var targetLegendaryRelicEnemy = InterSceneManager.Instance.OpponentInventoryData.LegendaryRelicList.FirstOrDefault(n => n.LegendaryRelicType == LegendaryRelicType.SKILL3_ATTACK);
                            mutliplierByLegendaryRelicEnemy = (targetLegendaryRelicEnemy != null)? targetLegendaryRelicEnemy.SKill3AttackMultiplier : 1;
                        }
                    }

                    var multiplier = (isMine)? ServerNetworkManager.Instance.User.SkillInfo[skillIndex - 1].EffectPower * mutliplierByLegendaryRelic : InterSceneManager.Instance.OpponentUserData.SkillInfo[skillIndex - 1].EffectPower * mutliplierByLegendaryRelicEnemy;
                    damageCalculateResult.damageToGive = damageCalculateResult.damageToGive * (int)(Mathf.Round(multiplier * 100f)) / 100;
                }
                
                // Give damage to the target hero
                if(isMine)
                    IngameArenaController.Instance.HeroDamage += damageCalculateResult.damageToGive;
                else
                    IngameArenaController.Instance.EnemyDamage += damageCalculateResult.damageToGive;

                if(!OptionManager.Instance.IsDamageInfoOff)
                {
                    // Display give damage
                    uiViewFloatingHUD.AddDamage(
                        LanguageManager.Instance.NumberToString(damageCalculateResult.damageToGive), 
                        damageCalculateResult.isCritical, 
                        damageCalculateResult.isSuper, 
                        damageCalculateResult.isUltimate,
                        damageCalculateResult.isHyper,
                        damageCalculateResult.maxCriticalType,
                        targetHeroIdentity.gameObject
                    );
                }

                // Show hit effects and animations
                EffectController.Instance.ShowHitEffect(targetHeroIdentity.transform, false);
            }
        }

        // When hit any monster...
        if(didHitHero)
        {
            // Play impact sound
            if(skillIndex == 0)
                heroAnimation.PlayImpactSound();
        }
        else
        {
            if(skillIndex == 0)
                heroAnimation.PlayNoImpactSound();
        }

        yield return new WaitForSeconds(afterAttackDuration / this.SpeedToUse);

        isAttacking = false;
    }

    public void AttackRaidBossAfterMoveToRaidBoss()
    {
        if(isAttacking || IsRunning || !isAlive)
            return;

        this.transform.localRotation = UnityEngine.Quaternion.LookRotation(UnityEngine.Vector3.zero - this.transform.position);

        // Choose proper skill or default attack
        for(var i = 3; i >= 0; i--)
        {
            // Check cooldown
            if(i != 0 && skillCooldownPassed[i - 1] < skillCooldownList[i - 1])
                continue;
            
            // Check skill's level
            if(i != 0)
            {
                var levelToUse = (isMine)? ServerNetworkManager.Instance.User.SkillInfo[i - 1].Level : InterSceneManager.Instance.OpponentUserData.SkillInfo[i - 1].Level;
                if(levelToUse < 1)
                    continue;
            }
            
            AttackRaidBoss(i);
            break;
        }
    }

    public void AttackRaidBoss(int skillIndex)
    {
        if(!IngameRaidController.Instance.IsPlaying)
            return;

        if(isAttacking || !isAlive)
            return;

        if(skillIndex != 0)
        {
            if(skillCooldownPassed[skillIndex - 1] < skillCooldownList[skillIndex - 1])
                return;

            skillCooldownPassed[skillIndex - 1] = 0f;

            InterSceneManager.Instance.RaidSkillUseCount ++;
        }

        StartCoroutine(AttackRaidBossCoroutine(skillIndex));
    }

    private IEnumerator AttackRaidBossCoroutine(int skillIndex)
    {
        isAttacking = true;

        var beforeAttackDuration = 0.3f;
        var afterAttackDuration = 0.9f;

        // When attack is with skill, it takes longer
        if(skillIndex == 1 || skillIndex == 2)
        {
            beforeAttackDuration = 0.6f;
            afterAttackDuration = 1.4f;
        }
        else if(skillIndex == 3)
        {
            beforeAttackDuration = 1f;
            afterAttackDuration = 1f;
        }

        // Show attacking animations
        if(skillIndex == 0)
            heroAnimation.PlayDefaultAttackAnimation(this.SpeedToUse);
        else if(skillIndex == 1)
            heroAnimation.PlaySkill1AttackAnimation(this.SpeedToUse);
        else if(skillIndex == 2)
            heroAnimation.PlaySkill2AttackAnimation(this.SpeedToUse);
        else if(skillIndex == 3)
            heroAnimation.PlaySkill3AttackAnimation(this.SpeedToUse);
        else
            heroAnimation.PlayDefaultAttackAnimation(this.SpeedToUse);

        var passed = 0f;
        while(passed < (beforeAttackDuration / this.SpeedToUse))
        {
            passed += Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }

        // Based on the chances, choose current attack's damage
        bool isSkill = (skillIndex != 0f);
        var damageCalculateResult = CalculateDamageRaid(isSkill);

        if(skillIndex != 0)
        {
            var mutliplierByLegendaryRelic = 1f;

            if(skillIndex == 1)
            {
                var targetLegendaryRelic = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.LegendaryRelicType == LegendaryRelicType.SKILL1_ATTACK);
                mutliplierByLegendaryRelic = (targetLegendaryRelic != null)? targetLegendaryRelic.SKill1AttackMultiplier : 1;
            }
            else if(skillIndex == 2)
            {
                var targetLegendaryRelic = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.LegendaryRelicType == LegendaryRelicType.SKILL2_ATTACK);
                mutliplierByLegendaryRelic = (targetLegendaryRelic != null)? targetLegendaryRelic.SKill2AttackMultiplier : 1;
            }
            else if(skillIndex == 3)
            {
                var targetLegendaryRelic = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.LegendaryRelicType == LegendaryRelicType.SKILL3_ATTACK);
                mutliplierByLegendaryRelic = (targetLegendaryRelic != null)? targetLegendaryRelic.SKill3AttackMultiplier : 1;
            }

            var multiplier = ServerNetworkManager.Instance.User.SkillInfo[skillIndex - 1].EffectPower * mutliplierByLegendaryRelic;
            damageCalculateResult.damageToGive = damageCalculateResult.damageToGive * (int)(Mathf.Round(multiplier * 100f)) / 100;
        }
        
        // Give damage to the raid boss
        IngameRaidController.Instance.HeroDamage += damageCalculateResult.damageToGive;

        if(!OptionManager.Instance.IsDamageInfoOff)
        {
            // Display give damage
            uiViewFloatingHUD.AddDamage(
                LanguageManager.Instance.NumberToString(damageCalculateResult.damageToGive), 
                damageCalculateResult.isCritical, 
                damageCalculateResult.isSuper, 
                damageCalculateResult.isUltimate,
                damageCalculateResult.isHyper,
                damageCalculateResult.maxCriticalType,
                this.gameObject
            );
        }

        // Show hit effects and animations
        EffectController.Instance.ShowHitEffect(this.transform);

        // Play impact sound
        if(skillIndex == 0)
            heroAnimation.PlayImpactSound();

        passed = 0f;
        while(passed < (afterAttackDuration / this.SpeedToUse))
        {
            passed += Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }

        isAttacking = false;
    }
    
    public void FakeAttack()
    {
        if(isAttacking || !isAlive)
            return;

        this.transform.localRotation = UnityEngine.Quaternion.LookRotation(UnityEngine.Vector3.zero - this.transform.position);

        // Choose proper skill or default attack
        var skillIndex = 0;
        for(var i = 3; i >= 0; i--)
        {
            // Check cooldown
            if(i != 0 && skillCooldownPassed[i - 1] < skillCooldownList[i - 1])
                continue;
            
            skillIndex = i;
            break;
        }

        if(skillIndex != 0)
        {
            if(skillCooldownPassed[skillIndex - 1] < skillCooldownList[skillIndex - 1])
                return;

            skillCooldownPassed[skillIndex - 1] = 0f;
        }

        StartCoroutine(FakeAttackCoroutine(skillIndex));
    }

    private IEnumerator FakeAttackCoroutine(int skillIndex)
    {
        isAttacking = true;

        yield return new WaitForSeconds(Random.Range(0f, 1f));

        var beforeAttackDuration = 0.3f;
        var afterAttackDuration = 0.9f;

        // When attack is with skill, it takes longer
        if(skillIndex == 1 || skillIndex == 2)
        {
            beforeAttackDuration = 0.6f;
            afterAttackDuration = 1.4f;
        }
        else if(skillIndex == 3)
        {
            beforeAttackDuration = 1f;
            afterAttackDuration = 1f;
        }

        if(IngameRaidController.Instance.IsPlaying)
        {
            // Show attacking animations
            if(skillIndex == 0)
                heroAnimation.PlayDefaultAttackAnimation(this.SpeedToUse);
            else if(skillIndex == 1)
                heroAnimation.PlaySkill1AttackAnimation(this.SpeedToUse);
            else if(skillIndex == 2)
                heroAnimation.PlaySkill2AttackAnimation(this.SpeedToUse);
            else if(skillIndex == 3)
                heroAnimation.PlaySkill3AttackAnimation(this.SpeedToUse);
            else
                heroAnimation.PlayDefaultAttackAnimation(this.SpeedToUse);

            yield return new WaitForSeconds(beforeAttackDuration / this.SpeedToUse);

            // Show hit effects and animations
            EffectController.Instance.ShowHitEffect(this.transform);

            // Play impact sound
            if(skillIndex == 0)
                heroAnimation.PlayImpactSound();

            yield return new WaitForSeconds(afterAttackDuration / this.SpeedToUse);
        }

        isAttacking = false;
    }

    public void AttackGuildBossAfterMoveToGuildBoss()
    {
        if(isAttacking || IsRunning || !isAlive)
            return;

        this.transform.localRotation = UnityEngine.Quaternion.LookRotation(UnityEngine.Vector3.zero - this.transform.position);

        // Choose proper skill or default attack
        for(var i = 3; i >= 0; i--)
        {
            // Check cooldown
            if(i != 0 && skillCooldownPassed[i - 1] < skillCooldownList[i - 1])
                continue;
            
            // Check skill's level
            if(i != 0)
            {
                var levelToUse = (isMine)? ServerNetworkManager.Instance.User.SkillInfo[i - 1].Level : InterSceneManager.Instance.OpponentUserData.SkillInfo[i - 1].Level;
                if(levelToUse < 1)
                    continue;
            }
            
            AttackGuildRaidBoss(i);
            break;
        }
    }

    public void AttackGuildRaidBoss(int skillIndex)
    {
        if(!IngameGuildRaidController.Instance.IsPlaying)
            return;

        if(isAttacking || !isAlive)
            return;

        if(skillIndex != 0)
        {
            if(skillCooldownPassed[skillIndex - 1] < skillCooldownList[skillIndex - 1])
                return;

            skillCooldownPassed[skillIndex - 1] = 0f;

            InterSceneManager.Instance.RaidSkillUseCount ++;
        }

        StartCoroutine(AttackRaidGuildBossCoroutine(skillIndex));
    }

    private IEnumerator AttackRaidGuildBossCoroutine(int skillIndex)
    {
        isAttacking = true;

        var beforeAttackDuration = 0.3f;
        var afterAttackDuration = 0.9f;

        // When attack is with skill, it takes longer
        if(skillIndex == 1 || skillIndex == 2)
        {
            beforeAttackDuration = 0.6f;
            afterAttackDuration = 1.4f;
        }
        else if(skillIndex == 3)
        {
            beforeAttackDuration = 1f;
            afterAttackDuration = 1f;
        }

        // Show attacking animations
        if(skillIndex == 0)
            heroAnimation.PlayDefaultAttackAnimation(this.SpeedToUse);
        else if(skillIndex == 1)
            heroAnimation.PlaySkill1AttackAnimation(this.SpeedToUse);
        else if(skillIndex == 2)
            heroAnimation.PlaySkill2AttackAnimation(this.SpeedToUse);
        else if(skillIndex == 3)
            heroAnimation.PlaySkill3AttackAnimation(this.SpeedToUse);
        else
            heroAnimation.PlayDefaultAttackAnimation(this.SpeedToUse);

        var passed = 0f;
        while(passed < (beforeAttackDuration / this.SpeedToUse))
        {
            passed += Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }

        // Based on the chances, choose current attack's damage
        bool isSkill = (skillIndex != 0f);
        var damageCalculateResult = CalculateDamageRaid(isSkill);

        if(skillIndex != 0)
        {
            var mutliplierByLegendaryRelic = 1f;

            if(skillIndex == 1)
            {
                var targetLegendaryRelic = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.LegendaryRelicType == LegendaryRelicType.SKILL1_ATTACK);
                mutliplierByLegendaryRelic = (targetLegendaryRelic != null)? targetLegendaryRelic.SKill1AttackMultiplier : 1;
            }
            else if(skillIndex == 2)
            {
                var targetLegendaryRelic = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.LegendaryRelicType == LegendaryRelicType.SKILL2_ATTACK);
                mutliplierByLegendaryRelic = (targetLegendaryRelic != null)? targetLegendaryRelic.SKill2AttackMultiplier : 1;
            }
            else if(skillIndex == 3)
            {
                var targetLegendaryRelic = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.LegendaryRelicType == LegendaryRelicType.SKILL3_ATTACK);
                mutliplierByLegendaryRelic = (targetLegendaryRelic != null)? targetLegendaryRelic.SKill3AttackMultiplier : 1;
            }

            var multiplier = ServerNetworkManager.Instance.User.SkillInfo[skillIndex - 1].EffectPower * mutliplierByLegendaryRelic;
            damageCalculateResult.damageToGive = damageCalculateResult.damageToGive * (int)(Mathf.Round(multiplier * 100f)) / 100;
        }

        // Get guild relic effect
        var targetGuildRelic = ServerNetworkManager.Instance.GuildRelicData.GuildRelicList.FirstOrDefault(n => n.RelicType == GuildRelicType.GUILD_RAID_ATTACK);
        var mutliplierByGuildRelic = (targetGuildRelic != null)? targetGuildRelic.GuildRaidAttackMultiplier : 1;
        
        damageCalculateResult.damageToGive = damageCalculateResult.damageToGive * mutliplierByGuildRelic;
        
        // Give damage to the raid boss
        IngameGuildRaidController.Instance.HeroScore += (int)System.Math.Floor(BigInteger.Log10(damageCalculateResult.damageToGive)) + 1;

        if(!OptionManager.Instance.IsDamageInfoOff)
        {
            // Display give damage
            uiViewFloatingHUD.AddDamage(
                LanguageManager.Instance.NumberToString(damageCalculateResult.damageToGive), 
                damageCalculateResult.isCritical, 
                damageCalculateResult.isSuper, 
                damageCalculateResult.isUltimate,
                damageCalculateResult.isHyper,
                damageCalculateResult.maxCriticalType,
                this.gameObject
            );
        }

        // Show hit effects and animations
        EffectController.Instance.ShowHitEffect(this.transform);

        // Play impact sound
        if(skillIndex == 0)
            heroAnimation.PlayImpactSound();

        passed = 0f;
        while(passed < (afterAttackDuration / this.SpeedToUse))
        {
            passed += Time.fixedDeltaTime;
            yield return new WaitForFixedUpdate();
        }

        isAttacking = false;
    }

    public void FakeGuildAttack()
    {
        if(isAttacking || !isAlive)
            return;

        this.transform.localRotation = UnityEngine.Quaternion.LookRotation(UnityEngine.Vector3.zero - this.transform.position);

        // Choose proper skill or default attack
        var skillIndex = 0;
        for(var i = 3; i >= 0; i--)
        {
            // Check cooldown
            if(i != 0 && skillCooldownPassed[i - 1] < skillCooldownList[i - 1])
                continue;
            
            skillIndex = i;
            break;
        }

        if(skillIndex != 0)
        {
            if(skillCooldownPassed[skillIndex - 1] < skillCooldownList[skillIndex - 1])
                return;

            skillCooldownPassed[skillIndex - 1] = 0f;
        }

        StartCoroutine(FakeGuildAttackCoroutine(skillIndex));
    }

    private IEnumerator FakeGuildAttackCoroutine(int skillIndex)
    {
        isAttacking = true;

        yield return new WaitForSeconds(Random.Range(0f, 1f));

        var beforeAttackDuration = 0.3f;
        var afterAttackDuration = 0.9f;

        // When attack is with skill, it takes longer
        if(skillIndex == 1 || skillIndex == 2)
        {
            beforeAttackDuration = 0.6f;
            afterAttackDuration = 1.4f;
        }
        else if(skillIndex == 3)
        {
            beforeAttackDuration = 1f;
            afterAttackDuration = 1f;
        }

        if(IngameGuildRaidController.Instance.IsPlaying)
        {
            // Show attacking animations
            if(skillIndex == 0)
                heroAnimation.PlayDefaultAttackAnimation(this.SpeedToUse);
            else if(skillIndex == 1)
                heroAnimation.PlaySkill1AttackAnimation(this.SpeedToUse);
            else if(skillIndex == 2)
                heroAnimation.PlaySkill2AttackAnimation(this.SpeedToUse);
            else if(skillIndex == 3)
                heroAnimation.PlaySkill3AttackAnimation(this.SpeedToUse);
            else
                heroAnimation.PlayDefaultAttackAnimation(this.SpeedToUse);

            yield return new WaitForSeconds(beforeAttackDuration / this.SpeedToUse);

            // Show hit effects and animations
            EffectController.Instance.ShowHitEffect(this.transform);

            // Play impact sound
            if(skillIndex == 0)
                heroAnimation.PlayImpactSound();

            yield return new WaitForSeconds(afterAttackDuration / this.SpeedToUse);
        }

        isAttacking = false;
    }

    private (BigInteger damageToGive, bool isCritical, bool isSuper, bool isUltimate, bool isHyper, bool isInstant, EMaxCriticalType maxCriticalType) CalculateDamageMain(bool isSkill)
    {
        bool isCritical = false;
        bool isSuper = false;
        bool isUltimate = false;
        bool isHyper = false;
        bool isInstant = false;

        var damageToGive = NumberMainController.Instance.FinalBaseAttackDamage;

        EMaxCriticalType maxCriticalType = GetMaxCiricalType(NumberMainController.Instance.FinalCriticalChance,
            NumberMainController.Instance.FinalSuperChance,
            NumberMainController.Instance.FinalUltimateChance,
            NumberMainController.Instance.FinalHyperChance);

        var randomValue = Random.Range(0f, 1f);
        if(isSkill) // For skills, there are 2x chances to have this
            randomValue *= 0.5f;

        if(randomValue <= NumberMainController.Instance.FinalCriticalChance)
        {
            isCritical = true;
            damageToGive = NumberMainController.Instance.FinalCriticalAttackDamage;
        }

        randomValue = Random.Range(0f, 1f);
        if(isSkill) // For skills, there are 2x chances to have this
            randomValue *= 0.5f;

        if(isCritical && randomValue <= NumberMainController.Instance.FinalSuperChance)
        {
            isSuper = true;
            damageToGive = NumberMainController.Instance.FinalSuperAttackDamage;
        }

        randomValue = Random.Range(0f, 1f);
        if(isSkill) // For skills, there are 2x chances to have this
            randomValue *= 0.5f;

        if(isSuper && randomValue <= NumberMainController.Instance.FinalUltimateChance)
        {
            isUltimate = true;
            damageToGive = NumberMainController.Instance.FinalUltimateAttackDamage;
        }

        randomValue = Random.Range(0f, 1f);
        if(isSkill) // For skills, there are 2x chances to have this
            randomValue *= 0.5f;

        if(isUltimate && randomValue <= NumberMainController.Instance.FinalHyperChance)
        {
            isHyper = true;
            damageToGive = NumberMainController.Instance.FinalHyperAttackDamage;
        }

        if(targetMobIdentity != null && damageToGive >= targetMobIdentity.MaxHP && targetMobIdentity.HP == targetMobIdentity.MaxHP)
        {
            isInstant = true;
        }

        return (damageToGive, isCritical, isSuper, isUltimate, isHyper, isInstant, maxCriticalType);
    }

    private EMaxCriticalType GetMaxCiricalType(ObscuredFloat criticalChance, ObscuredFloat superChance, ObscuredFloat ultimateChance, ObscuredFloat hyperChance)
    {
        if (hyperChance > 0.0f)
            return EMaxCriticalType.Hyper;
        if (ultimateChance > 0.0f)
            return EMaxCriticalType.Ultimate;
        if (superChance > 0.0f)
            return EMaxCriticalType.Super;
        if (criticalChance > 0.0f)
            return EMaxCriticalType.Critical;
        return EMaxCriticalType.None;
    }

    private (BigInteger damageToGive, bool isCritical, bool isSuper, bool isUltimate, bool isHyper, bool isInstant, EMaxCriticalType maxCriticalType) CalculateMecenaryDamageMain()
    {
        bool isCritical = false;
        bool isSuper = false;
        bool isUltimate = false;
        bool isHyper = false;
        bool isInstant = false;
        BigInteger damageToGive = 0;
        EMaxCriticalType maxCriticalType = GetMaxCiricalType(NumberMainController.Instance.FinalCriticalChance,
            NumberMainController.Instance.FinalSuperChance,
            NumberMainController.Instance.FinalUltimateChance,
            NumberMainController.Instance.FinalHyperChance); 
        if (ServerNetworkManager.Instance.Inventory.SelectedMercenary != null)
        {
            damageToGive = NumberMainController.Instance.FinalBaseAttackDamage * (int)(ServerNetworkManager.Instance.Inventory.SelectedMercenary.MercenaryAttackMultiplier) / 100;

            var randomValue = Random.Range(0f, 1f);
            if (randomValue <= NumberMainController.Instance.FinalCriticalChance)
            {
                isCritical = true;
                damageToGive = NumberMainController.Instance.FinalCriticalAttackDamage * (int)(ServerNetworkManager.Instance.Inventory.SelectedMercenary.MercenaryAttackMultiplier) / 100;
            }

            randomValue = Random.Range(0f, 1f);
            if (isCritical && randomValue <= NumberMainController.Instance.FinalSuperChance)
            {
                isSuper = true;
                damageToGive = NumberMainController.Instance.FinalSuperAttackDamage * (int)(ServerNetworkManager.Instance.Inventory.SelectedMercenary.MercenaryAttackMultiplier) / 100;
            }

            randomValue = Random.Range(0f, 1f);
            if (isSuper && randomValue <= NumberMainController.Instance.FinalUltimateChance)
            {
                isUltimate = true;
                damageToGive = NumberMainController.Instance.FinalUltimateAttackDamage * (int)(ServerNetworkManager.Instance.Inventory.SelectedMercenary.MercenaryAttackMultiplier) / 100;
            }

            randomValue = Random.Range(0f, 1f);
            if (isUltimate && randomValue <= NumberMainController.Instance.FinalHyperChance)
            {
                isHyper = true;
                damageToGive = NumberMainController.Instance.FinalHyperAttackDamage * (int)(ServerNetworkManager.Instance.Inventory.SelectedMercenary.MercenaryAttackMultiplier) / 100;
            }
            // Get guild relic effect
            var targetGuildRelic = ServerNetworkManager.Instance.GuildRelicData.GuildRelicList.FirstOrDefault(n => n.RelicType == GuildRelicType.MERCENARY_ATTACK);
            var mutliplierByGuildRelic = (targetGuildRelic != null) ? targetGuildRelic.MercenaryAttackMultiplier : 1f;

            damageToGive = damageToGive * (BigInteger)(mutliplierByGuildRelic * 100f) / 100;

            if (targetMobIdentity != null && damageToGive >= targetMobIdentity.MaxHP && targetMobIdentity.HP == targetMobIdentity.MaxHP)
            {
                isInstant = true;
            }
        }        
        return (damageToGive, isCritical, isSuper, isUltimate, isHyper, isInstant, maxCriticalType);
    }

    private (BigInteger damageToGive, bool isCritical, bool isSuper, bool isUltimate, bool isHyper, bool isisInstant, EMaxCriticalType maxCriticalType) CalculateDamageArena(bool isMine, bool isSkill)
    {
        bool isCritical = false;
        bool isSuper = false;
        bool isUltimate = false;
        bool isHyper = false;
        bool isInstant = false;

        var criticalChanceToUse = (isMine)? NumberArenaController.Instance.FinalCriticalChance : NumberArenaController.Instance.FinalCriticalChanceEnemy;
        var superChanceToUse = (isMine)? NumberArenaController.Instance.FinalSuperChance : NumberArenaController.Instance.FinalSuperChanceEnemy;
        var ultimateChanceToUse = (isMine)? NumberArenaController.Instance.FinalUltimateChance : NumberArenaController.Instance.FinalUltimateChanceEnemy;
        var hyperChanceToUse = (isMine)? NumberArenaController.Instance.FinalHyperChance : NumberArenaController.Instance.FinalHyperChanceEnemy;

        EMaxCriticalType maxCriticalType = GetMaxCiricalType(criticalChanceToUse,
            superChanceToUse,
            ultimateChanceToUse,
            hyperChanceToUse);

        var damageToGive = (isMine)? NumberArenaController.Instance.FinalBaseAttackDamage : NumberArenaController.Instance.FinalBaseAttackDamageEnemy;
        
        var randomValue = Random.Range(0f, 1f);
        if(isSkill) // For skills, there are 2x chances to have this
            randomValue *= 0.5f;

        if(randomValue <= criticalChanceToUse)
        {
            isCritical = true;
            damageToGive = (isMine)? NumberArenaController.Instance.FinalCriticalAttackDamage : NumberArenaController.Instance.FinalCriticalAttackDamageEnemy;
        }

        randomValue = Random.Range(0f, 1f);
        if(isSkill) // For skills, there are 2x chances to have this
            randomValue *= 0.5f;

        if(isCritical && randomValue <= superChanceToUse)
        {
            isSuper = true;
            damageToGive = (isMine)? NumberArenaController.Instance.FinalSuperAttackDamage : NumberArenaController.Instance.FinalSuperAttackDamageEnemy;
        }

        randomValue = Random.Range(0f, 1f);
        if(isSkill) // For skills, there are 2x chances to have this
            randomValue *= 0.5f;
            
        if(isSuper && randomValue <= ultimateChanceToUse)
        {
            isUltimate = true;
            damageToGive = (isMine)? NumberArenaController.Instance.FinalUltimateAttackDamage : NumberArenaController.Instance.FinalUltimateAttackDamageEnemy;
        }

        randomValue = Random.Range(0f, 1f);
        if(isSkill) // For skills, there are 2x chances to have this
            randomValue *= 0.5f;

        if(isUltimate && randomValue <= hyperChanceToUse)
        {
            isHyper = true;
            damageToGive = (isMine)? NumberArenaController.Instance.FinalHyperAttackDamage : NumberArenaController.Instance.FinalHyperAttackDamageEnemy;
        }

        return (damageToGive, isCritical, isSuper, isUltimate, isHyper, isInstant, maxCriticalType);
    }

    private (BigInteger damageToGive, bool isCritical, bool isSuper, bool isUltimate, bool isHyper, EMaxCriticalType maxCriticalType) CalculateDamageRaid(bool isSkill)
    {
        bool isCritical = false;
        bool isSuper = false;
        bool isUltimate = false;
        bool isHyper = false;

        var damageToGive = NumberRaidController.Instance.FinalBaseAttackDamage;

        EMaxCriticalType maxCriticalType = GetMaxCiricalType(NumberRaidController.Instance.FinalCriticalChance,
            NumberRaidController.Instance.FinalSuperChance,
            NumberRaidController.Instance.FinalUltimateChance,
            NumberRaidController.Instance.FinalHyperChance);


        var randomValue = Random.Range(0f, 1f);
        if(isSkill) // For skills, there are 2x chances to have this
            randomValue *= 0.5f;

        if(randomValue <= NumberRaidController.Instance.FinalCriticalChance)
        {
            isCritical = true;
            damageToGive = NumberRaidController.Instance.FinalCriticalAttackDamage;
        }

        randomValue = Random.Range(0f, 1f);
        if(isSkill) // For skills, there are 2x chances to have this
            randomValue *= 0.5f;

        if(isCritical && randomValue <= NumberRaidController.Instance.FinalSuperChance)
        {
            isSuper = true;
            damageToGive = NumberRaidController.Instance.FinalSuperAttackDamage;
        }

        randomValue = Random.Range(0f, 1f);
        if(isSkill) // For skills, there are 2x chances to have this
            randomValue *= 0.5f;

        if(isSuper && randomValue <= NumberRaidController.Instance.FinalUltimateChance)
        {
            isUltimate = true;
            damageToGive = NumberRaidController.Instance.FinalUltimateAttackDamage;
        }

        randomValue = Random.Range(0f, 1f);
        if(isSkill) // For skills, there are 2x chances to have this
            randomValue *= 0.5f;

        if(isUltimate && randomValue <= NumberRaidController.Instance.FinalHyperChance)
        {
            isHyper = true;
            damageToGive = NumberRaidController.Instance.FinalHyperAttackDamage;
        }

        return (damageToGive, isCritical, isSuper, isUltimate, isHyper, maxCriticalType);
    }

    public void FindNextTarget()
    {
        if(IngameMainController.Instance.IsShowingBoss)
        {
            targetMobIdentity = IngameMainController.Instance.BossIdentity;
        }
        else
        {
            foreach(var tmp in IngameMainController.Instance.MobIdentityList.OrderBy(n => 
                UnityEngine.Vector3.Distance(this.transform.position, n.transform.position) 
                + ((UnityEngine.Vector3.Dot(this.transform.forward, (n.transform.position - this.transform.position).normalized) < 0.5f)? 3f : 0f) // Better not to look behind too often
            ))
            {
                targetMobIdentity = tmp;
                
                // The condition matching mob exists, so break now
                if(tmp.HP > 0)
                    break;
            }
        }
    }

    public void Die()
    {
        isAlive = false;
        heroAnimation.PlayDieAnimation();
    }

    private UnityEngine.Vector3 GetSpawnPositionForMain()
    {
        var distantDictionary = new Dictionary<Transform, float>();
        foreach(var tmp in IngameMainController.Instance.SpawnPositionList)
        {
            // Avoid other mobs
            var distantTotal = 0f;
            foreach(var tmp2 in IngameMainController.Instance.MobIdentityList)
            {
                distantTotal += 1f / Mathf.Pow(UnityEngine.Vector3.Distance(tmp.transform.position, tmp2.transform.position), 2f);   
            }
            
            distantDictionary[tmp] = distantTotal;
        }
        
        // Randomly choose from some secluded places
        var orderedList = distantDictionary.OrderBy(n => n.Value).ToList();
        return orderedList[UnityEngine.Random.Range(0, orderedList.Count / 4)].Key.position;
    }

    private UnityEngine.Vector3 GetSpawnPositionForRaid()
    {
        var distantDictionary = new Dictionary<Transform, float>();
        foreach(var tmp in IngameRaidController.Instance.SpawnPositionList)
        {
            // Avoid other heroes
            var distantTotal = 0f;
            for(int i = 0; i < IngameRaidController.Instance.AllyHeroIdList.Count; i++)
            {
                var tmp2 = IngameRaidController.Instance.AllyHeroList[i];
                distantTotal += 1f / Mathf.Pow(UnityEngine.Vector3.Distance(tmp.transform.position, tmp2.transform.position), 2f);   
            }
            
            distantDictionary[tmp] = distantTotal;
        }
        
        // Choose the most secluded place
        return distantDictionary.OrderBy(n => n.Value).First().Key.position;
    }

    private UnityEngine.Vector3 GetSpawnPositionForGuildRaid()
    {
        var distantDictionary = new Dictionary<Transform, float>();
        foreach(var tmp in IngameGuildRaidController.Instance.SpawnPositionList)
        {
            // Avoid other heroes
            var distantTotal = 0f;
            for(int i = 0; i < IngameGuildRaidController.Instance.AllyHeroIdList.Count; i++)
            {
                var tmp2 = IngameGuildRaidController.Instance.AllyHeroList[i];
                distantTotal += 1f / Mathf.Pow(UnityEngine.Vector3.Distance(tmp.transform.position, tmp2.transform.position), 2f);   
            }
            
            distantDictionary[tmp] = distantTotal;
        }
        
        // Choose the most secluded place
        return distantDictionary.OrderBy(n => n.Value).First().Key.position;
    }

}
