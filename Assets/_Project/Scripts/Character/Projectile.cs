﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class Projectile : MonoBehaviour {
	
	[SerializeField]
	private GameObject hitEffect;
	[Title("Setting")]
	
	[SerializeField]
	private float xSpeed;

	[SerializeField]
	private float zSpeed;

	[SerializeField]
	private QuickPool.DespawnIn despawnIn;

	private Transform target;

	public void SetProjectile(Vector3 startPosition, Transform target)
	{
		this.target = target;
		this.transform.position = startPosition;

		var endPosition = target.transform.position + Vector3.up;

		despawnIn.time = (endPosition - startPosition).magnitude / 10f;

		var direction = (endPosition - startPosition).normalized;
		xSpeed = direction.x * 10f;
		zSpeed = direction.z * 10f;
	}


	private void Update()
	{
		this.transform.position += (Vector3.right * xSpeed + Vector3.forward * zSpeed) * Time.deltaTime;
	}

	private void OnDespawn()
	{
		if(hitEffect != null)
		{
			if(target != null)
				EffectController.Instance.ShowHitEffect(target.transform, hitEffect);
			else
				QuickPool.PoolsManager.Spawn(hitEffect, this.gameObject.transform.position, Quaternion.identity);
		}
	}
	
}
