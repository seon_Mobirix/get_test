﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PetAnimation : MonoBehaviour
{
    private string currentPetId = "";

    [SerializeField]
    private Transform visual;

    [SerializeField]
    private Animator animator;

    [SerializeField]
    private NavMeshAgent navMeshAgent;

    [SerializeField]
    private Transform targetHero = null;

    [SerializeField]
    private bool isApproaching = false;
    public bool IsApproaching
    {
        get{
            return isApproaching;
        }
    }

    private void Update()
    {
        if(targetHero != null && this.navMeshAgent.velocity.magnitude < 0.1f)
            this.transform.localRotation = Quaternion.Slerp(this.transform.localRotation, Quaternion.LookRotation(targetHero.position - this.transform.position, Vector3.up), 1f * Time.deltaTime);
    }

    public void Reset(bool resetPosition)
    {
        if(resetPosition)
            navMeshAgent.Warp(targetHero.position - Vector3.forward * 0.5f);

        PlayIdleAnimation();

        StopAllCoroutines();
        ApproachTarget();
    }

    public void RefreshVisual(string id)
    {
        if(currentPetId != id)
        {
            if(visual != null)
            {
                Destroy(visual.gameObject);
                visual = null;
                animator = null;
            }
            
            if(id != "")
            {
                visual = Instantiate(Resources.Load<Transform>("Pets/Visuals/" + "P" + id.Substring(1)));
                visual.parent = this.transform;
                visual.localRotation = Quaternion.identity;
                visual.localPosition = Vector3.zero;
                animator = visual.GetComponent<Animator>();
            }
        }
        currentPetId = id;
    }

    public void ApproachTarget()
    {
        StartCoroutine(ApproachTargetCoroutine());
    }

    private IEnumerator ApproachTargetCoroutine()
    {
        while(true)
        {
            var distance = Vector3.Distance(this.transform.position, targetHero.position);

            if(distance > 2.5f)
            {
                this.navMeshAgent.SetDestination(targetHero.position);
                PlayRunAnimation();
                yield return new WaitForSeconds(0.25f);
            }
            else
            {
                PlayIdleAnimation();
                yield return new WaitForSeconds(2f);
            }
        }
        
    }

    public void PlayIdleAnimation()
	{
        if(animator != null)
        {
            animator.speed = 1f;
            animator.ResetTrigger("Run");
            animator.SetTrigger("Idle");
        }
	}
    
    public void PlayRunAnimation()
    {
        if(animator != null)
        {
            animator.speed = 2f;
            animator.SetTrigger("Run");
            animator.ResetTrigger("Idle");
        }
    }
}
