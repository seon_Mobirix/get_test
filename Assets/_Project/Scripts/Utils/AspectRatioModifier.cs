﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Sirenix.OdinInspector;

public class AspectRatioModifier : MonoBehaviour
{
    [SerializeField]
    private List<CanvasScaler> canvasScalers;

	[Title("Scaling For < 4:3")]
    [SerializeField]
    private List<RectTransform> heightAdjustedUIList;
	[SerializeField]
	private bool useDoubleOffset;

    [Title("Scaling for > 2:1")]
    [SerializeField]
    private List<RectTransform> safeZoneAdjustedUIList;
    [SerializeField]
    private List<RectTransform> safeZoneReverseAdjustedUIList;
    [SerializeField]
    private float safezoneWidth = 100f;

    private void Awake()
    {
        var screenRatio = (float)Screen.width / (float)Screen.height;
        if(screenRatio <= 1f)
            screenRatio = 1f / screenRatio;

        if(screenRatio >= 1.85f)
        {
            foreach(var tmp in canvasScalers)
                tmp.matchWidthOrHeight = 1f;
            ApplySafezone();
        }
        else if(screenRatio >= 1.7f)
        {
            foreach(var tmp in canvasScalers)
                tmp.matchWidthOrHeight = 1f;
            // ex) 16:9. Do nothing
        }
        else
        {
            foreach(var tmp in canvasScalers)
                tmp.matchWidthOrHeight = 0f;
            AdjustCanvasHeight(screenRatio);
        }
    }

    private void ApplySafezone()
    {
        // Apply safezone offset to deal with notch devices
        foreach(var item in safeZoneAdjustedUIList)
        {
            item.offsetMin = new Vector2(safezoneWidth, item.offsetMin.y);
            item.offsetMax = new Vector2(-safezoneWidth, item.offsetMax.y);
        }
        foreach(var item in safeZoneReverseAdjustedUIList)
        {
            item.offsetMin = new Vector2(-safezoneWidth, item.offsetMin.y);
            item.offsetMax = new Vector2(safezoneWidth, item.offsetMax.y);
        }
    }

    private void AdjustCanvasHeight(float screenRatio)
    {
        var sizeY = 1920f / screenRatio;
        var desiredHeight = 1080f;
        var offset = Mathf.Floor((sizeY - desiredHeight) / 2f);

		if(useDoubleOffset)
			offset *= 2;

        foreach(var item in heightAdjustedUIList)
        {
            item.offsetMin += new Vector2(0f, offset);
            item.offsetMax -= new Vector2(0f, offset);
        }
    }
}
