﻿using System.Collections.Generic;

public static class ListExtensions {
    /// <summary>
    /// Shuffles the element order of the specified list.
    /// </summary>
    public static List<T> Shuffle<T>(this List<T> list) {
        var newList = new List<T>(list);
        var count = newList.Count;
        var last = count - 1;
        for (var i = 0; i < last; ++i) {
            var r = UnityEngine.Random.Range(i, count);
            var tmp = newList[i];
            newList[i] = newList[r];
            newList[r] = tmp;
        }
        return newList;
    }
}