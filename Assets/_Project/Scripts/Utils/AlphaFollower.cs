﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlphaFollower : MonoBehaviour
{   
    [SerializeField]
    private Button reference = null;
    private Text targetText = null;
    private Image targetImage = null;

    private bool previousState = true;

    private void Start()
    {
        if(reference == null)
            reference = this.transform.parent.GetComponent<Button>();
        targetText = this.transform.GetComponent<Text>();
        targetImage = this.transform.GetComponent<Image>();
    }

    private void Update()
    {
        Refresh();
    }

    private void Refresh()
    {
        if(reference != null && previousState != reference.interactable)
        {
            if(!reference.interactable)
            {
                if(targetImage != null)
                    targetImage.color = new Color(targetImage.color.r, targetImage.color.g, targetImage.color.b, reference.colors.disabledColor.a * 0.5f);
                if(targetText != null)
                    targetText.color = new Color(targetText.color.r, targetText.color.g, targetText.color.b, reference.colors.disabledColor.a);
            }
            else
            {
                if(targetImage != null)
                    targetImage.color = new Color(targetImage.color.r, targetImage.color.g, targetImage.color.b, reference.colors.normalColor.a);
                if(targetText != null)
                    targetText.color = new Color(targetText.color.r, targetText.color.g, targetText.color.b, reference.colors.normalColor.a);
            }
        }
        previousState = reference.interactable;
    }
}
