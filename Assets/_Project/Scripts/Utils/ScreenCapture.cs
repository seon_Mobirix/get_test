using UnityEngine;
using System.Collections;

public class ScreenCapture : MonoBehaviour
{
#if UNITY_EDITOR
	private int screenshotCount = 0;
	// Check for screenshot key each frame
	void Update()
	{
		// take screenshot on up->down transition of F9 key
		if (Input.GetKeyUp(KeyCode.Slash))
		{        
			string screenshotFilename;
			do
			{
				screenshotCount++;
				screenshotFilename = "screenshot" + screenshotCount + ".png";

			} while (System.IO.File.Exists(screenshotFilename));

			UnityEngine.ScreenCapture.CaptureScreenshot(screenshotFilename);

			Debug.Log("file name => " + screenshotFilename);
		}
	}
#endif	
}