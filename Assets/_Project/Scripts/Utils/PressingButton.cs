﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;


public class PressingButton : MonoBehaviour
{

    private bool isPressed = false;
    private bool isPointUpActionEnabled = false;
    private float lastTime = 0;

    public UnityEngine.Events.UnityEvent OnInteraction = null;
    
    private void Update()
    {
        if(isPressed && (Time.realtimeSinceStartup - lastTime) > 0.1f)
        {
            if(OnInteraction != null)
                OnInteraction.Invoke();
            lastTime = Time.realtimeSinceStartup;
            isPointUpActionEnabled = false;
        }
    }

    public void OnPointerDown()
    {
        lastTime = Time.realtimeSinceStartup;
        isPressed = true;
        isPointUpActionEnabled = true;
    }
    
    public void OnPointerUp()
    {
        if(isPressed && isPointUpActionEnabled)
        {
            if(OnInteraction != null)
                OnInteraction.Invoke();
        }
        isPressed = false;
        isPointUpActionEnabled = false;
    }
}
