﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FontSetter : MonoBehaviour {

	public static bool didSetMinimumFontSize = false;

	private Text textToUse;

	public bool disabled = false;

	private static string defaultFont = "Font-Global";

	private static string krFont = "Font-KR";
	private static string jpFont = "Font-JP";
	private static string scFont = "Font-SC";
	private static string tcFont = "Font-TC";
	private static string thFont = "Font-TH";

	private void Awake()
	{
		if(disabled)
			return;

		textToUse = this.GetComponent<Text>();

		if(textToUse != null)
		{
			if(OptionManager.Instance.Language == OptionManager.LanguageModeType.ko)
			{
				textToUse.font = Resources.Load<Font>(krFont);
			}
			else if(OptionManager.Instance.Language == OptionManager.LanguageModeType.ja)
			{
				textToUse.font = Resources.Load<Font>(jpFont);
			}
			else if(OptionManager.Instance.Language == OptionManager.LanguageModeType.zh_chs)
			{
				textToUse.font = Resources.Load<Font>(scFont);
			}
			else if(OptionManager.Instance.Language == OptionManager.LanguageModeType.zh_cht)
			{
				textToUse.font = Resources.Load<Font>(tcFont);
			}
			else if(OptionManager.Instance.Language == OptionManager.LanguageModeType.th)
			{
				textToUse.font = Resources.Load<Font>(thFont);
			}
			else
			{
				textToUse.font = Resources.Load<Font>(defaultFont);
			}
		}

		// This is for preventing font texture to be too small.
		if(!FontSetter.didSetMinimumFontSize)
		{
			textToUse.font.RequestCharactersInTexture("ABCDEFGHIJ", 500);

			// For default font. (This is because some texts don't have font setter, so that they will us default font.)
			Resources.Load<Font>(defaultFont).RequestCharactersInTexture("ABCDEFGHIJ", 500);
		}
		FontSetter.didSetMinimumFontSize = true;

#if UNITY_EDITOR
		PreventMultipleSetters();
#endif
	}

	public void PreventMultipleSetters()
	{
		if(this.gameObject.GetComponents<FontSetter>().Length >= 2){
			if(this.transform.parent != null)
				Debug.LogError(this.transform.parent.name + "/" + this.name + " has multiple font setters!");
			else
				Debug.LogError(this.name + " has multiple font setters!");
		}
			
	}

}
