﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RatioSetter : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        var ratio = this.GetComponent<RectTransform>().rect.width / this.GetComponent<RectTransform>().rect.height;
        var instanceMaterial = Instantiate(this.GetComponent<Image>().material);
        this.GetComponent<Image>().material = instanceMaterial;
        this.GetComponent<Image>().material.SetFloat("AnimatedOffsetUV_ZoomX_1", ratio);
    }
}
