﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Studio : MonoBehaviour 
{

	public Transform rotator;

	public List<string> characterNames;

	private int i = 0;

#if UNITY_EDITOR
	private int screenshotCount = 0;
	// Check for screenshot key each frame
	void Update()
	{

		if (Input.GetKey(KeyCode.LeftArrow))
		{
			rotator.transform.Rotate(0f, Time.deltaTime * 60f, 0f); 
		}
		else if (Input.GetKey(KeyCode.RightArrow))
		{
			rotator.transform.Rotate(0f, Time.deltaTime * -60f, 0f);
		}
		
		if (Input.GetKeyUp(KeyCode.Space))
		{        
			string screenshotFilename;
			do
			{
				screenshotCount++;
				screenshotFilename = "screenshot" + screenshotCount + ".png";

			} while (System.IO.File.Exists(screenshotFilename));

			Studio.SaveScreenshotToFile(screenshotFilename);

			Debug.Log ("file name => " + screenshotFilename);
		}
	}
	 public static Texture2D TakeScreenShot()
     {
         return Screenshot ();
     }
     
	static Texture2D Screenshot() {
		
		int resWidth = Camera.main.pixelWidth;
		int resHeight = Camera.main.pixelHeight;
		Camera camera = Camera.main;
		RenderTexture rt = new RenderTexture(resWidth, resHeight, 32);
		camera.targetTexture = rt;
		Texture2D screenShot = new Texture2D(resWidth, resHeight, TextureFormat.ARGB32, false);
		camera.Render();
		RenderTexture.active = rt;
		screenShot.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
		screenShot.Apply ();
		camera.targetTexture = null;
		RenderTexture.active = null; // JC: added to avoid errors
		Destroy(rt);
		return screenShot;
	}

	public static Texture2D SaveScreenshotToFile(string fileName)
	{
		Texture2D screenShot = Screenshot ();
		byte[] bytes = screenShot.EncodeToPNG();
		System.IO.File.WriteAllBytes(fileName, bytes);
		return screenShot;
	}
	
#endif	
}
