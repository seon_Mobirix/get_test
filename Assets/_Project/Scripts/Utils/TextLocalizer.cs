﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextLocalizer : MonoBehaviour 
{
    public LanguageDataType type = LanguageDataType.UI;

    public string key = "";

	public bool disabled = false;

	private void Awake()
	{
		
		if(disabled)
			return;
		
		var targetText = this.GetComponent<Text>();

		if(targetText != null)
			targetText.text = LanguageManager.Instance.GetTextData(type, key);

	}
}
