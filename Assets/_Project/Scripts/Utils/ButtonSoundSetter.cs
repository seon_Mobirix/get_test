﻿using UnityEngine;
using UnityEngine.UI;

public class ButtonSoundSetter : MonoBehaviour {

	public bool disabled = false;

	private void Awake()
	{
		if(disabled)
			return;
			
		this.GetComponent<Button>().onClick.AddListener(delegate{SoundManager.Instance.PlaySound("Button");});
#if UNITY_EDITOR
		PreventMultipleSetters();
#endif
	}

	public void PreventMultipleSetters()
	{
		if(this.gameObject.GetComponents<ButtonSoundSetter>().Length >= 2){
			if(this.transform.parent != null)
				Debug.LogError(this.transform.parent.name + "/" + this.name + " has multiple button sound setters!");
			else
				Debug.LogError(this.name + " has multiple button sound setters!");
		}
			
	}

}
