﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPSCounter : MonoBehaviour
{
    const float fpsMeasurePeriod = 0.5f;
	private int m_FpsAccumulator = 0;
	private float m_FpsNextPeriod = 0f;
	private float m_LastFrameTime = 0f;
	private int m_CurrentFps;
	const string display = "{0}fps";
	private Text m_Text;

	private void Start()
	{
		m_Text = GetComponent<Text>();

		m_FpsNextPeriod = Time.realtimeSinceStartup + fpsMeasurePeriod;
		m_LastFrameTime = Time.realtimeSinceStartup;

		SetVisual(false);
	}

	private void Update()
	{
		// measure average frames per second
		m_FpsAccumulator++;
		if (Time.realtimeSinceStartup > m_FpsNextPeriod)
		{
			m_CurrentFps = Mathf.RoundToInt((float)m_FpsAccumulator / (Time.realtimeSinceStartup - m_LastFrameTime));
			m_FpsAccumulator = 0;
			m_FpsNextPeriod += fpsMeasurePeriod;
			m_LastFrameTime = Time.realtimeSinceStartup;

			if(m_Text.gameObject.activeSelf)
				m_Text.text = string.Format(display, m_CurrentFps);
		}
	}

	private bool currentOn = true;
	public void SetVisual(bool isOn)
	{
		if(currentOn != isOn)
		{
			this.m_Text.gameObject.SetActive(isOn);
		}
		currentOn = isOn;
	}
}
