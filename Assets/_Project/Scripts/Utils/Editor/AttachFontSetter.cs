using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
 
public class AttachFontSetter
{
    // Add a new menu item under an existing menu
 
    [MenuItem("Tools/Attach Font Setter")]
    private static void DoAttachFontSetter()
    {
        var fontMaterial = Resources.Load<Material>("AdvancedText/OutlineTextMaterial");

        foreach (var tmp in Resources.FindObjectsOfTypeAll(typeof(Text)) as Text[])
        {
            if (tmp.gameObject.hideFlags == HideFlags.NotEditable || tmp.gameObject.hideFlags == HideFlags.HideAndDontSave)
                continue;

            // Check for objects in the scene that are instances of any prefab.
            if (!EditorUtility.IsPersistent(tmp.gameObject.transform.root.gameObject) && PrefabUtility.GetPrefabInstanceHandle(tmp.gameObject) != null)
                continue;

            if(tmp is AdvancedText)
            {
                if(tmp.gameObject.GetComponents<FontSetter>().Length == 0)
                    tmp.gameObject.AddComponent<FontSetter>();
                
                var fontSetter = tmp.gameObject.GetComponent<FontSetter>();
                fontSetter.PreventMultipleSetters();
                
                // Remove any shadow effects attached to this text.
                foreach(var tmp2 in tmp.GetComponents<Shadow>())
                    UnityEngine.MonoBehaviour.DestroyImmediate(tmp2, true);

                if((tmp as AdvancedText).effectType != AdvancedText.TextEffectType.NONE)
                    tmp.material = fontMaterial;
                else
                    tmp.material = null;
            }
            else
            {
                PrintUpToParent(tmp.transform);
                Debug.LogWarning(tmp.transform.name + " is not advanced text");
            }
            
        }
    }

    [MenuItem("Tools/Check Maxsize")]
    private static void DoCheckMaxsize()
    {
        var fontMaterial = Resources.Load<Material>("AdvancedText/OutlineTextMaterial");

        foreach (var tmp in Resources.FindObjectsOfTypeAll(typeof(Text)) as Text[])
        {
            if (tmp.gameObject.hideFlags == HideFlags.NotEditable || tmp.gameObject.hideFlags == HideFlags.HideAndDontSave)
                continue;

            // Check for objects in the scene that are instances of any prefab.
            if (!EditorUtility.IsPersistent(tmp.gameObject.transform.root.gameObject) && PrefabUtility.GetPrefabInstanceHandle(tmp.gameObject) != null)
                continue;

            if(tmp.resizeTextForBestFit && tmp.resizeTextMaxSize >= 60 && tmp.fontSize < 60)
            {
                PrintUpToParent(tmp.transform);
                Debug.LogWarning(tmp.transform.name + " is big");
            }
            
        }
    }

    private static int PrintUpToParent(Transform target)
    {
        var toReturn = 0;
        if(target.parent != null)
        {
            toReturn = PrintUpToParent(target.parent) + 1;
        }

        var stringToUse = "";
        for(int i = 0; i < toReturn; i ++)
            stringToUse += ">";
        Debug.Log(stringToUse + " " + target);
        return toReturn;
    }
}