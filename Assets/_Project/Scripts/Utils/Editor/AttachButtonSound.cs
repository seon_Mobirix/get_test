﻿using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
 
public class AttachButtonSound
{

    // Add a new menu item under an existing menu
 
    [MenuItem("Tools/Attach Button Sound")]
    private static void DoAttachButtonSound()
    {
        foreach (var tmp in Resources.FindObjectsOfTypeAll(typeof(Button)) as Button[])
        {
            if (tmp.gameObject.hideFlags == HideFlags.NotEditable || tmp.gameObject.hideFlags == HideFlags.HideAndDontSave)
                continue;

            // Check for objects in the scene that are instances of any prefab.
            if (!EditorUtility.IsPersistent(tmp.gameObject.transform.root.gameObject) && PrefabUtility.GetPrefabInstanceHandle(tmp.gameObject) != null)
                continue;

			if(tmp.gameObject.GetComponents<ButtonSoundSetter>().Length == 0)
				tmp.gameObject.AddComponent<ButtonSoundSetter>();
			tmp.gameObject.GetComponent<ButtonSoundSetter>().PreventMultipleSetters();
        }
    }
    
}