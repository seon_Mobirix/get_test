﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
#if UNITY_IPHONE
using UnityEditor.iOS.Xcode;
#endif
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class ShrikePostProcessor: ScriptableObject{

	public DefaultAsset liappLibrary;

	public TextAsset replaceMainPart1;
	public TextAsset replaceMainPart2;

    [PostProcessBuild(99999)] // We should try to run last
	public static void OnPostProcessBuild(BuildTarget target, string path)
	{
#if UNITY_IPHONE
		if (target != BuildTarget.iOS) {
			return; // How did we get here?
		}

		UnityEngine.Debug.Log("ShrikePostProcessor: Add Info.plist items");

		// Get plist
		string plistPath = path + "/Info.plist";
		PlistDocument plist = new PlistDocument();
		plist.ReadFromString(File.ReadAllText(plistPath));
	
		// Get root
		PlistElementDict rootDict = plist.root;

		var locale = rootDict.CreateArray("CFBundleLocalizations");
		locale.AddString("en");
		locale.AddString("ko");
		locale.AddString("ja");
		locale.AddString("zh_CN");
		locale.AddString("zh_TW");
		locale.AddString("de");
		locale.AddString("es");
		locale.AddString("fr");
		locale.AddString("vi");
		locale.AddString("th");
		locale.AddString("ru");
		locale.AddString("it");
		locale.AddString("nl");

		var supported = rootDict.CreateArray("UIBackgroundModes");
		supported.AddString("remote-notification");

		// Write to file
		File.WriteAllText(plistPath, plist.WriteToString());

		// Let's use PBXProject
		string projectPath = PBXProject.GetPBXProjectPath(path);
		PBXProject project = new PBXProject();
		project.ReadFromString(File.ReadAllText(projectPath));
		string targetGUID = project.GetUnityMainTargetGuid();

		// Need to add iAd framework
		project.AddFrameworkToProject(targetGUID, "iAd.framework", false);
		
	#region Liapp

		// Also add UnityFramework.framework
		project.AddFrameworkToProject(targetGUID, "UnityFramework.framework", false);

		// Disable bitcode
		project.SetBuildProperty(targetGUID, "ENABLE_BITCODE", "NO");

		// Modify Main.mm
		var dummy = ScriptableObject.CreateInstance<ShrikePostProcessor>();
		var mainAppPath = Path.Combine(path, "MainApp", "main.mm");
		var mainContent = File.ReadAllText(mainAppPath);
		var newContent = mainContent.Replace("#include <UnityFramework/UnityFramework.h>", dummy.replaceMainPart1.text);
		newContent = newContent.Replace("int main(int argc, char* argv[])\n{", dummy.replaceMainPart2.text);
		File.WriteAllText(mainAppPath, newContent);
		
	#endregion

		// Finally write
		project.WriteToFile(projectPath);

#endif
	}

}