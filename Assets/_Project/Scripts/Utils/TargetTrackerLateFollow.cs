﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetTrackerLateFollow : MonoBehaviour {

	public Transform TargetToFollow;

	public Transform UpVectorTransform;

	public bool FollowRotation = false;

	[SerializeField]
	private Vector3 deviation;

	private float followRotationPassed = 0f;
	private void LateUpdate () {
		if(TargetToFollow != null)
		{
			if(FollowRotation)
			{
				float followPower = Mathf.Lerp(1f, 2.5f, followRotationPassed / 1f);
				followRotationPassed += Time.deltaTime;
				followRotationPassed = Mathf.Min(followRotationPassed, 1f);

				this.transform.rotation = Quaternion.Slerp(this.transform.rotation, Quaternion.Euler(this.transform.rotation.eulerAngles.x, TargetToFollow.rotation.eulerAngles.y, TargetToFollow.rotation.eulerAngles.z), followPower * Time.deltaTime);
				this.transform.position = Vector3.Lerp(this.transform.position, TargetToFollow.TransformPoint(deviation), followPower * Time.deltaTime);
			}
			else
			{
				this.transform.position = Vector3.Lerp(this.transform.position, TargetToFollow.position + UpVectorTransform.TransformVector(deviation), 2.5f * Time.deltaTime);
				followRotationPassed = 0f;
			}
		}
	}
}
