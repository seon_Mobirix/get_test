﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Whitecard : MonoBehaviour {

	[SerializeField]
	private Image imageToUse;
	
	private void Start () {
		StartCoroutine(DestroyThisAfterCrossFade());
	}

	private IEnumerator DestroyThisAfterCrossFade()
	{
		yield return new WaitForSeconds(0.25f);
		this.imageToUse.CrossFadeAlpha(0f, 1f, true);
		yield return new WaitForSeconds(1f);
		Destroy(this.gameObject);
	}

}
