﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class ImageAutoScale : MonoBehaviour
{
    [SerializeField]
    private List<CanvasScaler> canvasScalers = new List<CanvasScaler>();

    [SerializeField]
    private Image image = null;

    private void Awake()
    {
        var screenRatio = (float)Screen.width / (float)Screen.height;
        if(screenRatio <= 1f)
            screenRatio = 1f / screenRatio;

        if(screenRatio >= 1.7f)
        {
            foreach(var tmp in canvasScalers)
                tmp.matchWidthOrHeight = 1f;
        }
        else
        {
            foreach(var tmp in canvasScalers)
                tmp.matchWidthOrHeight = 0f;
        }
        
        image.rectTransform.SetTop(Mathf.Min(-1f * (1080f * screenRatio - 1920f), 0f));
        image.rectTransform.SetBottom(Mathf.Min(-1f * (1080f * screenRatio - 1920f), 0f));
        image.rectTransform.SetLeft(Mathf.Min(-1f * (1920f / screenRatio - 1080), 0f));
        image.rectTransform.SetRight(Mathf.Min(-1f * (1920f / screenRatio - 1080), 0f));
    }
}
