﻿using UnityEngine;
using UnityEditor;
 
public class RemoveCollider : MonoBehaviour
{
#if UNITY_EDITOR
    [MenuItem("Tools/Remove Collider")]
    static public void DoRemoveCollider()
    {
        string log = "";
        var obj = Selection.gameObjects;
        if(obj!=null)
        {
            for (int i = 0; i < obj.Length; i++)
            {
                log+="<color=white>CHECKING Object:</color> " + obj[i].name+"\n";
                // now check to see if has a collider
                Collider[] colliders = obj[i].GetComponentsInChildren<Collider>( );
                foreach(Collider collider in colliders )
                {
                    log+="\t<color=red>REMOVING COLLIDER:</color> "+collider.name+"\n";
                    
                    // remove the collider
                    DestroyImmediate(collider);
                }
            }
            Debug.Log(log);
        }
        else
        {
            Debug.Log("Nothing selected");
        }
    }
#endif
}