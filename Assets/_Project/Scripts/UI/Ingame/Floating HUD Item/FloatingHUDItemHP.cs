﻿using UnityEngine;
using UnityEngine.UI;
using System.Numerics;
using System.Globalization;

public class FloatingHUDItemHP : FloatingHUDItem {

	[SerializeField]
	private Slider slider;
	[SerializeField]
	private Image fill;
	[SerializeField]
	private Sprite allyColor;
	[SerializeField]
	private Sprite enemyColor;
	[SerializeField]
	private Text hpText;

	public bool IsForBoss = false;

	[SerializeField]
	private MobIdentity targetIdentity;

	public void Init(MobIdentity targetIdentity, bool isAlly)
	{
		this.targetIdentity = targetIdentity;
		base.Init(targetIdentity.transform);

		slider.maxValue = 1f;
		slider.value = 1f;

		if(isAlly)
			fill.sprite = allyColor;
		else
			fill.sprite = enemyColor;
	}

	private BigInteger previousHP = -1;
	public void Refresh()
	{
		slider.value = targetIdentity.LeftHPRatio;
		
		if(IsForBoss)
		{
			if(IngameMainController.Instance.BossIdentity != targetIdentity || !IngameMainController.Instance.IsShowingBoss || slider.value == 0)
			{
				slider.gameObject.SetActive(false);
				hpText.gameObject.SetActive(false);
			}
			else
			{
				slider.gameObject.SetActive(true);
				hpText.gameObject.SetActive(true);
			}
		}
		else
		{
			if(targetIdentity.IsFullHP || (IngameMainController.Instance.BossIdentity == targetIdentity && IngameMainController.Instance.IsShowingBoss) || slider.value == 0)
				slider.gameObject.SetActive(false);
			else
				slider.gameObject.SetActive(true);
		}

		if(hpText != null && previousHP != targetIdentity.HP)
		{
			hpText.text = LanguageManager.Instance.NumberToString(targetIdentity.HP);
		}
		previousHP = targetIdentity.HP;
	}
}