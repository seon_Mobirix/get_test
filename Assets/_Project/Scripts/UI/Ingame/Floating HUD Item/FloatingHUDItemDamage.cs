﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum DamageDisplayType {DAMAGE, HEAL, MISS, BLOCK}

public class FloatingHUDItemDamage : FloatingHUDItem {
	
	[SerializeField]
	private Text text;

	[SerializeField]
	private float duration = 1f;

	[SerializeField]
	private Color normalColor = Color.white;

	[SerializeField]
	private Color cricitalColor = Color.red;

	[SerializeField]
	private Color superColor = Color.yellow;

	[SerializeField]
	private Color ultimateColor = Color.green;
	
	[SerializeField]
	private Color hyperColor = Color.green;

	// Will this HUD rendered within screen?
	protected bool isVisible = false;
	public bool IsVisible
	{
		get{
			return isVisible;
		}
	}

	public void SetText(Vector3 startPosition, string damage, bool isCritical, bool isSuper, bool isUltimate, bool isHyper, EMaxCriticalType maxCriticalType, bool isFlowing = true)
	{
		this.transform.SetAsLastSibling();
		this.transform.position = startPosition;

		if(isHyper)
		{
			this.text.text = $"<SIZE=38>{LanguageManager.Instance.GetTextData(LanguageDataType.UI, "ingame_hyper_attack")}</SIZE>\n{damage}";
			//this.text.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "ingame_hyper_attack") +  "\n" + damage;
			this.text.color = hyperColor;
		}
		else if(isUltimate)
		{
			if(maxCriticalType > EMaxCriticalType.Ultimate)
				this.text.text = $"{damage}";
			else
				this.text.text = $"<SIZE=38>{LanguageManager.Instance.GetTextData(LanguageDataType.UI, "ingame_ultimate_attack")}</SIZE>\n{damage}";
			//this.text.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "ingame_ultimate_attack") +  "\n" + damage;
			this.text.color = ultimateColor;
		}
		else if(isSuper)
		{
			if (maxCriticalType > EMaxCriticalType.Super)
				this.text.text = $"{damage}";
			else
				this.text.text = $"<SIZE=38>{LanguageManager.Instance.GetTextData(LanguageDataType.UI, "ingame_super_attack")}</SIZE>\n{damage}";
			//this.text.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "ingame_super_attack") +  "\n" + damage;
			this.text.color = superColor;
		}
		else if(isCritical)
		{
			if (maxCriticalType > EMaxCriticalType.Critical)
				this.text.text = $"{damage}";
			else
				this.text.text = $"<SIZE=38>{LanguageManager.Instance.GetTextData(LanguageDataType.UI, "ingame_cirtical_attack")}</SIZE>\n{damage}";
			//this.text.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "ingame_cirtical_attack") +  "\n" + damage;
			this.text.color = cricitalColor;
		}
		else
		{
			this.text.text = damage;
			this.text.color = normalColor;
		}
		
		// Enable.
		this.gameObject.SetActive(true);

		// Visible.
		isVisible = true;

		// Start coroutine for this HUD's life cycle.
		if(isFlowing)
			StartCoroutine(GoUpCoroutine());
	}

	private IEnumerator GoUpCoroutine()
	{
		var upAmount = 0f;
		var passed = 0f;
		bool isAplha = false;

		var rate = BuffManager.Instance.SpeedBuffTimeLeftSecond > 0 ? 0.65f : 1.0f;
		var durat = duration * rate;
		while (passed < durat)
		{
			if(upAmount < 100f)
			{
				this.transform.localPosition += new Vector3(0f, Time.deltaTime * 240f / rate, 0f);
				upAmount += Time.deltaTime * 240f / rate;
			}
			else
            {
				if (!isAplha)
					this.text.CrossFadeAlpha(0.0f, durat - passed, true);
				isAplha = true;
			}

			passed += Time.deltaTime;
			yield return -1;
		}
		HideItem();
	}

	public void HideItem()
	{
		this.gameObject.SetActive(false);
		isVisible = false;
	}
}