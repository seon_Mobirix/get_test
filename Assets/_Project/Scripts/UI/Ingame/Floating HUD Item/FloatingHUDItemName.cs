﻿using UnityEngine;
using UnityEngine.UI;

public class FloatingHUDItemName : FloatingHUDItem {

	[SerializeField]
	private Text nickNameText = null;

	[SerializeField]
	private HeroIdentity targetIdentity;
	public HeroIdentity TargetIdentity{
		get{
			return targetIdentity;
		}
	}

	public void Init(HeroIdentity targetIdentity, string name)
	{
		this.targetIdentity = targetIdentity;
		base.Init(targetIdentity.transform);

		nickNameText.text = name;
	}

	public void ChangeName(string name)
	{
		nickNameText.text = name;
	}
}