﻿using UnityEngine;
using UnityEngine.UI;

public class FloatingHUDItemTime : FloatingHUDItem {

	[SerializeField]
	private Slider slider;

	int previousAdditionalHP = -1;
	public void Refresh()
	{
		if(UIManager.Instance.NeedUIForBoss)
			slider.value = Mathf.Pow(1f - IngameMainController.Instance.bossPassed / IngameMainController.Instance.BossDuration, 2f);
	}
}
