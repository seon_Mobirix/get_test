using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class FloatingHUDItem : MonoBehaviour {

	[SerializeField]
    private Transform followTarget;

    [SerializeField]
	private bool isFollowing = true;

    public float YOffset;

    private Camera mainCamera = null;

	private void LateUpdate()
	{
        if(mainCamera == null)
            mainCamera = Camera.main;

        if(followTarget != null && isFollowing)
        {
            var positionInfo = RectTransformUtility.WorldToScreenPoint(mainCamera, followTarget.position);
            this.transform.localPosition = this.transform.parent.InverseTransformPoint(positionInfo.x, positionInfo.y, 0f) + new Vector3(0f, YOffset, 0f);
        }
	}

	public void Init(Transform target)
    {
        followTarget = target;
    }

}
