﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class UIViewFloatingHUD : MonoBehaviour {

	[SerializeField]
	private Transform parent;

	[SerializeField]
	private Transform barParent;
	[SerializeField]
	private Transform damageParent;
	[SerializeField]
	private Transform bossRelatedParent;

	[Title("Damage")]
	[SerializeField]
	private GameObject floatingHUDItemDamagePrefab;

	[Title("Name Bar")]
	[SerializeField]
	private GameObject floatingHUDItemNamePrefab;
	[SerializeField]
	private List<FloatingHUDItemName> nameBarList;

	[Title("Hp Bar")]
	[SerializeField]
	private GameObject floatingHUDItemHPPrefab;
	[SerializeField]
	private GameObject floatingHUDItemHPBossPrefab;
	[SerializeField]
	private List<FloatingHUDItemHP> enemyHPBarList;

	[Title("Time Bar")]
	[SerializeField]
	private GameObject floatingHUDItemTimePrefab;
	[SerializeField]
	private List<FloatingHUDItemTime> timeBarList;

	private List<FloatingHUDItemDamage> listOfDamage = new List<FloatingHUDItemDamage>();

	private void Awake()
	{
		for(int i = 0; i < 25; i++)
		{
			var tmp = Instantiate(floatingHUDItemDamagePrefab).GetComponent<FloatingHUDItemDamage>();
			tmp.transform.SetParent(damageParent, false);
			tmp.transform.localScale = Vector3.one;
			tmp.HideItem();
			listOfDamage.Add(tmp);
		}

		//SetScaleFloatingHUDItemDamage(0.5f);
	}

	public void Refresh()
	{
		foreach(var tmp in enemyHPBarList)
			tmp.Refresh();
		foreach(var tmp in timeBarList)
			tmp.Refresh();

		if(UIManager.Instance.NeedUIForBoss)
			bossRelatedParent.gameObject.SetActive(true);
		else
			bossRelatedParent.gameObject.SetActive(false);
	}
	
	public void SetScaleFloatingHUDItemDamage(float rate)
    {
		listOfDamage.ForEach(p => p.transform.localScale = new Vector3(rate, rate, 1.0f));

	}

	public void AddDamage(string damage, bool isCritical, bool isSuper, bool isUltimate, bool isHyper, EMaxCriticalType maxCriticalType, GameObject targetObject)
	{
		foreach(var tmp in listOfDamage)
		{
			if(!tmp.IsVisible)
			{
				var positionInfo = RectTransformUtility.WorldToScreenPoint(Camera.main, targetObject.transform.position);
				tmp.GetComponent<FloatingHUDItemDamage>().SetText(
					new Vector3(positionInfo.x + Random.Range(-15f, 15f), positionInfo.y + 100f, 0f), 
					damage, 
					isCritical, 
					isSuper,
					isUltimate,
					isHyper,
					maxCriticalType
				);
				break;
			}
		}
	}

	public void AddNameBar(HeroIdentity heroIdentity)
	{
		var tmpNameBar = ((GameObject)Instantiate(floatingHUDItemNamePrefab)).GetComponent<FloatingHUDItemName>();
		
		tmpNameBar.Init(heroIdentity, "");
		tmpNameBar.transform.SetParent(barParent, true);
		
		var positionInfo = RectTransformUtility.WorldToScreenPoint(Camera.main, heroIdentity.gameObject.transform.position);
		tmpNameBar.transform.position = new Vector3(positionInfo.x, positionInfo.y, 0f);
		tmpNameBar.transform.localPosition += new Vector3(0f, tmpNameBar.YOffset, 0f);
		tmpNameBar.transform.localScale = Vector3.one;

		nameBarList.Add(tmpNameBar);
	}

	public void ResetNameBar(HeroIdentity heroIdentity, string name)
	{
		var target = nameBarList.FirstOrDefault(n => n.TargetIdentity == heroIdentity);
		target?.ChangeName(name);
	}

	public void AddEnemyHPBar(MobIdentity mobIdentity)
	{
		var tmpHPBar = ((GameObject)Instantiate(floatingHUDItemHPPrefab)).GetComponent<FloatingHUDItemHP>();
		
		tmpHPBar.Init(mobIdentity, false);
		tmpHPBar.transform.SetParent(barParent, true);
		
		var positionInfo = RectTransformUtility.WorldToScreenPoint(Camera.main, mobIdentity.gameObject.transform.position);
		tmpHPBar.transform.position = new Vector3(positionInfo.x, positionInfo.y, 0f);
		tmpHPBar.transform.localPosition += new Vector3(0f, tmpHPBar.YOffset, 0f);
		tmpHPBar.transform.localScale = Vector3.one;

		enemyHPBarList.Add(tmpHPBar);
	}

	public void AddEnemyBossHPBar(MobIdentity mobIdentity)
	{
		var tmpHPBar = ((GameObject)Instantiate(floatingHUDItemHPBossPrefab)).GetComponent<FloatingHUDItemHP>();
		
		tmpHPBar.Init(mobIdentity, false);
		tmpHPBar.transform.SetParent(bossRelatedParent, false);
		tmpHPBar.transform.localScale = Vector3.one;
		tmpHPBar.IsForBoss = true;

		enemyHPBarList.Add(tmpHPBar);
	}

	public void AddEnemyBossTime()
	{
		var tmpTimeBar = ((GameObject)Instantiate(floatingHUDItemTimePrefab)).GetComponent<FloatingHUDItemTime>();
		
		tmpTimeBar.transform.SetParent(bossRelatedParent, false);
		tmpTimeBar.transform.localScale = Vector3.one;

		timeBarList.Add(tmpTimeBar);
	} 

}
