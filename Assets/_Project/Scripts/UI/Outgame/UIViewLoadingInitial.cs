﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class UIViewLoadingInitial : MonoBehaviour {
	
	public Canvas canvas;
	public RectTransform loadingMenu;

	[SerializeField]
	private Text loadingText;

	[SerializeField]
	private List<Sprite> logoList;

	[SerializeField]
	private Image logo;

	[SerializeField]
	private Image logo2;

	[SerializeField]
	private Text stateText;
	private string stateString = "";

	public Image fadeCover;

	private void Start()
	{
		loadingMenu.gameObject.SetActive(true);
		loadingText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "loading_loading");

		if(OptionManager.Instance.Language == OptionManager.LanguageModeType.ko)
		{
			logo2.sprite = logoList[0];
		}
		else
		{
			logo2.sprite = logoList[1];
		}
	}

	private void Update()
	{
		if(canvas.enabled)
		{
			if(stateString != ServerNetworkManager.Instance.loginChainPhaseText)
			{
				stateString = ServerNetworkManager.Instance.loginChainPhaseText;
				stateText.text = stateString;
			}
		}
	}

	public void Show()
	{
		canvas.enabled = true;
	}

	public void Hide()
	{
		canvas.enabled = false;
	}
}