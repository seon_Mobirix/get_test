﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class UIViewRaid : MonoBehaviour 
{   

    [Title("UI Contents")]

    [SerializeField]
    private Text battleStateText = null;

    [SerializeField]
    private Slider bossSlider = null;

    [SerializeField]
    private Text damageText = null;

    [SerializeField]
    private Text timeLeftText = null;

    [SerializeField]
    private Transform raidButtonTransform;

    [SerializeField]
    private Text scoreboardText = null;

    [SerializeField]
    private Text roomNumberText = null;

    [SerializeField]
    private Text floorText = null;

    [SerializeField]
    private Transform floorButtonsTransform = null;

    [SerializeField]
	private ScrollRect scrollRect;

    [SerializeField]
    private Transform backButton;

    [SerializeField]
    private Transform chatArea;

    [Title("Skill Controller")]

    [SerializeField]
    private Button autoAttackButton;

    [SerializeField]
    private Image autoAttackButtonBackground;

    [SerializeField]
    private Text autoAttackButtonText;

    [SerializeField]
    private Button skill1Button;
    [SerializeField]
    private Image skill1ButtonImage;
    [SerializeField]
    private Image skill1LockImage;

    [SerializeField]
    private Button skill2Button;
    [SerializeField]
    private Image skill2ButtonImage;
    [SerializeField]
    private Image skill2LockImage;

    [SerializeField]
    private Button skill3Button;
    [SerializeField]
    private Image skill3ButtonImage;
    [SerializeField]
    private Image skill3LockImage;

    [Title("Others")]
    public LayerMask layerOfBackground;

    private bool isInited = false;

#region Refresh Methods

	public void Refresh()
	{
        if(!isInited)
        {
            isInited = true;
            roomNumberText.text = $"{LanguageManager.Instance.GetTextData(LanguageDataType.UI, "raid_room_number")}: {RaidManager.Instance.CurrentRoomNumber}";
        }

        RefreshBattleUI(IngameRaidController.Instance.IsPlaying);
        RefreshGauge();
        RefreshTime();
        RefreshScoreBoard();
        RefreshFloor();

        RefreshSkillButtons();
        RefreshSkillCooldown();
        RefreshAttackButton();
	}

    public void RefreshBattleUI(bool isStarted)
    {
        if(!isStarted)
        {
            backButton.gameObject.SetActive(true);
            battleStateText.enabled = true;
            
            if(RaidManager.Instance.CurrentHostPlayerId != ServerNetworkManager.Instance.User.PlayFabId)
            {
                raidButtonTransform.gameObject.SetActive(false);
                floorButtonsTransform.gameObject.SetActive(false);
            }
            else
            {
                raidButtonTransform.gameObject.SetActive(true);
                floorButtonsTransform.gameObject.SetActive(true);
            }
        }
        else
        {
            backButton.gameObject.SetActive(false);
            battleStateText.enabled = false;
            raidButtonTransform.gameObject.SetActive(false);
            floorButtonsTransform.gameObject.SetActive(false);
        }
    }

    public void RefreshGauge()
    {
        if(IngameRaidController.Instance.BossMaxHp - IngameRaidController.Instance.HeroDamage - IngameRaidController.Instance.TeamDamage >= 0)
            damageText.text = LanguageManager.Instance.NumberToString(IngameRaidController.Instance.BossMaxHp - IngameRaidController.Instance.HeroDamage - IngameRaidController.Instance.TeamDamage);
        else
            damageText.text = "0";

        var gaugeValue = (float)Math.Exp(BigInteger.Log(IngameRaidController.Instance.BossMaxHp - IngameRaidController.Instance.HeroDamage - IngameRaidController.Instance.TeamDamage + 1) - BigInteger.Log(IngameRaidController.Instance.BossMaxHp + 1));
        gaugeValue = Mathf.Clamp01(gaugeValue);
        bossSlider.value = gaugeValue;
    }

    public void RefreshTime()
    {
        timeLeftText.text = IngameRaidController.Instance.LeftTime.ToString();
    }

    public void RefreshScoreBoard()
    {
        var stringToUse = "";
        var existBefore = false;
        int rank = 1;
        foreach(var tmp in IngameRaidController.Instance.ScoreBoard.OrderByDescending(x => x.Value).Take(Mathf.Min(ChatManager.Instance.PlayersInPrivateRoom.Count, 12)))
        {
            if(existBefore)
            {
                stringToUse += "\n";
            }
            existBefore = true;

            if(ChatManager.Instance.PlayersInPrivateRoom.ContainsKey(tmp.Key))
                stringToUse += rank.ToString() + ". " + ChatManager.Instance.PlayersInPrivateRoom[tmp.Key] + ": " + LanguageManager.Instance.NumberToString(tmp.Value);
            else
                stringToUse += rank.ToString() + ". ???: " + LanguageManager.Instance.NumberToString(tmp.Value);

            rank ++;
        }
        scoreboardText.text = Mathf.Min(12, IngameRaidController.Instance.ScoreBoard.Count()).ToString() + "/12\n" + stringToUse;
    }

    public void RefreshFloor()
    {
        floorText.text = RaidManager.Instance.CurrentFloor + "\n" + "STAGE";
    }

    public void RefreshAttackButton()
    {
        autoAttackButton.interactable = false;

        autoAttackButtonBackground.color = new Color(1f, 0.7215686f, 0, 1);
        autoAttackButtonText.color = new Color(1f, 0.7215686f, 0, 1);
    }

    public void RefreshSkillButtons()
    {
        bool isSkill1Unlock = (ServerNetworkManager.Instance.User.SkillInfo[0].Level > 0);
        bool isSkill2Unlock = (ServerNetworkManager.Instance.User.SkillInfo[1].Level > 0);
        bool isSkill3Unlock = (ServerNetworkManager.Instance.User.SkillInfo[2].Level > 0);

        skill1Button.interactable = isSkill1Unlock;
        skill1ButtonImage.gameObject.SetActive(isSkill1Unlock);
        skill1LockImage.gameObject.SetActive(!isSkill1Unlock);
        
        skill2Button.interactable = isSkill2Unlock;
        skill2ButtonImage.gameObject.SetActive(isSkill2Unlock);
        skill2LockImage.gameObject.SetActive(!isSkill2Unlock);

        skill3Button.interactable = isSkill3Unlock;
        skill3ButtonImage.gameObject.SetActive(isSkill3Unlock);
        skill3LockImage.gameObject.SetActive(!isSkill3Unlock);
    }

    public void RefreshSkillCooldown()
    {
        skill1ButtonImage.fillAmount = IngameRaidController.Instance.HeroIdentity.skill1Cooldown;
        if(skill1ButtonImage.fillAmount != 1f)
            skill1ButtonImage.color = new Color(1f, 1f, 1f, 0.5f);
        else
            skill1ButtonImage.color = new Color(1f, 1f, 1f, 1f);
        skill2ButtonImage.fillAmount = IngameRaidController.Instance.HeroIdentity.skill2Cooldown;
        if(skill2ButtonImage.fillAmount != 1f)
            skill2ButtonImage.color = new Color(1f, 1f, 1f, 0.5f);
        else
            skill2ButtonImage.color = new Color(1f, 1f, 1f, 1f);
        skill3ButtonImage.fillAmount = IngameRaidController.Instance.HeroIdentity.skill3Cooldown;
        if(skill3ButtonImage.fillAmount != 1f)
            skill3ButtonImage.color = new Color(1f, 1f, 1f, 0.5f);
        else
            skill3ButtonImage.color = new Color(1f, 1f, 1f, 1f);
    }

#endregion

#region Button Callbacks

    public void OnStartRaidButtonClicked()
    {
        IngameRaidController.Instance.StartRaid();
    }

    public void OnPreviousFloorButtonClicked()
    {
        IngameRaidController.Instance.ToPreviousFloor();
    }

    public void OnNextFloorButtonClicked()
    {
        IngameRaidController.Instance.ToNextFloor();
    }

    public void OnBackButtonClicked()
    {
        IngameRaidController.Instance.TryQuitIngame();
    }

    public void OnChatButtonClicked()
    {
        chatArea.gameObject.SetActive(!chatArea.gameObject.activeSelf);
    }

#endregion

#region Game Play Button Callbacks

    public void OnMoveAreaTouch()
    {
        // Check touch move option
        if(OptionManager.Instance.IsTouchMoveOn)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, layerOfBackground.value))
            {
                IngameRaidController.Instance.ReadyForMove(hit.point);
            }
        }
    }

    public void OnAttackButtonClicked()
    {
        IngameRaidController.Instance.ReadyForAttack();
    }

    public void OnSkillButtonClicked(int skillIndex)
    {
        IngameRaidController.Instance.ReadyForSkill(skillIndex);
    }

    public void OnAutoAttackButtonClicked()
    {
        // Do nothing
    }

#endregion

}
