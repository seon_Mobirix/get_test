﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Sirenix.OdinInspector;

public class UIPopupRanking : UIPopup
{
    [SerializeField]
	private UITab uiTab;
    [SerializeField]
	private Transform listItemRankingPrefab;

    [Title("Stage Ranking")]
    [SerializeField]
	private RectTransform rankingStageListParentTransform;
	
    [SerializeField]
    private ListItemRanking myRankingStage;

    [Title("Arena Ranking")]
    [SerializeField]
	private RectTransform rankingArenaListParentTransform;
    [SerializeField]
    private ListItemRanking myRankingArena;

    [Title("Total Arena Ranking")]
    [SerializeField]
	private RectTransform rankingTotalArenaListParentTransform;
    [SerializeField]
    private ListItemRanking myRankingTotalArena;

    private int currentTabId;

    public override void Open()
	{
		Debug.LogWarning("UIPopupRanking shouldn't be open without parameters");
	}

	public void OpenWithValues(int tabId)
	{
		base.Open();

		currentTabId = tabId;
		uiTab.OnTabButton(tabId);

		Refresh();

        FocusItems();
	}

    public override void Close()
    {
        base.Close();

        // Need to sync data so far
        OutgameController.Instance.SyncPlayerData(false, "UIPopupRanking");
    }

	public void OnTabButtonClicked(int tabId)
	{
		currentTabId = tabId;
        uiTab.OnTabButton(tabId);

        Refresh();

        FocusItems();
	}

    public override void Refresh()
	{
		if(currentTabId == 0)
        {
            OutgameController.Instance.RefreshRanking(currentTabId, RefreshStageRankingList, () => {
                UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
            });
        }
        else if(currentTabId == 1)
        {
            OutgameController.Instance.RefreshRanking(currentTabId, RefreshArenaRankingList, () => {
                UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
            });        
        }
        else if(currentTabId == 2)
        {
            OutgameController.Instance.RefreshRanking(currentTabId, RefreshTotalArenaRankingList, () => {
                UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
            });        
        }
    }

    private void RefreshStageRankingList()
    {
        var stageRankInfo = ServerNetworkManager.Instance.StageRank;

        if(stageRankInfo.MyRankingModel != null)      
        {
            int myStatValue = stageRankInfo.MyRankingModel.score;
            int myProgressValue = myStatValue / 100000;
            int myKillCount = myStatValue % 100000;
            
            int myWorldNumber = (myProgressValue - 1) / ServerNetworkManager.Instance.Setting.MaxStageInWorld + 1;
            int myStageNumber = (myProgressValue - 1) % ServerNetworkManager.Instance.Setting.MaxStageInWorld + 1;

            // Check max world number
            if(myWorldNumber > ServerNetworkManager.Instance.Setting.LastWorldIndex)
            {
                myWorldNumber = Mathf.Min(myWorldNumber, ServerNetworkManager.Instance.Setting.LastWorldIndex);
                myStageNumber = ServerNetworkManager.Instance.Setting.MaxStageInWorld;
            }

            myRankingStage.UpdateEntity(stageRankInfo.MyRankingModel.name, stageRankInfo.MyRankingModel.position, myWorldNumber, myStageNumber, myKillCount);
        }
        else
            myRankingStage.UpdateEntity(ServerNetworkManager.Instance.User.NickName, -1, 1, 1, 0);

        foreach(var tmp in rankingStageListParentTransform.GetComponentsInChildren<ListItemRanking>())
        {
            Destroy(tmp.gameObject);
        }

        foreach(var tmp in stageRankInfo.RankingList)
        {
            int statValue = tmp.score;
            int progressValue = statValue / 100000;
            int killCount = statValue % 100000;
            
            int worldNumber = (progressValue - 1) / ServerNetworkManager.Instance.Setting.MaxStageInWorld + 1;
            int stageNumber = (progressValue - 1) % ServerNetworkManager.Instance.Setting.MaxStageInWorld + 1;

            // Check max world number
            if(worldNumber > ServerNetworkManager.Instance.Setting.LastWorldIndex)
            {
                worldNumber = Mathf.Min(worldNumber, ServerNetworkManager.Instance.Setting.LastWorldIndex);
                stageNumber = ServerNetworkManager.Instance.Setting.MaxStageInWorld;
            }

            var transformToUse = Instantiate(listItemRankingPrefab);
            transformToUse.GetComponent<ListItemRanking>().UpdateEntity(
                tmp.name,
                tmp.position,
                worldNumber,
                stageNumber,
                killCount
            );

            transformToUse.SetParent(rankingStageListParentTransform, false);
        }
    }

    private void RefreshArenaRankingList()
    {
        var arenaRankInfo = ServerNetworkManager.Instance.ArenaRank;
        
        if(arenaRankInfo.MyRankingModel != null)
            myRankingArena.UpdateEntity(arenaRankInfo.MyRankingModel.name, arenaRankInfo.MyRankingModel.position, arenaRankInfo.MyRankingModel.score);
        else
            myRankingArena.UpdateEntity(ServerNetworkManager.Instance.User.NickName, -1, 0);

        foreach(var tmp in rankingArenaListParentTransform.GetComponentsInChildren<ListItemRanking>())
        {
            Destroy(tmp.gameObject);
        }

        foreach(var tmp in arenaRankInfo.RankingList)
        {
            var transformToUse = Instantiate(listItemRankingPrefab);
            transformToUse.GetComponent<ListItemRanking>().UpdateEntity(
                tmp.name,
                tmp.position,
                tmp.score
            );

            transformToUse.SetParent(rankingArenaListParentTransform, false);
        }
    }

    private void RefreshTotalArenaRankingList()
    {
        var totalArenaRankInfo = ServerNetworkManager.Instance.TotalArenaRank;

        if(totalArenaRankInfo.MyRankingModel != null)
            myRankingTotalArena.UpdateEntity(totalArenaRankInfo.MyRankingModel.name, totalArenaRankInfo.MyRankingModel.position, totalArenaRankInfo.MyRankingModel.score);
        else
            myRankingTotalArena.UpdateEntity(ServerNetworkManager.Instance.User.NickName, -1, 0);

        foreach(var tmp in rankingTotalArenaListParentTransform.GetComponentsInChildren<ListItemRanking>())
        {
            Destroy(tmp.gameObject);
        }

        foreach(var tmp in totalArenaRankInfo.RankingList)
        {
            var transformToUse = Instantiate(listItemRankingPrefab);
            transformToUse.GetComponent<ListItemRanking>().UpdateEntity(
                tmp.name,
                tmp.position,
                tmp.score
            );

            transformToUse.SetParent(rankingTotalArenaListParentTransform, false);
        }
    }

    private void FocusItems()
    {
        StartCoroutine(FocusItemsCoroutine());
    }

    private IEnumerator FocusItemsCoroutine()
    {
        if(currentTabId == 0)
        {
            rankingStageListParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Unrestricted;
            yield return new WaitForEndOfFrame();
            rankingStageListParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Elastic;

            Canvas.ForceUpdateCanvases();

            rankingStageListParentTransform.anchoredPosition = new Vector2(rankingStageListParentTransform.anchoredPosition.x, 0f);
        }
        else if(currentTabId == 1)
        {
            rankingArenaListParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Unrestricted;
            yield return new WaitForEndOfFrame();
            rankingArenaListParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Elastic;

            Canvas.ForceUpdateCanvases();

            rankingArenaListParentTransform.anchoredPosition = new Vector2(rankingArenaListParentTransform.anchoredPosition.x, 0f);
        }
        else if(currentTabId == 2)
        {
            rankingTotalArenaListParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Unrestricted;
            yield return new WaitForEndOfFrame();
            rankingTotalArenaListParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Elastic;

            Canvas.ForceUpdateCanvases();

            rankingTotalArenaListParentTransform.anchoredPosition = new Vector2(rankingTotalArenaListParentTransform.anchoredPosition.x, 0f);
        }
    }
}
