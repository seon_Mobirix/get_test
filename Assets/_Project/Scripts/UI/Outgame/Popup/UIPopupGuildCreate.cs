﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class UIPopupGuildCreate : UIPopup
{
    [SerializeField]
	private InputField guildNameTextField;
    [SerializeField]
	private InputField guildNoticeTextField;
    [SerializeField]
	private InputField guildRequiredStageProgressTextField;
    [SerializeField]
    private List<ListItemGuildFlag> listGuildFlag;
    [SerializeField]
	private Toggle privateToggle;
	[SerializeField]
	private Text createPriceText;

    private int selectedGuildFlagIndex = 0;
	
	public override void Open()
	{
		base.Open();
        guildRequiredStageProgressTextField.text = ServerNetworkManager.Instance.Setting.GuildOpenStageProgress.ToString();
        Refresh();
	}

    public override void Refresh()
    {
        // Set guild private setting
		this.privateToggle.isOn = false;

        guildNameTextField.text = "";
        guildNoticeTextField.text = "";
        guildRequiredStageProgressTextField.text = ServerNetworkManager.Instance.Setting.GuildOpenStageProgress.ToString();

        int flagIndex = 0;
        this.selectedGuildFlagIndex = 0;
        
        foreach(var tmp in listGuildFlag)
        {
            if(flagIndex == 0)
                tmp.UpdateEntity(flagIndex, true);
            else
                tmp.UpdateEntity(flagIndex, false);
                
            flagIndex++;
        }

		createPriceText.text = ServerNetworkManager.Instance.Setting.GuildCreatePrice.ToString();
    }

	public void OnCreateButtonClicked()
	{
        if(selectedGuildFlagIndex > -1 && selectedGuildFlagIndex < listGuildFlag.Count)
        {
            bool isGuildNameViolating = false;
            bool isGuildNoticeViolating = false;
            bool isGuildRequiredStageProgressViolating = false;

            foreach(var tmp in guildNameTextField.text)
            {
                if(!char.IsLetterOrDigit(tmp) && !char.IsWhiteSpace(tmp))
                    isGuildNameViolating = true;
            }

            if(guildNameTextField.text.Length < 2)
                isGuildNameViolating = true;

            if(guildNameTextField.text == "")
                isGuildNameViolating = true;

            if(guildNoticeTextField.text.Length > 100)
                isGuildNoticeViolating = true;

            var settingInfo = ServerNetworkManager.Instance.Setting;
            int maxStagePrgoress = settingInfo.LastWorldIndex * settingInfo.MaxStageInWorld;

            int requiredStageProgress = Convert.ToInt32(guildRequiredStageProgressTextField.text);
            
            if(requiredStageProgress < settingInfo.GuildOpenStageProgress || requiredStageProgress > maxStagePrgoress)
                isGuildRequiredStageProgressViolating = true;

            if(isGuildNameViolating)
                UIManager.Instance.ShowAlertLocalized("message_guild_name_limit", null, null);
            else if(isGuildNoticeViolating)
                UIManager.Instance.ShowAlertLocalized("message_guild_notice_format_wrong", null, null);
            else if(isGuildRequiredStageProgressViolating)
                UIManager.Instance.ShowAlertLocalized("message_guild_required_stage_progress_wrong", null, null);
            else if(!OutgameController.Instance.CheckText(guildNameTextField.text))
                UIManager.Instance.ShowAlertLocalized("message_guild_name_bad", null, null);
            else
                OutgameController.Instance.CreateGuild(guildNameTextField.text, selectedGuildFlagIndex, guildNoticeTextField.text, this.privateToggle.isOn, requiredStageProgress);
        }
        else
        {
            UIManager.Instance.ShowAlertLocalized("message_guild_guild_flag_index_wrong", null, null);
        }
	}

    public void RefreshSelectedGuildFlag(int selectedflagIndex)
    {
        // Check validate flag index
        if(selectedflagIndex < listGuildFlag.Count && selectedflagIndex > -1)
        {
            this.selectedGuildFlagIndex = selectedflagIndex;

            int flagIndex = 0;
            foreach(var tmp in listGuildFlag)
            {
                if(flagIndex == selectedflagIndex)
                    tmp.UpdateEntity(flagIndex, true);
                else
                    tmp.UpdateEntity(flagIndex, false);
                
                flagIndex ++;
            }
        }
    }
}