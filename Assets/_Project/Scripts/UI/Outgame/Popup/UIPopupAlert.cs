﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupAlert : UIPopup {

	[SerializeField]
	private Text contentText;
	[SerializeField]
	private Text acceptButtonText;
	[SerializeField]
	private Text cancelButtonText;

	private Action OnAcceptAction;
	private Action OnCancelAction;

	public override void Open()
	{
		Debug.LogWarning("UIPopupAlert shouldn't be open without parameters");
	}

	public void OpenWithValues(string textToUse, bool showYesNo, Action OnAccept, Action OnCancel)
	{
		base.Open();
		
		if(showYesNo)
		{
			this.acceptButtonText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_common_yes");
			if(this.cancelButtonText != null)
				this.cancelButtonText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_common_no");
		}
		else
		{
			this.acceptButtonText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_common_ok");
			if(this.cancelButtonText != null)
				this.cancelButtonText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_common_cancel");
		}
		
		contentText.text = textToUse;
		this.OnAcceptAction = OnAccept;
		this.OnCancelAction = OnCancel;
	}

	protected override void OnOpen()
	{
		// Do nothing
	}

	public void OnAcceptButton()
	{
		base.Close();
		if(OnAcceptAction != null)
			OnAcceptAction.Invoke();
	}

	public void OnCancelButton()
	{
		base.Close();
		if(OnCancelAction != null)
			OnCancelAction.Invoke();
	}
	
	public override void Close()
	{
		base.Close();
		if(OnCancelAction != null)
			OnCancelAction.Invoke();
		else if(OnAcceptAction != null)
			OnAcceptAction.Invoke();
	}

	public override void OnPopupCloseCompleted()
	{
		Destroy(this.gameObject);
	}

}