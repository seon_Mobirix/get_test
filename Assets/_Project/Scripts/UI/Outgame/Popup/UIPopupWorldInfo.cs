﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Numerics;
using System.Collections.Generic;
using System.Globalization;
using System.Collections;

public class UIPopupWorldInfo : UIPopup
{
    [SerializeField]
	private Text stageNameText;
    [SerializeField]
    private Transform prevStageButton;
    [SerializeField]
    private Transform nextStageButton;

    [SerializeField]
	private RectTransform stageListParentTransform;
    [SerializeField]
    private List<ListItemStage> stageInfoList;

    int currentWorldIdx = 1;
    int currentMobIdx = 1;

    public override void Open()
    {
        base.Open();

        currentWorldIdx = (ServerNetworkManager.Instance.User.SelectedStage - 1) / ServerNetworkManager.Instance.Setting.MaxStageInWorld + 1;
        currentMobIdx = (ServerNetworkManager.Instance.User.SelectedStage - 1) % ServerNetworkManager.Instance.Setting.MaxStageInWorld + 1;
        
        Refresh();
        
        FocusItems();
    }

    public override void Refresh()
    {
        stageNameText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "world_name"), currentWorldIdx);
    
        if(currentWorldIdx == 1)
        {
            prevStageButton.gameObject.SetActive(false);
            nextStageButton.gameObject.SetActive(true);
        }
        else
        {
            prevStageButton.gameObject.SetActive(true);
            
            if(currentWorldIdx == ServerNetworkManager.Instance.Setting.LastWorldIndex)
                nextStageButton.gameObject.SetActive(false);
            else
                nextStageButton.gameObject.SetActive(true);
        }

        var targetStageInfoList = BalanceInfoManager.Instance.StageInfoList.FindAll(n => n.RegionId == currentWorldIdx.ToString()).ToList();
        int idx = 0;
        var lastClearWorldIdx = (ServerNetworkManager.Instance.User.StageProgress - 1) / ServerNetworkManager.Instance.Setting.MaxStageInWorld + 1;
        var lastClearMobIdx = (ServerNetworkManager.Instance.User.StageProgress - 1) % ServerNetworkManager.Instance.Setting.MaxStageInWorld + 1;

        foreach(var tmp in targetStageInfoList)
        {
            bool isMoved = false;
            bool isLock = true;
            int mobIdx = idx + 1;
            int stageNumber = (currentWorldIdx - 1) * ServerNetworkManager.Instance.Setting.MaxStageInWorld + mobIdx;

            if(currentWorldIdx < lastClearWorldIdx)
                isLock = false;
            else if(currentWorldIdx == lastClearWorldIdx)
            {
                if(mobIdx <= lastClearMobIdx)
                    isLock = false;
            }

            if(ServerNetworkManager.Instance.User.SelectedStage == stageNumber)
                isMoved = true;
            
            string monsterId = currentWorldIdx.ToString("D2") + "_" + mobIdx.ToString("D2");
            var rewardGold = BigInteger.Parse(BalanceInfoManager.Instance.RewardGoldInfoList[int.Parse(tmp.RewardTier) - 1].Value, NumberStyles.Any, CultureInfo.InvariantCulture);
            stageInfoList[idx].UpdateEntity(stageNumber, mobIdx, monsterId, tmp.FarmItem, float.Parse(tmp.FarmChance, NumberStyles.Any, CultureInfo.InvariantCulture), rewardGold, isLock, isMoved);

            idx = idx + 1;
        }
    }

    public void OnPrevWorldButtonClicked()
    {
        if(currentWorldIdx == 1)
            return;
        else
        {
            currentWorldIdx = currentWorldIdx - 1;
            Refresh();
        }
    }

    public void OnNextWorldButtonClicked()
    {
        if(currentWorldIdx == ServerNetworkManager.Instance.Setting.LastWorldIndex)
            return;
        else
        {
            currentWorldIdx = currentWorldIdx + 1;
            Refresh();
        }
    }

    private double lastChange = -1;
    public void SelectedStage(int selectedStage)
    {
        var timeLeft = System.Math.Max(1.5d + lastChange - Time.realtimeSinceStartup, 0d);

        if(timeLeft > 0)
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), (int)timeLeft + 1));
        else
        {
            OutgameController.Instance.SelectStage(selectedStage, () => {
                currentWorldIdx = (selectedStage - 1) / ServerNetworkManager.Instance.Setting.MaxStageInWorld + 1;
                currentMobIdx = (selectedStage - 1) % ServerNetworkManager.Instance.Setting.MaxStageInWorld + 1;
                Refresh();
            });

            lastChange = Time.realtimeSinceStartup;
        }
    }

    private void FocusItems()
    {
        StartCoroutine(FocusItemsCoroutine());
    }

    private IEnumerator FocusItemsCoroutine()
    {
        stageListParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Unrestricted;
        yield return new WaitForEndOfFrame();
        stageListParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Elastic;

        Canvas.ForceUpdateCanvases();

		stageListParentTransform.anchoredPosition = new UnityEngine.Vector2(stageListParentTransform.anchoredPosition.x, 0f);
    

        var adjustValue = currentMobIdx * (stageListParentTransform.GetComponent<VerticalLayoutGroup>().spacing + stageInfoList[0].gameObject.GetComponent<RectTransform>().rect.height)
			+ stageListParentTransform.GetComponent<VerticalLayoutGroup>().padding.top
			+ stageListParentTransform.GetComponent<VerticalLayoutGroup>().padding.bottom
			- stageListParentTransform.GetComponent<VerticalLayoutGroup>().spacing
			- stageListParentTransform.parent.GetComponent<RectTransform>().sizeDelta.y;

		if(adjustValue <= 0f)
			adjustValue = 0f;

        // Focus item
		Canvas.ForceUpdateCanvases();
        
		stageListParentTransform.anchoredPosition = new UnityEngine.Vector2(0f, adjustValue);
    }
}
