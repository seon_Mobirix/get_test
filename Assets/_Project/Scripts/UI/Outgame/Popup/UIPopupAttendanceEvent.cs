﻿using UnityEngine;
using System;

public class UIPopupAttendanceEvent : UIPopup
{
    [SerializeField]
	private RectTransform eventRewardListParentTransform;
	[SerializeField]
	private Transform listItemEventRewardPrefab;

    private Action onClose = null;

    public override void Open()
	{
        Debug.LogWarning("UIPopupAttendanceEvent shouldn't be open without parameters");
    }

    public void OpenWithValues(Action onClose)
    {
        base.Open();
        		
        Refresh();

        this.onClose = onClose;
    }

    public override void Close()
	{
        base.Close();

        onClose?.Invoke();
    }

    public override void Refresh()
	{
		foreach(var tmp in eventRewardListParentTransform.GetComponentsInChildren<ListItemEventReward>())
		{
			Destroy(tmp.gameObject);
		}

        int index = 0;
        
        // Check day change
        bool isAvailable = false;
        var currentTimeStamp = DateTimeOffset.Now.ToUnixTimeSeconds();
        var currentTimeStampDay = currentTimeStamp / (60 * 60 * 24); 

        if(currentTimeStampDay > ServerNetworkManager.Instance.LastAttendanceTimeStampDay)
            isAvailable = true;
        
        foreach(var tmp in ServerNetworkManager.Instance.AttendanceEventInfo.AttendanceEventRewardList)
		{
			var transformToUse = Instantiate(listItemEventRewardPrefab);
            
            transformToUse.GetComponent<ListItemEventReward>().UpdateEntity(
                index,
                tmp.ItemId,
                tmp.ItemCode,
                tmp.ItemCount,
                isAvailable
            );

			transformToUse.SetParent(eventRewardListParentTransform, false);

            index = index + 1;
		}
    }

    public void RequestEventReward(int rewardIndex)
    {
        OutgameController.Instance.RequestAttendanceEventReward(rewardIndex, () => {
            UIManager.Instance.ShowAlertLocalized("message_thank_you", null, null);
            this.Close();
        });
    }
}
