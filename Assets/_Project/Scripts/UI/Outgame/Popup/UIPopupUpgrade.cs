﻿using UnityEngine;
using UnityEngine.UI;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;

public class UIPopupUpgrade : UIPopup
{
    [SerializeField]
	private UITab uiTab;

    [SerializeField]
    private Transform statRedDot;
    [SerializeField]
    private Transform skillRedDot;

    [SerializeField]
    private List<ListItemStat> statInfoList;
    [SerializeField]
    private List<ListItemSkill> skillInfoList;

    [Title("Rect Objects")]
	[SerializeField]
	private ScrollRect statRect;

    [Title("Lists")]
    [SerializeField]
    private RectTransform statList;

    private int currentTabId = 0;
    private bool isChanged = false;

    public override void Open()
	{
		Debug.LogWarning("UIPopupUpgrade shouldn't be open without parameters");
	}

    public void OpenWithValues(int tabId)
	{
		base.Open();

        isChanged = false;
		currentTabId = tabId;
		uiTab.OnTabButton(tabId);

		Refresh();

        FocusItems();
	}

    public override void Close()
    {
        base.Close();

        // Need to sync data so far
        if(isChanged)
            OutgameController.Instance.SyncPlayerData(false, "UIPopupUpgrade");

        isChanged = false;
        
        if(GameObject.FindObjectOfType<UIViewMain>() != null)
            GameObject.FindObjectOfType<UIViewMain>().ShowRightTopContent();
    }

    public void CloseWithOutShowRightTopContent()
    {
        base.Close();

        // Need to sync data so far
        if(isChanged)
            OutgameController.Instance.SyncPlayerData(false, "UIPopupUpgrade");

        isChanged = false;
    }

    public void OnTabButtonClicked(int tabId)
	{
		currentTabId = tabId;
        uiTab.OnTabButton(tabId);

        Refresh();
	}

    public override void Refresh()
    {
        RefreshStatReddot();
        RefreshSkillReddot();

        if(currentTabId == 0)
        {
            statInfoList[0].UpdateEntity(StatType.ATTACK_POWER, ServerNetworkManager.Instance.User.Upgrade.BaseAttackLevel, false, false);
            statInfoList[1].UpdateEntity(StatType.CRITICAL_POWER, ServerNetworkManager.Instance.User.Upgrade.CriticalAttackLevel, false, false);
            statInfoList[2].UpdateEntity(StatType.CRITICAL_ATTACK_CHANCE, ServerNetworkManager.Instance.User.Upgrade.CriticalChanceLevel, false, false);
            statInfoList[3].UpdateEntity(StatType.SUPER_POWER, ServerNetworkManager.Instance.User.Upgrade.SuperAttackLevel, false, false);
            statInfoList[4].UpdateEntity(StatType.SUPER_ATTACK_CHANCE, ServerNetworkManager.Instance.User.Upgrade.SuperChanceLevel, false, false);
            
            // Check super attack chane 100% or not
            if(ServerNetworkManager.Instance.User.Upgrade.SuperChance < 1f)
            {
                statInfoList[5].UpdateEntity(StatType.ULTIMATE_POWER, ServerNetworkManager.Instance.User.Upgrade.UltimateAttackLevel, true, false);
                statInfoList[6].UpdateEntity(StatType.ULTIMATE_ATTACK_CHANCE, ServerNetworkManager.Instance.User.Upgrade.UltimateChanceLevel, true, false);
            }
            else
            {
                statInfoList[5].UpdateEntity(StatType.ULTIMATE_POWER, ServerNetworkManager.Instance.User.Upgrade.UltimateAttackLevel, false, false);
                statInfoList[6].UpdateEntity(StatType.ULTIMATE_ATTACK_CHANCE, ServerNetworkManager.Instance.User.Upgrade.UltimateChanceLevel, false, false);
            }

            // Check ultimate attack chane 100% or not
            if(ServerNetworkManager.Instance.User.Upgrade.UltimateChance < 1f)
            {
                statInfoList[7].UpdateEntity(StatType.HYPER_POWER, ServerNetworkManager.Instance.User.Upgrade.HyperAttackLevel, true, false);
                statInfoList[8].UpdateEntity(StatType.HYPER_ATTACK_CHANCE, ServerNetworkManager.Instance.User.Upgrade.HyperChanceLevel, true, false);
            }
            else
            {
                statInfoList[7].UpdateEntity(StatType.HYPER_POWER, ServerNetworkManager.Instance.User.Upgrade.HyperAttackLevel, false, false);
                statInfoList[8].UpdateEntity(StatType.HYPER_ATTACK_CHANCE, ServerNetworkManager.Instance.User.Upgrade.HyperChanceLevel, false, false);
            }
        }
        else if(currentTabId == 1)
        {
            for(int i = 0; i < this.skillInfoList.Count; i++)
            {
                skillInfoList[i].UpdateEntity(i, false);
            }
        }
    }

    private void RefreshStatReddot()
    {
        bool isUpgradeAvailable = false;
        BigInteger currentGold = ServerNetworkManager.Instance.Inventory.Gold;
        
        if(currentGold >= ServerNetworkManager.Instance.User.Upgrade.BaseAttackLevelupPrice && ServerNetworkManager.Instance.User.Upgrade.BaseAttackLevelupPrice != -1 && ServerNetworkManager.Instance.User.Upgrade.BaseAttackLevel < ServerNetworkManager.Instance.Setting.MaxStatLevel)
            isUpgradeAvailable = true;
        else if(currentGold >= ServerNetworkManager.Instance.User.Upgrade.CriticalAttackLevelupPrice && ServerNetworkManager.Instance.User.Upgrade.CriticalAttackLevelupPrice != -1 && ServerNetworkManager.Instance.User.Upgrade.CriticalAttackLevel < ServerNetworkManager.Instance.Setting.MaxStatLevel)
            isUpgradeAvailable = true;
        else if(currentGold >= ServerNetworkManager.Instance.User.Upgrade.CriticalChanceLevelupPrice && ServerNetworkManager.Instance.User.Upgrade.CriticalChanceLevelupPrice != -1 && ServerNetworkManager.Instance.User.Upgrade.CriticalChance < 1f)
            isUpgradeAvailable = true;
        else if(currentGold >= ServerNetworkManager.Instance.User.Upgrade.SuperAttackLevelupPrice && ServerNetworkManager.Instance.User.Upgrade.SuperAttackLevelupPrice != -1 && ServerNetworkManager.Instance.User.Upgrade.SuperAttackLevel < ServerNetworkManager.Instance.Setting.MaxStatLevel)
            isUpgradeAvailable = true;
        else if(currentGold >= ServerNetworkManager.Instance.User.Upgrade.SuperChanceLevelupPrice && ServerNetworkManager.Instance.User.Upgrade.SuperChanceLevelupPrice != -1 && ServerNetworkManager.Instance.User.Upgrade.SuperChance < 1f)
            isUpgradeAvailable = true;
        else if(ServerNetworkManager.Instance.User.Upgrade.SuperChance >= 1f && currentGold >= ServerNetworkManager.Instance.User.Upgrade.UltimateAttackLevelupPrice && ServerNetworkManager.Instance.User.Upgrade.UltimateAttackLevelupPrice != -1 && ServerNetworkManager.Instance.User.Upgrade.UltimateAttackLevel < ServerNetworkManager.Instance.Setting.MaxStatLevel)
            isUpgradeAvailable = true;
        else if(ServerNetworkManager.Instance.User.Upgrade.SuperChance >= 1f && currentGold >= ServerNetworkManager.Instance.User.Upgrade.UltimateChanceLevelupPrice && ServerNetworkManager.Instance.User.Upgrade.UltimateChanceLevelupPrice != -1 && ServerNetworkManager.Instance.User.Upgrade.UltimateChance < 1f)
            isUpgradeAvailable = true;
        else if(ServerNetworkManager.Instance.Setting.IsHyperOpen && ServerNetworkManager.Instance.User.Upgrade.UltimateChance >= 1f && currentGold >= ServerNetworkManager.Instance.User.Upgrade.HyperAttackLevelupPrice && ServerNetworkManager.Instance.User.Upgrade.HyperAttackLevelupPrice != -1 && ServerNetworkManager.Instance.User.Upgrade.HyperAttackLevel < ServerNetworkManager.Instance.Setting.MaxStatLevel)
            isUpgradeAvailable = true;
        else if(ServerNetworkManager.Instance.Setting.IsHyperOpen && ServerNetworkManager.Instance.User.Upgrade.UltimateChance >= 1f && currentGold >= ServerNetworkManager.Instance.User.Upgrade.HyperChanceLevelupPrice && ServerNetworkManager.Instance.User.Upgrade.HyperChanceLevelupPrice != -1 && ServerNetworkManager.Instance.User.Upgrade.HyperChance < 1f)
            isUpgradeAvailable = true;

        if(isUpgradeAvailable)
            statRedDot.gameObject.SetActive(true);
        else
            statRedDot.gameObject.SetActive(false);
    }

    private void RefreshSkillReddot()
    {
        bool isUpgradeAvailable = false;
        BigInteger currentGold = ServerNetworkManager.Instance.Inventory.Gold;

        if(currentGold >= ServerNetworkManager.Instance.User.SkillInfo[0].GoldLevelupCost && ServerNetworkManager.Instance.User.SkillInfo[0].GoldLevelupCost != -1 && ServerNetworkManager.Instance.User.SkillInfo[0].Level < ServerNetworkManager.Instance.Setting.MaxSkillLevel)
            isUpgradeAvailable = true;
        else if(currentGold >= ServerNetworkManager.Instance.User.SkillInfo[1].GoldLevelupCost && ServerNetworkManager.Instance.User.SkillInfo[1].GoldLevelupCost != -1 && ServerNetworkManager.Instance.User.SkillInfo[1].Level < ServerNetworkManager.Instance.Setting.MaxSkillLevel)
            isUpgradeAvailable = true;
        else if(currentGold >= ServerNetworkManager.Instance.User.SkillInfo[2].GoldLevelupCost && ServerNetworkManager.Instance.User.SkillInfo[2].GoldLevelupCost != -1 && ServerNetworkManager.Instance.User.SkillInfo[2].Level < ServerNetworkManager.Instance.Setting.MaxSkillLevel)
            isUpgradeAvailable = true;
        
        if(isUpgradeAvailable)
            skillRedDot.gameObject.SetActive(true);
        else
            skillRedDot.gameObject.SetActive(false);
    }

    private void RefreshWithStatUpgradeEffect(StatType statType)
    {
        if(statType == StatType.ATTACK_POWER)
            statInfoList[0].UpdateEntity(StatType.ATTACK_POWER, ServerNetworkManager.Instance.User.Upgrade.BaseAttackLevel, false, true);
        else
            statInfoList[0].UpdateEntity(StatType.ATTACK_POWER, ServerNetworkManager.Instance.User.Upgrade.BaseAttackLevel, false, false);

        if(statType == StatType.CRITICAL_POWER)
            statInfoList[1].UpdateEntity(StatType.CRITICAL_POWER, ServerNetworkManager.Instance.User.Upgrade.CriticalAttackLevel, false, true);
        else
            statInfoList[1].UpdateEntity(StatType.CRITICAL_POWER, ServerNetworkManager.Instance.User.Upgrade.CriticalAttackLevel, false, false);

        if(statType == StatType.CRITICAL_ATTACK_CHANCE)
            statInfoList[2].UpdateEntity(StatType.CRITICAL_ATTACK_CHANCE, ServerNetworkManager.Instance.User.Upgrade.CriticalChanceLevel, false, true);
        else
            statInfoList[2].UpdateEntity(StatType.CRITICAL_ATTACK_CHANCE, ServerNetworkManager.Instance.User.Upgrade.CriticalChanceLevel, false, false);

        if(statType == StatType.SUPER_POWER)
            statInfoList[3].UpdateEntity(StatType.SUPER_POWER, ServerNetworkManager.Instance.User.Upgrade.SuperAttackLevel, false, true);
        else
            statInfoList[3].UpdateEntity(StatType.SUPER_POWER, ServerNetworkManager.Instance.User.Upgrade.SuperAttackLevel, false, false);
        
        if(statType == StatType.SUPER_ATTACK_CHANCE)
            statInfoList[4].UpdateEntity(StatType.SUPER_ATTACK_CHANCE, ServerNetworkManager.Instance.User.Upgrade.SuperChanceLevel, false, true);
        else
            statInfoList[4].UpdateEntity(StatType.SUPER_ATTACK_CHANCE, ServerNetworkManager.Instance.User.Upgrade.SuperChanceLevel, false, false);
        
        // Check super attack chane 100% or not
        if(ServerNetworkManager.Instance.User.Upgrade.SuperChance < 1f)
        {
            statInfoList[5].UpdateEntity(StatType.ULTIMATE_POWER, ServerNetworkManager.Instance.User.Upgrade.UltimateAttackLevel, true, false);
            statInfoList[6].UpdateEntity(StatType.ULTIMATE_ATTACK_CHANCE, ServerNetworkManager.Instance.User.Upgrade.UltimateChanceLevel, true, false);
        }
        else
        {
            if(statType == StatType.ULTIMATE_POWER)
                statInfoList[5].UpdateEntity(StatType.ULTIMATE_POWER, ServerNetworkManager.Instance.User.Upgrade.UltimateAttackLevel, false, true);
            else
                statInfoList[5].UpdateEntity(StatType.ULTIMATE_POWER, ServerNetworkManager.Instance.User.Upgrade.UltimateAttackLevel, false, false);

            if(statType == StatType.ULTIMATE_ATTACK_CHANCE)
                statInfoList[6].UpdateEntity(StatType.ULTIMATE_ATTACK_CHANCE, ServerNetworkManager.Instance.User.Upgrade.UltimateChanceLevel, false, true);
            else
                statInfoList[6].UpdateEntity(StatType.ULTIMATE_ATTACK_CHANCE, ServerNetworkManager.Instance.User.Upgrade.UltimateChanceLevel, false, false);
        }

        // Check ultimate attack chane 100% or not
        if(ServerNetworkManager.Instance.User.Upgrade.UltimateChance < 1f)
        {
            statInfoList[7].UpdateEntity(StatType.HYPER_POWER, ServerNetworkManager.Instance.User.Upgrade.HyperAttackLevel, true, false);
            statInfoList[8].UpdateEntity(StatType.HYPER_ATTACK_CHANCE, ServerNetworkManager.Instance.User.Upgrade.HyperChanceLevel, true, false);
        }
        else
        {
            if(statType == StatType.HYPER_POWER)
                statInfoList[7].UpdateEntity(StatType.HYPER_POWER, ServerNetworkManager.Instance.User.Upgrade.HyperAttackLevel, false, true);
            else
                statInfoList[7].UpdateEntity(StatType.HYPER_POWER, ServerNetworkManager.Instance.User.Upgrade.HyperAttackLevel, false, false);

            if(statType == StatType.HYPER_ATTACK_CHANCE)
                statInfoList[8].UpdateEntity(StatType.HYPER_ATTACK_CHANCE, ServerNetworkManager.Instance.User.Upgrade.HyperChanceLevel, false, true);
            else
                statInfoList[8].UpdateEntity(StatType.HYPER_ATTACK_CHANCE, ServerNetworkManager.Instance.User.Upgrade.HyperChanceLevel, false, false);
        }
    }

    private void RefreshWithSkillUpgradeEffect(int skillIndex)
    {
        for(int i = 0; i < this.skillInfoList.Count; i++)
        {
            if(i == skillIndex)
                skillInfoList[i].UpdateEntity(i, true);
            else
                skillInfoList[i].UpdateEntity(i, false);
        }
    }

    public void RequestStatUpgrade(StatType statType)
    {
        OutgameController.Instance.UpgradeStat(statType, () => {
            isChanged = true; 
            RefreshWithStatUpgradeEffect(statType);
            RefreshStatReddot();
            RefreshSkillReddot();
        });
    }

    public void RequestSkillUpgrade(int skillIndex)
    {
        OutgameController.Instance.UpgradeSkill(skillIndex, () => {
            isChanged = true; 
            RefreshWithSkillUpgradeEffect(skillIndex);
            RefreshStatReddot();
            RefreshSkillReddot();
        });
    }

    private void FocusItems()
    {
        StartCoroutine(FocusItemsCoroutine());
    }

    private IEnumerator FocusItemsCoroutine()
    {
        statRect.movementType = ScrollRect.MovementType.Unrestricted;
        yield return new WaitForEndOfFrame();
        statRect.movementType = ScrollRect.MovementType.Elastic;

        Canvas.ForceUpdateCanvases();

		statList.anchoredPosition = new UnityEngine.Vector2(statList.anchoredPosition.x, 0f);
    }

}
