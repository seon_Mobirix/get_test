﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIPopupGrowthSupport : UIPopup
{	
	[SerializeField]
	private Transform growthSupportPackage1BuyButton;
	[SerializeField]
	private Transform growthSupportPackage2BuyButton;
    [SerializeField]
	private RectTransform growthSupportRewardListParentTransform;
	[SerializeField]
	private Transform listItemGrowthSupportRewardPrefab;

	public override void Open()
	{
        base.Open();
        Refresh();
		FocusItems();
    }

	public override void Refresh()
	{
		if(ServerNetworkManager.Instance.PurchasedGrowthPackageList.Exists(n => n == "bundle_package_growth_1"))
			growthSupportPackage1BuyButton.gameObject.SetActive(false);
		else
			growthSupportPackage1BuyButton.gameObject.SetActive(true);

		if(ServerNetworkManager.Instance.PurchasedGrowthPackageList.Exists(n => n == "bundle_package_growth_2"))
			growthSupportPackage2BuyButton.gameObject.SetActive(false);
		else
			growthSupportPackage2BuyButton.gameObject.SetActive(true);

		foreach(var tmp in growthSupportRewardListParentTransform.GetComponentsInChildren<ListItemGrowthSupportReward>())
		{
			Destroy(tmp.gameObject);
		}

		for(int i = 10; i <= ServerNetworkManager.Instance.Setting.MaxGrowthSupportStage; i+= 10)
		{
			bool isGrowthSupport1Lock = true;
			bool isGrowthSupport2Lock = true;
			bool isOwnedGrowthSupport1 = false;
			bool isOwnedGrowthSupport2 = false;
			bool isAvailableGrowthSupport1 = true;
			bool isAvailableGrowthSupport2 = true;

			// Check stage progress
			if(ServerNetworkManager.Instance.User.StageProgress >= i)
			{
				isGrowthSupport1Lock = false;
				isGrowthSupport2Lock = false;
			}

			// Check user have growth support package 1 or not
			if(ServerNetworkManager.Instance.PurchasedGrowthPackageList.Exists(n => n == "bundle_package_growth_1"))
			{
				isOwnedGrowthSupport1 = true;

				// Check user already gain reward or not
				var growthSupportRewardId = string.Format("growth_support_01_{0}", i);

				if(ServerNetworkManager.Instance.ReceivedGrowthSupportFirstRewardList.Exists(n => n == growthSupportRewardId))
					isAvailableGrowthSupport1 = false;
			}

			// Check user have growth support package 2 or not
			if(ServerNetworkManager.Instance.PurchasedGrowthPackageList.Exists(n => n == "bundle_package_growth_2"))
			{
				isOwnedGrowthSupport2 = true;

				// Check user already gain reward or not
				var growthSupportRewardId = string.Format("growth_support_02_{0}", i);

				if(ServerNetworkManager.Instance.ReceivedGrowthSupportSecondRewardList.Exists(n => n == growthSupportRewardId))
					isAvailableGrowthSupport2 = false;
			}
			
			var transformToUse = Instantiate(listItemGrowthSupportRewardPrefab);
			transformToUse.GetComponent<ListItemGrowthSupportReward>().UpdateEntity(
				i,
				isOwnedGrowthSupport1,
				isAvailableGrowthSupport1,
				isGrowthSupport1Lock,
				isOwnedGrowthSupport2,
				isAvailableGrowthSupport2,
				isGrowthSupport2Lock
			);
			transformToUse.SetParent(growthSupportRewardListParentTransform, false);
		}
	}

	private void FocusItems()
    {
        StartCoroutine(FocusItemsCoroutine());
    }

    private IEnumerator FocusItemsCoroutine()
    {
        growthSupportRewardListParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Unrestricted;
        yield return new WaitForEndOfFrame();
        growthSupportRewardListParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Elastic;

        Canvas.ForceUpdateCanvases();

		growthSupportRewardListParentTransform.anchoredPosition = new Vector2(growthSupportRewardListParentTransform.anchoredPosition.x, 0f);
    }

	public void OnBuyButtonClicked(int growthSupportPackageNumber)
	{
		string targetPackageId = $"bundle_package_growth_{growthSupportPackageNumber}";
		
		OutgameController.Instance.ShowGrowthSupportPurchasePopup(targetPackageId);
	}
}
