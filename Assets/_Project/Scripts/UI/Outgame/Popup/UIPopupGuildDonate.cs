﻿using UnityEngine;
using UnityEngine.UI;

public class UIPopupGuildDonate : UIPopup
{
	[SerializeField]
	private Text contentText;
	[SerializeField]
	private Text donateGemStartValueText;
	[SerializeField]
	private Text donateGemMaxValueText;
    [SerializeField]
	private Text donateValueText;
    [SerializeField]
    private Slider donateValueSlider;
	[SerializeField]
	private Button donateButton;
	
	public override void Open()
	{
		base.Open();

		var message = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "desc_guild_donate");
		contentText.text = string.Format(message, ServerNetworkManager.Instance.Setting.LimitGuildDonate, ServerNetworkManager.Instance.Setting.MaxGuildDonateValue);
        
		var myGuildData = ServerNetworkManager.Instance.GuildData;
		int maxValue = 10;

		if(myGuildData != null)
		{
			if(myGuildData.GuildLevel >= ServerNetworkManager.Instance.Setting.GuildMaxLevel - 1)
			{
				int remainExp = myGuildData.GuildNextExp - myGuildData.GuildExp;
				maxValue = Mathf.Max(1, Mathf.FloorToInt(remainExp / 10));
			}
		}

		donateValueSlider.minValue = 1;
		donateValueSlider.maxValue = Mathf.Min(maxValue, 10);
		donateValueSlider.value = 1;
		
		donateGemStartValueText.text = "100";
		donateGemMaxValueText.text = (donateValueSlider.maxValue * 100).ToString();

		if(ServerNetworkManager.Instance.TotalGem < 100)
			donateButton.interactable = false;
		else
			donateButton.interactable = true;

		donateValueText.text = "100";
	}

    public void OnDonateGoldSlider(float value)
	{
		int gemAmount = Mathf.FloorToInt(value * 100);

		if(value == 0)
			donateButton.interactable = false;
		else
		{
			if(ServerNetworkManager.Instance.TotalGem < gemAmount)
				donateButton.interactable = false;
			else
				donateButton.interactable = true;
		}

		donateValueText.text = gemAmount.ToString();
	}

	public void OnDonateButtonClicked()
	{
		OutgameController.Instance.GuildDonate(int.Parse(donateValueText.text));
	}
}
