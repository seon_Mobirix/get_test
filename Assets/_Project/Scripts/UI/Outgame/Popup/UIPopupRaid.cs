﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;
using System.Linq;

public class UIPopupRaid : UIPopup
{
    private bool isSellMode = false;
    private bool isChanged = false;

    [SerializeField]
	private RectTransform trophyParentTransform;

    [SerializeField]
    private Transform listItemTrophyPrefab;

    [SerializeField]
    private List<ListItemTrophySlot> trophySlotList = new List<ListItemTrophySlot>();

    [SerializeField]
    private Transform smallChatLog;

    [SerializeField]
    private Transform largeChatLog;

    [SerializeField]
    private Text sellText = null;

    [SerializeField]
    private Text leftJoinCountText = null;

    [SerializeField]
    private Text leftCreateCountText = null;

    [SerializeField]
    private Button joinButton = null;

    [SerializeField]
    private Button createButton = null;

    [SerializeField]
    private Text leftTimeInfoText = null;

    [SerializeField]
    private InputField raidIdInput;

    [SerializeField]
    private InputField floorInput;

    [SerializeField]
    private Text smallChatTextArea = null;
    [SerializeField]
    private Text largeChatTextArea = null;

    [SerializeField]
    private InputField chatInput = null;

    private string previousChatLog = "";

    // Cool time for buttons
    private double lastRaidJoinClickTimeStampMilliseconds;
    private double lastRaidCreatClickTimeStampMilliseconds;

	protected override void OnOpen()
    {
        previousChatLog = "";
        smallChatTextArea.text = "";
        largeChatTextArea.text = "";

        floorInput.text = RaidManager.Instance.CurrentRaidFloorCache.ToString();

        smallChatLog.gameObject.SetActive(true);
        largeChatLog.gameObject.SetActive(false);

        Refresh();
        RefreshTrophyInitial();

        base.OnOpen();
    }

    public override void Close()
	{
		base.Close();

        // Need to sync data so far
        if(isChanged)
            OutgameController.Instance.SyncPlayerData(false, "UIPopupRaid");

        isChanged = false;
        
        OutgameController.Instance.LeaveRaid();
        
        if(GameObject.FindObjectOfType<UIViewMain>() != null)
        {
            GameObject.FindObjectOfType<UIViewMain>().Refresh();
            GameObject.FindObjectOfType<UIViewMain>().SetRaidTicketAmountInfoActive(false);
            GameObject.FindObjectOfType<UIViewMain>().ShowRightTopContent();
        }
	}
    
    public void CloseWithOutShowRightTopContent()
    {
        base.Close();

        // Need to sync data so far
        if(isChanged)
            OutgameController.Instance.SyncPlayerData(false, "UIPopupRaid");

        isChanged = false;

        OutgameController.Instance.LeaveRaid();

        if(GameObject.FindObjectOfType<UIViewMain>() != null)
            GameObject.FindObjectOfType<UIViewMain>().Refresh();
    }

	float passed = 0f;
    float passedLocalCacheForceSave = 0f;
    float lastCheck = float.MinValue;
    private void Update()
    {
        passed += Time.deltaTime;
        if(passed >= 0.5f && this.gameObject.activeSelf)
        {
            if(!previousChatLog.Equals(ChatManager.Instance.ChatLogSub))
            {
                RefreshChatText();
                previousChatLog = ChatManager.Instance.ChatLogSub;
            }
            
            passed = 0f;
        }

        passedLocalCacheForceSave += Time.deltaTime;
        if(passedLocalCacheForceSave >= 1f && this.gameObject.activeSelf)
        {
            OutgameController.Instance.SaveLocalCache(isChanged);
            passedLocalCacheForceSave = 0f;
        }

        // For sync data on trophies
        if(isChanged)
		{
			if(lastCheck + ServerNetworkManager.Instance.Setting.AutoSyncPeriod < Time.realtimeSinceStartup)
			{
				// Need to sync data so far
				OutgameController.Instance.SyncPlayerData(true, "UIPopupRaid");
				isChanged = false;

				lastCheck = Time.realtimeSinceStartup;
			}
		}
    }

	public override void Refresh()
	{
        joinButton.interactable = true;
        createButton.interactable = true;

        if(ServerNetworkManager.Instance.Raid.DailyLimitLeft > 0)
        {
            leftJoinCountText.text = string.Format(
                LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_raid_join"),
                ServerNetworkManager.Instance.Raid.DailyLimitLeft
            );

            leftCreateCountText.text = string.Format(
                LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_raid_create"),
                ServerNetworkManager.Instance.Raid.DailyLimitLeft
            );
        }
        else
        {
            var ticketNeeded = 50 * (ServerNetworkManager.Instance.Raid.DailyLimitLeft * -1 + 1);
            
            leftJoinCountText.text = string.Format(
                LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_raid_join_ticket"),
                ticketNeeded
            );

            leftCreateCountText.text = string.Format(
                LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_raid_create_ticket"),
                ticketNeeded
            );
        }

        var offset = DateTimeOffset.Now.Offset;
        if(offset.TotalSeconds < 0)
            offset += new TimeSpan(24, 0, 0);
        if(offset.Hours < 0)
            offset = new TimeSpan(24, 0, 0) - offset;

        leftTimeInfoText.text = string.Format(
            LanguageManager.Instance.GetTextData(LanguageDataType.UI, "desc_raid_reset"),
            offset
        );

        RefreshChatText();
	}

    private void RefreshChatText()
    {
        smallChatTextArea.text = ChatManager.Instance.ChatLogSub;
        largeChatTextArea.text = ChatManager.Instance.ChatLogSub;
    }

    private void RefreshTrophyInitial()
    {
        foreach(var tmp in trophyParentTransform.GetComponentsInChildren<ListItemTrophy>())
        {
            Destroy(tmp.gameObject);
        }

        foreach(var tmp in BalanceInfoManager.Instance.TrophyInfoList)
        {				
            var tmpTrophyModel = ServerNetworkManager.Instance.Inventory.TrophyList.FirstOrDefault(n => n.Id == tmp.Id);
            
            // Check user owned this item
            if(tmpTrophyModel != null)
            {
                var transformToUse = Instantiate(listItemTrophyPrefab);
                transformToUse.GetComponent<ListItemTrophy>().UpdateEntity(
                    isSellMode,
                    tmp.Id, 
                    tmpTrophyModel.Level,
                    tmpTrophyModel.Count,
                    tmpTrophyModel.TrophySellReward,
                    true,
                    ServerNetworkManager.Instance.Inventory.IsEquipTrophy(tmpTrophyModel.Id),
                    false
                );
                transformToUse.SetParent(trophyParentTransform, false);
            }
            else
            {
                var transformToUse = Instantiate(listItemTrophyPrefab);
                transformToUse.GetComponent<ListItemTrophy>().UpdateEntity(
                    isSellMode,
                    tmp.Id, 
                    1,
                    0,
                    0,
                    false,
                    false,
                    false
                );
                transformToUse.SetParent(trophyParentTransform, false);
            }
        }

        if(isSellMode)
            sellText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_trophy_manage");
        else
            sellText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_trophy_sell");

        RefreshTrophySlot();
    }

    private void RefreshTrophyNonDestuctive(bool isUpgradeSuccess)
    {
        foreach(var tmp in trophyParentTransform.GetComponentsInChildren<ListItemTrophy>())
        {
            var tmpTrophyModel = ServerNetworkManager.Instance.Inventory.TrophyList.FirstOrDefault(n => n.Id == tmp.TrophyId);

             // Check user owned this item
            if(tmpTrophyModel != null)
            {
                tmp.UpdateEntity(
                    isSellMode,
                    tmp.TrophyId, 
                    tmpTrophyModel.Level,
                    tmpTrophyModel.Count,
                    tmpTrophyModel.TrophySellReward,
                    true,
                    ServerNetworkManager.Instance.Inventory.IsEquipTrophy(tmpTrophyModel.Id),
                    isUpgradeSuccess
                );
            }
            else
            {
                tmp.UpdateEntity(
                    isSellMode,
                    tmp.TrophyId, 
                    1,
                    0,
                    0,
                    false,
                    false,
                    false
                );
            }
        }

        if(isSellMode)
            sellText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_trophy_manage");
        else
            sellText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_trophy_sell");

        RefreshTrophySlot();
    }

    private void RefreshTrophySlot()
    {
        int i = 0;
        foreach(var tmp in ServerNetworkManager.Instance.Inventory.SelectedTrophyList)
        {
            trophySlotList[i].UpdateEntity(false, "", true, tmp.Id);
            i++;
            if(i == 8)
                break;
        }

        for(; i < 8; i++)
        {
            if(i == 7)
            {
                if(int.Parse(ServerNetworkManager.Instance.Inventory.BestClass.Id.Substring(6)) >= 25)
                    trophySlotList[i].UpdateEntity(false, "", false, "");
                else
                    trophySlotList[i].UpdateEntity(true, "Y", false, "");
            }
            else if(i == 6)
            {
                if(int.Parse(ServerNetworkManager.Instance.Inventory.BestClass.Id.Substring(6)) >= 19)
                    trophySlotList[i].UpdateEntity(false, "", false, "");
                else
                    trophySlotList[i].UpdateEntity(true, "U", false, "");
            } 
            else if(i == 5)
            {
                if(int.Parse(ServerNetworkManager.Instance.Inventory.BestClass.Id.Substring(6)) >= 16)
                    trophySlotList[i].UpdateEntity(false, "", false, "");
                else
                    trophySlotList[i].UpdateEntity(true, "R", false, "");
            }
            else if(i == 4)
            {
                if(int.Parse(ServerNetworkManager.Instance.Inventory.BestClass.Id.Substring(6)) >= 13)
                    trophySlotList[i].UpdateEntity(false, "", false, "");
                else
                    trophySlotList[i].UpdateEntity(true, "S", false, "");
            }
            else if(i == 3)
            {
                if(int.Parse(ServerNetworkManager.Instance.Inventory.BestClass.Id.Substring(6)) >= 10)
                    trophySlotList[i].UpdateEntity(false, "", false, "");
                else
                    trophySlotList[i].UpdateEntity(true, "A", false, "");
            }
            else
            {
                trophySlotList[i].UpdateEntity(false, "", false, "");
            }
        }
    }

#region ButtonCallBack

    public void OnOpenLargeChatButtonClicked()
    {
        smallChatLog.gameObject.SetActive(false);
        largeChatLog.gameObject.SetActive(true);
    }

    public void OnCloseLargeChatButtonClicked()
    {
        smallChatLog.gameObject.SetActive(true);
        largeChatLog.gameObject.SetActive(false);
    }

    public void OnSendButtonClicked()
	{
        if(chatInput.text == "")
            return;

        if(ServerNetworkManager.Instance.IsChatBanned)
        {
            UIManager.Instance.ShowAlertLocalized("message_chat_banned", null, null);
            return;
        }
        
        var ranking = "?";
        if(ServerNetworkManager.Instance.RankingPosition != 0)
        {
            ranking = ChatManager.AddOrdinal(ServerNetworkManager.Instance.RankingPosition);
        }
        
        var rankColor = InterSceneManager.Instance.GetColorForRank(ServerNetworkManager.Instance.RankingPosition);

        var filteredText = ChatManager.RemoveRichText(ChatManager.Instance.FilteredText(chatInput.text));
        var message = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "chat_message_format_2"), ranking, ServerNetworkManager.Instance.User.NickName, filteredText, rankColor);
        
        message = message.Replace("@", string.Empty);

        ChatManager.Instance.SendChatTextMessageToSub(message);
        chatInput.text = "";
	}

    public void OnJoinRaidButtonClicked()
    {
        var passedMilliSeconds = DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastRaidJoinClickTimeStampMilliseconds;

		if(passedMilliSeconds >= 500d)
		{
            lastRaidJoinClickTimeStampMilliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();

            var ticketNeeded = 50 * (ServerNetworkManager.Instance.Raid.DailyLimitLeft * -1 + 1);
            if(ServerNetworkManager.Instance.Raid.DailyLimitLeft <= 0 && ticketNeeded > ServerNetworkManager.Instance.Inventory.TicketRaid)
            {
                OutgameController.Instance.ShowRaidTicketPurchasePopup();
            }
            else
            {
                if(raidIdInput.text.Length == 4)
                    OutgameController.Instance.JoinRaid(raidIdInput.text);
                else
                    UIManager.Instance.ShowAlertLocalized("message_raid_not_open", null, null);
            }
        }
        
    }

    public void OnCreateRaidButtonClicked()
    {
       var passedMilliSeconds = DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastRaidCreatClickTimeStampMilliseconds;

		if(passedMilliSeconds >= 500d)
		{
            lastRaidCreatClickTimeStampMilliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();

            var ticketNeeded = 50 * (ServerNetworkManager.Instance.Raid.DailyLimitLeft * -1 + 1);
            if(ServerNetworkManager.Instance.Raid.DailyLimitLeft <= 0 && ticketNeeded > ServerNetworkManager.Instance.Inventory.TicketRaid)
            {
                OutgameController.Instance.ShowRaidTicketPurchasePopup();
            }
            else
            {
                int number;
                bool success = int.TryParse(floorInput.text, out number);
                if(success && 0 < number)
                {
                    if(number > 199)
                        number = 199;
                    floorInput.text = number.ToString();
                    OutgameController.Instance.CreateRaid(int.Parse(floorInput.text));
                }
            }
        }
    }

    public void AddTrophyToSlot(string id)
    {
        if(isSellMode)
            return;

        OutgameController.Instance.AddTrophyToSlot(id);
        RefreshTrophyNonDestuctive(false);
        isChanged = true;
    }

    public void RemoveTrophyFromSlot(string id)
    {
        if(isSellMode)
            return;
            
        OutgameController.Instance.RemoveTrophyFromSlot(id);
        RefreshTrophyNonDestuctive(false);
        isChanged = true;
    }

    public void HeartLevelUp(string id)
    {
        var levelUp = OutgameController.Instance.HeartLevelUpTrophy(id);
        RefreshTrophyNonDestuctive(levelUp);
        isChanged = true;
    }

    public void TrophyLevelUp(string id)
    {
        var levelUp = OutgameController.Instance.TrophyLevelUpTrophy(id);
        RefreshTrophyNonDestuctive(levelUp);
        isChanged = true;
    }

    public void TrophySell(string id)
    {
        OutgameController.Instance.SellTrophy(id);
        RefreshTrophyNonDestuctive(true);
        isChanged = true;
    }

    public void OnTrophySellButtonClicked()
    {
        isSellMode = !isSellMode;
        RefreshTrophyNonDestuctive(false);
    }

    public void OnInfoButtonClicked()
    {
        OutgameController.Instance.ShowGenericInfo(
            String.Format(
                LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_raid_info"),
                ServerNetworkManager.Instance.Setting.BigRoomSizeForRaid
            )
        );   
    }

#endregion
}

