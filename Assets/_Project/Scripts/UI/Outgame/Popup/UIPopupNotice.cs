﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;
using System.Collections;

public class UIPopupNotice : UIPopup
{
    [SerializeField]
	private RectTransform noticeListParentTransform;
	[SerializeField]
	private Transform listItemNoticePrefab;
    
    [SerializeField]
    private RectTransform contentParent;
    [SerializeField]
    private Text contentTitle;

    private string selectedId = "";

    public override void Open()
	{
		base.Open();

        // Set default selected notice
        if(ServerNetworkManager.Instance.NoticeList.Count > 0)
            selectedId = ServerNetworkManager.Instance.NoticeList[0].Id;

		Refresh();

        FocusItems();
	}

    public override void Close()
    {
        base.Close();

        if(GameObject.FindObjectOfType<UIViewMain>() != null)
            GameObject.FindObjectOfType<UIViewMain>().Refresh();
    }

    public override void Refresh()
    {
        foreach(var tmp in noticeListParentTransform.GetComponentsInChildren<ListItemNotice>())
        {
            Destroy(tmp.gameObject);
        }

        var lastReadNoticeTimeStamp = 0;
        
        if(PlayerPrefs.HasKey("last_read_notice_time_stamp"))
            lastReadNoticeTimeStamp = PlayerPrefs.GetInt("last_read_notice_time_stamp");

        foreach(var tmp in ServerNetworkManager.Instance.NoticeList)
		{
            var isNew = false;
            var isSelected = false;

            // If this notice's date is in 3 days then show new badge
            var currentTimeStamp = DateTimeOffset.Now.ToUnixTimeSeconds();

            if(Mathf.Abs(currentTimeStamp - tmp.TimeSeconds) <= 3600 * 24 * 3)
                isNew = true;

            if(tmp.TimeSeconds > lastReadNoticeTimeStamp)
            {
                PlayerPrefs.SetInt("last_read_notice_time_stamp", tmp.TimeSeconds);
                lastReadNoticeTimeStamp = tmp.TimeSeconds;
            }

            // Check selected notice
            if(selectedId == tmp.Id)
            {
                isSelected = true;
                contentTitle.text = tmp.ContentText;
            }

			var transformToUse = Instantiate(listItemNoticePrefab);
			transformToUse.GetComponent<ListItemNotice>().UpdateEntity(
				tmp.Id,
                tmp.TimeText,
                tmp.TitleText,
                isNew,
                isSelected
			);
			transformToUse.SetParent(noticeListParentTransform, false);
		}
    }

    public void SelectNotice(string id)
    {
        selectedId = id;

        var targetNotice = ServerNetworkManager.Instance.NoticeList.FirstOrDefault(n => n.Id == id);

        if(targetNotice != null)
        {
            PlayerPrefs.SetInt("last_read_notice_time_stamp", targetNotice.TimeSeconds);
            Refresh();
        }
    }

    private void FocusItems()
    {
        StartCoroutine(FocusItemsCoroutine());
    }

    private IEnumerator FocusItemsCoroutine()
    {
        contentParent.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Unrestricted;
        yield return new WaitForEndOfFrame();
        contentParent.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Elastic;

        Canvas.ForceUpdateCanvases();

		contentParent.anchoredPosition = new Vector2(contentParent.anchoredPosition.x, 0f);
    }
}