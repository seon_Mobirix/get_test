﻿using UnityEngine;
using UnityEngine.UI;

public class UIPopupRaidTicketPurchase : UIPopup
{
    [SerializeField]
	private Text ticketStartValueText;
	[SerializeField]
	private Text ticketMaxValueText;
    [SerializeField]
	private Text gemValueText;
    [SerializeField]
	private Slider ticketSilder;
    [SerializeField]
	private Button purchaseButton;

    public override void Open()
	{
		base.Open();
    
		ticketStartValueText.text = "100";
        ticketMaxValueText.text = "1,000";
        gemValueText.text = "100";
		
        ticketSilder.minValue = 1;
        ticketSilder.maxValue = 10;
		ticketSilder.value = 1;

        if(ServerNetworkManager.Instance.TotalGem < 100)
			purchaseButton.interactable = false;
		else
			purchaseButton.interactable = true;
	}

    public void OnRaidTicketSlider(float value)
	{
        int gemValue = (int)(value * 100);

		if(ServerNetworkManager.Instance.TotalGem < gemValue)
			purchaseButton.interactable = false;
		else
			purchaseButton.interactable = true;

		gemValueText.text = gemValue.ToString();
	}

	public void OnPurchaseButtonClicked()
	{
		OutgameController.Instance.PurchaseRaidTicket(int.Parse(gemValueText.text));
	}
    
}
