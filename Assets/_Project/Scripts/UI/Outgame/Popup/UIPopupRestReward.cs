﻿using System;
using System.Numerics;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupRestReward : UIPopup
{
    [SerializeField]
    private Text goldRewardInfoText;
    [SerializeField]
    private Text totalRestTimeInfoText;
    [SerializeField]
    private Text maxRestTimeInfoText;
    [SerializeField]
    private Slider rewardSlider;
    [SerializeField]
    private Transform longOkButton;

    private int restHours;
    private BigInteger rewadGold;
    private bool isOwnedBasicPackage;
    private Action onDoubleReward;

    public override void Open()
	{
		Debug.LogWarning("UIPopupRestReward shouldn't be open without parameters");
	}

	public void OpenWithValues(int restHours, BigInteger rewadGold, bool isOwnedBasicPackage, Action onDoubleReward)
	{
		base.Open();

        this.restHours = restHours;
        this.rewadGold = rewadGold;
        this.isOwnedBasicPackage = isOwnedBasicPackage;
        this.onDoubleReward = onDoubleReward;

        Refresh();
    }

    public override void Refresh()
    {
        string covertedGold = LanguageManager.Instance.NumberToString(rewadGold);
        string text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_supply_box"), covertedGold);

        if(isOwnedBasicPackage)
        {
            string bonusInfoText = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "bonus_applied"), 2);
            
            goldRewardInfoText.text = string.Format("{0}\n{1}", text, bonusInfoText);
            longOkButton.gameObject.SetActive(true);
        }
        else
        {
            goldRewardInfoText.text = text;
            longOkButton.gameObject.SetActive(false);
        }

        totalRestTimeInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "total_rest_time_info"), restHours);
        maxRestTimeInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "max_rest_time_info"), 1440);
        rewardSlider.value = Mathf.Clamp01((float)restHours / (float)1440);
    }

    public void OnOkButtonClicked()
    {
        base.Close();

        if(GameObject.FindObjectOfType<UIViewMain>() != null)
            GameObject.FindObjectOfType<UIViewMain>().RefreshTopbar();
    }

    public void OnDoubleRewardButtonClicked()
    {
        if(onDoubleReward != null)
            onDoubleReward.Invoke();
    }
}
