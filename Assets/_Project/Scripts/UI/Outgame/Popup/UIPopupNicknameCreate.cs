﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupNicknameCreate : UIPopup
{
    [SerializeField]
	private InputField textField;
	[SerializeField]
	private Text placeHolderText;
	[SerializeField]
	private Button createButton;

	private Action onSucess = null;
	
	public override void Open()
	{
		Debug.LogWarning("UIPopupNicknameCreate shouldn't be open without parameters");
	}

	public void OpenWithValues(Action onSucess)
	{
		base.Open();

		this.onSucess = onSucess;
		createButton.interactable = false;
		placeHolderText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "placehoder_nickname");
	}

	public void OnValueChange()
	{
		placeHolderText.gameObject.SetActive(false);
		
		bool isViolating = false;

		foreach(var tmp in textField.text)
		{
			if(!char.IsLetterOrDigit(tmp) && !char.IsWhiteSpace(tmp))
				isViolating = true;
		}

		if(textField.text.Length <= 2)
			isViolating = true;

		if(textField.text == "")
			isViolating = true;

		if(isViolating)
			createButton.interactable = false;
		else
			createButton.interactable = true;
	}

	public void OnEditFinish()
	{
		if(textField.text.Length == 0 && !placeHolderText.gameObject.activeSelf)
			placeHolderText.gameObject.SetActive(true);
	}

	public void OnCreateButton()
	{
		OutgameController.Instance.CreateNickName(textField.text, onSucess);
	}
}
