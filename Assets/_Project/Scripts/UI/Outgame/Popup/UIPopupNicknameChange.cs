﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupNicknameChange : UIPopup 
{
	[SerializeField]
	private InputField textField;
	[SerializeField]
	private Text changePriceText;
	[SerializeField]
	private Button changeButton;
	
	public override void Open()
	{
		base.Open();

		textField.text = ServerNetworkManager.Instance.User.NickName;
		changePriceText.text = "500";
	}

	public void OnValueChange()
	{		
		bool isViolating = false;

		foreach(var tmp in textField.text)
		{
			if(!char.IsLetterOrDigit(tmp) && !char.IsWhiteSpace(tmp))
				isViolating = true;
		}

		if(textField.text.Length <= 2)
			isViolating = true;

		if(textField.text == "")
			isViolating = true;

		if(isViolating)
			changeButton.interactable = false;
		else
			changeButton.interactable = true;
	}

	public void OnChangeButton()
	{
		if(textField.text == ServerNetworkManager.Instance.User.NickName)
			UIManager.Instance.ShowAlertLocalized("message_nickname_overlap", null, null);
		else
			OutgameController.Instance.ChangeNickName(textField.text);
	}
}