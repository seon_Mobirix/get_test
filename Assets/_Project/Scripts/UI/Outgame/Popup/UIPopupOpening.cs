﻿using UnityEngine;
using UnityEngine.UI;

public class UIPopupOpening : UIPopup {

	[SerializeField]
	private Text openingText;

    private int currentValue = 0;

    public override void Open()
	{
        base.Open();

		openingText.text = LanguageManager.Instance.GetTextData(LanguageDataType.NARRATIVE, "opening_01");
	}

	public void OnNextButtonClicked()
	{
		if(currentValue == 0)
        {
            openingText.text += "\n" + LanguageManager.Instance.GetTextData(LanguageDataType.NARRATIVE, "opening_02");
        }
        else if(currentValue == 1)
        {
            openingText.text += "\n" + LanguageManager.Instance.GetTextData(LanguageDataType.NARRATIVE, "opening_03");
        }
        else
        {
            OutgameController.Instance.SetOpeningSeen();
            base.Close();
        }

        currentValue++;
	}
}
