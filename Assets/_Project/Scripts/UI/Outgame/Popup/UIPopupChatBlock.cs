﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections;
using Sirenix.OdinInspector;

public class UIPopupChatBlock : UIPopup
{
	[SerializeField]
	private Transform listItemChatBlockPlayerPrefab;

	[SerializeField]
	private UITab uiTab;

    [Title("Chat User List")]
    [SerializeField]
	private RectTransform userListParentTransform;

    [Title("Block List")]
    [SerializeField]
	private RectTransform banListParentTransform;
    [SerializeField]
	private Text emptyChatBlockListText;

    private int currentTabId;

	public override void Open()
	{
		Debug.LogWarning("UIPopupChatBlock shouldn't be open without parameters");
	}

	public void OpenWithValues(int tabId)
	{
		base.Open();

		currentTabId = tabId;
		uiTab.OnTabButton(tabId);

		Refresh();

        FocusItems();
	}

    public void OnTabButtonClicked(int tabId)
	{
		currentTabId = tabId;
        uiTab.OnTabButton(tabId);

        Refresh();

        FocusItems();
	}

    public override void Refresh()
	{
        if(currentTabId == 0)
        {
            foreach(var tmp in userListParentTransform.GetComponentsInChildren<ListItemChatBlockPlayer>())
            {
                Destroy(tmp.gameObject);
            }
            
            foreach(var tmp in ChatManager.Instance.RecentPlayers)
            {
                if(tmp.Key != ServerNetworkManager.Instance.User.PlayFabId)
                {
                    var transformToUse = Instantiate(listItemChatBlockPlayerPrefab);
                    var isBlocked = false;

                    if(ServerNetworkManager.Instance.ChatBlockInfo.ChatBlockList.FirstOrDefault(n => n.Id == tmp.Key) != null)
                        isBlocked = true;
                    
                    transformToUse.GetComponent<ListItemChatBlockPlayer>().UpdateEntity(
                        tmp.Key,
                        tmp.Value,
                        isBlocked
                    );

                    transformToUse.SetParent(userListParentTransform, false);
                }
            }
        }
        else if(currentTabId == 1)
        {
            foreach(var tmp in banListParentTransform.GetComponentsInChildren<ListItemChatBlockPlayer>())
            {
                Destroy(tmp.gameObject);
            }

            if(ServerNetworkManager.Instance.ChatBlockInfo.ChatBlockList.Count > 0)
                emptyChatBlockListText.gameObject.SetActive(false);
            else
                emptyChatBlockListText.gameObject.SetActive(true);

            foreach(var tmp in ServerNetworkManager.Instance.ChatBlockInfo.ChatBlockList)
            {
                var transformToUse = Instantiate(listItemChatBlockPlayerPrefab);

                var convertedNickName = tmp.Nickname;
                var nicknameSeparatorList = tmp.Nickname.Split('>');

                if(nicknameSeparatorList.Length > 2)
                    convertedNickName = nicknameSeparatorList[2];

                transformToUse.GetComponent<ListItemChatBlockPlayer>().UpdateEntity(
					tmp.Id,
                    convertedNickName,
					true
                );

                transformToUse.SetParent(banListParentTransform, false);
            }   
        }
    }

	public void Block(string id, string nickname)
	{
		OutgameController.Instance.AddChatBlockList(id, nickname, Refresh);
	}

    public void UnBlock(string id)
	{
		OutgameController.Instance.RemoveChatBlockList(id, Refresh);
	}

    private void FocusItems()
    {
        StartCoroutine(FocusItemsCoroutine());
    }

    private IEnumerator FocusItemsCoroutine()
    {
        userListParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Unrestricted;
        yield return new WaitForEndOfFrame();
        userListParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Elastic;

        Canvas.ForceUpdateCanvases();

        userListParentTransform.anchoredPosition = new Vector2(userListParentTransform.anchoredPosition.x, 0f);
    }
}
