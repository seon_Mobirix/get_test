﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class UIPopupReceivedReward : UIPopup
{
    [SerializeField]
	private RectTransform listParentTransform;
	[SerializeField]
	private Transform listItemReceivedItemPrefab;

    // Real receive from server
    private RewardListModel receivedInfo = new RewardListModel();

    private Action onClose = null;

    public override void Open()
	{
		Debug.LogWarning("UIPopupReceivedPostReward shouldn't be open without parameters");
	}

    public override void Close()
    {
        base.Close();

        if(onClose != null)
            onClose.Invoke();
    }

    public void OpenWithValues(PostRewardListModel receivedPostInfo)
	{
        base.Open();
        this.onClose = null;
        
        receivedInfo.ItemList.Clear();

        foreach(var tmp in receivedPostInfo.ItemList)
        {
            var rewardInfo = new RewardModel();
            
            rewardInfo.ItemId = tmp.ItemId;
            rewardInfo.Count = tmp.Count;
            
            receivedInfo.ItemList.Add(rewardInfo);
        }

		RefreshWithRewardList();

        FocusItems();
    }

    public void OpenWithValues(RewardListModel receivedInfo)
	{
        if(receivedInfo.ItemList.Count == 0)
            return;

        base.Open();
        this.onClose = null;
        
        this.receivedInfo = receivedInfo;
		RefreshWithRewardList();

        FocusItems();
    }

    public void OpenWithValues(string itemId, string count)
    {
        base.Open();
        this.onClose = null;
        
		RefreshWithSingleItem(itemId, count);

        FocusItems();
    }

    public void OpenWithValues(string itemId, string count, Action onClose)
    {
        base.Open();
        
        this.onClose = onClose;
		RefreshWithSingleItem(itemId, count);
        
        FocusItems();
    }

    public void RefreshWithRewardList()
    {   
        foreach(var tmp in listParentTransform.GetComponentsInChildren<ListItemReceivedReward>())
		{
			Destroy(tmp.gameObject);
		}

        foreach(var tmp in receivedInfo.ItemList)
        {
            if(tmp.Count == 0)
                continue;

            var transformToUse = Instantiate(listItemReceivedItemPrefab);
            transformToUse.GetComponent<ListItemReceivedReward>().UpdateEntity(tmp.ItemId, LanguageManager.Instance.NumberToString(tmp.Count));
            transformToUse.SetParent(listParentTransform, false);
        }
    }

    public void RefreshWithSingleItem(string itemId, string count)
    {
        foreach(var tmp in listParentTransform.GetComponentsInChildren<ListItemReceivedReward>())
		{
			Destroy(tmp.gameObject);
		}

        var transformToUse = Instantiate(listItemReceivedItemPrefab);
        transformToUse.GetComponent<ListItemReceivedReward>().UpdateEntity(itemId, count);
        transformToUse.SetParent(listParentTransform, false);
    }

    private void FocusItems()
    {
        StartCoroutine(FocusItemsCoroutine());
    }

    private IEnumerator FocusItemsCoroutine()
    {
        listParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Unrestricted;
        yield return new WaitForEndOfFrame();
        listParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Elastic;

        Canvas.ForceUpdateCanvases();

		listParentTransform.anchoredPosition = new Vector2(listParentTransform.anchoredPosition.x, 0f);
    }
}