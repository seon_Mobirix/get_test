﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class UIPopupArena : UIPopup
{
    [SerializeField]
    private Text currentScoreAndPositionText = null;

    [SerializeField]
    private Text leftCountText = null;

    [SerializeField]
    private Text leftTimeInfoText = null;

    [SerializeField]
    private Text weeklyRewardTierText = null;

    [SerializeField]
    private Image weeklyRewardTierImage = null;

    [SerializeField]
    private Text currentStarText = null;

    [SerializeField]
    private Transform weeklyRewardInfo = null;

    [SerializeField]
    private Text leftStarText = null;

    [SerializeField]
    private Button battleButton = null;

    [SerializeField]
	private RectTransform rankListParentTransform;
	[SerializeField]
	private Transform listItemArenaRankPrefab;

    // Cool time for buttons
    private double lastArenaStartClickTimeStampMilliseconds;

    public override void Open()
	{
        base.Open();
        Refresh();
        FocusItems();        
    }

    
    public override void Close()
	{
		base.Close();

        if(GameObject.FindObjectOfType<UIViewMain>() != null)
        {
            GameObject.FindObjectOfType<UIViewMain>().Refresh();
            GameObject.FindObjectOfType<UIViewMain>().ShowRightTopContent();
        }
	}

    public void CloseWithOutShowRightTopContent()
    {
        base.Close();

        if(GameObject.FindObjectOfType<UIViewMain>() != null)
            GameObject.FindObjectOfType<UIViewMain>().Refresh();
    }

	public override void Refresh()
	{
        foreach(var tmp in rankListParentTransform.GetComponentsInChildren<ListItemArenaRank>())
        {
            Destroy(tmp.gameObject);
        }

        currentScoreAndPositionText.text = string.Format(
            LanguageManager.Instance.GetTextData(LanguageDataType.UI, "arena_my_position_score"),
            ServerNetworkManager.Instance.Arena.Position, 
            ServerNetworkManager.Instance.Arena.Score
        );
        leftCountText.text = string.Format(
            LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_arena_start"),
            ServerNetworkManager.Instance.Arena.DailyLimitLeft
        );

        var currentWeeklyRewardTier = 7;
        var requiredPromotionStar = ServerNetworkManager.Instance.Setting.ArenaWeeklyRequiredStarsList[0];
        foreach(var tmp in ServerNetworkManager.Instance.Setting.ArenaWeeklyRequiredStarsList)
        {
            if(tmp > ServerNetworkManager.Instance.Arena.Star)
            {
                currentWeeklyRewardTier --;
                requiredPromotionStar = tmp;
            }
        }

        weeklyRewardTierText.text = string.Format(
            LanguageManager.Instance.GetTextData(LanguageDataType.UI, "arena_league_tier"),
            currentWeeklyRewardTier
        );

        currentStarText.text = "★ " + Mathf.Max(ServerNetworkManager.Instance.Arena.Star, 0) + "/" + requiredPromotionStar;

        if(currentWeeklyRewardTier == 7)
        {
            leftStarText.text = "";
        }
        else
        {
            leftStarText.text = string.Format(
                LanguageManager.Instance.GetTextData(LanguageDataType.UI, "arena_league_promotion"),
                requiredPromotionStar - ServerNetworkManager.Instance.Arena.Star
            );
        }

        leftTimeInfoText.text = GetPlayLimitText();

        if(ServerNetworkManager.Instance.Arena.DailyLimitLeft > 0)
            battleButton.interactable = true;
        else
            battleButton.interactable = false;

        foreach(var tmp in ServerNetworkManager.Instance.Arena.rankList)
        {
            var transformToUse = Instantiate(listItemArenaRankPrefab);
            transformToUse.GetComponent<ListItemArenaRank>().UpdateEntity(
                tmp.name,
                tmp.position + 1,
                tmp.score
            );
            transformToUse.SetParent(rankListParentTransform, false);
        }

        rankListParentTransform.parent.GetComponent<ScrollRect>().verticalNormalizedPosition = 0f;
    }

    private void FocusItems()
    {
        StartCoroutine(FocusItemsCoroutine());
    }

    private IEnumerator FocusItemsCoroutine()
    {
        rankListParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Unrestricted;
        yield return new WaitForEndOfFrame();
        rankListParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Elastic;

        Canvas.ForceUpdateCanvases();

		rankListParentTransform.anchoredPosition = new UnityEngine.Vector2(rankListParentTransform.anchoredPosition.x, 0f);
    }

    public void OnBattleButtleClicked()
    {
        var passedMilliSeconds = DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastArenaStartClickTimeStampMilliseconds;

        if (passedMilliSeconds >= 500d)
        {
            lastArenaStartClickTimeStampMilliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();

            var minuteOfDay = DateTime.UtcNow.Hour * 60 + DateTime.UtcNow.Minute;
            if (minuteOfDay < 10 || minuteOfDay >= 1435)
            {
                IngameArenaController.ArenaConstantBattleData.IsConstantBattle = false;
                UIManager.Instance.ShowAlert(GetPlayLimitText(), null, null);
            }
            else
            {
                IngameArenaController.ArenaConstantBattleData.CurrentCount++;
                OutgameController.Instance.ArenaStart();
            }
        }
        else
            IngameArenaController.ArenaConstantBattleData.IsConstantBattle = false;
    }

    public void OnContinueBattleButton()
    {
        IngameArenaController.ArenaConstantBattleData.IsConstantBattle = true;
        IngameArenaController.ArenaConstantBattleData.RemindCount = ServerNetworkManager.Instance.Arena.DailyLimitLeft;
        IngameArenaController.ArenaConstantBattleData.CurrentCount = 0;
        OnBattleButtleClicked();
    }

    public void OnInfoButtonClicked()
    {
        // Prevent error
        if(ServerNetworkManager.Instance.Setting.ArenaRewardList.Count == 7 && ServerNetworkManager.Instance.Setting.ArenaWeeklyRewardList.Count == 7)
        {
            var arenaRewardList = ServerNetworkManager.Instance.Setting.ArenaRewardList;
            var arenaWeeklyRewardList = ServerNetworkManager.Instance.Setting.ArenaWeeklyRewardList;
            var arenaWeeklyRequiredList = ServerNetworkManager.Instance.Setting.ArenaWeeklyRequiredStarsList;

            bool useSunday = false;
            string timeToShow = DateTimeOffset.Now.Offset.ToString();
            if(DateTimeOffset.Now.Offset.Hours < 0)
            {
                useSunday = true;
                timeToShow = (new TimeSpan(24, 0, 0) + DateTimeOffset.Now.Offset).ToString();
            }
            
            var text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_arena_info");
            var text2 = (useSunday)? LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_arena_3_info") : LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_arena_2_info");

            OutgameController.Instance.ShowGenericInfo(
                String.Format(
                    text, 
                    arenaRewardList[0],
                    arenaRewardList[1],
                    arenaRewardList[2],
                    arenaRewardList[3],
                    arenaRewardList[4],
                    arenaRewardList[5],
                    arenaRewardList[6]
                ) 
                + "\n\n"
                + String.Format(
                    text2,
                    timeToShow,
                    arenaWeeklyRewardList[0],
                    arenaWeeklyRewardList[1],
                    arenaWeeklyRewardList[2],
                    arenaWeeklyRewardList[3],
                    arenaWeeklyRewardList[4],
                    arenaWeeklyRewardList[5],
                    arenaWeeklyRewardList[6],
                    arenaWeeklyRequiredList[0],
                    arenaWeeklyRequiredList[1],
                    arenaWeeklyRequiredList[2],
                    arenaWeeklyRequiredList[3],
                    arenaWeeklyRequiredList[4],
                    arenaWeeklyRequiredList[5],
                    arenaWeeklyRequiredList[6]
                ) 
            );
        }
    }

    private string GetPlayLimitText()
    {
        var offset = DateTimeOffset.Now.Offset;
		
		var startTime = offset + TimeSpan.FromMinutes(10);
        if(startTime.TotalSeconds < 0)
            startTime += new TimeSpan(24, 0, 0);
        if(startTime.Hours < 0)
            startTime = new TimeSpan(24, 0, 0) - startTime;

        var endTime = offset - TimeSpan.FromMinutes(5);
        if(endTime.TotalSeconds < 0)
            endTime += new TimeSpan(24, 0, 0);
        if(endTime.Hours < 0)
            endTime = new TimeSpan(24, 0, 0) - endTime;

        if(offset < endTime)
        {
            return string.Format(
                LanguageManager.Instance.GetTextData(LanguageDataType.UI, "desc_arena_avail_time_same"),
                startTime,
                endTime,
				offset
            );
        }
        else
        {
            return string.Format(
                LanguageManager.Instance.GetTextData(LanguageDataType.UI, "desc_arena_avail_time_different"),
                startTime,
                endTime,
				offset
            );
        }
    }
}

