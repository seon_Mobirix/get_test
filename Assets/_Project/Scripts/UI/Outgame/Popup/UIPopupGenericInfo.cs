﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIPopupGenericInfo : UIPopup {

    [SerializeField]
	private Text titleText;
	[SerializeField]
	private Text contentText;

	[SerializeField]
	private RectTransform listParent = null;

	public override void Open()
	{
		Debug.LogWarning("UIPopupGenericInfo shouldn't be open without parameters");
	}

	public void OpenWithValues(string textToUse)
	{
		base.Open();
		titleText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "title_alert");
		contentText.text = textToUse;

		FocusItems();
	}

	private void FocusItems()
    {
        StartCoroutine(FocusItemsCoroutine());
    }

    private IEnumerator FocusItemsCoroutine()
    {
        listParent.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Unrestricted;
        yield return new WaitForEndOfFrame();
        listParent.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Elastic;

        Canvas.ForceUpdateCanvases();

		listParent.anchoredPosition = new Vector2(listParent.anchoredPosition.x, 0f);
    }
}
