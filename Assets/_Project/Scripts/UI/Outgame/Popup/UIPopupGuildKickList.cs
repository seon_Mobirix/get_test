﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIPopupGuildKickList : UIPopup
{
    [SerializeField]
    private Transform emptyInfoText;

    [SerializeField]
	private Transform listItemKickedMemberPrefab;

    [SerializeField]
	private RectTransform kickListParentTransform;

    public override void Open()
	{
		base.Open();

		Refresh();

        FocusItems();
	}

    public override void Refresh()
	{
        foreach(var tmp in kickListParentTransform.GetComponentsInChildren<ListItemKickMember>())
        {
            Destroy(tmp.gameObject);
        }

        int kickMemberCount = 0;

        foreach(var tmp in ServerNetworkManager.Instance.GuildData.GuildManagementData.GuildKickMemberData.KickMemberList)
        {
            kickMemberCount ++;
            
            var transformToUse = Instantiate(listItemKickedMemberPrefab);
            transformToUse.GetComponent<ListItemKickMember>().UpdateEntity(tmp.Id, tmp.Name);

            transformToUse.SetParent(kickListParentTransform, false);
        }

        if(kickMemberCount != 0)
            emptyInfoText.gameObject.SetActive(false);
        else
            emptyInfoText.gameObject.SetActive(true);
    }

    private void FocusItems()
    {
        StartCoroutine(FocusItemsCoroutine());
    }

    private IEnumerator FocusItemsCoroutine()
    {
        kickListParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Unrestricted;
        yield return new WaitForEndOfFrame();
        kickListParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Elastic;

        Canvas.ForceUpdateCanvases();

        kickListParentTransform.anchoredPosition = new Vector2(kickListParentTransform.anchoredPosition.x, 0f);
    }
}
