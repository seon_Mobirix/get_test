﻿using UnityEngine;
using UnityEngine.UI;

public class UIPopupSummonLevelInfo : UIPopup
{
    [SerializeField]
    private Text weaponSummonLevelInfoText;
    [SerializeField]
    private Text weaponSummonLevelProgressInfoText;
    [SerializeField]
    private Text classSummonLevelInfoText;
    [SerializeField]
    private Text classSummonLevelProgressInfoText;
    [SerializeField]
    private Text mercenarySummonLevelInfoText;
    [SerializeField]
    private Text mercenarySummonLevelProgressInfoText;

    public override void Open()
	{
        base.Open();
        Refresh();
    }

	public override void Refresh()
	{
        int weaponSummonLevel = ServerNetworkManager.Instance.User.WeaponGachaLevel;
        int weaponSummonExp = ServerNetworkManager.Instance.User.WeaponGachaExp;
        int weaponSummonRequireExp = GetRequireExp(weaponSummonLevel);

        weaponSummonLevelInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "summon_level_weapon"), weaponSummonLevel);
        
        if(weaponSummonRequireExp != -1)
        {
            weaponSummonLevelProgressInfoText.text = string.Format(
                LanguageManager.Instance.GetTextData(LanguageDataType.UI, "summon_level_progress_info"), 
                weaponSummonExp, 
                weaponSummonRequireExp,
                Mathf.Max(weaponSummonRequireExp - weaponSummonExp, 0)
            );
        }
        else
        {
            weaponSummonLevelProgressInfoText.text = string.Format(
                LanguageManager.Instance.GetTextData(LanguageDataType.UI, "summon_level_progress_info"), 
                weaponSummonExp, 
                "∞",
                "∞"
            );
        }

        int classSummonLevel = ServerNetworkManager.Instance.User.ClassGachaLevel;
        int classSummonExp = ServerNetworkManager.Instance.User.ClassGachaExp;
        int classSummonRequireExp = GetRequireExp(classSummonLevel);

        classSummonLevelInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "summon_level_class"), classSummonLevel);
        
        if(classSummonRequireExp != -1)
        {
            classSummonLevelProgressInfoText.text = string.Format(
                LanguageManager.Instance.GetTextData(LanguageDataType.UI, "summon_level_progress_info"), 
                classSummonExp, 
                classSummonRequireExp,
                Mathf.Max(classSummonRequireExp - classSummonExp, 0)
            );
        }
        else
        {
            classSummonLevelProgressInfoText.text = string.Format(
                LanguageManager.Instance.GetTextData(LanguageDataType.UI, "summon_level_progress_info"), 
                classSummonExp, 
                "∞",
                "∞"
            );
        }

        int mercenarySummonLevel = ServerNetworkManager.Instance.User.MercenaryGachaLevel;
        int mercenarySummonExp = ServerNetworkManager.Instance.User.MercenaryGachaExp;
        int mercenarySummonRequireExp = GetRequireExp(mercenarySummonLevel);

        mercenarySummonLevelInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "summon_level_mercenary"), mercenarySummonLevel);
        
        if(mercenarySummonRequireExp != -1)
        {
            mercenarySummonLevelProgressInfoText.text = string.Format(
                LanguageManager.Instance.GetTextData(LanguageDataType.UI, "summon_level_progress_info"), 
                mercenarySummonExp, 
                mercenarySummonRequireExp,
                Mathf.Max(mercenarySummonRequireExp - mercenarySummonExp, 0)
            );
        }
        else
        {
            mercenarySummonLevelProgressInfoText.text = string.Format(
                LanguageManager.Instance.GetTextData(LanguageDataType.UI, "summon_level_progress_info"), 
                mercenarySummonExp, 
                "∞",
                "∞"
            );
        }
    }

    private int GetRequireExp(int level)
    {
        int requireExp = 0;

        if(level < BalanceInfoManager.Instance.SummonInfoList.Count)
            requireExp = int.Parse(BalanceInfoManager.Instance.SummonInfoList[level].SummonExp);
        else
            requireExp = -1;

        return requireExp;
    }
}
