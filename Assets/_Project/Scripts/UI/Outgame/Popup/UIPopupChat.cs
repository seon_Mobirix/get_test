﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using Sirenix.OdinInspector;

public class UIPopupChat : UIPopup
{

#region First tab
    
    [Title("For First Tab")]

    private string previousChatLog = "";

    [SerializeField]
    private Transform tab = null;

    [SerializeField]
    private List<Image> tabButtonImageList = new List<Image>();

    [SerializeField]
    private Text textArea = null;

    [SerializeField]
    private InputField inputField = null;

    [SerializeField]
	private ScrollRect scrollRect;

    [SerializeField]
	private Transform reportButton;

    [SerializeField]
	private Transform chatBlockButton;

#endregion

#region Second tab

    [Title("For Second Tab")]

    [SerializeField]
    private bool isForGuild = false;

    private string previousChatLog2 = "";

    [SerializeField]
    private Transform tab2 = null;

    [SerializeField]
    private List<Image> tab2ButtonImageList = new List<Image>();

    [SerializeField]
    private Text textArea2 = null;

    [SerializeField]
    private InputField inputField2 = null;

    [SerializeField]
	private ScrollRect scrollRect2;

    [SerializeField]
	private Transform reportButton2;

    [SerializeField]
	private Transform chatBlockButton2;

#endregion

#region Third tab

    [SerializeField]
    private bool isThirdTabUsed = false;

    private string previousChatLog3 = "";

    [Title("For Third Tab")]

    [SerializeField]
    private Transform tab3 = null;

    [SerializeField]
    private List<Button> tab3ButtonList = new List<Button>();

    [SerializeField]
    private List<Image> tab3ButtonImageList = new List<Image>();

    [SerializeField]
    private Text textArea3 = null;

    [SerializeField]
    private InputField inputField3 = null;

    [SerializeField]
	private ScrollRect scrollRect3;

#endregion

    [Title("For Common Things")]

    [SerializeField]
    private List<Sprite> tabSpriteList = new List<Sprite>();

    [SerializeField]
    private bool isForIngame = false;

    [SerializeField]
    private Transform outgameUI = null;

    [SerializeField]
    private Transform ingameUI = null;

    [SerializeField]
    private Image sizeIcon = null;

    [SerializeField]
    private List<Sprite> sizeSpriteList = new List<Sprite>();

	protected override void OnOpen()
    {
        reportButton2.gameObject.SetActive(false);
        chatBlockButton2.gameObject.SetActive(false);
        
        if(!isForIngame)
        {
            outgameUI.gameObject.SetActive(true);
            ingameUI.gameObject.SetActive(false);

            reportButton.gameObject.SetActive(ServerNetworkManager.Instance.IsModerator);
            chatBlockButton.gameObject.SetActive(!ServerNetworkManager.Instance.IsModerator);
        }
        else
        {
            outgameUI.gameObject.SetActive(false);
            ingameUI.gameObject.SetActive(true);

            reportButton.gameObject.SetActive(false);
            chatBlockButton.gameObject.SetActive(false);
        }

        OnChangeTabToMainButtonClicked();

        previousChatLog = "";
        previousChatLog2 = "";
        if(isThirdTabUsed)
            previousChatLog3 = "";
        textArea.text = "";
        textArea2.text = "";
        if(isThirdTabUsed)
            textArea3.text = "";

        scrollRect.verticalNormalizedPosition = 1f;
        scrollRect2.verticalNormalizedPosition = 1f;
        if(isThirdTabUsed)
            scrollRect3.verticalNormalizedPosition = 1f;

        inputField.gameObject.SetActive(false);
        inputField2.gameObject.SetActive(false);
        if(isThirdTabUsed)
            inputField3.gameObject.SetActive(false);
        this.gameObject.transform.localScale = new Vector3(0.5f, 0.5f, 1f);
        inputField.gameObject.SetActive(true);
        inputField2.gameObject.SetActive(true);
        if(isThirdTabUsed)
            inputField3.gameObject.SetActive(true);
    }

	public override void Close()
	{
		base.Close();
		scrollRect.verticalNormalizedPosition = 0f;
        scrollRect2.verticalNormalizedPosition = 0f;
        if(isThirdTabUsed)
            scrollRect3.verticalNormalizedPosition = 0f;
        
        if(GameObject.FindObjectOfType<UIViewMain>() != null)
            GameObject.FindObjectOfType<UIViewMain>().SetChatButtonActive(true);
	}

    float passed = 0f;
    private void Update()
    {
        passed += Time.deltaTime;
        if(passed >= 0.5f && this.gameObject.activeSelf)
        {
            // Auto close when connection is gone
            if(!ChatManager.Instance.IsConnected)
                Close();

            if(!isForIngame)
            {
                if(!previousChatLog.Equals(ChatManager.Instance.ChatLogMain))
                {
                    Refresh();
                    previousChatLog = ChatManager.Instance.ChatLogMain;
                }
            }
            else
            {
                if(!previousChatLog.Equals(ChatManager.Instance.ChatLogIngame))
                {
                    Refresh();
                    previousChatLog = ChatManager.Instance.ChatLogIngame;
                }
            }

            if(isForGuild)
            {
                if(!previousChatLog2.Equals(ChatManager.Instance.ChatLogGuild))
                {
                    Refresh();
                    previousChatLog2 = ChatManager.Instance.ChatLogGuild;
                }
            }
            else
            {
                if(!previousChatLog2.Equals(ChatManager.Instance.ChatLogSub))
                {
                    Refresh();
                    previousChatLog2 = ChatManager.Instance.ChatLogSub;
                }
            }

            if(isThirdTabUsed)
            {
                if(!previousChatLog3.Equals(ChatManager.Instance.ChatLogGuild))
                {
                    Refresh();
                    previousChatLog3 = ChatManager.Instance.ChatLogGuild;
                }
            }

            passed = 0f;
        }
    }

	public override void Refresh()
	{
        if(!isForIngame)
        {
            textArea.text = ChatManager.Instance.ChatLogMain;
        }
        else
        {
            textArea.text = ChatManager.Instance.ChatLogIngame;
        }

        if(isForGuild)
        {
            textArea2.text = ChatManager.Instance.ChatLogGuild;
        }
        else
        {
            textArea2.text = ChatManager.Instance.ChatLogSub;
        }

        if(isThirdTabUsed)
        {
            textArea3.text = ChatManager.Instance.ChatLogGuild;
        }
	}

	public void OnSendButtonClicked()
	{
        if(inputField.text == "")
            return;

        if(ServerNetworkManager.Instance.IsChatBanned)
        {
            UIManager.Instance.ShowAlertLocalized("message_chat_banned", null, null);
            return;
        }
        
        var rankColor = InterSceneManager.Instance.GetColorForRank(ServerNetworkManager.Instance.RankingPosition);

        var ranking = "?";
        if(ServerNetworkManager.Instance.RankingPosition != 0)
        {
            ranking = ChatManager.AddOrdinal(ServerNetworkManager.Instance.RankingPosition);
        }

        var filteredText = ChatManager.RemoveRichText(ChatManager.Instance.FilteredText(inputField.text));
        var message = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "chat_message_format_2"), ranking, ServerNetworkManager.Instance.User.NickName, filteredText, rankColor);
        message = message.Replace("@", string.Empty);

        if(!isForIngame)
            ChatManager.Instance.SendChatTextMessageToMain(message);
        else
            ChatManager.Instance.SendChatTextMessageToIngame(message);

        inputField.text = "";
	}

    public void OnSendButton2Clicked()
	{
        if(inputField2.text == "")
            return;

        if(ServerNetworkManager.Instance.IsChatBanned)
        {
            UIManager.Instance.ShowAlertLocalized("message_chat_banned", null, null);
            return;
        }
        
        var rankColor = InterSceneManager.Instance.GetColorForRank(ServerNetworkManager.Instance.RankingPosition);

        var ranking = "?";
        if(ServerNetworkManager.Instance.RankingPosition != 0)
        {
            ranking = ChatManager.AddOrdinal(ServerNetworkManager.Instance.RankingPosition);
        }
        
        var filteredText = ChatManager.RemoveRichText(ChatManager.Instance.FilteredText(inputField2.text));
        var message = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "chat_message_format_2"), ranking, ServerNetworkManager.Instance.User.NickName, filteredText, rankColor);
        message = message.Replace("@", string.Empty);

        if(isForGuild)
            ChatManager.Instance.SendChatTextMessageToGuild(message);
        else
            ChatManager.Instance.SendChatTextMessageToSub(message);

        inputField2.text = "";
	}

    public void OnSendButton3Clicked()
	{
        if(inputField3.text == "")
            return;

        if(ServerNetworkManager.Instance.IsChatBanned)
        {
            UIManager.Instance.ShowAlertLocalized("message_chat_banned", null, null);
            return;
        }
        
        var rankColor = InterSceneManager.Instance.GetColorForRank(ServerNetworkManager.Instance.RankingPosition);

        var ranking = "?";
        if(ServerNetworkManager.Instance.RankingPosition != 0)
        {
            ranking = ChatManager.AddOrdinal(ServerNetworkManager.Instance.RankingPosition);
        }
        
        var filteredText = ChatManager.RemoveRichText(ChatManager.Instance.FilteredText(inputField3.text));
        var message = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "chat_message_format_2"), ranking, ServerNetworkManager.Instance.User.NickName, filteredText, rankColor);
        message = message.Replace("@", string.Empty);

        ChatManager.Instance.SendChatTextMessageToGuild(message);

        inputField3.text = "";
	}

    public void OnScaleButtonClicked()
    {
        if(this.gameObject.transform.localScale.x == 1f)
        {
            this.gameObject.transform.localScale = new Vector3(0.5f, 0.5f, 1f);
            sizeIcon.sprite = sizeSpriteList[1];
        }
        else
        {
            this.gameObject.transform.localScale = new Vector3(1f, 1f, 1f);
            sizeIcon.sprite = sizeSpriteList[0];
        }
    }

    public void OnChangeTabToMainButtonClicked()
    {
        tab.gameObject.SetActive(true);
        tab2.gameObject.SetActive(false);
        if(isThirdTabUsed)
            tab3.gameObject.SetActive(false);

        foreach(var tmp in tabButtonImageList)
            tmp.sprite = tabSpriteList[1];
        foreach(var tmp in tab2ButtonImageList)
            tmp.sprite = tabSpriteList[0];
        if(isThirdTabUsed)
        {
            foreach(var tmp in tab3ButtonImageList)
                tmp.sprite = tabSpriteList[0];
            if(ServerNetworkManager.Instance.MyGuildId != null && ServerNetworkManager.Instance.MyGuildId != "")
            {
                foreach(var tmp in tab3ButtonList)
                    tmp.gameObject.SetActive(true);
            }
            else
            {
                foreach(var tmp in tab3ButtonList)
                    tmp.gameObject.SetActive(false);
            }
        }    
    }

    public void OnChangeTabToSubButtonClicked()
    {
        tab.gameObject.SetActive(false);
        tab2.gameObject.SetActive(true);
        if(isThirdTabUsed)
            tab3.gameObject.SetActive(false);

        foreach(var tmp in tabButtonImageList)
            tmp.sprite = tabSpriteList[0];
        foreach(var tmp in tab2ButtonImageList)
            tmp.sprite = tabSpriteList[1];
        if(isThirdTabUsed)
        {
            foreach(var tmp in tab3ButtonImageList)
                tmp.sprite = tabSpriteList[0];
            if(ServerNetworkManager.Instance.MyGuildId != null && ServerNetworkManager.Instance.MyGuildId != "")
            {
                foreach(var tmp in tab3ButtonList)
                    tmp.gameObject.SetActive(true);
            }
            else
            {
                foreach(var tmp in tab3ButtonList)
                    tmp.gameObject.SetActive(false);
            }
        } 
    }

    public void OnChangeTabToThirdButtonClicked()
    {
        OutgameController.Instance.GetGuildData(
            true,
            () => {

                if(ServerNetworkManager.Instance.MyGuildId != null && ServerNetworkManager.Instance.MyGuildId != "")
                {
                    ChatManager.Instance.Subscribe("guild_" + ServerNetworkManager.Instance.GuildData.GuildManagementData.GroupId);

                    tab.gameObject.SetActive(false);
                    tab2.gameObject.SetActive(false);
                    tab3.gameObject.SetActive(true);

                    foreach(var tmp in tabButtonImageList)
                        tmp.sprite = tabSpriteList[0];
                    foreach(var tmp in tab2ButtonImageList)
                        tmp.sprite = tabSpriteList[0];
                    foreach(var tmp in tab3ButtonImageList)
                        tmp.sprite = tabSpriteList[1];
                }
            },
            () => {
                OnChangeTabToMainButtonClicked();
            }
        );
    }

    public void OnReportButtonClicked()
	{
        // Check moderator
        if(!ServerNetworkManager.Instance.IsModerator)
            return;

        OutgameController.Instance.ShowReportPopup();
	}

    public void OnChatBlockButtonClicked()
	{
        // Check player
        if(ServerNetworkManager.Instance.IsModerator)
            return;

        OutgameController.Instance.ShowChatBlockPopup();
	}
}