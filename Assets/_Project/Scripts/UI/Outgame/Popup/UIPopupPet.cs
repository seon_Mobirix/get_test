﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class UIPopupPet : UIPopup
{   
    private bool isSellMode = false;
    private bool isChanged = false;

    [SerializeField]
	private RectTransform petParentTransform;

    [SerializeField]
    private Transform listItemPetPrefab;

    [SerializeField]
    private List<ListItemPetSlot> petSlotList = new List<ListItemPetSlot>();

    [SerializeField]
    private Text petText = null;

    [SerializeField]
    private Text sellText = null;

    public override void Open()
	{
		base.Open();
        Refresh();
	}
    
    public override void Close()
    {
        base.Close();

        // Need to sync data so far
        if(isChanged)
            OutgameController.Instance.SyncPlayerData(false, "UIPopupPet");

        isChanged = false;

        if(GameObject.FindObjectOfType<UIViewMain>() != null)
        {
            GameObject.FindObjectOfType<UIViewMain>().SetHeartAmountInfoActive(false);
            GameObject.FindObjectOfType<UIViewMain>().RefreshMenuGroup();
            GameObject.FindObjectOfType<UIViewMain>().ShowRightTopContent();
        }
    }

    public void CloseWithOutShowRightTopContent()
    {
        base.Close();

        // Need to sync data so far
        if(isChanged)
            OutgameController.Instance.SyncPlayerData(false, "UIPopupPet");

        isChanged = false;
    }

    public override void Refresh()
    { 
        foreach(var tmp in petParentTransform.GetComponentsInChildren<ListItemPet>())
        {
            Destroy(tmp.gameObject);
        }

        for(int i = 0; i < ServerNetworkManager.Instance.Setting.TotalPetCount; i++)
        {	
            var tmp = BalanceInfoManager.Instance.PetInfoList[i];
            
            bool isMain = false;			
            var tmpPetModel = ServerNetworkManager.Instance.Inventory.PetList.FirstOrDefault(n => n.Id == tmp.Id);
            
            // Check user owned this item
            if(tmpPetModel != null)
            {
                if(ServerNetworkManager.Instance.Inventory.MainPet != null && tmpPetModel.Id == ServerNetworkManager.Instance.Inventory.MainPet.Id)
                    isMain = true;

                var transformToUse = Instantiate(listItemPetPrefab);
                transformToUse.GetComponent<ListItemPet>().UpdateEntity(
                    isSellMode,
                    tmp.Id, 
                    tmpPetModel.Level,
                    tmpPetModel.Count,
                    tmpPetModel.PetSellReward,
                    true,
                    ServerNetworkManager.Instance.Inventory.IsEquipPet(tmpPetModel.Id),
                    isMain,
                    false
                );
                transformToUse.SetParent(petParentTransform, false);
            }
            else
            {
                var transformToUse = Instantiate(listItemPetPrefab);
                transformToUse.GetComponent<ListItemPet>().UpdateEntity(
                    isSellMode,
                    tmp.Id, 
                    1,
                    0,
                    0,
                    false,
                    false,
                    isMain,
                    false
                );
                transformToUse.SetParent(petParentTransform, false);
            }
        }

        if(isSellMode)
            sellText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_pet_manage");
        else
            sellText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_pet_sell");

        RefreshPetSlot();

        RefreshRedDot();
    }

    private void RefreshRedDot()
    {
		// Check pet
        foreach(var tmp in ServerNetworkManager.Instance.Inventory.PetList)
        {
			tmp.IsNew = false;
        }
    }

    private void RefreshNonDestuctive(bool isUpgradeSuccess)
    {
        foreach(var tmp in petParentTransform.GetComponentsInChildren<ListItemPet>())
        {
            bool isMain = false;
            var tmpPetModel = ServerNetworkManager.Instance.Inventory.PetList.FirstOrDefault(n => n.Id == tmp.PetId);

            // Check user owned this item
            if(tmpPetModel != null)
            {
                if(ServerNetworkManager.Instance.Inventory.MainPet != null && tmpPetModel.Id == ServerNetworkManager.Instance.Inventory.MainPet.Id)
                    isMain = true;
                    
                tmp.UpdateEntity(
                    isSellMode,
                    tmp.PetId, 
                    tmpPetModel.Level,
                    tmpPetModel.Count,
                    tmpPetModel.PetSellReward,
                    true,
                    ServerNetworkManager.Instance.Inventory.IsEquipPet(tmpPetModel.Id),
                    isMain,
                    isUpgradeSuccess
                );
            }
            else
            {
                tmp.UpdateEntity(
                    isSellMode,
                    tmp.PetId, 
                    1,
                    0,
                    0,
                    false,
                    false,
                    isMain,
                    false
                );
            }
        }

        if(isSellMode)
            sellText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_pet_manage");
        else
            sellText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_pet_sell");

        RefreshPetSlot();
    }

    private void RefreshPetSlot()
    {
        int i = 0;
        foreach(var tmp in ServerNetworkManager.Instance.Inventory.SelectedPetList)
        {
            petSlotList[i].UpdateEntity(false, "", true, tmp.Id);
            i++;
            if(i == 8)
                break;
        }

        for(; i < 8; i++)
        {
            if(i == 7)
            {
                if(int.Parse(ServerNetworkManager.Instance.Inventory.BestClass.Id.Substring(6)) >= 25)
                    petSlotList[i].UpdateEntity(false, "", false, "");
                else
                    petSlotList[i].UpdateEntity(true, "Y", false, "");
            }
            else if(i == 6)
            {
                if(int.Parse(ServerNetworkManager.Instance.Inventory.BestClass.Id.Substring(6)) >= 19)
                    petSlotList[i].UpdateEntity(false, "", false, "");
                else
                    petSlotList[i].UpdateEntity(true, "U", false, "");
            }
            else if(i == 5)
            {
                if(int.Parse(ServerNetworkManager.Instance.Inventory.BestClass.Id.Substring(6)) >= 16)
                    petSlotList[i].UpdateEntity(false, "", false, "");
                else
                    petSlotList[i].UpdateEntity(true, "R", false, "");
            }
            else if(i == 4)
            {
                if(int.Parse(ServerNetworkManager.Instance.Inventory.BestClass.Id.Substring(6)) >= 13)
                    petSlotList[i].UpdateEntity(false, "", false, "");
                else
                    petSlotList[i].UpdateEntity(true, "S", false, "");
            }
            else if(i == 3)
            {
                if(int.Parse(ServerNetworkManager.Instance.Inventory.BestClass.Id.Substring(6)) >= 10)
                    petSlotList[i].UpdateEntity(false, "", false, "");
                else
                    petSlotList[i].UpdateEntity(true, "A", false, "");
            }
            else
            {
                petSlotList[i].UpdateEntity(false, "", false, "");
            }
        }
        
        var totalEffect = 0f;
        foreach(var tmp in ServerNetworkManager.Instance.Inventory.SelectedPetList)
        {
            totalEffect += tmp.EffectPower;
        }
        petText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "pet_collected_power"), totalEffect.ToString("F2"));
    }

    public void SetMainPet(string petId)
    {
        if(isSellMode)
            return;

        OutgameController.Instance.SetMainPet(petId);
        RefreshNonDestuctive(false);
        isChanged = true;
    }

    public void AddPetToSlot(string petId)
    {
        if(isSellMode)
            return;

        OutgameController.Instance.AddPetToSlot(petId);
        RefreshNonDestuctive(false);
        isChanged = true;
    }

    public void RemovePetFromSlot(string petId)
    {
        if(isSellMode)
            return;
        
        OutgameController.Instance.RemovePetFromSlot(petId);
        RefreshNonDestuctive(false);
        isChanged = true;
    }

    public void GoldLevelUp(string petId)
    {
        var levelUp = OutgameController.Instance.GoldLevelUpPet(petId);
        RefreshNonDestuctive(levelUp);
        isChanged = true;
    }

    public void PetLevelUp(string petId)
    {
        var levelUp = OutgameController.Instance.PetLevelUpPet(petId);
        RefreshNonDestuctive(levelUp);
        isChanged = true;
    }

    public void HeartLevelUp(string petId)
    {
        var levelUp = OutgameController.Instance.HeartLevelUpPet(petId);
        RefreshNonDestuctive(levelUp);
        isChanged = true;
    }

    public void PetSell(string petId)
    {
        OutgameController.Instance.SellPet(petId);
        RefreshNonDestuctive(true);
        isChanged = true;
    }

    public void OnPetSellButtonClicked()
    {
        isSellMode = !isSellMode;
        RefreshNonDestuctive(false);
    }

}