﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections;
using Sirenix.OdinInspector;

public class UIPopupTowerPlayer : UIPopup
{
    [Title("User List")]
    [SerializeField]
	private RectTransform userListParentTransform;
    [SerializeField]
	private Transform listItemTowerPlayerPrefab;

    [SerializeField]
    private Text titleText;

    private int currentFloor = 0;

    public override void Open()
	{
		Debug.LogWarning("UIPopupReport shouldn't be open without parameters");
	}

	public void OpenWithValues(int floorIndex)
	{
		base.Open();

        this.currentFloor = floorIndex;

		Refresh();
        FocusItems();
	}

    public override void Refresh()
	{
        foreach(var tmp in userListParentTransform.GetComponentsInChildren<ListItemTowerPlayer>())
        {
            Destroy(tmp.gameObject);
        }
        
        int i = 0;
        foreach(var tmp in ServerNetworkManager.Instance.TowerFloorPlayers[currentFloor - 1].CurrentPlayers)
        {
            if(i >= ServerNetworkManager.Instance.Setting.TowerSizes[currentFloor - 1])
                break;
            i++;

            var transformToUse = Instantiate(listItemTowerPlayerPrefab);

            transformToUse.GetComponent<ListItemTowerPlayer>().UpdateEntity(
                tmp.Value
            );

            transformToUse.SetParent(userListParentTransform, false);
        }

        titleText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_tower") + " " + currentFloor + "F";
    }

	public void Report(string id)
	{
		OutgameController.Instance.ReportChatViolenceUser(id, Refresh);
	}

    public void ReportCancel(string id)
	{
		OutgameController.Instance.RequestReportCancel(id, Refresh);
	}

    public void ShowChatLog(string id)
	{
		OutgameController.Instance.ShowChatLog(id);
	}

    private void FocusItems()
    {
        StartCoroutine(FocusItemsCoroutine());
    }

    private IEnumerator FocusItemsCoroutine()
    {
        userListParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Unrestricted;
        yield return new WaitForEndOfFrame();
        userListParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Elastic;

        Canvas.ForceUpdateCanvases();

        userListParentTransform.anchoredPosition = new Vector2(userListParentTransform.anchoredPosition.x, 0f);
    }
}