﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class UIPopupGuildSetting : UIPopup
{
    [SerializeField]
	private InputField guildNoticeTextField;
    [SerializeField]
	private InputField guildRequiredStageProgressTextField;
    [SerializeField]
    private List<ListItemGuildFlag> listGuildFlag;
    [SerializeField]
	private Toggle privateToggle;

    private int selectedGuildFlagIndex = 0;
	
	public override void Open()
	{
		base.Open();

        Refresh();
	}

    public override void Refresh()
    {
        // Set guild private setting
        var tmpGuildManagementModel = ServerNetworkManager.Instance.GuildData.GuildManagementData;
       
        guildNoticeTextField.text = tmpGuildManagementModel.GuildNotice;
        this.privateToggle.isOn = tmpGuildManagementModel.IsPrivate;
        guildRequiredStageProgressTextField.text = tmpGuildManagementModel.RequiredStageProgress.ToString();

        int flagIndex = 0;
        this.selectedGuildFlagIndex = tmpGuildManagementModel.GuildFlagIndex;
        
        foreach(var tmp in listGuildFlag)
        {
            if(flagIndex == selectedGuildFlagIndex)
                tmp.UpdateEntity(flagIndex, true);
            else
                tmp.UpdateEntity(flagIndex, false);
                
            flagIndex++;
        }
    }

	public void OnConfirmButtonClicked()
	{
        if(selectedGuildFlagIndex > -1 && selectedGuildFlagIndex < listGuildFlag.Count)
        {
            bool isGuildNameViolating = false;
            bool isGuildNoticeViolating = false;
            bool isGuildRequiredStageProgressViolating = false;

            if(guildNoticeTextField.text.Length > 100)
                isGuildNoticeViolating = true;

            var settingInfo = ServerNetworkManager.Instance.Setting;
            int maxStagePrgoress = settingInfo.LastWorldIndex * settingInfo.MaxStageInWorld;

            int requiredStageProgress = Convert.ToInt32(guildRequiredStageProgressTextField.text);
            
            if(requiredStageProgress < settingInfo.GuildOpenStageProgress || requiredStageProgress > maxStagePrgoress)
                isGuildRequiredStageProgressViolating = true;

            if(isGuildNameViolating)
                UIManager.Instance.ShowAlertLocalized("message_guild_name_limit", null, null);
            else if(isGuildNoticeViolating)
                UIManager.Instance.ShowAlertLocalized("message_guild_notice_format_wrong", null, null);
            else if(isGuildRequiredStageProgressViolating)
                UIManager.Instance.ShowAlertLocalized("message_guild_required_stage_progress_wrong", null, null);
            else
                OutgameController.Instance.EditGuildSetting(guildNoticeTextField.text, this.privateToggle.isOn, requiredStageProgress, selectedGuildFlagIndex);
        }
        else
        {
            UIManager.Instance.ShowAlertLocalized("message_guild_guild_flag_index_wrong", null, null);
        }
	}

    public void RefreshSelectedGuildFlag(int selectedflagIndex)
    {
        // Check validate flag index
        if(selectedflagIndex < listGuildFlag.Count && selectedflagIndex > -1)
        {
            this.selectedGuildFlagIndex = selectedflagIndex;

            int flagIndex = 0;
            foreach(var tmp in listGuildFlag)
            {
                if(flagIndex == selectedflagIndex)
                    tmp.UpdateEntity(flagIndex, true);
                else
                    tmp.UpdateEntity(flagIndex, false);
                
                flagIndex ++;
            }
        }
    }
}
