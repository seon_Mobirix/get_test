﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections;
using Sirenix.OdinInspector;

public class UIPopupReport : UIPopup
{
    [SerializeField]
	private UITab uiTab;

    [Title("User List")]
    [SerializeField]
	private RectTransform userListParentTransform;
    [SerializeField]
	private Transform listItemChatBlockPrefab;

    [Title("Ban List")]
    [SerializeField]
	private RectTransform banListParentTransform;
    [SerializeField]
	private Transform listItemChatBanPrefab;

    private int currentTabId;

    public override void Open()
	{
		Debug.LogWarning("UIPopupReport shouldn't be open without parameters");
	}

	public void OpenWithValues(int tabId)
	{
		base.Open();

		currentTabId = tabId;
		uiTab.OnTabButton(tabId);

		Refresh();

        FocusItems();
	}

    public void OnTabButtonClicked(int tabId)
	{
		currentTabId = tabId;
        uiTab.OnTabButton(tabId);

        Refresh();

        FocusItems();
	}

    public override void Refresh()
	{
        if(currentTabId == 0)
        {
            foreach(var tmp in userListParentTransform.GetComponentsInChildren<ListItemChatBlock>())
            {
                Destroy(tmp.gameObject);
            }
            
            foreach(var tmp in ChatManager.Instance.RecentPlayers)
            {
                if(tmp.Key != ServerNetworkManager.Instance.User.PlayFabId)
                {
                    var transformToUse = Instantiate(listItemChatBlockPrefab);
                    var isBanned = false;

                    if(ServerNetworkManager.Instance.ChatBanInfo.BanList.FirstOrDefault(n => n.Id == tmp.Key) != null)
                        isBanned = true;
                    
                    transformToUse.GetComponent<ListItemChatBlock>().UpdateEntity(
                        tmp.Key,
                        tmp.Value,
                        isBanned
                    );

                    transformToUse.SetParent(userListParentTransform, false);
                }
            }
        }
        else if(currentTabId == 1)
        {
            foreach(var tmp in banListParentTransform.GetComponentsInChildren<ListItemChatBanUser>())
            {
                Destroy(tmp.gameObject);
            }

            foreach(var tmp in ServerNetworkManager.Instance.ChatBanInfo.BanList)
            {
                var transformToUse = Instantiate(listItemChatBanPrefab);

                transformToUse.GetComponent<ListItemChatBanUser>().UpdateEntity(
                    tmp.Id,
                    tmp.BanTime
                );

                transformToUse.SetParent(banListParentTransform, false);
            }   
        }
    }

	public void Report(string id)
	{
		OutgameController.Instance.ReportChatViolenceUser(id, Refresh);
	}

    public void ReportCancel(string id)
	{
		OutgameController.Instance.RequestReportCancel(id, Refresh);
	}

    public void ShowChatLog(string id)
	{
		OutgameController.Instance.ShowChatLog(id);
	}

    private void FocusItems()
    {
        StartCoroutine(FocusItemsCoroutine());
    }

    private IEnumerator FocusItemsCoroutine()
    {
        userListParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Unrestricted;
        yield return new WaitForEndOfFrame();
        userListParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Elastic;

        Canvas.ForceUpdateCanvases();

        userListParentTransform.anchoredPosition = new Vector2(userListParentTransform.anchoredPosition.x, 0f);
    }
}