﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;

public class UIPopupGuild : UIPopup
{
	[SerializeField]
	private Text titleText;

    [SerializeField]
	private UITab uiTab;
	[SerializeField]
	private Text guildTabText;
	[SerializeField]
	private List<Sprite> guildFlagIconList;

    private int currentTabId;

#region Guild Rankings  
    [Title("Guild Rankings")]
    [SerializeField]
	private RectTransform guildRankingListParentTransform;
	[SerializeField]
	private Transform listItemGuildRankingPrefab;
	[SerializeField]
	private ListItemMyGuildRanking myGuildRankingInfo;
#endregion

#region Recommend Guild Info
    [Title("Guild", "Recommend Guild Info")]
	[SerializeField]
	private RectTransform recommendedGuildInfo;
	[SerializeField]
	private Image recommendedGuildFlagIcon = null;
	[SerializeField]
	private Text recommendedGuildNameText = null;
	[SerializeField]
	private Text recommendedGuildNoticeText = null;
	[SerializeField]
	private InputField guildIdTextField = null;
	[SerializeField]
	private RectTransform recommendedGuildListParentTransform;
	[SerializeField]
	private Transform listItemRecommendedGuildPrefab;
	[SerializeField]
	private Button guildJoinButton;
	[SerializeField]
	private Transform guildAppliedButton;
	[SerializeField]
	private Button guildSearchButton;

	private GuildRecommendationModel selectedRecommendedGuild;
#endregion

#region My Guild Info
    [Title("Guild", "My Guild Info")]
	[SerializeField]
	private Image guildFlagImage = null;
	[SerializeField]
	private RectTransform myGuildInfo;
	[SerializeField]
	private Text guildRankingText = null;
	[SerializeField]
	private Text guildMemberCountText = null;
	[SerializeField]
	private Text guildNoticeText = null;
	[SerializeField]
	private Text guildLevelValueText = null;
	[SerializeField]
	private Text guildExpText = null;
	[SerializeField]
	private Text guildNameText = null;
	[SerializeField]
	private Text guildIdText = null;
	[SerializeField]
	private Slider guildExpSlider;
	[SerializeField]
	private Transform guildSettingButton;
	[SerializeField]
	private Transform freeDonateButton;
	[SerializeField]
	private Transform donateButton;
	[SerializeField]
	private Text donateButtonText;
	[SerializeField]
	private Transform kickListButton;
	[SerializeField]
	private Transform applicantListButton;
	[SerializeField]
	private Transform deleteGuildButton;
	[SerializeField]
	private Transform leaveGuildButton;
	[SerializeField]
	private Transform applicantListAlert;

	[SerializeField]
	private RectTransform guildMemeberListParentTransform;
	[SerializeField]
	private Transform listItemGuildMemberPrefab;
#endregion

#region Guild Raid
    [Title("Guild Raid")]
	
	[SerializeField]
    private Text newLeftCreateCountText = null;

	[SerializeField]
    private Button newCreateButton = null;

    [SerializeField]
    private Text leftTimeInfoText = null;

    [SerializeField]
    private Text largeChatTextArea = null;

    [SerializeField]
    private InputField chatInput = null;

    private string previousChatLog = "";

	[SerializeField]
	private Image guildRaidFlagImage = null;
	[SerializeField]
	private Text guildRaidMyGuildNameText = null;
	[SerializeField]
	private Text guildRaidMyGuildScoreText = null;

	[SerializeField]
	private RectTransform guildMemberRankingListParentTransform;
	[SerializeField]
	private Transform listItemGuildMemberRanking;

    // Cool time for buttons
    private double lastRaidJoinClickTimeStampMilliseconds;
    private double lastRaidCreatClickTimeStampMilliseconds;
#endregion

#region Guild Shop  
    [Title("Guild Shop")]
    [SerializeField]
	private RectTransform guildRelicListParentTransform;
	[SerializeField]
	private Transform listItemGuildRelicPrefab;

	private int lastUpgradeTime = -1;

#endregion


    public override void Open()
	{
		Debug.LogWarning("UIPopupGuild shouldn't be open without parameters");
	}

    public override void Close()
	{
		base.Close();
        
        OutgameController.Instance.LeaveGuildRaid();
        
        if(GameObject.FindObjectOfType<UIViewMain>() != null)
        {
            GameObject.FindObjectOfType<UIViewMain>().Refresh();
			GameObject.FindObjectOfType<UIViewMain>().SetGuildTokenAmountInfoActive(false);
            GameObject.FindObjectOfType<UIViewMain>().ShowRightTopContent();
        }
	}
    
    public void CloseWithOutShowRightTopContent()
    {
        base.Close();

        OutgameController.Instance.LeaveGuildRaid();

        if(GameObject.FindObjectOfType<UIViewMain>() != null)
            GameObject.FindObjectOfType<UIViewMain>().Refresh();
    }

	public void OpenWithValues(int tabId)
	{
		base.Open();

		if(ServerNetworkManager.Instance.Setting.IsGuildBeta)
			titleText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "title_guild") + " (BETA)";
		else
			titleText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "title_guild");

		previousChatLog = "";
		largeChatTextArea.text = "";

		currentTabId = tabId;
		uiTab.OnTabButton(tabId);
		
		if(tabId == 0)
			RefreshGuildRanking();
		else if(tabId == 1)
		{
			if(ServerNetworkManager.Instance.GuildData.GuildManagementData.GuildId == "")
				RefreshRecommendGuild();
			else
				RefreshMyGuildInfo();
		}
		else if(tabId == 2)
			RefreshGuildRaid();
		else if(tabId == 3)
			RefreshGuildShop();
	}

    public void OnTabButtonClicked(int tabId)
	{	
		if(tabId == 0)
		{
			OutgameController.Instance.LeaveGuildRaid();
			OutgameController.Instance.GetGuildRankingList(
				() => {
					currentTabId = tabId;
					uiTab.OnTabButton(tabId);
					RefreshGuildRanking();
				}
			);
		}
		else if(tabId == 1)
		{
			if(ServerNetworkManager.Instance.GuildData.GuildManagementData.GuildId == "")
			{
				OutgameController.Instance.GetGuildRecommendation(
					(leftGuildJoinTime)=>{

						if(leftGuildJoinTime > 0)
							ShowGuildJoinCoolTime(leftGuildJoinTime);
						else
						{
							currentTabId = tabId;
							uiTab.OnTabButton(tabId);
							RefreshRecommendGuild();
						}
					}
				);
			}
			else
			{
				OutgameController.Instance.LeaveGuildRaid();
				OutgameController.Instance.GetGuildData(
					false,
					() => {
						currentTabId = tabId;
						uiTab.OnTabButton(tabId);
						RefreshMyGuildInfo();
					},
					null
				);
			}
		}
		else if(tabId == 2)
		{
			OutgameController.Instance.GetGuildDataForRaid(
				() => {
					if(ServerNetworkManager.Instance.GuildData.GuildManagementData.GuildId == "")
					{
						UIManager.Instance.ShowAlertLocalized("message_guild_raid_enter_failed", null, null);
					}
					else
					{
						OutgameController.Instance.LeaveGuildRaid();
						OutgameController.Instance.EnterGuildRaid(false);
					}
				},
				null
			);
		}
		else if(tabId == 3)
		{
			if(ServerNetworkManager.Instance.GuildData.GuildManagementData.GuildId != "")
				OutgameController.Instance.LeaveGuildRaid();
			
			currentTabId = tabId;
			uiTab.OnTabButton(tabId);
			RefreshGuildShop();
		}
	}

    float passed = 0f;
    private void Update()
    {
        if(currentTabId == 2)
		{
			passed += Time.deltaTime;
			if(passed >= 0.5f && this.gameObject.activeSelf)
			{
				if(!previousChatLog.Equals(ChatManager.Instance.ChatLogGuild))
				{
					RefreshChatText();
					previousChatLog = ChatManager.Instance.ChatLogGuild;
				}
				passed = 0f;
			}
		}
    }

#region Logic For Refresh

	public void RefreshGuildRanking()
	{
		if(ServerNetworkManager.Instance.MyGuildId == "")
		{
            myGuildRankingInfo.UpdateEntity(0, 0, "", "", false);
		}
		else
		{
            int myGuildRank = ServerNetworkManager.Instance.GuildData.Ranking;
            int myGuildRankScore = ServerNetworkManager.Instance.GuildData.RankingScore;
            string myGuildName = ServerNetworkManager.Instance.GuildData.GuildManagementData.GuildName;
            string myGuildMasterName = ServerNetworkManager.Instance.GuildData.GuildMasterName;

            myGuildRankingInfo.UpdateEntity(myGuildRank, myGuildRankScore, myGuildName, myGuildMasterName, true);
		}

        foreach(var tmp in guildRankingListParentTransform.GetComponentsInChildren<ListItemGuildRanking>())
		{
			Destroy(tmp.gameObject);
		}

		foreach(var tmp in ServerNetworkManager.Instance.GuildRankList)
		{
			var transformToUse = Instantiate(listItemGuildRankingPrefab);
			transformToUse.GetComponent<ListItemGuildRanking>().UpdateEntity(
				tmp.Position,
				tmp.Score,
				tmp.GuildName,
				tmp.GuildMasterName
			);
			transformToUse.SetParent(guildRankingListParentTransform, false);
		}

		RefreshTab();
    }

	public void RefreshMyGuildInfo()
	{
		myGuildInfo.gameObject.SetActive(true);
		recommendedGuildInfo.gameObject.SetActive(false);

		var myGuildData = ServerNetworkManager.Instance.GuildData;

		guildFlagImage.sprite = guildFlagIconList[Mathf.Min(myGuildData.GuildManagementData.GuildFlagIndex, guildFlagIconList.Count)];

		if(myGuildData.RankingScore != 0)
			guildRankingText.text = string.Format("# {0}", myGuildData.Ranking);
		else
			guildRankingText.text = "-";

		guildNoticeText.text = myGuildData.GuildManagementData.GuildNotice;
		guildMemberCountText.text = string.Format("{0} / {1}", myGuildData.GuildMemberData.MemberCacheDataList.Count, myGuildData.MaxMemeberCount);		
		guildLevelValueText.text = $"Lv. {myGuildData.GuildLevel.ToString()}";

		bool isAvailableDonate = true;
		
		// Guild's max level is 20
		if(myGuildData.GuildLevel >= 20)
		{
			this.guildExpSlider.value = 1f;
			this.guildExpText.text = "Max";
			isAvailableDonate = false;
		}
		else
		{
			this.guildExpSlider.value = Mathf.Clamp01((float)myGuildData.GuildExp / (float)myGuildData.GuildNextExp);
			this.guildExpText.text = string.Format("{0}/{1}", myGuildData.GuildExp, myGuildData.GuildNextExp);
		}

		if(isAvailableDonate)
		{
			if(myGuildData.DailyLimitFreeDonateLeft > 0)
			{
				freeDonateButton.gameObject.SetActive(true);
				freeDonateButton.GetComponent<Button>().interactable = true;
				
				donateButton.gameObject.SetActive(false);
			}
			else
			{
				freeDonateButton.GetComponent<Button>().interactable = false;
				
				donateButton.gameObject.SetActive(false);
				
				/*freeDonateButton.gameObject.SetActive(false);
				donateButton.gameObject.SetActive(true);

				donateButtonText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_guild_donate"), myGuildData.DailyLimitDonateLeft, 1);*/
			}
			
			if(myGuildData.DailyLimitDonateLeft > 0)
				donateButton.GetComponent<Button>().interactable = true;
			else
				donateButton.GetComponent<Button>().interactable = false;
		}
		else
		{
			freeDonateButton.gameObject.SetActive(false);
			donateButton.gameObject.SetActive(false);
		}

		guildNameText.text = myGuildData.GuildManagementData.GuildName;
		guildIdText.text = string.Format("(ID: {0})", myGuildData.GuildManagementData.GuildId);

		// Check guild master
		if(ServerNetworkManager.Instance.User.PlayFabId == myGuildData.GuildManagementData.GuildId)
		{			
			guildSettingButton.gameObject.SetActive(true);
			kickListButton.gameObject.SetActive(true);
			deleteGuildButton.gameObject.SetActive(true);
			leaveGuildButton.gameObject.SetActive(false);

			if(myGuildData.GuildManagementData.IsPrivate)
				applicantListButton.gameObject.SetActive(true);
			else
				applicantListButton.gameObject.SetActive(false);

			// For guild appilcant red dot
			int oldApplicantListCount = 0;

			if(PlayerPrefs.HasKey("applicant_list_count"))
				oldApplicantListCount = PlayerPrefs.GetInt("applicant_list_count");

			if(myGuildData.ApplicantData.ApplicantList == null)
				applicantListAlert.gameObject.SetActive(false);
			else
			{
				if(myGuildData.ApplicantData.ApplicantList.Count > oldApplicantListCount)
					applicantListAlert.gameObject.SetActive(true);
				else
					applicantListAlert.gameObject.SetActive(false);
			}
		}
		else
		{
			guildSettingButton.gameObject.SetActive(false);
			kickListButton.gameObject.SetActive(false);
			applicantListButton.gameObject.SetActive(false);
			deleteGuildButton.gameObject.SetActive(false);
			leaveGuildButton.gameObject.SetActive(true);
		}

		foreach(var tmp in guildMemeberListParentTransform.GetComponentsInChildren<ListItemGuildMember>())
		{
			Destroy(tmp.gameObject);
		}
		
		foreach(var tmp in myGuildData.GuildMemberData.MemberCacheDataList.OrderByDescending(n => n.Id == myGuildData.GuildManagementData.GuildId).ThenByDescending(n => n.StageProgress))
		{
			bool isGuildMaster = false;

			if(tmp.Id == myGuildData.GuildManagementData.GuildId)
				isGuildMaster = true;

			var transformToUse = Instantiate(listItemGuildMemberPrefab);
			transformToUse.GetComponent<ListItemGuildMember>().UpdateEntity(
				tmp.Id,
				tmp.Name,
				tmp.StageProgress,
				tmp.TotalContribution,
				tmp.RaidScore,
				tmp.LastTimeStamp,
				isGuildMaster
			);
			transformToUse.SetParent(guildMemeberListParentTransform, false);
		}

		RefreshTab();
	}

	public void RefreshRecommendGuild()
	{
		myGuildInfo.gameObject.SetActive(false);
		recommendedGuildInfo.gameObject.SetActive(true);

		RefreshSelectedRecommendedGuildInfo(null);

		guildIdTextField.text = "";
		guildSearchButton.interactable = false;

		foreach(var tmp in recommendedGuildListParentTransform.GetComponentsInChildren<ListItemRecommendedGuild>())
		{
			Destroy(tmp.gameObject);
		}

		foreach(var tmp in ServerNetworkManager.Instance.GuildData.RecommendedGuildData.GuildDataList)
		{
			bool isAvailable = true;

			if(ServerNetworkManager.Instance.GuildData.GuildApplyData.ApplyGuildList.Exists(n => n.ToUpper() == tmp.GuildId))
				isAvailable = false;
			
			var transformToUse = Instantiate(listItemRecommendedGuildPrefab);
			transformToUse.GetComponent<ListItemRecommendedGuild>().UpdateEntity(
				tmp.GuildId,
				tmp.GuildRanking,
				tmp.GuildLevel,
				tmp.GuildFlagIndex,
				tmp.GuildName,
				tmp.GuildNotice,
				tmp.GuildRequiredStageProgress,
				tmp.GuildMemberCount,
				tmp.MaxMemeberCount,
				tmp.IsPrivate,
				isAvailable
			);
			transformToUse.SetParent(recommendedGuildListParentTransform, false);
		}

		RefreshTab();
	}

	private void RefreshSelectedRecommendedGuildInfo(GuildRecommendationModel selectedRecommendedGuild)
	{
		this.selectedRecommendedGuild = selectedRecommendedGuild;
		
		if(selectedRecommendedGuild != null)
		{
			recommendedGuildFlagIcon.gameObject.SetActive(true);
			recommendedGuildFlagIcon.sprite = guildFlagIconList[Mathf.Min(selectedRecommendedGuild.GuildFlagIndex, guildFlagIconList.Count)];
			recommendedGuildNameText.text = selectedRecommendedGuild.GuildName;
			recommendedGuildNoticeText.text = selectedRecommendedGuild.GuildNotice;

			bool isApplied = false;

			if(selectedRecommendedGuild.IsPrivate)
			{
				// Check already applied
				if(ServerNetworkManager.Instance.GuildData.GuildApplyData.ApplyGuildList.Exists(n => n.ToUpper() == selectedRecommendedGuild.GuildId))
					isApplied = true;
			}

			guildJoinButton.interactable = true;
			guildJoinButton.gameObject.SetActive(!isApplied);
			guildAppliedButton.gameObject.SetActive(isApplied);
		}
		else
		{
			recommendedGuildFlagIcon.gameObject.SetActive(false);
			recommendedGuildNameText.text = "-";
			recommendedGuildNoticeText.text = "";

			guildJoinButton.interactable = false;
			guildJoinButton.gameObject.SetActive(true);
			guildAppliedButton.gameObject.SetActive(false);
		}
	}

    public void RefreshGuildRaid()
    {
		
		guildRaidFlagImage.sprite = guildFlagIconList[Mathf.Min(ServerNetworkManager.Instance.GuildData.GuildManagementData.GuildFlagIndex, guildFlagIconList.Count)];

		newLeftCreateCountText.text = string.Format(
            LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_raid_create"),
            ServerNetworkManager.Instance.GuildRaidData.DailyLimitLeft
        );

        if(ServerNetworkManager.Instance.GuildRaidData.DailyLimitLeft > 0)
        {
			newCreateButton.interactable = true;
        }
        else
        {
			newCreateButton.interactable = false;
        }

        leftTimeInfoText.text = GetPlayLimitText();

		guildRaidMyGuildNameText.text = ServerNetworkManager.Instance.GuildData.GuildManagementData.GuildName;
		guildRaidMyGuildScoreText.text = ServerNetworkManager.Instance.GuildRaidData.Score.ToString();

		foreach(var tmp in guildMemberRankingListParentTransform.GetComponentsInChildren<ListItemGuildRanking>())
		{
			Destroy(tmp.gameObject);
		}
		
		int i = 1;
		foreach(var tmp in ServerNetworkManager.Instance.GuildRaidData.rankList.OrderByDescending(n => n.score))
		{
			var transformToUse = Instantiate(listItemGuildMemberRanking);
			transformToUse.GetComponent<ListItemGuildRanking>().UpdateEntity(i++, tmp.score, tmp.name, tmp.name);
			transformToUse.SetParent(guildMemberRankingListParentTransform, false);
		}

		RefreshTab();
		RefreshChatText();
    }

	public void RefreshGuildShop()
	{
        foreach(var tmp in guildRelicListParentTransform.GetComponentsInChildren<ListItemGuildRelic>())
		{
			Destroy(tmp.gameObject);
		}

		foreach(var tmp in BalanceInfoManager.Instance.GuildRelicInfoList)
		{
			int guildRelicLevel = 1;
			bool isOwned = false;
			bool isMaxLevel = false;

			var tmpGuildRelic = ServerNetworkManager.Instance.GuildRelicData.GuildRelicList.FirstOrDefault(n => n.Id == tmp.Id);
			
			if(tmpGuildRelic != null)
			{
				guildRelicLevel = tmpGuildRelic.Level;
				isOwned = true;
				isMaxLevel = tmpGuildRelic.IsMaxLevel;

				if(!isMaxLevel && guildRelicLevel >= tmp.MaxLevel)
					isMaxLevel = true;
			}

			var transformToUse = Instantiate(listItemGuildRelicPrefab);
			transformToUse.GetComponent<ListItemGuildRelic>().UpdateEntity(
				tmp.Id,
				guildRelicLevel,
				tmp.Price,
				isOwned,
				isMaxLevel,
				false
			);

			transformToUse.SetParent(guildRelicListParentTransform, false);
		}
    }

	private void RefreshTargetGuildRelic(string relicId, bool isShowUpgradeEffect)
    {
		foreach(var tmp in guildRelicListParentTransform.GetComponentsInChildren<ListItemGuildRelic>())
		{
			var tmpGuildRelicInfoModel = BalanceInfoManager.Instance.GuildRelicInfoList.FirstOrDefault(n => n.Id == tmp.Id);
			var tmpGuildRelicModel = ServerNetworkManager.Instance.GuildRelicData.GuildRelicList.FirstOrDefault(n => n.Id == tmp.Id);

			int relicPrice = 0;

			if(tmpGuildRelicInfoModel != null)
				relicPrice = tmpGuildRelicInfoModel.Price;

			if(tmpGuildRelicModel != null)
			{
				if(tmp.Id == relicId)
				{
					tmp.UpdateEntity(
						tmp.Id,
						tmpGuildRelicModel.Level,
						relicPrice,
						true,
						tmpGuildRelicModel.IsMaxLevel,
						isShowUpgradeEffect
					);
				}
				else
				{
					tmp.UpdateEntity(
						tmp.Id,
						tmpGuildRelicModel.Level,
						relicPrice,
						true,
						tmpGuildRelicModel.IsMaxLevel,
						false
					);
				}
			}
			else //Target guild relic not possessed
			{
				tmp.UpdateEntity(
					tmp.Id,
					1,
					relicPrice,
					false,
					false,
					false
				);
			}
		}
    }

	private void RefreshTab()
	{
		if(ServerNetworkManager.Instance.MyGuildId == "")
		{
			guildTabText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_tab_guild_search");
		}
		else
		{
			guildTabText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_tab_guild_info");
		}
	}

	private void RefreshChatText()
	{
		// smallChatTextArea.text = ChatManager.Instance.ChatLogGuild;
        largeChatTextArea.text = ChatManager.Instance.ChatLogGuild;
	}

#endregion

#region ButtonCallBack

	public void OnGuildInfoButonClicked()
	{
		var settingInfo = ServerNetworkManager.Instance.Setting;
		var text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_guild_info_2"), settingInfo.GuilFunctionCoolTime  / (60 * 60));
		text = text + "\n\n" + string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_guild_info_3"), settingInfo.FreeDonateEXPReward);
		//text = text + "\n" + string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_guild_info_4"), 100, settingInfo.DonateEXPReward, settingInfo.MaxGuildDonateValue);
		text = text + "\n\n" + LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_guild_info");

		OutgameController.Instance.ShowGenericInfo(text);
	}
	
	public void OnIDCoplyButtonClick(){
		UniClipboard.SetText(ServerNetworkManager.Instance.MyGuildId);
		UIManager.Instance.ShowAlertLocalized("message_copy_id", null, null);
	}

	public void OnInfoButtonClicked()
    {
		bool useSunday = false;
		string timeToShow = DateTimeOffset.Now.Offset.ToString();
		if(DateTimeOffset.Now.Offset.Hours < 0)
		{
			useSunday = true;
			timeToShow = (new TimeSpan(24, 0, 0) + DateTimeOffset.Now.Offset).ToString();
		}
	
		var text = (useSunday)? LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_guild_raid_info") : LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_guild_raid_info_2");

        OutgameController.Instance.ShowGenericInfo(
            String.Format(
                LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_guild_raid_info"),
				timeToShow,
                ServerNetworkManager.Instance.Setting.GuildRaidInfoList[0],
				ServerNetworkManager.Instance.Setting.GuildRaidInfoList[1],
				ServerNetworkManager.Instance.Setting.GuildRaidInfoList[2],
				ServerNetworkManager.Instance.Setting.GuildRaidInfoList[3],
				ServerNetworkManager.Instance.Setting.GuildRaidInfoList[4],
				ServerNetworkManager.Instance.Setting.GuildRaidInfoList[5],
				ServerNetworkManager.Instance.Setting.GuildRaidInfoList[6]
            )
			+ "\n\n"
			+  String.Format(
                LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_guild_raid_info_4"),
				ServerNetworkManager.Instance.Setting.GuildRaidTokenInfoList[0],
				ServerNetworkManager.Instance.Setting.GuildRaidTokenInfoList[1],
				ServerNetworkManager.Instance.Setting.GuildRaidTokenInfoList[2],
				ServerNetworkManager.Instance.Setting.GuildRaidTokenInfoList[3],
				ServerNetworkManager.Instance.Setting.GuildRaidTokenInfoList[4]
			)
			+ "\n\n"
			+  String.Format(
                LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_guild_raid_info_5"),
				ServerNetworkManager.Instance.Setting.GuildRaidExpInfoList[0],
				ServerNetworkManager.Instance.Setting.GuildRaidExpInfoList[1],
				ServerNetworkManager.Instance.Setting.GuildRaidExpInfoList[2],
				ServerNetworkManager.Instance.Setting.GuildRaidExpInfoList[3],
				ServerNetworkManager.Instance.Setting.GuildRaidExpInfoList[4]
			)
        );   
    }

	public void OnGuildJoinButtonClicked()
	{
		if(selectedRecommendedGuild != null)
			OutgameController.Instance.JoinGuild(selectedRecommendedGuild.GuildId, selectedRecommendedGuild.IsPrivate);
	}

	public void OnCancelApplyGuildButtonClicked()
	{
		if(selectedRecommendedGuild != null)
			OutgameController.Instance.CancelApplyGuild(selectedRecommendedGuild.GuildId);
	}

	public void OnGuildCreateButtonClicked()
	{
		OutgameController.Instance.ShowGuildCreatePopup();
	}

	public void OnGuildSettingButtonClicked()
	{
		OutgameController.Instance.ShowGuildSettingPopup();
	}

	public void OnGuildSearchButtonClicked()
	{
		OutgameController.Instance.GetGuildWithId(guildIdTextField.text);
	}

	public void OnRecommendedGuildRefreshButtonClicked()
	{
		OutgameController.Instance.GetGuildRecommendation(
			(leftGuildJoinTime)=>{
				RefreshRecommendGuild();
			}
		);
	}

	public void OnRecommendedGuildSelected(string selectedGuildId)
	{
		var selectedRecommendedGuild = ServerNetworkManager.Instance.GuildData.RecommendedGuildData.GuildDataList.FirstOrDefault(n => n.GuildId == selectedGuildId);
		RefreshSelectedRecommendedGuildInfo(selectedRecommendedGuild);
	}

	public void OnGuildApplicantionButtonClicked()
	{
		OutgameController.Instance.GetGuildApplicantList();
	}

	public void OnGuildKickListButtonClicked()
	{
		OutgameController.Instance.GetGuildKickList();	
	}

	public void OnGuildDeleteButtonClicked()
	{		
		UIManager.Instance.ShowAlert(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_guild_disband"), 
			() => {
				OutgameController.Instance.DeleteGuild();
			},
			() => {},
			false,
			true
		);
	}

	public void OnGuildLeaveButtonClicked()
	{
		var coolTime = ServerNetworkManager.Instance.Setting.GuildReJoinCoolTime / (60 * 60); 
		var text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_guild_quit"), coolTime);

		UIManager.Instance.ShowAlert(text, 
			() => {
				OutgameController.Instance.LeaveGuild();
			},
			() => {},
			false,
			true
		);
	}

	public void OnGuildFreeDonateButtonClicked()
	{
		if(ServerNetworkManager.Instance.GuildData.DailyLimitFreeDonateLeft > 0)
		{
			OutgameController.Instance.GuildDonateFree();
		}
	}

	public void OnGuildDonateButtonClicked()
	{
		if(ServerNetworkManager.Instance.GuildData.DailyLimitDonateLeft > 0)
		{
			OutgameController.Instance.ShowGuildDonatePopup();
		}
	}

    public void OnSendButtonClicked()
	{
        if(chatInput.text == "")
            return;

        if(ServerNetworkManager.Instance.IsChatBanned)
        {
            UIManager.Instance.ShowAlertLocalized("message_chat_banned", null, null);
            return;
        }
        
        var ranking = "?";
        if(ServerNetworkManager.Instance.RankingPosition != 0)
        {
            ranking = ChatManager.AddOrdinal(ServerNetworkManager.Instance.RankingPosition);
        }
        
        var rankColor = InterSceneManager.Instance.GetColorForRank(ServerNetworkManager.Instance.RankingPosition);

        var filteredText = ChatManager.RemoveRichText(ChatManager.Instance.FilteredText(chatInput.text));
        var message = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "chat_message_format_2"), ranking, ServerNetworkManager.Instance.User.NickName, filteredText, rankColor);
        
        message = message.Replace("@", string.Empty);

        ChatManager.Instance.SendChatTextMessageToGuild(message);
        chatInput.text = "";
	}

    public void OnCreateRaidButtonClicked()
    {
       var passedMilliSeconds = DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastRaidCreatClickTimeStampMilliseconds;

		if(passedMilliSeconds >= 500d)
		{
            lastRaidCreatClickTimeStampMilliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();

			var minuteOfDay = DateTime.UtcNow.Hour * 60 + DateTime.UtcNow.Minute;
            if(minuteOfDay < 10 || minuteOfDay >= 1435)
            {
                UIManager.Instance.ShowAlert(GetPlayLimitText(), null, null);
            }
            else
            {
				OutgameController.Instance.CreateGuildRaid(1);
			}
        }
    }

	public void OnGuildRelicUpgrade(string targetId, bool isUnlock)
	{
		var timeLeft = Mathf.Max(2f + lastUpgradeTime - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
        {
			StartCoroutine(WaitForCooldownThenUpgrade(targetId, isUnlock));
		}
		else
		{
			OutgameController.Instance.UpgradeGuildRelic(targetId, isUnlock,
				() => {
					RefreshTargetGuildRelic(targetId, true);
				}
			);

			lastUpgradeTime = (int)Time.realtimeSinceStartup;
		}
	}

	private IEnumerator WaitForCooldownThenUpgrade(string targetId, bool isUnlock)
    {
        UIManager.Instance.ShowCanvas("Loading Canvas");

        var timeLeft = Mathf.Max(2f + lastUpgradeTime - (int)Time.realtimeSinceStartup, 0);
        while(timeLeft > 0)
        {
            timeLeft = Mathf.Max(2f + lastUpgradeTime - (int)Time.realtimeSinceStartup, 0);
            yield return - 1;
        }

        UIManager.Instance.HideCanvas("Loading Canvas");

        // Upgrade again
        OnGuildRelicUpgrade(targetId, isUnlock);
    }

#endregion

#region Util
	private void ShowGuildJoinCoolTime(int timeLeft)
	{
		// Check 48 hours after leave last guild
		var timeLeftText = "00:00:00";
	
		if(timeLeft > 0)
		{
			var hours = (timeLeft / 3600);
			timeLeft -= hours * 3600;
			var minutes = (timeLeft / 60);
			timeLeft -= minutes * 60;
			var seconds = timeLeft;
			timeLeft -= seconds;

			string strHours = "00";
			string strMin = "00";
			string strSec = "00";

			if(hours < 10)
				strHours = string.Format("0{0}", hours);
			else
				strHours = hours.ToString();

			if(minutes < 10)
				strMin = string.Format("0{0}", minutes);
			else
				strMin = minutes.ToString();
			
			if(seconds < 10)
				strSec = string.Format("0{0}", seconds);
			else
				strSec = seconds.ToString();
			
			timeLeftText = string.Format("{0}:{1}:{2}", strHours, strMin, strSec);

			var toastText = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_info_left_time_rejoin_guild"), timeLeftText);
			OutgameController.Instance.ShowToast(toastText);
		}
	}

	public void OnGuildIdTextFieldValueChange()
	{		
		bool isViolating = false;

		foreach(var tmp in guildIdTextField.text)
		{
			if(!char.IsLetterOrDigit(tmp) && !char.IsWhiteSpace(tmp))
				isViolating = true;
		}

		string filteredText = guildIdTextField.text.Replace(" ","");

		if(filteredText.Length < 15)
			isViolating = true;

		if(filteredText == "")
			isViolating = true;

		if(isViolating)
			guildSearchButton.interactable = false;
		else
			guildSearchButton.interactable = true;
	}

	public void OffCogButtonsWithOutSelected(string selectedMemberId)
	{
		foreach(var tmp in guildMemeberListParentTransform.GetComponentsInChildren<ListItemGuildMember>())
		{
			if(tmp.MemberId != selectedMemberId)
			{
				tmp.OffCogButton();
			}
		}
	}

	private string GetPlayLimitText()
    {
        var offset = DateTimeOffset.Now.Offset;
		
		var startTime = offset + TimeSpan.FromMinutes(10);
        if(startTime.TotalSeconds < 0)
            startTime += new TimeSpan(24, 0, 0);
        if(startTime.Hours < 0)
            startTime = new TimeSpan(24, 0, 0) - startTime;

        var endTime = offset - TimeSpan.FromMinutes(5);
        if(endTime.TotalSeconds < 0)
            endTime += new TimeSpan(24, 0, 0);
        if(endTime.Hours < 0)
            endTime = new TimeSpan(24, 0, 0) - endTime;

        if(offset < endTime)
        {
            return string.Format(
                LanguageManager.Instance.GetTextData(LanguageDataType.UI, "desc_arena_avail_time_same"),
                startTime,
                endTime,
				offset
            );
        }
        else
        {
            return string.Format(
                LanguageManager.Instance.GetTextData(LanguageDataType.UI, "desc_arena_avail_time_different"),
                startTime,
                endTime,
				offset
            );
        }
    }

#endregion

}

