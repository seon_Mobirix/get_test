﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class UIPopupAchievement : UIPopup
{
    [SerializeField]
	private List<ListItemAchievement> listItemAchievementList;
    [SerializeField]
    private RectTransform achievementListParentTransform;

    private bool isChanged = false;

    public override void Open()
	{
		base.Open();

		isChanged = false;

		var paddingLeft = achievementListParentTransform.GetComponent<GridLayoutGroup>().padding.left;
		var paddingRight = achievementListParentTransform.GetComponent<GridLayoutGroup>().padding.right;
		var spacingX = achievementListParentTransform.GetComponent<GridLayoutGroup>().spacing.x;
		var cellWidth = (achievementListParentTransform.rect.width - spacingX * 3 - paddingLeft - paddingRight) / 4;

        var paddingTop = achievementListParentTransform.GetComponent<GridLayoutGroup>().padding.top;
		var PaddingBottom = achievementListParentTransform.GetComponent<GridLayoutGroup>().padding.bottom;
		var spacingY = achievementListParentTransform.GetComponent<GridLayoutGroup>().spacing.y;
		var cellHeight = (achievementListParentTransform.rect.height - spacingY * 2 - paddingTop - PaddingBottom) / 3;

        achievementListParentTransform.GetComponent<GridLayoutGroup>().cellSize = new UnityEngine.Vector2(cellWidth, cellHeight);
	
		Refresh();
	}

    public override void Close()
    {
        base.Close();

        // Need to sync data so far
        if(isChanged)
            OutgameController.Instance.SyncPlayerData(false, "UIPopupAchievement");

		isChanged = false;
    }

    public override void Refresh()
    {
        int idx = 0;

        foreach(var tmp in BalanceInfoManager.Instance.AchievementInfoList)
        {
            var tmpAchievementModel = ServerNetworkManager.Instance.User.AchievementInfo.FirstOrDefault(n => n.Id == tmp.Id);

            if(tmpAchievementModel != null)
            {
                int previousGoal = 0;

                if(tmpAchievementModel.Level > 0)
                {
                    if(tmp.IsIncreaseGoal)
                        previousGoal = tmpAchievementModel.Goal - Mathf.Min(tmp.BasicGoal * tmpAchievementModel.Level, tmp.MaxIncreaseGoal);
                    else
                        previousGoal = tmpAchievementModel.Goal - tmp.BasicGoal;
                }

                if(tmpAchievementModel.Id == "achievement_05" || tmpAchievementModel.Id == "achievement_07")
                {
                    if(tmpAchievementModel.Level >= tmp.MaxLevel && !tmpAchievementModel.IsMaxLevel)
                        tmpAchievementModel.IsMaxLevel = true;
                }
                
                listItemAchievementList[idx].UpdateEntity(tmp.Id, tmpAchievementModel.Count, tmpAchievementModel.Goal, previousGoal, tmp.GemReward, tmpAchievementModel.IsMaxLevel);
            }
            
            idx ++;
        }
    }

    public void RequestReward(string achievementId)
	{
		OutgameController.Instance.RequestAchievementReward(achievementId, (rewardGem) => {
            isChanged = true;
            
            if(rewardGem != 0)
                OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_thank_you_with_rewards"), rewardGem));

            Refresh();
        });
	}
}