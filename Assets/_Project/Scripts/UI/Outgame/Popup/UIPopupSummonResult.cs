﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class UIPopupSummonResult : UIPopup
{
    [SerializeField]
	private RectTransform summonResultParentFor1;
    [SerializeField]
	private RectTransform summonResultParentFor11;
    [SerializeField]
	private RectTransform summonResultParentFor55;

    [SerializeField]
    private Transform listItemSummonResultPrefab;

    [SerializeField]
    private List<Button> summon1Buttons = new List<Button>();

    [SerializeField]
    private List<Button> summon11Buttons = new List<Button>();

    [SerializeField]
    private List<Button> summon55Buttons = new List<Button>();

    private SummonType summonType = SummonType.WEAPON;
    private List<string> summonedItems = new List<string>();
    private List<bool> newInfoList = new List<bool>();
    private List<bool> whiteInfoList = new List<bool>();
    private int currentlyShownItemIndex = 0;

    // For skip
    [SerializeField]
    private List<ListItemSummonResult> resultListCache = new List<ListItemSummonResult>();

    public override void Open()
	{
		Debug.LogWarning("UIPopupSummonResult shouldn't be open without parameters");
	}

	public void OpenWithValues(SummonType summonType, List<string> summonedItems, List<bool> newInfoList, List<bool> whiteInfoList)
	{
        base.Open();
        
        this.summonType = summonType;
        this.summonedItems = summonedItems;
        this.newInfoList = newInfoList;
        this.whiteInfoList = whiteInfoList;
        Refresh();
    }

    public override void Refresh()
    {
        foreach(var tmp in summonResultParentFor1.GetComponentsInChildren<ListItemSummonResult>())
        {
			Destroy(tmp.gameObject);
		}
        foreach(var tmp in summonResultParentFor11.GetComponentsInChildren<ListItemSummonResult>())
        {
			Destroy(tmp.gameObject);
		}
        foreach(var tmp in summonResultParentFor55.GetComponentsInChildren<ListItemSummonResult>())
        {
			Destroy(tmp.gameObject);
		}

        resultListCache.Clear();

        if(this.summonedItems.Count == 1)
        {
            summonResultParentFor1.parent.gameObject.SetActive(true);
            summonResultParentFor11.parent.gameObject.SetActive(false);
            summonResultParentFor55.parent.gameObject.SetActive(false);

            for(int i = 0; i < this.summonedItems.Count; i++)
            {
                var transformToUse = Instantiate(listItemSummonResultPrefab);

                if(this.newInfoList[i])
                    transformToUse.GetComponent<ListItemSummonResult>().UpdateEntity(summonType, this.summonedItems[i], true, this.whiteInfoList[i], 1f, 1f);
                else
                    transformToUse.GetComponent<ListItemSummonResult>().UpdateEntity(summonType, this.summonedItems[i], false, this.whiteInfoList[i], 0.1f, 1f);
                transformToUse.SetParent(summonResultParentFor1, false);

                resultListCache.Add(transformToUse.GetComponent<ListItemSummonResult>());
            }
        }
        else if(this.summonedItems.Count == 11)
        {
            summonResultParentFor1.parent.gameObject.SetActive(false);
            summonResultParentFor11.parent.gameObject.SetActive(true);
            summonResultParentFor55.parent.gameObject.SetActive(false);

            var delay = 0f;
            for(int i = 0; i < this.summonedItems.Count; i++)
            {
                var transformToUse = Instantiate(listItemSummonResultPrefab);
                if(this.newInfoList[i])
                {
                    delay += 1f;
                    transformToUse.GetComponent<ListItemSummonResult>().UpdateEntity(summonType, this.summonedItems[i], true, this.whiteInfoList[i], delay, 1f);
                }
                else
                {
                    delay += 0.1f;
                    transformToUse.GetComponent<ListItemSummonResult>().UpdateEntity(summonType, this.summonedItems[i], false, this.whiteInfoList[i], delay, 1f);
                }
                transformToUse.SetParent(summonResultParentFor11, false);

                resultListCache.Add(transformToUse.GetComponent<ListItemSummonResult>());
            }
        }
        else
        {
            summonResultParentFor1.parent.gameObject.SetActive(false);
            summonResultParentFor11.parent.gameObject.SetActive(false);
            summonResultParentFor55.parent.gameObject.SetActive(true);

            var delay = 0f;
            for(int i = 0; i < this.summonedItems.Count; i++)
            {
                var transformToUse = Instantiate(listItemSummonResultPrefab);
                if(this.newInfoList[i])
                {
                    delay += 1f;
                    transformToUse.GetComponent<ListItemSummonResult>().UpdateEntity(summonType, this.summonedItems[i], true, this.whiteInfoList[i], delay, 0.5f);
                }
                else
                {
                    delay += 0.1f;
                    transformToUse.GetComponent<ListItemSummonResult>().UpdateEntity(summonType, this.summonedItems[i], false, this.whiteInfoList[i], delay, 0.5f);
                }
                transformToUse.SetParent(summonResultParentFor55, false);

                resultListCache.Add(transformToUse.GetComponent<ListItemSummonResult>());
            }
        }

        if(this.summonType == SummonType.WEAPON || this.summonType == SummonType.CLASS)
        {
            foreach(var tmp in summon1Buttons)
                tmp.interactable = (ServerNetworkManager.Instance.TotalGem >= 10);
        
            foreach(var tmp in summon11Buttons)
                tmp.interactable = (ServerNetworkManager.Instance.TotalGem >= 100);

            foreach(var tmp in summon55Buttons)
                tmp.interactable = (ServerNetworkManager.Instance.TotalGem >= 500);
        }
        else if(this.summonType == SummonType.PET)
        {
            foreach(var tmp in summon1Buttons)
                tmp.interactable = (ServerNetworkManager.Instance.TotalGem >= 30);
        
            foreach(var tmp in summon11Buttons)
                tmp.interactable = (ServerNetworkManager.Instance.TotalGem >= 300);

            foreach(var tmp in summon55Buttons)
                tmp.interactable = (ServerNetworkManager.Instance.TotalGem >= 1500);
        }
        else if(this.summonType == SummonType.RELIC)
        {
            foreach(var tmp in summon1Buttons)
                tmp.interactable = (ServerNetworkManager.Instance.TotalGem >= 100);
        
            foreach(var tmp in summon11Buttons)
                tmp.interactable = (ServerNetworkManager.Instance.TotalGem >= 1000);

            foreach(var tmp in summon55Buttons)
                tmp.interactable = (ServerNetworkManager.Instance.TotalGem >= 5000);
        }
        else if(this.summonType == SummonType.LEGEMDARY_RELIC)
        {
            foreach(var tmp in summon1Buttons)
                tmp.interactable = (ServerNetworkManager.Instance.TotalGem >= 200);
        
            foreach(var tmp in summon11Buttons)
                tmp.interactable = (ServerNetworkManager.Instance.TotalGem >= 2000);

            foreach(var tmp in summon55Buttons)
                tmp.interactable = (ServerNetworkManager.Instance.TotalGem >= 10000);
        }
        else if(this.summonType == SummonType.MERCENARY)
        {
            foreach(var tmp in summon1Buttons)
                tmp.interactable = (ServerNetworkManager.Instance.TotalGem >= ServerNetworkManager.Instance.Setting.MercenarySummonPrice);
        
            foreach(var tmp in summon11Buttons)
                tmp.interactable = (ServerNetworkManager.Instance.TotalGem >= ServerNetworkManager.Instance.Setting.MercenarySummonPrice * 10);

            foreach(var tmp in summon55Buttons)
                tmp.interactable = (ServerNetworkManager.Instance.TotalGem >= ServerNetworkManager.Instance.Setting.MercenarySummonPrice * 50);
        }
        else
        {
            foreach(var tmp in summon1Buttons)
                tmp.interactable = (ServerNetworkManager.Instance.TotalGem >= 300);
        
            foreach(var tmp in summon11Buttons)
                tmp.interactable = (ServerNetworkManager.Instance.TotalGem >= 3000);

            foreach(var tmp in summon55Buttons)
                tmp.interactable = (ServerNetworkManager.Instance.TotalGem >= 15000);
        }
    }

    public void OnSummonButtonClicked(int count)
    {
        Close();

        resultListCache.Clear();

        if(GameObject.FindObjectOfType<UIPopupSummon2>() != null)
            GameObject.FindObjectOfType<UIPopupSummon2>().Summon(this.summonType, count);
    }

    public void OnSkipButtonClicked()
    {
        foreach(var tmp in resultListCache)
        {
            tmp.SkipReveal();
        }

        resultListCache.Clear();
    }

}