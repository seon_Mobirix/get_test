﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using SB;

public class UIPopupMoreGame : UIPopup
{
    [SerializeField]
    private Image backgroundImage = null;

    [SerializeField]
    private Image closeButtonIcon = null;

    [SerializeField]
    private Image moreButtonIcon = null;

    [SerializeField]
    private Image startButtonIcon = null;

    [SerializeField]
    private GridLayoutGroup gridLayoutGroup = null;
    [SerializeField]
    private RectTransform moreGameListParentTransform;

    [SerializeField]
    List<ListItemMoreGame> moreGameList = new List<ListItemMoreGame>();

    public override void Open()
	{
		base.Open();

        var paddingX = (moreGameListParentTransform.rect.width - gridLayoutGroup.cellSize.x * 6) / 5;
        var paddingY = moreGameListParentTransform.rect.height - gridLayoutGroup.cellSize.y * 2 - gridLayoutGroup.padding.bottom - gridLayoutGroup.padding.top;

        gridLayoutGroup.spacing = new Vector2(paddingX, paddingY);

		Refresh();
	}

	public override void Refresh()
    {
        MoreManager.IconInfo[] iConInfo = new MoreManager.IconInfo[12];
        MoreManager.MoreEtcImgInfo moreEtcImgInfo = new MoreManager.MoreEtcImgInfo();
        MoreManager.Instance.getIconlist(ref iConInfo, ref moreEtcImgInfo);

        for (int i = 0; i < iConInfo.Length; i++)
        {
            // Set shadow image 
            loadTextureFromFile(ref moreGameList[i]._imgShadow, moreEtcImgInfo.strBtnbackfile);
            // moreGameList[i]._imgShadow.SetNativeSize();

            // Set icon image 
            moreGameList[i].packageInfo = iConInfo[i].strPackage;
            loadTextureFromFile(ref moreGameList[i]._imgIcon, iConInfo[i].strImgfile);
            // moreGameList[i]._imgIcon.SetNativeSize();

            // if this game already install then make it more dark
            if (iConInfo[i].nExist == MoreManager.MOBI_EXIST._exist)
                moreGameList[i]._imgIcon.color = new Color(1, 1, 1, 0.2941f);
            else
                moreGameList[i]._imgIcon.color = Color.white;

            if (iConInfo[i].nExist == MoreManager.MOBI_EXIST._exist)
            {
                moreGameList[i]._imgIcon.color = new Color(1, 1, 1, 0.2941f);
                
                moreGameList[i]._imgGotIt.enabled = true;
                loadTextureFromFile(ref moreGameList[i]._imgGotIt, moreEtcImgInfo.strGotitfile);
                // moreGameList[i]._imgGotIt.SetNativeSize();
            }
            else
            {
                moreGameList[i]._imgIcon.color = Color.white;
                moreGameList[i]._imgGotIt.enabled = false;
            }
        }
        
        // Get background image
        loadTextureFromFile(ref backgroundImage, moreEtcImgInfo.strBackgroundfile);

        // Get close button image 
        loadTextureFromFile(ref closeButtonIcon, moreEtcImgInfo.strXbtnfile);

        // Get more button image 
        loadTextureFromFile(ref moreButtonIcon, moreEtcImgInfo.strMorebtnfile);

        // Get start button image 
        loadTextureFromFile(ref startButtonIcon, moreEtcImgInfo.strStartbtnfile);
    }

    private bool loadTextureFromFile(ref Image image, string path)
    {
        Texture2D texture = null;
        if (System.IO.File.Exists(path))
        {
            byte[] byteTexture = System.IO.File.ReadAllBytes(path);
            if (byteTexture.Length > 0)
            {
                texture = new Texture2D(2, 2);
                if (!texture.LoadImage(byteTexture))
                    return false;

                texture.filterMode = FilterMode.Bilinear;

                image.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));

                return true;
            }
        }

        return false;
    }

    public override void Close()
    {
        base.Close();
    }

    public void OnMoreButtonClicked()
    {
        MoreManager.openMoregameDeveloperPage();
    }

    public void OnStartButtonClicked()
    {
        base.Close();
    }
}
