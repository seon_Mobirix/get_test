﻿using UnityEngine;
using UnityEngine.UI;

public class UIPopupCoupon : UIPopup {

	[SerializeField]
	private InputField textField;

	public override void Close()
	{
		base.Close();
		textField.text = "";
	}

	public void OnAcceptButton()
	{
		bool isViolating = false;
		

		if(textField.text.Length == 10 && !textField.text.Contains("-"))
		{
			// Check already get coupon
			if(ServerNetworkManager.Instance.IsUsedYT10Coupon)
				isViolating = true;
			else
				textField.text = textField.text.Substring(0, 3) + "-" + textField.text.Substring(3, 4) + "-" + textField.text.Substring(7, 3);
		}

		if(textField.text.Length < 3)
			isViolating = true;

		if(isViolating)
		{
			if(ServerNetworkManager.Instance.IsUsedYT10Coupon)
				UIManager.Instance.ShowAlertLocalized("message_coupon_yt_already_use", null, null);
			else
				UIManager.Instance.ShowAlertLocalized("message_coupon_failed", null, null);
		}
        else
            OutgameController.Instance.RedeemCoupon(textField.text, () => {
                textField.text = "";
				this.Close();
            });
	}
}
