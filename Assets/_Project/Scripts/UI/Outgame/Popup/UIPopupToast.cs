﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class UIPopupToast : UIPopup
{
    [SerializeField]
    private Text toastText;

    [SerializeField]
    private CanvasGroup canvasGroup;

    private Queue<string> textQueue = new Queue<string>();
    private bool isToastShowing = false;	
	private float currentAfter = 2.5f;

	public override void Open()
	{
		// Do nothing
	}

	public override void Close()
	{
		// Do nothing
	}

    public void ShowToast(string text, bool isImportant = false)
    {
        if(isImportant)
        {
            textQueue.Clear();
            isToastShowing = false;
			StopAllCoroutines();
        }
        
        textQueue.Enqueue(text);
		if(!isToastShowing)
		{
			ShowNextToast();
		}
		else
		{
			currentAfter = 1f;
		}
    }

    private IEnumerator ShowNextToastAfter(float after)
	{
		float passed = 0f;
		currentAfter = after;
		while(passed < currentAfter)
		{
			passed += Time.deltaTime;

			if(passed / currentAfter > 0.5f)
				canvasGroup.alpha = Mathf.Pow((1f - (passed / currentAfter)) * 2f, 3f);
			else
				canvasGroup.alpha = 1f;
			
			yield return -1;
		}
		ShowNextToast();
	}

    private void ShowNextToast()
	{
		if(textQueue.Count == 0)
		{
			HideToastGraphic();
		}
		else
		{
			ShowToastGraphic();
            toastText.text = textQueue.Dequeue();
			
			// Show next announce after delay.
			if(textQueue.Count >= 1)
				StartCoroutine(ShowNextToastAfter(1f));
			else
				StartCoroutine(ShowNextToastAfter(2.5f));
		}
	}

	private void HideToastGraphic()
	{
		canvasGroup.alpha = 0f;
		isToastShowing = false;

		this.gameObject.SetActive(false);
	}

	private void ShowToastGraphic()
	{
		canvasGroup.alpha = 1f;
		isToastShowing = true;

		this.gameObject.SetActive(true);
	}

}