﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class UIPopupSummon2 : UIPopup
{
    [SerializeField]
	private UITab uiTab;

    [SerializeField]
    private Transform summonItemTransform;

    [SerializeField]
    private List<ListItemSummon> summonItems = new List<ListItemSummon>();

    [SerializeField]
    private List<ListItemSummon> summonRelic = new List<ListItemSummon>();
    
    [SerializeField]
    private List<ListItemSummon> summonMercenary = new List<ListItemSummon>();

    private int currentTabId = 0;
    private bool isChanged = false;

    private int lastSummonTime = -1;

    public override void Open()
	{
		Debug.LogWarning("UIPopupSummon shouldn't be open without parameters");
	}

    public void OpenWithValues(int tabId)
	{
        base.Open();

        isChanged = false;
		currentTabId = tabId;
		uiTab.OnTabButton(tabId);

		Refresh();
	}

    public override void Close()
    {
        base.Close();

        // Need to sync data so far
        if(isChanged)
            OutgameController.Instance.SyncPlayerData(false, "UIPopupSummon2");

        isChanged = false;

        if(GameObject.FindObjectOfType<UIViewMain>() != null)
        {
            GameObject.FindObjectOfType<UIViewMain>().Refresh();
            GameObject.FindObjectOfType<UIViewMain>().ShowRightTopContent();
        }
    }

    public void CloseWithOutShowRightTopContent()
    {
        base.Close();

        // Need to sync data so far
        if(isChanged)
            OutgameController.Instance.SyncPlayerData(false, "UIPopupSummon2");

        isChanged = false;

        if(GameObject.FindObjectOfType<UIViewMain>() != null)
            GameObject.FindObjectOfType<UIViewMain>().Refresh();
    }

    public void OnTabButtonClicked(int tabId)
	{
		currentTabId = tabId;
        uiTab.OnTabButton(tabId);

        Refresh();
	}

    public override void Refresh()
    {
        if(currentTabId == 0)
        {
            foreach(var tmp in summonItems)
                tmp.UpdateEntity();
        }
        else if(currentTabId == 1)
        {
            foreach(var tmp in summonRelic)
            {
                tmp.UpdateEntity();
                tmp.GetComponent<RectTransform>().sizeDelta = new Vector2(summonItemTransform.GetComponent<RectTransform>().sizeDelta.x, summonItemTransform.GetComponent<RectTransform>().sizeDelta.y);
                tmp.GetComponent<RectTransform>().ForceUpdateRectTransforms();
            }
        }
        else if(currentTabId == 2)
        {
            foreach(var tmp in summonMercenary)
            {
                tmp.UpdateEntity();
                tmp.GetComponent<RectTransform>().sizeDelta = new Vector2(summonItemTransform.GetComponent<RectTransform>().sizeDelta.x, summonItemTransform.GetComponent<RectTransform>().sizeDelta.y);
                tmp.GetComponent<RectTransform>().ForceUpdateRectTransforms();
            }
        }
    }

    public void Summon(SummonType summonType, int count)
    {
        var timeLeft = Mathf.Max(2f + lastSummonTime - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
        {
            StartCoroutine(WaitForCooldownThenSummon(summonType, count));
        }
        else if(Application.internetReachability == NetworkReachability.NotReachable)
        {
            UIManager.Instance.ShowAlertLocalized("message_etc", null, null);
        }
        else
        {
            isChanged = true;
            var summonResult = OutgameController.Instance.Summon(summonType, count);

            if(summonResult.isEnoughGem)
            {
                // Need to sync data so far
                OutgameController.Instance.SyncPlayerData(true, "UIPopupSummon2", () => {
                        OutgameController.Instance.OnSummonAndSync(summonType, count, summonResult.summonItemIdList, summonResult.newInfoList, summonResult.goodSummonIdList);
                    }
                );
            }

            lastSummonTime = (int)Time.realtimeSinceStartup;
        }
    }

    // Logic or blocking summon too often
    private IEnumerator WaitForCooldownThenSummon(SummonType summonType, int count)
    {
        UIManager.Instance.ShowCanvas("Loading Canvas");

        var timeLeft = Mathf.Max(2f + lastSummonTime - (int)Time.realtimeSinceStartup, 0);
        while(timeLeft > 0)
        {
            timeLeft = Mathf.Max(2f + lastSummonTime - (int)Time.realtimeSinceStartup, 0);
            yield return - 1;
        }

        UIManager.Instance.HideCanvas("Loading Canvas");

        // Summon again
        Summon(summonType, count);
    }
}