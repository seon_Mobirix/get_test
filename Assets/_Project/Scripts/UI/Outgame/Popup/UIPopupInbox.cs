﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupInbox : UIPopup
{
    [SerializeField]
	private RectTransform listParentTransform;
	[SerializeField]
	private Transform listItemPostPrefab;
	[SerializeField]
	private ScrollRect scrollRect;
	[SerializeField]
	private RectTransform rectTransform;
	[SerializeField]
	private Transform noItemText;

	protected override void OnOpen()
    {
		Refresh();
        scrollRect.verticalNormalizedPosition = 1f;

		base.OnOpen();
    }

	public override void Close()
	{
		base.Close();
		scrollRect.verticalNormalizedPosition = 0f;
        
		if(GameObject.FindObjectOfType<UIViewMain>() != null)
        {
            GameObject.FindObjectOfType<UIViewMain>().Refresh();
            GameObject.FindObjectOfType<UIViewMain>().ShowRightTopContent();
        }
	}

	public void CloseWithOutShowRightTopContent()
    {
       	base.Close();
		scrollRect.verticalNormalizedPosition = 0f;

        if(GameObject.FindObjectOfType<UIViewMain>() != null)
            GameObject.FindObjectOfType<UIViewMain>().Refresh();
    }

	public override void Refresh()
	{
		foreach(var tmp in listParentTransform.GetComponentsInChildren<ListItemPost>())
		{
			Destroy(tmp.gameObject);
		}

		if(ServerNetworkManager.Instance.Post.MailList.Count == 0)
			noItemText.gameObject.SetActive(true);
		else
		{
			noItemText.gameObject.SetActive(false);
			
			foreach(var tmp in ServerNetworkManager.Instance.Post.MailList)
			{
				var timeLeft = (int)(tmp.ExpiredTime - DateTimeOffset.Now.ToUnixTimeSeconds());

				if(timeLeft > 0)
				{
					var transformToUse = Instantiate(listItemPostPrefab);
					transformToUse.GetComponent<ListItemPost>().UpdateEntity(tmp.Id, tmp.Message, tmp.ExpiredTime, tmp.AttachedItem);
					transformToUse.SetParent(listParentTransform, false);
				}
			}
		}	
	}

	public void OnReceiveAllButtonClick()
	{
		OutgameController.Instance.RequestAttachedItemAll();
	}
}
