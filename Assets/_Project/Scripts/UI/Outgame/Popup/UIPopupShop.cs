﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;

public class UIPopupShop : UIPopup {

	[SerializeField]
	private UITab uiTab;

	[Title("Refund Policy Objects")]
	[SerializeField]
	private Transform refundPolicy;

	[Title("Package Shop")]
	[SerializeField]
	private RectTransform packageListParentTransform;
	[SerializeField]
	private Transform listItemNormalPackagePrefab;

	[Title("Gem Shop")]
	[SerializeField]
	private RectTransform gemListParentTransform;
	[SerializeField]
	private Transform listItemGemPrefab;
	
	[Title("Gold Shop")]
	[SerializeField]
	private RectTransform goldListParentTransform;
	[SerializeField]
	private Transform listItemGoldPrefab;

	[Title("VIP Shop")]
	[SerializeField]
	private Text vipRewardInfo;
	[SerializeField]
	private Text vipTicketInfo;
	[SerializeField]
	private Button requestVIPRewardButton;

	[Title("Rect Objects")]
	[SerializeField]
	private ScrollRect packageRect;
	[SerializeField]
	private ScrollRect gemRect;
	[SerializeField]
	private ScrollRect goldRect;

	[SerializeField]
	private GameObject noItemText;

	private int currentTabId;

	private double lastVIPRewardClickTimeStampMilliseconds = 0;

	private List<int> dailyPacakge1AvailCountList = new List<int>();
	private List<int> dailyPackage3AvailCountList = new List<int>();
	private List<int> weeklyPackage1AvailCountList = new List<int>();
	private List<int> weeklyPackage3AvailCountList = new List<int>();

	public override void Open()
	{
		Debug.LogWarning("UIPopupShop shouldn't be open without parameters");
	}

	public void OpenWithValues(int tabID, bool showLastItems = false)
	{
		base.Open();

		if(OptionManager.Instance.Language == OptionManager.LanguageModeType.ko)
			refundPolicy.gameObject.SetActive(true);
		else
			refundPolicy.gameObject.SetActive(false);
		
		if(GameObject.FindObjectOfType<UIPopupUpgrade>() != null && GameObject.FindObjectOfType<UIPopupUpgrade>().gameObject.activeSelf)
			GameObject.FindObjectOfType<UIPopupUpgrade>().CloseWithOutShowRightTopContent();
		if(GameObject.FindObjectOfType<UIPopupEquipment>() != null && GameObject.FindObjectOfType<UIPopupEquipment>().gameObject.activeSelf)
			GameObject.FindObjectOfType<UIPopupEquipment>().CloseWithOutShowRightTopContent();
		if(GameObject.FindObjectOfType<UIPopupPet>() != null && GameObject.FindObjectOfType<UIPopupPet>().gameObject.activeSelf)
			GameObject.FindObjectOfType<UIPopupPet>().CloseWithOutShowRightTopContent();
		if(GameObject.FindObjectOfType<UIPopupSummon2>() != null && GameObject.FindObjectOfType<UIPopupSummon2>().gameObject.activeSelf)
			GameObject.FindObjectOfType<UIPopupSummon2>().CloseWithOutShowRightTopContent();
		if(GameObject.FindObjectOfType<UIPopupRaid>() != null && GameObject.FindObjectOfType<UIPopupRaid>().gameObject.activeSelf)
			GameObject.FindObjectOfType<UIPopupRaid>().CloseWithOutShowRightTopContent();
		if(GameObject.FindObjectOfType<UIPopupArena>() != null && GameObject.FindObjectOfType<UIPopupArena>().gameObject.activeSelf)
			GameObject.FindObjectOfType<UIPopupArena>().CloseWithOutShowRightTopContent();
		if(GameObject.FindObjectOfType<UIPopupInbox>() != null && GameObject.FindObjectOfType<UIPopupInbox>().gameObject.activeSelf)
			GameObject.FindObjectOfType<UIPopupInbox>().CloseWithOutShowRightTopContent();
		if(GameObject.FindObjectOfType<UIPopupInbox2>() != null && GameObject.FindObjectOfType<UIPopupInbox2>().gameObject.activeSelf)
			GameObject.FindObjectOfType<UIPopupInbox2>().CloseWithOutShowRightTopContent();
		if(GameObject.FindObjectOfType<UIPopupOption>() != null && GameObject.FindObjectOfType<UIPopupOption>().gameObject.activeSelf)
			GameObject.FindObjectOfType<UIPopupOption>().CloseWithOutShowRightTopContent();
		if(GameObject.FindObjectOfType<UIPopupTower>() != null && GameObject.FindObjectOfType<UIPopupTower>().gameObject.activeSelf)
			GameObject.FindObjectOfType<UIPopupTower>().CloseWithOutShowRightTopContent();
		if(GameObject.FindObjectOfType<UIPopupWing>() != null && GameObject.FindObjectOfType<UIPopupWing>().gameObject.activeSelf)
			GameObject.FindObjectOfType<UIPopupWing>().CloseWithOutShowRightTopContent();
		if(GameObject.FindObjectOfType<UIPopupGuild>() != null && GameObject.FindObjectOfType<UIPopupGuild>().gameObject.activeSelf)
			GameObject.FindObjectOfType<UIPopupGuild>().CloseWithOutShowRightTopContent();

		if(GameObject.FindObjectOfType<UIViewMain>() != null)
		{
			GameObject.FindObjectOfType<UIViewMain>().HideRightTopContent();
			GameObject.FindObjectOfType<UIViewMain>().SetRaidTicketAmountInfoActive(false);
			GameObject.FindObjectOfType<UIViewMain>().SetHeartAmountInfoActive(false);
			GameObject.FindObjectOfType<UIViewMain>().SetFeatherAmountInfoActive(false);
			GameObject.FindObjectOfType<UIViewMain>().SetGuildTokenAmountInfoActive(false);
		}

		currentTabId = tabID;
		uiTab.OnTabButton(tabID);

		Refresh();

		FocusItemsToLastItems(showLastItems);
	}

	public override void Close()
	{
		base.Close();
		
		packageRect.horizontalNormalizedPosition = 0f;
		gemRect.horizontalNormalizedPosition = 0f;
		goldRect.horizontalNormalizedPosition = 0f;
		
		if(GameObject.FindObjectOfType<UIViewMain>() != null)
        {
            GameObject.FindObjectOfType<UIViewMain>().Refresh();
            GameObject.FindObjectOfType<UIViewMain>().ShowRightTopContent();
        }
	}

	public void OnRefundPolicyInfoButtonClicked()
	{
		PageOpenController.Instance.OpenRefundPolicyPage();
	}

	public void OnTabButtonClicked(int tabID)
	{
		currentTabId = tabID;
        uiTab.OnTabButton(tabID);

        Refresh();
	}

	protected override void OnOpen()
	{
		packageRect.horizontalNormalizedPosition = 0f;
		gemRect.horizontalNormalizedPosition = 0f;
		goldRect.horizontalNormalizedPosition = 0f;
		packageRect.horizontal = true;
		gemRect.horizontal = true;
		goldRect.horizontal = true;

		base.OnOpen();
	}

	public override void Refresh()
	{
		foreach(var tmp in packageListParentTransform.GetComponentsInChildren<ListItemNormalPackage>())
		{
			Destroy(tmp.gameObject);
		}

		foreach(var tmp in gemListParentTransform.GetComponentsInChildren<ListItemGem>())
		{
			Destroy(tmp.gameObject);
		}

		foreach(var tmp in goldListParentTransform.GetComponentsInChildren<ListItemGold>())
		{
			Destroy(tmp.gameObject);
		}

		switch(currentTabId)
		{
			case 0:
				UIManager.Instance.ShowCanvas("Loading Canvas");
				ServerNetworkManager.Instance.CheckDailyWeeklyPackageAvailable(
					(dailyPackage1, dailyPackage3, weeklyPackage1, weeklyPackage3) => {
						UIManager.Instance.HideCanvas("Loading Canvas");
						
						this.dailyPacakge1AvailCountList = dailyPackage1;
						this.dailyPackage3AvailCountList = dailyPackage3;
						this.weeklyPackage1AvailCountList = weeklyPackage1;
						this.weeklyPackage3AvailCountList = weeklyPackage3;

						OnRefreshPackageTab();
					},
					() => {
						UIManager.Instance.HideCanvas("Loading Canvas");
					}
				);
				break;
			case 1:
				OnRefreshGemTab();
				break;
			case 2:
				OnRefreshGoldTab();
				break;
			case 3:
				OnRefreshVIPTab();
				break;
		}
	}
    private void UpdatePackage(ListItemNormalPackage packageItem, NormalPackageModel packageModel)
    {
        var isAvailable = true;
        var leftPurchaseCount = 1;
		var purchasableCount = 1;
 
        if (packageModel.TotalPurchasablePerPeriod != 0) // check if product is consumable by time (daily or weekly)
        {
            if (packageModel.Id.Contains("daily")) // check daily or weekly
            {
				if(packageModel.TotalPurchasablePerPeriod == 1)
                	leftPurchaseCount = this.dailyPacakge1AvailCountList[packageModel.AvailInfoInPeriodIndex]; // get purchased count
				else if(packageModel.TotalPurchasablePerPeriod == 3)
                	leftPurchaseCount = this.dailyPackage3AvailCountList[packageModel.AvailInfoInPeriodIndex]; // get purchased count
            }
            else if (packageModel.Id.Contains("weekly"))
            {
				if(packageModel.TotalPurchasablePerPeriod == 1)
                	leftPurchaseCount = this.weeklyPackage1AvailCountList[packageModel.AvailInfoInPeriodIndex]; // get purchased count
				else if(packageModel.TotalPurchasablePerPeriod == 3)
                	leftPurchaseCount = this.weeklyPackage3AvailCountList[packageModel.AvailInfoInPeriodIndex]; // get purchased count
            }

            if (leftPurchaseCount <= 0) // check if product is available to purchase
                isAvailable = false;

			purchasableCount = packageModel.TotalPurchasablePerPeriod;
        }
		packageItem.UpdateEntity(
			packageModel.Id,
			packageModel.PackageBuffValue,
			packageModel.PackageBackgroundIndex,
			packageModel.GemReward,
			packageModel.FeatherReward,
			packageModel.VIPReward,
			packageModel.BonusStatReward,
			packageModel.RealMoneyPrice.ToString(),
			isAvailable,
			leftPurchaseCount,
			packageModel.TotalPurchasablePerPeriod
		);
    }

    private void OnRefreshPackageTab()
    {
        // Get next basic package id
        var availableBasicPackageIdList = new List<string>();
        string basicPackageId = "";

        foreach (var package in ServerNetworkManager.Instance.Catalog.NormalPackageList)
        {
            if (!ServerNetworkManager.Instance.PurchasedBasicPackageList.Contains(package.Id))
                availableBasicPackageIdList.Add(package.Id);
        }

        if (availableBasicPackageIdList.Count > 0)
            basicPackageId = availableBasicPackageIdList.OrderBy(n => n).ToList()[0];

#if !UNITY_EDITOR
		if(!PurchaseManager.Instance.IsInited)
		{
			noItemText.SetActive(true);
			return;
		}
#endif

        foreach (var tmp in ServerNetworkManager.Instance.Catalog.NormalPackageList.OrderBy(n => n.Id))
        {
            if (tmp.IsAvailable)
            {
				if (tmp.IsLimited)
                {
                    if (tmp.Id.Contains("bundle_package_basic"))
                    {
                        if (!ServerNetworkManager.Instance.PurchasedBasicPackageList.Contains(tmp.Id) && tmp.Id == basicPackageId)
                        {
                            var transformToUse = Instantiate(listItemNormalPackagePrefab);
                            UpdatePackage(transformToUse.GetComponent<ListItemNormalPackage>(), tmp);
                            transformToUse.SetParent(packageListParentTransform, false);
                        }
                    }
                    else if (!ServerNetworkManager.Instance.PurchasedLimitedPackageList.Contains(tmp.Id))
                    {
                        var transformToUse = Instantiate(listItemNormalPackagePrefab);
                        UpdatePackage(transformToUse.GetComponent<ListItemNormalPackage>(), tmp);
                        transformToUse.SetParent(packageListParentTransform, false);
                    }
                }
                else
                {
                    var transformToUse = Instantiate(listItemNormalPackagePrefab);
                    UpdatePackage(transformToUse.GetComponent<ListItemNormalPackage>(), tmp);
                    transformToUse.SetParent(packageListParentTransform, false);
                }
            }
			// TODO: remove below
			else if (ServerNetworkManager.Instance.Setting.Use180Package && tmp.Id == "bundle_package_weekly_3")
			{
				var transformToUse = Instantiate(listItemNormalPackagePrefab);
				UpdatePackage(transformToUse.GetComponent<ListItemNormalPackage>(), tmp);
				transformToUse.SetParent(packageListParentTransform, false);
			}
        }
        noItemText.SetActive(false);
    }

	private void OnRefreshGemTab()
	{
#if UNITY_EDITOR
		foreach(var tmp in ServerNetworkManager.Instance.Catalog.GemBundleList.OrderBy(n => n.GivingAmount))
		{
			var transformToUse = Instantiate(listItemGemPrefab);

			bool isGemBonus = true;

			if(ServerNetworkManager.Instance.PurchasedGemBundleList.Contains(tmp.ItemId))
				isGemBonus = false;

			transformToUse.GetComponent<ListItemGem>().UpdateEntity(
				tmp.ItemId,
				tmp.GivingAmount,
                tmp.VipReward,
				tmp.RealMoneyPrice.ToString(),
				isGemBonus
			);
			transformToUse.SetParent(gemListParentTransform, false);
		}
		noItemText.SetActive(false);
#else 
		if(PurchaseManager.Instance.IsInited)
		{
			foreach(var tmp in ServerNetworkManager.Instance.Catalog.GemBundleList.OrderBy(n => n.GivingAmount))
			{
				var transformToUse = Instantiate(listItemGemPrefab);

				bool isGemBonus = true;

				if(ServerNetworkManager.Instance.PurchasedGemBundleList.Contains(tmp.ItemId))
					isGemBonus = false;

				transformToUse.GetComponent<ListItemGem>().UpdateEntity(
					tmp.ItemId,
					tmp.GivingAmount,
					tmp.VipReward,
					tmp.RealMoneyPrice.ToString(),
					isGemBonus
				);
				transformToUse.SetParent(gemListParentTransform, false);
			}
			noItemText.SetActive(false);
		}
		else 
			noItemText.SetActive(true);
#endif
	}

	private void OnRefreshGoldTab()
	{
		foreach(var tmp in ServerNetworkManager.Instance.Catalog.GoldBundleList)
		{
			var transformToUse = Instantiate(listItemGoldPrefab);
			transformToUse.GetComponent<ListItemGold>().UpdateEntity(
				tmp.ItemId,
				tmp.GivingAmount,
				tmp.Price.ToString()
			);
			transformToUse.SetParent(goldListParentTransform, false);
		}
	}

	private void OnRefreshVIPTab()
	{
		// VIP Part
		vipRewardInfo.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "desc_vip_exchange"), ServerNetworkManager.Instance.Setting.RequireVIPTicket, 
		ServerNetworkManager.Instance.Setting.VIPRewardGem);
		vipTicketInfo.text = $"{ServerNetworkManager.Instance.Inventory.VipPoint} / {ServerNetworkManager.Instance.Setting.RequireVIPTicket}";

		if(ServerNetworkManager.Instance.Inventory.VipPoint >= ServerNetworkManager.Instance.Setting.RequireVIPTicket)
			requestVIPRewardButton.interactable = true;
		else
			requestVIPRewardButton.interactable = false;
	}

	public void OnPurchaseLimitedPackage(string id)
	{
		OutgameController.Instance.CheckLimitedPackageAvailable(id);
	}

	public void OnPurchaseNormalPackage(string id)
	{
		OutgameController.Instance.PurchaseNormalPackage(id);
	}

	public void OnPurchaseGrowthPackage(string id)
	{
		OutgameController.Instance.PurchaseGrowthPackage(id);
	}

	public void OnPurchaseGemBundle(string id)
	{
		OutgameController.Instance.PurchaseGemBundle(id);
	}
	
	public void OnPurchaseGoldBundle(string id)
	{
		OutgameController.Instance.PurchaseGoldBundle(id);
	}

	public void OnVIPExchangeButtonClicked()
	{
		var passedMilliSeconds = DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastVIPRewardClickTimeStampMilliseconds;

        if(passedMilliSeconds >= 500d)
        {
            lastVIPRewardClickTimeStampMilliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();
			OutgameController.Instance.RequestVIPReward(Refresh);
		}
	}

	private void FocusItemsToLastItems(bool isLast)
    {
        StartCoroutine(FocusItemsCoroutine(isLast));
    }

    private IEnumerator FocusItemsCoroutine(bool isLast)
    {
        packageListParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Unrestricted;
        yield return new WaitForEndOfFrame();
        packageListParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Elastic;

        // Focus item
		Canvas.ForceUpdateCanvases();

		if(isLast)
		{
			packageListParentTransform.pivot = new Vector2(1f, 0.5f);
			packageListParentTransform.anchorMin = new Vector2(1f, 0f);
			packageListParentTransform.anchorMax = new Vector2(1f, 1f);
		}
		else
		{
			packageListParentTransform.pivot = new Vector2(0f, 0.5f);
			packageListParentTransform.anchorMin = new Vector2(0f, 0f);
			packageListParentTransform.anchorMax = new Vector2(0f, 1f);
		}

		packageListParentTransform.anchoredPosition = new Vector2(0f, 0f);
    }
}