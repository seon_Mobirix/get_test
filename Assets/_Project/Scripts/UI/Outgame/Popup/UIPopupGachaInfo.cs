﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;

public class UIPopupGachaInfo : UIPopup
{
    [SerializeField]
	private Text gachaNameText;
    [SerializeField]
    private Transform prevButton;
    [SerializeField]
    private Transform nextButton;

    [SerializeField]
	private RectTransform listParentTransform;
    [SerializeField]
	private Transform listItemGachaInfo;

    private SummonType summonType;
    private int summonLevel;

    public override void Open()
	{
		Debug.LogWarning("UIPopupGachaInfo shouldn't be open without parameters");
	}

    public void OpenWithValues(SummonType summonType, int summonLevel)
	{
		base.Open();

        this.summonType = summonType;
        this.summonLevel = summonLevel;

		Refresh();
        
        FocusItems();
	}

    public override void Refresh()
    {
        // Setting top bar
        if(summonType == SummonType.WEAPON || summonType == SummonType.CLASS)
        {
            if(summonType == SummonType.WEAPON)
                gachaNameText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "summon_level_weapon"), summonLevel);
            else if(summonType == SummonType.CLASS)
                gachaNameText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "summon_level_class"), summonLevel);

            if(summonLevel == 1)
            {
                prevButton.gameObject.SetActive(false);
                nextButton.gameObject.SetActive(true);
            }
            else if(summonLevel >= ServerNetworkManager.Instance.Setting.MaxSummonLevel)
            {
                prevButton.gameObject.SetActive(true);
                nextButton.gameObject.SetActive(false);
            }
            else
            {
                prevButton.gameObject.SetActive(true);
                nextButton.gameObject.SetActive(true);
            }
        }
        else if(summonType == SummonType.MERCENARY)
        {
            gachaNameText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "summon_level_mercenary"), summonLevel);

            if(summonLevel == 1)
            {
                prevButton.gameObject.SetActive(false);
                nextButton.gameObject.SetActive(true);
            }
            else if(summonLevel >= ServerNetworkManager.Instance.Setting.MaxMercenarySummonLevel)
            {
                prevButton.gameObject.SetActive(true);
                nextButton.gameObject.SetActive(false);
            }
            else
            {
                prevButton.gameObject.SetActive(true);
                nextButton.gameObject.SetActive(true);
            }
        }
        else
        {
            if(summonType == SummonType.RELIC)
                gachaNameText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "summon_relic");
            else if(summonType == SummonType.LEGEMDARY_RELIC)
                gachaNameText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "summon_legendary_relic");
            else if(summonType == SummonType.PET)
                gachaNameText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "summon_pet");
        
            prevButton.gameObject.SetActive(false);
            nextButton.gameObject.SetActive(false);
        }

        // Make chance info list
        foreach(var tmp in listParentTransform.GetComponentsInChildren<ListItemGachaInfo>())
		{
			Destroy(tmp.gameObject);
		}

        if(summonType == SummonType.WEAPON)
        {
            int minimumIndexOfWeaponSummon = Mathf.Min(ServerNetworkManager.Instance.Setting.TotalWeaponCount - 1, int.Parse(BalanceInfoManager.Instance.SummonInfoList[summonLevel - 1].SummonMinIndex));

            float totalPower = 0f;
            for(int i = minimumIndexOfWeaponSummon; i < ServerNetworkManager.Instance.Setting.TotalWeaponCount; i ++)
                totalPower += float.Parse(BalanceInfoManager.Instance.WeaponInfoList[i].SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture);

            for(int i = minimumIndexOfWeaponSummon; i < ServerNetworkManager.Instance.Setting.TotalWeaponCount; i++)
			{	
				var tmp = BalanceInfoManager.Instance.WeaponInfoList[i];
                
                var transformToUse = Instantiate(listItemGachaInfo);
					transformToUse.GetComponent<ListItemGachaInfo>().UpdateEntity(
						LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "weapon_name_" + tmp.Id.Substring(tmp.Id.Length - 2)),
                        LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "rank_name_" + int.Parse(tmp.Id.Substring(tmp.Id.Length -2)).ToString("D2")),
                        float.Parse(tmp.SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture) / (float)totalPower
					);
					transformToUse.SetParent(listParentTransform, false);
            }
        }
        else if(summonType == SummonType.CLASS)
        {
            int minimumIndexOfClassSummon = Mathf.Min(ServerNetworkManager.Instance.Setting.TotalClassCount - 1, int.Parse(BalanceInfoManager.Instance.SummonInfoList[summonLevel - 1].SummonMinIndex));

            float totalPower = 0f;
            for(int i = minimumIndexOfClassSummon; i < ServerNetworkManager.Instance.Setting.TotalClassCount; i ++)
                totalPower += float.Parse(BalanceInfoManager.Instance.ClassInfoList[i].SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture);

            for(int i = minimumIndexOfClassSummon; i < ServerNetworkManager.Instance.Setting.TotalClassCount; i++)
			{	
				var tmp = BalanceInfoManager.Instance.ClassInfoList[i];
                
                var transformToUse = Instantiate(listItemGachaInfo);
					transformToUse.GetComponent<ListItemGachaInfo>().UpdateEntity(
						LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "class_name_" + tmp.Id.Substring(tmp.Id.Length - 2)),
                        LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "rank_name_" + int.Parse(tmp.Id.Substring(tmp.Id.Length -2)).ToString("D2")),
                        float.Parse(tmp.SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture) / (float)totalPower
					);
					transformToUse.SetParent(listParentTransform, false);
            }
        }
        else if(summonType == SummonType.MERCENARY)
        {
            int minimumIndexOfMercenarySummon = Mathf.Min(ServerNetworkManager.Instance.Setting.TotalMercenaryCount - 1, int.Parse(BalanceInfoManager.Instance.SummonInfoList[summonLevel - 1].SummonMinIndex));

            float totalPower = 0f;
            for(int i = minimumIndexOfMercenarySummon; i < ServerNetworkManager.Instance.Setting.TotalMercenaryCount; i ++)
                totalPower += float.Parse(BalanceInfoManager.Instance.MercenaryInfoList[i].SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture);

            for(int i = minimumIndexOfMercenarySummon; i < ServerNetworkManager.Instance.Setting.TotalMercenaryCount; i++)
			{	
				var tmp = BalanceInfoManager.Instance.MercenaryInfoList[i];
                
                var transformToUse = Instantiate(listItemGachaInfo);
					transformToUse.GetComponent<ListItemGachaInfo>().UpdateEntity(
						LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "mercenary_name_" + tmp.Id.Substring(tmp.Id.Length - 2)),
                        LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "rank_name_" + int.Parse(tmp.Id.Substring(tmp.Id.Length -2)).ToString("D2")),
                        float.Parse(tmp.SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture) / (float)totalPower
					);
					transformToUse.SetParent(listParentTransform, false);
            }
        }
        else if(summonType == SummonType.RELIC)
        {
            float totalPower = 0f;
            for(int i = 0; i < BalanceInfoManager.Instance.RelicInfoList.Count; i ++)
                totalPower += float.Parse(BalanceInfoManager.Instance.RelicInfoList[i].SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture);

            for(int i = 0; i < BalanceInfoManager.Instance.RelicInfoList.Count; i ++)
			{	
				var tmp = BalanceInfoManager.Instance.RelicInfoList[i];
                
                var transformToUse = Instantiate(listItemGachaInfo);
					transformToUse.GetComponent<ListItemGachaInfo>().UpdateEntity(
						LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "relic_name_" + tmp.Id.Substring(tmp.Id.Length - 2)),
                        "",
                        float.Parse(tmp.SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture) / (float)totalPower
					);
					transformToUse.SetParent(listParentTransform, false);
            }
        }
        else if(summonType == SummonType.LEGEMDARY_RELIC)
        {
            var convertedIndexLegendaryRelicInfoList = new List<RelicInfoModel>();

            // Remove max level legendary relic
            foreach (var tmp in BalanceInfoManager.Instance.LegendaryRelicInfoList)
            {
                var targetLegendaryRelic = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.Id == tmp.Id);
                
                if(targetLegendaryRelic != null)
                {
                    // Check level + count vs target legendary relic max level
                    if((targetLegendaryRelic.Level + targetLegendaryRelic.Count - 1) < int.Parse(tmp.MaxLevel))
                        convertedIndexLegendaryRelicInfoList.Add(tmp);
                }
                else
                    convertedIndexLegendaryRelicInfoList.Add(tmp);
            }

            float totalPower = 0f;
            for(int i = 0; i < convertedIndexLegendaryRelicInfoList.Count; i ++)
                totalPower += float.Parse(convertedIndexLegendaryRelicInfoList[i].SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture);

            for(int i = 0; i < convertedIndexLegendaryRelicInfoList.Count; i ++)
			{	
				var tmp = convertedIndexLegendaryRelicInfoList[i];
                
                var transformToUse = Instantiate(listItemGachaInfo);
					transformToUse.GetComponent<ListItemGachaInfo>().UpdateEntity(
						LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "legendary_relic_name_" + tmp.Id.Substring(tmp.Id.Length - 2)),
                        "",
                        float.Parse(tmp.SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture) / (float)totalPower
					);
					transformToUse.SetParent(listParentTransform, false);
            }
        }
        else if(summonType == SummonType.PET)
        {
            float totalPower = 0f;
            for(int i = 0; i < ServerNetworkManager.Instance.Setting.TotalPetCount; i ++)
                totalPower += float.Parse(BalanceInfoManager.Instance.PetInfoList[i].SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture);

            for(int i = 0; i < ServerNetworkManager.Instance.Setting.TotalPetCount; i++)
			{	
				var tmp = BalanceInfoManager.Instance.PetInfoList[i];
                
                var transformToUse = Instantiate(listItemGachaInfo);
					transformToUse.GetComponent<ListItemGachaInfo>().UpdateEntity(
						LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "pet_name_" + tmp.Id.Substring(tmp.Id.Length - 2)),
                        tmp.Rank,
                        float.Parse(tmp.SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture) / (float)totalPower
					);
					transformToUse.SetParent(listParentTransform, false);
            }
        }
    }

    public void OnPrevButtonClicked()
    {
        if(summonLevel == 1)
            return;
        else
        {
            summonLevel = summonLevel - 1;
            Refresh();
            FocusItems();
        }
    }

    public void OnNextButtonClicked()
    {
        int MaxSummonLevel = 0;
        
        if(summonType == SummonType.MERCENARY)
            MaxSummonLevel = ServerNetworkManager.Instance.Setting.MaxMercenarySummonLevel;
        else
            MaxSummonLevel = ServerNetworkManager.Instance.Setting.MaxSummonLevel;

        if(summonLevel == MaxSummonLevel)
            return;
        else
        {
            summonLevel = summonLevel + 1;
            Refresh();
            FocusItems();
        }
    }

    private void FocusItems()
    {
        StartCoroutine(FocusItemsCoroutine());
    }

    private IEnumerator FocusItemsCoroutine()
    {
        listParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Unrestricted;
        yield return new WaitForEndOfFrame();
        listParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Elastic;

        Canvas.ForceUpdateCanvases();

        listParentTransform.anchoredPosition = new Vector2(listParentTransform.anchoredPosition.x, 0f);
    }
}
