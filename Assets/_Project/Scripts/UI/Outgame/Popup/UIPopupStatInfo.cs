﻿using UnityEngine;
using System.Linq;
using System.Numerics;
using System.Collections.Generic;

public class UIPopupStatInfo : UIPopup
{
    [SerializeField]
    private List<ListItemStatInfo> statInfoList;

    public override void Open()
	{
		base.Open();

		Refresh();
	}

    public override void Refresh()
    {
        foreach(var tmp in statInfoList)
        {
            string basicInfoText = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "stat_info_base_" + tmp.StatType.ToString().ToLower());
            string detailInfoText = "";
            string value = "";
            string subValue = "";

            if(tmp.StatType == StatInfoType.ATTACK)
            {
                value = LanguageManager.Instance.NumberToString(NumberMainController.Instance.FinalBaseAttackDamage);
                
                string baseAttackByUpgrade = LanguageManager.Instance.NumberToString(NumberMainController.Instance.BaseAttackByUpgrade);
                string baseAttackMultiplierByWeapon = LanguageManager.Instance.NumberToString((BigInteger)(NumberMainController.Instance.BaseAttackMultiplierByWeapon * 100f));
                string baseAttackMultiplierByRelic = NumberMainController.Instance.BaseAttackMultiplierByRelic.ToString();
                string baseAttackMultiplierByPet = NumberMainController.Instance.BaseAttackMultiplierByPet.ToString();
                string baseAttackMultiplierByBuff = NumberMainController.Instance.BaseAttackMultiplierByBuff.ToString();
                string baseAttackMultiplierByMercenary = LanguageManager.Instance.NumberToString((BigInteger)(NumberMainController.Instance.BaseAttackMultiplierByMercenary * 100f));
                string baseAttackMultiplierByWing = LanguageManager.Instance.NumberToString((int)(NumberMainController.Instance.BaseAttackMultiplierByWing));

                detailInfoText = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "stat_info_detail_" + tmp.StatType.ToString().ToLower() + "_3"), 
                baseAttackByUpgrade, baseAttackMultiplierByWeapon, baseAttackMultiplierByRelic, baseAttackMultiplierByPet, baseAttackMultiplierByBuff, baseAttackMultiplierByMercenary, baseAttackMultiplierByWing, value); 
            }
            else if(tmp.StatType == StatInfoType.CRITICAL_ATTACK)
            {
                value = LanguageManager.Instance.NumberToString(NumberMainController.Instance.FinalCriticalAttackDamage);

                string criticalAttackPercentByUpgrade = LanguageManager.Instance.NumberToString(NumberMainController.Instance.CriticalAttackPercentByUpgrade);
                string criticalMultiplierByRelic = NumberMainController.Instance.CriticalMultiplierByRelic.ToString();
                float finalCriticalChance = NumberMainController.Instance.FinalCriticalChance * 100f;

                detailInfoText = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "stat_info_detail_" + tmp.StatType.ToString().ToLower()), 
                criticalAttackPercentByUpgrade, criticalMultiplierByRelic, value, finalCriticalChance);
            }
            else if(tmp.StatType == StatInfoType.SUPER_CRITICAL_ATTACK)
            {
                value = LanguageManager.Instance.NumberToString(NumberMainController.Instance.FinalSuperAttackDamage);

                string superAttackPercentByUpgrade = LanguageManager.Instance.NumberToString(NumberMainController.Instance.SuperAttackPercentByUpgrade);
                string superAttackByRelic = NumberMainController.Instance.SuperAttackByRelic.ToString();
                float finalSuperChance = NumberMainController.Instance.FinalSuperChance * 100f;

                detailInfoText = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "stat_info_detail_" + tmp.StatType.ToString().ToLower()), 
                superAttackPercentByUpgrade, superAttackByRelic, value, finalSuperChance);
            }
            else if(tmp.StatType == StatInfoType.ULTIMATE_ATTACK)
            {
                value = LanguageManager.Instance.NumberToString(NumberMainController.Instance.FinalUltimateAttackDamage);
                
                string ultimateAttackPercentByUpgrade = LanguageManager.Instance.NumberToString(NumberMainController.Instance.UltimateAttackPercentByUpgrade);
                string ultimateAttackByLegendaryRelic = NumberMainController.Instance.UltimateAttackMultiplierByLegendaryRelic.ToString();
                float finalUltimateChance = NumberMainController.Instance.FinalUltimateChance * 100f;

                detailInfoText = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "stat_info_detail2_" + tmp.StatType.ToString().ToLower()), 
                ultimateAttackPercentByUpgrade, ultimateAttackByLegendaryRelic, value, finalUltimateChance);            
            }
            else if(tmp.StatType == StatInfoType.HYPER_ATTACK)
            {
                value = LanguageManager.Instance.NumberToString(NumberMainController.Instance.FinalHyperAttackDamage);
                
                string ultimateAttackPercentByUpgrade = LanguageManager.Instance.NumberToString(NumberMainController.Instance.HyperAttackPercentByUpgrade);
                float finalHyperChance = NumberMainController.Instance.FinalHyperChance * 100f;

                detailInfoText = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "stat_info_detail_" + tmp.StatType.ToString().ToLower()), ultimateAttackPercentByUpgrade, value, finalHyperChance);            
            }
            else if(tmp.StatType == StatInfoType.ATTACK_SPEED)
                value = NumberMainController.Instance.FinalAttackSpeed.ToString("F1");
            else if(tmp.StatType == StatInfoType.BERSERK_POINT)
                value = NumberMainController.Instance.FinalBerserkTargetPoint.ToString("F1");
            else if(tmp.StatType == StatInfoType.BERSERK_RECHARGING)
                value = NumberMainController.Instance.FinalBerserkChargeSpeed.ToString("F1");
            else if(tmp.StatType == StatInfoType.BERSERK_TIME)
                value = NumberMainController.Instance.FinalBerserkTime.ToString("F1");
            else if(tmp.StatType == StatInfoType.GOLD_NORMAL)
            {
                value = LanguageManager.Instance.NumberToString(NumberMainController.Instance.FinalGoldIncome);
                
                string goldIncomeByEnemy = LanguageManager.Instance.NumberToString(NumberMainController.Instance.GoldIncomeByEnemy);
                string goldIncomeMultiplierByClass = LanguageManager.Instance.NumberToString((BigInteger)(NumberMainController.Instance.GoldIncomeMultiplierByClass * 100f));
                string goldIncomeMultiplierByRelic = NumberMainController.Instance.GoldIncomeMultiplierByRelic.ToString();
                string goldIncomeMuliplierByPet = NumberMainController.Instance.GoldIncomeMuliplierByPet.ToString();
                string goldIncomeMultiplierByBuff = NumberMainController.Instance.GoldIncomeMultiplierByBuff.ToString();
                string goldIncomeMultiplierByWing = LanguageManager.Instance.NumberToString((int)NumberMainController.Instance.GoldIncomeMuliplierByWing);

                detailInfoText = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "stat_info_detail_" + tmp.StatType.ToString().ToLower()+"_2"), 
                goldIncomeByEnemy, goldIncomeMultiplierByClass, goldIncomeMultiplierByRelic, goldIncomeMuliplierByPet, goldIncomeMultiplierByBuff, goldIncomeMultiplierByWing, value); 
            }
            else if(tmp.StatType == StatInfoType.GOLD_BERSERK)
            {
                value = LanguageManager.Instance.NumberToString(NumberMainController.Instance.FinalBerserkGoldIncome);
                detailInfoText = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "stat_info_detail_" + tmp.StatType.ToString().ToLower());

                string finalGoldIncome = LanguageManager.Instance.NumberToString(NumberMainController.Instance.FinalGoldIncome);
                string berserkGoldIncomeIncreasePercentByRelic = NumberMainController.Instance.BerserkGoldIncomeIncreasePercentByRelic.ToString();
                string berserkGoldIncomeMultiplierByBuff = NumberMainController.Instance.BerserkGoldIncomeMultiplierByBuff.ToString();

                detailInfoText = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "stat_info_detail_" + tmp.StatType.ToString().ToLower()), 
                finalGoldIncome, berserkGoldIncomeIncreasePercentByRelic, berserkGoldIncomeMultiplierByBuff, value); 
            }
            else if(tmp.StatType == StatInfoType.GOLD_PET)
                value = LanguageManager.Instance.NumberToString(NumberMainController.Instance.FinalSupplyBoxIncome);
            else if(tmp.StatType == StatInfoType.FARMING_KILL)
            {
                subValue = (NumberMainController.Instance.FinalFarmingChance * 100f).ToString("F2");

                int currentWorldIndex = (ServerNetworkManager.Instance.User.SelectedStage - 1) / ServerNetworkManager.Instance.Setting.MaxStageInWorld + 1;
                var targetStageInfoList = BalanceInfoManager.Instance.StageInfoList.FindAll(n => n.RegionId == currentWorldIndex.ToString()).OrderBy(n => n.Id).ToList();
                
                string currentMobIndex;
                
                if(ServerNetworkManager.Instance.User.SelectedStage > 9)
                    currentMobIndex = string.Format("0{0}", ServerNetworkManager.Instance.User.SelectedStage);
                else
                    currentMobIndex = string.Format("00{0}", ServerNetworkManager.Instance.User.SelectedStage);

                var targetStageInfo = targetStageInfoList.FirstOrDefault(n => n.Id == currentMobIndex);
                
                if(targetStageInfo != null)
                    value = targetStageInfo.FarmItem;
                else
                    value = "F";
                
                string farmingChanceByEnemy = (NumberMainController.Instance.FarmingChanceByEnemy * 100f).ToString("F2");
                string farmingChanceIncreasePercentByRelic = (NumberMainController.Instance.FarmingChanceIncreasePercentByRelic).ToString("F2");

                detailInfoText = string.Format(LanguageManager.Instance.GetTextData(
                    LanguageDataType.UI, 
                    "stat_info_detail_" + tmp.StatType.ToString().ToLower()), 
                    value, 
                    farmingChanceByEnemy, 
                    farmingChanceIncreasePercentByRelic, 
                    subValue
                );
            }
            else if(tmp.StatType == StatInfoType.MONSTER_HP)
                value = Mathf.Abs(NumberMainController.Instance.MonsterHPReducePercentByRelic).ToString();
            else if(tmp.StatType == StatInfoType.BOSS_MONSTER_HP)
                value = Mathf.Abs(NumberMainController.Instance.BossHPReducePercentByRelic).ToString();

            tmp.UpdateEntity(basicInfoText, value, subValue, detailInfoText);
        }
    }
}
