﻿using UnityEngine;
using UnityEngine.UI;

public class UIPopupDailyReward : UIPopup
{
    [SerializeField]
	private RectTransform dailyRewardListParentTransform;
	[SerializeField]
	private Transform listItemDailyRewardPrefab;
    [SerializeField]
	private Text nextRewardTimeText;

    private bool needUpdateNextRewardTimeCoolDown = false;
    private float nextRewardTimeCoolDownPassed = 0f;
    private int nextRewardLeftTime = 0;

    public override void Open()
	{
        Debug.LogWarning("UIPopupDailyReward shouldn't be open without parameters");
    }

    public void OpenWithValues(int nextRewardLeftTime)
    {
        base.Open();
        
        if(ServerNetworkManager.Instance.Inventory.DailyRewardTicket > 0)
        {
            this.nextRewardLeftTime = 0;
            needUpdateNextRewardTimeCoolDown = false;
            nextRewardTimeText.text = "";
        }
        else
        {
            this.nextRewardLeftTime = nextRewardLeftTime;
            needUpdateNextRewardTimeCoolDown = true;
            nextRewardTimeText.text = TimeFormatConverter(nextRewardLeftTime);
        }

		var paddingLeft = dailyRewardListParentTransform.GetComponent<GridLayoutGroup>().padding.left;
		var paddingRight = dailyRewardListParentTransform.GetComponent<GridLayoutGroup>().padding.right;
		var spacingX = dailyRewardListParentTransform.GetComponent<GridLayoutGroup>().spacing.x;

		Refresh();
    }

    public override void Refresh()
	{
		foreach(var tmp in dailyRewardListParentTransform.GetComponentsInChildren<ListItemDailyReward>())
		{
			Destroy(tmp.gameObject);
		}

        int index = 0;
        
        foreach(var tmp in ServerNetworkManager.Instance.DailyRewardTable.DailyRewardList)
		{
			var transformToUse = Instantiate(listItemDailyRewardPrefab);
            var isReceived = false;
            var isLocked = false;

            if(index <= ServerNetworkManager.Instance.DailyRewardCount)
                isReceived = true;
            else if(index > ServerNetworkManager.Instance.DailyRewardCount + 1)
                isLocked = true;
            
            if(isReceived == false && isLocked == false)
            {
                if(ServerNetworkManager.Instance.Inventory.DailyRewardTicket < 1)
                    isLocked = true;
            }
			
            transformToUse.GetComponent<ListItemDailyReward>().UpdateEntity(
                index + 1,
                tmp.ItemId,
                tmp.ItemCode,
                tmp.ItemCount,
                isReceived,
                isLocked
            );

			transformToUse.SetParent(dailyRewardListParentTransform, false);

            index = index + 1;
		}
    }

    private void Update()
	{
		if(needUpdateNextRewardTimeCoolDown)
			UpdateNextRewardTimeCoolDown();
	}

    public void UpdateNextRewardTimeCoolDown()
    {
        nextRewardTimeCoolDownPassed += Time.deltaTime;

		if(nextRewardTimeCoolDownPassed >= 1f)
		{
			nextRewardLeftTime -= Mathf.CeilToInt(nextRewardTimeCoolDownPassed);

			if(nextRewardLeftTime > 0)
			{
                nextRewardTimeText.text = TimeFormatConverter(nextRewardLeftTime);
			}
			else
			{
				needUpdateNextRewardTimeCoolDown = false;
                
                OutgameController.Instance.RefreshDailyRewardPopup(
                    (nextRewardLeftTime) => {
                            
                            if(ServerNetworkManager.Instance.Inventory.DailyRewardTicket > 0)
                            {
                                this.nextRewardLeftTime = 0;
                                nextRewardTimeText.text = "";
                            }
                            else
                            {
                                this.nextRewardLeftTime = nextRewardLeftTime;
                                nextRewardTimeText.text = TimeFormatConverter(nextRewardLeftTime);
                            }
                            
                            Refresh();
                    }
                );
			}

			nextRewardTimeCoolDownPassed = 0f;
		}
    }

    private string TimeFormatConverter(int timeLeft)
    {
        var hours = (timeLeft / 3600);
        timeLeft -= hours * 3600;
        var minutes = (timeLeft / 60);
        timeLeft -= minutes * 60;
        var seconds = timeLeft;
        timeLeft -= seconds;
        
        if(hours > 0)
            return string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "time_hours"), hours);
        else if(minutes > 0)
            return string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "time_mins"), minutes);
        else
            return LanguageManager.Instance.GetTextData(LanguageDataType.UI, "time_get_soon");
    }
}
