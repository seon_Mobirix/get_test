﻿using UnityEngine;
using System.Linq;

public class UIPopupEquipment : UIPopup
{
    [SerializeField]
	private UITab uiTab;
	[SerializeField]
	private RectTransform tabButtonGroup;
	
	[SerializeField]
	private Transform weaponRedDot;
	[SerializeField]
	private Transform classRedDot;
	[SerializeField]
	private Transform relicRedDot;
	[SerializeField]
	private Transform legendaryRelicRedDot;
	[SerializeField]
	private Transform mercenaryRedDot;

    [SerializeField]
	private RectTransform weaponListParentTransform;
	[SerializeField]
	private Transform listItemWeaponPrefab;

    [SerializeField]
	private RectTransform classListParentTransform;
	[SerializeField]
	private Transform listItemClassPrefab;

	[SerializeField]
	private RectTransform mercenaryListParentTransform;
	[SerializeField]
	private Transform listItemMercenaryPrefab;

    [SerializeField]
	private RectTransform relicListParentTransform;
	[SerializeField]
	private Transform listItemRelicPrefab;

	[SerializeField]
	private RectTransform legendaryRelicListParentTransform;
	[SerializeField]
	private Transform listItemLegendaryRelicPrefab;

    private int currentTabId;

	private bool isChanged = false;

    public override void Open()
	{
		base.Open();

		isChanged = false;
		
		currentTabId = 0;
		uiTab.OnTabButton(0);
		Refresh();
	}

	public override void Close()
    {
        base.Close();

        // Need to sync data so far
        if(isChanged)
            OutgameController.Instance.SyncPlayerData(false, "UIPopupEquipment");
            
		isChanged = false;

		if(GameObject.FindObjectOfType<UIViewMain>() != null)
		{
			GameObject.FindObjectOfType<UIViewMain>().SetHeartAmountInfoActive(false);
			GameObject.FindObjectOfType<UIViewMain>().ShowRightTopContent();
			GameObject.FindObjectOfType<UIViewMain>().RefreshRightHorizontalbar();
		}
    }

	float passedLocalCacheForceSave = 0f;
	float lastCheck = float.MinValue;
	private void Update()
	{
		passedLocalCacheForceSave += Time.deltaTime;
        if(passedLocalCacheForceSave >= 1f && this.gameObject.activeSelf)
        {
            OutgameController.Instance.SaveLocalCache(isChanged);
            passedLocalCacheForceSave = 0f;
        }
		
		if(isChanged)
		{
			if(lastCheck + ServerNetworkManager.Instance.Setting.AutoSyncPeriod < Time.realtimeSinceStartup)
			{
				// Need to sync data so far
				OutgameController.Instance.SyncPlayerData(true, "UIPopupEquipment");
				isChanged = false;

				lastCheck = Time.realtimeSinceStartup;
			}
		}
	}

	public void CloseWithOutShowRightTopContent()
    {
        base.Close();

        // Need to sync data so far
        if(isChanged)
            OutgameController.Instance.SyncPlayerData(false, "UIPopupEquipment");
		
        isChanged = false;
    }

	public void OnTabButtonClicked(int tabID)
	{
		if(currentTabId == tabID)
			return;

		if(tabID == 3)
		{
			if(ServerNetworkManager.Instance.User.StageProgress < ServerNetworkManager.Instance.Setting.LegendaryRelicOpenStageProgress)
			{
				// For need clear stage info
				int legendaryRelicOpenStageProgress = ServerNetworkManager.Instance.Setting.LegendaryRelicOpenStageProgress - 1;

				var needClearWorldIdx = (legendaryRelicOpenStageProgress - 1) / ServerNetworkManager.Instance.Setting.MaxStageInWorld + 1;
				var needClearMobIdx = (legendaryRelicOpenStageProgress - 1) % ServerNetworkManager.Instance.Setting.MaxStageInWorld + 1;
				var text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_unlock_stage_progress"), needClearWorldIdx, needClearMobIdx);

				UIManager.Instance.ShowAlert(text, null, null);

				return;
			}
		}
		else if(tabID == 4)
		{
			if(ServerNetworkManager.Instance.User.StageProgress < ServerNetworkManager.Instance.Setting.MercenaryOpenStageProgress)
			{
				// For need clear stage info
				int legendaryRelicOpenStageProgress = ServerNetworkManager.Instance.Setting.MercenaryOpenStageProgress - 1;

				var needClearWorldIdx = (legendaryRelicOpenStageProgress - 1) / ServerNetworkManager.Instance.Setting.MaxStageInWorld + 1;
				var needClearMobIdx = (legendaryRelicOpenStageProgress - 1) % ServerNetworkManager.Instance.Setting.MaxStageInWorld + 1;
				var text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_unlock_stage_progress"), needClearWorldIdx, needClearMobIdx);

				UIManager.Instance.ShowAlert(text, null, null);

				return;
			}
		}

		currentTabId = tabID;
		uiTab.OnTabButton(tabID);
		Refresh();	
	}

	// Refresh all of data
	public override void Refresh()
	{
		RefreshWeaponRedDot();
		RefreshClassRedDot();
		RefreshRelicRedDot();
		RefreshLegendaryRelicRedDot();
		RefreshMercenaryRedDot();

		// Weapon
		if(currentTabId == 0)
		{
			foreach(var tmp in weaponListParentTransform.GetComponentsInChildren<ListItemWeapon>())
			{
				Destroy(tmp.gameObject);
			}

			for(int i = 0; i < ServerNetworkManager.Instance.Setting.TotalWeaponCount; i++)
			{	
				var tmp = BalanceInfoManager.Instance.WeaponInfoList[i];
				var tmpWeaponModel = ServerNetworkManager.Instance.Inventory.WeaponList.FirstOrDefault(n => n.Id == tmp.Id);
				
				// Check user owned this item
				if(tmpWeaponModel != null)
				{
					var transformToUse = Instantiate(listItemWeaponPrefab);
					transformToUse.GetComponent<ListItemWeapon>().UpdateEntity(
						tmp.Id, 
						tmpWeaponModel.Level,
						tmpWeaponModel.Count,
						true,
						ServerNetworkManager.Instance.Inventory.IsEquipedWeapon(tmpWeaponModel.Id),
						false,
						false
					);
					transformToUse.SetParent(weaponListParentTransform, false);
				}
				else
				{
					var transformToUse = Instantiate(listItemWeaponPrefab);
					transformToUse.GetComponent<ListItemWeapon>().UpdateEntity(
						tmp.Id,
						1,
						0,
						false,
						false,
						false,
						false
					);
					transformToUse.SetParent(weaponListParentTransform, false);
				}
			}
		}
		// Class
		else if(currentTabId == 1)
		{
			foreach(var tmp in classListParentTransform.GetComponentsInChildren<ListItemClass>())
			{
				Destroy(tmp.gameObject);
			}

			for(int i = 0; i < ServerNetworkManager.Instance.Setting.TotalClassCount; i++)
			{	
				var tmp = BalanceInfoManager.Instance.ClassInfoList[i];
				var tmpClassModel = ServerNetworkManager.Instance.Inventory.ClassList.FirstOrDefault(n => n.Id == tmp.Id);
				
				// Check user owned this item
				if(tmpClassModel != null)
				{
					var transformToUse = Instantiate(listItemClassPrefab);
					transformToUse.GetComponent<ListItemClass>().UpdateEntity(
						tmp.Id, 
						tmpClassModel.Level,
						tmpClassModel.Count,
						true,
						ServerNetworkManager.Instance.Inventory.IsEquipedClass(tmpClassModel.Id),
						false,
						false
					);
					transformToUse.SetParent(classListParentTransform, false);
				}
				else
				{
					var transformToUse = Instantiate(listItemClassPrefab);
					transformToUse.GetComponent<ListItemClass>().UpdateEntity(
						tmp.Id,
						1,
						0,
						false,
						false,
						false,
						false
					);
					transformToUse.SetParent(classListParentTransform, false);
				}
			}
		}
		// Relic
		else if(currentTabId == 2)
		{
			foreach(var tmp in relicListParentTransform.GetComponentsInChildren<ListItemRelic>())
			{
				Destroy(tmp.gameObject);
			}

			foreach(var tmp in BalanceInfoManager.Instance.RelicInfoList)
			{	
				var tmpRelicModel = ServerNetworkManager.Instance.Inventory.RelicList.FirstOrDefault(n => n.Id == tmp.Id);
				var tmpExtendedRelicModel = ServerNetworkManager.Instance.Inventory.ExtendedRelicList.FirstOrDefault(n => n.Id == tmp.Id);
				
				if(tmpExtendedRelicModel != null && tmpRelicModel != null) // Originally the player possessed the relic and also extended with bonus stat
				{
					var transformToUse = Instantiate(listItemRelicPrefab);
					transformToUse.GetComponent<ListItemRelic>().UpdateEntity(
						tmp.Id,
						tmpRelicModel.Level,
						tmpExtendedRelicModel.Level,
						tmpExtendedRelicModel.Count,
						true,
						false,
						false
					);
					transformToUse.SetParent(relicListParentTransform, false);
				}
				else if(tmpExtendedRelicModel != null && tmpRelicModel == null) // Only extended relic list has it
				{
					var transformToUse = Instantiate(listItemRelicPrefab);
					transformToUse.GetComponent<ListItemRelic>().UpdateEntity(
						tmp.Id,
						0,
						tmpExtendedRelicModel.Level,
						tmpExtendedRelicModel.Count,
						true,
						false,
						false
					);
					transformToUse.SetParent(relicListParentTransform, false);
				}
				else // No relic possessed
				{
					var transformToUse = Instantiate(listItemRelicPrefab);
					transformToUse.GetComponent<ListItemRelic>().UpdateEntity(
						tmp.Id,
						0,
						0,
						0,
						false,
						false,
						false
					);
					transformToUse.SetParent(relicListParentTransform, false);
				}
			}
		}
		else if(currentTabId == 3)
		{
			foreach(var tmp in legendaryRelicListParentTransform.GetComponentsInChildren<ListItemLegendaryRelic>())
			{
				Destroy(tmp.gameObject);
			}

			foreach(var tmp in BalanceInfoManager.Instance.LegendaryRelicInfoList)
			{	
				var tmpLegendaryRelicModel = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.Id == tmp.Id);
				
				if(tmpLegendaryRelicModel != null) // Originally the player possessed the relic and also extended with bonus stat
				{
					var transformToUse = Instantiate(listItemLegendaryRelicPrefab);
					transformToUse.GetComponent<ListItemLegendaryRelic>().UpdateEntity(
						tmp.Id,
						tmpLegendaryRelicModel.Level,
						tmpLegendaryRelicModel.Count,
						true,
						false,
						false
					);
					transformToUse.SetParent(legendaryRelicListParentTransform, false);
				}
				else // No relic possessed
				{
					var transformToUse = Instantiate(listItemLegendaryRelicPrefab);
					transformToUse.GetComponent<ListItemLegendaryRelic>().UpdateEntity(
						tmp.Id,
						0,
						0,
						false,
						false,
						false
					);
					transformToUse.SetParent(legendaryRelicListParentTransform, false);
				}
			}
		}
		// Mercenary
		else if(currentTabId == 4)
		{
			foreach(var tmp in mercenaryListParentTransform.GetComponentsInChildren<ListItemMercenary>())
			{
				Destroy(tmp.gameObject);
			}

			for(int i = 0; i < ServerNetworkManager.Instance.Setting.TotalMercenaryCount; i++)
			{	
				var tmp = BalanceInfoManager.Instance.MercenaryInfoList[i];
				var tmpMercenaryModel = ServerNetworkManager.Instance.Inventory.MercenaryList.FirstOrDefault(n => n.Id == tmp.Id);
				
				// Check user owned this item
				if(tmpMercenaryModel != null)
				{
					var transformToUse = Instantiate(listItemMercenaryPrefab);
					transformToUse.GetComponent<ListItemMercenary>().UpdateEntity(
						tmp.Id, 
						tmpMercenaryModel.Level,
						tmpMercenaryModel.Count,
						true,
						ServerNetworkManager.Instance.Inventory.IsEquipedMercenary(tmpMercenaryModel.Id),
						false,
						false
					);
					transformToUse.SetParent(mercenaryListParentTransform, false);
				}
				else
				{
					var transformToUse = Instantiate(listItemMercenaryPrefab);
					transformToUse.GetComponent<ListItemMercenary>().UpdateEntity(
						tmp.Id,
						1,
						0,
						false,
						false,
						false,
						false
					);
					transformToUse.SetParent(mercenaryListParentTransform, false);
				}
			}
		}
	}

	public void RefreshWithOutDestroy()
	{
		RefreshWeaponRedDot();
		RefreshClassRedDot();
		RefreshMercenaryRedDot();

		if(currentTabId == 0)
			RefreshWeaponList();
		else if(currentTabId == 1)
			RefreshClassList();
		else if(currentTabId == 4)
			RefreshMercenaryList();
	}

#region Related With Weapon

	private void RefreshWeaponRedDot()
	{
		bool isRedDot = false;

		// Check weapon
        foreach(var tmp in ServerNetworkManager.Instance.Inventory.WeaponList)
        {
            if(tmp.IsNew)
				isRedDot = true;

			tmp.IsNew = false;
        }

        if(isRedDot)
            weaponRedDot.gameObject.SetActive(true);
        else
            weaponRedDot.gameObject.SetActive(false);
	}

	private void RefreshWeaponList()
	{
		foreach(var tmp in weaponListParentTransform.GetComponentsInChildren<ListItemWeapon>())
		{
			var tmpWeaponModel = ServerNetworkManager.Instance.Inventory.WeaponList.FirstOrDefault(n => n.Id == tmp.Id);
			
			if(tmpWeaponModel != null)
			{
				tmp.UpdateEntity(
					tmp.Id, 
					tmpWeaponModel.Level,
					tmpWeaponModel.Count,
					true,
					ServerNetworkManager.Instance.Inventory.IsEquipedWeapon(tmpWeaponModel.Id),
					false,
					false
				);
			}
			else
			{
				tmp.UpdateEntity(
					tmp.Id,
					1,
					0,
					false,
					false,
					false,
					false
				);
			}
		}
	}

	private void RefreshWeaponListWithTarget(string weaponId, bool isUpgradeSuccess)
	{
		foreach(var tmp in weaponListParentTransform.GetComponentsInChildren<ListItemWeapon>())
		{
			var tmpWeaponModel = ServerNetworkManager.Instance.Inventory.WeaponList.FirstOrDefault(n => n.Id == tmp.Id);

			if(tmpWeaponModel != null)
			{
				// Check target weapon
				if(tmpWeaponModel.Id == weaponId)
				{
					tmp.UpdateEntity(
						tmpWeaponModel.Id, 
						tmpWeaponModel.Level,
						tmpWeaponModel.Count,
						true,
						ServerNetworkManager.Instance.Inventory.IsEquipedWeapon(tmpWeaponModel.Id),
						true,
						isUpgradeSuccess
					);
				}
				else
				{
					tmp.UpdateEntity(
						tmpWeaponModel.Id, 
						tmpWeaponModel.Level,
						tmpWeaponModel.Count,
						true,
						ServerNetworkManager.Instance.Inventory.IsEquipedWeapon(tmpWeaponModel.Id),
						false,
						false
					);
				}
			}
			else
			{
				tmp.UpdateEntity(
					tmp.Id,
					1,
					0,
					false,
					false,
					false,
					false
				);
			}
		}
	}

	public void RequestWeaponEquip(string weaponId)
    {
		OutgameController.Instance.EquipWeapon(weaponId, () => {
			RefreshWeaponList();
			isChanged = true;
		});
    }

	public void RequestWeaponMerge(string weaponId)
    {
		OutgameController.Instance.MergeWeapon(weaponId, (isUpgradeSuccess) => {
			RefreshWeaponListWithTarget(weaponId, isUpgradeSuccess);
			isChanged = true;

			RefreshWeaponRedDot();
			RefreshClassRedDot();
		});
    }

	public void RequestWeaponUpgrade(string weaponId)
    {
		OutgameController.Instance.UpgradeWeapon(weaponId, (isUpgradeSuccess) => {
			RefreshWeaponListWithTarget(weaponId, isUpgradeSuccess);
			isChanged = true;

			RefreshWeaponRedDot();
			RefreshClassRedDot();
		});
    }
#endregion

#region Related With Class
	
	private void RefreshClassRedDot()
	{
		bool isRedDot = false;

		// Check class
        foreach(var tmp in ServerNetworkManager.Instance.Inventory.ClassList)
        {
            if(tmp.IsNew)
				isRedDot = true;

			tmp.IsNew = false;
        }

		if(isRedDot)
            classRedDot.gameObject.SetActive(true);
        else
            classRedDot.gameObject.SetActive(false);
	}

	public void RefreshClassList()
	{
		foreach(var tmp in classListParentTransform.GetComponentsInChildren<ListItemClass>())
		{
			var tmpClassModel = ServerNetworkManager.Instance.Inventory.ClassList.FirstOrDefault(n => n.Id == tmp.Id);
			
			if(tmpClassModel != null)
			{
				tmp.UpdateEntity(
					tmp.Id, 
					tmpClassModel.Level,
					tmpClassModel.Count,
					true,
					ServerNetworkManager.Instance.Inventory.IsEquipedClass(tmpClassModel.Id),
					false,
					false
				);
			}
			else
			{
				tmp.UpdateEntity(
					tmp.Id,
					1,
					0,
					false,
					false,
					false,
					false
				);
			}
		}
	}

	private void RefreshClassListWithTarget(string classId, bool isUpgradeSuccess)
	{
		foreach(var tmp in classListParentTransform.GetComponentsInChildren<ListItemClass>())
		{
			var tmpClassModel = ServerNetworkManager.Instance.Inventory.ClassList.FirstOrDefault(n => n.Id == tmp.Id);

			if(tmpClassModel != null)
			{
				// Check target class
				if(tmpClassModel.Id == classId)
				{
					tmp.UpdateEntity(
						tmpClassModel.Id, 
						tmpClassModel.Level,
						tmpClassModel.Count,
						true,
						ServerNetworkManager.Instance.Inventory.IsEquipedClass(tmpClassModel.Id),
						true,
						isUpgradeSuccess
					);
				}
				else
				{
					tmp.UpdateEntity(
						tmpClassModel.Id, 
						tmpClassModel.Level,
						tmpClassModel.Count,
						true,
						ServerNetworkManager.Instance.Inventory.IsEquipedClass(tmpClassModel.Id),
						false,
						false
					);
				}
			}
			else
			{
				tmp.UpdateEntity(
					tmp.Id,
					1,
					0,
					false,
					false,
					false,
					false
				);
			}
		}
	}

	public void RequestClassEquip(string classId)
    {
		OutgameController.Instance.EquipClass(classId, () => {
			RefreshClassList();
			isChanged = true;
		});
    }

	public void RequestClassMerge(string classId)
    {
		OutgameController.Instance.MergeClass(classId, (isUpgradeSuccess) => {
			RefreshClassListWithTarget(classId, isUpgradeSuccess);
			isChanged = true;

			RefreshWeaponRedDot();
			RefreshClassRedDot();
		});
    }

	public void RequestClassUpgrade(string classId)
    {
		OutgameController.Instance.UpgradeClass(classId, (isUpgradeSuccess) => {
			RefreshClassListWithTarget(classId, isUpgradeSuccess);
			isChanged = true;

			RefreshWeaponRedDot();
			RefreshClassRedDot();
		});
    }
#endregion

#region Related With Mercenary
	
	private void RefreshMercenaryRedDot()
	{
		bool isRedDot = false;

		// Check mercenary
        foreach(var tmp in ServerNetworkManager.Instance.Inventory.MercenaryList)
        {
            if(tmp.IsNew)
				isRedDot = true;

			tmp.IsNew = false;
        }

		if(isRedDot)
            mercenaryRedDot.gameObject.SetActive(true);
        else
            mercenaryRedDot.gameObject.SetActive(false);
	}

	public void RefreshMercenaryList()
	{
		foreach(var tmp in mercenaryListParentTransform.GetComponentsInChildren<ListItemMercenary>())
		{
			var tmpMercenaryModel = ServerNetworkManager.Instance.Inventory.MercenaryList.FirstOrDefault(n => n.Id == tmp.Id);
			
			if(tmpMercenaryModel != null)
			{
				tmp.UpdateEntity(
					tmp.Id, 
					tmpMercenaryModel.Level,
					tmpMercenaryModel.Count,
					true,
					ServerNetworkManager.Instance.Inventory.IsEquipedMercenary(tmpMercenaryModel.Id),
					false,
					false
				);
			}
			else
			{
				tmp.UpdateEntity(
					tmp.Id,
					1,
					0,
					false,
					false,
					false,
					false
				);
			}
		}
	}

	private void RefreshMercenaryListWithTarget(string mercenaryId, bool isUpgradeSuccess)
	{
		foreach(var tmp in mercenaryListParentTransform.GetComponentsInChildren<ListItemMercenary>())
		{
			var tmpMercenaryModel = ServerNetworkManager.Instance.Inventory.MercenaryList.FirstOrDefault(n => n.Id == tmp.Id);

			if(tmpMercenaryModel != null)
			{
				// Check target mercenary
				if(tmpMercenaryModel.Id == mercenaryId)
				{
					tmp.UpdateEntity(
						tmpMercenaryModel.Id, 
						tmpMercenaryModel.Level,
						tmpMercenaryModel.Count,
						true,
						ServerNetworkManager.Instance.Inventory.IsEquipedMercenary(tmpMercenaryModel.Id),
						true,
						isUpgradeSuccess
					);
				}
				else
				{
					tmp.UpdateEntity(
						tmpMercenaryModel.Id, 
						tmpMercenaryModel.Level,
						tmpMercenaryModel.Count,
						true,
						ServerNetworkManager.Instance.Inventory.IsEquipedMercenary(tmpMercenaryModel.Id),
						false,
						false
					);
				}
			}
			else
			{
				tmp.UpdateEntity(
					tmp.Id,
					1,
					0,
					false,
					false,
					false,
					false
				);
			}
		}
	}

	public void RequestMercenaryEquip(string mercenaryId)
    {
		OutgameController.Instance.EquipMercenary(mercenaryId, () => {
			RefreshMercenaryList();
			isChanged = true;
		});
    }

	public void RequestMercenaryMerge(string mercenaryId)
    {
		OutgameController.Instance.MergeMercenary(mercenaryId, (isUpgradeSuccess) => {
			RefreshMercenaryListWithTarget(mercenaryId, isUpgradeSuccess);
			isChanged = true;

			RefreshMercenaryRedDot();
		});
    }

	public void RequestMercenarypUpgrade(string mercenaryId)
    {
		OutgameController.Instance.UpgradeMercenary(mercenaryId, (isUpgradeSuccess) => {
			RefreshMercenaryListWithTarget(mercenaryId, isUpgradeSuccess);
			isChanged = true;

			RefreshMercenaryRedDot();
		});
    }
#endregion

#region Related With Relic

	private void RefreshRelicRedDot()
	{
		bool isRedDot = false;

		// Check Relic
        foreach(var tmp in ServerNetworkManager.Instance.Inventory.RelicList)
        {
            // Check available upgrade
            if(!tmp.IsMaxLevel)
            {
                if((tmp.Count - 1) > 0)
                {
					isRedDot = true;
				}
            }

			if(tmp.IsNew)
				isRedDot = true;

			tmp.IsNew = false;
        }

		if(isRedDot)
            relicRedDot.gameObject.SetActive(true);
        else
            relicRedDot.gameObject.SetActive(false);
	}

	private void RefreshTargetRelic(string relicId, bool isShowUpgradeEffect, bool isUpgradeSuccess)
    {
		foreach(var tmp in relicListParentTransform.GetComponentsInChildren<ListItemRelic>())
		{
			if(tmp.Id == relicId)
			{
				var tmpRelicModel = ServerNetworkManager.Instance.Inventory.RelicList.FirstOrDefault(n => n.Id == tmp.Id);
				var tmpExtendedRelicModel = ServerNetworkManager.Instance.Inventory.ExtendedRelicList.FirstOrDefault(n => n.Id == tmp.Id);
				
				if(tmpExtendedRelicModel != null && tmpRelicModel != null) // Originally the player possessed the relic and also extended with bonus stat
				{
					tmp.UpdateEntity(
						tmp.Id,
						tmpRelicModel.Level,
						tmpExtendedRelicModel.Level,
						tmpExtendedRelicModel.Count,
						true,
						isShowUpgradeEffect,
						isUpgradeSuccess
					);
				}
				else if(tmpExtendedRelicModel != null && tmpRelicModel == null) // Only extended relic list has it
				{
					tmp.UpdateEntity(
						tmp.Id,
						0,
						tmpExtendedRelicModel.Level,
						tmpExtendedRelicModel.Count,
						true,
						isShowUpgradeEffect,
						isUpgradeSuccess
					);
				}
				else // No relic possessed
				{
					tmp.UpdateEntity(
						tmp.Id,
						0,
						0,
						0,
						false,
						false,
						false
					);
				}
			}
		}
    }

	public void RequestRelicUpgrade(string relicId)
    {
		OutgameController.Instance.UpgradeRelic(relicId, (isUpgradeSuccess) => {
			RefreshTargetRelic(relicId, true, isUpgradeSuccess);
			isChanged = true;

			RefreshRelicRedDot();
		});
    }

	public void RequestRelicRefund(string relicId)
	{
		OutgameController.Instance.RefundRelic(relicId, () => {
			RefreshTargetRelic(relicId, false, false);
			isChanged = true;

			RefreshRelicRedDot();
		});
	}

#endregion

#region Related With Legendary Relic

	private void RefreshLegendaryRelicRedDot()
	{
		bool isRedDot = false;

		// Check Legendary Relic
        foreach(var tmp in ServerNetworkManager.Instance.Inventory.LegendaryRelicList)
        {
            // Check available upgrade
            if(!tmp.IsMaxLevel)
            {
                if((tmp.Count - 1) > 0)
                {
					isRedDot = true;
				}
            }

			if(tmp.IsNew)
				isRedDot = true;

			tmp.IsNew = false;
        }

		if(isRedDot)
            legendaryRelicRedDot.gameObject.SetActive(true);
        else
            legendaryRelicRedDot.gameObject.SetActive(false);
	}

	private void RefreshTargetLegendaryRelic(string relicId, bool isShowUpgradeEffect, bool isUpgradeSuccess)
    {
		foreach(var tmp in legendaryRelicListParentTransform.GetComponentsInChildren<ListItemLegendaryRelic>())
		{
			if(tmp.Id == relicId)
			{
				var tmplegendaryRelicModel = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.Id == tmp.Id);
				
				if(tmplegendaryRelicModel != null)
				{
					tmp.UpdateEntity(
						tmp.Id,
						tmplegendaryRelicModel.Level,
						tmplegendaryRelicModel.Count,
						true,
						isShowUpgradeEffect,
						isUpgradeSuccess
					);
				}
				else // No legendary relic possessed
				{
					tmp.UpdateEntity(
						tmp.Id,
						0,
						0,
						false,
						false,
						false
					);
				}
			}
		}
    }

	public void RequestLegendaryRelicUpgrade(string relicId)
    {
		OutgameController.Instance.UpgradeLegendaryRelic(relicId, () => {
			RefreshTargetLegendaryRelic(relicId, true, true);
			isChanged = true;

			RefreshLegendaryRelicRedDot();
		});
    }

	public void RequestLegendaryRelicRefund(string relicId)
	{
		OutgameController.Instance.RefundLegendaryRelic(relicId, () => {
			RefreshTargetLegendaryRelic(relicId, false, false);
			isChanged = true;

			RefreshLegendaryRelicRedDot();
		});
	}

#endregion
}
