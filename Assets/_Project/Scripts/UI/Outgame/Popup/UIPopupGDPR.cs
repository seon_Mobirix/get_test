﻿using UnityEngine;
using UnityEngine.UI;

public class UIPopupGDPR : UIPopup {

	[SerializeField]
	private Button okButton;

	private bool isAgree = false;

	public override void Open()
	{
		base.Open();

		okButton.interactable = false;
	}

	public void OnCheckBoxClicked()
	{
		if(isAgree)
		{
			isAgree = false;
			okButton.interactable = false;
		}
		else
		{
			isAgree = true;
			okButton.interactable = true;
		}
	}

	public void OnLinkButtonClicked()
	{
		PageOpenController.Instance.OpenPrivacy();
	}

	public void OnOkButtonClicked()
	{
		OutgameController.Instance.SetGDPRAgree();
		base.Close();
	}
}
