﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Sirenix.OdinInspector;

public class UIPopupInbox2 : UIPopup
{
	[SerializeField]
	private UITab uiTab;

	[Title("Post")]
    [SerializeField]
	private RectTransform listParentTransform;
	[SerializeField]
	private Transform listItemPostPrefab;
	[SerializeField]
	private ScrollRect scrollRect;
	[SerializeField]
	private Transform noItemText;

	[Title("System Post")]
	[SerializeField]
	private RectTransform systemPostListParentTransform;
	[SerializeField]
	private Transform listItemSystemPostPrefab;
	[SerializeField]
	private ScrollRect systemPostScrollRect;
	[SerializeField]
	private Transform systemPostNoItemText;

	private int currentTabId;

	private double lastClickTimeStampMilliseconds = 0d;

	public override void Open()
	{
		Debug.LogWarning("UIPopupInbox2 shouldn't be open without parameters");
	}

	public void OpenWithValues(int tabId)
	{
		base.Open();

		currentTabId = tabId;
		uiTab.OnTabButton(tabId);

		Refresh();

        FocusItems();
	}

	public override void Close()
	{
		base.Close();
		scrollRect.verticalNormalizedPosition = 0f;
		systemPostScrollRect.verticalNormalizedPosition = 0f;
        
		if(GameObject.FindObjectOfType<UIViewMain>() != null)
        {
            GameObject.FindObjectOfType<UIViewMain>().Refresh();
            GameObject.FindObjectOfType<UIViewMain>().ShowRightTopContent();
        }
	}

	public void CloseWithOutShowRightTopContent()
    {
       	base.Close();
		scrollRect.verticalNormalizedPosition = 0f;
		systemPostScrollRect.verticalNormalizedPosition = 0f;

        if(GameObject.FindObjectOfType<UIViewMain>() != null)
            GameObject.FindObjectOfType<UIViewMain>().Refresh();
    }

	public void OnTabButtonClicked(int tabId)
	{
		currentTabId = tabId;
        uiTab.OnTabButton(tabId);

        Refresh();

        FocusItems();
	}

	public override void Refresh()
	{
		if(currentTabId == 0)
		{
			foreach(var tmp in listParentTransform.GetComponentsInChildren<ListItemPost>())
			{
				Destroy(tmp.gameObject);
			}
			
			if(ServerNetworkManager.Instance.Post.MailList.Count == 0)
				noItemText.gameObject.SetActive(true);
			else
			{	
				int activeMailCount = 0;
			
				foreach(var tmp in ServerNetworkManager.Instance.Post.MailList)
				{
					var timeLeft = (int)(tmp.ExpiredTime - DateTimeOffset.Now.ToUnixTimeSeconds());

					if(timeLeft > 0)
					{
						var transformToUse = Instantiate(listItemPostPrefab);
						transformToUse.GetComponent<ListItemPost>().UpdateEntity(tmp.Id, tmp.Message, tmp.ExpiredTime, tmp.AttachedItem);
						transformToUse.SetParent(listParentTransform, false);
						activeMailCount ++;
					}
				}

				if(activeMailCount > 0)
					noItemText.gameObject.SetActive(false);
				else
					noItemText.gameObject.SetActive(true);
			}	
		}
		else if(currentTabId == 1)
		{
			foreach(var tmp in systemPostListParentTransform.GetComponentsInChildren<ListItemSystemPost>())
			{
				Destroy(tmp.gameObject);
			}

			if(ServerNetworkManager.Instance.SystemPost.MailList.Count == 0)
				systemPostNoItemText.gameObject.SetActive(true);
			else
			{
				systemPostNoItemText.gameObject.SetActive(false);
				
				int activeMailCount = 0;

				foreach(var tmp in ServerNetworkManager.Instance.SystemPost.MailList)
				{
					var timeLeft = (int)(tmp.ExpiredTime - DateTimeOffset.Now.ToUnixTimeSeconds());

					if(timeLeft > 0)
					{
						var transformToUse = Instantiate(listItemSystemPostPrefab);
						transformToUse.GetComponent<ListItemSystemPost>().UpdateEntity(tmp.Id, tmp.Message, tmp.ExpiredTime, tmp.AttachedItem);
						transformToUse.SetParent(systemPostListParentTransform, false);
						activeMailCount++;
					}
				}

				if(activeMailCount > 0)
					noItemText.gameObject.SetActive(false);
				else
					noItemText.gameObject.SetActive(true);
			}
		}
	}

	private void FocusItems()
    {
        StartCoroutine(FocusItemsCoroutine());
    }

    private IEnumerator FocusItemsCoroutine()
    {
        if(currentTabId == 0)
        {
            listParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Unrestricted;
            yield return new WaitForEndOfFrame();
            listParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Elastic;

            Canvas.ForceUpdateCanvases();

            listParentTransform.anchoredPosition = new Vector2(listParentTransform.anchoredPosition.x, 0f);
        }
        else if(currentTabId == 1)
        {
            systemPostListParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Unrestricted;
            yield return new WaitForEndOfFrame();
            systemPostListParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Elastic;

            Canvas.ForceUpdateCanvases();

            systemPostListParentTransform.anchoredPosition = new Vector2(systemPostListParentTransform.anchoredPosition.x, 0f);
        }
    }

	public void OnReceiveAllButtonClick()
	{
		var passedMilliSeconds = DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastClickTimeStampMilliseconds;

		if(passedMilliSeconds >= 500d)
		{
			lastClickTimeStampMilliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();

			if(currentTabId == 0)
				OutgameController.Instance.RequestAttachedItemAll();
			else if(currentTabId == 1)
				OutgameController.Instance.RequestAttachedItemSystemMailItemAll();
		}
	}
}
