﻿using UnityEngine;
using UnityEngine.UI;

public class UIPopupGrowthSupportPurchase : UIPopup
{
    [SerializeField]
	private Text packageInfoText;
	[SerializeField]
	private Text purchaseButtonText;

    private string packageId;

    public override void Open()
	{
		Debug.LogWarning("UIPopupGrowthSupportPurchase shouldn't be open without parameters");
	}

	public void OpenWithValues(string packageId, int vipReward, int gemReward, string price)
	{
		base.Open();
		
        this.packageId = packageId;

        string text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "growth_support_info");

		packageInfoText.text = string.Format(text, vipReward, gemReward, ServerNetworkManager.Instance.Setting.MaxGrowthSupportStage);
        purchaseButtonText.text = price;
	}

	public void OnPurchaseButton()
	{
		OutgameController.Instance.PurchaseGrowthPackage(packageId);
	}
}
