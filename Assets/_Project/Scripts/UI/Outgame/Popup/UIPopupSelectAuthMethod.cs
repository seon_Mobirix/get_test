﻿using UnityEngine;
using UnityEngine.UI;

public class UIPopupSelectAuthMethod : UIPopup {

    [SerializeField]
    private bool isForRelogin = false;

	[SerializeField]
	private RectTransform socialLoginButtoniOS;

    [SerializeField]
	private RectTransform socialLoginButtonAndroid;

    [SerializeField]
    private RectTransform guestButton;

    public override void Open()
    {
        Debug.LogWarning("UIPopupSelectAuthMethod shouldn't be open without parameters");
	}

    public void OpenWithValues(bool isInitial)
    {
        base.Open();

        if(isInitial)
        {
#if UNITY_IPHONE
		    socialLoginButtoniOS.gameObject.SetActive(true);
            socialLoginButtonAndroid.gameObject.SetActive(false);
#elif UNITY_ANDROID
            socialLoginButtoniOS.gameObject.SetActive(false);
            socialLoginButtonAndroid.gameObject.SetActive(true);
#endif

#if UNITY_EDITOR
            guestButton.gameObject.SetActive(true);
#else
            guestButton.gameObject.SetActive(false);
#endif
        }
        else
        {
#if UNITY_IPHONE
		    socialLoginButtoniOS.gameObject.SetActive(true);
            socialLoginButtonAndroid.gameObject.SetActive(false);
#elif UNITY_ANDROID
            socialLoginButtoniOS.gameObject.SetActive(false);
            socialLoginButtonAndroid.gameObject.SetActive(true);
#endif

#if UNITY_EDITOR
            guestButton.gameObject.SetActive(true);
#else
            guestButton.gameObject.SetActive(false);
#endif

            if(OptionManager.Instance.LoginMode != OptionManager.LoginModeType.PLATFORM_DEFAULT)
            {
                socialLoginButtoniOS.GetComponent<Button>().interactable = true;
                socialLoginButtonAndroid.GetComponent<Button>().interactable = true;
                guestButton.gameObject.GetComponent<Button>().interactable = false;
            }
            else
            {
                socialLoginButtoniOS.GetComponent<Button>().interactable = false;
                socialLoginButtonAndroid.GetComponent<Button>().interactable = false;
                guestButton.gameObject.GetComponent<Button>().interactable = true;
            }
        }
    }

    public void OnPlatformDefaultSocialButton()
    {
        OptionManager.Instance.LoginMode = OptionManager.LoginModeType.PLATFORM_DEFAULT;
        ServerNetworkManager.Instance.Logout();
        ChatManager.Instance.Disconnect();
        SceneChangeManager.Instance.LoadMainGame(true, isForRelogin);
    }

    public void OnGuestButton()
    {
        // For guest mode, logout
        if(Social.localUser.authenticated)
            SocialPlatformManager.Instance.LogoutSocialService();

        OptionManager.Instance.LoginMode = OptionManager.LoginModeType.GUEST;
        ServerNetworkManager.Instance.Logout();
        ChatManager.Instance.Disconnect();
        SceneChangeManager.Instance.LoadMainGame(true, isForRelogin);
    }
}
