﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;
using System.Collections;
using Sirenix.OdinInspector;

public class UIPopupTower : UIPopup
{
	[Title("Tower Title")]
	[SerializeField]
	private Text towerTitle = null;

#region Deploy Info
	[Title("Deploy Info")]

    [SerializeField]
    private Transform nonDeployedCover = null;
	[SerializeField]
	private Button deployButton = null;
	[SerializeField]
    private Button deployCoolDownButton = null;
	[SerializeField]
    private Text deployCoolDownButtonText = null;
#endregion

#region In Progress Info

    [Title("In Progress Info")]

	[SerializeField]
    private Transform inProgressInfo = null;
	[SerializeField]
    private Text floorText = null;
    [SerializeField]
    private Text timeText = null;
    [SerializeField]
    private Text inProgressRewardInfoText = null;
    [SerializeField]
    private Text rewardRateText = null;
	[SerializeField]
    private Text inProgressRewardFeatherInfoText = null;
    [SerializeField]
    private Text rewardRateFeatherText = null;
    [SerializeField]
    private Button challengeButton = null;
    [SerializeField]
    private Text floorChallengeText = null;
	[SerializeField]
    private Button challengeCoolDownButton = null;
	[SerializeField]
    private Text challengeCoolDownButtonText = null;
    
	[Title("Complete Info")]

    [SerializeField]
    private Transform completeInfo = null;

    [SerializeField]
    private Text completeRewardInfoText = null;

#endregion

#region Tower Info

	[Title("Tower Info")]

	[SerializeField]
	private RectTransform towerFloorParent;

	[SerializeField]
	private Transform listItemTowerFloorPrefab = null;

	[SerializeField]
    private float maxItemOffset;

#endregion

#region Battle Log Info

    [Title("Battle Log Info")]
    [SerializeField]
	private RectTransform listItemTowerBattleDataParentTransform;
	[SerializeField]
	private Transform listItemTowerBattleDataPrefab;
	[SerializeField]
	private Transform listItemTowerBattleDataSimplePrefab;

#endregion

	private int floorIndex = 0;
	private int maxFloorIndex = 11;	
	private bool needUpdateDeployCoolDown = false;
	private bool needUpdateBattleCoolDown = false;
	private float towerDeployCooldownLeft = 0f;
	private float towerBattleCooldownLeft = 0f;
	private double lastClickTimeStampMilliseconds = 0d;

	public override void Open()
	{
		base.Open();
		Refresh();
	}

	public override void Close()
	{
        base.Close();

		// Save last check tower time stamp min
		var currentTimeMinutes = DateTimeOffset.Now.ToUnixTimeSeconds() / 60;
		OutgameController.Instance.LastCloseTowerPopupTimeStampMinute = currentTimeMinutes;
	
        if(GameObject.FindObjectOfType<UIViewMain>() != null)
		{
			GameObject.FindObjectOfType<UIViewMain>().RefreshMenuGroup();
			GameObject.FindObjectOfType<UIViewMain>().SetFeatherAmountInfoActive(false);
			GameObject.FindObjectOfType<UIViewMain>().ShowRightTopContent();
		}
    }

	public void CloseWithOutShowRightTopContent()
    {
       	base.Close();

		// Save last check tower time stamp min
		var currentTimeMinutes = DateTimeOffset.Now.ToUnixTimeSeconds() / 60;
		OutgameController.Instance.LastCloseTowerPopupTimeStampMinute = currentTimeMinutes;

        if(GameObject.FindObjectOfType<UIViewMain>() != null)
            GameObject.FindObjectOfType<UIViewMain>().Refresh();
    }
	
	private void Update()
	{
		if(needUpdateDeployCoolDown)
			UpdateDeployCoolDown();
		if(needUpdateBattleCoolDown)
			UpdateBattleCoolDown();
	}

	private float deployCoolDownPassed = 0f;
	private float battleCoolDownPassed = 0f;

	private void UpdateDeployCoolDown()
	{
		deployCoolDownPassed += Time.deltaTime;

		if(deployCoolDownPassed >= 1f)
		{
			towerDeployCooldownLeft -= deployCoolDownPassed;

			if(towerDeployCooldownLeft > 0)
			{
				deployCoolDownButtonText.text = 
					string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "tower_cooldown"), Mathf.FloorToInt(towerDeployCooldownLeft))
					+ "\n" + string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_left_count"), ServerNetworkManager.Instance.TowerDailyLimitLeft);
			
				deployButton.gameObject.SetActive(false);
				deployCoolDownButton.gameObject.SetActive(true);

			}
			else
			{
				needUpdateDeployCoolDown = false;

				deployButton.gameObject.SetActive(true);
				deployCoolDownButton.gameObject.SetActive(false);
			}

			deployCoolDownPassed = 0f;
		}
	}

	private void UpdateBattleCoolDown()
	{
		battleCoolDownPassed += Time.deltaTime;

		if(battleCoolDownPassed >= 1f)
		{
			towerBattleCooldownLeft -= battleCoolDownPassed;

			if(towerBattleCooldownLeft > 0)
			{
				challengeCoolDownButtonText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "tower_cooldown"), Mathf.FloorToInt(towerBattleCooldownLeft));

				challengeButton.gameObject.SetActive(false);
				challengeCoolDownButton.gameObject.SetActive(true);
			}
			else
			{
				needUpdateBattleCoolDown = false;

				challengeButton.gameObject.SetActive(true);
				challengeCoolDownButton.gameObject.SetActive(false);
			}


			battleCoolDownPassed = 0;
		}
	}

	public override void Refresh()
	{
		if(ServerNetworkManager.Instance.Setting.IsInBeta)
			towerTitle.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_tower") + " (beta)";
		else
			towerTitle.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_tower");

		needUpdateDeployCoolDown = false;
		needUpdateBattleCoolDown = false;
		
		// Refresh progress info
        var modelToUse = ServerNetworkManager.Instance.MyTowerData;

        if(!modelToUse.IsDeployed)
        {
			floorText.text = "-";

            nonDeployedCover.gameObject.SetActive(true);           
            inProgressInfo.gameObject.SetActive(false);
            completeInfo.gameObject.SetActive(false);

			if(ServerNetworkManager.Instance.TowerDailyLimitLeft > 0)
			{
				if(ServerNetworkManager.Instance.TowerCooldownDeployLeft > 0)
				{
					towerDeployCooldownLeft = ServerNetworkManager.Instance.TowerCooldownDeployLeft;
					needUpdateDeployCoolDown = true;

					deployButton.gameObject.SetActive(false);
					deployCoolDownButton.gameObject.SetActive(true);
					deployCoolDownButtonText.text = 
						string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "tower_cooldown"), Mathf.FloorToInt(towerDeployCooldownLeft))
						+ "\n" + string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_left_count"), ServerNetworkManager.Instance.TowerDailyLimitLeft);
				}
				else
				{
					deployButton.gameObject.SetActive(true);
					deployCoolDownButton.gameObject.SetActive(false);
				}
			}
			else
			{
				deployButton.gameObject.SetActive(false);
					deployCoolDownButton.gameObject.SetActive(true);
					deployCoolDownButtonText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_left_count"), ServerNetworkManager.Instance.TowerDailyLimitLeft);
			}
        }
        else
        {
            floorText.text = modelToUse.Floor + "F";
            
            if(!modelToUse.IsCompleted)
            {
				nonDeployedCover.gameObject.SetActive(false);           
				inProgressInfo.gameObject.SetActive(true);
				completeInfo.gameObject.SetActive(false);

				if(modelToUse.SecondLeft > 60)
            	    timeText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "time_mins"), Math.Min(Math.Floor(modelToUse.SecondLeft / 60) + 1, 240));
                else
					timeText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "time_end_soon");

                inProgressRewardInfoText.text = Mathf.FloorToInt(ServerNetworkManager.Instance.MyTowerData.CollectedGems).ToString();
                rewardRateText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "tower_hourly_gems"), modelToUse.RewardRate);

				inProgressRewardFeatherInfoText.text = Mathf.FloorToInt(ServerNetworkManager.Instance.MyTowerData.CollectedFeathers).ToString();
                rewardRateFeatherText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "tower_hourly_feathers"), modelToUse.RewardFeatherRate);

                if(modelToUse.Floor < 11)
                {
                    floorChallengeText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_challenge_tower"), modelToUse.Floor + 1);
                    
					if(ServerNetworkManager.Instance.TowerCooldownLeft > 0)
					{
						towerBattleCooldownLeft = ServerNetworkManager.Instance.TowerCooldownLeft;
						needUpdateBattleCoolDown = true;

						challengeButton.gameObject.SetActive(false);
						challengeCoolDownButton.gameObject.SetActive(true);
						challengeCoolDownButtonText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "tower_cooldown"), Mathf.FloorToInt(towerBattleCooldownLeft));
					}
					else
					{
						challengeButton.gameObject.SetActive(true);
						challengeCoolDownButton.gameObject.SetActive(false);
					}
                }
                else
                {
					challengeButton.gameObject.SetActive(false);
					challengeCoolDownButton.gameObject.SetActive(false);
                }
            }
            else
            {
                nonDeployedCover.gameObject.SetActive(false);           
				inProgressInfo.gameObject.SetActive(false);
				completeInfo.gameObject.SetActive(true);

                completeRewardInfoText.text = 
					string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_tower_get_reward"), Mathf.FloorToInt(ServerNetworkManager.Instance.MyTowerData.CollectedGems))
					+ "\n" + string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_tower_get_reward_2"), Mathf.FloorToInt(ServerNetworkManager.Instance.MyTowerData.CollectedFeathers));
				timeText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "tower_complete");
            }
        }

		RefreshBattelLog();
	}

	public void RefreshFloors()
	{
		if(ServerNetworkManager.Instance.MyTowerData != null)
			this.floorIndex = Mathf.Max(ServerNetworkManager.Instance.MyTowerData.Floor, 1);

		foreach(var tmp in towerFloorParent.GetComponentsInChildren<ListItemTowerFloor>())
		{
			Destroy(tmp.gameObject);
		}

        foreach(var tmp in ServerNetworkManager.Instance.TowerFloorPlayers.OrderByDescending(n => n.Floor))
        {
            var tempListItemTowerFloor = Instantiate(listItemTowerFloorPrefab);
			var myTowerInfo = ServerNetworkManager.Instance.MyTowerData;

			// Check my floor status
			if(myTowerInfo != null && myTowerInfo.Floor == tmp.Floor)
				tempListItemTowerFloor.GetComponent<ListItemTowerFloor>().UpdateEntity(tmp.Floor, tmp.CurrentPlayers.Count(), tmp.MaxAllowedPlayersNumber, myTowerInfo.IsDeployed, myTowerInfo.IsCompleted);
			else
				tempListItemTowerFloor.GetComponent<ListItemTowerFloor>().UpdateEntity(tmp.Floor, tmp.CurrentPlayers.Count(), tmp.MaxAllowedPlayersNumber, tmp.IsOccupy, tmp.IsComplete);

			tempListItemTowerFloor.SetParent(towerFloorParent, false);
        }

		Canvas.ForceUpdateCanvases();

        var rectTransform = listItemTowerFloorPrefab.GetComponent<RectTransform>();
        var yPosition = 
            rectTransform.rect.size.y * (Mathf.Min(floorIndex, maxFloorIndex - 3) - 1) 
            + towerFloorParent.GetComponent<VerticalLayoutGroup>().spacing * (Mathf.Min(floorIndex, maxFloorIndex - 3) - 1);

        if(floorIndex == maxFloorIndex)
            yPosition += maxItemOffset;
        
        towerFloorParent.anchoredPosition = new Vector2(towerFloorParent.anchoredPosition.x, yPosition * -1f);
	}

	public void RefreshBattelLog()
    {
        foreach(var tmp in listItemTowerBattleDataParentTransform.GetComponentsInChildren<ListItemTowerBattleData>())
		{
			Destroy(tmp.gameObject);
		}

		var towerBattleLogList = ServerNetworkManager.Instance.MyTowerData.BattleLog;

		var smallCount = 0;
		var largeCount = 0;

        foreach(var tmp in towerBattleLogList)
        {
            if(tmp.Name != "")
			{
				var transformToUse = Instantiate(listItemTowerBattleDataPrefab);
				transformToUse.GetComponent<ListItemTowerBattleData>().UpdateEntity(tmp.TimeStamp, tmp.Name, tmp.Floor, tmp.Win, tmp.IsDefense);
				transformToUse.SetParent(listItemTowerBattleDataParentTransform, false);
				largeCount ++;
			}
			else
			{
				var transformToUse = Instantiate(listItemTowerBattleDataSimplePrefab);
				transformToUse.GetComponent<ListItemTowerBattleData>().UpdateEntity(tmp.TimeStamp, tmp.Name, tmp.Floor, tmp.Win, tmp.IsDefense);
				transformToUse.SetParent(listItemTowerBattleDataParentTransform, false);
				smallCount ++;
			}
        } 

		// Focus item
		Canvas.ForceUpdateCanvases();

		var adjustValue = smallCount * (listItemTowerBattleDataParentTransform.GetComponent<VerticalLayoutGroup>().spacing + listItemTowerBattleDataSimplePrefab.gameObject.GetComponent<RectTransform>().rect.height)
			+ largeCount * (listItemTowerBattleDataParentTransform.GetComponent<VerticalLayoutGroup>().spacing + listItemTowerBattleDataPrefab.gameObject.GetComponent<RectTransform>().rect.height)
			+ listItemTowerBattleDataParentTransform.GetComponent<VerticalLayoutGroup>().padding.top
			+ listItemTowerBattleDataParentTransform.GetComponent<VerticalLayoutGroup>().padding.bottom
			- listItemTowerBattleDataParentTransform.GetComponent<VerticalLayoutGroup>().spacing
			- listItemTowerBattleDataParentTransform.parent.GetComponent<RectTransform>().sizeDelta.y;

		if(adjustValue <= 0f)
			adjustValue = 0f;
            
		listItemTowerBattleDataParentTransform.anchoredPosition = new Vector2(0f, adjustValue);
    }

#region Button Event
	public void OnInfoButtonClicked()
	{
		OutgameController.Instance.ShowGenericInfo(
			LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_tower")
		);
	}

	public void OnPlayerListButtonClicked(int floor)
	{
		OutgameController.Instance.ShowTowerPlayerPopup(floor);
	}

	public void OnDeployButtonClicked()
    {
		var passedMilliSeconds = DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastClickTimeStampMilliseconds;

		if(passedMilliSeconds >= 500d)
		{
			lastClickTimeStampMilliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();

			var timeLeft = Mathf.Max(2f + OutgameController.Instance.TimeTowerFunction - (int)Time.realtimeSinceStartup, 0);

			if(timeLeft > 0)
			{
				OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
			}
			else 
			{
				OutgameController.Instance.DeployTower();
			}
		}
    }

    public void OnAbortButtonClicked()
    {
        var timeLeft = Mathf.Max(2f + OutgameController.Instance.TimeTowerFunction - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
        else
        {
            UIManager.Instance.ShowAlert(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_tower_abort_2"), () => {
                OutgameController.Instance.RetreatTower();
            }, () => {
                // Nothing to do.
            });
        }
    }

    public void OnReceiveRewardButtonClicked()
    {
		var timeLeft = Mathf.Max(2f + OutgameController.Instance.TimeTowerFunction - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
        {
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
        }
        else 
        {
        	OutgameController.Instance.RetreatTower();
        }
    }

    public void OnChallengeButtonClicked()
    {
        var timeLeft = Mathf.Max(2f + OutgameController.Instance.TimeTowerFunction - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
        else 
       		OutgameController.Instance.GetTowerOpponentAndTryBattle();
    }

	public void OnRefreshButtonClicked()
	{
		var timeLeft = Mathf.Max(2f + OutgameController.Instance.TimeTowerFunction - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
        else 
       		OutgameController.Instance.RefreshTower();
	}
#endregion

}
