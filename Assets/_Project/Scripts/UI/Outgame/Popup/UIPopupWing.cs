﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;

using PlayFab;

public class UIPopupWing : UIPopup
{
    [Title("Common")]
    [SerializeField]
	private ScrollRect wingRect;  
    [SerializeField]
	private RectTransform wingListParentTransform;
    [SerializeField]
    private Transform listItemWingPrefab;

    [Title("Lock Content")]
    [SerializeField]
    private Transform lockPanel;
    [SerializeField]
    private Text wingNameText;
    [SerializeField]
    private Image wingImage;
    [SerializeField]
    private List<Sprite> currencyIconList;
    [SerializeField]
    private Image currencyIcon;
    [SerializeField]
    private Text priceText;
    [SerializeField]
    private Button purchaseButton;

    [Title("UnLock Content")]
    [SerializeField]
    private Text wingTitleText;
    [SerializeField]
    private Image wingTitleImage;

    [SerializeField]
    private Transform unlockPanel;
    [SerializeField]
    private Text totalOwnedStatBonus1Text;
    [SerializeField]
    private Text totalOwnedStatBonus2Text;
    [SerializeField]
    private List<ListItemWingStat> wingStatInfoList;  
    [SerializeField]
    private Text resetPriceText;
    [SerializeField]
    private Button resetButton;
    [SerializeField]
    private Transform equipButton;
    [SerializeField]
    private Transform equipedButton;

    [Title("Wing Body Upgrade")]
    [SerializeField]
    private Transform wingBodyLockContent;
    [SerializeField]
    private Text wingBodyUnlockInfoText;
    [SerializeField]
    private Button wingBodyUnlockButton;
    [SerializeField]
    private Text wingBodyUnlockPriceText;
    [SerializeField]
    private Transform wingBodyContent;
    [SerializeField]
    private Text wingBodyLevelText;
    [SerializeField]
    private Text wingBodyEquipmentStatBonusText;
    [SerializeField]
    private Text wingBodyOwnedStatBonusText;
    [SerializeField]
    private Text upgradePercentageText;
    [SerializeField]
    private Transform upgradePercentageInfo;
    [SerializeField]
    private Text upgradePriceText;
    [SerializeField]
    private Button upgradeButton;

    private string selectedWingId;
    private bool isLock;
    private List<bool> statLockList = new List<bool>(){false, false, false, false, false};
    private Dictionary<string, List<bool>> statLockLists = new Dictionary<string, List<bool>>();
    private bool isChanged = false;

    private double lastResetClickTimeStampMilliseconds;

    public override void Open()
	{
		Debug.LogWarning("UIPopupWing shouldn't be open without parameters");
	}

	public void OpenWithValues(string wingId, bool isLock)
	{
        base.Open();

        this.selectedWingId = wingId;
        this.isLock = isLock;

        CreateOrLoadLockList(wingId);
        
        isChanged = false;

		Refresh();
    }

    public override void Close()
    {
        base.Close();

        // Need to sync data so far
        if(isChanged)
            OutgameController.Instance.SyncPlayerData(true, "UIPopupWing");

        isChanged = false;
        
        if(GameObject.FindObjectOfType<UIViewMain>() != null)
        {
            GameObject.FindObjectOfType<UIViewMain>().Refresh();
            GameObject.FindObjectOfType<UIViewMain>().SetFeatherAmountInfoActive(false);
            GameObject.FindObjectOfType<UIViewMain>().ShowRightTopContent();
        }
    }

    public void CloseWithOutShowRightTopContent()
    {
       	base.Close();

        // Need to sync data so far
        if(isChanged)
            OutgameController.Instance.SyncPlayerData(false, "UIPopupWing");

        isChanged = false;
    }

    public override void Refresh()
    {
        RefreshWingInfo();
        RefreshWingList();
    }

    public void RefreshSelectedWing(string wingId, bool isLock)
    {
        this.selectedWingId = wingId;
        this.isLock = isLock;
        
        CreateOrLoadLockList(wingId);
        
        RefreshWingInfo();
    }

    public void RefreshWingBody()
    {
        lockPanel.gameObject.SetActive(false);
        unlockPanel.gameObject.SetActive(false);

        var wingBodyInfo = ServerNetworkManager.Instance.WingBodyInfo;

        // Check wing body info null or not
        if(wingBodyInfo == null)
            return;

        int totalOwnedStatBonus1Value = 0;
        int totalOwnedStatBonus2Value = 0;

        // Caculate total owned
        foreach(var tmp in ServerNetworkManager.Instance.PurchasedWingList)
        {
            totalOwnedStatBonus1Value = totalOwnedStatBonus1Value + tmp.OwnedStatBonus1Value;
            totalOwnedStatBonus2Value = totalOwnedStatBonus2Value + tmp.OwnedStatBonus2Value;
        }

        totalOwnedStatBonus1Text.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "wing_atk_bonus"), totalOwnedStatBonus1Value * wingBodyInfo.OwnedStatBonusValue);
        totalOwnedStatBonus2Text.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "wing_gold_bonus"), totalOwnedStatBonus2Value * wingBodyInfo.OwnedStatBonusValue);

        if(wingBodyInfo.IsOpen)
        {
            wingBodyLockContent.gameObject.SetActive(false);
            wingBodyContent.gameObject.SetActive(true);

            // Show wing body upgrade info
            if(wingBodyInfo.Level >= ServerNetworkManager.Instance.Setting.WingMaxLevel)
            {
                wingBodyLevelText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "wing_body_level"), ServerNetworkManager.Instance.Setting.WingMaxLevel);
                upgradePercentageInfo.gameObject.SetActive(false);
                upgradeButton.interactable = false;
            }  
            else
            {
                wingBodyLevelText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "wing_body_level"), wingBodyInfo.Level);
                upgradePercentageText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "upgrade_percentage_info"), wingBodyInfo.UpgradeSuccessPercentage);
                upgradePercentageInfo.gameObject.SetActive(true);
                
                if(ServerNetworkManager.Instance.Inventory.Feather < ServerNetworkManager.Instance.Setting.WingUpgradePrice)
                    upgradeButton.interactable = false;
                else
                    upgradeButton.interactable = true;
            }
        
            wingBodyEquipmentStatBonusText.text = $"{LanguageManager.Instance.GetTextData(LanguageDataType.UI, "wing_equipment_stat_bonus")} +{wingBodyInfo.EquipmentStatBonusValue}";
            wingBodyOwnedStatBonusText.text = $"{LanguageManager.Instance.GetTextData(LanguageDataType.UI, "total_wing_owned_stat_bonus")} x{wingBodyInfo.OwnedStatBonusValue}";

            upgradePriceText.text = ServerNetworkManager.Instance.Setting.WingUpgradePrice.ToString();
            upgradeButton.gameObject.SetActive(true);
        }
        else
        {
            wingBodyLockContent.gameObject.SetActive(true);
            wingBodyContent.gameObject.SetActive(false);

            wingBodyUnlockInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "desc_wing_body_unlock"), ServerNetworkManager.Instance.Setting.WingBodyOpenCount);
            wingBodyUnlockPriceText.text = ServerNetworkManager.Instance.Setting.WingUpgradePrice.ToString();

            if(ServerNetworkManager.Instance.PurchasedWingList.Count < ServerNetworkManager.Instance.Setting.WingBodyOpenCount)
                wingBodyUnlockButton.interactable = false;
            else
                wingBodyUnlockButton.interactable = true;
        }
    }

    public void RefreshResetPrice()
    {
        // Caculate reset price
        int index = 0;

        foreach(var tmp in statLockList)
        {
            if(tmp)
                index = index + 1;
        }

        index = Mathf.Min(index, 4);

        int resetPrice = ServerNetworkManager.Instance.Setting.WingResetStatPriceList[index];
        resetPriceText.text = resetPrice.ToString();

        if(ServerNetworkManager.Instance.TotalGem < resetPrice)
            resetButton.interactable = false;
        else
            resetButton.interactable = true;
    }

    private void RefreshWingInfo()
    {
        wingBodyLockContent.gameObject.SetActive(false);
        wingBodyContent.gameObject.SetActive(false);

        int totalOwnedStatBonus1Value = 0;
        int totalOwnedStatBonus2Value = 0;

        // Get wing body info
        var wingBodyInfo = ServerNetworkManager.Instance.WingBodyInfo;
        int wingBodyOwnedStatBonus = 1;
        int wingBodyEquipmentStatBonus = 0;

        if(wingBodyInfo != null)
        {
            wingBodyOwnedStatBonus = wingBodyInfo.OwnedStatBonusValue;
            wingBodyEquipmentStatBonus = wingBodyInfo.EquipmentStatBonusValue;
        }

        // Caculate total owned
        foreach(var tmp in ServerNetworkManager.Instance.PurchasedWingList)
        {
            totalOwnedStatBonus1Value = totalOwnedStatBonus1Value + tmp.OwnedStatBonus1Value;
            totalOwnedStatBonus2Value = totalOwnedStatBonus2Value + tmp.OwnedStatBonus2Value;
        }

        totalOwnedStatBonus1Text.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "wing_atk_bonus"), totalOwnedStatBonus1Value * wingBodyOwnedStatBonus);
        totalOwnedStatBonus2Text.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "wing_gold_bonus"), totalOwnedStatBonus2Value * wingBodyOwnedStatBonus);

        // Refresh depend on lock status
        if(isLock)
        {
            lockPanel.gameObject.SetActive(true);
            unlockPanel.gameObject.SetActive(false);

            wingNameText.text = LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "wing_name_" + int.Parse(selectedWingId.Substring(selectedWingId.Length - 2)).ToString("D2"));
            wingImage.sprite = Resources.Load<Sprite>("Thumbnails/Wing_" + int.Parse(selectedWingId.Substring(selectedWingId.Length - 2)).ToString("D2"));

            // Get wing product info
            var tmpWingProductModel = ServerNetworkManager.Instance.Catalog.WingProductList.FirstOrDefault(n => n.Id == selectedWingId);
            bool isAvailable = false;

            if(tmpWingProductModel != null)
            {
                if(tmpWingProductModel.PriceType == GoodsPriceType.GEM)
                {
                    currencyIcon.sprite = currencyIconList[0];
                    priceText.text = tmpWingProductModel.Price.ToString();

                    if(ServerNetworkManager.Instance.TotalGem >= tmpWingProductModel.Price)
                        isAvailable = true;
                }
                else if(tmpWingProductModel.PriceType == GoodsPriceType.FEATHER)
                {
                    currencyIcon.sprite = currencyIconList[1];
                    priceText.text = tmpWingProductModel.Price.ToString();

                    if(ServerNetworkManager.Instance.Inventory.Feather >= tmpWingProductModel.Price)
                        isAvailable = true;
                }
                else if(tmpWingProductModel.PriceType == GoodsPriceType.PACKAGE)
                {
                    currencyIcon.sprite = currencyIconList[2];
                    priceText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_tab_package");
                    
                    isAvailable = true;
                }

                purchaseButton.gameObject.SetActive(true);
            }
            else
                purchaseButton.gameObject.SetActive(false);

            purchaseButton.interactable = isAvailable;
        }
        else
        {
            lockPanel.gameObject.SetActive(false);
            unlockPanel.gameObject.SetActive(true);

            wingTitleText.text = LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "wing_name_" + int.Parse(selectedWingId.Substring(selectedWingId.Length - 2)).ToString("D2"));
            wingTitleImage.sprite = Resources.Load<Sprite>("Thumbnails/Wing_" + int.Parse(selectedWingId.Substring(selectedWingId.Length - 2)).ToString("D2"));

            // Get selected wing model
            var selectedWingModel = ServerNetworkManager.Instance.PurchasedWingList.FirstOrDefault(n => n.WingId == selectedWingId);

            if(selectedWingModel != null)
            {
                foreach(var tmp in wingStatInfoList)
                {
                    if(tmp.wingStatType == WingStatInfoType.ATTACK)
                    {
                        tmp.UpdateEntity(selectedWingModel.OwnedStatBonus1Rank, selectedWingModel.OwnedStatBonus1Value, statLockList[(int)tmp.wingStatType]);
                    }
                    else if(tmp.wingStatType == WingStatInfoType.GOLD)
                    {
                        tmp.UpdateEntity(selectedWingModel.OwnedStatBonus2Rank, selectedWingModel.OwnedStatBonus2Value, statLockList[(int)tmp.wingStatType]);
                    }
                    else if(tmp.wingStatType == WingStatInfoType.SKILL_1)
                    {
                        tmp.UpdateEntity(selectedWingModel.EquipmentStatBonus1Rank, selectedWingModel.EquipmentStatBonus1Value, statLockList[(int)tmp.wingStatType]);
                    }
                    else if(tmp.wingStatType == WingStatInfoType.SKILL_2)
                    {
                        tmp.UpdateEntity(selectedWingModel.EquipmentStatBonus2Rank, selectedWingModel.EquipmentStatBonus2Value, statLockList[(int)tmp.wingStatType]);
                    }
                    else if(tmp.wingStatType == WingStatInfoType.SKILL_3)
                    {
                        tmp.UpdateEntity(selectedWingModel.EquipmentStatBonus3Rank, selectedWingModel.EquipmentStatBonus3Value, statLockList[(int)tmp.wingStatType]);
                    }
                }

                equipButton.gameObject.SetActive(!selectedWingModel.IsEquipment);
                equipedButton.gameObject.SetActive(selectedWingModel.IsEquipment);

                RefreshResetPrice();
            }
            else
            {
                equipButton.gameObject.SetActive(false);
                equipedButton.gameObject.SetActive(false);
            }
        }
    }

    private void RefreshWingList()
    {
        foreach(var tmp in wingListParentTransform.GetComponentsInChildren<ListItemWing>())
        {
            Destroy(tmp.gameObject);
        }

        foreach(var tmp in ServerNetworkManager.Instance.Catalog.WingProductList.OrderBy(n => n.Id))
        {
            if(tmp.IsAvailable)
            {
                // Check user owned this wing
                var isPurchased = false;
                var isEquipment = false;

                var tmpWingModel = ServerNetworkManager.Instance.PurchasedWingList.FirstOrDefault(n => n.WingId == tmp.Id);
                
                if(tmpWingModel != null)
                {
                    isPurchased = true;
                    isEquipment = tmpWingModel.IsEquipment;
                }

                var transformToUse = Instantiate(listItemWingPrefab);

                transformToUse.GetComponent<ListItemWing>().UpdateEntity(
                    tmp.Id,
                    !isPurchased,
                    isEquipment
                );

                transformToUse.SetParent(wingListParentTransform, false);
            }
        }

        FocusItems();
    }

    private void CreateOrLoadLockList(string wingId)
    {
        if(PlayerPrefs.HasKey("stat_lock_list_" + ServerNetworkManager.Instance.User.PlayFabId))
        {
            var json = PlayerPrefs.GetString("stat_lock_list_" + ServerNetworkManager.Instance.User.PlayFabId);
            this.statLockLists = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).DeserializeObject<Dictionary<string,List<bool>>>(json);
        }
        else
        {
            this.statLockLists = new Dictionary<string, List<bool>>();
        }

        if(this.statLockLists.ContainsKey(wingId))
            this.statLockList = statLockLists[wingId];
        else
            this.statLockList = new List<bool>(){false, false, false, false, false};
    }

    private void SaveStatLock(string wingId)
    {
        this.statLockLists[wingId] = this.statLockList;
        var json = PlayFab.PluginManager.GetPlugin<ISerializerPlugin>(PluginContract.PlayFab_Serializer).SerializeObject(this.statLockLists);
        PlayerPrefs.SetString("stat_lock_list_" + ServerNetworkManager.Instance.User.PlayFabId, json);
    }

#region Button Event

    public void OnInfoButtonClicked()
    {
        var wingInfoText = 
            LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_wing_info") 
            + "\n"
            + LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_wing_info_3")
            + "\n\n"
            + string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_wing_info_4"), ServerNetworkManager.Instance.Setting.WingMaxLevel);
        
        OutgameController.Instance.ShowGenericInfo(wingInfoText);
    }

    public void OnStatLockButtonClicked(int statIndex, Action onSucces)
    {
        if(IsAvailableStatLock(statIndex))
        {
            this.statLockList[statIndex] = true;
            SaveStatLock(selectedWingId);   
            RefreshResetPrice();
            
            onSucces?.Invoke();
        }
    }

    public void OnStatUnLockButtonClicked(int statIndex, Action onSucces)
    {
        this.statLockList[statIndex] = false;
        SaveStatLock(selectedWingId);  
        RefreshResetPrice();

        onSucces?.Invoke();
    }

    public void OnPurchaseButtonClicked()
    {
        var passedMilliSeconds = DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastResetClickTimeStampMilliseconds;

		if(passedMilliSeconds >= 500d)
		{
            lastResetClickTimeStampMilliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();

            OutgameController.Instance.PurchaseWing(selectedWingId, () => {
                
                if(ServerNetworkManager.Instance.PurchasedWingList.FirstOrDefault(n => n.WingId == selectedWingId) != null)
                    this.isLock = false;

                Refresh();
                isChanged = true;
            });
        }
    }

    public void OnEquipmentButtonClicked()
    {
        OutgameController.Instance.EquipWing(selectedWingId, () => {
            Refresh();
            isChanged = true;
        });
    }

    public void OnStatResetButtonClicked()
    {
        var passedMilliSeconds = DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastResetClickTimeStampMilliseconds;

		if(passedMilliSeconds >= 500d)
		{
            lastResetClickTimeStampMilliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();

            var selectedWingModel = ServerNetworkManager.Instance.PurchasedWingList.FirstOrDefault(n => n.WingId == selectedWingId);
            if(selectedWingModel != null)
            {
                if(
                    ((selectedWingModel.OwnedStatBonus1Rank == "S" || selectedWingModel.OwnedStatBonus1Rank == "A") && !statLockList[(int)WingStatInfoType.ATTACK])
                    || ((selectedWingModel.OwnedStatBonus2Rank == "S" || selectedWingModel.OwnedStatBonus2Rank == "A") && !statLockList[(int)WingStatInfoType.GOLD])
                    || ((selectedWingModel.EquipmentStatBonus1Rank == "S" || selectedWingModel.EquipmentStatBonus1Rank == "A") && !statLockList[(int)WingStatInfoType.SKILL_1])
                    || ((selectedWingModel.EquipmentStatBonus2Rank == "S" || selectedWingModel.EquipmentStatBonus2Rank == "A") && !statLockList[(int)WingStatInfoType.SKILL_2])
                    || ((selectedWingModel.EquipmentStatBonus3Rank == "S" || selectedWingModel.EquipmentStatBonus3Rank == "A") && !statLockList[(int)WingStatInfoType.SKILL_3])
                )
                {
                    UIManager.Instance.ShowAlertLocalized(
                        "message_wing_warning", 
                        () => {
                            OutgameController.Instance.ResetWingStat(selectedWingId, statLockList, () => {
                                RefreshWingInfo();
                                isChanged = true;
                            });
                        }, 
                        () => {}
                    );
                }
                else
                {
                    OutgameController.Instance.ResetWingStat(selectedWingId, statLockList, () => {
                        RefreshWingInfo();
                        isChanged = true;
                    });
                }
            }
        }
    }

    public void OnUlockWingBodyButtonClicked()
    {
        var passedMilliSeconds = DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastResetClickTimeStampMilliseconds;

		if(passedMilliSeconds >= 500d)
		{
            lastResetClickTimeStampMilliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();

            OutgameController.Instance.OpenWingBody(
                () => {
                    RefreshWingBody();

                    if(!isChanged)
                        isChanged = true;
            });
        }
    }

    public void OnUpgradeButtonClicked()
    {
        var passedMilliSeconds = DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastResetClickTimeStampMilliseconds;

		if(passedMilliSeconds >= 500d)
		{
            lastResetClickTimeStampMilliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();

            OutgameController.Instance.UpgradeWingBody(
                (isUpgradeSuccess) => {

                    if(isUpgradeSuccess)
                    {
                        OutgameController.Instance.ShowToast(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_upgrade_success"));
                        SoundManager.Instance.PlaySound("Upgrade");

                        RefreshWingBody();

                        if(!isChanged)
                            isChanged = true;
                    }
                    else
                    {
                        OutgameController.Instance.ShowToast(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_wing_upgrade_fail"));
                        SoundManager.Instance.PlaySound("UpgradeFail");
                    }
            });
        }
    }

#endregion

#region Util

    private bool IsAvailableStatLock(int statIndex)
    {
        // Check stat index validate
        if(statIndex < 0 || statIndex > 4)
            return false;

        // Check stat lock limit
        int lockCount = 0;

        foreach(var tmp in statLockList)
        {
            if(tmp)
                lockCount ++;
        }

        if(lockCount < 4)
            return true;
        else
            return false;
    }

    private void FocusItems()
    {
        StartCoroutine(FocusItemsCoroutine());
    }

    private IEnumerator FocusItemsCoroutine()
    {
        wingRect.movementType = ScrollRect.MovementType.Unrestricted;
        yield return new WaitForEndOfFrame();
        wingRect.movementType = ScrollRect.MovementType.Elastic;

        Canvas.ForceUpdateCanvases();

		wingListParentTransform.anchoredPosition = new UnityEngine.Vector2(wingListParentTransform.anchoredPosition.x, 0f);
    }

#endregion

}
