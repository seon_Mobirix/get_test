﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class UIPopupOption : UIPopup {

	[Title("Center")]
	[SerializeField]
	private UITab uiTab;

	[Title("General", "Left Side")]
	[SerializeField]
	private Dropdown langugateDropDown;
	[SerializeField]
	private Dropdown chatLangugateDropDown;
	[SerializeField]
	private Toggle pushNotificationToggle;
	[SerializeField]
	private Button googlePlayLogOutButton;
	[SerializeField]
	private Button googlePlayBindingButton;
	[SerializeField]
	private Button gameCenterBindingButton;
	[SerializeField]
	private Button changeAccountButton;

	[Title("General", "Right Side")]
	[SerializeField]
	private Text idValueText;
	[SerializeField]
	private Text nicknameValueText;
	[SerializeField]
	private Button couponButton;
	[SerializeField]
	private Button supportButton;
	[SerializeField]
	private Button longSupportButton;
	[SerializeField]
	private Text versionInfoText;

	[Title("Sound")]
	[SerializeField]
	private Slider soundFXSilder;
	[SerializeField]
	private Slider bgmSlider;
	[SerializeField]
	private Toggle vibrationToggle;
	[SerializeField]
	private Transform vibrationArea;

	[Title("Graphic")]
	[SerializeField]
	private Dropdown frameRateDropDown;
	[SerializeField]
	private Toggle postEffectToggle;
	[SerializeField]
	private Toggle performanceMonitorToggle;
	[SerializeField]
	private Toggle damageInfoToggle;

	[Title("Control")]
	[SerializeField]
	private Toggle touchMoveToggle;
	[SerializeField]
	private Toggle autoSkillToggle;
	[SerializeField]
	private Toggle notifyNewRaidsToggle;

	public override void Open()
	{
		Debug.LogWarning("UIPopupOption shouldn't be open without parameters");
	}

	public void OpenWithValues(int tabID)
	{
		base.Open();

		uiTab.OnTabButton(tabID);
		Refresh();
	}

	public override void Close()
	{
		base.Close();

		if(GameObject.FindObjectOfType<UIViewMain>() != null)
        {
            GameObject.FindObjectOfType<UIViewMain>().Refresh();
            GameObject.FindObjectOfType<UIViewMain>().ShowRightTopContent();
        }
	}

	public void CloseWithOutShowRightTopContent()
    {
       	base.Close();

        if(GameObject.FindObjectOfType<UIViewMain>() != null)
            GameObject.FindObjectOfType<UIViewMain>().Refresh();
    }

	public override void Refresh()
	{
#region General

		// Set language dropdowns
		this.langugateDropDown.ClearOptions();
		foreach(var tmp in Enum.GetValues(typeof(OptionManager.LanguageModeType)))
		{
			var stringToUse = tmp.ToString();

			if(stringToUse == "zh_cht")
			{
				this.langugateDropDown.options.Add(new Dropdown.OptionData("繁體中文"));
			}
			else if(stringToUse == "zh_chs")
			{
				this.langugateDropDown.options.Add(new Dropdown.OptionData("简体中文"));
			}
			else if(stringToUse == "th")
			{
				this.langugateDropDown.options.Add(new Dropdown.OptionData("Thai"));
			}
			else
			{
				var textTouse = new CultureInfo(stringToUse).NativeName;
				var tmp2 = new Dropdown.OptionData(textTouse.First().ToString().ToUpper() + textTouse.Substring(1));
				this.langugateDropDown.options.Add(tmp2);
			}
		}
		this.langugateDropDown.value = (int)OptionManager.Instance.Language;
		this.langugateDropDown.RefreshShownValue();

		// Set chat dropdowns
		this.chatLangugateDropDown.ClearOptions();
		
		if(ServerNetworkManager.Instance.User.ServerId == "03")
	    {
			this.chatLangugateDropDown.options.Add(new Dropdown.OptionData("한국어"));
			this.chatLangugateDropDown.value = 0;
		}
		else if(ServerNetworkManager.Instance.User.ServerId == "04")
	    {
			this.chatLangugateDropDown.options.Add(new Dropdown.OptionData("日本語"));
			this.chatLangugateDropDown.value = 0;
		}
		else
		{
			this.chatLangugateDropDown.options.Add(new Dropdown.OptionData("Global"));

			if(ServerNetworkManager.Instance.Setting.LocalChatList.Contains(OptionManager.Instance.Language.ToString()))
			{
				if(OptionManager.Instance.Language.ToString() == "zh_cht")
				{
					this.chatLangugateDropDown.options.Add(new Dropdown.OptionData("繁體中文"));
				}
				else if(OptionManager.Instance.Language.ToString() == "zh_chs")
				{
					this.chatLangugateDropDown.options.Add(new Dropdown.OptionData("简体中文"));
				}
				else
				{
					var textTouse = new CultureInfo(OptionManager.Instance.Language.ToString()).NativeName;
					var tmp2 = new Dropdown.OptionData(textTouse.First().ToString().ToUpper() + textTouse.Substring(1));
					this.chatLangugateDropDown.options.Add(tmp2);
				}

				if(OptionManager.Instance.ForceGlobalChat)
					this.chatLangugateDropDown.value = 0;
				else
					this.chatLangugateDropDown.value = 1;
			}
			else
			{
				this.chatLangugateDropDown.value = 0;
			}
		}

		this.chatLangugateDropDown.RefreshShownValue();

		// Set push notification
		this.pushNotificationToggle.isOn = !OptionManager.Instance.IsPushOff;
 
#if UNITY_IPHONE
			couponButton.gameObject.SetActive(false);
			supportButton.gameObject.SetActive(false);
			longSupportButton.gameObject.SetActive(true);
#else
			longSupportButton.gameObject.SetActive(false);
			supportButton.gameObject.SetActive(true);
			couponButton.gameObject.SetActive(true);
#endif
		
#if UNITY_ANDROID
		gameCenterBindingButton.gameObject.SetActive(false);
		if(Social.localUser.authenticated)
		{
			googlePlayLogOutButton.gameObject.SetActive(true);	
			googlePlayBindingButton.gameObject.SetActive(false);
			changeAccountButton.gameObject.SetActive(false);
		}
		else
		{
			googlePlayLogOutButton.gameObject.SetActive(false);
			googlePlayBindingButton.gameObject.SetActive(true);
			changeAccountButton.gameObject.SetActive(true);
		}
#elif UNITY_IPHONE
		googlePlayLogOutButton.gameObject.SetActive(false);
		googlePlayBindingButton.gameObject.SetActive(false);
		
		gameCenterBindingButton.gameObject.SetActive(false); // No guest mode available, so no need to give the binding option.
		changeAccountButton.gameObject.SetActive(false); // Change account is not possible for iOS, so don't use it.
#endif

		idValueText.text = ServerNetworkManager.Instance.User.PlayFabId;
		nicknameValueText.text = ServerNetworkManager.Instance.User.NickName;

		// Show version
		this.versionInfoText.text = "Version: " + Application.version + " (Build " + RetrieveBuildName() + ")";

#endregion

#region Sound
		// Get volume data
		soundFXSilder.value = OptionManager.Instance.SoundVolume;
		bgmSlider.value = OptionManager.Instance.BGMVolume;

		// Set vibration
		vibrationToggle.isOn = OptionManager.Instance.IsVibrationOn;

		// Vibration should be default
		vibrationArea.gameObject.SetActive(true);

#if UNITY_IPHONE
		// For iPad detection
		var deviceIsIpad = UnityEngine.iOS.Device.generation.ToString().Contains("iPad");
		if (deviceIsIpad) {
			vibrationArea.gameObject.SetActive(false);
		}
#endif

#endregion

#region Graphic
		// Set post effect
		this.postEffectToggle.isOn = OptionManager.Instance.IsPostEffectOn;

		// Set performance monitor
		this.performanceMonitorToggle.isOn = OptionManager.Instance.IsPerformanceMonitorOn;

		// Set damage info off
		this.damageInfoToggle.isOn = OptionManager.Instance.IsDamageInfoOff;

		// Set frameRate DropDown
		this.frameRateDropDown.ClearOptions();
		List<string> frameList = new List<string>() { LanguageManager.Instance.GetTextData(LanguageDataType.UI, "option_frame_auto"), "30fps", "60fps", "120fps"};
		foreach(var tmp in frameList)
		{
			var tmp2 = new Dropdown.OptionData(tmp);
			this.frameRateDropDown.options.Add(tmp2);
		}
		switch(OptionManager.Instance.TargetFrameRate)
		{
			case -1:	
				this.frameRateDropDown.value = 0;
			break;
			case 30:	
				this.frameRateDropDown.value = 1;
			break;
			case 60:
				this.frameRateDropDown.value = 2;
			break;
			case 120:
				this.frameRateDropDown.value = 3;
			break;
			default:
				this.frameRateDropDown.value = 0;
			break;
		} 
		this.frameRateDropDown.RefreshShownValue();

#endregion

#region Control
		// Set touch move
		this.touchMoveToggle.isOn = OptionManager.Instance.IsTouchMoveOn;

		// Set auto skill
		// this.autoSkillToggle.isOn = OptionManager.Instance.IsAutoSkillOn;

		// Set notify new raids
		this.notifyNewRaidsToggle.isOn = OptionManager.Instance.NotifyNewRaidsToggle;
#endregion

	}

#region Toggle

	public void OnPushToggle()
	{
		if(this.pushNotificationToggle.isOn)
			OutgameController.Instance.SetPushNotificationOn();
		else
			OutgameController.Instance.SetPushNotificationOff();
	}

	public void OnVibrationToggle()
	{
		OptionManager.Instance.IsVibrationOn = this.vibrationToggle.isOn;

		if(OptionManager.Instance.IsVibrationOn)
			Handheld.Vibrate();
	}

	public void OnPostEffectToggle()
	{
		OptionManager.Instance.IsPostEffectOn = this.postEffectToggle.isOn;
	}

	public void OnPerformanceMonitorToggle()
	{
		OptionManager.Instance.IsPerformanceMonitorOn = this.performanceMonitorToggle.isOn;
	}

	public void OnDamageInfoToggle()
	{
		OptionManager.Instance.IsDamageInfoOff = this.damageInfoToggle.isOn;
	}

	public void OnTouchMoveToggle()
	{
		OptionManager.Instance.IsTouchMoveOn = this.touchMoveToggle.isOn;
	}

	public void OnAutoSkillToggle()
	{
		OptionManager.Instance.IsAutoSkillOn = this.autoSkillToggle.isOn;
	}

	public void OnNotifyNewRaidsToggle()
	{
		OptionManager.Instance.NotifyNewRaidsToggle = this.notifyNewRaidsToggle.isOn;
	}

#endregion

#region Slider

	public void OnSoundSlider(float value)
	{
		OptionManager.Instance.SoundVolume = value;
	}

	public void OnBGMSlider(float value)
	{
		OptionManager.Instance.BGMVolume = value;
	}

#endregion

#region Drop Down Menu

	public void OnLangguageDropDown(int id)
	{
		if(this.langugateDropDown.value == (int)OptionManager.Instance.Language)
			return;
			
		UIManager.Instance.ShowAlertLocalized(
			"message_change_language",
			() => {
				OptionManager.Instance.Language = (OptionManager.LanguageModeType)id;

				PlayerPrefs.SetInt("is_server_lanugage_set", 0);
				InterSceneManager.Instance.IsMoreGameShown = true;
				ChatManager.Instance.Disconnect();

				SceneChangeManager.Instance.LoadMainGame(true, true);
			},
			() => {
				this.langugateDropDown.value = (int)OptionManager.Instance.Language;
			}
		);
	}

	public void OnChatLanguageDropDown(int id)
	{
		if(ServerNetworkManager.Instance.Setting.LocalChatList.Contains(OptionManager.Instance.Language.ToString()))
		{
			if(this.chatLangugateDropDown.value == 0 && OptionManager.Instance.ForceGlobalChat)
				return;

			if(this.chatLangugateDropDown.value == 1 && !OptionManager.Instance.ForceGlobalChat)
				return;

			if(id == 0)
				OptionManager.Instance.ForceGlobalChat = true;
			else
				OptionManager.Instance.ForceGlobalChat = false;

			ChatManager.Instance.Disconnect();
		}
	}

	public void OnFrameRateDropDown(int id)
	{
		switch(id)
		{
			case 0:
				OptionManager.Instance.TargetFrameRate = -1;
			break;
			case 1:
				OptionManager.Instance.TargetFrameRate = 30;
			break;
			case 2:
				OptionManager.Instance.TargetFrameRate = 60;
			break;
			case 3:
				OptionManager.Instance.TargetFrameRate = 120;
			break;
			default:
				OptionManager.Instance.TargetFrameRate = -1;
			break;
		}
	}

#endregion

#region Buttons

	public void OnCouponButtonClick(){
		OutgameController.Instance.ShowCouponPopup();
	}

	public void OnHelpButtonClick(){
        PageOpenController.Instance.OpenHelp();
    }

	public void OnPlayLogoutButtonClick(){
		OutgameController.Instance.ExplicitSignOutSocial();
    }

	public void OnAccountBindingButtonClick(){
		OutgameController.Instance.BindAuthToExistingGuestAccount();
    }

	public void OnChangeAccountButtonClick(){
		OutgameController.Instance.ShowReloginPopup();
    }

	public void OnIDCoplyButtonClick(){
		UniClipboard.SetText(idValueText.text);
		UIManager.Instance.ShowAlertLocalized("message_copy_id", null, null);
	}

	public void OnNicknameChangeButtonClick()
	{
		OutgameController.Instance.ShowNicknameChangePopup();
	}

	public void OnMoreGameButtonClick()
	{
		bool isMoreViewOK = SB.MoreManager.Instance.isMoreViewOk();

		if(isMoreViewOK)
			OutgameController.Instance.ShowMoreGame();
		else
			SB.MoreManager.openMoregameDeveloperPage();
	}

	public void OnFanPageButtonClick()
	{
		PageOpenController.Instance.OpenFanPage();
	}

	public void OnSyncButtonClick()
	{
		var timeLeft = Mathf.Max(30 + OutgameController.Instance.TimeSyncPlayerData - (int)Time.realtimeSinceStartup, 0);

        if(timeLeft > 0)
        {
			OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
		}
		else 
		{
			OutgameController.Instance.SyncPlayerData(true, "OnSyncButtonClick");
		}
	}

	public void OnPowerSavingModeButtonClick()
	{
		this.Close();
		OutgameController.Instance.ShowPowerSavingCanvas();
	}

	public void OnSoundFXResetButtonClick(){
		soundFXSilder.value = soundFXSilder.maxValue;
		OptionManager.Instance.SoundVolume = soundFXSilder.value;
    }

	public void OnBGMResetButtonClick(){
		bgmSlider.value = bgmSlider.maxValue * 0.75f;
		OptionManager.Instance.BGMVolume = bgmSlider.value;
    }

#endregion
	
	private string RetrieveBuildName()
	{
		var tmp = OptionManager.Instance.ManifestData;
		if(tmp != null)
	        return tmp.buildNumber;
		else
			return "U";
	}

}
