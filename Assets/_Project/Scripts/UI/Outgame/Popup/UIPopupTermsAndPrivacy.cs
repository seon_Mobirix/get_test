﻿using UnityEngine;
using UnityEngine.UI;

public class UIPopupTermsAndPrivacy : UIPopup {

	[SerializeField]
	private Button okButton;

	private bool isAgree = false;

	private bool isAgree2 = false;

	public override void Open()
	{
		base.Open();

		okButton.interactable = false;
	}

	public void OnCheckBoxClicked()
	{
		if(isAgree)
		{
			isAgree = false;
		}
		else
		{
			isAgree = true;
		}

		okButton.interactable = isAgree && isAgree2;
	}

	public void OnCheckBox2Clicked()
	{
        var currentTime = System.DateTime.Now.ToString("yyyy-MM-dd");
		
		if(isAgree2)
		{
			isAgree2 = false;
			
			string text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_push_disagree"), currentTime);
			UIManager.Instance.ShowAlert(text, null , null, true);
			
		}
		else
		{
			isAgree2 = true;

			string text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_push_agree"), currentTime);
			UIManager.Instance.ShowAlert(text, null , null, true);
		}

		okButton.interactable = isAgree && isAgree2;
	}

	public void OnOkButtonClicked()
	{
		OutgameController.Instance.SetTermsAndPrivacyAgree();
		base.Close();
	}
}
