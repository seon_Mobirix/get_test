﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIPopupApplication : UIPopup
{
    [SerializeField]
    private Transform emptyInfoText;

    [SerializeField]
	private Transform listItemApplicantPrefab;

    [SerializeField]
	private RectTransform applicantListParentTransform;

    public override void Open()
	{
		base.Open();

		Refresh();

        FocusItems();
	}

    public override void Refresh()
	{
        foreach(var tmp in applicantListParentTransform.GetComponentsInChildren<ListItemApplicant>())
        {
            Destroy(tmp.gameObject);
        }

        int applicationCount = 0;

        foreach(var tmp in ServerNetworkManager.Instance.GuildData.ApplicantData.ApplicantList)
        {
            applicationCount ++;
            
            var transformToUse = Instantiate(listItemApplicantPrefab);
            transformToUse.GetComponent<ListItemApplicant>().UpdateEntity(tmp.Id, tmp.Name, tmp.StageProgress);

            transformToUse.SetParent(applicantListParentTransform, false);
        }

        if(applicationCount != 0)
            emptyInfoText.gameObject.SetActive(false);
        else
            emptyInfoText.gameObject.SetActive(true);
    }

    private void FocusItems()
    {
        StartCoroutine(FocusItemsCoroutine());
    }

    private IEnumerator FocusItemsCoroutine()
    {
        applicantListParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Unrestricted;
        yield return new WaitForEndOfFrame();
        applicantListParentTransform.parent.GetComponent<ScrollRect>().movementType = ScrollRect.MovementType.Elastic;

        Canvas.ForceUpdateCanvases();

        applicantListParentTransform.anchoredPosition = new Vector2(applicantListParentTransform.anchoredPosition.x, 0f);
    }
}
