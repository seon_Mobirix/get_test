﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIPopup : MonoBehaviour {
	
	[SerializeField]
	private bool skipAnimation = false;

	private float onOffTime = 0.1f;
	private GameObject background;
	private GameObject whitecard;
	
	[SerializeField]
	private bool addBackground = true;

	[SerializeField]
	private bool showWhitecard = false;

	[SerializeField]
	private bool addBackBlocker = false;

	[SerializeField]
	private RectTransform topBarPosition;

	private bool isOpening = false;
	public virtual void Open()
	{	
		if(addBackground)
			AddBackground();
		
		this.gameObject.SetActive(true);

		if(showWhitecard)
			AddWhitecard();
			
		if(addBackBlocker)
		{
			var viewToAsk = GameObject.FindObjectOfType<UIViewMain>();
			if(viewToAsk != null)
				viewToAsk.ShowBackBlocker();
		}

		isOpening = true;

		OutgameController.Instance.PushPopupInStack(this);

		StartCoroutine(RunPopupOpen());
	}

	protected virtual void OnOpen()
	{
		if(ServerNetworkManager.Instance.CheckNeedRelogIn())
		{
			UIManager.Instance.ShowAlertLocalized(
				"message_new_update",
				() => {
					Application.Quit();
				}, 
				() => {
					Application.Quit();
				}
			);
		}
	}

	public virtual void Refresh()
	{
		// Do nothing here.
	}

	private bool isClosing = false;
	public virtual void Close()
	{
		if(isOpening || isClosing || !this.gameObject.activeSelf)
			return;

		isClosing = true;

		if(GameObject.FindObjectOfType<OutgameController>() != null)
			OutgameController.Instance.PopPopupInStack(this);

		if(addBackBlocker)
		{
			var viewToAsk = GameObject.FindObjectOfType<UIViewMain>();
			if(viewToAsk != null)
				viewToAsk.HideBackBlocker();
		}

		StartCoroutine(RunPopupClose());
	}

	private IEnumerator RunPopupOpen()
	{
		float passed = 0f;
		while(passed < onOffTime && !skipAnimation)
		{
			this.transform.localScale = Vector3.Lerp(new Vector3(0f, 0f, 0f), new Vector3(1f, 1f, 1f), Mathf.Sqrt(passed/onOffTime));
			passed += Time.unscaledDeltaTime;
			yield return -1;
		}
		this.transform.localScale = new Vector3(1f, 1f, 1f);

		if(topBarPosition != null)
		{
			var screenRatio = (float)Screen.width / (float)Screen.height;
			if(screenRatio < 1.7f)
			{
				var sizeY = 1920f / screenRatio;
				var desiredHeight = 1080f;
				var offset = Mathf.Floor((sizeY - desiredHeight) / 2f);
				topBarPosition.anchoredPosition = new Vector2(topBarPosition.anchoredPosition.x, offset);
			}
		}

		isOpening = false;
		OnOpen();
	}

	private IEnumerator RunPopupClose()
	{
		if(addBackground)
		{
			var image = background.GetComponent<Image>();
			if (image != null)
				image.CrossFadeAlpha(0f, onOffTime * 0.95f, false);
		}

		float passed = 0f;
		while(passed < onOffTime && !skipAnimation)
		{
			this.transform.localScale =  Vector3.Lerp(new Vector3(1f, 1f, 1f), new Vector3(0f, 0f, 0f), Mathf.Pow(passed/onOffTime, 2f));
			passed += Time.unscaledDeltaTime;
			yield return -1;
		}
		this.transform.localScale = new Vector3(0f, 0f, 0f);

		if(addBackground)
			Destroy(background);

		this.gameObject.SetActive(false);
		isClosing = false;
		OnPopupCloseCompleted();
	}

	private void AddBackground()
	{
		background = Instantiate<Transform>(Resources.Load<Transform>("Popup Back")).gameObject;

		GameObject canvas = GameObject.Find("Popup Canvas");

		if(this.transform.parent != null)
			canvas = this.transform.parent.gameObject;
		
		background.transform.SetParent(canvas.transform, false);
		background.transform.SetSiblingIndex(transform.GetSiblingIndex());
	}

	public void AddWhitecard()
	{
		whitecard = Instantiate<Transform>(Resources.Load<Transform>("Whitecard")).gameObject;
		
		GameObject canvas = GameObject.Find("Popup Canvas");

		if(this.transform.parent != null)
			canvas = this.transform.parent.gameObject;
		
		whitecard.transform.SetParent(canvas.transform, false);
		whitecard.transform.SetSiblingIndex(transform.GetSiblingIndex() + 1);
	}

	public virtual void OnPopupCloseCompleted()
	{
		
	}
}