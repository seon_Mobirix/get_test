﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class UIViewArena : MonoBehaviour 
{   

    [Title("Topbar UI Contents")]

    [SerializeField]
    private Text myProfileText = null;

    [SerializeField]
    private Text opponentProfileText = null;

    [SerializeField]
    private Text battleStateText = null;

    [Title("Skill Controller")]

    [SerializeField]
    private Button autoAttackButton;

    [SerializeField]
    private Image autoAttackButtonBackground;

    [SerializeField]
    private Text autoAttackButtonText;

    [SerializeField]
    private Button skill1Button;
    [SerializeField]
    private Image skill1ButtonImage;
    [SerializeField]
    private Image skill1LockImage;

    [SerializeField]
    private Button skill2Button;
    [SerializeField]
    private Image skill2ButtonImage;
    [SerializeField]
    private Image skill2LockImage;

    [SerializeField]
    private Button skill3Button;
    [SerializeField]
    private Image skill3ButtonImage;
    [SerializeField]
    private Image skill3LockImage;

    [Title("Skill Info")]
    [SerializeField] Image _arenaSpeedRateButtonBackgound;
    [SerializeField] Text _arenaSpeedRateButtonText;

    [SerializeField]
    private Text mySkill1PowerText;
    [SerializeField]
    private Text mySkill2PowerText;
    [SerializeField]
    private Text mySkill3PowerText;

    [SerializeField]
    private Text enemySkill1PowerText;
    [SerializeField]
    private Text enemySkill2PowerText;
    [SerializeField]
    private Text enemySkill3PowerText;

	[Title("Other UI Contents")]

    [SerializeField]
    private Slider mySlider = null;

    [SerializeField]
    private Slider opponentSlider = null;

    [SerializeField]
    private Text myDamageText = null;

    [SerializeField]
    private Text opponentDamageText = null;

    [SerializeField]
    private Text timeLeftText = null;

    [Title("Others")]
    public LayerMask layerOfBackground;

#region Refresh Methods

	public void Refresh()
	{
        RefreshBattleText(false, false);
        RefreshProfile();
        RefreshAttackButton();
        RefreshGauge();
        RefreshTime();
        RefreshSkillButtons();
        RefreshSkillCooldown();
        RefreshSkillPower();
        RefreshArenaSpeedRateButtonStatus();
    }

    public void RefreshArenaSpeedRateButtonStatus()
    {
        _arenaSpeedRateButtonBackgound.gameObject.SetActive(SceneChangeManager.Instance.SceneName == "ArenaGame");

        if (!PlayerPrefs.HasKey("arenaSpeedRate") || PlayerPrefs.GetInt("arenaSpeedRate") == 0)
        {
            _arenaSpeedRateButtonBackgound.color = new Color(1f, 1f, 1f, 1f);
            _arenaSpeedRateButtonText.color = new Color(1f, 1f, 1f, 1f);            
        }
        else
        {
            _arenaSpeedRateButtonBackgound.color = new Color(1f, 0.7215686f, 0, 1);
            _arenaSpeedRateButtonText.color = new Color(1f, 0.7215686f, 0, 1);
        }        
    }

    public void OnArenaSpeedRateButtonClicked()
    {
        UIManager.Instance.IsArenaSpeedRate = !UIManager.Instance.IsArenaSpeedRate;
        PlayerPrefs.SetInt("arenaSpeedRate", UIManager.Instance.IsArenaSpeedRate ? 1 : 0);
        RefreshArenaSpeedRateButtonStatus();
        IngameArenaController.Instance.SetArenaSpeedRate(isUp: UIManager.Instance.IsArenaSpeedRate);
    }

    public void RefreshBattleText(bool isEnd, bool didWin)
    {
        battleStateText.enabled = true;
        if(isEnd)
        {
            if(didWin)
                battleStateText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "arena_win");
            else
                battleStateText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "arena_lose");
        }
        else
        {
            battleStateText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "arena_start");
        }

        StartCoroutine(HideBattleTextCoroutine());
    }

    private IEnumerator HideBattleTextCoroutine()
    {
        yield return new WaitForSeconds(2.5f);
        battleStateText.enabled = false;
    }

    private void RefreshProfile()
    {
        var tmpText = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "profile_info_arena");
        myProfileText.text = string.Format(
            tmpText, 
            ServerNetworkManager.Instance.User.NickName, 
            LanguageManager.Instance.NumberToString(NumberArenaController.Instance.DPS)
        );
        opponentProfileText.text = string.Format(
            tmpText, 
            InterSceneManager.Instance.OpponentName, 
            LanguageManager.Instance.NumberToString(NumberArenaController.Instance.DPSEnemy)
        );
    }

    public void RefreshAttackButton()
    {
        autoAttackButton.interactable = false;

        autoAttackButtonBackground.color = new Color(1f, 0.7215686f, 0, 1);
        autoAttackButtonText.color = new Color(1f, 0.7215686f, 0, 1);
    }

    public void RefreshGauge()
    {
        myDamageText.text = LanguageManager.Instance.NumberToString(IngameArenaController.Instance.HeroDamage);
        opponentDamageText.text = LanguageManager.Instance.NumberToString(IngameArenaController.Instance.EnemyDamage);
        var gaugeValue = (float)Math.Exp(BigInteger.Log(IngameArenaController.Instance.HeroDamage + 1) - BigInteger.Log(IngameArenaController.Instance.HeroDamage + IngameArenaController.Instance.EnemyDamage + 2));
        gaugeValue = Mathf.Clamp01(gaugeValue);

        if(IngameArenaController.Instance.LeftTime >= 21)
        {
            if(IngameArenaController.Instance.AttackCount < 5)
            {
                gaugeValue = Mathf.Clamp(gaugeValue, 0.1f, 0.9f);
            }
            else if(IngameArenaController.Instance.AttackCount < 10)
            {
                gaugeValue = Mathf.Clamp(gaugeValue, 0.05f, 0.95f);
            }
            else if(IngameArenaController.Instance.AttackCount < 20)
            {
                gaugeValue = Mathf.Clamp(gaugeValue, 0.025f, 0.975f);
            }
        }

        mySlider.value = gaugeValue;
        opponentSlider.value = 1f - gaugeValue;
    }

    public void RefreshTime()
    {
        timeLeftText.text = IngameArenaController.Instance.LeftTime.ToString();
    }

    public void RefreshSkillButtons()
    {
        bool isSkill1Unlock = (ServerNetworkManager.Instance.User.SkillInfo[0].Level > 0);
        bool isSkill2Unlock = (ServerNetworkManager.Instance.User.SkillInfo[1].Level > 0);
        bool isSkill3Unlock = (ServerNetworkManager.Instance.User.SkillInfo[2].Level > 0);
        
        skill1Button.interactable = isSkill1Unlock;
        skill1ButtonImage.gameObject.SetActive(isSkill1Unlock);
        skill1LockImage.gameObject.SetActive(!isSkill1Unlock);
        
        skill2Button.interactable = isSkill2Unlock;
        skill2ButtonImage.gameObject.SetActive(isSkill2Unlock);
        skill2LockImage.gameObject.SetActive(!isSkill2Unlock);

        skill3Button.interactable = isSkill3Unlock;
        skill3ButtonImage.gameObject.SetActive(isSkill3Unlock);
        skill3LockImage.gameObject.SetActive(!isSkill3Unlock);
    }

    public void RefreshSkillCooldown()
    {
        skill1ButtonImage.fillAmount = IngameArenaController.Instance.HeroIdentity.skill1Cooldown;
        if(skill1ButtonImage.fillAmount != 1f)
            skill1ButtonImage.color = new Color(1f, 1f, 1f, 0.5f);
        else
            skill1ButtonImage.color = new Color(1f, 1f, 1f, 1f);
        skill2ButtonImage.fillAmount = IngameArenaController.Instance.HeroIdentity.skill2Cooldown;
        if(skill2ButtonImage.fillAmount != 1f)
            skill2ButtonImage.color = new Color(1f, 1f, 1f, 0.5f);
        else
            skill2ButtonImage.color = new Color(1f, 1f, 1f, 1f);
        skill3ButtonImage.fillAmount = IngameArenaController.Instance.HeroIdentity.skill3Cooldown;
        if(skill3ButtonImage.fillAmount != 1f)
            skill3ButtonImage.color = new Color(1f, 1f, 1f, 0.5f);
        else
            skill3ButtonImage.color = new Color(1f, 1f, 1f, 1f);
    }

    public void RefreshSkillPower()
    {
        if(ServerNetworkManager.Instance.User.SkillInfo[0].Level != 0)
        {
            var targetLegendaryRelic = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.LegendaryRelicType == LegendaryRelicType.SKILL1_ATTACK);
            var mutliplierByLegendaryRelic = (targetLegendaryRelic != null)? targetLegendaryRelic.SKill1AttackMultiplier : 1;
            mySkill1PowerText.text = string.Format("x{0:0.00}", mutliplierByLegendaryRelic * ServerNetworkManager.Instance.User.SkillInfo[0].EffectPower);
        }
        else
        {
            mySkill1PowerText.transform.parent.gameObject.SetActive(false);
        }
        
        if(ServerNetworkManager.Instance.User.SkillInfo[1].Level != 0)
        {
            var targetLegendaryRelic = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.LegendaryRelicType == LegendaryRelicType.SKILL2_ATTACK);
            var mutliplierByLegendaryRelic = (targetLegendaryRelic != null)? targetLegendaryRelic.SKill2AttackMultiplier : 1;
            mySkill2PowerText.text = string.Format("x{0:0.00}", mutliplierByLegendaryRelic * ServerNetworkManager.Instance.User.SkillInfo[1].EffectPower);
        }
        else
        {
            mySkill2PowerText.transform.parent.gameObject.SetActive(false);
        }
        
        if(ServerNetworkManager.Instance.User.SkillInfo[2].Level != 0)
        {
            var targetLegendaryRelic = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.LegendaryRelicType == LegendaryRelicType.SKILL3_ATTACK);
            var mutliplierByLegendaryRelic = (targetLegendaryRelic != null)? targetLegendaryRelic.SKill3AttackMultiplier : 1;
            mySkill3PowerText.text = string.Format("x{0:0.00}", mutliplierByLegendaryRelic * ServerNetworkManager.Instance.User.SkillInfo[2].EffectPower);
        }
        else
        {
            mySkill3PowerText.transform.parent.gameObject.SetActive(false);
        }

        if(InterSceneManager.Instance.OpponentUserData.SkillInfo[0].Level != 0)
        {
            var targetLegendaryRelic = InterSceneManager.Instance.OpponentInventoryData.LegendaryRelicList.FirstOrDefault(n => n.LegendaryRelicType == LegendaryRelicType.SKILL1_ATTACK);
            var mutliplierByLegendaryRelic = (targetLegendaryRelic != null)? targetLegendaryRelic.SKill1AttackMultiplier : 1;
            enemySkill1PowerText.text = string.Format("x{0:0.00}", mutliplierByLegendaryRelic * InterSceneManager.Instance.OpponentUserData.SkillInfo[0].EffectPower);
        }
        else
        {
            enemySkill1PowerText.transform.parent.gameObject.SetActive(false);
        }
        
        if(InterSceneManager.Instance.OpponentUserData.SkillInfo[1].Level != 0)
        {
            var targetLegendaryRelic = InterSceneManager.Instance.OpponentInventoryData.LegendaryRelicList.FirstOrDefault(n => n.LegendaryRelicType == LegendaryRelicType.SKILL2_ATTACK);
            var mutliplierByLegendaryRelic = (targetLegendaryRelic != null)? targetLegendaryRelic.SKill2AttackMultiplier : 1;
            enemySkill2PowerText.text = string.Format("x{0:0.00}", mutliplierByLegendaryRelic * InterSceneManager.Instance.OpponentUserData.SkillInfo[1].EffectPower);
        }
        else
        {
            enemySkill2PowerText.transform.parent.gameObject.SetActive(false);
        }
        
        if(InterSceneManager.Instance.OpponentUserData.SkillInfo[2].Level != 0)
        {
            var targetLegendaryRelic = InterSceneManager.Instance.OpponentInventoryData.LegendaryRelicList.FirstOrDefault(n => n.LegendaryRelicType == LegendaryRelicType.SKILL3_ATTACK);
            var mutliplierByLegendaryRelic = (targetLegendaryRelic != null)? targetLegendaryRelic.SKill3AttackMultiplier : 1;
            enemySkill3PowerText.text = string.Format("x{0:0.00}", mutliplierByLegendaryRelic * InterSceneManager.Instance.OpponentUserData.SkillInfo[2].EffectPower);
        }
        else
        {
            enemySkill3PowerText.transform.parent.gameObject.SetActive(false);
        }
    }

#endregion

#region Game Play Button Callbacks

    public void OnMoveAreaTouch()
    {
        // Check touch move option
        if(OptionManager.Instance.IsTouchMoveOn)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, layerOfBackground.value))
            {
                IngameArenaController.Instance.ReadyForMove(hit.point);
            }
        }
    }

    public void OnAttackButtonClicked()
    {
        IngameArenaController.Instance.ReadyForAttack();
    }

    public void OnSkillButtonClicked(int skillIndex)
    {
        IngameArenaController.Instance.ReadyForSkill(skillIndex);
    }

    public void OnAutoAttackButtonClicked()
    {
        // Do nothing
    }

#endregion

}
