﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.Rendering.PostProcessing;

public class UIViewPowerSaving : MonoBehaviour
{
    [SerializeField]
    private Camera mainCamera;
    [SerializeField]
    private PostProcessLayer postProcessLayer;

    [SerializeField]
    private Text closeInfoText;
    [SerializeField]
    private List<Sprite> batterySpriteList;
    [SerializeField]
    private Image batteryInfoIcon;
    [SerializeField]
    private Text batteryInfoText;
    [SerializeField]
    private List<Sprite> wifiSignalSpriteList;
    [SerializeField]
    private Image wifiSignalInfoIcon;
    [SerializeField]
    private Transform mobileDataText;

    [SerializeField]
    private int offCount = 3;

    private bool isOn = false;
    public bool IsOn{
        get{
            return isOn;
        }
    }

    private int originalCullingMask;

    private bool previouslyConnected = false;

    public void ShowPowerSavingMode()
    {
        if(this.isOn)
            return;
            
        this.isOn = true;
        this.offCount = 3;

        previouslyConnected = ChatManager.Instance.IsConnected;

        ChatManager.Instance.Disconnect();

        SoundManager.Instance.MuteBGM();
        SoundManager.Instance.MuteEffectVolume();

        UIManager.Instance.HideCanvas("Main Canvas");
        UIManager.Instance.ShowCanvas("Power Saving Canvas");

        originalCullingMask = mainCamera.cullingMask;
        mainCamera.cullingMask = 0;
        postProcessLayer.enabled = false;

        closeInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_power_saving_mode_release"), offCount);

        UpdateBatteryInfo();
        UpdateWifiInfo();
    }

    public void TouchScreen()
    {
        this.offCount = this.offCount - 1;

        if(this.offCount <= 0)
            ClosePowerSavingMode();
        else
            closeInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_power_saving_mode_release"), offCount);
    }

    private void ClosePowerSavingMode()
    {
        this.isOn = false;

		SoundManager.Instance.SetBGMVolume(OptionManager.Instance.BGMVolume);
		SoundManager.Instance.SetEffectVolume(OptionManager.Instance.SoundVolume);

        UIManager.Instance.ShowCanvas("Main Canvas");
        UIManager.Instance.HideCanvas("Power Saving Canvas");

        mainCamera.cullingMask = originalCullingMask;
        postProcessLayer.enabled = true;

        if(previouslyConnected)
            OutgameController.Instance.ChatOnOff();
    }

    public void UpdateBatteryInfo()
    {
        float batteyLevel = SystemInfo.batteryLevel;

        batteryInfoText.text = $"{Mathf.FloorToInt(batteyLevel * 100)}%";

        switch(SystemInfo.batteryStatus)
        {
            case BatteryStatus.Charging:
                batteryInfoIcon.sprite = batterySpriteList[0];
            break;
            case BatteryStatus.Discharging:
                if(batteyLevel < 0.2f)
                    batteryInfoIcon.sprite = batterySpriteList[1];
                else if(batteyLevel >= 0.9f)
                    batteryInfoIcon.sprite = batterySpriteList[2];
                else
                    batteryInfoIcon.sprite = batterySpriteList[3];
            break;
        }
    }

    public void UpdateWifiInfo()
    {
        if(Application.internetReachability == NetworkReachability.NotReachable)
        {
            wifiSignalInfoIcon.sprite = wifiSignalSpriteList[0];
            wifiSignalInfoIcon.gameObject.SetActive(true);
            mobileDataText.gameObject.SetActive(false);
        }
        else if(Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork)
        {
            wifiSignalInfoIcon.sprite = wifiSignalSpriteList[1];
            wifiSignalInfoIcon.gameObject.SetActive(true);
            mobileDataText.gameObject.SetActive(false);
        }
        else if(Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork)
        {
            wifiSignalInfoIcon.gameObject.SetActive(false);
            mobileDataText.gameObject.SetActive(true);
        }
    }
}
