﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIViewFade : MonoBehaviour {

	[SerializeField]
	private bool shouldCoverWhenOpen = true;

	[SerializeField]
	private Image fadeImage;

	[SerializeField]
	private Image fadeImageWhite;

	private void Awake()
	{
		if(shouldCoverWhenOpen)
			CoverBlack();
	}

	public void CoverBlack()
	{
		UIManager.Instance.ShowCanvas("Fade Canvas");
		fadeImage.color = new Color(fadeImage.color.r, fadeImage.color.g, fadeImage.color.b, 1f);
		fadeImageWhite.color = new Color(fadeImageWhite.color.r, fadeImageWhite.color.g, fadeImageWhite.color.b, 0f);
	}

	public void CoverWhite()
	{
		UIManager.Instance.ShowCanvas("Fade Canvas");
		fadeImage.color = new Color(fadeImage.color.r, fadeImage.color.g, fadeImage.color.b, 0f);
		fadeImageWhite.color = new Color(fadeImageWhite.color.r, fadeImageWhite.color.g, fadeImageWhite.color.b, 1f);
	}

	public void FadeOutIn(bool isWhite = false, float duration = 1f, float fadeOutDuration = 0.75f, float fadeInDuration = 0.75f)
	{
		StartCoroutine(FadeOutInCoroutine(isWhite, duration, fadeOutDuration, fadeInDuration));
	}

	private IEnumerator FadeOutInCoroutine(bool isWhite = false, float duration = 1f, float fadeOutDuration = 0.75f, float fadeInDuration = 0.75f)
	{
		FadeOut(isWhite, fadeOutDuration);
		yield return new WaitForSeconds(duration);
		FadeIn(isWhite, fadeInDuration);
	}

	public void FadeIn(bool isWhite = false, float duration = 0.75f)
	{
		UIManager.Instance.ShowCanvas("Fade Canvas");

		if(!isWhite)
		{
			CoverBlack();
			StartCoroutine(CrossFade(fadeImage, 0f, duration));
		}
		else
		{
			CoverWhite();
			StartCoroutine(CrossFade(fadeImageWhite, 0f, duration));
		}
	}

	public void FadeOut(bool isWhite = false, float duration = 0.75f)
	{
		UIManager.Instance.ShowCanvas("Fade Canvas");

		if(!isWhite)
		{
			fadeImageWhite.color = new Color(fadeImageWhite.color.r, fadeImageWhite.color.g, fadeImageWhite.color.b, 0f);
			fadeImage.color = new Color(fadeImage.color.r, fadeImage.color.g, fadeImage.color.b, 0f);
			StartCoroutine(CrossFade(fadeImage, 1f, duration));
		}
		else
		{
			fadeImage.color = new Color(fadeImage.color.r, fadeImage.color.g, fadeImage.color.b, 0f);
			fadeImageWhite.color = new Color(fadeImageWhite.color.r, fadeImageWhite.color.g, fadeImageWhite.color.b, 0f);
			StartCoroutine(CrossFade(fadeImageWhite, 1f, duration));
		}
	}

	private IEnumerator CrossFade(Image image, float target, float duration)
	{
		float passed = 0f;
		float original = image.color.a;
		
		while(passed < duration)
		{
			yield return -1;
			passed += Time.deltaTime;
			image.color = new Color(image.color.r, image.color.g, image.color.b, Mathf.Lerp(original, target, passed/duration));
		}

		image.color = new Color(image.color.r, image.color.g, image.color.b, target);

		if(target == 0f)
		{
			UIManager.Instance.HideCanvas("Fade Canvas");
		}
	}
}
