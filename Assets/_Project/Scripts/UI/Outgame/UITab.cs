﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITab : MonoBehaviour {

	[Serializable]
	public class TabButton
	{
		public Image background;
		public Text text;
		public Image icon;
	}
	[SerializeField]
	private Sprite selectedSprite;
	[SerializeField]
	private Sprite unSelectedSprite;

	[SerializeField]
	private Color selectedColor;
	[SerializeField]
	private Color unSelectedColor;

	[SerializeField]
	private List<TabButton> tabButtons;
	[SerializeField]
	private List<Transform> tabContents;

	public void OnTabButton(int buttonId)
	{
		foreach(var tmp in tabContents)
		{
			tmp.gameObject.SetActive(false);
		}
		tabContents[buttonId].gameObject.SetActive(true);

		foreach(var tmp in tabButtons)
		{
			tmp.background.sprite = unSelectedSprite;
			
			tmp.text.color = unSelectedColor;

			if(tabButtons[buttonId].icon != null)
				tmp.icon.color = unSelectedColor;
		}
		
		tabButtons[buttonId].background.sprite = selectedSprite;
		tabButtons[buttonId].text.color = selectedColor;

		if(tabButtons[buttonId].icon != null)
			tabButtons[buttonId].icon.color = selectedColor;
	}

}
