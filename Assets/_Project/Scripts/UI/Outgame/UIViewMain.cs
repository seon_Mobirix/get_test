﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class UIViewMain : MonoBehaviour 
{   
	[TitleGroup("Top Bar")]
    [SerializeField]
	private Text profileNicknameText;
	[SerializeField]
	private Text goldAmountText;
	[SerializeField]
	private Text gemAmountText;
    [SerializeField]
    private Transform raidTicketAmountInfo;
    [SerializeField]
    private Text raidTicketAmountText;
    [SerializeField]
    private Transform heartAmountInfo;
    [SerializeField]
    private Text heartAmountText;
    [SerializeField]
    private Transform featherAmountInfo;
    [SerializeField]
    private Text featherAmountText;
    [SerializeField]
    private Transform guildTokenAmountInfo;
    [SerializeField]
    private Text guildTokenAmountText;

    [SerializeField]
    private Text berserkNameText;
    [SerializeField]
    private Slider berserkSlider;
    [SerializeField]
    private Image beserkSliderFill;
    [SerializeField]
    private List<Sprite> berserkSliderSpriteList = new List<Sprite>();
    [SerializeField]
    private Text berserkText;

    [Title("Right Horizontal Bar")]
    [SerializeField]
    private Transform horizontalMenuList;
    [SerializeField]
    private Transform upgradeReddot;
    [SerializeField]
    private Transform upgradeEquipmentReddot;
    [SerializeField]
    private Transform achievementRedDot;
    [SerializeField]
    private Transform moreButton;
    [SerializeField]
    private Transform moreRedDot;
    [SerializeField]
    private Transform closeMoreButton;
    [SerializeField]
    private Transform moreMenuGroup;

    [Title("More Group Menu")]
    [SerializeField]
    private Transform noticeRedDot;
    [SerializeField]
	private Transform alertForInbox;
    [SerializeField]
    private Transform petRedDot;
    [SerializeField]
    private Transform growthSupportRedDot;
    [SerializeField]
	private Transform dailyRewardReddot;
    [SerializeField]
	private Transform freeCoinReddot;
    [SerializeField]
	private Transform freeCoinButton;
    [SerializeField]
	private Text freeCoinText;
    [SerializeField]
    private Transform eventReddot;
    [SerializeField]
    private Transform eventButton;
    [SerializeField]
    private Transform towerReddot;
   
    [Title("Buffs")]

    [SerializeField]
	private Text atkBuffValueText;
    [SerializeField]
	private Text atkBuffLeftTimeText;
    [SerializeField]
	private Transform atkBuffButton;

    [SerializeField]
	private Text goldBuffValueText;
    [SerializeField]
	private Text goldBuffLeftTimeText;
    [SerializeField]
	private Transform goldBuffButton;

    [SerializeField]
	private Text speedBuffValueText;
    [SerializeField]
	private Text speedBuffLeftTimeText;
    [SerializeField]
	private Transform speedBuffButton;

	[Title("Skill Controller")]        
    [SerializeField]
    private Button autoAttackButton;

    [SerializeField]
    private Image autoAttackButtonBackground;

    [SerializeField]
    private Text autoAttackButtonText;

    [SerializeField]
    private Button skill1Button;
    [SerializeField]
    private Image skill1ButtonImage;
    [SerializeField]
    private Image skill1LockImage;

    [SerializeField]
    private Button skill2Button;
    [SerializeField]
    private Image skill2ButtonImage;
    [SerializeField]
    private Image skill2LockImage;

    [SerializeField]
    private Button skill3Button;
    [SerializeField]
    private Image skill3ButtonImage;
    [SerializeField]
    private Image skill3LockImage;

	[Title("Left Items")]
    [SerializeField]
    private Button bossButton;
    [SerializeField]
    private Image bossIconBackground;
    [SerializeField]
    private Transform chatButton;
    [SerializeField]
    private Image followCameraButtonBackground;
    [SerializeField]
    private Image followCameraButtonIcon;
    [SerializeField]
    private Text followCameraButtonText;

    [Title("Others")]
    [SerializeField]
    private Text currentStageInfoText;
    [SerializeField]
    private Text newStageInfoText;
    [SerializeField]
    private Transform backBlocker;
    public LayerMask layerOfBackground;

    private bool originalMoreButtonActiveValue = true;
    private bool originalCloseMoreButtonActiveValue = false;
    private bool originalMoreMenuGroupActiveValue = false;

    // Cool time for buttons
    private double lastArenaClickTimeStampMilliseconds;
    private double lastRaidClickTimeStampMilliseconds;
    private double lastGuildClickTimeStampMilliseconds;
    private double lastFreeGemClickTimeStampMilliseconds;

    private void Start()
    {
        if(PlayerPrefs.HasKey("ui_auto_attack"))
        {   
            if(PlayerPrefs.GetInt("ui_auto_attack") == 0)
                UIManager.Instance.IsAutoOn = false;
            else if(PlayerPrefs.GetInt("ui_auto_attack") == 1)
            {
                UIManager.Instance.IsAutoOn = true;
                OptionManager.Instance.IsAutoSkillOn = true;
            }
            else if(PlayerPrefs.GetInt("ui_auto_attack") == 2)
            {
                UIManager.Instance.IsAutoOn = true;
                OptionManager.Instance.IsAutoSkillOn = false;
            }
        }
        else
            UIManager.Instance.IsAutoOn = false;

        if(PlayerPrefs.HasKey("ui_camera_follow_off") && PlayerPrefs.GetInt("ui_camera_follow_off") == 1)
            UIManager.Instance.IsCameraFollowRotation = false;
        else
            UIManager.Instance.IsCameraFollowRotation = true;

        
    }

	public void StartUpdateTime()
    {
        StartCoroutine(UpdateTimeCoroutine());
    }

    private IEnumerator UpdateTimeCoroutine()
    {
        while(true)
        {
            yield return new WaitForSeconds(1);
        }
    }

#region Refresh Methods

	public void Refresh()
	{
	    RefreshTopbar();
        RefreshMenuGroup();
        RefreshLeftBuffHorizontalBar();
        RefreshAutoButtonStatus();
        RefreshRightHorizontalbar();
        RefreshCurrentStageInfo();
        RefreshBoss();
        RefreshSkillButtons();
        RefreshSkillCooldown();
        RefreshCameraButton();
	}

	public void RefreshTopbar()
	{
        var rankColor = InterSceneManager.Instance.GetColorForRank(ServerNetworkManager.Instance.RankingPosition);

        var ranking = "?";
        if(ServerNetworkManager.Instance.RankingPosition != 0)
        {
            if(OptionManager.Instance.Language != OptionManager.LanguageModeType.en)
                ranking = ServerNetworkManager.Instance.RankingPosition.ToString();
            else
                ranking = ChatManager.AddOrdinal(ServerNetworkManager.Instance.RankingPosition);
        }
        
        this.profileNicknameText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "ranking_format"),ranking, rankColor) + " " + ServerNetworkManager.Instance.User.NickName;

		this.gemAmountText.text = string.Format(
            "{0:#,###0.#}", 
            ServerNetworkManager.Instance.TotalGem
        );

        this.goldAmountText.text = LanguageManager.Instance.NumberToString(
            ServerNetworkManager.Instance.Inventory.Gold
        );
        
        if(heartAmountInfo.gameObject.activeSelf)
        {
            this.heartAmountText.text = LanguageManager.Instance.NumberToString(
                ServerNetworkManager.Instance.Inventory.Heart
            );
        }

        if(raidTicketAmountInfo.gameObject.activeSelf)
        {
            this.raidTicketAmountText.text = string.Format(
                "{0:#,###0.#}", 
                ServerNetworkManager.Instance.Inventory.TicketRaid
            );
        }

        if(featherAmountInfo.gameObject.activeSelf)
        {
            this.featherAmountText.text = string.Format(
                "{0:#,###0.#}", 
                ServerNetworkManager.Instance.Inventory.Feather
            );
        }

        if(guildTokenAmountInfo.gameObject.activeSelf)
        {
            this.guildTokenAmountText.text = string.Format(
                "{0:#,###0.#}", 
                ServerNetworkManager.Instance.Inventory.GuildToken
            );
        }
    }

	public void RefreshMenuGroup()
	{
        // Check notice reddot
        if(ServerNetworkManager.Instance.NoticeList.Count > 0)
        {
            var latestNoticeTimeSeconds = ServerNetworkManager.Instance.NoticeList[0].TimeSeconds;
            var lastReadNoticeTimeStamp = 0;

            if(PlayerPrefs.HasKey("last_read_notice_time_stamp"))
                lastReadNoticeTimeStamp = PlayerPrefs.GetInt("last_read_notice_time_stamp");
            
            if(latestNoticeTimeSeconds > lastReadNoticeTimeStamp)
                noticeRedDot.gameObject.SetActive(true);
            else
                noticeRedDot.gameObject.SetActive(false);
        }
        else
            noticeRedDot.gameObject.SetActive(false);

        // Check expired post count for notification
        int expiredPostCount = 0;
        int expiredSystemPostCount = 0;

        foreach(var tmp in ServerNetworkManager.Instance.Post.MailList)
		{
			var timeLeft = (int)(tmp.ExpiredTime - DateTimeOffset.Now.ToUnixTimeSeconds());

            if(timeLeft <= 0)
                expiredPostCount++;
        }

        int availablePostCount = ServerNetworkManager.Instance.Post.MailList.Count - expiredPostCount;

        foreach(var tmp in ServerNetworkManager.Instance.SystemPost.MailList)
		{
			var timeLeft = (int)(tmp.ExpiredTime - DateTimeOffset.Now.ToUnixTimeSeconds());

            if(timeLeft <= 0)
                expiredSystemPostCount++;
        }

        int availableSystemPostCount = ServerNetworkManager.Instance.SystemPost.MailList.Count - expiredSystemPostCount;
        
        if(availablePostCount > 0 || availableSystemPostCount > 0)
            alertForInbox.gameObject.SetActive(true);
        else
            alertForInbox.gameObject.SetActive(false);

        // Check available red dot for pets
        petRedDot.gameObject.SetActive(IsRedDotForPet());

        // Check available red dot for growth support
        growthSupportRedDot.gameObject.SetActive(IsRedDotForGrowthSupport());

        // Check available red dot for daily reward
        if(ServerNetworkManager.Instance.Inventory.DailyRewardTicket > 0)
            dailyRewardReddot.gameObject.SetActive(true);
        else
            dailyRewardReddot.gameObject.SetActive(false);

        // Check available free gem
        freeCoinReddot.gameObject.SetActive(RefreshFreeGem());

        // Check event button active
        eventButton.gameObject.SetActive(IsEventButtonAvailable());

        // Check available Event red dot
        eventReddot.gameObject.SetActive(IsRedDotForEvent());

        // Check available tower red dot
        towerReddot.gameObject.SetActive(IsRedDotForTower());

        if(
            noticeRedDot.gameObject.activeSelf 
            || alertForInbox.gameObject.activeSelf
            || petRedDot.gameObject.activeSelf 
            || growthSupportRedDot.gameObject.activeSelf 
            || dailyRewardReddot.gameObject.activeSelf 
            || freeCoinReddot.gameObject.activeSelf
            || eventReddot.gameObject.activeSelf
            || towerReddot.gameObject.activeSelf
        )
            moreRedDot.gameObject.SetActive(true);
        else
            moreRedDot.gameObject.SetActive(false);
	}

    public void RefreshLeftBuffHorizontalBar()
	{
        bool isOnwdBasicPackge = false;

        if(ServerNetworkManager.Instance.PurchasedBasicPackageList.Count > 0)
            isOnwdBasicPackge = true;

        if(isOnwdBasicPackge)
        {
            atkBuffButton.gameObject.SetActive(false);
            atkBuffLeftTimeText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_applied");
            atkBuffValueText.text = string.Format("x{0}", BuffManager.Instance.GetAttackBuffValue(ServerNetworkManager.Instance.User.AttackBuffTier));
            
            goldBuffButton.gameObject.SetActive(false);
            goldBuffValueText.text = string.Format("x{0}", BuffManager.Instance.GetGoldBuffValue(ServerNetworkManager.Instance.User.GoldBuffTier));
            goldBuffLeftTimeText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_applied");
            
            speedBuffButton.gameObject.SetActive(false);
            speedBuffValueText.text = string.Format("x{0}", BuffManager.Instance.GetSpeedBuffValue(ServerNetworkManager.Instance.User.SpeedBuffTier));
            speedBuffLeftTimeText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_applied");
        }
        else
        {
            atkBuffValueText.text = "x10";

            if(BuffManager.Instance.AttackBuffTimeLeftSecond > 0)
            {
                atkBuffButton.gameObject.SetActive(false);
                atkBuffLeftTimeText.text = ConvertedTimeText(BuffManager.Instance.AttackBuffTimeLeftSecond);
            }
            else
            {      
                atkBuffLeftTimeText.text = "";     
                atkBuffButton.gameObject.SetActive(true);
            }

            goldBuffValueText.text = "x10";

            if(BuffManager.Instance.GoldBuffTimeLeftSecond > 0)
            {
                goldBuffButton.gameObject.SetActive(false);
                goldBuffLeftTimeText.text = ConvertedTimeText(BuffManager.Instance.GoldBuffTimeLeftSecond);
            }
            else
            {
                goldBuffLeftTimeText.text = "";
                goldBuffButton.gameObject.SetActive(true);
            }

            speedBuffValueText.text = "x2";

            if(BuffManager.Instance.SpeedBuffTimeLeftSecond > 0)
            {
                speedBuffButton.gameObject.SetActive(false);
                speedBuffLeftTimeText.text = ConvertedTimeText(BuffManager.Instance.SpeedBuffTimeLeftSecond);
            }
            else
            {
                speedBuffLeftTimeText.text = "";
                speedBuffButton.gameObject.SetActive(true);
            }
        }        
    }

    public void RefreshCameraButton()
    {
        if(UIManager.Instance.IsCameraFollowRotation)
        {
            followCameraButtonBackground.color = new Color(1f, 1f, 1f, 1f);
            followCameraButtonIcon.color = new Color(1f, 1f, 1f, 1f);
            followCameraButtonText.color = new Color(1f, 1f, 1f, 1f);
        }
        else
        {
            followCameraButtonBackground.color = new Color(0.6f, 0.6f, 0.6f, 0.7f);
            followCameraButtonIcon.color = new Color(0.6f, 0.6f, 0.6f, 0.7f);
            followCameraButtonText.color = new Color(0.6f, 0.6f, 0.6f, 0.7f);
        }
    }

    public void RefreshAutoButtonStatus()
    {
        if(UIManager.Instance.IsAutoOn)
        {
            autoAttackButtonBackground.color = new Color(1f, 0.7215686f, 0, 1);
            autoAttackButtonText.color = new Color(1f, 0.7215686f, 0, 1);

            if(OptionManager.Instance.IsAutoSkillOn)
            {
                autoAttackButtonText.text = "FULL\nAUTO";
            }
            else
            {
                autoAttackButtonText.text = "AUTO";
            }
        }
        else
        {
            autoAttackButtonBackground.color = new Color(1f, 1f, 1f, 1f);
            autoAttackButtonText.color = new Color(1f, 1f, 1f, 1f);

            autoAttackButtonText.text = "MANUAL";
        }
    }



    public void RefreshRightHorizontalbar()
	{
        // Check available upgrade stat
        upgradeReddot.gameObject.SetActive(IsRedDotForUpgradeStatAndSkill());

        // Check available achievemen
        achievementRedDot.gameObject.SetActive(IsRedDotForAchievement());

        // Check available red dot for weapon and class
        upgradeEquipmentReddot.gameObject.SetActive(IsRedDotForUpgradeEquipment());
    }

    public bool RefreshFreeGem()
    {
        bool isAvailable = true;

        if(ServerNetworkManager.Instance.FreeGemDailyLeftLimit > 0)
        {
            var freeGemAdLeftTime = ServerNetworkManager.Instance.NextFreeGemAdsTimeStamp - DateTimeOffset.Now.ToUnixTimeSeconds();
            
            if(freeGemAdLeftTime > 0)
                isAvailable = false;

            freeCoinButton.gameObject.GetComponent<Button>().interactable = isAvailable;
            
            if(isAvailable)
                freeCoinText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "free_gem").ToUpper();
            else
                freeCoinText.text = ConvertedTimeText(Convert.ToInt32(freeGemAdLeftTime));
            
        }
        else
        {
            isAvailable = false;
            freeCoinButton.gameObject.GetComponent<Button>().interactable = isAvailable;
            freeCoinText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "free_gem").ToUpper();
        }

        return isAvailable;
    }

    public void RefreshBoss()
    {
        bossIconBackground.fillAmount = IngameMainController.Instance.bossCooldownPassed / IngameMainController.Instance.bossCooldown;
    }

    float previousBerserkPoint = -1f;
    BigInteger previousBerserkGold = -1;
    public void RefreshBerserk()
    {
        var currentBerserkPoint = InterSceneManager.Instance.BerserkPoint;
        if(previousBerserkPoint != currentBerserkPoint)
        {
            berserkText.text = string.Format("{0:0.00}%", currentBerserkPoint / NumberMainController.Instance.FinalBerserkTargetPoint * 100f);
            berserkSlider.value = currentBerserkPoint / NumberMainController.Instance.FinalBerserkTargetPoint;

            if(!IngameMainController.Instance.IsBerserk)
                beserkSliderFill.sprite = berserkSliderSpriteList[0];
            else
                beserkSliderFill.sprite = berserkSliderSpriteList[1];
        }
        previousBerserkPoint = currentBerserkPoint;

        var currentBerserkGold = IngameMainController.Instance.GoldByThisBerserk;
       
        previousBerserkGold = currentBerserkGold;

        if(IngameMainController.Instance.IsBerserk)
            berserkNameText.gameObject.SetActive(true);
        else
            berserkNameText.gameObject.SetActive(false);
    }

    public void RefreshSkillButtons()
    {
        bool isSkill1Unlock = (ServerNetworkManager.Instance.User.SkillInfo[0].Level > 0);
        bool isSkill2Unlock = (ServerNetworkManager.Instance.User.SkillInfo[1].Level > 0);
        bool isSkill3Unlock = (ServerNetworkManager.Instance.User.SkillInfo[2].Level > 0);

        skill1Button.interactable = isSkill1Unlock;
        skill1ButtonImage.gameObject.SetActive(isSkill1Unlock);
        skill1LockImage.gameObject.SetActive(!isSkill1Unlock);
        
        skill2Button.interactable = isSkill2Unlock;
        skill2ButtonImage.gameObject.SetActive(isSkill2Unlock);
        skill2LockImage.gameObject.SetActive(!isSkill2Unlock);

        skill3Button.interactable = isSkill3Unlock;
        skill3ButtonImage.gameObject.SetActive(isSkill3Unlock);
        skill3LockImage.gameObject.SetActive(!isSkill3Unlock);
    }

    public void RefreshSkillCooldown()
    {
        skill1ButtonImage.fillAmount = IngameMainController.Instance.HeroIdentity.skill1Cooldown;
        if(skill1ButtonImage.fillAmount != 1f)
            skill1ButtonImage.color = new Color(1f, 1f, 1f, 0.5f);
        else
            skill1ButtonImage.color = new Color(1f, 1f, 1f, 1f);
        skill2ButtonImage.fillAmount = IngameMainController.Instance.HeroIdentity.skill2Cooldown;
        if(skill2ButtonImage.fillAmount != 1f)
            skill2ButtonImage.color = new Color(1f, 1f, 1f, 0.5f);
        else
            skill2ButtonImage.color = new Color(1f, 1f, 1f, 1f);
        skill3ButtonImage.fillAmount = IngameMainController.Instance.HeroIdentity.skill3Cooldown;
        if(skill3ButtonImage.fillAmount != 1f)
            skill3ButtonImage.color = new Color(1f, 1f, 1f, 0.5f);
        else
            skill3ButtonImage.color = new Color(1f, 1f, 1f, 1f);
    }

    private int previousWorldIdx = -1;
    private int previousMobIdx = -1;
    private Coroutine newStageAnimationCoroutine = null;
    public void RefreshCurrentStageInfo()
    {
        int currentWorldIdx = (ServerNetworkManager.Instance.User.SelectedStage - 1) / ServerNetworkManager.Instance.Setting.MaxStageInWorld + 1;
        int currentMobIdx = (ServerNetworkManager.Instance.User.SelectedStage - 1) % ServerNetworkManager.Instance.Setting.MaxStageInWorld + 1;

        string worldInfo = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "world_name"), currentWorldIdx);
        string stageInfo = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "stage_name"), currentMobIdx);

        currentStageInfoText.text = $"{worldInfo}\n{stageInfo}";
        newStageInfoText.text = $"{worldInfo}\n{stageInfo}";

        if(previousWorldIdx != currentWorldIdx || currentMobIdx != previousMobIdx)
        {
            if(newStageAnimationCoroutine != null)
                StopCoroutine(newStageAnimationCoroutine);
                
            newStageAnimationCoroutine = StartCoroutine(NewStageAnimateCoroutine());
        }

        previousWorldIdx = currentWorldIdx;
        previousMobIdx = currentMobIdx;

        if(ServerNetworkManager.Instance.User.SelectedStage < ServerNetworkManager.Instance.Setting.MaxStageInWorld * ServerNetworkManager.Instance.Setting.LastWorldIndex)
            bossButton.interactable = true;
        else
            bossButton.interactable = false;
    }

    private IEnumerator NewStageAnimateCoroutine()
    {
        newStageInfoText.CrossFadeAlpha(1f, 0f, false);
        yield return new WaitForSeconds(2f);
        newStageInfoText.CrossFadeAlpha(0f, 1.5f, false);
    }

#endregion

#region Back Blocker MethodBase

    public void ShowBackBlocker()
    {
        backBlocker.gameObject.SetActive(true);
    }

    public void HideBackBlocker()
    {
        backBlocker.gameObject.SetActive(false);
    }

#endregion

#region Game Play Button Callbacks

    public void OnMoveAreaTouch()
    {
        // Check touch move option
        if(OptionManager.Instance.IsTouchMoveOn)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, layerOfBackground.value))
            {
                IngameMainController.Instance.ReadyForMove(hit.point);
            }
        }
    }

    public void OnAttackButtonClicked()
    {
        IngameMainController.Instance.ReadyForAttack();
    }

    public void OnSkillButtonClicked(int skillIndex)
    {
        IngameMainController.Instance.ReadyForSkill(skillIndex);
    }

    public void OnAutoAttackButtonClicked()
    {
        // In case the hero was moving
        if(!UIManager.Instance.IsAutoOn)
            IngameMainController.Instance.StopMove();

        if(UIManager.Instance.IsAutoOn)
        {
            if(OptionManager.Instance.IsAutoSkillOn)
            {
                UIManager.Instance.IsAutoOn = false;
                OptionManager.Instance.IsAutoSkillOn = false;
                PlayerPrefs.SetInt("ui_auto_attack", 0);
            }
            else
            {
                UIManager.Instance.IsAutoOn = true;
                OptionManager.Instance.IsAutoSkillOn = true;
                PlayerPrefs.SetInt("ui_auto_attack", 1);
            }
        }
        else
        {
            UIManager.Instance.IsAutoOn = true;
            OptionManager.Instance.IsAutoSkillOn = false;
            PlayerPrefs.SetInt("ui_auto_attack", 2);
        }

        UIManager.Instance.IsTouchedForAttackRecently = false;
        UIManager.Instance.IsTouchedForMoveRecently = false;

        RefreshAutoButtonStatus();
    }

    public void OnCameraFollowButtonClicked()
    {
        UIManager.Instance.IsCameraFollowRotation = !UIManager.Instance.IsCameraFollowRotation;

        if(UIManager.Instance.IsCameraFollowRotation)
            PlayerPrefs.SetInt("ui_camera_follow_off", 0);
        else
            PlayerPrefs.SetInt("ui_camera_follow_off", 1);

        RefreshCameraButton();

        MiniTutorialController.Instance.NeedTutorial3Stop = true;
    }

    public void OnPowerSavingModeButtonClicked()
	{
		OutgameController.Instance.ShowPowerSavingCanvas();
	}

#endregion

#region Buff Button Callbacks

    public void OnAdATkBuffButtonClicked()
    {
        OutgameController.Instance.ShowBuffAds(BuffType.ATK);
        PlayerPrefs.SetInt("buff_ever", 1);
        MiniTutorialController.Instance.NeedTutorial2Stop = true;
    }

    public void OnAdGoldBuffButtonClicked()
    {
        OutgameController.Instance.ShowBuffAds(BuffType.GOLD);
        PlayerPrefs.SetInt("buff_ever", 1);
        MiniTutorialController.Instance.NeedTutorial2Stop = true;
    }

    public void OnAdSpeedBuffButtonClicked()
    {
        OutgameController.Instance.ShowBuffAds(BuffType.SPEED);
        PlayerPrefs.SetInt("buff_ever", 1);
        MiniTutorialController.Instance.NeedTutorial2Stop = true;
    }

#endregion

#region Right Horizontal Button Callbacks
    
    public void OnShopButtonClicked(int tabId)
	{
        var shop = GameObject.FindObjectOfType<UIPopupShop>();
        if(shop != null && shop.gameObject.activeSelf)
    	    shop.OnTabButtonClicked(tabId);
        else
            OutgameController.Instance.ShowShopPopup(tabId);
	}

    public void OnShopButtonClicked(int tabId, bool showLastItems)
	{
		var shop = GameObject.FindObjectOfType<UIPopupShop>();
        if(shop != null && shop.gameObject.activeSelf)
    	    shop.OnTabButtonClicked(tabId);
        else
            OutgameController.Instance.ShowShopPopup(tabId, showLastItems);
	}

    public void OnRaidTicketButtonClicked()
	{
        OutgameController.Instance.ShowRaidTicketPurchasePopup();
	}

    public void OnAchievementButtonClicked()
	{
		OutgameController.Instance.ShowAchievementPopup();
	}

    public void OnEventButtonClicked()
	{
		OutgameController.Instance.ShowAttendancePopup();
	}

    public void OnMoreMenuButtonClicked()
    {
        moreButton.gameObject.SetActive(false);
        closeMoreButton.gameObject.SetActive(true);
        moreMenuGroup.gameObject.SetActive(true);
        currentStageInfoText.gameObject.SetActive(false);

        eventButton.gameObject.SetActive(IsEventButtonAvailable());
    }

    public void OnMoreMenuCloseButtonClicked()
    {
        moreButton.gameObject.SetActive(true);
        closeMoreButton.gameObject.SetActive(false);
        moreMenuGroup.gameObject.SetActive(false);
        currentStageInfoText.gameObject.SetActive(true);
    }

#endregion

#region More Menu Button Callbacks
    public void OnNoticeButtonClicked()
    {
        OutgameController.Instance.ShowNoticePopup();        
    }

    public void OnInboxButtonClicked()
    {
        HideRightTopContent();
        OutgameController.Instance.ShowInboxPopup();
    }

    public void OnUserInfoButtonClicked()
    {
        OutgameController.Instance.ShowStatInfoPopup();
    }

    public void OnOptionButtonClicked()
	{
        HideRightTopContent();
		OutgameController.Instance.ShowOptionPopup();
	}

    public void OnRankingButtonClicked()
    {
        OutgameController.Instance.ShowRankingPopup();
    }

    public void OnUpgradeButtonClicked()
    {
        HideRightTopContent();
        OutgameController.Instance.ShowUpgradePopup();
    }

    public void OnSummonButtonClicked()
    {
        HideRightTopContent();
        OutgameController.Instance.ShowSummon2Popup();
    }

    public void OnEquipmentButtonClicked()
    {
        HideRightTopContent();
        OutgameController.Instance.ShowEquipmentPopup();
    }

    public void OnPetButtonClicked()
    {
        HideRightTopContent();
        OutgameController.Instance.ShowPetPopup();
    }

    public void OnWorldInfoButonClicked()
    {
        OutgameController.Instance.ShowWorldInfoPopup();
    }

    public void OnArenaButtonClicked()
    {
        var passedMilliSeconds = DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastArenaClickTimeStampMilliseconds;

		if(passedMilliSeconds >= 500d)
		{
            lastArenaClickTimeStampMilliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();
            
            HideRightTopContent();
            OutgameController.Instance.ShowArenaPopup();   
        }
    }

    public void OnRaidButtonClicked()
    {
        var passedMilliSeconds = DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastRaidClickTimeStampMilliseconds;

		if(passedMilliSeconds >= 500d)
		{
            lastRaidClickTimeStampMilliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();

            if(ServerNetworkManager.Instance.User.StageProgress < ServerNetworkManager.Instance.Setting.RaidOpenStageProgress)
            {
                // For need clear stage info
                int raidOpenStageProgress = ServerNetworkManager.Instance.Setting.RaidOpenStageProgress - 1;

                var needClearWorldIdx = (raidOpenStageProgress - 1) / ServerNetworkManager.Instance.Setting.MaxStageInWorld + 1;
                var needClearMobIdx = (raidOpenStageProgress - 1) % ServerNetworkManager.Instance.Setting.MaxStageInWorld + 1;
                var text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_need_stage_progress"), needClearWorldIdx, needClearMobIdx);

                UIManager.Instance.ShowAlert(text, null, null);
            }
            else
            {
                HideRightTopContent();
                OutgameController.Instance.EnterRaid();  
            }
        }
    }

    public void OnGuildButtonClicked()
    {
        bool isGuildOpen = false;
        
#if UNITY_IPHONE && !UNITY_EDITOR
		isGuildOpen = ServerNetworkManager.Instance.Setting.IsGuildiOSOpen;
#else
        isGuildOpen = ServerNetworkManager.Instance.Setting.IsGuildAndroidOpen;
#endif

        if(isGuildOpen)
        {
            var passedMilliSeconds = DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastGuildClickTimeStampMilliseconds;

            if(passedMilliSeconds >= 500d)
            {
                lastGuildClickTimeStampMilliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();

                if(ServerNetworkManager.Instance.User.StageProgress < ServerNetworkManager.Instance.Setting.GuildOpenStageProgress)
                {
                    // For need clear stage info
                    int guildOpenStageProgress = ServerNetworkManager.Instance.Setting.GuildOpenStageProgress - 1;

                    var needClearWorldIdx = (guildOpenStageProgress - 1) / ServerNetworkManager.Instance.Setting.MaxStageInWorld + 1;
                    var needClearMobIdx = (guildOpenStageProgress - 1) % ServerNetworkManager.Instance.Setting.MaxStageInWorld + 1;
                    var text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_need_stage_progress"), needClearWorldIdx, needClearMobIdx);

                    UIManager.Instance.ShowAlert(text, null, null);
                }
                else
                    OutgameController.Instance.ShowGuildPopup();
            }
        }
        else
        {
            UIManager.Instance.ShowAlertLocalized("message_coming_soon", null, null);
        }
    }

    public void OnGrowthSupportButtonClicked()
    {
        OutgameController.Instance.ShowGrowthSupportPopup();        
    }

    public void OnFreeCoinButtonClicked()
	{
        var passedMilliSeconds = DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastFreeGemClickTimeStampMilliseconds;

		if(passedMilliSeconds >= 500d)
		{
            lastFreeGemClickTimeStampMilliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();
		    OutgameController.Instance.ShowFreeGemPopup();
        }
	}

    public void OnTowerButtonClicked()
    {        
        bool isTowerOpen = false;
        
        #if UNITY_IPHONE
		    isTowerOpen = ServerNetworkManager.Instance.Setting.IsToweriOSOpen;
        #else
            isTowerOpen = ServerNetworkManager.Instance.Setting.IsTowerAndroidOpen;
        #endif

        if(isTowerOpen)
        {
            if(ServerNetworkManager.Instance.User.StageProgress < ServerNetworkManager.Instance.Setting.TowerOpenStageProgress)
            {
                // For need clear stage info
                int towerOpenStageProgress = ServerNetworkManager.Instance.Setting.TowerOpenStageProgress - 1;

                var needClearWorldIdx = (towerOpenStageProgress - 1) / ServerNetworkManager.Instance.Setting.MaxStageInWorld + 1;
                var needClearMobIdx = (towerOpenStageProgress - 1) % ServerNetworkManager.Instance.Setting.MaxStageInWorld + 1;
                var text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_need_stage_progress"), needClearWorldIdx, needClearMobIdx);

                UIManager.Instance.ShowAlert(text, null, null);
            }
            else
            {
                HideRightTopContent();
                OutgameController.Instance.ShowTowerPopup();  
            }
        }
        else
        {
            UIManager.Instance.ShowAlertLocalized("message_coming_soon", null, null);
        }
    }

    public void OnWingButtonClicked()
    {
        bool isWingOpen = false;
        
#if UNITY_IPHONE && !UNITY_EDITOR
		    isWingOpen = ServerNetworkManager.Instance.Setting.IsWingiOSOpen;
#else
            isWingOpen = ServerNetworkManager.Instance.Setting.IsWingAndroidOpen;
#endif

        if(isWingOpen)
        {
            if(ServerNetworkManager.Instance.User.StageProgress < ServerNetworkManager.Instance.Setting.WingOpenStageProgress)
            {
                // For need clear stage info
                int wingOpenStageProgress = ServerNetworkManager.Instance.Setting.WingOpenStageProgress - 1;

                var needClearWorldIdx = (wingOpenStageProgress - 1) / ServerNetworkManager.Instance.Setting.MaxStageInWorld + 1;
                var needClearMobIdx = (wingOpenStageProgress - 1) % ServerNetworkManager.Instance.Setting.MaxStageInWorld + 1;
                var text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_need_stage_progress"), needClearWorldIdx, needClearMobIdx);

                UIManager.Instance.ShowAlert(text, null, null);
            }
            else
            {
                var timeLeft = Mathf.Max(2f + OutgameController.Instance.TimeSyncPlayerData - (int)Time.realtimeSinceStartup, 0);

                if(timeLeft > 0)
                    OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "network_cooldown"), timeLeft));
                else
                {
                    HideRightTopContent();
                    OutgameController.Instance.ShowWingPopup();  
                }
            }
        }
        else
        {
            UIManager.Instance.ShowAlertLocalized("message_coming_soon", null, null);
        }
    }

#endregion

#region Other Button Callbacks

    public void OnChatButtonClicked()
    {
        OutgameController.Instance.ChatOnOff();
    }

    public void OnDailyRewardButtonClicked()
	{
		OutgameController.Instance.ShowDailyRewardPopup();
	}

    public void OnBossButonClicked()
    {
        IngameMainController.Instance.StartBossMode();
        MiniTutorialController.Instance.NeedTutorial1Stop = true;
    }

#endregion

#region Util

    public void SetRaidTicketAmountInfoActive(bool value)
    {
        if(value)
        {
            this.raidTicketAmountText.text = string.Format(
                "{0:#,###0.#}", 
                ServerNetworkManager.Instance.Inventory.TicketRaid
            );
        }

        raidTicketAmountInfo.gameObject.SetActive(value);
    }

    public void SetHeartAmountInfoActive(bool value)
    {
        if(value)
        {
            this.heartAmountText.text = LanguageManager.Instance.NumberToString(
                ServerNetworkManager.Instance.Inventory.Heart
            );
        }

        heartAmountInfo.gameObject.SetActive(value);
    }

    public void SetFeatherAmountInfoActive(bool value)
    {
        if(value)
        {
            this.featherAmountText.text = string.Format(
                "{0:#,###0.#}", 
                ServerNetworkManager.Instance.Inventory.Feather
            );
        }

        featherAmountInfo.gameObject.SetActive(value);
    }

    public void SetGuildTokenAmountInfoActive(bool value)
    {
        if(guildTokenAmountInfo.gameObject.activeSelf == value)
            return;
            
        if(value)
        {
            this.guildTokenAmountText.text = string.Format(
                "{0:#,###0.#}", 
                ServerNetworkManager.Instance.Inventory.GuildToken
            );
        }

        guildTokenAmountInfo.gameObject.SetActive(value);
    }

    public void SetChatButtonActive(bool value)
    {
        chatButton.gameObject.GetComponentInChildren<Button>().interactable = value;
    }
    
    public void HideRightTopContent()
    {
        if(!horizontalMenuList.gameObject.activeSelf)
            return;

        originalMoreButtonActiveValue = moreButton.gameObject.activeSelf;
        originalCloseMoreButtonActiveValue = closeMoreButton.gameObject.activeSelf;
        originalMoreMenuGroupActiveValue = moreMenuGroup.gameObject.activeSelf;

        horizontalMenuList.gameObject.SetActive(false);

        moreButton.gameObject.SetActive(false);
        closeMoreButton.gameObject.SetActive(false);
        moreMenuGroup.gameObject.SetActive(false);
    }

    public void ShowRightTopContent()
    {
        horizontalMenuList.gameObject.SetActive(true);

        moreButton.gameObject.SetActive(originalMoreButtonActiveValue);
        closeMoreButton.gameObject.SetActive(originalCloseMoreButtonActiveValue);
        moreMenuGroup.gameObject.SetActive(originalMoreMenuGroupActiveValue);
    }

    public bool IsRedDotForEvent()
    {
        bool isGetEventRewardAvailable = true;

        if(this.IsEventButtonAvailable())
        {
            // Check time stamp day
            var currentTimeStamp = DateTimeOffset.Now.ToUnixTimeSeconds();
            var currentTimeStampDay = currentTimeStamp / (60 * 60 * 24);

            if(currentTimeStampDay <= ServerNetworkManager.Instance.LastAttendanceTimeStampDay)
                isGetEventRewardAvailable = false;
            else
            {
                var attendanceEventInfo = ServerNetworkManager.Instance.AttendanceEventInfo;

                if(attendanceEventInfo == null)
                    isGetEventRewardAvailable = false;
            }
        }
        else
            isGetEventRewardAvailable = false;

        return isGetEventRewardAvailable;
    }

    public bool IsEventButtonAvailable()
    {
        // Check available event button
        var isEventAvailable = false;
        var attendanceEventInfo = ServerNetworkManager.Instance.AttendanceEventInfo;

        if(attendanceEventInfo != null)
        {
            var currentTimeStamp = DateTimeOffset.Now.ToUnixTimeSeconds();
            var currentTimeStampDay = currentTimeStamp / (60 * 60 * 24);
            
            // Check event day vaildate
            if(currentTimeStampDay >= attendanceEventInfo.StartTimeStampDay && currentTimeStampDay <= attendanceEventInfo.EndTimeStampDay)
                isEventAvailable = true;
        }

        return isEventAvailable;
    }

    private bool IsRedDotForUpgradeStatAndSkill()
    {
        bool isUpgradeAvailable = false;
        BigInteger currentGold = ServerNetworkManager.Instance.Inventory.Gold;
        
        if(currentGold >= ServerNetworkManager.Instance.User.Upgrade.BaseAttackLevelupPrice && ServerNetworkManager.Instance.User.Upgrade.BaseAttackLevelupPrice != -1 && ServerNetworkManager.Instance.User.Upgrade.BaseAttackLevel < ServerNetworkManager.Instance.Setting.MaxStatLevel)
            isUpgradeAvailable = true;
        else if(currentGold >= ServerNetworkManager.Instance.User.Upgrade.CriticalAttackLevelupPrice && ServerNetworkManager.Instance.User.Upgrade.CriticalAttackLevelupPrice != -1 && ServerNetworkManager.Instance.User.Upgrade.CriticalAttackLevel < ServerNetworkManager.Instance.Setting.MaxStatLevel)
            isUpgradeAvailable = true;
        else if(currentGold >= ServerNetworkManager.Instance.User.Upgrade.CriticalChanceLevelupPrice && ServerNetworkManager.Instance.User.Upgrade.CriticalChanceLevelupPrice != -1 && ServerNetworkManager.Instance.User.Upgrade.CriticalChance < 1f)
            isUpgradeAvailable = true;
        else if(currentGold >= ServerNetworkManager.Instance.User.Upgrade.SuperAttackLevelupPrice && ServerNetworkManager.Instance.User.Upgrade.SuperAttackLevelupPrice != -1 && ServerNetworkManager.Instance.User.Upgrade.SuperAttackLevel < ServerNetworkManager.Instance.Setting.MaxStatLevel)
            isUpgradeAvailable = true;
        else if(currentGold >= ServerNetworkManager.Instance.User.Upgrade.SuperChanceLevelupPrice && ServerNetworkManager.Instance.User.Upgrade.SuperChanceLevelupPrice != -1 && ServerNetworkManager.Instance.User.Upgrade.SuperChance < 1f)
            isUpgradeAvailable = true;
        else if(ServerNetworkManager.Instance.User.Upgrade.SuperChance >= 1f && currentGold >= ServerNetworkManager.Instance.User.Upgrade.UltimateAttackLevelupPrice && ServerNetworkManager.Instance.User.Upgrade.UltimateAttackLevelupPrice != -1 && ServerNetworkManager.Instance.User.Upgrade.UltimateAttackLevel < ServerNetworkManager.Instance.Setting.MaxStatLevel)
            isUpgradeAvailable = true;
        else if(ServerNetworkManager.Instance.User.Upgrade.SuperChance >= 1f && currentGold >= ServerNetworkManager.Instance.User.Upgrade.UltimateChanceLevelupPrice && ServerNetworkManager.Instance.User.Upgrade.UltimateChanceLevelupPrice != -1 && ServerNetworkManager.Instance.User.Upgrade.UltimateChance < 1f)
            isUpgradeAvailable = true;
        else if(ServerNetworkManager.Instance.Setting.IsHyperOpen && ServerNetworkManager.Instance.User.Upgrade.UltimateChance >= 1f && currentGold >= ServerNetworkManager.Instance.User.Upgrade.HyperAttackLevelupPrice && ServerNetworkManager.Instance.User.Upgrade.HyperAttackLevelupPrice != -1 && ServerNetworkManager.Instance.User.Upgrade.HyperAttackLevel < ServerNetworkManager.Instance.Setting.MaxStatLevel)
            isUpgradeAvailable = true;
        else if(ServerNetworkManager.Instance.Setting.IsHyperOpen && ServerNetworkManager.Instance.User.Upgrade.UltimateChance >= 1f && currentGold >= ServerNetworkManager.Instance.User.Upgrade.HyperChanceLevelupPrice && ServerNetworkManager.Instance.User.Upgrade.HyperChanceLevelupPrice != -1 && ServerNetworkManager.Instance.User.Upgrade.HyperChance < 1f)
            isUpgradeAvailable = true;
        else if(currentGold >= ServerNetworkManager.Instance.User.SkillInfo[0].GoldLevelupCost && ServerNetworkManager.Instance.User.SkillInfo[0].GoldLevelupCost != -1 && ServerNetworkManager.Instance.User.SkillInfo[0].Level < ServerNetworkManager.Instance.Setting.MaxSkillLevel)
            isUpgradeAvailable = true;
        else if(currentGold >= ServerNetworkManager.Instance.User.SkillInfo[1].GoldLevelupCost && ServerNetworkManager.Instance.User.SkillInfo[1].GoldLevelupCost != -1 && ServerNetworkManager.Instance.User.SkillInfo[1].Level < ServerNetworkManager.Instance.Setting.MaxSkillLevel)
            isUpgradeAvailable = true;
        else if(currentGold >= ServerNetworkManager.Instance.User.SkillInfo[2].GoldLevelupCost && ServerNetworkManager.Instance.User.SkillInfo[2].GoldLevelupCost != -1 && ServerNetworkManager.Instance.User.SkillInfo[2].Level < ServerNetworkManager.Instance.Setting.MaxSkillLevel)
            isUpgradeAvailable = true;

        return isUpgradeAvailable;
    }

    private bool IsRedDotForUpgradeEquipment()
    {
        // Check weapon
        foreach(var tmp in ServerNetworkManager.Instance.Inventory.WeaponList)
        {
            if(tmp.IsNew)
                return true;
        }

        // Check class
        foreach(var tmp in ServerNetworkManager.Instance.Inventory.ClassList)
        {
            if(tmp.IsNew)
                return true;
        }

        // Check mercenary
        foreach(var tmp in ServerNetworkManager.Instance.Inventory.MercenaryList)
        {
            if(tmp.IsNew)
                return true;
        }

        // Check Relic
        foreach(var tmp in ServerNetworkManager.Instance.Inventory.RelicList)
        {
            // Check available upgrade
            if(!tmp.IsMaxLevel)
            {
                if((tmp.Count - 1) > 0)
                    return true;
            }

            if(tmp.IsNew)
                return true;
        }

        // Check Legendary Relic
        foreach(var tmp in ServerNetworkManager.Instance.Inventory.LegendaryRelicList)
        {
            // Check available upgrade
            if(!tmp.IsMaxLevel)
            {
                if((tmp.Count - 1) > 0)
                    return true;
            }

            if(tmp.IsNew)
                return true;
        }

        return false;
    }

    private bool IsRedDotForPet()
    {
        foreach(var tmp in ServerNetworkManager.Instance.Inventory.PetList)
        {
            if(tmp.IsNew)
                return true;
        }

        return false;
    }

    private bool IsRedDotForGrowthSupport()
    {
        bool isAvailableGrowthSupportFree = false;
        bool isAvailableGrowthSupport1 = false;
		bool isAvailableGrowthSupport2 = false;

        int maxStagePrgoress = Mathf.Min(ServerNetworkManager.Instance.User.StageProgress, ServerNetworkManager.Instance.Setting.MaxGrowthSupportStage);

        for(int i = 10; i <= maxStagePrgoress; i+= 10)
		{   
            // Check user already gain free reward or not
            string growthSupportRewardId = string.Format("growth_support_00_{0}", i);

            if(!ServerNetworkManager.Instance.ReceivedGrowthSupportFreeRewardList.Exists(n => n == growthSupportRewardId))
            {
                isAvailableGrowthSupportFree = true;
                break;
            }

			// Check user have growth support package 1 or not
			if(ServerNetworkManager.Instance.PurchasedGrowthPackageList.Exists(n => n == "bundle_package_growth_1"))
			{
				// Check user already gain reward or not
				growthSupportRewardId = string.Format("growth_support_01_{0}", i);

				if(!ServerNetworkManager.Instance.ReceivedGrowthSupportFirstRewardList.Exists(n => n == growthSupportRewardId))
				{
                    isAvailableGrowthSupport1 = true;
                    break;
                }
			}

			// Check user have growth support package 2 or not
			if(ServerNetworkManager.Instance.PurchasedGrowthPackageList.Exists(n => n == "bundle_package_growth_2"))
			{
				// Check user already gain reward or not
				growthSupportRewardId = string.Format("growth_support_02_{0}", i);

				if(!ServerNetworkManager.Instance.ReceivedGrowthSupportSecondRewardList.Exists(n => n == growthSupportRewardId))
                {
                    isAvailableGrowthSupport2 = true;
                    break;
                }
			}
		}

        if(isAvailableGrowthSupportFree || isAvailableGrowthSupport1 || isAvailableGrowthSupport2)
            return true;
        else
            return false;
    }

    private bool IsRedDotForAchievement()
    {
        bool isAvailableAchievement = false;
        
        foreach(var tmp in ServerNetworkManager.Instance.User.AchievementInfo)
        {
            if(tmp.Count >= tmp.Goal && !tmp.IsMaxLevel)
            {
                isAvailableAchievement = true;
                break;
            }
        }

        return isAvailableAchievement;
    }

    private bool IsRedDotForTower()
    {
        bool isNotify = false;

        var myTowerData = ServerNetworkManager.Instance.MyTowerData;
        if(myTowerData != null)
        {
            if(myTowerData.IsCompleted)
                isNotify = true;
            else
            {
                if(myTowerData.BattleLog.Count > 0 && 
                OutgameController.Instance.LastCloseTowerPopupTimeStampMinute < myTowerData.BattleLog[myTowerData.BattleLog.Count - 1].TimeStamp) 
                    isNotify = true;  
            }
        }

        return isNotify;
    }

    private string ConvertedTimeText(int timeLeft)
	{
		int minutes = Mathf.FloorToInt (timeLeft / 60);
		timeLeft -= minutes * 60;
		int seconds = Mathf.FloorToInt (timeLeft);
		
		// For time format
		string minutesText = minutes.ToString();
		string secondsText = seconds.ToString();

		if(minutes < 10)
		{
			minutesText = "0" + minutes.ToString();
		}
		if(seconds < 10)
		{
			secondsText = "0" + seconds.ToString();
		}
		
		return string.Format("{0}:{1}", minutesText, secondsText);
	}

#endregion

}
