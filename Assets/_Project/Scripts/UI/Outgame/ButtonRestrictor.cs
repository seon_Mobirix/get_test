﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class ButtonRestrictor : MonoBehaviour
{
    public int requiredStageProgress = 1;

    public UnityEvent onClick;

    private Color originalNormaColor = Color.black;
    private Color originalPressedColor = Color.black;

    [SerializeField]
    private List<Graphic> additionalItemsToChange = new List<Graphic>();

    [SerializeField]
    private bool blockWhenBossIsShowing = true;

    private void Start()
    {
        originalNormaColor = this.GetComponent<Button>().colors.normalColor;
        originalPressedColor = this.GetComponent<Button>().colors.pressedColor;

        Refresh();
    }

    private void Update()
    {
        Refresh();
    }

    private int previousLevel = -1;    
    private void Refresh()
    {
        if(previousLevel != ServerNetworkManager.Instance.User.StageProgress)
        {
            if(ServerNetworkManager.Instance.User.StageProgress >= requiredStageProgress)
            {
                var colors = this.GetComponent<Button>().colors;
                colors.normalColor = originalNormaColor;
                colors.highlightedColor = originalNormaColor;
                colors.selectedColor = originalNormaColor;
                this.GetComponent<Button>().colors = colors;
                
                foreach(var tmp in additionalItemsToChange)
                    tmp.color = originalNormaColor;

            }
            else
            {
                var colors = this.GetComponent<Button>().colors;
                colors.normalColor = originalPressedColor;
                colors.highlightedColor = originalPressedColor;
                colors.selectedColor = originalPressedColor;
                this.GetComponent<Button>().colors = colors;

                foreach(var tmp in additionalItemsToChange)
                    tmp.color = originalPressedColor;
            }
        }
        previousLevel = ServerNetworkManager.Instance.User.StageProgress;
    }

    public void OnClick()
    {
        if(blockWhenBossIsShowing && UIManager.Instance.NeedUIForBoss)
        {
            OutgameController.Instance.ShowToast(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_boss_is_showing"));
        }
        else if(ServerNetworkManager.Instance.User.StageProgress >= requiredStageProgress)
        {
            foreach(var tmp in this.gameObject.GetComponentsInChildren<MonoBehaviour>())
            {
                if(tmp.name.Contains("Tutorial"))
                {
                    tmp.gameObject.SetActive(false);
                }
            }
            onClick.Invoke();
        }
        else
        {
            OutgameController.Instance.ShowToast(string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_avail_after_level"), requiredStageProgress));
        }    
    }
}