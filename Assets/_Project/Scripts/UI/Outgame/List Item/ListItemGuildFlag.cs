﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class ListItemGuildFlag : MonoBehaviour
{
    [SerializeField]
	private Image flagIcon;
    [SerializeField]
	private List<Sprite> flagIconList;
    [SerializeField]
    private Transform selectedIcon;

    private int flagIndex;

    public void UpdateEntity(int flagIndex, bool isSelected)
	{
        this.flagIndex = flagIndex;

        flagIcon.sprite = flagIconList[flagIndex];
        selectedIcon.gameObject.SetActive(isSelected);
    }

    public void OnButtonClicked()
    {
		if(GameObject.FindObjectOfType<UIPopupGuildCreate>() != null && GameObject.FindObjectOfType<UIPopupGuildCreate>().gameObject.activeSelf)
            GameObject.FindObjectOfType<UIPopupGuildCreate>().RefreshSelectedGuildFlag(flagIndex);
        
		if(GameObject.FindObjectOfType<UIPopupGuildSetting>() != null && GameObject.FindObjectOfType<UIPopupGuildSetting>().gameObject.activeSelf)
            GameObject.FindObjectOfType<UIPopupGuildSetting>().RefreshSelectedGuildFlag(flagIndex);
    }    
}