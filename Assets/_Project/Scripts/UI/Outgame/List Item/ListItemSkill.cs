﻿using UnityEngine;
using UnityEngine.UI;
using System.Numerics;
using System.Linq;
using System.Collections.Generic;

public class ListItemSkill : MonoBehaviour
{
    [SerializeField]
	private Image skillIcon;
    [SerializeField]
	private List<Sprite> skillIconList;
    [SerializeField]
	private Text skillNameAndLevelText;
    [SerializeField]
	private Text skillValueText;
    [SerializeField]
	private Text requireGoldText;
    [SerializeField]
    private Text upgradeButtonText;
    [SerializeField]
    private Button upgradeButton;
    [SerializeField]
    private Transform lockPanel;

    private int skillIndex;
    private bool isAvailableUpgrade = true;

    private bool isPressed = false;
    private bool isPointUpActionEnabled = false;
    private float lastTime = 0;

    private void Update()
    {
        if(isPressed && (Time.realtimeSinceStartup - lastTime) > 0.1f)
        {
            if(isAvailableUpgrade)
                GameObject.FindObjectOfType<UIPopupUpgrade>().RequestSkillUpgrade(skillIndex);
            lastTime = Time.realtimeSinceStartup;
            isPointUpActionEnabled = false;
        }
    }

    public void UpdateEntity(int skillIndex, bool isShowUpgradeEffect)
	{
        this.skillIndex = skillIndex;
        skillIcon.sprite = skillIconList[skillIndex];
        
        BigInteger requiredGold = ServerNetworkManager.Instance.User.SkillInfo[skillIndex].GoldLevelupCost;
        bool isLock = (ServerNetworkManager.Instance.User.SkillInfo[skillIndex].Level == 0);
        int skillLevel = Mathf.Max(1, ServerNetworkManager.Instance.User.SkillInfo[skillIndex].Level);
        var levelText = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "level");
        var bonusLevel = ServerNetworkManager.Instance.User.SkillInfo[skillIndex].BonusLevel;

        if(bonusLevel != 0)
            skillNameAndLevelText.text = $"{LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, $"skill_name_0{this.skillIndex + 1}")} ({levelText}{skillLevel} +{bonusLevel})";
        else
            skillNameAndLevelText.text = $"{LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, $"skill_name_0{this.skillIndex + 1}")} ({levelText}{skillLevel})";

        requireGoldText.text = LanguageManager.Instance.NumberToString(requiredGold);

        lockPanel.gameObject.SetActive(false);
        if(isLock)
        {
            upgradeButtonText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_skill_unlock"));

            var currentValue = ServerNetworkManager.Instance.User.SkillInfo[skillIndex].EffectPower;
            skillValueText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "stat_skill_unlock_desc"), currentValue.ToString("F2"));
        }
        else
        {
            upgradeButtonText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_item_upgrade"));
            
            var currentValue = ServerNetworkManager.Instance.User.SkillInfo[skillIndex].EffectPower;
            var nextValue = ServerNetworkManager.Instance.User.SkillInfo[skillIndex].NextEffectPower;
            skillValueText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "stat_skill_desc"), currentValue.ToString("F2"), nextValue.ToString("F2"));
        }

        // Check gold and level
        BigInteger currentGold = ServerNetworkManager.Instance.Inventory.Gold;

        if(currentGold < requiredGold || requiredGold == -1 || skillLevel >= ServerNetworkManager.Instance.Setting.MaxSkillLevel)
            isAvailableUpgrade = false;
        else
            isAvailableUpgrade = true;

        if(isAvailableUpgrade)
            upgradeButton.interactable = true;
        else
            upgradeButton.interactable = false;

        if(isShowUpgradeEffect)
            ShowUpgradeEffect();
    }

    private void ShowUpgradeEffect()
	{
		SoundManager.Instance.PlaySound("Upgrade");
	}

    public void OnPointerDown()
    {
        lastTime = Time.realtimeSinceStartup;
        isPressed = true;
        isPointUpActionEnabled = true;
    }
    
    public void OnPointerUp()
    {
        if(isPressed && isPointUpActionEnabled)
        {
            if(isAvailableUpgrade)
                GameObject.FindObjectOfType<UIPopupUpgrade>().RequestSkillUpgrade(skillIndex);
        }
        isPressed = false;
        isPointUpActionEnabled = false;
    }
}
