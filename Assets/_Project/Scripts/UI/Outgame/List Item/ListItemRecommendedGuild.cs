﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class ListItemRecommendedGuild : MonoBehaviour
{
    [SerializeField]
    private Image guildFlagIcon;
    [SerializeField]
    private List<Sprite> guildFlagIconList;

    [SerializeField]
    private Text guildRankingText = null;
    [SerializeField]
    private Text guildNameText = null;
    [SerializeField]
    private Text needStageProgressText = null;
    [SerializeField]
    private Text guildMemberInfoText = null;

    [SerializeField]
    private Transform privateGuildBadge = null;
    [SerializeField]
    private Transform publicGuildBadge = null;

    [SerializeField]
    private Transform appliedPanel = null;

    string guildId;

    public void UpdateEntity(string guildId, int guildRanking, int guildLevel, int guildFlagIndex, string guildName, string guildNotice, int needStageProgress, 
    int currentGuildMemberCount, int maxGuildMemberCount, bool isPrivate, bool isAvailable)
	{
        this.guildId = guildId;
        guildFlagIcon.sprite = guildFlagIconList[Mathf.Min(guildFlagIndex, guildFlagIconList.Count)];
        
        if(guildRanking == -1)
            guildRankingText.text = "-";
        else
            guildRankingText.text = $"#{guildRanking + 1}";

        guildNameText.text = guildName;
        needStageProgressText.text = $"{LanguageManager.Instance.GetTextData(LanguageDataType.UI, "guild_need_stage_progress")}: {needStageProgress}";
        guildMemberInfoText.text = $"{currentGuildMemberCount}/{maxGuildMemberCount}";

        privateGuildBadge.gameObject.SetActive(isPrivate);
        publicGuildBadge.gameObject.SetActive(!isPrivate);

        appliedPanel.gameObject.SetActive(!isAvailable);
    }

    public void OnListItemClicked()
    {
        GameObject.FindObjectOfType<UIPopupGuild>().OnRecommendedGuildSelected(guildId);
    }
}

