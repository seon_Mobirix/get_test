﻿using UnityEngine;
using UnityEngine.UI;

using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class ListItemSummonResult : MonoBehaviour
{
    [SerializeField]
    private GameObject newTag;

    [SerializeField]
    private Image thumbnailImage;

    [SerializeField]
    private GameObject whiteEffect;
    
    [SerializeField]
    private Text rankInfoText; 

    [SerializeField]
    private List<Sprite> backImageList = new List<Sprite>();

    private Sprite spriteToShow = null;

    public void UpdateEntity(SummonType summonType, string Id, bool isNew, bool isWhite, float revealAfter, float effectScale)
	{
        if(isNew)
        {
            thumbnailImage.sprite = backImageList[1];
            newTag.SetActive(true);
        }
        else
        {
            if(isWhite)
                whiteEffect.SetActive(true);
            else
                whiteEffect.SetActive(false);
                
            thumbnailImage.sprite = backImageList[0];
            newTag.SetActive(false);
        }

        newTag.transform.localScale = new Vector3(effectScale, effectScale, effectScale);
        whiteEffect.transform.localScale = new Vector3(effectScale, effectScale, effectScale);

        rankInfoText.enabled = false;
        
        Sprite targetSprite = null;
        if(summonType == SummonType.WEAPON)
        {
            targetSprite = Resources.Load<Sprite>("Thumbnails/Weapon_" + int.Parse(Id.Substring(Id.Length -2)).ToString("D2"));
            rankInfoText.text = LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "rank_name_" + int.Parse(Id.Substring(Id.Length -2)).ToString("D2"));
        }
        else if(summonType == SummonType.CLASS)
        {
            targetSprite = Resources.Load<Sprite>("Thumbnails/Class_" + int.Parse(Id.Substring(Id.Length -2)).ToString("D2"));
            rankInfoText.text = LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "rank_name_" + int.Parse(Id.Substring(Id.Length -2)).ToString("D2"));
        }
        else if(summonType == SummonType.MERCENARY)
        {
            targetSprite = Resources.Load<Sprite>("Thumbnails/Mercenary_" + int.Parse(Id.Substring(Id.Length -2)).ToString("D2"));
            rankInfoText.text = LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "rank_name_" + int.Parse(Id.Substring(Id.Length -2)).ToString("D2"));
        }
        else if(summonType == SummonType.RELIC)
        {
            targetSprite = Resources.Load<Sprite>("Thumbnails/Relic_" + int.Parse(Id.Substring(Id.Length -2)).ToString("D2"));
            rankInfoText.text = "";
        }
        else if(summonType == SummonType.LEGEMDARY_RELIC)
        {
            targetSprite = Resources.Load<Sprite>("Thumbnails/Legendary_Relic_" + int.Parse(Id.Substring(Id.Length -2)).ToString("D2"));
            rankInfoText.text = "";
        }
        else if(summonType == SummonType.PET)
        {
            var currentPetBalanceInfo = BalanceInfoManager.Instance.PetInfoList.FirstOrDefault(n => n.Id == Id);
            targetSprite = Resources.Load<Sprite>("Pets/Thumbnails/pet_" + int.Parse(Id.Substring(Id.Length -2)).ToString("D2"));
            
            if(currentPetBalanceInfo != null)
                rankInfoText.text = currentPetBalanceInfo.Rank;
            else
                rankInfoText.text = "";
        }

        revealCoroutine = StartCoroutine(RevealCoroutine(targetSprite, revealAfter, isNew));
    }

    private Coroutine revealCoroutine = null;
    private IEnumerator RevealCoroutine(Sprite sprite, float revealAfter, bool isNew)
    {
        spriteToShow = sprite;

        if(!isNew)
        {
            yield return new WaitForSeconds(revealAfter);
            SoundManager.Instance.PlaySound("Upgrade");
        }
        else
        {
            yield return new WaitForSeconds(revealAfter - 0.5f);
            SoundManager.Instance.PlaySound("New");
            GameObject.FindObjectOfType<UIPopupSummonResult>().AddWhitecard();
        }

        Reveal();
    }

    public void SkipReveal()
    {
        if(revealCoroutine != null)
            StopCoroutine(revealCoroutine);

        Reveal();
    }

    private void Reveal()
    {
        thumbnailImage.sprite = spriteToShow;
        rankInfoText.enabled = true;
        newTag.SetActive(false);
    }
}
