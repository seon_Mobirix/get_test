﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class ListItemTowerBattleData : MonoBehaviour
{
    [SerializeField]
	private Text timeText;
    [SerializeField]
	private Text nameText;
    [SerializeField]
	private Text battleResultText;
    [SerializeField]
	private Text floorChangeInfoText;
    [SerializeField]
	private List<Color> battleResultTextColorList;
    [SerializeField]
	private List<Color> floorChangeInfoTextColorList;

    public void UpdateEntity(int timeStamp, string memberName, int floorNumber, bool isWin, bool isDefence)
	{
		var currentTimeStampMinute = DateTimeOffset.Now.ToUnixTimeSeconds() / 60;
        var hourDifference = Mathf.RoundToInt(Mathf.Abs(currentTimeStampMinute - timeStamp) / 60);

        if(hourDifference < 1)
        {
            timeText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "tower_time_recent");
        }
        else if(hourDifference < 24)
        {
            timeText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "tower_time_hours"), hourDifference);
        }
        else
        {
            var dayDifference = hourDifference / 24;
            timeText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "tower_time_days"), dayDifference);
        }
        
        if(nameText != null)
            nameText.text = memberName;

        if(isDefence)
        {            
            if(isWin)
            {
                if(battleResultText != null)
                {
                    battleResultText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "arena_win");
                    battleResultText.color = battleResultTextColorList[0];
                }

                floorChangeInfoText.text = "-";
                floorChangeInfoText.color = floorChangeInfoTextColorList[2];
            }
            else
            {
                if(battleResultText != null)
                {
                    battleResultText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "arena_lose");
                    battleResultText.color = battleResultTextColorList[1];
                }

                floorChangeInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "tower_move_down"), floorNumber);
                floorChangeInfoText.color = floorChangeInfoTextColorList[1];
            }
        }
        else
        {
            if(isWin)
            {
                if(battleResultText != null)
                {
                    battleResultText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "arena_win");
                    battleResultText.color = battleResultTextColorList[0];
                }
                
                floorChangeInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "tower_move_up"), floorNumber);
                floorChangeInfoText.color = floorChangeInfoTextColorList[0];
            }
            else
            {
                if(battleResultText != null)
                {
                    battleResultText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "arena_lose");
                    battleResultText.color = battleResultTextColorList[1];
                }

                floorChangeInfoText.text = "-";
                floorChangeInfoText.color = floorChangeInfoTextColorList[2];
            }
        }
	}
}
