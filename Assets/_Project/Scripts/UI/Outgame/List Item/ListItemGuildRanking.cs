﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class ListItemGuildRanking : MonoBehaviour
{
    [SerializeField]
    private Text rankText = null;
    [SerializeField]
    private Image rankIcon;
    [SerializeField]
    private List<Sprite> rankIconList;

    [SerializeField]
    private Text guildNameText = null;
    [SerializeField]
    private Text guildMasterNameText = null;

    [SerializeField]
    private Text statValueText = null;

    public void UpdateEntity(int rank, int statValue, string guildName, string guildMasterName)
	{
        if(rank == -1)
        {
            rankIcon.gameObject.SetActive(false);
            rankText.gameObject.SetActive(true);
            rankText.text = "-";
        }
        if(rank < 4)
        {
            rankIcon.sprite = rankIconList[rank - 1];
            rankIcon.gameObject.SetActive(true);
            rankText.gameObject.SetActive(false);
        }
        else
        {
            rankIcon.gameObject.SetActive(false);
            rankText.gameObject.SetActive(true);
            rankText.text = rank.ToString();
        }

        guildNameText.text = guildName;
        guildMasterNameText.text = guildMasterName;
        statValueText.text = statValue.ToString();
    }
}