﻿using UnityEngine;
using UnityEngine.UI;

public class ListItemWing : MonoBehaviour
{
    [SerializeField]
	private Text wingNameText;
    [SerializeField]
	private Image wingIcon;
    [SerializeField]
	private Transform equipmentDisplay;
    [SerializeField]
    private Transform lockPanel;

    private string wingId;
    private bool isLock;

    public void UpdateEntity(string wingId, bool isLock, bool isEquipment)
	{
        this.wingId = wingId;
        this.isLock = isLock;

        wingIcon.sprite = Resources.Load<Sprite>("Thumbnails/Wing_" + int.Parse(wingId.Substring(wingId.Length - 2)).ToString("D2"));
        wingNameText.text = LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "wing_name_" + int.Parse(wingId.Substring(wingId.Length - 2)).ToString("D2"));

        lockPanel.gameObject.SetActive(isLock);
        equipmentDisplay.gameObject.SetActive(isEquipment);
    }

    public void OnButtonClicked()
    {
        GameObject.FindObjectOfType<UIPopupWing>().RefreshSelectedWing(wingId, isLock);
    }
}
