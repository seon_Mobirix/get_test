﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Globalization;

public class ListItemLegendaryRelic : MonoBehaviour
{
    [SerializeField]
	private Text countInfoText;
    [SerializeField]
	private Image relicIcon;

    [SerializeField]
	private Text relicNameAndLevelText;
    [SerializeField]
	private Text statValueText;

    [SerializeField]
    private Transform upgradeChanceInfo;
    [SerializeField]
	private Text upgradeChanceInfoText;
    [SerializeField]
	private Text requiredMergeAmountText;
    [SerializeField]
    private Button mergeButton;
    [SerializeField]
	private Image mergeIcon;

    [SerializeField]
	private Text refundButtonText;
    [SerializeField]
    private Button refundButton;

    [SerializeField]
    private Transform lockPanel;

    private RelicInfoModel currentLegendaryRelicInfo;

    private bool isInit = false;
    private bool isOwned;
    private bool isAvailableRefund = false;
    private bool isAvailableUpgrade = true;

    private string id;
    public string Id
    {
        get{ 
            return id;
        }
    }

    public void UpdateEntity(string id, int level, int count, bool isOwned, bool isShowUpgradeEffect, bool isUpgradeSuccess)
	{
        this.isOwned = isOwned;
        this.id = id;

        // Init first time
        if(!isInit)
        {
            currentLegendaryRelicInfo = BalanceInfoManager.Instance.LegendaryRelicInfoList.FirstOrDefault(n => n.Id == id);

            if(currentLegendaryRelicInfo != null)
            {
                relicIcon.sprite = Resources.Load<Sprite>("Thumbnails/Legendary_Relic_" + int.Parse(id.Substring(id.Length - 2)).ToString("D2"));
                mergeIcon.sprite = relicIcon.sprite;
            }
        }

        countInfoText.text = "x" + count.ToString();
    
        var relicBasicEffect = int.Parse(currentLegendaryRelicInfo.DefaultEffect);
        var currentValue = relicBasicEffect + Mathf.Abs(float.Parse(currentLegendaryRelicInfo.EffectPower, NumberStyles.Any, CultureInfo.InvariantCulture)) * level;

        if(currentValue < 1f)
            statValueText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, id + "_upgrade_desc"), currentValue.ToString("F2"));
        else
            statValueText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, id + "_upgrade_desc"), currentValue);
        
        if(isOwned)
        {
            lockPanel.gameObject.SetActive(false);
            int maxLevel = int.Parse(currentLegendaryRelicInfo.MaxLevel);

            // Check max level
            if(maxLevel != -1 && level >= maxLevel)
            {
                relicNameAndLevelText.text = string.Format("{0} {1}", LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "legendary_relic_name_" + int.Parse(id.Substring(id.Length - 2)).ToString("D2")), "\n"+ LanguageManager.Instance.GetTextData(LanguageDataType.UI, "level_max"));
                upgradeChanceInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "upgrade_percentage_info"), 0);
                refundButtonText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_refund_relic"), count - 1);

                // Exclude using relic count
                requiredMergeAmountText.text = "x1";

                if(count - 1 > 0)
                    isAvailableRefund = true;
                else
                    isAvailableRefund = false;

                mergeButton.interactable = false;
                refundButton.interactable = isAvailableRefund;
            }
            else
            {
                relicNameAndLevelText.text = string.Format("{0}\n"+ LanguageManager.Instance.GetTextData(LanguageDataType.UI, "level") + "{1}", LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "legendary_relic_name_" + int.Parse(id.Substring(id.Length - 2)).ToString("D2")), level);
                upgradeChanceInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "upgrade_percentage_info"), 100);
                refundButtonText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_refund_relic"), 0);

                // Exclude using relic count
                requiredMergeAmountText.text = "x1";

                // Check relic count
                if(count > 1)
                    isAvailableUpgrade = true;
                else
                    isAvailableUpgrade = false;

                mergeButton.interactable = isAvailableUpgrade;
                refundButton.interactable = false;
            }

            if(isShowUpgradeEffect)
                ShowUpgradeEffect(isUpgradeSuccess);
        }
        else
        {
            lockPanel.gameObject.SetActive(true);

            relicNameAndLevelText.text = string.Format("{0}\n"+ LanguageManager.Instance.GetTextData(LanguageDataType.UI, "level") + "{1}", LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "legendary_relic_name_" + int.Parse(id.Substring(id.Length - 2)).ToString("D2")), level);
            upgradeChanceInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "upgrade_percentage_info"), 100);
            requiredMergeAmountText.text = "x1";
            refundButtonText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_refund_relic"), 0);

            mergeButton.interactable = false;
            refundButton.interactable = false;
        }
    }

    private void ShowUpgradeEffect(bool isSuccess)
	{
	    if(isSuccess)
        {
            OutgameController.Instance.ShowToast(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_upgrade_success"));
		    SoundManager.Instance.PlaySound("Upgrade");
        }
        else
        {
            OutgameController.Instance.ShowToast(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_relic_upgrade_fail"));
            SoundManager.Instance.PlaySound("UpgradeFail");
        }
	}

    public void OnRefundButtonClicked()
    {
        if(isAvailableRefund)
            GameObject.FindObjectOfType<UIPopupEquipment>().RequestLegendaryRelicRefund(currentLegendaryRelicInfo.Id);
    }

    public void OnMergeButtonClicked()
    {
        if(isAvailableUpgrade)
            GameObject.FindObjectOfType<UIPopupEquipment>().RequestLegendaryRelicUpgrade(currentLegendaryRelicInfo.Id);
    }
}
