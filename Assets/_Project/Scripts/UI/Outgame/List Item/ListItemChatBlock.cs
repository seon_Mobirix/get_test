﻿using UnityEngine;
using UnityEngine.UI;

public class ListItemChatBlock : MonoBehaviour
{
    [SerializeField]
    private Text nicknameText;
    [SerializeField]
    private Button reportButton;
    [SerializeField]
    private Button cancelButton;
    
    private string id = "";

    public void UpdateEntity(string id, string nickname, bool isBanned)
	{
        this.id = id;

        nicknameText.text = nickname;
        
        reportButton.gameObject.SetActive(!isBanned);
        cancelButton.gameObject.SetActive(isBanned);
    }

    public void OnReportButtonClicked()
    {
        GameObject.FindObjectOfType<UIPopupReport>().Report(id);
    }

    public void OnReportCancelButtonClicked()
    {
        GameObject.FindObjectOfType<UIPopupReport>().ReportCancel(id);
    }
}
