﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class ListItemNotice : MonoBehaviour
{
    [SerializeField]
    private List<Sprite> backgroundSpriteList;
    [SerializeField]
    private Image background;
    [SerializeField]
    private Text titleText;
    [SerializeField]
    private Text dateText;
    [SerializeField]
    private Transform newBdage;
    [SerializeField]
    private Button listItemButton;

    private string noticeId;

    public void UpdateEntity(string id, string date, string titleText, bool isNew, bool isSelected)
	{
        noticeId = id;
        this.titleText.text = titleText;
        this.dateText.text = date;
        
        newBdage.gameObject.SetActive(isNew);

        if(isSelected)
        {
            background.sprite = backgroundSpriteList[1];
            listItemButton.interactable = false;
        }
        else
        {
            background.sprite = backgroundSpriteList[0];
            listItemButton.interactable = true;
        }
    }

    public void OnListItemClicked()
    {
        if(GameObject.FindObjectOfType<UIPopupNotice>() != null)
            GameObject.FindObjectOfType<UIPopupNotice>().SelectNotice(noticeId);
    }
}
