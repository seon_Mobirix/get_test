using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class ListItemApplicant: MonoBehaviour
{
    [SerializeField]
    private Text nameText;
    [SerializeField]
    private Text stageText;

    private string id = "";

    private double lastClickTimeStampMilliseconds = 0f;

	public void UpdateEntity(string id, string name, int stage)
	{
        this.id = id;

        nameText.text = name;
        stageText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "stage_name"), stage);
	}
    
    public void OnAcceptButtonClick()
    {
		var passedMilliSeconds = DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastClickTimeStampMilliseconds;
		
		if(passedMilliSeconds >= 500d)
		{
			lastClickTimeStampMilliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();
			OutgameController.Instance.AccpectGuildApplicant(id);
		}
	}

    public void OnRefuseButtonClick()
    {
		var passedMilliSeconds = DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastClickTimeStampMilliseconds;
		
		if(passedMilliSeconds >= 500d)
		{
			lastClickTimeStampMilliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();
			OutgameController.Instance.RefuseGuildApplicant(id);
		}
	}
}