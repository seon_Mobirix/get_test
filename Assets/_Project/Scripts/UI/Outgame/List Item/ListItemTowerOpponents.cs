﻿using UnityEngine;
using UnityEngine.UI;

public class ListItemTowerOpponents : MonoBehaviour
{

    [SerializeField]
    private Text nameText = null;

    [SerializeField]
    private Text powerText = null;

    [SerializeField]
    private GameObject nonExistenceCover = null;

    [SerializeField]
    private int teamNumber = 0;

    private TowerBattleOpponentModel opponentModel;

    public void UpdateEntity(bool isAvail, int teamNumber, TowerBattleOpponentModel opponentModel)
	{
        if(isAvail)
        {
            this.teamNumber = teamNumber;
            this.opponentModel = opponentModel;

            nameText.text = opponentModel.NickName;
            
            int power = 0;

            if(opponentModel != null)
            {
                nonExistenceCover.gameObject.SetActive(false);
            }
            else
            {
                nonExistenceCover.gameObject.SetActive(true);
            }

            powerText.text = power.ToString();
        }
        else
        {
            nonExistenceCover.gameObject.SetActive(true);
        }
    }

    public void OnChallengebuttonClicked()
	{
        //OutgameController.Instance.MineBattleStart(teamNumber, opponentModel);
	}
}