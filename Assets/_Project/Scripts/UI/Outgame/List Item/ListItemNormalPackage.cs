﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class ListItemNormalPackage : MonoBehaviour
{
	[SerializeField]
	private List<Sprite> backgroundSpriteList;
	[SerializeField]
	private Image packageBackground;

	[SerializeField]
	private List<Sprite> nameSpriteList;
	[SerializeField]
	private Image nameBackground;
    [SerializeField]
	private List<Text> packageNameTextList;
	[SerializeField]
	private Text purchaseInfoText;

	[SerializeField]
	private Transform nameWithLimit;

	[SerializeField]
	private Transform nameWithoutLimit;

	[SerializeField]
	private Text vipPointText;
	
	[SerializeField]
	private Text packageContentsText;

	[SerializeField]
	private Text priceText;
	[SerializeField]
	private Button purchaseButton;
	
	private string normalPackageId;
	private bool needToCheckAvailablity = false;

    public void UpdateEntity(string id, string packageValue, int packageBackgroundIndex, int gemReward, int featherReward, int vipReward, int bonusStatReward, string price, bool isAvailable, int purchasedCount = 1, int allowedPurchaseCount = 1)
	{
		needToCheckAvailablity = false;
		this.normalPackageId = id;

        packageBackground.sprite = backgroundSpriteList[packageBackgroundIndex];
		nameBackground.sprite = nameSpriteList[packageBackgroundIndex];

		foreach(var tmp in packageNameTextList)
		{
			tmp.text = LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, id);
		}
		    
        vipPointText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "vip_point") + " +" + vipReward;

		if(id == "bundle_package_daily")
		{
			needToCheckAvailablity = true;

			nameWithLimit.gameObject.SetActive(true);
			nameWithoutLimit.gameObject.SetActive(false);

			packageContentsText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "desc_package_daily"), gemReward);

			if(allowedPurchaseCount == 1)
			{
				purchaseInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "daily_limited_package_info"), purchasedCount, allowedPurchaseCount);
			}
			else
			{
				purchaseInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "daily_limited_package_info_3"), purchasedCount, allowedPurchaseCount);
			}
		}
		else if(id == "bundle_package_daily_2")
		{
			needToCheckAvailablity = true;

			nameWithLimit.gameObject.SetActive(true);
			nameWithoutLimit.gameObject.SetActive(false);

			packageContentsText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "desc_package_daily_2"), gemReward, featherReward);

			if(allowedPurchaseCount == 1)
			{
				purchaseInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "daily_limited_package_info"), purchasedCount, allowedPurchaseCount);
			}
			else
			{
				purchaseInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "daily_limited_package_info_3"), purchasedCount, allowedPurchaseCount);
			}
		}
		else if(id == "bundle_package_weekly")
		{
			needToCheckAvailablity = true;

			nameWithLimit.gameObject.SetActive(true);
			nameWithoutLimit.gameObject.SetActive(false);

			packageContentsText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "desc_package_weekly"), gemReward, bonusStatReward);

			if(allowedPurchaseCount == 1)
			{
				purchaseInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "weekly_limited_package_info"), purchasedCount, allowedPurchaseCount);
			}
			else
			{
				purchaseInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "weekly_limited_package_info_3"), purchasedCount, allowedPurchaseCount);
			}
		}
		else if(id == "bundle_package_weekly_2")
		{
			needToCheckAvailablity = true;

			nameWithLimit.gameObject.SetActive(true);
			nameWithoutLimit.gameObject.SetActive(false);

			packageContentsText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "desc_package_daily"), gemReward, bonusStatReward);

			if(allowedPurchaseCount == 1)
			{
				purchaseInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "weekly_limited_package_info"), purchasedCount, allowedPurchaseCount);
			}
			else
			{
				purchaseInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "weekly_limited_package_info_3"), purchasedCount, allowedPurchaseCount);
			}
		}
		else if(id == "bundle_package_weekly_3")
		{
			needToCheckAvailablity = true;

			nameWithLimit.gameObject.SetActive(true);
			nameWithoutLimit.gameObject.SetActive(false);

			packageContentsText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "desc_package_daily_2"), gemReward, featherReward);

			if(allowedPurchaseCount == 1)
			{
				purchaseInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "weekly_limited_package_info"), purchasedCount, allowedPurchaseCount);
			}
			else
			{
				purchaseInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "weekly_limited_package_info_3"), purchasedCount, allowedPurchaseCount);
			}
		}
		else if(id == "bundle_package_basic_01")
		{
			nameWithLimit.gameObject.SetActive(false);
			nameWithoutLimit.gameObject.SetActive(true);

			packageContentsText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "desc_package_basic_1"), gemReward, packageValue, 2);
		}
		else
		{
			nameWithLimit.gameObject.SetActive(false);
			nameWithoutLimit.gameObject.SetActive(true);

			packageContentsText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "desc_package_basic_2"), gemReward, packageValue);
		}

        if(priceText != null)
			priceText.text = price;

		if(isAvailable)
			purchaseButton.interactable = true;
		else
			purchaseButton.interactable = false;
	}

	public void OnPurchaseButton()
	{
		if(needToCheckAvailablity)
			GameObject.FindObjectOfType<UIPopupShop>().OnPurchaseLimitedPackage(normalPackageId);
		else
			GameObject.FindObjectOfType<UIPopupShop>().OnPurchaseNormalPackage(normalPackageId);
	}
}
