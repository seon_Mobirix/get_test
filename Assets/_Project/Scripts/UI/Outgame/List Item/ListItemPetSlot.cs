﻿using UnityEngine;
using UnityEngine.UI;

public class ListItemPetSlot : MonoBehaviour
{
    private string petId = "";

    [SerializeField]
    private Image thumbnailImage = null;

    [SerializeField]
    private Button removeButton = null;

    [SerializeField]
    private Transform lockCover = null;

    [SerializeField]
    private Text lockText = null;

    public void UpdateEntity(bool isLock, string unlockTier, bool isInSlot, string petId = "")
	{
        if(isLock)
        {
            lockText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "unlock_at_class"), unlockTier);
            lockCover.gameObject.SetActive(true);
        }
        else
        {
            lockCover.gameObject.SetActive(false);
        
            if(isInSlot)
            {
                thumbnailImage.sprite = Resources.Load<Sprite>("Pets/Thumbnails/Pet_" + petId.Substring(petId.Length - 2));
                thumbnailImage.gameObject.SetActive(true);
                removeButton.gameObject.SetActive(true);
            }
            else
            {
                thumbnailImage.gameObject.SetActive(false);
                removeButton.gameObject.SetActive(false);
            }

            this.petId = petId;
        }
    }

    public void OnRemoveButtonClick()
    {
        if(petId != "")
            GameObject.FindObjectOfType<UIPopupPet>().RemovePetFromSlot(this.petId);
    }
}
