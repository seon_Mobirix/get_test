﻿using UnityEngine;
using UnityEngine.UI;
using System.Globalization;
using System.Collections.Generic;

public enum WingStatInfoType {ATTACK, GOLD, SKILL_1, SKILL_2, SKILL_3};

public class ListItemWingStat : MonoBehaviour
{
    public WingStatInfoType wingStatType;
    [SerializeField]
    private List<Color> rankColorList;
    [SerializeField]
    private Text statText;
    [SerializeField]
    private Transform lockPanel;

    public void UpdateEntity(string rank, int totalStatValue, bool isLock)
    {
        lockPanel.gameObject.SetActive(isLock);

        // Set rank color
        if(rank == "S")
            statText.color = rankColorList[0];
        else if(rank == "A")
            statText.color = rankColorList[1];
        else if(rank == "B")
            statText.color = rankColorList[2];
        else if(rank == "C")
            statText.color = rankColorList[3];
        else if(rank == "D")
            statText.color = rankColorList[4];
        else
            statText.color = rankColorList[5];

        string basicText = LanguageManager.Instance.GetTextData(LanguageDataType.UI , $"wing_stat_info_{wingStatType.ToString().ToLower()}");
        string statInfoText = $"{totalStatValue}";
        statText.text = string.Format(basicText, rank, statInfoText.ToString());
    }

    public void OnStatInfoButtonClicked()
    {
        var text = GetDetailInfoText();

        if(text != "")
            OutgameController.Instance.ShowGenericInfo(text);
    }

    public void OnStatLockButtonClicked()
    {
        GameObject.FindObjectOfType<UIPopupWing>().OnStatLockButtonClicked((int)wingStatType, () => {
            lockPanel.gameObject.SetActive(true);
        });
    }

    public void OnStatUnLockButtonClicked()
    {
        GameObject.FindObjectOfType<UIPopupWing>().OnStatUnLockButtonClicked((int)wingStatType, () => {
            lockPanel.gameObject.SetActive(false);
        });
    }

    private string GetDetailInfoText()
    {
        var detailInfoText = "";
        float totalSummonPower = 0f;

        foreach(var tmp in BalanceInfoManager.Instance.WingInfoList)
            totalSummonPower += float.Parse(tmp.SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture);

        if(wingStatType == WingStatInfoType.ATTACK)
        {
            var textForamt = LanguageManager.Instance.GetTextData(LanguageDataType.UI , "wing_owned_info");
            
            foreach(var tmp in BalanceInfoManager.Instance.WingInfoList)
            {
                var chance = float.Parse(tmp.SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture) / (float)totalSummonPower;                
                
                detailInfoText = detailInfoText + string.Format(textForamt, tmp.Rank, tmp.OwnedStatBonus1Min, tmp.OwnedStatBonus1Max, (chance * 100f).ToString());
                detailInfoText = detailInfoText + "\n";
            }
        }
        else if(wingStatType == WingStatInfoType.GOLD)
        {
            var textForamt = LanguageManager.Instance.GetTextData(LanguageDataType.UI , "wing_owned_info");
            
            foreach(var tmp in BalanceInfoManager.Instance.WingInfoList)
            {
                var chance = float.Parse(tmp.SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture) / (float)totalSummonPower;                

                detailInfoText = detailInfoText + string.Format(textForamt, tmp.Rank, tmp.OwnedStatBonus1Min, tmp.OwnedStatBonus1Max, (chance * 100f).ToString());
                detailInfoText = detailInfoText + "\n";
            }
        }
        else if(wingStatType == WingStatInfoType.SKILL_1)
        {
            var textForamt = LanguageManager.Instance.GetTextData(LanguageDataType.UI , "wing_equipment_info");
            
            foreach(var tmp in BalanceInfoManager.Instance.WingInfoList)
            {
                var chance = float.Parse(tmp.SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture) / (float)totalSummonPower;                

                detailInfoText = detailInfoText + string.Format(textForamt, tmp.Rank, tmp.EquipmentStatBonus1, (chance * 100f).ToString());
                detailInfoText = detailInfoText + "\n";
            }
        }
        else if(wingStatType == WingStatInfoType.SKILL_2)
        {
            var textForamt = LanguageManager.Instance.GetTextData(LanguageDataType.UI , "wing_equipment_info");
            
            foreach(var tmp in BalanceInfoManager.Instance.WingInfoList)
            {
                var chance = float.Parse(tmp.SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture) / (float)totalSummonPower;                

                detailInfoText = detailInfoText + string.Format(textForamt, tmp.Rank, tmp.EquipmentStatBonus2, (chance * 100f).ToString());
                detailInfoText = detailInfoText + "\n";
            }
        }
        else if(wingStatType == WingStatInfoType.SKILL_3)
        {
            var textForamt = LanguageManager.Instance.GetTextData(LanguageDataType.UI , "wing_equipment_info");
            
            foreach(var tmp in BalanceInfoManager.Instance.WingInfoList)
            {
                var chance = float.Parse(tmp.SummonPower, NumberStyles.Any, CultureInfo.InvariantCulture) / (float)totalSummonPower;                

                detailInfoText = detailInfoText + string.Format(textForamt, tmp.Rank, tmp.EquipmentStatBonus3, (chance * 100f).ToString());
                detailInfoText = detailInfoText + "\n";
            }
        }

        return detailInfoText;
    }
}
