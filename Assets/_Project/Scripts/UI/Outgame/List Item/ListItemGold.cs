﻿using UnityEngine;
using UnityEngine.UI;

public class ListItemGold : MonoBehaviour 
{
	[SerializeField]
	private Text itemNameText;
	[SerializeField]
	private Text priceText;
	[SerializeField]
	private Text goldAmountText;
	[SerializeField]
	private Image thumbnailImage;

	private string goldBundleId;

	public void UpdateEntity(string id, int goldWeight, string price)
	{
		this.goldBundleId = id;

		var tmpImage = Resources.Load<Sprite>(("Currency/" + id));
		if(tmpImage != null)
			thumbnailImage.sprite = tmpImage;

		if(id == "bundle_gold_03")
		{
			var covertedPostion = thumbnailImage.rectTransform.position;
			covertedPostion.y = covertedPostion.y - 40;
			thumbnailImage.rectTransform.position = covertedPostion;
		}

		itemNameText.text = LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, id);
		var gold = LanguageManager.Instance.NumberToString(NumberMainController.Instance.FinalBestGoldIncome * goldWeight);
		goldAmountText.text = string.Format("{0} " + LanguageManager.Instance.GetTextData(LanguageDataType.UI, "item_gold"), gold);
		priceText.text = string.Format("{0:#,###0.#}", int.Parse(price));
	}

	public void OnPurchaseButton()
	{
		GameObject.FindObjectOfType<UIPopupShop>().OnPurchaseGoldBundle(goldBundleId);
	}
}
