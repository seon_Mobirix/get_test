﻿using UnityEngine;
using UnityEngine.UI;

public class ListItemArenaRank : MonoBehaviour
{
    [SerializeField]
    private Text rankText = null;
    [SerializeField]
    private Text nameText = null;
    [SerializeField]
    private Text scoreText = null;

	public void UpdateEntity(string name, int rank, int score)
	{
        nameText.text = name;
        rankText.text = "#" + rank;
        scoreText.text = score.ToString();
	}
}
