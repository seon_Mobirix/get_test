﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;

public class ListItemSummon : MonoBehaviour
{

    [SerializeField]
    private SummonType summonType = SummonType.WEAPON;

    [SerializeField]
    private Text summonNameText = null;

    [SerializeField]
    private Text summonExpInfoText = null;
    
    [SerializeField]
    private Slider summonExpSlider = null;

    [SerializeField]
    private Text summonLevelInfoText = null;

    [SerializeField]
    private Text priceFor1Text = null;

    [SerializeField]
    private Text priceFor11Text = null;

    [SerializeField]
    private Button summon1Button = null;

    [SerializeField]
    private Button summon11Button = null;

    [SerializeField]
    private Transform lockPanel = null;

    [SerializeField]
    private Text lockText = null;

    private int summonLevel = 0;

    public void UpdateEntity()
	{
        if(summonType == SummonType.WEAPON)
        {
            summonNameText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "summon_weapon");

            var currentLevel = ServerNetworkManager.Instance.User.WeaponGachaLevel;
            summonLevel = currentLevel;
            var currentExp = ServerNetworkManager.Instance.User.WeaponGachaExp;
            var nextExp = -1;
            
            if(currentLevel < BalanceInfoManager.Instance.SummonInfoList.Count)
                nextExp = int.Parse(BalanceInfoManager.Instance.SummonInfoList[currentLevel].SummonExp);

            if(nextExp != -1)
            {
                summonExpInfoText.text = currentExp + "/" + nextExp;
                summonExpSlider.value = (float)currentExp / (float)nextExp;
            }
            else
            {
                summonExpInfoText.text = "MAX";
                summonExpSlider.value = 1f;
            }

            summonLevelInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "summon_level_weapon"), currentLevel);

            if(ServerNetworkManager.Instance.TotalGem < 10)
                summon1Button.interactable = false;
            else
                summon1Button.interactable = true;

            if(ServerNetworkManager.Instance.TotalGem < 100)
                summon11Button.interactable = false;
            else
                summon11Button.interactable = true;
        }
        else if(summonType == SummonType.CLASS)
        {
            summonNameText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "summon_class");

            var currentLevel = ServerNetworkManager.Instance.User.ClassGachaLevel;
            summonLevel = currentLevel;
            var currentExp = ServerNetworkManager.Instance.User.ClassGachaExp;
            var nextExp = -1;
            
            if(currentLevel < BalanceInfoManager.Instance.SummonInfoList.Count)
                nextExp = int.Parse(BalanceInfoManager.Instance.SummonInfoList[currentLevel].SummonExp);
                
            if(nextExp != -1)
            {
                summonExpInfoText.text = currentExp + "/" + nextExp;
                summonExpSlider.value = (float)currentExp / (float)nextExp;
            }
            else
            {
                summonExpInfoText.text = "MAX";
                summonExpSlider.value = 1f;
            }

            summonLevelInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "summon_level_class"), currentLevel);

            if(ServerNetworkManager.Instance.TotalGem < 10)
                summon1Button.interactable = false;
            else
                summon1Button.interactable = true;

            if(ServerNetworkManager.Instance.TotalGem < 100)
                summon11Button.interactable = false;
            else
                summon11Button.interactable = true;
        }
        else if(summonType == SummonType.MERCENARY)
        {
            bool isMercenaryOpen = false;
            string lockTextMessage = "";

        #if UNITY_IPHONE
		    isMercenaryOpen = ServerNetworkManager.Instance.Setting.IsMercenaryiOSOpen;
        #else
            isMercenaryOpen = ServerNetworkManager.Instance.Setting.IsMercenaryAndroidOpen;
        #endif

            // Check mercenary open
            if(isMercenaryOpen)
            {
                // Check mercenary open stage
                if(ServerNetworkManager.Instance.User.StageProgress < ServerNetworkManager.Instance.Setting.MercenaryOpenStageProgress)
                {
                    // For need clear stage info
                    int mercenaryOpenStageProgress = ServerNetworkManager.Instance.Setting.MercenaryOpenStageProgress - 1;

                    var needClearWorldIdx = (mercenaryOpenStageProgress - 1) / ServerNetworkManager.Instance.Setting.MaxStageInWorld + 1;
                    var needClearMobIdx = (mercenaryOpenStageProgress - 1) % ServerNetworkManager.Instance.Setting.MaxStageInWorld + 1;
                    
                    lockTextMessage = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_unlock_stage_progress"), needClearWorldIdx, needClearMobIdx);
                    isMercenaryOpen = false;
                }
            }
            else
                lockTextMessage = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_coming_soon");

            if(isMercenaryOpen)
            {
                if(lockPanel != null)
                    lockPanel.gameObject.SetActive(false);
            }
            else
            {
                if(lockPanel != null)
                    lockPanel.gameObject.SetActive(true);
                if(lockText != null)
                    lockText.text = lockTextMessage;
            }

            summonNameText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "summon_mercenary");

            var currentLevel = ServerNetworkManager.Instance.User.MercenaryGachaLevel;
            summonLevel = currentLevel;
            var currentExp = ServerNetworkManager.Instance.User.MercenaryGachaExp;
            var nextExp = -1;
            
            if(currentLevel < BalanceInfoManager.Instance.SummonInfoList.Count)
                nextExp = int.Parse(BalanceInfoManager.Instance.SummonInfoList[currentLevel].SummonExp);
                
            if(nextExp != -1)
            {
                summonExpInfoText.text = currentExp + "/" + nextExp;
                summonExpSlider.value = (float)currentExp / (float)nextExp;
            }
            else
            {
                summonExpInfoText.text = "MAX";
                summonExpSlider.value = 1f;
            }

            summonLevelInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "summon_level_mercenary"), currentLevel);

            if(ServerNetworkManager.Instance.TotalGem < 50)
                summon1Button.interactable = false;
            else
                summon1Button.interactable = true;

            if(ServerNetworkManager.Instance.TotalGem < 500)
                summon11Button.interactable = false;
            else
                summon11Button.interactable = true;
        }
        else if(summonType == SummonType.RELIC)
        {
            summonNameText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "summon_relic");

            if(ServerNetworkManager.Instance.TotalGem < 100)
                summon1Button.interactable = false;
            else
                summon1Button.interactable = true;

            if(ServerNetworkManager.Instance.TotalGem < 1000)
                summon11Button.interactable = false;
            else
                summon11Button.interactable = true;
        }
        else if(summonType == SummonType.LEGEMDARY_RELIC)
        {
            summonNameText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "summon_legendary_relic");

            // Check legendary relic open
            if(ServerNetworkManager.Instance.Setting.IsLegendaryRelicOpen)
            {
                // Check legendary relic open stage
                if(ServerNetworkManager.Instance.User.StageProgress < ServerNetworkManager.Instance.Setting.LegendaryRelicOpenStageProgress)
                {
                    // For need clear stage info
                    int legendaryRelicOpenStageProgress = ServerNetworkManager.Instance.Setting.LegendaryRelicOpenStageProgress - 1;

                    var needClearWorldIdx = (legendaryRelicOpenStageProgress - 1) / ServerNetworkManager.Instance.Setting.MaxStageInWorld + 1;
                    var needClearMobIdx = (legendaryRelicOpenStageProgress - 1) % ServerNetworkManager.Instance.Setting.MaxStageInWorld + 1;
                    var text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_unlock_stage_progress"), needClearWorldIdx, needClearMobIdx);

                    if(lockPanel != null)
                        lockPanel.gameObject.SetActive(true);

                    if(lockText != null)
                        lockText.text = text;                    
                }
                else
                {
                    var maxLegendaryRelicIdList = new List<string>();

                    // Check all legendary relic is full upgrade or available full upgrade
                    foreach (var tmp in BalanceInfoManager.Instance.LegendaryRelicInfoList)
                    {
                        var targetLegendaryRelic = ServerNetworkManager.Instance.Inventory.LegendaryRelicList.FirstOrDefault(n => n.Id == tmp.Id);
                        
                        if(targetLegendaryRelic != null)
                        {
                            // Check level + count vs target legendary relic max level
                            if((targetLegendaryRelic.Level + targetLegendaryRelic.Count - 1) >= int.Parse(tmp.MaxLevel))
                                maxLegendaryRelicIdList.Add(tmp.Id);
                        }
                    }

                    if(maxLegendaryRelicIdList.Count >= BalanceInfoManager.Instance.LegendaryRelicInfoList.Count)
                    {                        
                        if(lockPanel != null)
                            lockPanel.gameObject.SetActive(true);

                        if(lockText != null)
                            lockText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_max_reached");
                    }
                    else
                    {
                        if(lockPanel != null)
                            lockPanel.gameObject.SetActive(false);
                    }
                }
            }
            else
            {
                if(lockPanel != null)
                    lockPanel.gameObject.SetActive(true);

                if(lockText != null)
                    lockText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_coming_soon");
            }

            if(ServerNetworkManager.Instance.TotalGem < 200)
                summon1Button.interactable = false;
            else
                summon1Button.interactable = true;

            if(ServerNetworkManager.Instance.TotalGem < 2000)
                summon11Button.interactable = false;
            else
                summon11Button.interactable = true;
        }
        else // Pet
        {
            summonNameText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "summon_pet");

            var currentLevel = ServerNetworkManager.Instance.User.PetGachaLevel;
            var currentExp = ServerNetworkManager.Instance.User.PetGachaExp;
            var nextExp = -1;
            
            if(currentLevel < BalanceInfoManager.Instance.SummonInfoList.Count)
                nextExp = int.Parse(BalanceInfoManager.Instance.SummonInfoList[currentLevel].SummonExp);
                
            if(nextExp != -1)
            {
                summonExpInfoText.text = currentExp + "/" + nextExp;
                summonExpSlider.value = (float)currentExp / (float)nextExp;
            }
            else
            {
                summonExpInfoText.text = "MAX";
                summonExpSlider.value = 1f;
            }

            summonLevelInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "summon_level_pet"), currentLevel);

            if(ServerNetworkManager.Instance.TotalGem < 30)
                summon1Button.interactable = false;
            else
                summon1Button.interactable = true;

            if(ServerNetworkManager.Instance.TotalGem < 300)
                summon11Button.interactable = false;
            else
                summon11Button.interactable = true;
        }
    }

    public void OnSummon1Clicked()
    {
        if(GameObject.FindObjectOfType<UIPopupSummon2>() != null)
            GameObject.FindObjectOfType<UIPopupSummon2>().Summon(this.summonType, 1);
    }

    public void OnSummon11Clicked()
    {
        if(GameObject.FindObjectOfType<UIPopupSummon2>() != null)
            GameObject.FindObjectOfType<UIPopupSummon2>().Summon(this.summonType, 11);
    }

    public void OnSummonInfoButtonClicked()
    {
        OutgameController.Instance.ShowSummonLevelInfoPopup();
    }

    public void OnSummonChanceInfoButtonClicked()
    {
        OutgameController.Instance.ShowGachaInfoPopup(summonType, summonLevel);
    }
}
