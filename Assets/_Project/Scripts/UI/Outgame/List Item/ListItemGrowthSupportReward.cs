﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;

public class ListItemGrowthSupportReward : MonoBehaviour
{
    [SerializeField]
	private Text requireLevelText;
    [SerializeField]
	private List<Text> gemAmountTextList;

    [SerializeField]
    private List<Transform> gainButtonList;
    [SerializeField]
	private List<Transform> gainedTextList;

    private int requireStage;
    private bool isOwnedGrowthSupport1;
    private bool isOwnedGrowthSupport2;

    public void UpdateEntity(int requireStage, bool isOwnedGrowthSupport1, bool isAvailableGrowthSupport1, bool isGrowthSupport1Lock, bool isOwnedGrowthSupport2, bool isAvailableGrowthSupport2, bool isGrowthSupport2Lock)
	{
        this.requireStage = requireStage;
        this.isOwnedGrowthSupport1 = isOwnedGrowthSupport1;
        this.isOwnedGrowthSupport2 = isOwnedGrowthSupport2;
        
        requireLevelText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "growth_support_clear_stage"), requireStage);

        // Get growth support pakcage reward info
        var tmpGrowthSupportPackage1 = ServerNetworkManager.Instance.Catalog.GrowthPackageList.FirstOrDefault(n => n.Id == "bundle_package_growth_1");
        var tmpGrowthSupportPackage2 = ServerNetworkManager.Instance.Catalog.GrowthPackageList.FirstOrDefault(n => n.Id == "bundle_package_growth_2");

        if(tmpGrowthSupportPackage1 != null && tmpGrowthSupportPackage2 != null)
        {
            if(tmpGrowthSupportPackage1.GrowthRewardList.Exists(n => n.ItemId == "gem"))
            {
                gemAmountTextList[0].text = tmpGrowthSupportPackage1.GrowthRewardList.FirstOrDefault(n => n.ItemId == "gem").Amount.ToString();
            }
            
            if(tmpGrowthSupportPackage2.GrowthRewardList.Exists(n => n.ItemId == "gem"))
            {
                gemAmountTextList[1].text = tmpGrowthSupportPackage2.GrowthRewardList.FirstOrDefault(n => n.ItemId == "gem").Amount.ToString();
            }
        }

        if(isGrowthSupport1Lock)
        {
            gainButtonList[0].GetComponentInChildren<Button>().interactable = false;
            gainButtonList[0].gameObject.SetActive(true);
            gainedTextList[0].gameObject.SetActive(false);
        }
        else
        {
            gainButtonList[0].GetComponentInChildren<Button>().interactable = true;

            if(isAvailableGrowthSupport1)
            {
                gainButtonList[0].gameObject.SetActive(true);
                gainedTextList[0].gameObject.SetActive(false);
            }
            else
            {
                gainButtonList[0].gameObject.SetActive(false);
                gainedTextList[0].gameObject.SetActive(true);
            }
        }

        if(isGrowthSupport2Lock)
        {
            gainButtonList[1].GetComponentInChildren<Button>().interactable = false;
            gainButtonList[1].gameObject.SetActive(true);
            gainedTextList[1].gameObject.SetActive(false);
        }
        else
        {
            gainButtonList[1].GetComponentInChildren<Button>().interactable = true;

            if(isAvailableGrowthSupport2)
            {
                gainButtonList[1].gameObject.SetActive(true);
                gainedTextList[1].gameObject.SetActive(false);
            }
            else
            {
                gainButtonList[1].gameObject.SetActive(false);
                gainedTextList[1].gameObject.SetActive(true);
            }
        }
    }

    public void OnGrowthReward1GainButtonClicked()
    {
        if(isOwnedGrowthSupport1)
            OutgameController.Instance.RequestGrowthSupportPackageReward(1, requireStage);
        else
            OutgameController.Instance.ShowToast(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_need_growth_support_package_1"));
    }

    public void OnGrowthReward2GainButtonClicked()
    {
        if(isOwnedGrowthSupport2)
            OutgameController.Instance.RequestGrowthSupportPackageReward(2, requireStage);
        else
            OutgameController.Instance.ShowToast(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_need_growth_support_package_2"));
    }
}
