﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Numerics;
using System.Globalization;

public class ListItemMercenary : MonoBehaviour
{
    [SerializeField]
	private Text rankInfoText;
    [SerializeField]
	private Text countInfoText;
    [SerializeField]
	private Image mercenaryIcon;

    [SerializeField]
	private Text mercenaryNameAndLevelText;
    [SerializeField]
	private Text statValueText;

    [SerializeField]
    private Transform heartUpgradeButton;
    [SerializeField]
	private Text requiredHeartAmountText;

    [SerializeField]
    private Transform upgradeChanceInfo;
    [SerializeField]
	private Text upgradeChanceInfoText;

    [SerializeField]
    private Button mergeButton;
    [SerializeField]
	private Text mergeInfoText;

    [SerializeField]
    private Button equipButton;
    [SerializeField]
    private Transform equipedButton;
    
    [SerializeField]
    private Transform lockPanel;

    private MercenaryModel currentMercenaryInfo;
    private MercenaryInfoModel currentMercenaryBalanceInfo;

    private bool isInit = false;
    private bool isAvailableUpgrade = true;
    private bool isHeartUpgrade = true;
    private bool isAvailableMerge = true;

    private string id;
    public string Id
    {
        get{ 
            return id;
        }
    }

    public void UpdateEntity(string id, int level, int count, bool isOwned, bool isEquipped, bool isShowUpgradeEffect, bool isUpgradeSuccess)
	{
        this.id = id;

        // Init first time
        if(!isInit)
        {
            isInit = true;

            currentMercenaryBalanceInfo = BalanceInfoManager.Instance.MercenaryInfoList.FirstOrDefault(n => n.Id == id);

            if(currentMercenaryBalanceInfo != null)
            {
                mercenaryIcon.sprite = Resources.Load<Sprite>("Thumbnails/Mercenary_" + int.Parse(id.Substring(id.Length - 2)).ToString("D2"));
                rankInfoText.text =  LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "rank_name_" + int.Parse(id.Substring(id.Length - 2)).ToString("D2"));
            }
        }

        countInfoText.text = "x" + count.ToString();
    
        var mercenaryBasicAttackPower = BigInteger.Parse(currentMercenaryBalanceInfo.AttackPower, NumberStyles.Any, CultureInfo.InvariantCulture) * 100;
        var currentValue = mercenaryBasicAttackPower + (mercenaryBasicAttackPower / 100) * (level - 1);

        statValueText.text = 
            string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "weapon_upgrade_desc"), LanguageManager.Instance.NumberToString(currentValue))
            + "\n" + string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "mercenary_upgrade_desc"), currentMercenaryBalanceInfo.MercenaryAttackPower);
                   
        var nextRank = int.Parse(id.Substring(id.Length - 2)) + 1;

        if(isOwned)
        {
            currentMercenaryInfo = ServerNetworkManager.Instance.Inventory.MercenaryList.FirstOrDefault(n => n.Id == id);

            // Check max level
            if(level >= ServerNetworkManager.Instance.Setting.MaxMercenaryLevel)
            {
                mercenaryNameAndLevelText.text = string.Format("{0} {1}", LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "mercenary_name_" + int.Parse(id.Substring(id.Length - 2)).ToString("D2")), "\n"+ LanguageManager.Instance.GetTextData(LanguageDataType.UI, "level_max"));
                upgradeChanceInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "upgrade_percentage_info"), 0);
                requiredHeartAmountText.text = "-";

                isHeartUpgrade = false;
                isAvailableUpgrade = false;
            }
            else
            {   
                mercenaryNameAndLevelText.text = string.Format("{0}\n"+ LanguageManager.Instance.GetTextData(LanguageDataType.UI, "level")+ "{1}", LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "mercenary_name_" + int.Parse(id.Substring(id.Length - 2)).ToString("D2")), level);
                upgradeChanceInfo.gameObject.SetActive(true);

                upgradeChanceInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "upgrade_percentage_info"), currentMercenaryInfo.LevelUpChancePercent);
                requiredHeartAmountText.text = LanguageManager.Instance.NumberToString(currentMercenaryInfo.LevelupCost);

                // Check heart
                if(ServerNetworkManager.Instance.Inventory.Heart < currentMercenaryInfo.LevelupCost)
                    isAvailableUpgrade = false;
                else
                    isAvailableUpgrade = true;
            }

            int currentCount = count;

            if(isEquipped)
            {
                currentCount = currentCount - 1;

                equipButton.gameObject.SetActive(false);
                equipedButton.gameObject.SetActive(true);
            }
            else
            {
                equipButton.gameObject.SetActive(true);
                equipedButton.gameObject.SetActive(false);

                if(currentCount <= 0)
                    isAvailableUpgrade = false;
            }

            // Check count for merge
            int convertedMercenaryIndex = int.Parse(id.Substring(id.Length - 2));

            // Check this mercenary is last rank or not
            if(convertedMercenaryIndex >= ServerNetworkManager.Instance.Setting.TotalMercenaryCount)
            {
                mergeButton.gameObject.SetActive(false);
                isAvailableMerge = false;
            }
            else if(currentCount < 5)
            {
                mergeButton.gameObject.SetActive(true);
                isAvailableMerge = false;
            }
            else
            {
                mergeButton.gameObject.SetActive(true);
                isAvailableMerge = true;
            }

            if(isAvailableMerge)
                mergeInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_merge"), LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "rank_name_" + nextRank.ToString("D2")), currentCount , 5);
            else
                mergeInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_merge_none"), LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "rank_name_" + nextRank.ToString("D2")), currentCount, 5);

            mergeButton.interactable = isAvailableMerge;

            if(isShowUpgradeEffect)
                ShowUpgradeEffect(isUpgradeSuccess);
        }
        else
        {
            upgradeChanceInfo.gameObject.SetActive(true);

            mercenaryNameAndLevelText.text = string.Format("{0}\n"+ LanguageManager.Instance.GetTextData(LanguageDataType.UI, "level")+ "{1}", LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "mercenary_name_" + int.Parse(id.Substring(id.Length - 2)).ToString("D2")), level);
            upgradeChanceInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "upgrade_percentage_info"), 100);
            requiredHeartAmountText.text = LanguageManager.Instance.NumberToString(BigInteger.Parse(currentMercenaryBalanceInfo.LevelupCost, NumberStyles.Any, CultureInfo.InvariantCulture));
            mergeInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_merge_none"), LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "rank_name_" + nextRank.ToString("D2")), 0, 5);

            mergeButton.interactable = false;

            equipButton.gameObject.SetActive(true);
            equipedButton.gameObject.SetActive(false);
        }

        heartUpgradeButton.GetComponentInChildren<Button>().interactable = isAvailableUpgrade;        
        lockPanel.gameObject.SetActive(!isOwned || count <= 0);
    }

    private void ShowUpgradeEffect(bool isSuccess)
	{
	    if(isSuccess)
        {
            OutgameController.Instance.ShowToast(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_upgrade_success"));
		    SoundManager.Instance.PlaySound("Upgrade");
        }
        else
        {
            OutgameController.Instance.ShowToast(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_item_upgrade_fail"));
            SoundManager.Instance.PlaySound("UpgradeFail");
        }
	}

    public void OnMergeButtonClicked()
    {
        if(isAvailableMerge)
            GameObject.FindObjectOfType<UIPopupEquipment>().RequestMercenaryMerge(currentMercenaryBalanceInfo.Id);
    }

    public void OnEquipButtonClicked()
    {
        GameObject.FindObjectOfType<UIPopupEquipment>().RequestMercenaryEquip(currentMercenaryBalanceInfo.Id);
    }

    public void OnHeartUpgradeButtonClicked()
    {
        if(isAvailableUpgrade && isHeartUpgrade)
            GameObject.FindObjectOfType<UIPopupEquipment>().RequestMercenarypUpgrade(currentMercenaryBalanceInfo.Id);
    }
}
