﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class ListItemReceivedReward : MonoBehaviour
{
    [SerializeField]
	private List<Sprite> itemList;
    [SerializeField]
    private List<Sprite> itemTrophyList;

	[SerializeField]
	private Image itemIcon;

	[SerializeField]
	private Text itemCountText;

    public void UpdateEntity(string itemId, int itemCount)
	{
        UpdateEntity(itemId, string.Format("x {0}", itemCount));
    }

    public void UpdateEntity(string itemId, string itemCount)
	{
        if(itemId == "gem")
            itemIcon.sprite = itemList[0];
        else if(itemId == "gold")
            itemIcon.sprite = itemList[1];
        else if(itemId == "heart")
            itemIcon.sprite = itemList[2];
        else if(itemId == "ticket_raid")
            itemIcon.sprite = itemList[3];
        else if(itemId == "guild_exp")
            itemIcon.sprite = itemList[4];
        else if(itemId == "guild_token")
            itemIcon.sprite = itemList[5];
        else if(itemId == "trophy_01_01")
            itemIcon.sprite = itemTrophyList[0];
        else if(itemId == "trophy_01_02")
            itemIcon.sprite = itemTrophyList[1];
        else if(itemId == "trophy_01_03")
            itemIcon.sprite = itemTrophyList[2];
        else if(itemId == "trophy_01_04")
            itemIcon.sprite = itemTrophyList[3];
        else if(itemId == "trophy_02_01")
            itemIcon.sprite = itemTrophyList[4];
        else if(itemId == "trophy_02_02")
            itemIcon.sprite = itemTrophyList[5];
        else if(itemId == "trophy_02_03")
            itemIcon.sprite = itemTrophyList[6];
        else if(itemId == "trophy_02_04")
            itemIcon.sprite = itemTrophyList[7];
        else if(itemId == "trophy_03_01")
            itemIcon.sprite = itemTrophyList[8];
        else if(itemId == "trophy_03_02")
            itemIcon.sprite = itemTrophyList[9];
        else if(itemId == "trophy_03_03")
            itemIcon.sprite = itemTrophyList[10];
        else if(itemId == "trophy_03_04")
            itemIcon.sprite = itemTrophyList[11];
        else if(itemId == "trophy_04_01")
            itemIcon.sprite = itemTrophyList[12];
        else if(itemId == "trophy_04_02")
            itemIcon.sprite = itemTrophyList[13];
        else if(itemId == "trophy_04_03")
            itemIcon.sprite = itemTrophyList[14];
        else if(itemId == "trophy_04_04")
            itemIcon.sprite = itemTrophyList[15];

		itemCountText.text = itemCount;
    }
}
