﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Linq;
using System.Collections.Generic;

public class ListItemGrowthSupportReward2 : MonoBehaviour
{
    [SerializeField]
	private Text requireLevelText;
    [SerializeField]
	private List<Text> gemAmountTextList;

    [SerializeField]
    private List<Transform> gainButtonList;
    [SerializeField]
	private List<Transform> gainedTextList;

    private int requireStage;
    private bool isOwnedGrowthSupport1;
    private bool isOwnedGrowthSupport2;

    private double lastClickTimeStampMilliseconds = 0;

    public void UpdateEntity(int requireStage, bool isAvailableGrowthSupportFree, bool isGrowthSupportFreeLock, bool isOwnedGrowthSupport1, bool isAvailableGrowthSupport1, bool isGrowthSupport1Lock, bool isOwnedGrowthSupport2, bool isAvailableGrowthSupport2, bool isGrowthSupport2Lock)
	{
        this.requireStage = requireStage;
        this.isOwnedGrowthSupport1 = isOwnedGrowthSupport1;
        this.isOwnedGrowthSupport2 = isOwnedGrowthSupport2;
        
        requireLevelText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "growth_support_clear_stage"), requireStage);

        // Ge growth support free reward info
        var freeGrowthRewardTable = ServerNetworkManager.Instance.FreeGrowthRewardTable;

        if(freeGrowthRewardTable != null && freeGrowthRewardTable.GrowthSupportFreeRewardList != null)
        {
            var tagetRewardInfo = freeGrowthRewardTable.GrowthSupportFreeRewardList.FirstOrDefault(n => n.RequireStage == requireStage);
            
            if(tagetRewardInfo != null)
                gemAmountTextList[0].text = tagetRewardInfo.ItemCount.ToString();
            else
                gemAmountTextList[0].text = "-";
        }
        else
            gemAmountTextList[0].text = "-";
        
        // Get growth support pakcage reward info
        var tmpGrowthSupportPackage1 = ServerNetworkManager.Instance.Catalog.GrowthPackageList.FirstOrDefault(n => n.Id == "bundle_package_growth_1");
        var tmpGrowthSupportPackage2 = ServerNetworkManager.Instance.Catalog.GrowthPackageList.FirstOrDefault(n => n.Id == "bundle_package_growth_2");
        
        if(tmpGrowthSupportPackage1 != null && tmpGrowthSupportPackage2 != null)
        {
            if(tmpGrowthSupportPackage1.GrowthRewardList.Exists(n => n.ItemId == "gem"))
            {
                gemAmountTextList[1].text = tmpGrowthSupportPackage1.GrowthRewardList.FirstOrDefault(n => n.ItemId == "gem").Amount.ToString();
            }
            
            if(tmpGrowthSupportPackage2.GrowthRewardList.Exists(n => n.ItemId == "gem"))
            {
                gemAmountTextList[2].text = tmpGrowthSupportPackage2.GrowthRewardList.FirstOrDefault(n => n.ItemId == "gem").Amount.ToString();
            }
        }

        // Gain button setting
        if(isGrowthSupportFreeLock)
        {
            gainButtonList[0].GetComponentInChildren<Button>().interactable = false;
            gainButtonList[0].gameObject.SetActive(true);
            gainedTextList[0].gameObject.SetActive(false);
        }
        else
        {
            gainButtonList[0].GetComponentInChildren<Button>().interactable = true;

            if(isAvailableGrowthSupportFree)
            {
                gainButtonList[0].gameObject.SetActive(true);
                gainedTextList[0].gameObject.SetActive(false);
            }
            else
            {
                gainButtonList[0].gameObject.SetActive(false);
                gainedTextList[0].gameObject.SetActive(true);
            }   
        }

        if(isGrowthSupport1Lock)
        {
            gainButtonList[1].GetComponentInChildren<Button>().interactable = false;
            gainButtonList[1].gameObject.SetActive(true);
            gainedTextList[1].gameObject.SetActive(false);
        }
        else
        {
            gainButtonList[1].GetComponentInChildren<Button>().interactable = true;

            if(isAvailableGrowthSupport1)
            {
                gainButtonList[1].gameObject.SetActive(true);
                gainedTextList[1].gameObject.SetActive(false);
            }
            else
            {
                gainButtonList[1].gameObject.SetActive(false);
                gainedTextList[1].gameObject.SetActive(true);
            }
        }

        if(isGrowthSupport2Lock)
        {
            gainButtonList[2].GetComponentInChildren<Button>().interactable = false;
            gainButtonList[2].gameObject.SetActive(true);
            gainedTextList[2].gameObject.SetActive(false);
        }
        else
        {
            gainButtonList[2].GetComponentInChildren<Button>().interactable = true;

            if(isAvailableGrowthSupport2)
            {
                gainButtonList[2].gameObject.SetActive(true);
                gainedTextList[2].gameObject.SetActive(false);
            }
            else
            {
                gainButtonList[2].gameObject.SetActive(false);
                gainedTextList[2].gameObject.SetActive(true);
            }
        }
    }

    public void OnGrowthRewardFreeGainButtonClicked()
    {
        var passedMilliSeconds = DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastClickTimeStampMilliseconds;

		if(passedMilliSeconds >= 500d)
		{
            lastClickTimeStampMilliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();
            
            OutgameController.Instance.RequestGrowthSupportPackageReward(0, requireStage);
        }
    }

    public void OnGrowthReward1GainButtonClicked()
    {
        var passedMilliSeconds = DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastClickTimeStampMilliseconds;

		if(passedMilliSeconds >= 500d)
		{
            lastClickTimeStampMilliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();
            
            if(isOwnedGrowthSupport1)
                OutgameController.Instance.RequestGrowthSupportPackageReward(1, requireStage);
            else
                OutgameController.Instance.ShowToast(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_need_growth_support_package_1"));
        }
    }

    public void OnGrowthReward2GainButtonClicked()
    {
        var passedMilliSeconds = DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastClickTimeStampMilliseconds;

		if(passedMilliSeconds >= 500d)
		{
            lastClickTimeStampMilliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();

            if(isOwnedGrowthSupport2)
                OutgameController.Instance.RequestGrowthSupportPackageReward(2, requireStage);
            else
                OutgameController.Instance.ShowToast(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_need_growth_support_package_2"));
        }
    }
}
