﻿using UnityEngine;
using UnityEngine.UI;
using System.Numerics;
using System.Collections.Generic;

public class ListItemStat : MonoBehaviour
{
    [SerializeField]
	private Image statIcon;
    [SerializeField]
	private List<Sprite> statIconList;
    [SerializeField]
	private Text statNameAndLevelText;
    [SerializeField]
	private Text statValueText;
    [SerializeField]
	private Text requireGoldText;
    [SerializeField]
    private Button upgradeButton;
    [SerializeField]
    private Transform lockPanel;
    [SerializeField]
    private Text lockText;

    private StatType statType;
    private bool isLock;
    private bool isAvailableUpgrade = true;

    private bool isPressed = false;
    private bool isPointUpActionEnabled = false;
    private float lastTime = 0;

    private void Update()
    {
        if(isPressed && (Time.realtimeSinceStartup - lastTime) > 0.1f)
        {
            if(isAvailableUpgrade)
                GameObject.FindObjectOfType<UIPopupUpgrade>().RequestStatUpgrade(statType);
            lastTime = Time.realtimeSinceStartup;
            isPointUpActionEnabled = false;
        }
    }

    public void UpdateEntity(StatType statType, int level, bool isLock, bool isShowUpgradeEffect)
	{
        this.statType = statType;
        this.isLock = isLock;
        statIcon.sprite = statIconList[(int)statType];
        
        BigInteger requiredGold = 0;

        BigInteger currentValue = 0;
        BigInteger nextValue = 0;
        float currentFloatValue = 0f;
        float nextFloatValue = 0f;

        if(isLock)
            lockPanel.gameObject.SetActive(true);
        else
            lockPanel.gameObject.SetActive(false);
        
        switch(statType)
        {
            case StatType.ATTACK_POWER:
                statNameAndLevelText.text = string.Format("{0} ("+ LanguageManager.Instance.GetTextData(LanguageDataType.UI, "level") + "{1})", LanguageManager.Instance.GetTextData(LanguageDataType.UI, "stat_attack"), level);
                currentValue = ServerNetworkManager.Instance.User.Upgrade.BaseAttack;
                nextValue = ServerNetworkManager.Instance.User.Upgrade.NextBaseAttack;
                statValueText.text = string.Format("{0} ▶ {1}", LanguageManager.Instance.NumberToString(currentValue), LanguageManager.Instance.NumberToString(nextValue));
                requiredGold = ServerNetworkManager.Instance.User.Upgrade.BaseAttackLevelupPrice;
            break;
            case StatType.CRITICAL_POWER:
                statNameAndLevelText.text = string.Format("{0} ("+ LanguageManager.Instance.GetTextData(LanguageDataType.UI, "level") + "{1})", LanguageManager.Instance.GetTextData(LanguageDataType.UI, "stat_critical_power"), level);
                currentValue = ServerNetworkManager.Instance.User.Upgrade.CriticalAttackPercent;
                nextValue = ServerNetworkManager.Instance.User.Upgrade.NextCriticalAttackPercent;
                statValueText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "stat_critical_power_desc"), LanguageManager.Instance.NumberToString(currentValue), LanguageManager.Instance.NumberToString(nextValue));
                requiredGold = ServerNetworkManager.Instance.User.Upgrade.CriticalAttackLevelupPrice;
            break;
            case StatType.CRITICAL_ATTACK_CHANCE:
                statNameAndLevelText.text = string.Format("{0} ("+ LanguageManager.Instance.GetTextData(LanguageDataType.UI, "level") + "{1})", LanguageManager.Instance.GetTextData(LanguageDataType.UI, "stat_critical_chance"), level);
                currentFloatValue = ServerNetworkManager.Instance.User.Upgrade.CriticalChance * 100f;
                nextFloatValue = Mathf.Min(ServerNetworkManager.Instance.User.Upgrade.NextCriticalChance * 100f, 100f);
                if(currentFloatValue < 100f)
                    statValueText.text = string.Format("{0}% ▶ {1}%", currentFloatValue.ToString("F2"), nextFloatValue.ToString("F2"));
                else
                    statValueText.text = "100%";
                requiredGold = ServerNetworkManager.Instance.User.Upgrade.CriticalChanceLevelupPrice;
            break;
            case StatType.SUPER_POWER:
                statNameAndLevelText.text = string.Format("{0} ("+ LanguageManager.Instance.GetTextData(LanguageDataType.UI, "level") + "{1})", LanguageManager.Instance.GetTextData(LanguageDataType.UI, "stat_super_power"), level);
                currentValue = ServerNetworkManager.Instance.User.Upgrade.SuperAttackPercent;
                nextValue = ServerNetworkManager.Instance.User.Upgrade.NextSuperAttackPercent;
                statValueText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "stat_super_power_desc"), LanguageManager.Instance.NumberToString(currentValue), LanguageManager.Instance.NumberToString(nextValue));
                requiredGold = ServerNetworkManager.Instance.User.Upgrade.SuperAttackLevelupPrice;
            break;
            case StatType.SUPER_ATTACK_CHANCE:
                statNameAndLevelText.text = string.Format("{0} ("+ LanguageManager.Instance.GetTextData(LanguageDataType.UI, "level") + "{1})", LanguageManager.Instance.GetTextData(LanguageDataType.UI, "stat_super_chance"), level);
                currentFloatValue = ServerNetworkManager.Instance.User.Upgrade.SuperChance * 100f;
                nextFloatValue = Mathf.Min(ServerNetworkManager.Instance.User.Upgrade.NextSuperChance * 100f, 100f);
                if(currentFloatValue < 100f)
                    statValueText.text = string.Format("{0}% ▶ {1}%", currentFloatValue.ToString("F2"), nextFloatValue.ToString("F2"));
                else
                    statValueText.text = "100%";
                requiredGold = ServerNetworkManager.Instance.User.Upgrade.SuperChanceLevelupPrice;
            break;
            case StatType.ULTIMATE_POWER:
                statNameAndLevelText.text = string.Format("{0} ("+ LanguageManager.Instance.GetTextData(LanguageDataType.UI, "level") + "{1})", LanguageManager.Instance.GetTextData(LanguageDataType.UI, "stat_ultimate_power"), level);
                currentValue = ServerNetworkManager.Instance.User.Upgrade.UltimateAttackPercent;
                nextValue = ServerNetworkManager.Instance.User.Upgrade.NextUltimateAttackPercent;
                statValueText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "stat_ultimate_power_desc"), LanguageManager.Instance.NumberToString(currentValue), LanguageManager.Instance.NumberToString(nextValue));
                requiredGold = ServerNetworkManager.Instance.User.Upgrade.UltimateAttackLevelupPrice;
                lockText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "unlock_at_super_critical_100");
            break;
            case StatType.ULTIMATE_ATTACK_CHANCE:
                statNameAndLevelText.text = string.Format("{0} ("+ LanguageManager.Instance.GetTextData(LanguageDataType.UI, "level") + "{1})", LanguageManager.Instance.GetTextData(LanguageDataType.UI, "stat_ultimate_chance"), level);
                currentFloatValue = ServerNetworkManager.Instance.User.Upgrade.UltimateChance * 100f;
                nextFloatValue = Mathf.Min(ServerNetworkManager.Instance.User.Upgrade.NextUltimateChance * 100f, 100f);
                if(currentFloatValue < 100f)
                    statValueText.text = string.Format("{0}% ▶ {1}%", currentFloatValue.ToString("F2"), nextFloatValue.ToString("F2"));
                else
                    statValueText.text = "100%";
                requiredGold = ServerNetworkManager.Instance.User.Upgrade.UltimateChanceLevelupPrice;
                lockText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "unlock_at_super_critical_100");
            break;
            case StatType.HYPER_POWER:
                statNameAndLevelText.text = string.Format("{0} ("+ LanguageManager.Instance.GetTextData(LanguageDataType.UI, "level") + "{1})", LanguageManager.Instance.GetTextData(LanguageDataType.UI, "stat_hyper_power"), level);
                currentValue = ServerNetworkManager.Instance.User.Upgrade.HyperAttackPercent;
                nextValue = ServerNetworkManager.Instance.User.Upgrade.NextHyperAttackPercent;
                statValueText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "stat_hyper_power_desc"), LanguageManager.Instance.NumberToString(currentValue), LanguageManager.Instance.NumberToString(nextValue));
                requiredGold = ServerNetworkManager.Instance.User.Upgrade.HyperAttackLevelupPrice;
                lockText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "unlock_at_ultimate_critical_100");
            break;
            case StatType.HYPER_ATTACK_CHANCE:
                statNameAndLevelText.text = string.Format("{0} ("+ LanguageManager.Instance.GetTextData(LanguageDataType.UI, "level") + "{1})", LanguageManager.Instance.GetTextData(LanguageDataType.UI, "stat_hyper_chance"), level);
                currentFloatValue = ServerNetworkManager.Instance.User.Upgrade.HyperChance * 100f;
                nextFloatValue = Mathf.Min(ServerNetworkManager.Instance.User.Upgrade.NextHyperChance * 100f, 100f);
                if(currentFloatValue < 100f)
                    statValueText.text = string.Format("{0}% ▶ {1}%", currentFloatValue.ToString("F2"), nextFloatValue.ToString("F2"));
                else
                    statValueText.text = "100%";
                requiredGold = ServerNetworkManager.Instance.User.Upgrade.HyperChanceLevelupPrice;
                lockText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "unlock_at_ultimate_critical_100");
            break;
        }

        if(!ServerNetworkManager.Instance.Setting.IsHyperOpen && (statType == StatType.HYPER_POWER || statType == StatType.HYPER_ATTACK_CHANCE))
            this.gameObject.SetActive(false);
        else
            this.gameObject.SetActive(true);

        // Based on current values, adjust required gold values to be shown
        if(requiredGold != -1 && currentFloatValue < 100f)
        {
            requireGoldText.text = LanguageManager.Instance.NumberToString(requiredGold);
        }
        else
        {
            requireGoldText.text = "∞";
        }
    
        // Check gold, level, and value
        BigInteger currentGold = ServerNetworkManager.Instance.Inventory.Gold;
        if(currentGold < requiredGold || requiredGold == -1 || currentFloatValue >= 100f || level >= ServerNetworkManager.Instance.Setting.MaxStatLevel)
            isAvailableUpgrade = false;
        else
            isAvailableUpgrade = true;

        // Base on the result, make the button enabled or not
        if(isAvailableUpgrade && !isLock)
            upgradeButton.interactable = true;
        else
            upgradeButton.interactable = false;

        if(isShowUpgradeEffect)
            ShowUpgradeEffect();
    }

    private void ShowUpgradeEffect()
	{
		SoundManager.Instance.PlaySound("Upgrade");
	}

    public void OnPointerDown()
    {
        lastTime = Time.realtimeSinceStartup;
        isPressed = true;
        isPointUpActionEnabled = true;
    }
    
    public void OnPointerUp()
    {
        if(isPressed && isPointUpActionEnabled)
        {
            if(isAvailableUpgrade)
                GameObject.FindObjectOfType<UIPopupUpgrade>().RequestStatUpgrade(statType);
        }
        isPressed = false;
        isPointUpActionEnabled = false;
    }
}
