﻿using System;
using System.Numerics;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class ListItemSystemPost : MonoBehaviour
{
    [SerializeField]
	private List<Sprite> itemList;
	[SerializeField]
	private Image currencyIcon;
	[SerializeField]
	private Image itemIcon;
	[SerializeField]
	private Image emptyItemIcon;
	[SerializeField]
	private Text itemCountText;
	[SerializeField]
	private Text currencyCountText;
	[SerializeField]
	private Text messageText;
	[SerializeField]
	private Button deleteButton;
	
	private bool isInit = false;
	private int mailId;
	private SystemPostRewardModel attachedItemInfo;

	[SerializeField]
	private Text timeText;
	private double expiredTime = 0f;

	private void Start()
	{
		StartCoroutine(UpdateTimeCoroutine());
	}

    private IEnumerator UpdateTimeCoroutine()
    {
        while(true)
        {
			if(isInit)
			{
				var timeLeft = (int)(expiredTime - DateTimeOffset.Now.ToUnixTimeSeconds());

				if(timeLeft < 0)
				{
					this.timeText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "time_end");
					
					if(attachedItemInfo != null)
						deleteButton.gameObject.SetActive(true);

					break;
				}
				else
				{
					int hours = timeLeft / 3600;
					timeLeft -= hours * 3600;
					int minutes = timeLeft / 60;
					timeLeft -= minutes * 60;
					int seconds = timeLeft;
					timeLeft -= seconds;
					
					if(hours / 24 > 0)
						this.timeText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "time_days"), (hours/24).ToString());
					else if(hours > 0)
						this.timeText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "time_hours"), hours.ToString());
					else if(minutes > 0)
						this.timeText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "time_mins"), minutes.ToString());
					else
						this.timeText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "time.end_soon");
				}
			}
			yield return new WaitForSeconds(1f);
		}
	}
	
	public void UpdateEntity(int id, string message, double expiredTime, SystemPostRewardModel attachedItem)
	{
		mailId = id;
		
		var convertMessage = LanguageManager.Instance.GetTextData(LanguageDataType.UI, message);
		
		if(convertMessage != "")
			this.messageText.text = convertMessage;
		else
		{
			string[] separatingStrings = { "<s>", "<m>" };
			string[] words = message.Split(separatingStrings, System.StringSplitOptions.RemoveEmptyEntries);

			if(words.Length == 2)
				this.messageText.text = $"{words[0]}: {words[1]}";
			else
				this.messageText.text = message;
		}
		
		this.attachedItemInfo = attachedItem;

		if(attachedItemInfo == null)
		{
			currencyIcon.gameObject.SetActive(false);
			itemIcon.gameObject.SetActive(false);
			emptyItemIcon.gameObject.SetActive(true);
			deleteButton.gameObject.SetActive(true);
		}
		else
		{
			if(attachedItemInfo.ItemId == "gem")
			{
				currencyIcon.gameObject.SetActive(true);
				itemIcon.gameObject.SetActive(false);
				emptyItemIcon.gameObject.SetActive(false);
				
				currencyIcon.sprite = itemList[0];
				this.currencyCountText.text = string.Format("{0}", attachedItem.Count);

				deleteButton.gameObject.SetActive(false);
			}
			else if(attachedItemInfo.ItemId == "gold")
			{
				currencyIcon.gameObject.SetActive(true);
				itemIcon.gameObject.SetActive(false);
				emptyItemIcon.gameObject.SetActive(false);

				currencyIcon.sprite = itemList[1];
				this.currencyCountText.text = LanguageManager.Instance.NumberToString(BigInteger.Parse(attachedItem.Count, NumberStyles.Any, CultureInfo.InvariantCulture));

				deleteButton.gameObject.SetActive(false);
			}
            else if(attachedItemInfo.ItemId == "heart")
			{
				currencyIcon.gameObject.SetActive(true);
				itemIcon.gameObject.SetActive(false);
				emptyItemIcon.gameObject.SetActive(false);

				currencyIcon.sprite = itemList[2];
				this.currencyCountText.text = LanguageManager.Instance.NumberToString(BigInteger.Parse(attachedItem.Count, NumberStyles.Any, CultureInfo.InvariantCulture));
				deleteButton.gameObject.SetActive(false);
			}
            else if(attachedItemInfo.ItemId == "ticket_raid")
			{
				currencyIcon.gameObject.SetActive(true);
				itemIcon.gameObject.SetActive(false);
				emptyItemIcon.gameObject.SetActive(false);

				currencyIcon.sprite = itemList[3];
				this.currencyCountText.text = string.Format("{0}", attachedItem.Count);

				deleteButton.gameObject.SetActive(false);
			}
            else if(attachedItemInfo.ItemId == "weapon")
			{
				currencyIcon.gameObject.SetActive(false);
				itemIcon.gameObject.SetActive(true);
				emptyItemIcon.gameObject.SetActive(false);

				itemIcon.sprite = Resources.Load<Sprite>("Thumbnails/Weapon_" + int.Parse(attachedItemInfo.ItemCode.Substring(attachedItemInfo.ItemCode.Length - 2)).ToString("D2"));
				this.itemCountText.text = string.Format("x {0}", attachedItem.Count);
				
				deleteButton.gameObject.SetActive(false);
			}
			else if(attachedItemInfo.ItemId == "class")
			{
				currencyIcon.gameObject.SetActive(false);
				itemIcon.gameObject.SetActive(true);
				emptyItemIcon.gameObject.SetActive(false);

				itemIcon.sprite = Resources.Load<Sprite>("Thumbnails/Class_" + int.Parse(attachedItemInfo.ItemCode.Substring(attachedItemInfo.ItemCode.Length - 2)).ToString("D2"));
				this.itemCountText.text = string.Format("x {0}", attachedItem.Count);
				
				deleteButton.gameObject.SetActive(false);
			}
			else if(attachedItemInfo.ItemId == "pet")
			{
				currencyIcon.gameObject.SetActive(false);
				itemIcon.gameObject.SetActive(true);
				emptyItemIcon.gameObject.SetActive(false);

				itemIcon.sprite = Resources.Load<Sprite>("Pets/Thumbnails/Pet_" + int.Parse(attachedItemInfo.ItemCode.Substring(attachedItemInfo.ItemCode.Length - 2)).ToString("D2"));
				this.itemCountText.text = string.Format("x {0}", attachedItem.Count);
				
				deleteButton.gameObject.SetActive(false);
			}
			else if(attachedItemInfo.ItemId == "mercenary")
			{
				currencyIcon.gameObject.SetActive(false);
				itemIcon.gameObject.SetActive(true);
				emptyItemIcon.gameObject.SetActive(false);

				itemIcon.sprite = Resources.Load<Sprite>("Thumbnails/Mercenary_" + int.Parse(attachedItemInfo.ItemCode.Substring(attachedItemInfo.ItemCode.Length - 2)).ToString("D2"));
				this.itemCountText.text = string.Format("x {0}", attachedItem.Count);
				
				deleteButton.gameObject.SetActive(false);
			}
			else
			{
				currencyIcon.gameObject.SetActive(false);
				itemIcon.gameObject.SetActive(false);
				emptyItemIcon.gameObject.SetActive(true);
				this.itemCountText.text = "";

				deleteButton.gameObject.SetActive(false);
			}
		}
		
		this.expiredTime = expiredTime;
		isInit = true;
	}

	public void OnDeleteButtonClick()
	{
		OutgameController.Instance.RequestDeleteSystemMail(mailId);
	}
}
