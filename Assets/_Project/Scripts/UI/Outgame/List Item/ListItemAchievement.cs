﻿using UnityEngine;
using UnityEngine.UI;
using System.Numerics;
using System.Collections.Generic;

public class ListItemAchievement : MonoBehaviour
{
    [SerializeField]
	private Text achievementNameText;
    [SerializeField]
	private Text achievementValueText;
    [SerializeField]
	private Text rewardGemText;
    [SerializeField]
    private Slider  achievementValueSlider;

    [SerializeField]
    private Button getButton;
    [SerializeField]
    private Image getButtonImage;
    [SerializeField]
    private List<Sprite> getButtonSpriteList = new List<Sprite>();

    private bool isAvailableGetReward = true;

    private bool isPressed = false;
    private bool isPointUpActionEnabled = false;
    private float lastTime = 0;
    
    private string achievementId;

    private void Update()
    {
        if(isPressed && (Time.realtimeSinceStartup - lastTime) > 0.1f)
        {
            if(isAvailableGetReward)
            {
                GameObject.FindObjectOfType<UIPopupAchievement>().RequestReward(achievementId);
                SoundManager.Instance.PlaySound("Upgrade");
            }
            lastTime = Time.realtimeSinceStartup;
            isPointUpActionEnabled = false;
        }
    }

    public void UpdateEntity(string id, int currentValue, int goalValue, int previousGoal, int rewardGemCount, bool isMax)
	{
        this.achievementId = id;
        
        getButton.gameObject.SetActive(false);

        achievementNameText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, id + "_name");
        rewardGemText.text = rewardGemCount.ToString();

        if(isMax)
        {
            achievementValueText.text = "Max";
            achievementValueSlider.value = 1f;

            isAvailableGetReward = false;
        }
        else
        {
            if(id == "achievement_02")
                achievementValueText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, id + "_progress"), currentValue / 60, goalValue / 60);
            else
                achievementValueText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, id + "_progress"), currentValue, goalValue);

            achievementValueSlider.value = Mathf.Clamp01((float)(currentValue - previousGoal) / (float)(goalValue - previousGoal));

            if(currentValue >= goalValue)
                isAvailableGetReward = true;
            else
                isAvailableGetReward = false;
        }

        getButton.interactable = isAvailableGetReward;

        if(isAvailableGetReward)
            getButtonImage.sprite = getButtonSpriteList[0];
        else
            getButtonImage.sprite = getButtonSpriteList[1];
            
        getButton.gameObject.SetActive(true);
    }

    public void OnRequestRewardButtonClicked()
    {
        if(isAvailableGetReward)
        {
            GameObject.FindObjectOfType<UIPopupAchievement>().RequestReward(achievementId);
            SoundManager.Instance.PlaySound("Upgrade");
        }
    }
}
