﻿using UnityEngine;
using UnityEngine.UI;

public class ListItemGem : MonoBehaviour {

	[SerializeField]
	private Text itemNameText;
	[SerializeField]
	private Text priceText;
	[SerializeField]
	private Text vipRewardText;
	[SerializeField]
	private Text gemAmountText;
	[SerializeField]
	private Image thumbnailImage;
	[SerializeField]
	private Transform purchaseButton;
	[SerializeField]
	private Text gemBonusInfoText;
	[SerializeField]
	private Transform gemBonusTag;

	private string gemPackageId;

	public void UpdateEntity(string id, int givingAmount, int vipReward, string price, bool isGemBonus)
	{
		vipRewardText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "vip_point") + " +" + vipReward;
		
		if(isGemBonus)
		{
			gemBonusInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "gem_bonus_info"), string.Format("{0:#,###0.#}", givingAmount));
			gemBonusTag.gameObject.SetActive(true);
		}
		else
			gemBonusTag.gameObject.SetActive(false);

		this.gemPackageId = id;
		
		var tmpImage = Resources.Load<Sprite>(("Currency/" + id));
		if(tmpImage != null)
			thumbnailImage.sprite = tmpImage;

		if(id == "bundle_gem_04")
		{
			var covertedPostion = thumbnailImage.rectTransform.position;
			covertedPostion.y = covertedPostion.y - 40;
			thumbnailImage.rectTransform.position = covertedPostion;
		}
		else if(id == "bundle_gem_05")
		{
			var covertedPostion = thumbnailImage.rectTransform.position;
			covertedPostion.y = covertedPostion.y - 40;
			thumbnailImage.rectTransform.position = covertedPostion;

			thumbnailImage.rectTransform.sizeDelta = new Vector2(560f, 560f);
		}

		itemNameText.text = LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, id);
		gemAmountText.text = string.Format("{0} " + LanguageManager.Instance.GetTextData(LanguageDataType.UI, "item_gem"), string.Format("{0:#,###0.#}", givingAmount));
		priceText.text = price;
	}

	public void OnPurchaseButton()
	{
		GameObject.FindObjectOfType<UIPopupShop>().OnPurchaseGemBundle(gemPackageId);
	}
}
