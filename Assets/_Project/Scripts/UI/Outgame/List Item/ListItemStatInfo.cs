﻿using UnityEngine;
using UnityEngine.UI;

public enum StatInfoType {NONE, ATTACK, CRITICAL_ATTACK, SUPER_CRITICAL_ATTACK, ULTIMATE_ATTACK, HYPER_ATTACK, ATTACK_SPEED, 
BERSERK_POINT, BERSERK_RECHARGING, BERSERK_TIME, GOLD_NORMAL, GOLD_BERSERK, GOLD_PET, FARMING_KILL, MONSTER_HP, BOSS_MONSTER_HP};

public class ListItemStatInfo : MonoBehaviour
{
    public StatInfoType StatType;

    [SerializeField]
    private Text statText;

    [SerializeField]
    private Transform infoButton;

    private string detailInfoText;

    public void UpdateEntity(string basicInfotext, string value, string subValue, string detailInfoText)
    {
        this.detailInfoText = detailInfoText;

        if(subValue != "")
            statText.text = string.Format(basicInfotext, value, subValue);
        else
            statText.text = string.Format(basicInfotext, value);

        if(detailInfoText != "")
            infoButton.gameObject.SetActive(true);
        else
            infoButton.gameObject.SetActive(false);
    }

    public void OnInfoButtonClicked()
    {
        OutgameController.Instance.ShowGenericInfo(detailInfoText);
    }
}
