﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Globalization;

public class ListItemGuildRelic : MonoBehaviour
{
    [SerializeField]
	private Image relicIcon;

    [SerializeField]
	private Text relicNameAndLevelText;
    [SerializeField]
	private Text statValueText;

    [SerializeField]
	private Text upgradeChanceInfoText;
    [SerializeField]
    private Transform upgradeChanceInfo;
    [SerializeField]
	private Text upgradePriceText;
    [SerializeField]
    private Button upgradeButton;

    [SerializeField]
    private Transform lockPanel;
    [SerializeField]
	private Text unlockPriceText;
    [SerializeField]
    private Button unlockButton;

    private GuildRelicInfoModel currentGuildRelicInfo;

    private bool isInit = false;
    private int price = 0; 
    private bool isAvailableUnlockOrUpgrade = true;

    private string id;
    public string Id
    {
        get{ 
            return id;
        }
    }

    public void UpdateEntity(string id, int level, int price, bool isOwned, bool IsMaxLevel, bool isShowUpgradeEffect)
	{
        this.price = price;
        this.id = id;

        // Init first time
        if(!isInit)
        {
            currentGuildRelicInfo = BalanceInfoManager.Instance.GuildRelicInfoList.FirstOrDefault(n => n.Id == id);

            if(currentGuildRelicInfo != null)
                relicIcon.sprite = Resources.Load<Sprite>("Thumbnails/Guild_Relic_" + int.Parse(id.Substring(id.Length - 2)).ToString("D2"));

            upgradePriceText.text = price.ToString();
            unlockPriceText.text = price.ToString();
        }

        // Check guild token
        if(ServerNetworkManager.Instance.Inventory.GuildToken >= price)
            isAvailableUnlockOrUpgrade = true;
        else
            isAvailableUnlockOrUpgrade = false;
    
        var relicBasicEffect = int.Parse(currentGuildRelicInfo.DefaultEffect);
        var currentValue = relicBasicEffect + Mathf.Abs(float.Parse(currentGuildRelicInfo.EffectPower, NumberStyles.Any, CultureInfo.InvariantCulture)) * level;      
        bool isFloat = false;

        if((currentValue - Mathf.FloorToInt(currentValue)) != 0)
            isFloat = true;
        
        if(isFloat)
            statValueText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, id + "_upgrade_desc"), currentValue.ToString("F2"));
        else
            statValueText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, id + "_upgrade_desc"), currentValue);
        
        if(isOwned)
        {
            upgradeChanceInfo.gameObject.SetActive(true);
            upgradeButton.gameObject.SetActive(true);
            lockPanel.gameObject.SetActive(false);
            
            int maxLevel = currentGuildRelicInfo.MaxLevel;

            // Check max level
            if(IsMaxLevel)
            {
                relicNameAndLevelText.text = string.Format("{0} {1}", LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "guild_relic_name_" + int.Parse(id.Substring(id.Length - 2)).ToString("D2")), "\n"+ LanguageManager.Instance.GetTextData(LanguageDataType.UI, "level_max"));
                upgradeChanceInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "upgrade_percentage_info"), 0);

                upgradeButton.interactable = false;
            }
            else
            {
                relicNameAndLevelText.text = string.Format("{0}\n"+ LanguageManager.Instance.GetTextData(LanguageDataType.UI, "level") + "{1}", LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "guild_relic_name_" + int.Parse(id.Substring(id.Length - 2)).ToString("D2")), level);
                upgradeChanceInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "upgrade_percentage_info"), 100);

                upgradeButton.interactable = isAvailableUnlockOrUpgrade;
                unlockButton.interactable = false;
            }

            if(isShowUpgradeEffect)
            {
                if(level == 1)
                    ShowUnlockEffect();
                else
                    ShowUpgradeEffect();
            }
        }
        else
        {
            upgradeChanceInfo.gameObject.SetActive(false);
            upgradeButton.gameObject.SetActive(false);
            lockPanel.gameObject.SetActive(true);

            relicNameAndLevelText.text = string.Format("{0}\n"+ LanguageManager.Instance.GetTextData(LanguageDataType.UI, "level") + "{1}", LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "guild_relic_name_" + int.Parse(id.Substring(id.Length - 2)).ToString("D2")), level);
            upgradeChanceInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "upgrade_percentage_info"), 100);

            upgradeButton.interactable = false;
            unlockButton.interactable = isAvailableUnlockOrUpgrade;
        }
    }

    private void ShowUnlockEffect()
	{
        var text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_hunt_got_item"),  LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "guild_relic_name_" + int.Parse(id.Substring(id.Length - 2)).ToString("D2")));
	    OutgameController.Instance.ShowToast(text);
		SoundManager.Instance.PlaySound("Upgrade");
	}

    private void ShowUpgradeEffect()
	{
	    OutgameController.Instance.ShowToast(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_upgrade_success"));
		SoundManager.Instance.PlaySound("Upgrade");
	}

    public void OnUnlockButtonClicked()
    {
        if(isAvailableUnlockOrUpgrade)
        {
            if(ServerNetworkManager.Instance.Inventory.GuildToken < price)
                UIManager.Instance.ShowAlertLocalized("message_not_enough_guild_token", null, null);
            else
                GameObject.FindObjectOfType<UIPopupGuild>().OnGuildRelicUpgrade(id, true);
        }
    }

    public void OnUpgradeButtonClicked()
    {
        if(isAvailableUnlockOrUpgrade)
        {
            if(ServerNetworkManager.Instance.Inventory.GuildToken < price)
                UIManager.Instance.ShowAlertLocalized("message_not_enough_guild_token", null, null);
            else
                GameObject.FindObjectOfType<UIPopupGuild>().OnGuildRelicUpgrade(id, false);
        }
    }
}
