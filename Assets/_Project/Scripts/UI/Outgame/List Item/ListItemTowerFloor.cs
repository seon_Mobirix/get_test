﻿using UnityEngine;
using UnityEngine.UI;

public class ListItemTowerFloor : MonoBehaviour
{
    [SerializeField]
    private Text floorText = null;
    [SerializeField]
    private Text teamCountText = null;
    [SerializeField]
    private Transform occupyInfo;
    [SerializeField]
    private Transform unoccupyInfo;

    [SerializeField]
    private Button playersButton = null;
    private int floor = 0;

    public void UpdateEntity(int floor, int teamCountCurrent, int teamCountMax, bool isOccupy, bool isComplete)
    {
        occupyInfo.gameObject.SetActive(isOccupy);
        unoccupyInfo.gameObject.SetActive(!isOccupy);

        floorText.text = floor.ToString() + "F";
        
        this.floor = floor;
        
        if(floor == 1)
        {
            teamCountText.text = "∞";
            playersButton.gameObject.SetActive(false);
        }
        else if(teamCountCurrent == -1)
        {
            teamCountText.text = "";
            playersButton.gameObject.SetActive(true);
        }
        else
        {
            teamCountText.text = Mathf.Min(teamCountCurrent, teamCountMax).ToString() + " / " + teamCountMax.ToString();
            playersButton.gameObject.SetActive(true);
        }
    }

    public void OnPlayerInfoButtonClicked()
    {
        GameObject.FindObjectOfType<UIPopupTower>().OnPlayerListButtonClicked(floor);
    }
}