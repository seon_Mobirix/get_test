﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;

public class ListItemGuildMember : MonoBehaviour
{
    [SerializeField]
	private Text memberRoleText;
    [SerializeField]
	private List<Color32> memberRoleColorList;
    [SerializeField]
	private Text lastLoginInfoText;
    [SerializeField]
	private List<Color32> lastLoginInfoColorList;
    [SerializeField]
    private Image roleIcon;
    [SerializeField]
    private List<Sprite> roleIconList = new List<Sprite>();
    [SerializeField]
	private Text stageProgressText;
    [SerializeField]
	private Text nameText;
    [SerializeField]
	private Text totalContributionText;
    [SerializeField]
	private Text raidScoreText;
    [SerializeField]
    private Transform buttonBackground;
    [SerializeField]
    private Transform cogButton;
    [SerializeField]
	private Transform kickButton;
    [SerializeField]
    private Transform transferButton;

    private string memberId;
    public string MemberId{
        get{
            return memberId;
        }
    }
    private string memeberName;
    private bool isGuildMaster;
    private bool isCogButtonOpened = false;

    public void UpdateEntity(string memberId, string name, int stageProgress, int totalContribution, int raidScore, int lastTimeStamp, bool isGuildMaster)
	{
        this.memberId = memberId;
        this.memeberName = name;
        this.isGuildMaster = isGuildMaster;
        this.isCogButtonOpened = false;
        
        stageProgressText.text = string.Format("Stage {0}", stageProgress);
        nameText.text = name;
        totalContributionText.text = totalContribution.ToString();
        raidScoreText.text = raidScore.ToString();
        
        TimeConverter(lastTimeStamp);

        if(isGuildMaster)
        {
            memberRoleText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "guild_master");
            memberRoleText.color = memberRoleColorList[0];
            roleIcon.sprite = roleIconList[0];
            
            cogButton.gameObject.SetActive(false);
        }
        else
        {
            memberRoleText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "guild_member");
            memberRoleText.color = memberRoleColorList[1];
            roleIcon.sprite = roleIconList[1];
            
            if(ServerNetworkManager.Instance.User.PlayFabId == ServerNetworkManager.Instance.GuildData.GuildManagementData.GuildId)
                cogButton.gameObject.SetActive(true);
            else
                cogButton.gameObject.SetActive(false);
        }

        buttonBackground.gameObject.SetActive(false);
        kickButton.gameObject.SetActive(false);
        transferButton.gameObject.SetActive(false);
    }

    public void OffCogButton()
    {
        if(kickButton.gameObject.activeSelf || transferButton.gameObject.activeSelf)
        {
            isCogButtonOpened = false;
            buttonBackground.gameObject.SetActive(false);
            kickButton.gameObject.SetActive(false);
            transferButton.gameObject.SetActive(false);
        }
    }

    public void OnCogButtonClicked()
    {
        if(kickButton.gameObject.activeSelf || transferButton.gameObject.activeSelf)
        {
            isCogButtonOpened = false;
            buttonBackground.gameObject.SetActive(false);
            kickButton.gameObject.SetActive(false);
            transferButton.gameObject.SetActive(false);
        }
        else
        {
            isCogButtonOpened = true;
            buttonBackground.gameObject.SetActive(true);
            kickButton.gameObject.SetActive(true);
            transferButton.gameObject.SetActive(true);

            GameObject.FindObjectOfType<UIPopupGuild>().OffCogButtonsWithOutSelected(this.memberId);
        }
    }

    public void OnKickButtonClicked()
    {
        var message = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_guild_kick"), nameText.text);

        UIManager.Instance.ShowAlert(message, () => {
			OutgameController.Instance.KickGuildMember(memberId, memeberName);
		}, () => {
            // Nothing to do.
		});
    }

    public void OnTransferButtonClicked()
    {
        var message = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_guild_give_master"), nameText.text);

        UIManager.Instance.ShowAlert(message, () => {
			OutgameController.Instance.GuildMasterTransfer(memberId);
		}, () => {
            // Nothing to do.
		});
    }

    private void TimeConverter(int lastTimeStamp)
    {
        if(lastTimeStamp == -1)
        {
            lastLoginInfoText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "active_time_unknown");
            lastLoginInfoText.color = lastLoginInfoColorList[0];
        }
        else
        {
            var myGuildData = ServerNetworkManager.Instance.GuildData.GuildMemberData.MemberCacheDataList.FirstOrDefault(n => n.Id == ServerNetworkManager.Instance.User.PlayFabId);

            if(myGuildData != null)
            {
                var myLastTimeStamp = myGuildData.LastTimeStamp;
                var hourDifference = Mathf.Abs(myLastTimeStamp - lastTimeStamp);

                if(hourDifference < 1)
                {
                    lastLoginInfoText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "active_time_recent");
                    lastLoginInfoText.color = lastLoginInfoColorList[1];
                }
                else if(hourDifference < 24)
                {
                    lastLoginInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "active_time_hours"), hourDifference);
                    lastLoginInfoText.color = lastLoginInfoColorList[1];
                }
                else
                {
                    var dayDifference = hourDifference/24;
                    lastLoginInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "active_time_days"), dayDifference);

                    if(dayDifference <= 3)
                        lastLoginInfoText.color = lastLoginInfoColorList[1];
                    else if(dayDifference <= 7)
                        lastLoginInfoText.color = lastLoginInfoColorList[2];
                    else
                        lastLoginInfoText.color = lastLoginInfoColorList[3];
                }
            }
            else
            {
                lastLoginInfoText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "active_time_unknown");
                lastLoginInfoText.color = lastLoginInfoColorList[0];
            }
        }
    }
}