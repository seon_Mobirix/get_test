﻿using UnityEngine;
using UnityEngine.UI;
using SB;

public class ListItemMoreGame : MonoBehaviour
{
    public string packageInfo = "";

    public Image _imgShadow = null;

    public Image _imgIcon = null;

    public Image _imgGotIt = null;

    public void OnItemClick()
    {
        MoreManager.openMoregameUrl(packageInfo);
    }
}
