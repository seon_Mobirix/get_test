﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class ListItemRanking : MonoBehaviour
{
    [SerializeField]
    private Text rankText;
    [SerializeField]
    private Image rankIcon;
    [SerializeField]
    private List<Sprite> rankIconList;
    [SerializeField]
    private Text nameText;
    [SerializeField]
    private Text scoreText;

	public void UpdateEntity(string name, int rank, int world, int stage, int killCount)
	{
        if(rank == -1)
        {
            rankIcon.gameObject.SetActive(false);
            rankText.gameObject.SetActive(true);
            rankText.text = "-";
        }
        if(rank < 4)
        {
            rankIcon.sprite = rankIconList[rank - 1];
            rankIcon.gameObject.SetActive(true);
            rankText.gameObject.SetActive(false);
        }
        else
        {
            rankIcon.gameObject.SetActive(false);
            rankText.gameObject.SetActive(true);
            rankText.text = rank.ToString();
        }
       
        nameText.text = name;
        scoreText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "ranking_world_score"), world, stage, killCount);
	}

    public void UpdateEntity(string name, int rank, int score)
	{
        if(rank == -1)
        {
            rankIcon.gameObject.SetActive(false);
            rankText.gameObject.SetActive(true);
            rankText.text = "-";
        }
        else if(rank < 4)
        {
            rankIcon.sprite = rankIconList[rank - 1];
            rankIcon.gameObject.SetActive(true);
            rankText.gameObject.SetActive(false);
        }
        else
        {
            rankIcon.gameObject.SetActive(false);
            rankText.gameObject.SetActive(true);
            rankText.text = rank.ToString();
        }
       
        nameText.text = name;
        scoreText.text = score.ToString();
	}
}