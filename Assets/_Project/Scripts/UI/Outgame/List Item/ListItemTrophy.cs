﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Numerics;
using Sirenix.OdinInspector;

public class ListItemTrophy : MonoBehaviour
{
    private string trophyId = "";
    public string TrophyId{ 
        get{ 
            return trophyId; 
        }
    }

    [Title("Manage Trophy")]

    [SerializeField]
    private Image thumbnailImage = null;

    [SerializeField]
    private Text nameAndLevelText = null;

    [SerializeField]
    private Text countText = null;

    [SerializeField]
    private Text infoText = null;

    [SerializeField]
    private Button heartLevelUpButton = null;

    [SerializeField]
    private Text heartLevelUpText = null;

    [SerializeField]
    private Button trophyLevelUpButton = null;

    [SerializeField]
    private Image trophyLevelUpIcon = null;

    [SerializeField]
    private Text trophyLevelUpText = null;

    [SerializeField]
    private Button equipButton = null;

    [SerializeField]
    private Text equipText = null;

    [SerializeField]
    private GameObject disableCover = null;

    [Title("Sell Trophys")]
    
    [SerializeField]
    private GameObject sellCover = null;

    [SerializeField]
    private Text rewardForSellText = null;

    [SerializeField]
    private Text availToSellText = null;

    [SerializeField]
    private Button sellButton = null;

    public void UpdateEntity(bool isSellMode, string id, int level, int count, int reversedRarity, bool isOwned, bool isEquipped, bool isShowUpgradeEffect)
	{
        this.trophyId = id;

        var currentTrophyBalanceInfo = BalanceInfoManager.Instance.TrophyInfoList.FirstOrDefault(n => n.Id == id);

        thumbnailImage.sprite = Resources.Load<Sprite>("Trophies/Trophy_" + id.Substring(id.Length - 5));
        trophyLevelUpIcon.sprite = thumbnailImage.sprite;
        countText.text = "x" + count;

        if(isOwned)
        {
            disableCover.gameObject.SetActive(false);

            var currentTrophyInfo = ServerNetworkManager.Instance.Inventory.TrophyList.FirstOrDefault(n => n.Id == id);
            
            if(id.Contains("trophy_01"))
                infoText.text = string.Format(
                    LanguageManager.Instance.GetTextData(LanguageDataType.UI, "trophy_effect_desc_" + id.Substring(id.Length - 5, 2)), 
                    LanguageManager.Instance.NumberToString((int)(currentTrophyInfo.EffectPowerWithMultiply * 100f) * NumberMainController.Instance.FinalBestGoldIncome * 3 / 100)
                );
            else if(id.Contains("trophy_02") || id.Contains("trophy_04"))
                infoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "trophy_effect_desc_" + id.Substring(id.Length - 5, 2)), currentTrophyInfo.EffectPowerWithAddition);
            else if(id.Contains("trophy_03"))
                infoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "trophy_effect_desc_" + id.Substring(id.Length - 5, 2)), LanguageManager.Instance.NumberToString((BigInteger)currentTrophyInfo.EffectPowerWithMultiply * (1 + ServerNetworkManager.Instance.Inventory.TotalHeartCost / 10000 / 24)));
            else 
                infoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "trophy_effect_desc_" + id.Substring(id.Length - 5, 2)), currentTrophyInfo.EffectPowerWithMultiply);

            heartLevelUpText.text = LanguageManager.Instance.NumberToString(currentTrophyInfo.HeartLevelupCost);
            trophyLevelUpText.text = "x" + currentTrophyInfo.TrophyLevelupCost.ToString();

            if(level >= ServerNetworkManager.Instance.Setting.MaxTrophyLevel)
            {
                nameAndLevelText.text = LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "trophy_name_" + id.Substring(id.Length - 5)) + " " + LanguageManager.Instance.GetTextData(LanguageDataType.UI, "level_max");

                heartLevelUpButton.interactable = false;
                trophyLevelUpButton.interactable = false;
            }
            else
            {
                nameAndLevelText.text = LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "trophy_name_" + id.Substring(id.Length - 5)) + " " + LanguageManager.Instance.GetTextData(LanguageDataType.UI, "level") + level;

                if(currentTrophyInfo.HeartLevelupCost <= ServerNetworkManager.Instance.Inventory.Heart)
                    heartLevelUpButton.interactable = true;
                else
                    heartLevelUpButton.interactable = false;

                if(currentTrophyInfo.TrophyLevelupCost <= count)
                    trophyLevelUpButton.interactable = true;
                else
                    trophyLevelUpButton.interactable = false;
            }

            if(ServerNetworkManager.Instance.Inventory.SelectedTrophyList.Exists(n => n.Id == currentTrophyInfo.Id))
            {
                equipButton.interactable = false;
                equipText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_in_slot");
            }
            else if(ServerNetworkManager.Instance.Inventory.SelectedTrophyList.Count >= ServerNetworkManager.Instance.Inventory.TrophyCountByBestClass)
            {
                equipButton.interactable = false;
                equipText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_to_slot");
            }
            else
            {
                equipButton.interactable = true;
                equipText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_to_slot");
            }

            if(isSellMode)
            {
                sellCover.gameObject.SetActive(true);
                rewardForSellText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "trophy_sell_heart"), reversedRarity);
                availToSellText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "trophy_sell_avail"), Mathf.Max(count - 1, 0));
                if(count > 1)
                {
                    rewardForSellText.gameObject.SetActive(true);
                    availToSellText.gameObject.SetActive(true);
                    sellButton.gameObject.SetActive(true);
                }
                else
                {
                    rewardForSellText.gameObject.SetActive(false);
                    availToSellText.gameObject.SetActive(false);
                    sellButton.gameObject.SetActive(false);
                }
            }
            else
            {
                sellCover.gameObject.SetActive(false);
            }
        }
        else
        {
            if(isSellMode)
            {
                disableCover.gameObject.SetActive(false);
                sellCover.gameObject.SetActive(true);
                rewardForSellText.gameObject.SetActive(false);
                availToSellText.gameObject.SetActive(false);
                sellButton.gameObject.SetActive(false);
            }
            else
            {
                disableCover.gameObject.SetActive(true);
                sellCover.gameObject.SetActive(false);
            }

            if(id.Contains("trophy_01"))
                infoText.text = string.Format(
                    LanguageManager.Instance.GetTextData(LanguageDataType.UI, "trophy_effect_desc_" + id.Substring(id.Length - 5, 2)), 
                    LanguageManager.Instance.NumberToString(int.Parse(currentTrophyBalanceInfo.EffectPower) * NumberMainController.Instance.FinalBestGoldIncome * 3)
                );
            else
                infoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "trophy_effect_desc_" + id.Substring(id.Length - 5, 2)), currentTrophyBalanceInfo.EffectPower);
            
            nameAndLevelText.text = LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "trophy_name_" + id.Substring(id.Length - 5)) + " " + LanguageManager.Instance.GetTextData(LanguageDataType.UI, "level") + level;
            heartLevelUpText.text = "";
            trophyLevelUpText.text = "";
            equipText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_to_slot");

            heartLevelUpButton.interactable = false;
            trophyLevelUpButton.interactable = false;
            equipButton.interactable = false;
        }

        if(isShowUpgradeEffect)
            ShowUpgradeEffect();
    }

    private void ShowUpgradeEffect()
	{
		SoundManager.Instance.PlaySound("Upgrade");
	}

    public void OnHeartLevelupButtonClicked()
    {
        GameObject.FindObjectOfType<UIPopupRaid>().HeartLevelUp(this.trophyId);
    }

    public void OnTrophyLevelupButtonClicked()
    {
        GameObject.FindObjectOfType<UIPopupRaid>().TrophyLevelUp(this.trophyId);
    }

    public void OnEquipButtonClicked()
    {
        GameObject.FindObjectOfType<UIPopupRaid>().AddTrophyToSlot(this.trophyId);
    }

    public void OnSellbuttonClicked()
    {
        GameObject.FindObjectOfType<UIPopupRaid>().TrophySell(this.trophyId);
    }

}
