﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class ListItemKickMember: MonoBehaviour
{
    [SerializeField]
    private Text nameText;

    private string id = "";

    private double lastClickTimeStampMilliseconds = 0f;

	public void UpdateEntity(string id, string name)
	{
        this.id = id;

        nameText.text = name;
	}

    public void OnCancelButtonClick()
    {
		var passedMilliSeconds = DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastClickTimeStampMilliseconds;
		
		if(passedMilliSeconds >= 500d)
		{
			lastClickTimeStampMilliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();
			OutgameController.Instance.KickCancelGuildMember(id);
		}
	}
}