﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using Sirenix.OdinInspector;

public class ListItemPet : MonoBehaviour
{
    private string petId = "";
    public string PetId{ 
        get{ 
            return petId; 
        }
    }

    [Title("Manage Pet")]

    [SerializeField]
    private Image thumbnailImage = null;

    [SerializeField]
    private Text nameAndLevelText = null;

    [SerializeField]
    private Text rankText = null;

    [SerializeField]
    private Text countText = null;

    [SerializeField]
    private Text infoText = null;

    [SerializeField]
    private Button heartLevelUpButton = null;

    [SerializeField]
    private Text heartLevelUpInfoText = null;

    [SerializeField]
    private Text heartLevelUpText = null;

    [SerializeField]
    private Button goldLevelUpButton = null;

    [SerializeField]
    private Text goldLevelUpInfoText = null;

    [SerializeField]
    private Text goldLevelUpText = null;

    [SerializeField]
    private Button petLevelUpButton = null;

    [SerializeField]
    private Image petLevelUpIcon = null;

    [SerializeField]
    private Text petLevelUpInfoText = null;

    [SerializeField]
    private Text petLevelUpText = null;

    [SerializeField]
    private Transform equipedButton = null;
    [SerializeField]
    private Button equipButton = null;

    [SerializeField]
    private Button displayButton = null;

    [SerializeField]
    private Text displayText = null;

    [SerializeField]
    private GameObject disableCover = null;

    [Title("Sell Pets")]
    
    [SerializeField]
    private GameObject sellCover = null;

    [SerializeField]
    private Text rewardForSellText = null;

    [SerializeField]
    private Text availToSellText = null;

    [SerializeField]
    private Button sellButton = null;

    public void UpdateEntity(bool isSellMode, string id, int level, int count, int reversedRarity, bool isOwned, bool isEquipped, bool isMain, bool isShowUpgradeEffect)
	{
        this.petId = id;

        var currentPetBalanceInfo = BalanceInfoManager.Instance.PetInfoList.FirstOrDefault(n => n.Id == id);

        thumbnailImage.sprite = Resources.Load<Sprite>("Pets/Thumbnails/Pet_" + id.Substring(id.Length - 2));
        petLevelUpIcon.sprite = thumbnailImage.sprite;
        rankText.text = currentPetBalanceInfo.Rank;
        countText.text = "x" + count;

        heartLevelUpInfoText.text = $"{LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_item_upgrade")} +5";
        goldLevelUpInfoText.text = $"{LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_item_upgrade")} +1";
        petLevelUpInfoText.text = $"{LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_item_upgrade")} +10";

        if(isOwned)
        {
            // Setting main button status
            if(isEquipped)
            {
                if(isMain)
                {
                    displayButton.gameObject.GetComponentInChildren<Button>().interactable = false;
                    displayText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_set_display");
                }
                else
                {
                    displayButton.gameObject.GetComponentInChildren<Button>().interactable = true;
                    displayText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_set_not_displaying");
                }

                displayButton.gameObject.SetActive(true);
            }
            else
            {
                displayButton.gameObject.SetActive(false);
            }
            
            disableCover.gameObject.SetActive(false);

            var currentPetInfo = ServerNetworkManager.Instance.Inventory.PetList.FirstOrDefault(n => n.Id == id);
            infoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "pet_effect_desc"), currentPetInfo.EffectPower);

            if(level >= ServerNetworkManager.Instance.Setting.MaxPetLevel)
            {
                nameAndLevelText.text = LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "pet_name_" + id.Substring(id.Length - 2)) + "\n" + LanguageManager.Instance.GetTextData(LanguageDataType.UI, "level_max");

                heartLevelUpButton.interactable = false;
                goldLevelUpButton.interactable = false;
                petLevelUpButton.interactable = false;
            }
            else
            {
                nameAndLevelText.text = LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "pet_name_" + id.Substring(id.Length - 2)) + "\n"+ LanguageManager.Instance.GetTextData(LanguageDataType.UI, "level") + level;

                if(level <= (ServerNetworkManager.Instance.Setting.MaxPetLevel - 5))
                {   
                    if(currentPetInfo.HeartLevelupCost <= ServerNetworkManager.Instance.Inventory.Heart)
                        heartLevelUpButton.interactable = true;
                    else
                        heartLevelUpButton.interactable = false;
                }
                else
                    heartLevelUpButton.interactable = false;

                if(level < ServerNetworkManager.Instance.Setting.MaxPetLevel)
                {   
                    if(currentPetInfo.GoldLevelupCost <= ServerNetworkManager.Instance.Inventory.Gold)
                        goldLevelUpButton.interactable = true;
                    else
                        goldLevelUpButton.interactable = false;
                }
                else
                    goldLevelUpButton.interactable = false;

                if(level <= (ServerNetworkManager.Instance.Setting.MaxPetLevel - 10))
                {   
                    if(currentPetInfo.PetLevelupCost <= count)
                        petLevelUpButton.interactable = true;
                    else
                        petLevelUpButton.interactable = false;
                }
                else
                    petLevelUpButton.interactable = false;
            }

            heartLevelUpText.text = LanguageManager.Instance.NumberToString(currentPetInfo.HeartLevelupCost);
            goldLevelUpText.text = LanguageManager.Instance.NumberToString(currentPetInfo.GoldLevelupCost);
            petLevelUpText.text = "x" + currentPetInfo.PetLevelupCost.ToString();            

            if(ServerNetworkManager.Instance.Inventory.SelectedPetList.Exists(n => n.Id == currentPetInfo.Id))
            {
                equipButton.gameObject.SetActive(false);
                equipedButton.gameObject.SetActive(true);
            }
            else if(ServerNetworkManager.Instance.Inventory.SelectedPetList.Count >= ServerNetworkManager.Instance.Inventory.PetCountByBestClass)
            {
                equipButton.interactable = false;
                
                equipButton.gameObject.SetActive(true);
                equipedButton.gameObject.SetActive(false);
            }
            else
            {
                equipButton.interactable = true;

                equipButton.gameObject.SetActive(true);
                equipedButton.gameObject.SetActive(false);
            }

            if(isSellMode)
            {
                displayButton.gameObject.SetActive(false);

                sellCover.gameObject.SetActive(true);
                rewardForSellText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "pet_sell_heart"), reversedRarity);
                availToSellText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "pet_sell_avail"), Mathf.Max(count - 1, 0));
                if(count > 1)
                {
                    rewardForSellText.gameObject.SetActive(true);
                    availToSellText.gameObject.SetActive(true);
                    sellButton.gameObject.SetActive(true);
                }
                else
                {
                    rewardForSellText.gameObject.SetActive(false);
                    availToSellText.gameObject.SetActive(false);
                    sellButton.gameObject.SetActive(false);
                }
            }
            else
            {
                sellCover.gameObject.SetActive(false);
            }
        }
        else
        {
            displayButton.gameObject.SetActive(false);

            if(isSellMode)
            {
                disableCover.gameObject.SetActive(false);
                sellCover.gameObject.SetActive(true);
                rewardForSellText.gameObject.SetActive(false);
                availToSellText.gameObject.SetActive(false);
                sellButton.gameObject.SetActive(false);
            }
            else
            {
                disableCover.gameObject.SetActive(true);
                sellCover.gameObject.SetActive(false);
            }

            nameAndLevelText.text = LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "pet_name_" + id.Substring(id.Length - 2)) + "\n"+ LanguageManager.Instance.GetTextData(LanguageDataType.UI, "level") + level;
            infoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "pet_effect_desc"), currentPetBalanceInfo.EffectPower);
            heartLevelUpText.text = "";
            goldLevelUpText.text = "";
            petLevelUpText.text = "";

            heartLevelUpButton.interactable = false;
            goldLevelUpButton.interactable = false;
            petLevelUpButton.interactable = false;
            
            equipButton.gameObject.SetActive(true);
            equipedButton.gameObject.SetActive(false);
        }

        if(isShowUpgradeEffect)
            ShowUpgradeEffect();
    }

    private void ShowUpgradeEffect()
	{
		SoundManager.Instance.PlaySound("Upgrade");
	}

    public void OnGoldLevelupButtonClicked()
    {
        GameObject.FindObjectOfType<UIPopupPet>().GoldLevelUp(this.petId);
    }

    public void OnHeartLevelupButtonClicked()
    {
        GameObject.FindObjectOfType<UIPopupPet>().HeartLevelUp(this.petId);
    }

    public void OnPetLevelupButtonClicked()
    {
        GameObject.FindObjectOfType<UIPopupPet>().PetLevelUp(this.petId);
    }

    public void OnEquipButtonClicked()
    {
        GameObject.FindObjectOfType<UIPopupPet>().AddPetToSlot(this.petId);
    }

    public void OnSellbuttonClicked()
    {
        GameObject.FindObjectOfType<UIPopupPet>().PetSell(this.petId);
    }

    public void OnSetMainButtonClicked()
    {
        GameObject.FindObjectOfType<UIPopupPet>().SetMainPet(this.petId);
    }
}
