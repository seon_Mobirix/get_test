﻿using UnityEngine;
using UnityEngine.UI;

public class ListItemTrophySlot : MonoBehaviour
{
    private string trophyId = "";

    [SerializeField]
    private Image thumbnailImage = null;

    [SerializeField]
    private Button removeButton = null;

    [SerializeField]
    private Transform lockCover = null;

    [SerializeField]
    private Text lockText = null;

    public void UpdateEntity(bool isLock, string unlockTier, bool isInSlot, string trophyId = "")
	{
        if(isLock)
        {
            lockText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "unlock_at_class"), unlockTier);
            lockCover.gameObject.SetActive(true);
        }
        else
        {
            lockCover.gameObject.SetActive(false);
        
            if(isInSlot)
            {
                thumbnailImage.sprite = Resources.Load<Sprite>("Trophies/Trophy_" + trophyId.Substring(trophyId.Length - 5));
                thumbnailImage.gameObject.SetActive(true);
                removeButton.gameObject.SetActive(true);
            }
            else
            {
                thumbnailImage.gameObject.SetActive(false);
                removeButton.gameObject.SetActive(false);
            }

            this.trophyId = trophyId;
        }
    }

    public void OnRemoveButtonClick()
    {
        if(trophyId != "")
            GameObject.FindObjectOfType<UIPopupRaid>().RemoveTrophyFromSlot(this.trophyId);
    }
}
