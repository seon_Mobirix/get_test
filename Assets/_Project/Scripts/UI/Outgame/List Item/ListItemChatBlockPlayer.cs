﻿using UnityEngine;
using UnityEngine.UI;

public class ListItemChatBlockPlayer : MonoBehaviour
{
    [SerializeField]
    private Text nicknameText;
    [SerializeField]
    private Button blockButton;
    [SerializeField]
    private Button unblockButton;
    
    private string id = "";
    private string nickname = "";

    public void UpdateEntity(string id, string nickname, bool isBlocked)
	{
        this.id = id;
        this.nickname = nickname;

        nicknameText.text = nickname;
        
        blockButton.gameObject.SetActive(!isBlocked);
        unblockButton.gameObject.SetActive(isBlocked);
    }

    public void OnBlockButtonClicked()
    {
       GameObject.FindObjectOfType<UIPopupChatBlock>().Block(id, nickname);
    }

    public void OnUnblockButtonClicked()
    {
        GameObject.FindObjectOfType<UIPopupChatBlock>().UnBlock(id);
    }
}
