﻿using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System.Numerics;
using System.Globalization;

public class ListItemClass : MonoBehaviour
{
    [SerializeField]
	private Text rankInfoText;
    [SerializeField]
	private Text countInfoText;
    [SerializeField]
	private Image classIcon;

    [SerializeField]
	private Text classNameAndLevelText;
    [SerializeField]
	private Text statValueText;

    [SerializeField]
    private Transform goldUpgradeButton;
    [SerializeField]
    private Transform gemUpgradeButton;
    [SerializeField]
	private Text requiredGoldAmountText;
    [SerializeField]
	private Text requiredGemAmountText;

    [SerializeField]
    private Transform upgradeChanceInfo;
    [SerializeField]
	private Text upgradeChanceInfoText;

    [SerializeField]
    private Button mergeButton;
    [SerializeField]
	private Text mergeInfoText;

    [SerializeField]
    private Button equipButton;
    [SerializeField]
    private Transform equipedButton;
    
    [SerializeField]
    private Transform lockPanel;

    private ClassModel currentClassInfo;
    private ClassInfoModel currentClassBalanceInfo;

    private bool isInit = false;
    private bool isAvailableUpgrade = true;
    private bool isGoldUpgrade = true;
    private bool isAvailableMerge = true;

    private string id;
    public string Id
    {
        get{ 
            return id;
        }
    }

    public void UpdateEntity(string id, int level, int count, bool isOwned, bool isEquipped, bool isShowUpgradeEffect, bool isUpgradeSuccess)
	{
        this.id = id;

        goldUpgradeButton.gameObject.SetActive(false);
        gemUpgradeButton.gameObject.SetActive(false);

        // Init first time
        if(!isInit)
        {
            isInit = true;

            currentClassBalanceInfo = BalanceInfoManager.Instance.ClassInfoList.FirstOrDefault(n => n.Id == id);

            if(currentClassBalanceInfo != null)
            {
                classIcon.sprite = Resources.Load<Sprite>("Thumbnails/Class_" + int.Parse(id.Substring(id.Length - 2)).ToString("D2"));
                rankInfoText.text =  LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "rank_name_" + int.Parse(id.Substring(id.Length - 2)).ToString("D2"));
            }
        }

        countInfoText.text = "x" + count.ToString();
    
        var classBasicGoldPower = float.Parse(currentClassBalanceInfo.GoldPower, NumberStyles.Any, CultureInfo.InvariantCulture);
        var currentValue = classBasicGoldPower + classBasicGoldPower / 100f * (level - 1);

        statValueText.text = 
            string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "class_upgrade_desc"), LanguageManager.Instance.NumberToString((BigInteger)(currentValue * 100f)))
            + "\n" + string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "class_berserk_desc"), currentClassBalanceInfo.BerserkPower)
            + " / " + string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "class_attack_speed_desc"), currentClassBalanceInfo.AttackSpeedPower);
       
        var nextRank = int.Parse(id.Substring(id.Length - 2)) + 1;

        if(isOwned)
        {
            currentClassInfo = ServerNetworkManager.Instance.Inventory.ClassList.FirstOrDefault(n => n.Id == id);

            // Check max level
            if(level >= ServerNetworkManager.Instance.Setting.MaxItemLevel)
            {
                classNameAndLevelText.text = string.Format("{0} {1}", LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "class_name_" + int.Parse(id.Substring(id.Length - 2)).ToString("D2")), "\n"+ LanguageManager.Instance.GetTextData(LanguageDataType.UI, "level_max"));
                upgradeChanceInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "upgrade_percentage_info"), 0);
                requiredGemAmountText.text = "100";

                isGoldUpgrade = false;
                isAvailableUpgrade = false;
            }
            else
            {   
                classNameAndLevelText.text = string.Format("{0}\n"+ LanguageManager.Instance.GetTextData(LanguageDataType.UI, "level")+ "{1}", LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "class_name_" + int.Parse(id.Substring(id.Length - 2)).ToString("D2")), level);
                upgradeChanceInfo.gameObject.SetActive(true);

                if(currentClassInfo.LevelUpChancePercent == 0)
                {
                    upgradeChanceInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "upgrade_percentage_info"), 100);
                    requiredGemAmountText.text = "100";
                    
                    isGoldUpgrade = false;
                    
                    // Check gem
                    if(ServerNetworkManager.Instance.TotalGem < 100)
                        isAvailableUpgrade = false;
                    else
                        isAvailableUpgrade = true;
                }
                else
                {                    
                    upgradeChanceInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "upgrade_percentage_info"), currentClassInfo.LevelUpChancePercent);
                    requiredGoldAmountText.text = LanguageManager.Instance.NumberToString(currentClassInfo.LevelupCost);

                    // Check gold
                    if(ServerNetworkManager.Instance.Inventory.Gold < currentClassInfo.LevelupCost)
                        isAvailableUpgrade = false;
                    else
                        isAvailableUpgrade = true;
                }
            }

            int currentCount = count;

            if(isEquipped)
            {
                currentCount = currentCount - 1;

                equipButton.gameObject.SetActive(false);
                equipedButton.gameObject.SetActive(true);
            }
            else
            {
                equipButton.gameObject.SetActive(true);
                equipedButton.gameObject.SetActive(false);

                if(currentCount <= 0)
                    isAvailableUpgrade = false;
            }

            // Check count for merge
            int convertedClassIndex = int.Parse(id.Substring(id.Length - 2));

            // Check this class is last rank or not
            if(convertedClassIndex >= ServerNetworkManager.Instance.Setting.TotalClassCount)
            {
                mergeButton.gameObject.SetActive(false);
                isAvailableMerge = false;
            }
            else if(currentCount < 5)
            {
                mergeButton.gameObject.SetActive(true);
                isAvailableMerge = false;
            }
            else
            {
                mergeButton.gameObject.SetActive(true);
                isAvailableMerge = true;
            }

            if(isAvailableMerge)
                mergeInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_merge"), LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "rank_name_" + nextRank.ToString("D2")), currentCount , 5);
            else
                mergeInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_merge_none"), LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "rank_name_" + nextRank.ToString("D2")), currentCount, 5);

            mergeButton.interactable = isAvailableMerge;

            if(isShowUpgradeEffect)
                ShowUpgradeEffect(isUpgradeSuccess);
        }
        else
        {
            upgradeChanceInfo.gameObject.SetActive(true);

            classNameAndLevelText.text = string.Format("{0}\n"+ LanguageManager.Instance.GetTextData(LanguageDataType.UI, "level")+ "{1}", LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "class_name_" + int.Parse(id.Substring(id.Length - 2)).ToString("D2")), level);
            upgradeChanceInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "upgrade_percentage_info"), 100);
            requiredGoldAmountText.text = LanguageManager.Instance.NumberToString(BigInteger.Parse(currentClassBalanceInfo.LevelupCost, NumberStyles.Any, CultureInfo.InvariantCulture));
            mergeInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_merge_none"), LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "rank_name_" + nextRank.ToString("D2")), 0, 5);

            mergeButton.interactable = false;

            equipButton.gameObject.SetActive(true);
            equipedButton.gameObject.SetActive(false);
        }

        if(isGoldUpgrade)
        {
            goldUpgradeButton.GetComponentInChildren<Button>().interactable = isAvailableUpgrade;
            goldUpgradeButton.gameObject.SetActive(true);
            gemUpgradeButton.gameObject.SetActive(false);
        }
        else
        {
            gemUpgradeButton.GetComponentInChildren<Button>().interactable = isAvailableUpgrade;
            goldUpgradeButton.gameObject.SetActive(false);
            gemUpgradeButton.gameObject.SetActive(true);
        }
    
        lockPanel.gameObject.SetActive(!isOwned || count <= 0);
    }

    private void ShowUpgradeEffect(bool isSuccess)
	{
	    if(isSuccess)
        {
            OutgameController.Instance.ShowToast(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_upgrade_success"));
		    SoundManager.Instance.PlaySound("Upgrade");
        }
        else
        {
            OutgameController.Instance.ShowToast(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "message_item_upgrade_fail"));
            SoundManager.Instance.PlaySound("UpgradeFail");
        }
	}

    public void OnMergeButtonClicked()
    {
        if(isAvailableMerge)
           GameObject.FindObjectOfType<UIPopupEquipment>().RequestClassMerge(currentClassBalanceInfo.Id);
    }

    public void OnEquipButtonClicked()
    {
        GameObject.FindObjectOfType<UIPopupEquipment>().RequestClassEquip(currentClassBalanceInfo.Id);
    }

    public void OnGoldUpgradeButtonClicked()
    {
        if(isAvailableUpgrade && isGoldUpgrade)
            GameObject.FindObjectOfType<UIPopupEquipment>().RequestClassUpgrade(currentClassBalanceInfo.Id);
    }

    public void OnGemUpgradeButtonClicked()
    {
        if(isAvailableUpgrade)
            GameObject.FindObjectOfType<UIPopupEquipment>().RequestClassUpgrade(currentClassBalanceInfo.Id);
    }
}
