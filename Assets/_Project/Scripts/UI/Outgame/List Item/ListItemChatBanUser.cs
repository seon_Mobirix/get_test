﻿using UnityEngine;
using UnityEngine.UI;

public class ListItemChatBanUser : MonoBehaviour
{
    [SerializeField]
    private Text playFabIdText;
    [SerializeField]
    private Text banTimeText;
    [SerializeField]
    private Button chatLogButton;
    [SerializeField]
    private Button cancelButton;
    
    private string id = "";

    public void UpdateEntity(string id, double banTime)
	{
        this.id = id;

        playFabIdText.text = id;
    }

    public void OnChatLogButtonClicked()
    {
        GameObject.FindObjectOfType<UIPopupReport>().ShowChatLog(id);
    }

    public void OnReportCancelButtonClicked()
    {
        GameObject.FindObjectOfType<UIPopupReport>().ReportCancel(id);
    }
}
