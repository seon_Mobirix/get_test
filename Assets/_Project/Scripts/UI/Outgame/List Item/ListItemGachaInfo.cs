﻿using UnityEngine;
using UnityEngine.UI;

public class ListItemGachaInfo : MonoBehaviour
{
    [SerializeField]
    private Text itemNameAndGrade = null;

    [SerializeField]
    private Text gachaChance = null;
    
    public void UpdateEntity(string itemName, string itemRank, float chance)
	{
        if(itemRank == "")
            itemNameAndGrade.text = $"{itemName}";
        else
            itemNameAndGrade.text = $"{itemName} ({itemRank})";

        gachaChance.text = $"{(chance * 100f).ToString("F4")}%";
    }
}