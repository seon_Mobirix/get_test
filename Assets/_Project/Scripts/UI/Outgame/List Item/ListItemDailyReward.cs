﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class ListItemDailyReward : MonoBehaviour
{
    [SerializeField]
	private Transform itemInfo;
    [SerializeField]
	private List<Sprite> itemList;
    [SerializeField]
	private Text indexText;
	[SerializeField]
	private Image itemIcon;
	[SerializeField]
	private Text itemCountText;
    [SerializeField]
    private Button getButton;
    [SerializeField]
	private Transform blockPanelReceived;
    [SerializeField]
	private Transform blockPanelLocked;
    [SerializeField]
	private Transform outlineObject;

    private double lastClickTimeStampMilliseconds = 0f;

    public void UpdateEntity(int index, string itemId, string itemCode, int itemCount, bool isReceived, bool isLocked)
	{
        itemInfo.gameObject.SetActive(false);
        blockPanelReceived.gameObject.SetActive(false);
        blockPanelLocked.gameObject.SetActive(false);

        if(itemCount >= ServerNetworkManager.Instance.Setting.DailyRewardHighlightValue)
            outlineObject.gameObject.SetActive(true);
        else
            outlineObject.gameObject.SetActive(false);

        if(isReceived)
        {
            getButton.interactable = false;
            blockPanelReceived.gameObject.SetActive(true);
        }
        else if(isLocked)
        {
            getButton.interactable = false;
            blockPanelLocked.gameObject.SetActive(true);
        }
        else
        {
            getButton.interactable = true;
        }

        if(itemId == "gem")
        {
            itemInfo.gameObject.SetActive(true);
            itemIcon.sprite = itemList[0];
        }
        else if(itemId == "gacha")
        {
            itemInfo.gameObject.SetActive(true);
            itemIcon.sprite = itemList[1];
        }

        indexText.text = index.ToString();
		itemCountText.text = itemCount.ToString();
    }

    public void OnDailyRewardClicked()
    {
        var passedMilliSeconds = DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastClickTimeStampMilliseconds;

        if(passedMilliSeconds >= 500d)
        {
            lastClickTimeStampMilliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();
            OutgameController.Instance.RequestDailyReward();
        }
    }
}
