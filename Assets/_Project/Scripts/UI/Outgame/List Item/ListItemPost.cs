﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ListItemPost : MonoBehaviour
{
    [SerializeField]
	private List<Sprite> itemList;
	[SerializeField]
	private Image itemIcon;
	[SerializeField]
	private Image emptyItemIcon;
	[SerializeField]
	private Text itemCountText;
	[SerializeField]
	private Text messageText;
	[SerializeField]
	private Button receiveButton;
	[SerializeField]
	private Button deleteButton;
	
	private bool isInit = false;
	private int mailId;
	private PostRewardModel attachedItemInfo;

	[SerializeField]
	private Text timeText;
	private double expiredTime = 0f;

	private double lastClickTimeStampMilliseconds = 0f;

	private void Start()
	{
		StartCoroutine(UpdateTimeCoroutine());
	}

    private IEnumerator UpdateTimeCoroutine()
    {
        while(true)
        {
			if(isInit)
			{
				var timeLeft = (int)(expiredTime - DateTimeOffset.Now.ToUnixTimeSeconds());

				if(timeLeft < 0)
				{
					this.timeText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "time_end");
					
					if(attachedItemInfo != null)
					{
						receiveButton.gameObject.SetActive(false);
						deleteButton.gameObject.SetActive(true);
					}

					break;
				}
				else
				{
					int hours = timeLeft / 3600;
					timeLeft -= hours * 3600;
					int minutes = timeLeft / 60;
					timeLeft -= minutes * 60;
					int seconds = timeLeft;
					timeLeft -= seconds;
					
					if(hours / 24 > 0)
						this.timeText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "time_days"), (hours/24).ToString());
					else if(hours > 0)
						this.timeText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "time_hours"), hours.ToString());
					else if(minutes > 0)
						this.timeText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "time_mins"), minutes.ToString());
					else
						this.timeText.text = LanguageManager.Instance.GetTextData(LanguageDataType.UI, "time.end_soon");
				}
			}
			yield return new WaitForSeconds(1f);
		}
	}
	
	public void UpdateEntity(int id, string message, double expiredTime, PostRewardModel attachedItem)
	{
		mailId = id;
		
		var convertMessage = LanguageManager.Instance.GetTextData(LanguageDataType.UI, message);
		
		if(convertMessage != "")
			this.messageText.text = convertMessage;
		else
		{
			string[] separatingStrings = { "<s>", "<m>" };
			string[] words = message.Split(separatingStrings, System.StringSplitOptions.RemoveEmptyEntries);

			if(words.Length == 2)
				this.messageText.text = $"{words[0]}: {words[1]}";
			else
				this.messageText.text = message;
		}
		
		this.attachedItemInfo = attachedItem;

		if(attachedItemInfo == null)
		{
			itemIcon.gameObject.SetActive(false);
			emptyItemIcon.gameObject.SetActive(true);
			deleteButton.gameObject.SetActive(true);
			receiveButton.gameObject.SetActive(false);
		}
		else
		{
			if(attachedItemInfo.ItemId == "gem")
			{
				itemIcon.gameObject.SetActive(true);
				emptyItemIcon.gameObject.SetActive(false);
				itemIcon.sprite = itemList[0];
				this.itemCountText.text = string.Format("{0}", attachedItem.Count);

				deleteButton.gameObject.SetActive(false);
				receiveButton.gameObject.SetActive(true);
			}
			else if(attachedItemInfo.ItemId == "guild_token")
			{
				itemIcon.gameObject.SetActive(true);
				emptyItemIcon.gameObject.SetActive(false);
				itemIcon.sprite = itemList[2];
				this.itemCountText.text = string.Format("{0}", attachedItem.Count);

				deleteButton.gameObject.SetActive(false);
				receiveButton.gameObject.SetActive(true);
			}
			else
			{
				itemIcon.gameObject.SetActive(false);
				emptyItemIcon.gameObject.SetActive(true);
				this.itemCountText.text = "";

				deleteButton.gameObject.SetActive(true);
				receiveButton.gameObject.SetActive(false);
			}
		}
		
		this.expiredTime = expiredTime;
		isInit = true;
	}

	public void OnReceiveButtonClick()
    {
		var passedMilliSeconds = DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastClickTimeStampMilliseconds;
		
		if(passedMilliSeconds >= 500d)
		{
			lastClickTimeStampMilliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();
			OutgameController.Instance.RequestAttachedItem(mailId);
		}
	}

	public void OnDeleteButtonClick()
	{
		var passedMilliSeconds = DateTimeOffset.Now.ToUnixTimeMilliseconds() - lastClickTimeStampMilliseconds;

		if(passedMilliSeconds >= 500d)
		{
			lastClickTimeStampMilliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();
			OutgameController.Instance.RequestDeleteMail(mailId);
		}
	}
}