﻿using UnityEngine;
using UnityEngine.UI;

public class ListItemEventReward : MonoBehaviour
{
    [SerializeField]
    private Image rewardGemIcon = null;

    [SerializeField]
    private Image rewardIcon = null;

    [SerializeField]
    private Text rewardName = null;

    [SerializeField]
    private Text rewardCount = null;

    [SerializeField]
    private Button getButton = null;

    private int index;

    public void UpdateEntity(int index, string itemId, string itemCode, int count, bool isAvailable)
	{
        this.index = index;

        if(itemCode == "GE")
        {
            rewardGemIcon.gameObject.SetActive(true);
            rewardIcon.gameObject.SetActive(false);

            rewardGemIcon.sprite = Resources.Load<Sprite>("Currency/bundle_gem_01");
            rewardName.text = $"{count} {LanguageManager.Instance.GetTextData(LanguageDataType.UI, "button_tab_gem")}";
        }
        else if(itemCode == "relic")
        {
            rewardGemIcon.gameObject.SetActive(false);
            rewardIcon.gameObject.SetActive(true);

            rewardIcon.sprite = Resources.Load<Sprite>("Thumbnails/Relic_" + int.Parse(itemId.Substring(itemId.Length - 2)).ToString("D2"));
            rewardName.text = LanguageManager.Instance.GetTextData(LanguageDataType.ENTITY, "relic_name_" + int.Parse(itemId.Substring(itemId.Length - 2)).ToString("D2"));
        }

        rewardCount.text = $"x {count}";

        // Set get button interative
        getButton.interactable = isAvailable;
    }

    public void OnGetButtonClicked()
    {
        GameObject.FindObjectOfType<UIPopupAttendanceEvent>().RequestEventReward(this.index);
    }
}
