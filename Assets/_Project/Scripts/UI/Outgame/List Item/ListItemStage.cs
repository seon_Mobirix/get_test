﻿using UnityEngine;
using UnityEngine.UI;
using System.Numerics;
using Sirenix.OdinInspector;

public class ListItemStage : MonoBehaviour
{
    [Title ("Public Part")]
    [SerializeField]
	private Text stageNameText;
    [SerializeField]
    private Transform moveButton;
    [SerializeField]
    private Transform huntingButton;
    
    [Title ("Unlock Stage Part")]
    [SerializeField]
    private Transform unlockContent;
    [SerializeField]
	private Text itemFarmingInfoText;
    [SerializeField]
	private Text rewardGoldText;
    
    [Title ("Lock Stage Part")]
    [SerializeField]
    private Transform lockContent;
    
    private int stageNumber;

    public void UpdateEntity(int stageNumber, int mobIdx, string monsterId, string farmItem, float farmChance, BigInteger rewardGold, bool isLock, bool isMoved)
	{
        this.stageNumber = stageNumber;
        stageNameText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "stage_name"), mobIdx);

        lockContent.gameObject.SetActive(false);
        unlockContent.gameObject.SetActive(false);
        huntingButton.gameObject.SetActive(false);
        moveButton.gameObject.SetActive(false);
        
        if(isLock)
        {
            lockContent.gameObject.SetActive(true);
            moveButton.gameObject.SetActive(true);
        }
        else
        {
            unlockContent.gameObject.SetActive(true);
            itemFarmingInfoText.text = string.Format(LanguageManager.Instance.GetTextData(LanguageDataType.UI, "desc_farm_item"), farmItem, farmChance * 100f);
            rewardGoldText.text = LanguageManager.Instance.NumberToString(rewardGold);
            
            if(isMoved)
            {
                huntingButton.gameObject.SetActive(true);
                moveButton.gameObject.SetActive(false);
            }
            else
            {
                huntingButton.gameObject.SetActive(false);
                moveButton.gameObject.SetActive(true);
            }
        }
    }

    public void OnMoveButtonClicked()
    {
        GameObject.FindObjectOfType<UIPopupWorldInfo>().SelectedStage(stageNumber);
    }
}
