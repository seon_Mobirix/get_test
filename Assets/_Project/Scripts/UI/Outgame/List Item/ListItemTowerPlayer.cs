using UnityEngine;
using UnityEngine.UI;

public class ListItemTowerPlayer : MonoBehaviour
{
    [SerializeField]
    private Text nicknameText;

    public void UpdateEntity(string nickname)
	{
        nicknameText.text = nickname;
    }
}
