﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using LitJson;
using UnityEngine.Networking;

#if !UNITY_EDITOR && UNITY_IOS
using System.Runtime.InteropServices;
#endif

namespace SB
{
    public class MoreManager : MonoBehaviour
    {
        private const bool NEW_MORE_MANAGER = true;

        private const string MORE_MAIN_URL = "https://dzpu6za66svjl.cloudfront.net/moregame/main.json";

        /// OS 종류 
        enum MOBI_OS
        {
            _aos = 0,
            _ios
        }

        /// 파일 존재 여부 
        public enum MOBI_EXIST
        {
            _none_exist = 0,
            _exist
        }

        /// 이미지 확장자 종류 
        enum MOBI_IMGTYPE
        {
            _webp = 0,
            _png
        }

        /// 화면 회전 종류 
        public enum MOBI_SCREEN
        {
            _row = 0,
            _col
        }

        /// 아이콘 정보 
        public struct IconInfo
        {
            public MOBI_EXIST nExist;
            public string strImgfile;
            public string strPackage;
        }

        /// 아이콘 이외 이미지 정보 
        public struct MoreEtcImgInfo
        {
            public string strBackgroundfile;
            public string strGotitfile;
            public string strBtnbackfile;
            public string strMorebtnfile;
            public string strStartbtnfile;
            public string strXbtnfile;
        }

        MOBI_OS m_cOs;
        [HideInInspector]
        public MOBI_SCREEN m_cScreen;
        int m_nViewRate;
        bool m_bDownloadDone;
        bool m_bFirstIconCall;
        bool m_bPushStart;
        int m_nDownloadIconCnt;
        int m_nDownloadEtcCnt;
        string m_strImgFormat;
        string m_strInfo;
        string m_strCountry;

        List<IconInfo> m_IconinfoVector = new List<IconInfo>();
        List<string> m_strInstallList = new List<string>();
        MoreEtcImgInfo m_MoreEtcImgInfo;

        struct etcInfo
        {
            public MOBI_EXIST nExist;
            public string strfile;
        }

        etcInfo[] g_etcInfo = new etcInfo[6];

        MoreManager()
        {
            m_cOs = MOBI_OS._aos;
            m_cScreen = MOBI_SCREEN._row;
            m_bDownloadDone = false;
            m_bFirstIconCall = true;
            m_bPushStart = false;
            m_strInfo = "";
            m_strCountry = "";
            m_nViewRate = 10;
            m_nDownloadIconCnt = 0;
            m_nDownloadEtcCnt = 0;

            m_strInstallList.Clear();
        }

        ~MoreManager()
        {
            m_IconinfoVector.Clear();
            m_strInstallList.Clear();
        }

        private static MoreManager instance = null;
        public static MoreManager Instance
        {
            get{
                if(instance == null)
                    instance = GameObject.FindObjectOfType<MoreManager>();
                return instance;
            }
        }

        public void Start()
        {
#if UNITY_ANDROID || UNITY_EDITOR
            Init();
#endif   
        }

        /// <summary>
        /// Moremanager 초기화 함수 
        /// </summary>
        public void Init()
        {
            checkInstalledPackage();

            // CDN 접속 
#if UNITY_ANDROID || UNITY_EDITOR
            connect_cdn(MOBI_OS._aos, MOBI_IMGTYPE._png, MOBI_SCREEN._row, "com.fireshrike.dk", GetNationalCode(), "", false);
#elif UNITY_IOS
		    connect_cdn(MOBI_OS._ios, MOBI_IMGTYPE._png, MOBI_SCREEN._row, "1550256397", GetNationalCode(), "", false);
#endif
        }

        /// <summary>
        /// CDN 접속하는 함수 
        /// <param name="p_nOs">OS 종류</param>
        /// <param name="p_nImgType">이미지 확장자 종류</param>
        /// <param name="p_nScreen">화면 해상도 종류 ( 가로 또는 세로 )</param>
        /// <param name="p_strInfo">패키지 네임 또는 앱 ID</param>
        /// <param name="p_strCountry">2자리 ISO 국가코드</param>
        /// <param name="p_strConnectUrl">연결할 URL 주소</param>
        /// <param name="p_bPushStart">푸시 실행 여부 ( CrossManager를 위해 추가. 기본값은 false )</param>
        /// </summary>
        void connect_cdn(MOBI_OS p_nOs, MOBI_IMGTYPE p_nImgType, MOBI_SCREEN p_nScreen, string p_strInfo, string p_strCountry, string p_strConnectUrl, bool p_bPushStart)
        {
            if (p_strCountry.Equals("none"))
                p_strCountry = "";

            m_bDownloadDone = false;
            m_bFirstIconCall = true;
            m_bPushStart = p_bPushStart;
            m_nViewRate = 10;
            m_nDownloadIconCnt = 0;
            m_nDownloadEtcCnt = 0;
            m_IconinfoVector.Clear();

            for (short i = 0; i < 6; ++i)
            {
                g_etcInfo[i].nExist = MOBI_EXIST._none_exist;
                g_etcInfo[i].strfile = "";
            }

            m_cOs = p_nOs;
            m_cScreen = p_nScreen;
            if (p_nImgType == MOBI_IMGTYPE._webp) m_strImgFormat = ".webp";
            else m_strImgFormat = ".png";
            m_strInfo = p_strInfo;
            if (p_strCountry != null) m_strCountry = p_strCountry;
            else m_strCountry = "";

            // main Json 파일을 얻는다.
            if (NEW_MORE_MANAGER)
            {
                StartCoroutine(getJson(MORE_MAIN_URL));
            }
            else
            {
                StartCoroutine(getJson(p_strConnectUrl));
            }
        }

        IEnumerator getJson(string url)
        {
            using (UnityWebRequest www = UnityWebRequest.Get(url))
            {
                yield return www.SendWebRequest();

                // check error
                if (!string.IsNullOrEmpty(www.error))
                {
                    Debug.LogErrorFormat("connection error : {0}", www.error);
                    yield break;
                }

                // get reward
                string jsonString = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);

                if (NEW_MORE_MANAGER)
                    setMoreJson_main(jsonString);
                else
                    setMoreJson(jsonString);
            }
        }

        IEnumerator connect_cdn_new(string url)
        {
            using (UnityWebRequest www = UnityWebRequest.Get(url))
            {
                yield return www.SendWebRequest();

                // check error
                if (!string.IsNullOrEmpty(www.error))
                {
                    Debug.LogErrorFormat("connection error : {0}", www.error);
                    yield break;
                }

                // get reward
                string json = System.Text.Encoding.UTF8.GetString(www.downloadHandler.data);
                setMoreJson(json);
            }
        }

        /// <summary>
        /// 모어게임 준비 완료 여부를 체크하는 함수 
        /// <returns> 모어게임 준비 완료 여부. true - 완료 false - 준비되지 않음 </returns>
        /// </summary>
        public bool isMoreViewOk()
        {
            if (m_bFirstIconCall)
            {
                m_bFirstIconCall = false;

                if (m_bPushStart) return false;
                else
                {
                    if (m_bDownloadDone)
                    {
                        if (m_nViewRate >= (Random.Range(0, 100) + 1)) return true;
                    }
                }

                return false;
            }

            return m_bDownloadDone;
        }


        /// <summary>
        /// 모어게임 구성 정보를 가져오는 함수 
        /// <param name="p_IconInfo"> 아이콘 구성 데이터 </param>
        /// <param name="p_moreEtcInfo"> 아이콘 이외 구성 이미지 데이터 </param>
        /// </summary>
        public void getIconlist(ref IconInfo[] p_IconInfo, ref MoreEtcImgInfo p_moreEtcInfo)
        {
            if (m_bDownloadDone)
            {
                p_moreEtcInfo.strBackgroundfile = g_etcInfo[0].strfile;
                p_moreEtcInfo.strGotitfile = g_etcInfo[1].strfile;
                p_moreEtcInfo.strBtnbackfile = g_etcInfo[2].strfile;
                p_moreEtcInfo.strMorebtnfile = g_etcInfo[3].strfile;
                p_moreEtcInfo.strStartbtnfile = g_etcInfo[4].strfile;
                p_moreEtcInfo.strXbtnfile = g_etcInfo[5].strfile;

                int nCnt = m_IconinfoVector.Count;
                int[] nTmp = new int[nCnt];
                int[] nRand = new int[nCnt];

                int i, j, k;
                for (i = 0; i < nCnt; ++i) nTmp[i] = i;
                for (i = 0; i < nCnt; ++i)
                {
                    k = Random.Range(0, nCnt - i);
                    nRand[i] = nTmp[k];

                    for (j = k; j < nCnt - 1; ++j) nTmp[j] = nTmp[j + 1];
                }

                for (i = 0; i < 12; ++i)
                {
                    p_IconInfo[i].nExist = m_IconinfoVector[nRand[i]].nExist;
                    p_IconInfo[i].strPackage = m_IconinfoVector[nRand[i]].strPackage;
                    p_IconInfo[i].strImgfile = m_IconinfoVector[nRand[i]].strImgfile;
                }
            }
        }


        /// <summary>
        /// 이미 설치된 어플리케이션을 설정 해주는 함수 
        /// <param name="package"> 이미 설치된 어플리케이션의 패키지 네임 </param>
        /// </summary>
        void setPackagename(string p_strPackage)
        {
            m_strInstallList.Add(p_strPackage);
        }

        void setMoreJson_main(string p_strJson)
        {
            JsonData root = JsonMapper.ToObject(p_strJson);
            JsonData defailt_value;

            if(root.Count <= 0)
            {
                Debug.LogError("json parse failed");
                return;
            }

            JsonData cat_list = root["CATEGORYS"];
            JsonData app_list = root["APPS"];

            string cat = app_list[m_strInfo].ToString();

            if(string.Compare(cat, 0, "none", 0, 4) == 0)
            {

            }
            else
            {
                string url = cat_list[cat].ToString();
                if(string.Compare(url, 0, "none", 0, 4) == 0)
                {

                }
                else
                {
                    StartCoroutine(connect_cdn_new(url));
                }
            }
        }

        void setMoreJson(string p_strJson)
        {
            JsonData root = JsonMapper.ToObject(p_strJson);

            if (root.Count <= 0)
            {
                Debug.LogError("json parse failed");
                return;
            }

            // 제외 앱 
            string str_deny_app = root["DENY_APPID"].ToString();
            if (string.IsNullOrEmpty(str_deny_app))
                str_deny_app = "empty";
            string[] v_str_deny_app = str_deny_app.Split('|');

            bool bDenyAppExist = !(str_deny_app.Equals("empty") || string.IsNullOrEmpty(str_deny_app));
            if (bDenyAppExist)
            {
                // 제외 앱 검사
                for (int i = 0; i < v_str_deny_app.Length; ++i)
                {
                    if (m_strInfo.Equals(v_str_deny_app[i])) return;
                }
            }

            // 확률
            m_nViewRate = System.Convert.ToInt32(root["RATE"].ToString(), 10);

            // 기타 이미지
            if (m_cScreen == MOBI_SCREEN._row) g_etcInfo[0].strfile = root["BG_IMG_URL_ROW"].ToString();
            else g_etcInfo[0].strfile = root["BG_IMG_URL_COL"].ToString();
            if (string.IsNullOrEmpty(g_etcInfo[0].strfile)) return;
            setEtcProc(0);

            g_etcInfo[1].strfile = root["GOTIT_IMG_URL"].ToString();
            if (string.IsNullOrEmpty(g_etcInfo[1].strfile)) return;
            setEtcProc(1);

            g_etcInfo[2].strfile = root["ICONBACK_IMG_URL"].ToString();
            if (string.IsNullOrEmpty(g_etcInfo[2].strfile)) return;
            setEtcProc(2);

            g_etcInfo[3].strfile = root["MORE_IMG_URL"].ToString();
            if (string.IsNullOrEmpty(g_etcInfo[3].strfile)) return;
            setEtcProc(3);

            g_etcInfo[4].strfile = root["START_IMG_URL"].ToString();
            if (string.IsNullOrEmpty(g_etcInfo[4].strfile)) return;
            setEtcProc(4);

            g_etcInfo[5].strfile = root["X_IMG_URL"].ToString();
            if (string.IsNullOrEmpty(g_etcInfo[5].strfile)) return;
            setEtcProc(5);

            // Package
            JsonData each_event_value;
            string strAllowCountry = "";
            string strDenyCountry = "";
            string strPackage = "";
            string strImgUrl = "";
            string[] v_str_allow_country;
            string[] v_str_deny_country;

            bool bAllowCountryExist = false;
            bool bDenyCountryExist = false;
            bool bOk = false;

            int more_list_cnt = root["PACKAGE"].Count;
            for (int i = 0; i < more_list_cnt; ++i)
            {
                bOk = false;

                each_event_value = root["PACKAGE"][i];

                if (m_cOs == MOBI_OS._aos)
                {
                    strAllowCountry = each_event_value["AOS_ALLOW_CONTRY_CODE"].ToString();
                    strDenyCountry = each_event_value["AOS_DENY_CONTRY_CODE"].ToString();
                    strPackage = each_event_value["AOS_PACKAGE"].ToString();
                }
                else
                {
                    strAllowCountry = each_event_value["IOS_ALLOW_CONTRY_CODE"].ToString();
                    strDenyCountry = each_event_value["IOS_DENY_CONTRY_CODE"].ToString();
                    strPackage = each_event_value["iOS_APPID"].ToString();
                }

                strImgUrl = each_event_value["IMG_URL"].ToString();
                if (string.IsNullOrEmpty(strImgUrl))
                    strImgUrl = "empty";

                if (string.IsNullOrEmpty(strPackage))
                    strPackage = "empty";

                if (strPackage.Equals("empty") || strImgUrl.Equals("empty") || strPackage.Equals("") || strImgUrl.Equals("")) continue;

                bAllowCountryExist = !(strAllowCountry.Equals("empty") || string.IsNullOrEmpty(strAllowCountry) || strAllowCountry.Equals("all"));
                bDenyCountryExist = !(strDenyCountry.Equals("empty") || string.IsNullOrEmpty(strDenyCountry) || strDenyCountry.Equals("all"));

                // 내 국가 코드가 있는지 검사
                if (m_strCountry.Length == 0)
                {
                    if (bAllowCountryExist || bDenyCountryExist) continue;
                }

                // 내 패키지명이 있으면 제외
                if (m_strInfo.Equals(strPackage)) continue;

                if (!bAllowCountryExist && !bDenyCountryExist) bOk = true;
                else if (bAllowCountryExist)
                {
                    v_str_allow_country = strAllowCountry.Split('|');

                    for (int j = 0; j < v_str_allow_country.Length; ++j)
                    {
                        string country_name = v_str_allow_country[j];

                        if (string.Compare(m_strCountry, 0, country_name, 0, 2) == 0)
                        {
                            bOk = true;
                            break;
                        }
                    }
                }
                else
                {
                    bOk = true;
                    v_str_deny_country = strDenyCountry.Split('|');

                    for (int j = 0; j < v_str_deny_country.Length; ++j)
                    {
                        string country_name = v_str_deny_country[j];

                        if (string.Compare(m_strCountry, 0, country_name, 0, 2) == 0)
                        {
                            bOk = false;
                            break;
                        }
                    }
                }

                if (bOk)
                {
                    strImgUrl += m_strImgFormat;

                    IconInfo iconInfo;
                    iconInfo.nExist = MOBI_EXIST._none_exist;
                    iconInfo.strPackage = strPackage;

                    string strTmp = strImgUrl.Substring(strImgUrl.LastIndexOf("/"));

                    string path = string.Format("{0}{1}", Application.persistentDataPath, strTmp);

                    if (System.IO.File.Exists(path))
                    {
                        iconInfo.nExist = MOBI_EXIST._exist;
                        iconInfo.strImgfile = path;
                    }
                    else
                    {
                        iconInfo.strImgfile = strImgUrl;
                    }

                    m_IconinfoVector.Add(iconInfo);
                }
            }


            for (int i = 0; i < m_IconinfoVector.Count; ++i)
            {
                if (m_IconinfoVector[i].nExist == MOBI_EXIST._exist) ++m_nDownloadIconCnt;
                else
                {
                    StartCoroutine(iconDownload(m_IconinfoVector[i].strImgfile));
                }
            }

            if (m_nDownloadIconCnt == m_IconinfoVector.Count && m_nDownloadIconCnt >= 12)
            {
                package_install_check();
            }

        }

        IEnumerator iconDownload(string url)
        {
            WWW www = new WWW(url);

            yield return www;

            if (www.bytesDownloaded == 0)
                yield break;
            else
            {
                byte[] bytes = www.bytes;

                string filename = url.Substring(url.LastIndexOf('/'));
                string path = string.Format("{0}{1}", Application.persistentDataPath, filename);

                System.IO.File.WriteAllBytes(Application.persistentDataPath + url.Substring(url.LastIndexOf('/')), bytes);

                for (int i = 0; i < MoreManager.Instance.m_IconinfoVector.Count; ++i)
                {
                    if (MoreManager.Instance.m_IconinfoVector[i].strImgfile.Equals(url))
                    {
                        IconInfo iconInfo = MoreManager.Instance.m_IconinfoVector[i];
                        iconInfo.strImgfile = path;
                        iconInfo.nExist = MOBI_EXIST._exist;
                        MoreManager.Instance.m_IconinfoVector[i] = iconInfo;
                        MoreManager.Instance.m_nDownloadIconCnt += 1;
                    }
                }

                if (MoreManager.Instance.m_IconinfoVector.Count == MoreManager.Instance.m_nDownloadIconCnt && MoreManager.Instance.m_nDownloadIconCnt >= 12)
                {
                    MoreManager.Instance.package_install_check();
                }
            }

            www.Dispose();
            www = null;
            yield return null;
        }

        void setEtcProc(int p_nIdx)
        {
            g_etcInfo[p_nIdx].strfile = string.Format("{0}{1}", g_etcInfo[p_nIdx].strfile, m_strImgFormat);
            string strImgpath = string.Format("{0}{1}", Application.persistentDataPath, g_etcInfo[p_nIdx].strfile.Substring(g_etcInfo[p_nIdx].strfile.LastIndexOf('/')));

            if (System.IO.File.Exists(strImgpath))
            {
                ++m_nDownloadEtcCnt;
                g_etcInfo[p_nIdx].strfile = strImgpath;
                g_etcInfo[p_nIdx].nExist = MOBI_EXIST._exist;
            }
            else
            {
                StartCoroutine(etcIconDownload(g_etcInfo[p_nIdx].strfile));
            }
        }

        IEnumerator etcIconDownload(string url)
        {
            WWW www = new WWW(url);

            yield return www;

            if (www.bytesDownloaded == 0)
            {
                yield break;
            }
            else
            {
                byte[] bytes = www.bytes;

                string path = string.Format("{0}{1}", Application.persistentDataPath, url.Substring(url.LastIndexOf('/')));

                System.IO.File.WriteAllBytes(Application.persistentDataPath + url.Substring(url.LastIndexOf('/')), bytes);

                for (int i = 0; i < 6; ++i)
                {
                    if (g_etcInfo[i].strfile.Equals(url))
                    {
                        g_etcInfo[i].nExist = MOBI_EXIST._exist;
                        g_etcInfo[i].strfile = path;
                        MoreManager.Instance.m_nDownloadEtcCnt += 1;
                    }
                }

                if (MoreManager.Instance.m_nDownloadEtcCnt == 6) MoreManager.Instance.package_install_check();
            }

            www.Dispose();

            www = null;
        }

        void package_install_check()
        {
            if (m_nDownloadEtcCnt == 6 && m_nDownloadIconCnt == m_IconinfoVector.Count && m_nDownloadIconCnt >= 12)
            {

                for (int i = 0; i < 6; ++i)
                {
                    if (g_etcInfo[i].strfile.Length == 0) return;

                    // Texture 생성 테스트 
                    Texture2D texture = null;
                    byte[] byteTexture = System.IO.File.ReadAllBytes(g_etcInfo[i].strfile);
                    if (byteTexture.Length > 0)
                    {
                        texture = new Texture2D(2, 2);
                        if (!texture.LoadImage(byteTexture))
                        {
                            System.IO.File.Delete(g_etcInfo[i].strfile);
                            g_etcInfo[i].strfile = "";
                            g_etcInfo[i].nExist = MOBI_EXIST._none_exist;

                            return;
                        }
                    }
                }

                short nCnt = 0;
                for (int i = 0; i < m_IconinfoVector.Count; ++i)
                {
                    if (m_IconinfoVector[i].nExist == MOBI_EXIST._exist) ++nCnt;
                    IconInfo iconInfo = m_IconinfoVector[i];
                    iconInfo.nExist = MOBI_EXIST._none_exist;
                    m_IconinfoVector[i] = iconInfo;

                    if (iconInfo.strImgfile.Length == 0) return;

                    // Texture 생성 테스트 
                    Texture2D texture = null;
                    byte[] byteTexture = System.IO.File.ReadAllBytes(iconInfo.strImgfile);
                    if (byteTexture.Length > 0)
                    {
                        texture = new Texture2D(2, 2);
                        if (!texture.LoadImage(byteTexture))
                        {
                            System.IO.File.Delete(iconInfo.strImgfile);
                            iconInfo.strImgfile = "";
                            m_IconinfoVector[i] = iconInfo;

                            return;
                        }
                    }


                }

                if (nCnt != m_IconinfoVector.Count) return;

                nCnt = 0;
                for (short i = 0; i < 6; ++i)
                {
                    if (g_etcInfo[i].nExist == MOBI_EXIST._exist) ++nCnt;
                }

                if (nCnt != 6) return;

                for (int i = 0; i < m_strInstallList.Count; ++i)
                {
                    for (int j = 0; j < m_IconinfoVector.Count; ++j)
                    {
                        if (m_strInstallList[i].Equals(m_IconinfoVector[j].strPackage))
                        {
                            IconInfo iconInfo = m_IconinfoVector[j];
                            iconInfo.nExist = MOBI_EXIST._exist;
                            m_IconinfoVector[j] = iconInfo;
                        }
                    }
                }

                m_bDownloadDone = true;

            }

        }

        /// <summary>
        /// JNI 통신을 통해 설정하는 함수 
        /// <param name="data"> CDN 연결을 위한 파싱 전 데이터 </param>
        /// </summary>
        void NativeMoreSet(string data)
        {
            string[] splitData = data.Split('|');
            if (splitData.Length != 7)
                return;

            MOBI_OS nOS = MOBI_OS._aos;
            bool bResult = true;

            if (!string.IsNullOrEmpty(splitData[0]))
            {
                if (splitData[0].Equals("AOS_PACKAGE")) nOS = MOBI_OS._aos;
                else if (splitData[0].Equals("iOS_APPID")) nOS = MOBI_OS._ios;
                else bResult = false;
            }
            else
            {
                bResult = false;
            }

            MOBI_SCREEN nScreen = (int.Parse(splitData[5]) == (int)MOBI_SCREEN._row) ? MOBI_SCREEN._row : MOBI_SCREEN._col;

            MOBI_IMGTYPE nImgType = MOBI_IMGTYPE._png;
            if (!string.IsNullOrEmpty(splitData[1]) && bResult)
            {
                if (splitData[1].Equals("webp")) nImgType = MOBI_IMGTYPE._webp;
                else if (splitData[1].Equals("png")) nImgType = MOBI_IMGTYPE._png;
                else bResult = false;
            }
            else
            {
                bResult = false;
            }

            if (string.IsNullOrEmpty(splitData[3])) bResult = false;
            if (string.IsNullOrEmpty(splitData[4])) bResult = false;

            if (bResult)
            {
                bool bPushStart = (int.Parse(splitData[6]) == 1) ? true : false;
                MoreManager.Instance.connect_cdn(nOS, nImgType, nScreen, splitData[3], splitData[2], splitData[4], bPushStart);
            }
        }

        /// <summary>
        /// JNI통신을 통해 설치 여부를 설정하는 함수 
        /// <param name="package"> 이미 설치된 어플리케이션의 패키지 네임 </param>
        /// </summary>
        void NativeInstallPackage(string package)
        {
            if (!string.IsNullOrEmpty(package))
            {
                MoreManager.Instance.m_strInstallList.Add(package);
            }
        }

        private void checkInstalledPackage()
        {
            Debug.Log("checkInstalledPackage");
#if UNITY_EDITOR

            // add test installed packagename
            NativeInstallPackage("com.mobirix.throwball");
            NativeInstallPackage("com.mobirix.shuriken");
            NativeInstallPackage("com.mobirix.snake");

#elif UNITY_ANDROID

            using (AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            using (AndroidJavaObject currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
            using (AndroidJavaObject packageManager = currentActivity.Call<AndroidJavaObject>("getPackageManager"))
            using (AndroidJavaObject appList = packageManager.Call<AndroidJavaObject>("getInstalledApplications", 0))
            {
                int count = appList.Get<int>("size");

                for(int i=0; i<count; ++i)
                {
                    using (AndroidJavaObject app = appList.Call<AndroidJavaObject>("get", i))
                    {
                        string appPackageName = app.Get<string>("packageName");

                        // 패키지 네임에 mobirix 포함 여부 체크
                        if(appPackageName.Contains(".mobirix."))
                        {
                            // mobirix 포함된 패키지 네임의 경우 앱 설치 리스트에 추가 
                            NativeInstallPackage(appPackageName);
                        }
                    }
                }
            }
#elif UNITY_IOS
#endif
        }


#region Moregame Store Func
        
#if UNITY_EDITOR

        public static void openMoregameUrl(string packageName)
        {
            Debug.LogFormat("Open In Store : {0}", packageName);
        }

        public static void openMoregameDeveloperPage()
        {
            Debug.Log("open Developer Site");
        }

#elif UNITY_ANDROID

        public static void openMoregameUrl(string packageName)
        {
            using (AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            using (AndroidJavaObject currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
            {
                // uri
                using (AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri"))
                using (AndroidJavaObject uri = uriClass.CallStatic<AndroidJavaObject>("parse", string.Format("market://details?id={0}", packageName)))
                using (AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent"))
                {
                    // action view
                    string ACTION_VIEW = intentClass.GetStatic<string>("ACTION_VIEW");

                    // create Intent 
                    using (AndroidJavaObject intent = new AndroidJavaObject("android.content.Intent", ACTION_VIEW, uri))
                    {
                        currentActivity.Call("startActivity", intent);
                    }
                }
            }
        }

        public static void openMoregameDeveloperPage()
        {
            try
            {
                using (AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
                using (AndroidJavaObject currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
                using (AndroidJavaObject packageManager = currentActivity.Call<AndroidJavaObject>("getPackageManager"))
                using (AndroidJavaObject storeInfo = packageManager.Call<AndroidJavaObject>("getApplicationInfo", "com.android.vending", 0))
                using (AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent"))
                using (AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri"))
                {
                    string storePackageName = storeInfo.Get<string>("packageName");

                    // Play Store 존재 여부 검사
                    if (string.Compare(storePackageName, "com.android.vending") == 0)
                    {   // Play Store 가 설치 되어 있는 경우 
                        // Play Store 에서 열기 
                        using (AndroidJavaObject storeIntent = packageManager.Call<AndroidJavaObject>("getLaunchIntentForPackage", "com.android.vending"))
                        {
                            string ACTION_VIEW = intentClass.GetStatic<string>("ACTION_VIEW");
                            string CATEGORY_BROWSABLE = intentClass.GetStatic<string>("CATEGORY_BROWSABLE");

                            storeIntent.Call<AndroidJavaObject>("setAction", ACTION_VIEW);
                            storeIntent.Call<AndroidJavaObject>("addCategory", CATEGORY_BROWSABLE);

                            // Uri 설정
                            using (AndroidJavaObject uri = uriClass.CallStatic<AndroidJavaObject>("parse", "http://play.google.com/store/apps/dev?id=4864673505117639552"))
                            {
                                storeIntent.Call<AndroidJavaObject>("setData", uri);

                                // 스토어 오픈
                                currentActivity.Call("startActivity", storeIntent);
                            }
                        }
                    }
                    else
                    {   // Play Store 가 설치되지 않은 경우
                        Application.OpenURL("http://play.google.com/store/apps/dev?id=4864673505117639552");
                    }
                }
            }
            catch (System.Exception e)
            {
                // Exception 발생 시 브라우저에서 오픈
                Application.OpenURL("http://play.google.com/store/apps/dev?id=4864673505117639552");
            }
        }

#elif UNITY_IOS

        [DllImport ("__Internal")]
        private static extern void _sb_moregame_openMoregame(string packageName);

        public static void openMoregameUrl(string packageName)
        {
            _sb_moregame_openMoregame(packageName);
        }


        [DllImport ("__Internal")]
        private static extern void _sb_moregame_developersite();

        public static void openMoregameDeveloperPage()
        {
            _sb_moregame_developersite();
        }

#endif

#endregion

#region National Code
    #if UNITY_EDITOR
        public static string GetNationalCode()
        {
            return "GR";
        }
    #elif UNITY_ANDROID
        public static string GetNationalCode()
        {
            string nationalCode = "";

            using (AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            using (AndroidJavaObject currentActivity = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity"))
            using (AndroidJavaObject context = currentActivity.Call<AndroidJavaObject>("getBaseContext"))
            {
                // get TELEPHONY_SERVICE
                string TELEPHONY_SERVICE = "";
                using (AndroidJavaClass activity = new AndroidJavaClass("android.content.Context"))
                {
                    TELEPHONY_SERVICE = activity.GetStatic<string>("TELEPHONY_SERVICE");
                }

                // get national code
                using (AndroidJavaObject telephonyManager = context.Call<AndroidJavaObject>("getSystemService", TELEPHONY_SERVICE))
                {
                    nationalCode = telephonyManager.Call<string>("getNetworkCountryIso");
                }
            }


            return nationalCode;
        }
    #elif UNITY_IOS
        [DllImport ("__Internal")]
        private static extern string _sb_support_getNationalCode();
        public static string GetNationalCode()
        {
            string nationalCode = "none";

            string receiveNationalCode = _sb_support_getNationalCode();
            if(!string.IsNullOrEmpty(receiveNationalCode))
                nationalCode = receiveNationalCode;

            return nationalCode;
        }
    #endif
#endregion
    }
}