
extern "C"
{
    void _sb_moregame_developersite()
    {
        NSString* moreGameUrl = @"http://www.mobirix.net";
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:moreGameUrl]];
    }
    
    void _sb_moregame_openMoregame(const char* packageName)
    {
        
        NSString* moreGameUrl = [NSString stringWithFormat:@"http://itunes.apple.com/app/id%s?mt=8",packageName];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:moreGameUrl]];
    }
}
