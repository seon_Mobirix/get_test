I. 서버에서 특정 유저의 데이터를 강제로 변경하고자 할 때 함께 변경해야 하는 요소

1. last_device_tag: 아무 값이나 가능
2. progress_value: 1로 수정

II. 시스템 우편함 사용법
    1. Playfab에서 골드 지급을 받을 계정을 검색
    2. Cloud Script 메뉴 클릭
    3. Function name: addSystemMail 선택
    4. Arguments - 아래와 같은 양식으로 입력 (Count: 수량 = (string 값으로 입력))
{
  message: "message_mail_system_reward",
  attachedItem: {
    ItemId: "gold",
    ItemCode: "gold",
    Count: "10"
  }
}
    5. Run Cloud Script 클릭
    6. 게임 내 시스템 우편함에서 메일 확인
itemId: gold, heart, ticket_raid, weapon, class, pet , mercenary(아이템 분류)
itemCode: gold, heart, ticket_raid, weapon_11, class_22, pet_11, mercenary_04 (아이템 상세 아이디)
Count: 수량  (string 값으로 입력)
관련 함수
    1. 메일 보내기: addSystemMail
    2. 전체 메일 보상 받기: requestAttachedItemSystemMailItemAll
    3. 오래된 메일 삭제: requestDeleteSystemMail

III. 던전기사 키우기 유지 보수 로직 정리
1. 신규 무기, 클래스, 펫 추가 시
    * SendSummonResultMessage를 위해서 하드 코딩 된 최소 무기, 클래스, 펫 인덱스 값 수정하기
2. 신규 펫 추가 시
    1. 에셋 선정
    2. 에셋에 해당 펫 프리팹에서 콜라이더 제거 후 -> Pet_번호 프리팹 생성 후 Pet->visual에 넣기
    3. 인게임에 넣어서 프리팹 스케일 설정
    4. 해당 프리팹에 에니메이션 컨트롤러 수정 (entry -> idel -> walk)
        1. Transition durations 25s 설정 (앞에 애니가 긴 경우 수정 가능)
3. 신규 월드 추가 시
    1. Setting -> Last world Index 값 수정
4. 아레나 보상 및 횟수 설정 방법
    1. 아레나 일일 제한 횟수
        1. Arena.ts -> GetLeftDailyLimit 함수에서 arena’s allowedCount 변경
    2. setting.json -> arena reward list
        1. setting.json ->  ArenaRewardList안에 데이터 수정
        2. Arena.ts -> ReceiveArenaReward 함수에서 “Give reward based on last season's performance”의 주석 밑에 코드 수정
5. 스텟, 스킬 최대 레벨 설정 방법
    1. setting.json -> MaxStatLevel
    2. setting.json -> MaxSkillLevel
6. 무료젬 일일 횟수 설정
    1. setting.json -> FreeGemDailyLimit 변경
    2. economy.ts에서 하드 코딩된 부분 수정하기
7. 서버별 채팅 기능  on/off 설정
    1. setting.json -> chatoffserverlist에 채팅 끌 서버 아이디 추가하기 ex) 03
8. 텍스트 메일 보내기
    * {
        *   sender: "운영자",
        *   message: "환불하신 패키지들의 재화가 회수되었습니다."
    * }
9. 보상 메일 보내기
    * {
    *   message: "test",
    *   attachedItem: {
    *     ItemId: “gem”, “guild_token”
    *     ItemCode: “GE", “GT”
    *     Count: 10
    *   }
    * }
10. CheckCheater 함수 돌리는 방법
    1. {
    2.   limitWeaponId: 16,
    3.   limitStageProgress: 61
    4. }

IV. QA 브랜치를 만드는 방법

현재 아래 내용이 적용된 master_qa_2를 사용중입니다. 
master 브랜치에서 다음과 같이 수정을 하면 qa 서버에 접속합니다.

1. 게스트 로그인 버튼 강제로 열기
    * UIPopupSelectAuthMethod.cs -> guestButton.gameObject.SetActive(true)
2.  app_name 변경하기
    * values-ko/string.xml
3. Balance sheet and setting json QA용으로 받아오기
    * SevernetworkManager.cs -> // Get balance growth data 부분 수정
    * setting + “_qa”
4. UNITY_POST_PROCESSING_STACK_V2;ODIN_INSPECTOR;BYPASS_LIAPP (LIAPP 바이 패스 추가)
5. OutgameController.cs에서 ServerNetworkManager.Instance.AuthKey에 + “_qa”
6. OutgameController.cs -> Login()  함수수정
        uiViewOutgameLoadingInitial.Show();
        ServerNetworkManager.Instance.Login(
            () => {
                    RefreshLastDeviceForLogin(false);
            }, 
            OnLogInFailure
        );