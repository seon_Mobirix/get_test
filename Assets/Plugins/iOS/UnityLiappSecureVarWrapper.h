//
//  UnityLiappWrapper.h
// 
//
//  Created by Lockin Company on 2020/04/13.
//  Copyright © 2020 Lockin Company. All rights reserved.
//

#ifndef UnityLiappSecureVarWrapper_h
#define UnityLiappSecureVarWrapper_h

//LiappSecureVariable
typedef bool (*Wrapper_SVUI1)();
typedef bool (*Wrapper_SVUG1)(int*);
typedef bool (*Wrapper_SVUG2)(long*);

FOUNDATION_EXPORT void SettingLiappSVUI1( bool (*callback)() );
FOUNDATION_EXPORT void SettingLiappSVUG1( bool (*callback)(int*) );
FOUNDATION_EXPORT void SettingLiappSVUG2( bool (*callback)(long*) );
//LiappSecureVariable END

NS_CLASS_AVAILABLE_IOS(4_0) @interface UnityLiappSecureVarWrapper : NSObject

+ (bool)SVUI1;
+ (bool)SVUG1 : (int*)c_Array;
+ (bool)SVUG2 : (long*)c_Array;
@end

#endif /* UnityLiappWrapper_h */
