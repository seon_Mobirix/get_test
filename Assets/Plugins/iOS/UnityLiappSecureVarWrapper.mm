//
//  UnityLiappWrapper.m
// 
//
//  Created by Lockin Company on 2020/04/13.
//  Copyright © 2020 Lockin Company. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "UnityLiappSecureVarWrapper.h"

Wrapper_SVUI1 wrapper_SVUI1;
Wrapper_SVUG1 wrapper_SVUG1;
Wrapper_SVUG2 wrapper_SVUG2;


//LiappSecureVariable
FOUNDATION_EXPORT void __attribute__((visibility("default"))) SettingLiappSVUI1( bool (*callback)() )
{
    wrapper_SVUI1 = callback;
}

FOUNDATION_EXPORT void __attribute__((visibility("default"))) SettingLiappSVUG1( bool (*callback)(int*) )
{
    wrapper_SVUG1 = callback;
}

FOUNDATION_EXPORT void __attribute__((visibility("default"))) SettingLiappSVUG2( bool (*callback)(long*) )
{
    wrapper_SVUG2 = callback;
}

@implementation UnityLiappSecureVarWrapper

+ (bool)SVUI1
{
    return wrapper_SVUI1();
}

+ (bool)SVUG1 : (int*) Input
{
    return wrapper_SVUG1(Input);
}

+ (bool)SVUG2 : (long*) Input
{
    return wrapper_SVUG2(Input);
}

@end


extern "C"
{

    bool SVUI1()
    {
        return [UnityLiappSecureVarWrapper SVUI1];
    }

    bool SVUG1(int* c_array)
    {
        bool bRet = false;
#ifndef __ARM_ARCH_ISA_A64
        int *pnArray = new int[5];
        bRet = [UnityLiappSecureVarWrapper SVUG1:pnArray];
        
        if(bRet)
            *c_array = (int)(pnArray);
#endif
        return bRet;
    }

    bool SVUG2(long* c_array)
    {
        bool bRet = false;
        long *pnArray = new long[5];
        bRet = [UnityLiappSecureVarWrapper SVUG2:pnArray];
        
        if(bRet)
            *c_array = (long)pnArray;
        
        return bRet;
    }
}

