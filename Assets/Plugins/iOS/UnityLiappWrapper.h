
//
//  UnityLiappWrapper.h
// 
//
//  Created by Lockin Company on 2020/04/13.
//  Copyright © 2020 Lockin Company. All rights reserved.
//

#ifndef UnityLiappWrapper_h
#define UnityLiappWrapper_h

typedef int (*Wrapper_LiappLA1)();
typedef int (*Wrapper_LiappLA2)();
typedef NSString* (*Wrapper_LiappGA)(NSString* strData);
typedef NSString* (*Wrapper_LiappGetMassage)();
typedef NSString* (*Wrapper_LiappGI)();
typedef void (*Wrapper_LiappSUID)(NSString* strData);


FOUNDATION_EXPORT void SettingLiappLA1( int (*callback)() );
FOUNDATION_EXPORT void SettingLiappLA2( int (*callback)() );
FOUNDATION_EXPORT void SettingLiappGA( NSString* (*callback)( NSString* strData ) );
FOUNDATION_EXPORT void SettingLiappGetMessage( NSString* (*callback)() );
FOUNDATION_EXPORT void SettingLiappGI( NSString* (*callback)( ) );
FOUNDATION_EXPORT void SettingLiappSUID( void (*callback)(NSString* strData ) );

NS_CLASS_AVAILABLE_IOS(4_0) @interface UnityLiappWrapper : NSObject

+ (int)LA1;
+ (int)LA2;
+ (NSString*)GA;
+ (NSString*)GA: (NSString*)strData;
+ (NSString*)GetMessage;
+ (NSString*)GI;
+ (void)SUID: (NSString*)strData;

@end

#endif /* UnityLiappWrapper_h */
