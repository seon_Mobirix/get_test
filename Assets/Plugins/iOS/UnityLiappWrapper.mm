
//
//  UnityLiappWrapper.mm
// 
//
//  Created by Lockin Company on 2020/04/13.
//  Copyright © 2020 Lockin Company. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "UnityLiappWrapper.h"

Wrapper_LiappLA1 wrapper_LA1;
Wrapper_LiappLA2 wrapper_LA2;
Wrapper_LiappGA wrapper_GA;
Wrapper_LiappGetMassage wrapper_GM;
Wrapper_LiappGI wrapper_GI;
Wrapper_LiappSUID wrapper_SUID;

FOUNDATION_EXPORT void __attribute__((visibility("default"))) SettingLiappLA1( int (*callback)() )
{
    wrapper_LA1 = callback;
}

FOUNDATION_EXPORT void __attribute__((visibility("default"))) SettingLiappLA2( int (*callback)() )
{
    wrapper_LA2 = callback;
}

FOUNDATION_EXPORT void __attribute__((visibility("default"))) SettingLiappGA( NSString* (*callback)( NSString* strData ) )
{
    wrapper_GA = callback;
}

FOUNDATION_EXPORT void __attribute__((visibility("default"))) SettingLiappGetMessage( NSString* (*callback)() )
{
    wrapper_GM = callback;
}

FOUNDATION_EXPORT void __attribute__((visibility("default"))) SettingLiappGI( NSString* (*callback)() )
{
    wrapper_GI = callback;
}

FOUNDATION_EXPORT void __attribute__((visibility("default"))) SettingLiappSUID( void(*callback)( NSString* strData ) )
{
    wrapper_SUID = callback;
}



char* cStringCopy(const char* string)
{
    if (string == NULL)
        return NULL;
    char* res = (char*)malloc(strlen(string) + 1);
    strcpy(res, string);
    return res;
}


@implementation UnityLiappWrapper

+ (int)LA1
{
    return wrapper_LA1();
}

+ (int)LA2
{
    return wrapper_LA2();
}

+ (NSString*)GA: (NSString*)strData
{
    return wrapper_GA(strData);
}

+ (NSString*)GetMessage
{
    return wrapper_GM();
}

+ (NSString*)GI
{
    return wrapper_GI();
}

+ (void)SUID: (NSString*)strData
{
    return wrapper_SUID(strData);
}

@end



extern "C"
{
    int LiappLA1()
    {
        return [UnityLiappWrapper LA1];
    }

    int LiappLA2()
    {
        return [UnityLiappWrapper LA2];
    }

    char* LiappGA(char* user_key_from_server)
    {
        NSString *user_key = [NSString stringWithUTF8String: user_key_from_server];

        NSString *authtoken = [UnityLiappWrapper GA: user_key ];
        return cStringCopy([authtoken UTF8String]);
    }

    char* LiappGetMessage() {
        NSString* message = [UnityLiappWrapper GetMessage ];
        return cStringCopy( [message UTF8String] );
    }


    char* LiappGI() {
        NSString* nsRet = [UnityLiappWrapper GI ];
        return cStringCopy( [nsRet UTF8String] );
    }

    void LiappSUID(char *pszData) {
        
        NSString *nsData = [NSString stringWithUTF8String: pszData];
        [UnityLiappWrapper SUID: nsData ];
        return ;
    }

}

