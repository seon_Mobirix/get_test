﻿//#define LIAPP_FOR_DEV
#define LIAPP_FOR_DISTRIBUTE

namespace Liapp
{
    using UnityEngine;

    public class LiappForUnityImplAndroid
    {
        public static int LIAPP_EXCEPTION = -1;
        public static int LIAPP_SUCCESS = 0;
        public static int LIAPP_DETECTED = 1;
        public static int LIAPP_DETECTED_ANTI_DEBUGGER = 1;
        public static int LIAPP_DETECTED_INTEGRITY = 2;
        public static int LIAPP_DETECTED_VM = 3;
        public static int LIAPP_DETECTED_ROOTING = 7;
        public static int LIAPP_DETECTED_HACKING_TOOL = 8;

        private int nRet = LIAPP_EXCEPTION;

        AndroidJavaClass _LiappAgent = null;

        private static LiappForUnityImplAndroid instance = null;

        public static LiappForUnityImplAndroid Instance
        {
            get
            {
    #if LIAPP_FOR_DEV
                Debug.Log("[LiappForUnityImplAndroid] Instance get DEV VERSION");
    #elif LIAPP_FOR_DISTRIBUTE
                  
    #endif
                if (instance != null)
                    return instance;
                else
                {

                    instance = new LiappForUnityImplAndroid();
                    return instance;
                }
            }
        }

        public void Init()
        {
            if(_LiappAgent == null)
                _LiappAgent = new AndroidJavaClass("com.lockincomp.liappagent.LiappAgentforUnity");
        }

        public int LA1()
        {
            Init();

    #if UNITY_ANDROID && LIAPP_FOR_DISTRIBUTE
            return nRet = _LiappAgent.CallStatic<int>("LA1");
    #else
            return LIAPP_EXCEPTION;
    #endif
        }

        public int LA2()
        {
            Init();
    
    #if UNITY_ANDROID && LIAPP_FOR_DISTRIBUTE
            return nRet = _LiappAgent.CallStatic<int>("LA2");
    #else
            return LIAPP_EXCEPTION;
    #endif
        }

        public string GA(string user_key_from_server)
        {
            Init();

            // you don’t create to ‘user_key_from_server’.
            // ‘user_key_from_server’is from the app server.
    #if UNITY_ANDROID && LIAPP_FOR_DISTRIBUTE
            // send ‘authtoken’ to app server for token verification.
            return _LiappAgent.CallStatic<string>("GA", user_key_from_server);
    #else
            return "DEV_TEST_KEY";
    #endif
        }

        public string GetMessage()
        {
            Init();

    #if UNITY_ANDROID && LIAPP_FOR_DISTRIBUTE
            return _LiappAgent.CallStatic<string>("GetMessage");
    #else
            return "DEV_TEST_MESSAGE";
    #endif
        }
    }
}