﻿
//#define LIAPP_FOR_DEV
#define LIAPP_FOR_DISTRIBUTE

namespace Liapp
{
    using System;
    using System.Runtime.InteropServices;
    using UnityEngine;

    public class LiappforUnityImpl
    {
        public static int LIAPP_EXCEPTION = -1;
        public static int LIAPP_SUCCESS = 0;
        public static int LIAPP_DETECTED = 1;
        public static int LIAPP_DETECTED_ANTI_DEBUGGER = 1;
        public static int LIAPP_DETECTED_INTEGRITY = 2;
        public static int LIAPP_DETECTED_VM = 3;
        public static int LIAPP_DETECTED_ROOTING = 7;
        public static int LIAPP_DETECTED_HACKING_TOOL = 8;

#if UNITY_IOS && LIAPP_FOR_DISTRIBUTE
        [DllImport("__Internal")]
        private static extern int LiappLA1();

        [DllImport("__Internal")]
        private static extern int LiappLA2();

        [DllImport("__Internal")]
        private static extern string LiappGA(string pszUser_key_from_server);

        [DllImport("__Internal")]
        private static extern string LiappGetMessage();
#endif

        private static LiappforUnityImpl instance = null;

        public static LiappforUnityImpl Instance
        {
            
            get
            {
#if LIAPP_FOR_DEV
                Debug.Log("[LiappforUnityImpl] Instance get DEV VERSION");
#elif LIAPP_FOR_DISTRIBUTE
                      
#endif
                if (instance != null)
                    return instance;
                else
                {

                    instance = new LiappforUnityImpl();
                    return instance;
                }
            }
        }

        public int LA1()
        {
#if UNITY_IOS && LIAPP_FOR_DISTRIBUTE
            return LiappLA1();
#else
            return LIAPP_EXCEPTION;
#endif
        }

        public int LA2()
        {
#if UNITY_IOS && LIAPP_FOR_DISTRIBUTE
            return LiappLA2();
#else
            return LIAPP_EXCEPTION;
#endif
        }

        public string GA(string pszUser_key_from_server)
        {
#if UNITY_IOS && LIAPP_FOR_DISTRIBUTE
            return LiappGA(pszUser_key_from_server);
#else
            return "DEV_TEST_KEY";
#endif
        }

        public string GetMessage()
        {
#if UNITY_IOS && LIAPP_FOR_DISTRIBUTE
            return LiappGetMessage();
#else
            return "DEV_TEST_MESSAGE";
#endif
        }
    }

    

}