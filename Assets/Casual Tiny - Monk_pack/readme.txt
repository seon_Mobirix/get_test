            ▶ >>JJ Studio <<◀

Character Pack optimized for Mobile Action RPG implementations!

---------------------------------------------------------------
Casual Tiny Character - Monk Pack

■ Monk 1
Polys: 1894
Verts: 1422
Textures: 512x512-1, 256x256-1

■ Monk 2
Polys: 2502
Verts: 1995
Textures: 512x512-1, 256x256-1 

■ Monk 3
Polys: 2486
Verts: 1891
Textures: 512x512-1, 256x256-1
 
■ Monk 4
Polys: 2521
Verts: 2005
Textures: 512x512-1, 256x256-1

■ Monk 5
Polys: 3941
Verts: 3051
Textures: 512x512-1, 256x256-1

■ Monk 6
Polys: 3582
Verts: 3428
Textures: 512x512-1, 256x256-1

Fx_Prefaps(x10)

Animations(x5) :
(Animation Type_Generic)
Idle, Walk, Attack, Active, Passive
---------------------------------------------------------------



Thank you for your purchase!
If you have any questions mail me at 2018studiojj@gmail.com


▼Check out our other Assets▼
https://assetstore.unity.com/publishers/38479