import { APIGatewayProxyHandler } from "aws-lambda";
import "source-map-support/register";
import * as AWS from "aws-sdk";

AWS.config.update({ region: "ap-northeast-2" });

const dynamodb = new AWS.DynamoDB();

export const logChat: APIGatewayProxyHandler = async (event, _context) => {
  const body = JSON.parse(event.body);

  const item = {
    TableName: "chatLogTable",
    Item: {
      channel: { S: body.ChannelName },
      logged_at: { S: new Date().toISOString() },
      user_id: { S: body.UserId },
      msg: { S: body.Message },
    },
  };

  await dynamodb.putItem(item).promise();
  return {
    statusCode: 200,
    body: JSON.stringify(item, null, 2),
  };
};

export const logApplePurchase: APIGatewayProxyHandler = async (event, _context) => {
  const body = JSON.parse(event.body);

  const item = {
    TableName: "appleRefundLogTable",
    Item: {
      type: { S: body.notification_type },
      logged_at: { S: new Date().toISOString() },
      receipt: { S: JSON.stringify(body.unified_receipt.latest_receipt_info)},
      full_body: { S: JSON.stringify(body, null, 2) },
    },
  };

  await dynamodb.putItem(item).promise();

  return {
    statusCode: 200,
    body: JSON.stringify(body, null, 2)
  };
};
